#include "led.h"
#include "usart_nrf.h"
#include "main.h"
#define Overload            2000-1//200ms
#define FrequencyDivision   ((84000000/10000)-1)
extern volatile DtuDetail Dtu3Detail;
//网络标志位  0:未连接网络  1:连接网络
vu8 connect_net = 0;
//连接服务器标志位 0:未连接服务器 1:连接服务器
vu8 connect_server = 0;
//有服务器数据 0:无数据 1:有数据
vu8 server_data = 0;

extern vu8 downfile;
vu8 DTUError = 0;
vu8 MIError = 0;
vu8 MeterError = 0;
vu32 cnt = 0;
vu8 SysState = 0;
vu8 NetState = 0;
vu8 MiState = 0;
vu8 ModeState = 0;
vu8 FaultState = 0;
vu8 Sys_open_cnt = 0;
vu8 Sys_updata_cnt = 0;
vu8 Net_cnt = 0;
vu8 Mi_cnt = 0;
vu8 Mode_cnt = 0;
vu8 Fault_cnt = 0;
vu8 Down_cnt = 0;
extern vu8 APP_Flg;
extern vu32 server_link_time;
extern vu8 server_link_mode;
extern vu8 server_signal_strength;
void led_time_init(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    //使能TIM3时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    //自动重装载值
    TIM_TimeBaseInitStructure.TIM_Period = Overload;
    //定时器分频
    TIM_TimeBaseInitStructure.TIM_Prescaler = FrequencyDivision;
    //向上计数模式
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    //初始化TIM3
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);
    //允许定时器3更新中断
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
    //使能定时器3
    TIM_Cmd(TIM3, ENABLE);
    //定时器3中断
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    //抢占优先级1
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
    //子优先级3
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void LED_Init(void)
{
    /**
    * LED_RUN           PA0
    * LED_2.4G          PA4
    * LED_GPRS          PA5
    * LED_Internet      PA6
    * LED_Internet      PA8
    **/
    GPIO_InitTypeDef  GPIO_InitStructure;
    //使能GPIOF时钟
    RCC_AHB1PeriphClockCmd(LED0_GPIO_CLK | LED1_GPIO_CLK | LED2_GPIO_CLK | LED3_GPIO_CLK | LED4_GPIO_CLK, ENABLE);
    //GPIOF9,F10初始化设置
    GPIO_InitStructure.GPIO_Pin = LED0_GPIO_PIN | LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN | LED4_GPIO_PIN;
    //普通输出模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    //推挽输出
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    //100MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    //上拉
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    //初始化
    GPIO_Init(LED0_GPIO_PORT, &GPIO_InitStructure);
    //设置高，灯灭
    GPIO_SetBits(LED4_GPIO_PORT, LED0_GPIO_PIN | LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN | LED4_GPIO_PIN);
    led_time_init();
}

void led_Status_polling(void)
{
    switch(SysState)
    {
        case Sys_open:
            if(cnt % 3 == 0)
            {
                switch(Sys_open_cnt)
                {
                    case 0:
                        LED0_ON;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 1:
                        LED0_OFF;
                        LED1_ON;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 2:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_ON;
                        LED3_OFF;
                        break;

                    case 3:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_ON;
                        break;
                }

                Sys_open_cnt = (Sys_open_cnt + 1) % 4;

                if(Sys_open_cnt == 0)
                {
                    SysState = Sys_run;
                }
            }

            break;

        case Sys_run:
            {
                if(downfile == 1)
                {
                    if(cnt % 1 == 0)
                    {
                        switch(Down_cnt)
                        {
                            case 0:
                                LED0_ON;
                                LED1_OFF;
                                LED2_OFF;
                                break;

                            case 1:
                                LED0_OFF;
                                LED1_ON;
                                LED2_OFF;
                                break;

                            case 2:
                                LED0_OFF;
                                LED1_OFF;
                                LED2_ON;
                                break;

                            case 3:
                                LED0_OFF;
                                LED1_OFF;
                                LED2_OFF;
                                break;
                        }

                        Down_cnt = (Down_cnt + 1) % 4;
                    }
                }
                else
                {
                    if(APP_Flg == 1)
                    {
                        NetState = Net_OftenOff;
                    }
                    else
                    {
                        /*与服务器通讯成功后服务器灯光常亮*/
                        NetState = connect_net + server_data;
                        /*hzwang_20200424*/
                    }

                    //LED连接状态  0:无ID 1:搜索不全 2:全部搜到
                    MiState = Dtu3Detail.Property.LedState;
                    ModeState = Mode_OftenOn;

                    if(server_data == 1)
                    {
                        server_link_time = RTC_Getsecond();

                        if(Netmode_Used == GPRS_MODE)
                        {
                            server_signal_strength = Dtu3Major.Property.GPRS_CSQ;
                        }
                        else if(Netmode_Used == WIFI_MODE)
                        {
                            server_signal_strength = Dtu3Major.Property.wifi_RSSI;
                        }
                        else if(Netmode_Used == CABLE_MODE)
                        {
                            server_signal_strength = 100;
                        }
                    }

                    if(DTUError == 1)
                    {
                        FaultState = 0;
                    }
                    else if(MIError == 1)
                    {
                        FaultState = 1;
                    }
                    else if(MeterError == 1)
                    {
                        FaultState = 2;
                    }
                    else
                    {
                        FaultState = 3;
                    }

                    switch(NetState)
                    {
                        case Net_Flashing:
                            if(cnt % 1 == 0)
                            {
                                switch(Net_cnt)
                                {
                                    case 0:
                                        Net_Led_ON;
                                        break;

                                    case 1:
                                        Net_Led_OFF;
                                        break;
                                }

                                Net_cnt = (Net_cnt + 1) % 2;
                            }

                            break;

                        case Net_SlowFlash:
                            if(cnt % 3 == 0)
                            {
                                switch(Net_cnt)
                                {
                                    case 0:
                                        Net_Led_ON;
                                        break;

                                    case 1:
                                        Net_Led_OFF;
                                        break;
                                }

                                Net_cnt = (Net_cnt + 1) % 2;
                            }

                            break;

                        case Net_OftenOn:
                            Net_Led_ON;
                            break;

                        case Net_OftenOff:
                            if(cnt % 1 == 0)
                            {
                                switch(Net_cnt)
                                {
                                    case 0:
                                        Net_Led_ON;
                                        break;

                                    case 1:
                                        Net_Led_OFF;
                                        break;
                                }

                                Net_cnt = (Net_cnt + 1) % 10;
                            }

                            break;
                    }

                    switch(MiState)
                    {
                        case Mi_Flashing:
                            if(cnt % 1 == 0)
                            {
                                switch(Mi_cnt)
                                {
                                    case 0:
                                        Mi_Led_ON;
                                        break;

                                    case 1:
                                        Mi_Led_OFF;
                                        break;
                                }

                                Mi_cnt = (Mi_cnt + 1) % 2;
                            }

                            break;

                        case Mi_SlowFlash:
                            if(cnt % 3 == 0)
                            {
                                switch(Mi_cnt)
                                {
                                    case 0:
                                        Mi_Led_ON;
                                        break;

                                    case 1:
                                        Mi_Led_OFF;
                                        break;
                                }

                                Mi_cnt = (Mi_cnt + 1) % 2;
                            }

                            break;

                        case Mi_OftenOff:
                            if(cnt % 1 == 0)
                            {
                                switch(Mi_cnt)
                                {
                                    case 0:
                                        Mi_Led_ON;
                                        break;

                                    case 1:
                                        Mi_Led_OFF;
                                        break;
                                }

                                Mi_cnt = (Mi_cnt + 1) % 10;
                            }

                            break;

                        case Mi_OftenOn:
                            Mi_Led_ON;
                            break;
                    }

                    /*工作状态*/
                    switch(ModeState)
                    {
                        case Mode_Flashing:
                            if(cnt % 1 == 0)
                            {
                                switch(Mode_cnt)
                                {
                                    case 0:
                                        Mode_Led_ON;
                                        break;

                                    case 1:
                                        Mode_Led_OFF;
                                        break;
                                }

                                Mode_cnt = (Mode_cnt + 1) % 2;
                            }

                            break;

                        case Mode_SlowFlash:
                            if(cnt % 3 == 0)
                            {
                                switch(Mode_cnt)
                                {
                                    case 0:
                                        Mode_Led_ON;
                                        break;

                                    case 1:
                                        Mode_Led_OFF;
                                        break;
                                }

                                Mode_cnt = (Mode_cnt + 1) % 2;
                            }

                            break;

                        case Mode_OftenOff:
                            if(cnt % 1 == 0)
                            {
                                switch(Mode_cnt)
                                {
                                    case 0:
                                        Mode_Led_ON;
                                        break;

                                    case 1:
                                        Mode_Led_OFF;
                                        break;
                                }

                                Mode_cnt = (Mode_cnt + 1) % 10;
                            }

                            break;

                        case Mode_OftenOn:
                            Mode_Led_ON;
                            break;
                    }
                }

                /*故障*/
                switch(FaultState)
                {
                    case Fault_Flashing:
                        if(cnt % 1 == 0)
                        {
                            switch(Fault_cnt)
                            {
                                case 0:
                                    Fault_Led_ON;
                                    break;

                                case 1:
                                    Fault_Led_OFF;
                                    break;
                            }

                            Fault_cnt = (Fault_cnt + 1) % 2;
                        }

                        break;

                    case Fault_SlowFlash:
                        if(cnt % 3 == 0)
                        {
                            switch(Fault_cnt)
                            {
                                case 0:
                                    Fault_Led_ON;
                                    break;

                                case 1:
                                    Fault_Led_OFF;
                                    break;
                            }

                            Fault_cnt = (Fault_cnt + 1) % 2;
                        }

                        break;

                    case Fault_OftenOff:
                        if(cnt % 1 == 0)
                        {
                            switch(Fault_cnt)
                            {
                                case 0:
                                    Fault_Led_ON;
                                    break;

                                case 1:
                                    Fault_Led_OFF;
                                    break;
                            }

                            Fault_cnt = (Fault_cnt + 1) % 10;
                        }

                        break;

                    case Fault_OftenOn:
                        Fault_Led_OFF;
                        break;
                }
            }
            break;

        /*升级*/
        case Sys_updata:
            if(cnt % 1 == 0)
            {
                switch(Sys_updata_cnt)
                {
                    case 0:
                        LED0_ON;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 1:
                        LED0_OFF;
                        LED1_ON;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 2:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_ON;
                        LED3_OFF;
                        break;

                    case 3:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_ON;
                        break;
                }

                Sys_updata_cnt = (Sys_updata_cnt + 1) % 4;
            }

            break;
    }
}
//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
    vu16 count = 0;
    volatile calendar_obj calendar;

    //溢出中断
    if(TIM_GetITStatus(TIM3, TIM_IT_Update) == SET)
    {
        //清除中断标志位
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        led_Status_polling();
        cnt++;

        if(cnt >= 0xFFFFFFFF)
        {
            cnt = 0;
        }

        RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);

        if(ResetSwitch == true)
        {
            if((calendar.hour == ResetHours) && (calendar.min == ResetMinutes) && (calendar.sec == ResetSeconds))
            {
                count = 0;

                while(Get_FileStatus() != 0)
                {
                    File_Processing();
                    count++;

                    if(count >= 50)
                    {
                        break;
                    }
                }

                //关闭总中断
                __set_FAULTMASK(1);
                NVIC_SystemReset();
            }
        }
    }
}
