#ifndef __LED_H
#define __LED_H
#include "stm32f4xx.h"
//位带操作,实现51类似的GPIO控制功能
//具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).M4同M3类似,只是寄存器地址变了.
//IO口操作宏定义
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2))
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr))
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum))
//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
#define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
#define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014     

#define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
#define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
#define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 

//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入

#define digitalHi(p,i)          {p->BSRRL=i;}         //输出为高电平
#define digitalLo(p,i)          {p->BSRRH=i;}         //输出低电平
#define digitalToggle(p,i)      {p->ODR ^=i;}         //输出反转状态

#define LED0 PAout(0)
#define LED1 PAout(4)
#define LED2 PAout(5)
#define LED3 PAout(6)
#define LED4 PAout(8)
// R-红色
#define LED0_GPIO_PORT          GPIOA
#define LED0_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define LED0_GPIO_PIN           GPIO_Pin_0
// R-红色
#define LED1_GPIO_PORT          GPIOA
#define LED1_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define LED1_GPIO_PIN           GPIO_Pin_4
// B-蓝色
#define LED2_GPIO_PORT          GPIOA
#define LED2_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define LED2_GPIO_PIN           GPIO_Pin_6

// G-绿色
#define LED3_GPIO_PORT          GPIOA
#define LED3_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define LED3_GPIO_PIN           GPIO_Pin_5

// B-蓝色
#define LED4_GPIO_PORT          GPIOA
#define LED4_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define LED4_GPIO_PIN           GPIO_Pin_8

//红
#define LED0_TOGGLE             digitalToggle(LED0_GPIO_PORT,LED0_GPIO_PIN)
#define LED0_OFF                digitalHi(LED0_GPIO_PORT,LED0_GPIO_PIN)
#define LED0_ON                 digitalLo(LED0_GPIO_PORT,LED0_GPIO_PIN)

//红
#define LED1_TOGGLE             digitalToggle(LED1_GPIO_PORT,LED1_GPIO_PIN)
#define LED1_OFF                digitalHi(LED1_GPIO_PORT,LED1_GPIO_PIN)
#define LED1_ON                 digitalLo(LED1_GPIO_PORT,LED1_GPIO_PIN)
//蓝
#define LED2_TOGGLE             digitalToggle(LED2_GPIO_PORT,LED2_GPIO_PIN)
#define LED2_OFF                digitalHi(LED2_GPIO_PORT,LED2_GPIO_PIN)
#define LED2_ON                 digitalLo(LED2_GPIO_PORT,LED2_GPIO_PIN)
//绿
#define LED3_TOGGLE             digitalToggle(LED3_GPIO_PORT,LED3_GPIO_PIN)
#define LED3_OFF                digitalHi(LED3_GPIO_PORT,LED3_GPIO_PIN)
#define LED3_ON                 digitalLo(LED3_GPIO_PORT,LED3_GPIO_PIN)
//绿
#define LED4_TOGGLE             digitalToggle(LED4_GPIO_PORT,LED4_GPIO_PIN)
#define LED4_OFF                digitalHi(LED4_GPIO_PORT,LED4_GPIO_PIN)
#define LED4_ON                 digitalLo(LED4_GPIO_PORT,LED4_GPIO_PIN)


/*********************************************
        |状态|平台|微逆|故障|
        |D101|D103|D105|D104|
        |RUN |2.4G|Init|GPRS|
        |PA0 |PA4 |PA6 |PA5 |
        |LED0|LED1|LED2|LED3|
**********************************************/
//工作状态
#define Mode_Led_ON             LED0_ON
#define Mode_Led_OFF            LED0_OFF
//网络状态
#define Net_Led_ON              LED1_ON
#define Net_Led_OFF             LED1_OFF
//微逆状态
#define Mi_Led_ON               LED2_ON
#define Mi_Led_OFF              LED2_OFF
//错误状态
#define Fault_Led_ON            LED3_ON
#define Fault_Led_OFF           LED3_OFF

//系统状态
enum Sys_state
{
    Sys_open    = 0,
    Sys_run     = 1,
    Sys_updata  = 2,
};
//网络状态
enum Net_state
{
    Net_Flashing        = 0,//快闪
    Net_SlowFlash       = 1,//慢闪
    Net_OftenOn         = 2,//常亮
    Net_OftenOff        = 3,//常灭
};
//微逆状态
enum Mi_state
{
    Mi_Flashing        = 0,//快闪
    Mi_SlowFlash       = 1,//慢闪
    Mi_OftenOff        = 3,//常灭
    Mi_OftenOn         = 2,//常亮
};
//工作状态
enum Mode_state
{
    Mode_Flashing        = 0,//快闪
    Mode_SlowFlash       = 1,//慢闪
    Mode_OftenOff        = 2,//常灭
    Mode_OftenOn         = 3,//常亮
};
//错误状态
enum Fault_state
{
    Fault_Flashing        = 0,//快闪
    Fault_SlowFlash       = 1,//慢闪
    Fault_OftenOff        = 2,//常灭
    Fault_OftenOn         = 3,//常亮
};
void LED_Init(void);//初始化
#endif
