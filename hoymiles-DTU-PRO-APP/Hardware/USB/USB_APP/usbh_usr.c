#include "usbh_usr.h"
#include "led.h"
#include "ff.h"
#include "usart.h"

static u8 AppState;
extern USB_OTG_CORE_HANDLE  USB_OTG_Core;

uint8_t USB_Update_Link = 0;
uint32_t BootTime = 0;
uint32_t VID = 0;
uint32_t PID = 0;

//USB OTG 中断服务函数
//处理所有USB中断
void OTG_FS_IRQHandler(void)
{
    USBH_OTG_ISR_Handler(&USB_OTG_Core);
}
//USB HOST 用户回调函数.
USBH_Usr_cb_TypeDef USR_Callbacks =
{
    USBH_USR_Init,
    USBH_USR_DeInit,
    USBH_USR_DeviceAttached,
    USBH_USR_ResetDevice,
    USBH_USR_DeviceDisconnected,
    USBH_USR_OverCurrentDetected,
    USBH_USR_DeviceSpeedDetected,
    USBH_USR_Device_DescAvailable,
    USBH_USR_DeviceAddressAssigned,
    USBH_USR_Configuration_DescAvailable,
    USBH_USR_Manufacturer_String,
    USBH_USR_Product_String,
    USBH_USR_SerialNum_String,
    USBH_USR_EnumerationDone,
    USBH_USR_UserInput,
    USBH_USR_MSC_Application,
    USBH_USR_DeviceNotSupported,
    USBH_USR_UnrecoveredError
};

enum USH_Cmd
{
    USH_USR_FS_INIT         = 0,
    USH_USR_FS_USER         = 1,
    USH_USR_FS_WAIT         = 2,

};
/*---------------  Messages ---------------*/
const uint8_t MSG_HOST_INIT[]        = "> Host Library Initialized\r\n";
const uint8_t MSG_DEV_ATTACHED[]     = "> Device Attached \r\n";
const uint8_t MSG_DEV_DISCONNECTED[] = "> Device Disconnected\r\n";
const uint8_t MSG_DEV_ENUMERATED[]   = "> Enumeration completed \r\n";
const uint8_t MSG_DEV_HIGHSPEED[]    = "> High speed device detected\r\n";
const uint8_t MSG_DEV_FULLSPEED[]    = "> Full speed device detected\r\n";
const uint8_t MSG_DEV_LOWSPEED[]     = "> Low speed device detected\r\n";
const uint8_t MSG_DEV_ERROR[]        = "> Device fault \r\n";

const uint8_t MSG_MSC_CLASS[]        = "> Mass storage device connected\r\n";
const uint8_t MSG_HID_CLASS[]        = "> HID device connected\r\n";
const uint8_t MSG_DISK_SIZE[]        = "> Size of the disk in MBytes: \r\n";
const uint8_t MSG_LUN[]              = "> LUN Available in the device:\r\n";
const uint8_t MSG_ROOT_CONT[]        = "> Exploring disk flash ...\r\n";
const uint8_t MSG_WR_PROTECT[]       = "> The disk is write protected\r\n";
const uint8_t MSG_UNREC_ERROR[]      = "> UNRECOVERED ERROR STATE\r\n";


//USB HOST 初始化
void USBH_USR_Init(void)
{
    static uint8_t startup = 0;

    if(startup == 0)
    {
        startup = 1;
#ifndef USB_NO_DEBUG
#ifdef USE_USB_OTG_HS
        USB_DEBUG("> USB OTG HS MSC Host\r\n");
#else
        USB_DEBUG("> USB OTG FS MSC Host\r\n");
#endif
        USB_DEBUG("> USB Host library started.\r\n");
        USB_DEBUG(" USB Host Library v2.1.0\r\n");
#endif
    }
}
//检测到U盘插入
void USBH_USR_DeviceAttached(void)//U盘插入
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("%s", (char *)MSG_DEV_ATTACHED);
#endif
}
//检测到U盘拔出
void USBH_USR_UnrecoveredError(void)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("%s", (char *)MSG_UNREC_ERROR);
#endif
}
void USBH_USR_DeviceDisconnected(void) //U盘移除
{
    VID = 0;
    PID = 0;
#ifndef USB_NO_DEBUG
    USB_DEBUG("%s", (char *)MSG_DEV_DISCONNECTED);
#endif
}
//复位从机
void USBH_USR_ResetDevice(void)
{
    /* callback for USB-Reset */
#ifndef USB_NO_DEBUG
    USB_DEBUG("> USBH_USR_ResetDevice \r\n");
#endif
}
//检测到从机速度
//DeviceSpeed:从机速度(0,1,2 / 其他)
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
    if(DeviceSpeed == HPRT0_PRTSPD_HIGH_SPEED)
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_DEV_HIGHSPEED);
#endif
    }
    else if(DeviceSpeed == HPRT0_PRTSPD_FULL_SPEED)
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_DEV_FULLSPEED);
#endif
    }
    else if(DeviceSpeed == HPRT0_PRTSPD_LOW_SPEED)
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_DEV_LOWSPEED);
#endif
    }
    else
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_DEV_ERROR);
#endif
    }
}
//检测到从机的描述符
//DeviceDesc:设备描述符指针
void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{
    USBH_DevDesc_TypeDef *hs;
    hs = DeviceDesc;
    VID = (uint32_t)(*hs).idVendor;
    PID = (uint32_t)(*hs).idProduct;
#ifndef USB_NO_DEBUG
    USB_DEBUG("> VID : %04Xh\r\n", (uint32_t)(*hs).idVendor);
    USB_DEBUG("> PID : %04Xh\r\n", (uint32_t)(*hs).idProduct);
#endif
}
//从机地址分配成功
void USBH_USR_DeviceAddressAssigned(void)
{
    //    printf("从机地址分配成功!\r\n");
}
//配置描述符获有效
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef *cfgDesc,
        USBH_InterfaceDesc_TypeDef *itfDesc,
        USBH_EpDesc_TypeDef *epDesc)
{
    USBH_InterfaceDesc_TypeDef *id;
    id = itfDesc;

    if((*id).bInterfaceClass == 0x08)
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_MSC_CLASS);
#endif
    }
    else if((*id).bInterfaceClass == 0x03)
    {
#ifndef USB_NO_DEBUG
        USB_DEBUG("%s", (char *)MSG_HID_CLASS);
#endif
    }
}
//获取到设备Manufacturer String
void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("> Manufacturer : %sr\r\n", (char *)ManufacturerString);
#endif
}
//获取到设备Product String
void USBH_USR_Product_String(void *ProductString)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("> Product : %s\r\n", (char *)ProductString);
#endif
}
//获取到设备SerialNum String
void USBH_USR_SerialNum_String(void *SerialNumString)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("> Serial Number : %s\r\n", (char *)SerialNumString);
#endif
}
//设备USB枚举完成
void USBH_USR_EnumerationDone(void)
{
    /* Enumeration complete */
#ifndef USB_NO_DEBUG
    USB_DEBUG("%s", (char *)MSG_DEV_ENUMERATED);
#endif
}
//无法识别的USB设备
void USBH_USR_DeviceNotSupported(void)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("> Device not supported.\r\n");
#endif
}
//等待用户输入按键,执行下一步操作
USBH_USR_Status USBH_USR_UserInput(void)
{
#if 1
    /* HOST_ENUMERATION 和 HOST_CLASS_REQUEST
        在枚举成功和类请求之间等待用户输入。
        此处直接返回OK，无需等待。
    */
    return USBH_USR_RESP_OK;
#else
    USBH_USR_Status usbh_usr_status;
    usbh_usr_status = USBH_USR_NO_RESP;
#if 0

    /*Key B3 is in polling mode to detect user action */
    if(STM_EVAL_PBGetState(Button_KEY) == RESET)
    {
        usbh_usr_status = USBH_USR_RESP_OK;
    }

#endif
    return usbh_usr_status;
#endif
}
//USB接口电流过载
void USBH_USR_OverCurrentDetected(void)
{
#ifndef USB_NO_DEBUG
    USB_DEBUG("> Overcurrent detected.\r\n");
#endif
}


//USB HOST MSC类用户应用程序
int USBH_USR_MSC_Application(void)
{
    u8 res = 0;

    switch(AppState)
    {
        case USH_USR_FS_INIT://初始化文件系统
            //printf("开始执行用户程序!!!\r\n");
            AppState = USH_USR_FS_USER;
            break;

        case USH_USR_FS_USER:   //执行USB OTG 测试主程序
            //res = USH_User_App(); //用户主程序
            res = 0;

            if(res)
            {
                AppState = USH_USR_FS_INIT;
            }

            break;

        default:
            break;
    }

    return res;
}
//用户要求重新初始化设备
void USBH_USR_DeInit(void)
{
    AppState = USH_USR_FS_INIT;
}


//用户定义函数,实现fatfs diskio的接口函数
extern USBH_HOST              USB_Host;

//获取U盘状态
//返回值:0,U盘未就绪
//      1,就绪
u8 USBH_UDISK_Status(void)
{
    return HCD_IsDeviceConnected(&USB_OTG_Core);//返回U盘状态
}

//读U盘
//buf:读数据缓存区
//sector:扇区地址
//cnt:扇区个数
//返回值:错误状态;0,正常;其他,错误代码;
u8 USBH_UDISK_Read(u8 *buf, u32 sector, u32 cnt)
{
    u8 res = 1;

    if(HCD_IsDeviceConnected(&USB_OTG_Core))
    {
        do
        {
            res = USBH_MSC_Read10(&USB_OTG_Core, buf, sector, 512 * cnt);
            USBH_MSC_HandleBOTXfer(&USB_OTG_Core, &USB_Host);

            if(!HCD_IsDeviceConnected(&USB_OTG_Core))
            {
                res = 1; //读写错误
                break;
            }
        }
        while(res == USBH_MSC_BUSY);
    }
    else
    {
        res = 1;
    }

    if(res == USBH_MSC_OK)
    {
        res = 0;
    }

    res = 0;
    return res;
}

//写U盘
//buf:写数据缓存区
//sector:扇区地址
//cnt:扇区个数
//返回值:错误状态;0,正常;其他,错误代码;
u8 USBH_UDISK_Write(u8 *buf, u32 sector, u32 cnt)
{
    u8 res = 1;

    if(HCD_IsDeviceConnected(&USB_OTG_Core))
    {
        do
        {
            res = USBH_MSC_Write10(&USB_OTG_Core, buf, sector, 512 * cnt);
            USBH_MSC_HandleBOTXfer(&USB_OTG_Core, &USB_Host);

            if(!HCD_IsDeviceConnected(&USB_OTG_Core))
            {
                res = 1; //读写错误
                break;
            }
        }
        while(res == USBH_MSC_BUSY);
    }
    else
    {
        res = 1;
    }

    if(res == USBH_MSC_OK)
    {
        res = 0;
    }

    return res;
}
