#include "key.h"
#include "led.h"
#include "stdio.h"
#include "SysTick.h"
#include "Memory.h"
#include "HW_Version.h"

#define ResetPassTimeMin                5000
#define ResetPassTimeMax                10000
vu8 ResetMark = 0;
vu32 ResetTime = 0;
extern volatile DtuMajor Dtu3Major;
extern volatile DtuDetail Dtu3Detail;
extern volatile SystemCfgNew system_cfg;
void EXTIX_Init(void)
{
    NVIC_InitTypeDef   NVIC_InitStructure;
    EXTI_InitTypeDef   EXTI_InitStructure;
    //使能SYSCFG时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    //PD4 连接到中断线4
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource4);
    /* 配置EXTI_Line0 */
    EXTI_InitStructure.EXTI_Line = EXTI_Line4;
    //中断事件
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    //EXTI_Trigger_Rising; //上升沿触发//EXTI_Trigger_Falling; //下降沿触发
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    //使能LINE0
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    //配置
    EXTI_Init(&EXTI_InitStructure);
    //外部中断4
    NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
    //抢占优先级0
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    //子优先级2
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;
    //使能外部中断通道
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    //配置
    NVIC_Init(&NVIC_InitStructure);
}
//按键初始化函数
void KEY_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    //使能GPIOD时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    //KEY对应引脚
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    //普通输入模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    //100M
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    //上拉
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    //初始化GPIOE4
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    EXTIX_Init();
}

//外部中断4服务程序
void EXTI4_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line4) != RESET)
    {
        //清除LINE4上的中断标志位
        EXTI_ClearITPendingBit(EXTI_Line4);
        ResetMark = 1;
        ResetTime = LocalTime;
    }
}

void ResetPolling(void)
{
    static vu16 count = 0;

    if(ResetMark == 1)
    {
        if(KEY == 1)
        {
            if(((LocalTime - ResetTime) >= ResetPassTimeMin) && ((LocalTime - ResetTime) <= ResetPassTimeMax))
            {
                Initialization(ResetMark);
                count = 0;

                while(Get_FileStatus() != 0)
                {
                    File_Processing();
                    count++;

                    if(count >= 50)
                    {
                        break;
                    }
                }

                //关闭总中断
                __set_FAULTMASK(1);
                //请求单片机重启
                NVIC_SystemReset();
            }
            else
            {
                ResetMark = 0;
            }
        }
    }
    else
    {
        /*检测服务器域名，端口正确与否，不正确强制更新为正确的服务器，保证网络连接*/
        if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id,(char *)Dtu3Major.Property.ServerDomainName,(uint16_t)strlen((char*)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
            /*hzwang_20200424*/
        {
            Initialization(ResetMark);
        }
    }
}

