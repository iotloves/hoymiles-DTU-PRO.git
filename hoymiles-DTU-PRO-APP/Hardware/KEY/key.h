#ifndef __KEY_H
#define __KEY_H
#include "stm32f4xx.h"

/*下面的方式是通过直接操作库函数方式读取IO*/
#define KEY        GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_4) //PD4

#define KEY_PRES   1

void KEY_Init(void);    //IO初始化
u8 KEY_Scan(void);
void ResetPolling(void);
#endif
