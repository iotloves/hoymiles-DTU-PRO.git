#ifndef __DRM_H
#define __DRM_H
#include "stm32f4xx.h"

#define KEY_SCAN_TIME    200

#define DRM1_5_READ_VALUE   GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_12)
#define DRM2_6_READ_VALUE   GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_13)
#define DRM3_7_READ_VALUE   GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_14)
#define DRM4_8_READ_VALUE   GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_15)

void DRM_Init(void);
uint16_t Get_Adc(void);
uint16_t Get_Adc_Average(uint8_t times);
void process_my_power_key(void);
void DRM_Process(void);
#endif

