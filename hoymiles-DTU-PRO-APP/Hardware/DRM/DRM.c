#include "DRM.h"
#include "SysTick.h"
#include "usart_nrf.h"
#include "AntiReflux.h"

vu8 DRM_Control;
vu8 button_debounce = 0;
vu8 key_num_temp = 0;
vu8 key_num = 0;
vu8 key_num_last = 0;
vu16 ADC_Value = 0;
vu16 ConvertedVoltage = 0;
vu32 key_press_time = 0;
vu32 DRM_time = 0;
/**
* INVERTER_OFF_M            PB1
* DRM1/5                    PD12
* DRM2/6                    PD13
* DRM3/7                    PD14
* DRM4/8                    PD15
**/
void Rheostat_ADC_GPIO_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    // 使能 GPIO 时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    // 配置 IO
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
    //不上拉不下拉
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void Rheostat_ADC_Mode_Config(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    ADC_CommonInitTypeDef ADC_CommonInitStructure;
    // 开启ADC时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    // -------------------ADC Common 结构体 参数 初始化------------------------
    // 独立ADC模式
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    // 时钟为fpclk x分频
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
    // 禁止DMA直接访问模式
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    // 采样时间间隔
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_20Cycles;
    ADC_CommonInit(&ADC_CommonInitStructure);
    // -------------------ADC Init 结构体 参数 初始化--------------------------
    ADC_StructInit(&ADC_InitStructure);
    // ADC 分辨率
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    // 禁止扫描模式，多通道采集才需要
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    // 连续转换
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    //禁止外部边沿触发
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    //数据右对齐
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    //转换通道 1个
    ADC_InitStructure.ADC_NbrOfConversion = 1;
    ADC_Init(ADC1, &ADC_InitStructure);
    //---------------------------------------------------------------------------
    // 使能ADC
    ADC_Cmd(ADC1, ENABLE);
}


void DRM_Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    //使能GPIOF时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    //KEY对应引脚
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    //普通输入模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    //100M
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    //上拉
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    //初始化GPIOD
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    Rheostat_ADC_GPIO_Config();
    Rheostat_ADC_Mode_Config();
}


u16 Get_Adc(void)
{
    //设置指定ADC的规则组通道，一个序列，采样时间
    //ADC1,ADC通道,480个周期,提高采样时间可以提高精确度
    ADC_RegularChannelConfig(ADC1, ADC_Channel_9, 1, ADC_SampleTime_480Cycles);
    //使能指定的ADC1的软件转换启动功能
    ADC_SoftwareStartConv(ADC1);
    //等待转换结束

    while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC));

    //返回最近一次ADC1规则组的转换结果
    return ADC_GetConversionValue(ADC1);
}
//获取通道ch的转换值，取times次,然后平均
//ch:通道编号
//times:获取次数
//返回值:通道ch的times次转换结果平均值
u16 Get_Adc_Average(u8 times)
{
    vu32 temp_val = 0;
    vu8 t;

    for(t = 0; t < times; t++)
    {
        temp_val += Get_Adc();
        Delay_ms(5);
    }

    return (u16)(temp_val / times);
}





///按键处理
void process_my_power_key(void)
{
    //200mS按键去抖
    if(LocalTime > (KEY_SCAN_TIME + key_press_time))   //到时间检测按键
    {
        //        power_key_release = 0;
        ADC_Value = Get_Adc_Average(5);
        ConvertedVoltage = ADC_Value * 3300 / 0xFFF;

        //读取按键AD值
        switch(button_debounce)
        {
            case 0:
                if(DRM1_5_READ_VALUE == 1)
                {
                    key_num_temp = key_num_temp | 0x01;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xfe;
                }

                if(DRM2_6_READ_VALUE == 1)
                {
                    key_num_temp = key_num_temp | 0x02;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xfd;
                }

                if(DRM3_7_READ_VALUE == 1)
                {
                    key_num_temp = key_num_temp | 0x04;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xfb;
                }

                if(DRM4_8_READ_VALUE == 1)
                {
                    key_num_temp = key_num_temp | 0x08;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xf7;
                }

                //ADC  按键板   2996
                if((ConvertedVoltage > 2930) && (ConvertedVoltage < 3100))
                {
                    key_num_temp = key_num_temp | 0x10;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xef;
                }

                //ADC  s311   2829
                if((ConvertedVoltage > 50) && (ConvertedVoltage < 2930))
                {
                    key_num_temp = key_num_temp | 0x20;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xdf;
                }

                //高电平或者低电平  关机
                if((ConvertedVoltage < 50) || (ConvertedVoltage > 3100))
                {
                    key_num_temp = key_num_temp | 0x40;
                }
                else
                {
                    key_num_temp = key_num_temp & 0xbf;
                }

                button_debounce ++;
                break;

            case 1:
                if(DRM1_5_READ_VALUE == 1)
                {
                    key_num = key_num | 0x01;
                }
                else
                {
                    key_num = key_num & 0xfe;
                }

                if(DRM2_6_READ_VALUE == 1)
                {
                    key_num = key_num | 0x02;
                }
                else
                {
                    key_num = key_num & 0xfd;
                }

                if(DRM3_7_READ_VALUE == 1)
                {
                    key_num = key_num | 0x04;
                }
                else
                {
                    key_num = key_num & 0xfb;
                }

                if(DRM4_8_READ_VALUE == 1)
                {
                    key_num = key_num | 0x08;
                }
                else
                {
                    key_num = key_num & 0xf7;
                }

                //ADC  按键板   2996
                if((ConvertedVoltage > 2930) && (ConvertedVoltage < 3200))
                {
                    key_num = key_num | 0x10;
                }
                else
                {
                    key_num = key_num & 0xef;
                }

                //ADC  s311   2829
                if((ConvertedVoltage > 50) && (ConvertedVoltage < 2930))
                {
                    key_num = key_num | 0x20;
                }
                else
                {
                    key_num = key_num & 0xdf;
                }

                //高电平或者低电平  关机
                if(ConvertedVoltage < 50)
                {
                    key_num = key_num | 0x40;
                }
                else
                {
                    key_num = key_num & 0xbf;
                }

                //高电平满功率
                if(ConvertedVoltage > 3200)
                {
                    key_num = key_num | 0x80;
                }
                else
                {
                    key_num = key_num & 0x7f;
                }

                //按键去抖动  200ms  只有0和1相同才认为有按键
                key_num = key_num_temp & key_num;

                //process key
                if(key_num_last != key_num)
                {
                    key_num_last = key_num;
                }

                button_debounce = 0;
                break;
        }

        //判断是否有按键
        key_press_time = LocalTime;
    }
}

void PowerControl(u8 power_percentage)
{
    static vu8 power_percentage_last = 0xFF;
    static vu32 ControlTime = 0;
    vu16 i = 0;
    vu16 temp = 0;
    vu32 p_temp;
    vu32 power_value = 0;
    DRM_Control = power_percentage;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(PORT_NUMBER_CONFIRMATION(i))
        {
            if(ANTI_BACKFLOW_250W(i))
            {
                power_value = MY_MAX_PORT_LIMIT;
            }
            else if(ANTI_BACKFLOW_500W(i))
            {
                power_value = MY_MAX_PORT_LIMIT * 2;
            }
            else if(ANTI_BACKFLOW_1000W(i))
            {
                power_value = MY_MAX_PORT_LIMIT * 4;
            }

            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                temp = (u16)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
            }
            else
            {
                temp = (u16)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
            }

            if(power_percentage != power_percentage_last)
            {
                ControlTime = LocalTime;

                //                if((temp > (power_value + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != power_value * 4))
                //                {
                //发限功率命令
                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                {
                    //每台限功率值
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    {
                        MIMajor[i].Property.Power_Limit = power_percentage * 10;
                    }
                    else
                    {
                        MIMajor[i].Property.Power_Limit = (u16)((power_value * power_percentage) / 100);
                    }

                    //发限功率命令
                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;

                    if(power_percentage == 0)
                    {
                        //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                        //                            {
                        //                                //开机
                        //                                MIMajor[i].Property.Acq_Switch = 1;
                        //                            }
                        //                            else
                        //                            {
                        //关机
                        MIMajor[i].Property.Acq_Switch = 0;
                        //                            }
                    }
                    else
                    {
                        MIMajor[i].Property.Acq_Switch = 1;
                    }
                }

                //                }
            }
        }
    }

    if(LocalTime > (DRM_time + (Dtu3Detail.Property.PortNum * CONTROL_INTERVAL_TIME)))
    {
        DRM_time = LocalTime;
        Relative_Power_Sweep_Control();
    }

    power_percentage_last = power_percentage;
}


void DRM_Process(void)
{
    if((Dtu3Detail.Property.Zero_Export_Switch == 0) &&
            (Dtu3Detail.Property.Phase_Balance_Switch == 0) &&
            (Dtu3Detail.Property.SunSpec_Switch == 0))
    {
        process_my_power_key();
        Dtu3Detail.Property.DRM_Limit_Switch = 1;

        /*DRM5 不发电*/
        if((key_num_last & 0x01) != 0)
        {
            PowerControl(0);
        }
        /*DRM6 50%*/
        else if((key_num_last & 0x02) != 0)
        {
            PowerControl(50);
        }
        /*DRM7 75%*/
        else if((key_num_last & 0x04) != 0)
        {
            PowerControl(75);
        }
        /*DRM8 满功率*/
        else if((key_num_last & 0x08) != 0)
        {
            PowerControl(100);
        }
        /*关机*/
        else if(((key_num_last & 0x10) != 0) || ((key_num_last & 0x40) != 0))
        {
            PowerControl(0);
        }
        else if((key_num_last & 0x80) != 0)
        {
            Dtu3Detail.Property.DRM_Limit_Switch = 0;
            DRM_Control = 0xFF;
        }
        else
        {
            Dtu3Detail.Property.DRM_Limit_Switch = 0;
            DRM_Control = 0xFF;
        }
    }
    else
    {
        Dtu3Detail.Property.DRM_Limit_Switch = 0;
    }
}