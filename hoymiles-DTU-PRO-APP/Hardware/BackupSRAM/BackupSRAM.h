#ifndef __BACKUP_SRAM_H_
#define __BACKUP_SRAM_H_
#include "stm32f4xx.h"


u8 BackupSRAM_Init(void);                                                //备份域SRAM初始化
////u16 BackupSRAM_WriteData(u16 AddrOffset, u8 *pData, u16 DataLen);        //写入数据到备份SRAM中
////u16 BackupSRAM_ReadData(u16 AddrOffset, u8 *pData, u16 DataLen);        //从备份SRAM中读取数据
s8 write_to_backup_sram(u8 *data, u16 bytes, u16 offset);
s8 read_from_backup_sram(u8 *data, u16 bytes, u16 offset);
s8 write_to_backup_rtc(u32 *data, u16 bytes, u16 offset);
s8 read_from_backup_rtc(u32 *data, u16 bytes, u16 offset);
#endif //__BACKUP_SRAM_H_
