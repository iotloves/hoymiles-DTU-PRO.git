#ifndef __RTC_H
#define __RTC_H
#include "stm32f4xx.h"

typedef struct
{
    vu8 hour;
    vu8 min;
    vu8 sec;
    //公历日月年周
    vu16 w_year;//年
    vu8  w_month; //月
    vu8  w_date;  //日
    vu8  week;//星期
} calendar_obj;

u8 RTC_Initialize(calendar_obj calendar);
void RTC_SetTimes(calendar_obj calendar);
void RTC_GetTimes(calendar_obj *calendar);
u32 RTC_Getsecond(void);
void RTC_Setsecond(u32 Second);
void SecToDate(u32 Sec, calendar_obj *calendar);
u32 DateToSec(calendar_obj calendar);
u32 RTC_GetWorldSecond(s32 timezone);
void RTC_GetWorldTime(calendar_obj *calendar, s32 timezone);
void My_RTC_Init(void);
void RTC_Correction(calendar_obj *calendar);
#endif

















