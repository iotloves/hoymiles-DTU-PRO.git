#include "usart.h"
#include "led.h"
#include "string.h"
#include "crc16.h"
#include "SysTick.h"
#include "usart_nrf.h"
#include "Memory.h"
#include "Memory_Tools.h"
#include "spi_flash.h"
#include "iwdg.h"
#include "HW_Version.h"
#include "Memory.h"

extern volatile DtuMajor Dtu3Major;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
extern volatile InverterMajor MIMajor[PORT_LEN];
extern vu8 ETH_IPaddr[4];
extern vu8 BKP_Check;
extern vu8 DRM_Control;
extern vu8 Usart_485_Rec_Data;
extern vu8 Usart_Modbus_Rec_Data;

extern vu32 VID;
extern vu32 PID;
extern vu32 WriteDate;
extern vu32 WriteOffset;
extern vu32 UploadedDate;
extern vu32 UploadedOffset;
extern vu32 Usart_485_Rec_Time;
extern vu32 Usart_Modbus_Rec_Time;
extern vu32 storge_historical_data_flg;
extern volatile SystemCfgNew system_cfg;

vu8 temp_id[12];
vu8 computer_id[4] = {0};
vu8 Gprs_SIMNum[28];
vu8 USART_Wait_Mode = 0;
vu8 USART_RX_BUF[USART_REC_LEN];
//接收缓冲,最大USART_REC_LEN个字节.
vu8 Usart_485_Rx_Buf[Usart_485_Len];
//发送缓冲,最大USART_REC_LEN个字节.
vu8 Usart_485_Tx_Buf[Usart_485_Len];
//接收状态标记
vu16 USART_RX_STA = 0;
vu16 Usart_485_Tx_Rp = 0;
vu16 Usart_485_Tx_Cnt = 0;
vu16 Usart_485_Read_Cnt = 0;
vu16 Usart_Modbus_Rx_Cnt = 0;
void process_usart_data(u8 *receive_buf);

//接收状态
//bit15，    接收完成标志
//bit14，    接收到0x0d
//bit13~0，  接收到的有效字节数目
void USART_Mode_Switch(u8 Mode)
{
    if(Mode == 0)
    {
        Usart_485_Send;
    }
    else
    {
        USART_Wait_Switch();
    }
}

void USART_Wait_Switch(void)
{
    if(USART_Wait_Mode == 1)
    {
        hardware_delay_ms(40);
    }

    Usart_485_Rece;
}


/**
* @brief  配置嵌套向量中断控制器NVIC
* @param  无
* @retval 无
*/
static void NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    /* 嵌套向量中断控制器组选择 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    /* 配置USART为中断源 */
    NVIC_InitStructure.NVIC_IRQChannel = Usart_485_IRQ;
    /* 抢断优先级为1 */
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    /* 子优先级为1 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    /* 使能中断 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    /* 初始化配置NVIC */
    NVIC_Init(&NVIC_InitStructure);
}

/**
* @brief  Usart_485 GPIO 配置,工作模式配置。115200 8-N-1 ，中断接收模式
* @param  无
* @retval 无
*/
void Usart_485_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_AHB1PeriphClockCmd(Usart_485_RX_GPIO_CLK | Usart_485_TX_GPIO_CLK, ENABLE);
    /* 使能 USART 时钟 */
    RCC_APB2PeriphClockCmd(Usart_485_CLK, ENABLE);
    //USART_DeInit(Usart_485);
    //USART_StructInit(&USART_InitStructure);
    //USART_ClockStructInit(&USART_ClockInitStruct);
    /* GPIO初始化 */
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    /* 配置Tx引脚为复用功能  */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = Usart_485_TX_PIN;
    GPIO_Init(Usart_485_TX_GPIO_PORT, &GPIO_InitStructure);
    /* 配置Rx引脚为复用功能 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = Usart_485_RX_PIN;
    GPIO_Init(Usart_485_RX_GPIO_PORT, &GPIO_InitStructure);
    /* 连接 PXx 到 USARTx_Tx*/
    GPIO_PinAFConfig(Usart_485_RX_GPIO_PORT, Usart_485_RX_SOURCE, Usart_485_RX_AF);
    /*  连接 PXx 到 USARTx__Rx*/
    GPIO_PinAFConfig(Usart_485_TX_GPIO_PORT, Usart_485_TX_SOURCE, Usart_485_TX_AF);
    /* 配置串Usart_485 模式 */
    /* 波特率设置：Usart_485_BAUDRATE */
    USART_InitStructure.USART_BaudRate = Usart_485_BAUDRATE;
    /* 字长(数据位+校验位)：8 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    /* 停止位：1个停止位 */
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    /* 校验位选择：不使用校验 */
    USART_InitStructure.USART_Parity = USART_Parity_No;
    /* 硬件流控制：不使用硬件流 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    /* USART模式控制：同时使能接收和发送 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    /* 完成USART初始化配置 */
    USART_Init(Usart_485, &USART_InitStructure);
    /* 嵌套向量中断控制器NVIC配置 */
    NVIC_Configuration();
    /* 使能串口接收中断 */
    /* 使能串口 */
    USART_Cmd(Usart_485, ENABLE);
    USART_ITConfig(Usart_485, USART_IT_TC, ENABLE);
    USART_ITConfig(Usart_485, USART_IT_RXNE, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    USART_Mode_Switch(1);
}

/*****************  发送一个字符 **********************/
void Usart_SendByte(USART_TypeDef *pUSARTx, u8 ch)
{
    USART_Mode_Switch(0);
    /* 发送一个字节数据到USART */
    USART_SendData(pUSARTx, ch);

    /* 等待发送数据寄存器为空 */
    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);

    USART_Mode_Switch(1);
}

void Usart_SendBytes(u8 *ch, u16 len)
{
    vu32 cnt = 0;
    vu16 i = 0;
    USART_Mode_Switch(0);

    for(i = 0; i < len; i++)
    {
        /* 发送一个字节数据到USART */
        USART_SendData(Usart_485, ch[i]);
        cnt = 0;

        /* 等待发送数据寄存器为空 */
        while(USART_GetFlagStatus(Usart_485, USART_FLAG_TXE) == RESET)
        {
            cnt++;

            if(cnt >= 50000)
            {
                return;
            }
        }
    }

    USART_Mode_Switch(1);
}

/*****************  发送字符串 **********************/
void Usart_SendString(char *str)
{
    vu32 cnt = 0;
    vu16 k = 0;

    do
    {
        Usart_SendByte(Usart_485, *(str + k));
        k++;
    }
    while(*(str + k) != '\0');

    cnt = 0;

    /* 等待发送完成 */
    while(USART_GetFlagStatus(Usart_485, USART_FLAG_TC) == RESET)
    {
        cnt++;

        if(cnt >= 50000)
        {
            return;
        }
    }
}

/*****************  发送一个16位数 **********************/
void Usart_SendHalfWord(USART_TypeDef *pUSARTx, u16 ch)
{
    vu32 cnt = 0;
    vu8 temp_h, temp_l;
    USART_Mode_Switch(0);
    /* 取出高八位 */
    temp_h = (ch & 0XFF00) >> 8;
    /* 取出低八位 */
    temp_l = ch & 0XFF;
    /* 发送高八位 */
    USART_SendData(pUSARTx, temp_h);
    cnt = 0;

    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
    {
        cnt++;

        if(cnt >= 50000)
        {
            return;
        }
    }

    /* 发送低八位 */
    USART_SendData(pUSARTx, temp_l);
    cnt = 0;

    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
    {
        cnt++;

        if(cnt >= 50000)
        {
            return;
        }
    }

    USART_Mode_Switch(1);
}

//重定向c库函数printf到串口，重定向后可使用printf函数
int fputc(int ch, FILE *f)
{
    vu32 cnt = 0;
    USART_Mode_Switch(0);
    /* 发送一个字节数据到串口 */
    USART_SendData(Usart_485, (u8)ch);

    /* 等待发送完毕 */
    while(USART_GetFlagStatus(Usart_485, USART_FLAG_TXE) == RESET)
    {
        cnt++;

        if(cnt >= 50000)
        {
            return 0;
        }
    }

    USART_Mode_Switch(1);
    return (ch);
}

//重定向c库函数scanf到串口，重写向后可使用scanf、getchar等函数
int fgetc(FILE *f)
{
    vu32 cnt = 0;

    /* 等待串口输入数据 */
    while(USART_GetFlagStatus(Usart_485, USART_FLAG_RXNE) == RESET)
    {
        cnt++;

        if(cnt >= 50000)
        {
            return 0;
        }
    }

    return (int)USART_ReceiveData(Usart_485);
}
void Usart_Send(u8 len)
{
    Usart_485_Send;
    Usart_485_Tx_Cnt = len;
    memset((u8 *)Usart_485_Rx_Buf, 0, Usart_485_Len);
    Usart_485_Read_Cnt = 0;
    Usart_Modbus_Rx_Cnt = 0;
    /*发送一个字节数据到串口*/
    USART_SendData(Usart_485, Usart_485_Tx_Buf[0]);
}
//串口接收中断接收
void USART1_IRQHandler(void)
{
    vu8 Res;
    vu8 buffer = 0;

    if(USART_GetITStatus(Usart_485, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(Usart_485, USART_IT_RXNE);
        buffer = (u8)USART_ReceiveData(Usart_485);

        if((Usart_485_Read_Cnt + 1) >= Usart_485_Len)
        {
            Usart_485_Read_Cnt = 0;
            memset((u8 *)Usart_485_Rx_Buf, 0, sizeof(Usart_485_Rx_Buf));
        }

        Usart_485_Rx_Buf[Usart_485_Read_Cnt++] = buffer;
        Usart_Modbus_Rx_Cnt = Usart_485_Read_Cnt;
        Usart_485_Rec_Data = 1;
        Usart_Modbus_Rec_Data = 1;
        Usart_485_Rec_Time = LocalTime;
        Usart_Modbus_Rec_Time = LocalTime;
        Res = buffer;

        //接收未完成
        if((USART_RX_STA & 0x8000) == 0)
        {
            if(Res == 0x7E)
            {
                USART_RX_STA = 0;
                memset((u8 *)USART_RX_BUF, 0, sizeof(USART_RX_BUF));
                USART_RX_STA = USART_RX_STA | 0x4000;
            }

            //接收到了0x0d
            if(USART_RX_STA & 0x4000)
            {
                USART_RX_BUF[USART_RX_STA & 0x3FFF] = Res;
                USART_RX_STA++;

                if((USART_RX_STA & 0x3FFF) > (Usart_485_Len - 1))
                {
                    //接收数据错误,重新开始接收
                    USART_RX_STA = 0;
                }

                if(Res == 0x7F)
                {
                    //接收完成了
                    USART_RX_STA = USART_RX_STA | 0x8000;
                    //最后一次不加
                    USART_RX_STA--;
                    process_usart_data((u8 *)USART_RX_BUF);
                    //接收完成了
                    USART_RX_STA = USART_RX_STA & 0x0000;
                    memset((u8 *)Usart_485_Rx_Buf, 0, sizeof(Usart_485_Rx_Buf));
                    Usart_485_Read_Cnt = 0;
                    USART_Wait_Mode = 1;
                }
            }
        }
    }
    else if(USART_GetFlagStatus(Usart_485, USART_FLAG_ORE) != RESET)
    {
        USART_ReceiveData(Usart_485);
    }
    else if(USART_GetFlagStatus(Usart_485, USART_FLAG_TC) != RESET)
    {
        Usart_485_Tx_Rp++;

        if(Usart_485_Tx_Rp < Usart_485_Tx_Cnt)
        {
            USART_SendData(Usart_485, Usart_485_Tx_Buf[Usart_485_Tx_Rp]);
        }
        else
        {
            Usart_485_Tx_Rp = 0;
            Usart_485_Tx_Cnt = 0;
            USART_Mode_Switch(1);
        }

        USART_ClearITPendingBit(Usart_485, USART_IT_TC);
        USART_ClearITPendingBit(Usart_485, USART_FLAG_TC);
    }
}

//发送DTU参数
void send_dtu_para(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    vu8 temp_gprs_ver[4] = {0};
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //System_DtuMajor_Read(&Dtu3Major);
    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    Version_Init();
    //DTU硬件版本号
    temp_dat[10] = Dtu3Major.Property.DtuHw_Ver[0];
    temp_dat[11] = Dtu3Major.Property.DtuHw_Ver[1];
    //DTU软件件版本号
    temp_dat[12] = Dtu3Major.Property.DtuSw_Ver[0];
    temp_dat[13] = Dtu3Major.Property.DtuSw_Ver[1];
    //DTU NRF 硬件版本号
    temp_dat[14] = Dtu3Major.Property.RfHw_Ver[2];
    temp_dat[15] = Dtu3Major.Property.RfHw_Ver[3];
    //DTU NRF 软件版本号
    temp_dat[16] = Dtu3Major.Property.RfFw_Ver[2];
    temp_dat[17] = Dtu3Major.Property.RfFw_Ver[3];
    //WIFI模块固件版本号
    temp_dat[18] = 0x00;
    temp_dat[19] = 0x00;
    temp_dat[20] = 0x00;

    //GPRS软件版本号
    if(Dtu3Major.Property.Gprs_Ver[7] > '0')
    {
        temp_gprs_ver[0] = Dtu3Major.Property.Gprs_Ver[7] - '0';
    }
    else
    {
        temp_gprs_ver[0] = 0;
    }

    if(Dtu3Major.Property.Gprs_Ver[8] > '0')
    {
        temp_gprs_ver[1] = Dtu3Major.Property.Gprs_Ver[8] - '0';
    }
    else
    {
        temp_gprs_ver[1] = 0;
    }

    if(Dtu3Major.Property.Gprs_Ver[9] > '0')
    {
        temp_gprs_ver[2] = Dtu3Major.Property.Gprs_Ver[9] - '0';
    }
    else
    {
        temp_gprs_ver[2] = 0;
    }

    if(Dtu3Major.Property.Gprs_Ver[10] > '0')
    {
        temp_gprs_ver[3] = Dtu3Major.Property.Gprs_Ver[10] - '0';
    }
    else
    {
        temp_gprs_ver[3] = 0;
    }

    temp_dat[21] = (u8)((temp_gprs_ver[0] << 4) | temp_gprs_ver[1]);
    temp_dat[22] = (u8)((temp_gprs_ver[2] << 4) | temp_gprs_ver[3]);
    //WIFI信号值
    temp_dat[23] = 0x00;
    // 连接服务器状态
    temp_dat[24] = Dtu3Major.Property.netmode_select;
    //CSQ值
    temp_dat[25] = Dtu3Major.Property.GPRS_CSQ;
    //CRC
    temp_dat[26] = Get_crc_xor((u8 *)&temp_dat[0], 26);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 27);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复测试微逆id
void send_mi_id(u8 *target_adr, u8 *rout_adr, u8 cmd, u8 *mi_id)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //mi id
    memcpy((u8 *)&temp_dat[10], mi_id, 12);
    //CRC
    temp_dat[22] = Get_crc_xor((u8 *)&temp_dat[0], 22);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 23);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//应答结果
void send_result(u8 *target_adr, u8 *rout_adr, u8 cmd, u8 result)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //结果
    temp_dat[10] = result;
    //CRC
    temp_dat[11] = Get_crc_xor((u8 *)&temp_dat[0], 11);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}
//回复服务器域名和端口
void send_server_dns(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i, j = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //server
    j = (u8)strlen((char *)system_cfg.Property.ServerDomainName);
    temp_dat[10] = j;
    memcpy((u8 *)&temp_dat[11], (char *)system_cfg.Property.ServerDomainName, j);
    temp_dat[11 + j] = (u8)(system_cfg.Property.ServerPort >> 8);
    temp_dat[12 + j] = (u8)system_cfg.Property.ServerPort;
    //CRC
    temp_dat[13 + j] = Get_crc_xor((u8 *)&temp_dat[0], 13 + j);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 14 + j);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复GPRS APN
void send_gprs_apn(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 APN[40] = {0};
    vu8 i, j = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //APN
    memset((u8 *)APN, 0, sizeof(APN));
    memcpy((u8 *)APN, (u8 *)Dtu3Major.Property.APN, sizeof(system_cfg.Property.APN));
    memcpy((u8 *)&APN[sizeof(system_cfg.Property.APN)], (u8 *)Dtu3Major.Property.APN2, sizeof(system_cfg.Property.APN2));
    j = (u8)strlen((char *)APN);

    if(j > sizeof(APN))
    {
        j = sizeof(APN);
    }

    temp_dat[10] = j;
    memcpy((u8 *)&temp_dat[11], (char *)APN, j);
    //CRC
    temp_dat[11 + j] = Get_crc_xor((u8 *)&temp_dat[0], 11 + j);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12 + j);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复RTC时间
void send_rtc_time(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //time stamp;
    calendar_obj calendar;
    RTC_GetWorldTime(&calendar, 8 * 60 * 60);
    temp_dat[10] = (u8)(calendar.w_year % 2000);
    temp_dat[11] = (u8)(calendar.w_month);
    temp_dat[12] = (u8)(calendar.w_date);
    temp_dat[13] = (u8)(calendar.hour);
    temp_dat[14] = (u8)(calendar.min);
    temp_dat[15] = (u8)(calendar.sec);
    temp_dat[16] = BKP_Check;
    temp_dat[17] = Get_crc_xor((u8 *)&temp_dat[0], 17);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 18);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//应答升级命令
void send_update_command(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //CRC
    temp_dat[10] = Get_crc_xor((u8 *)&temp_dat[0], 10);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 11);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//串口回复
void send_serial_command(u8 *target_adr, u8 *rout_adr)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len] = {0};
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0x87;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = 0x02;
    //结果
    //正向替换
    temp_dat[24] = Get_crc_xor((u8 *)&temp_dat[0], 24);
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 25);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//应答升级过程或者结果
void send_update_result(u8 *target_adr, u8 *rout_adr, u8 result)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = 0x0c;
    //result
    temp_dat[10] = result;
    //CRC
    temp_dat[11] = Get_crc_xor((u8 *)&temp_dat[0], 11);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复卡号
void send_sim_card(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i, j = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //卡号
    j = sizeof(Gprs_SIMNum);
    temp_dat[10] = j;
    memcpy((u8 *)&temp_dat[11], (char *)Gprs_SIMNum, j);
    //CRC
    temp_dat[11 + j] = Get_crc_xor((u8 *)&temp_dat[0], 11 + j);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12 + j);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复DTU硬件小版本号
void send_small_version(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //DTU硬件小版本号
    temp_dat[10] = system_cfg.Property.small_version;
    //CRC
    temp_dat[11] = Get_crc_xor((u8 *)&temp_dat[0], 11);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复上网模式
void send_netmode_select(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //上网模式
    temp_dat[10] = system_cfg.Property.netmode_select;
    //CRC
    temp_dat[11] = Get_crc_xor((u8 *)&temp_dat[0], 11);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复USB连接信息
void send_usb_info(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //USB_VID
    temp_dat[10] = (u8)(VID >> 24);
    temp_dat[11] = (u8)(VID >> 16);
    temp_dat[12] = (u8)(VID >> 8);
    temp_dat[13] = (u8)(VID);
    //USB_PID
    temp_dat[14] = (u8)(PID >> 24);
    temp_dat[15] = (u8)(PID >> 16);
    temp_dat[16] = (u8)(PID >> 8);
    temp_dat[17] = (u8)(PID);
    //CRC
    temp_dat[18] = Get_crc_xor((u8 *)&temp_dat[0], 18);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 19);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}

//回复网络连接信息
void send_eth_info(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //网络连接信息
    temp_dat[10] = ETH_IPaddr[0];
    temp_dat[11] = ETH_IPaddr[1];
    temp_dat[12] = ETH_IPaddr[2];
    temp_dat[13] = ETH_IPaddr[3];
    //CRC
    temp_dat[14] = Get_crc_xor((u8 *)&temp_dat[0], 14);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 15);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}
//应答DRM控制信息
void send_drm_control(u8 *target_adr, u8 *rout_adr, u8 cmd)
{
    vu8 i = 0;
    vu8 temp_dat[Usart_485_Len];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Usart_485_Tx_Buf, 0, sizeof(Usart_485_Tx_Buf));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    //头
    Usart_485_Tx_Buf[0] = STX;
    //命令
    temp_dat[0] = 0xb0;
    //目标地址
    memcpy((u8 *)&temp_dat[1], target_adr, 4);
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);
    //子命令
    temp_dat[9] = cmd;
    //DRM控制信息
    temp_dat[10] = DRM_Control;
    //CRC
    temp_dat[11] = Get_crc_xor((u8 *)&temp_dat[0], 11);
    //正向替换
    i = ForwardSubstitution((u8 *)&Usart_485_Tx_Buf[1], (u8 *)&temp_dat[0], 12);
    //尾
    Usart_485_Tx_Buf[(i + 1)] = ETX;
    //发送
    Usart_SendBytes((u8 *)Usart_485_Tx_Buf, (i + 2));
    //等待发送完毕
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
}
/***********************************************
** Function name:
** Descriptions:        ID赋值
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void change_MI_id(u8 *source, u8 *pro_id, u8 *id)
{
    vu8 i;

    for(i = 0; i < 6; i++)
    {
        if(i < 2)
        {
            pro_id[i] = (McuAsciiToHex(source[2 * i]) << 4 & 0xf0) + (McuAsciiToHex(source[2 * i + 1]) & 0x0f);
        }
        else if(i >= 2)
        {
            id[i - 2] = (McuAsciiToHex(source[2 * i]) << 4 & 0xf0) + (McuAsciiToHex(source[2 * i + 1]) & 0x0f);
        }
    }
}
//处理串口接受数据
void process_usart_data(u8 *receive_buf)
{
    vu8 i = 0;
    vu8 rcv_len, data_len, temp;
    vu8 temp_buff[Usart_485_Len] = {0};
    vu8 temp_data[Usart_485_Len] = {0};
    vu8 apn_cfg[40] = {0};
    vu8 apn_dtu[40] = {0};
    vu16 port = 0;
    rcv_len = 0;
    temp = 0;
    temp = (u8)USART_RX_STA;
    IWDG_Feed();

    //判断头尾
    if((receive_buf[0] == STX) && (receive_buf[temp] == ETX))
    {
        //还原
        rcv_len = UsartNrf_Backward_substitution1((u8 *)temp_data, &receive_buf[1], (temp - 1));

        //判断校验
        if(0 == Get_crc_xor((u8 *)temp_data, rcv_len))
        {
            memset((u8 *)Gprs_SIMNum, 0, sizeof(Gprs_SIMNum));
            memcpy((u8 *)Gprs_SIMNum, (u8 *)Dtu3Major.Property.Gprs_SIMNum, sizeof(Gprs_SIMNum));

            if(temp_data[0] == 0x07)
            {
                //上位机ID
                memcpy((u8 *)computer_id, (u8 *)&temp_data[5], 4);
                send_serial_command((u8 *)computer_id, (u8 *)computer_id);
            }

            //大版本号
            if(temp_data[0] == REQ_TEST)
            {
                //上位机ID
                memcpy((u8 *)computer_id, (u8 *)&temp_data[5], 4);

                //DTU参数
                if(0x01 == temp_data[9])
                {
                    send_dtu_para((u8 *)computer_id, (u8 *)computer_id, 0x01);
                }
                else if(0x02 == temp_data[9])
                {
                    strncpy((char *)temp_id, (char *)&temp_data[10], 12);
                    memset((InverterMajor *)MIMajor, 0, sizeof(MIMajor));
                    Dtu3Detail.Property.InverterNum = 1;
                    change_MI_id((u8 *)&temp_id[0], (u8 *)&MIMajor[0].Property.Pre_Id[0], (u8 *)&MIMajor[0].Property.Id[0]);
                    //获取组件总数
                    UsartNrf_GetDtu3PortNum();
                    //排列Id位
                    UsartNrf_InitPVPannelToMIMajor();
                    send_mi_id((u8 *)computer_id, (u8 *)computer_id, 0x02, (u8 *)temp_id);
                    HasTempRegister = true;

                    if(Dtu3Detail.Property.InverterNum > 0)
                    {
                        UsartNrf_Process_LedShow(true, 20);
                    }
                }
                //擦除片外flash
                else if(0x03 == temp_data[9])
                {
                    //喂狗
#ifndef DEBUG
                    IWDG_Feed();
#endif
                    //历史数据
                    DeleteHistoryData();

                    //微逆id
                    //                    InverterMajor_Delete();
                    /*清空sn信息*/
                    for(i = 0; i < PORT_LEN; i++)
                    {
                        memset((u8 *)MIMajor[i].PropertyMsg, 0, sizeof(InverterMajor));
                    }

                    System_MI_SN_Write((InverterMajor *)MIMajor);
                    System_MI_SN_Write((InverterMajor *)MIMajor);
                    /*hzwang_20200424*/
                    //微逆信息
                    InverterDetail_Delete();
                    //发电量
                    InverterReal_Delete();
                    //DTU详细信息
                    //                    System_DtuDetail_Delete();
                    memset((u8 *)Dtu3Detail.PropertyMsg, 0, sizeof(Dtu3Detail));
                    //清除电表信息
                    MeterMajor_Delete();
                    memset((u8 *)Dtu3Major.Property.Gprs_SIMNum, 0, sizeof(Dtu3Major.Property.Gprs_SIMNum));
                    //写入参数
                    //System_DtuMajor_Write(&Dtu3Major);
                    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    send_result((u8 *)computer_id, (u8 *)computer_id, 0x03, 1);
                }
                //校验flash
                else if(0x04 == temp_data[9])
                {
                    //能读出id
                    if(SPI_FLASH_Check(0))
                    {
                        send_result((u8 *)computer_id, (u8 *)computer_id, 0x04, 1);
                    }
                    else
                    {
                        send_result((u8 *)computer_id, (u8 *)computer_id, 0x04, 0);
                    }
                }
                //查询服务器域名
                else if(0x05 == temp_data[9])
                {
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    //System_DtuMajor_Read(&Dtu3Major);
                    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);

                    if(strncmp((char *)Dtu3Major.Property.ServerDomainName, (char *)system_cfg.Property.ServerDomainName, sizeof(system_cfg.Property.ServerDomainName)) || (Dtu3Major.Property.ServerPort != system_cfg.Property.ServerPort)) //不一样
                    {
                        memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
                        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, (u8 *)system_cfg.Property.ServerDomainName, sizeof(system_cfg.Property.ServerDomainName));
                        Dtu3Major.Property.ServerPort = system_cfg.Property.ServerPort;
                        //写入参数
                        //System_DtuMajor_Write(&Dtu3Major);
                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    }

                    send_server_dns((u8 *)computer_id, (u8 *)computer_id, 0x05);
                }
                //设置服务器域名
                else if(0x06 == temp_data[9])
                {
                    data_len = temp_data[10];
                    memset((u8 *)temp_buff, 0, sizeof(temp_buff));
                    memcpy((u8 *)temp_buff, (char *)&temp_data[11], data_len);
                    port = ((u16)temp_data[11 + data_len] << 8) + temp_data[12 + data_len];

                    if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id,(char *)temp_buff, data_len, port) == 0)
                    {
                        if(strncmp((char *)&temp_buff, (char *)system_cfg.Property.ServerDomainName, data_len) ||
                                (port != system_cfg.Property.ServerPort) ||
                                (data_len != strlen((char *)system_cfg.Property.ServerDomainName))) //不一样
                        {
                            memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
                            memcpy((u8 *)Dtu3Major.Property.ServerDomainName, (u8 *)temp_buff, data_len);
                            Dtu3Major.Property.ServerPort = port;
                            //写入参数
                            //System_DtuMajor_Write(&Dtu3Major);
                            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                            System_Cfg_Read((SystemCfgNew *)&system_cfg);
                            memset((u8 *)system_cfg.Property.ServerDomainName, 0, sizeof(system_cfg.Property.ServerDomainName));
                            memcpy((u8 *)system_cfg.Property.ServerDomainName, (u8 *)temp_buff, data_len);
                            system_cfg.Property.ServerPort = port;
                            System_Cfg_Write((SystemCfgNew *)&system_cfg);
                        }
                    }

                    send_server_dns((u8 *)computer_id, (u8 *)computer_id, 0x06);
                }
                //查询GPRS APN
                else if(0x07 == temp_data[9])
                {
                    //读出参数
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    //System_DtuMajor_Read(&Dtu3Major);
                    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    memset((u8 *)apn_cfg, 0, sizeof(apn_cfg));
                    memset((u8 *)apn_dtu, 0, sizeof(apn_dtu));
                    memcpy((u8 *)&apn_cfg[0], (u8 *)system_cfg.Property.APN, 20);
                    memcpy((u8 *)&apn_cfg[20], (u8 *)system_cfg.Property.APN2, 20);
                    memcpy((u8 *)&apn_dtu[0], (u8 *)Dtu3Major.Property.APN, 20);
                    memcpy((u8 *)&apn_dtu[20], (u8 *)Dtu3Major.Property.APN2, 20);

                    if(strncmp((char *)apn_cfg, (char *)apn_dtu, sizeof(apn_cfg))) //不一样
                    {
                        memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
                        memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));
                        memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)system_cfg.Property.APN, sizeof(system_cfg.Property.APN));
                        memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)system_cfg.Property.APN2, sizeof(system_cfg.Property.APN2));
                        //写入参数
                        //System_DtuMajor_Write(&Dtu3Major);
                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    }

                    send_gprs_apn((u8 *)computer_id, (u8 *)computer_id, 0x07);
                }
                //设置GPRS APN
                else if(0x08 == temp_data[9])
                {
                    data_len = temp_data[10];
                    memcpy((u8 *)temp_buff, (char *)&temp_data[11], data_len);
                    //读出参数
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    //System_DtuMajor_Read(&Dtu3Major);
                    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    memset((u8 *)apn_cfg, 0, sizeof(apn_cfg));
                    memset((u8 *)apn_dtu, 0, sizeof(apn_dtu));
                    memcpy((u8 *)&apn_cfg[0], (u8 *)system_cfg.Property.APN, 20);
                    memcpy((u8 *)&apn_cfg[20], (u8 *)system_cfg.Property.APN2, 20);
                    memcpy((u8 *)&apn_dtu[0], (u8 *)Dtu3Major.Property.APN, 20);
                    memcpy((u8 *)&apn_dtu[20], (u8 *)Dtu3Major.Property.APN2, 20);

                    //不一样
                    if((strncmp((char *)&temp_buff, (char *)apn_cfg, data_len)) ||
                            (data_len != strlen((char *)apn_cfg)))
                    {
                        memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
                        memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));

                        if(data_len <= 20)
                        {
                            memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)temp_buff, data_len);
                        }
                        else
                        {
                            memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)temp_buff, 20);
                            memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)&temp_buff[20], (data_len - 20));
                        }

                        //写入参数
                        //System_DtuMajor_Write(&Dtu3Major);
                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                        System_Cfg_Read((SystemCfgNew *)&system_cfg);
                        memset((u8 *)system_cfg.Property.APN, 0, sizeof(system_cfg.Property.APN));
                        memset((u8 *)system_cfg.Property.APN2, 0, sizeof(system_cfg.Property.APN2));
                        memcpy((u8 *)system_cfg.Property.APN, (u8 *)Dtu3Major.Property.APN, sizeof(system_cfg.Property.APN));
                        memcpy((u8 *)system_cfg.Property.APN2, (u8 *)Dtu3Major.Property.APN2, sizeof(system_cfg.Property.APN2));
                        System_Cfg_Write((SystemCfgNew *)&system_cfg);
                    }

                    if(strncmp((char *)apn_cfg, (char *)apn_dtu, sizeof(apn_cfg)) ||
                            (strlen((char *)apn_cfg)) != strlen((char *)apn_dtu)) //不一样
                    {
                        memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
                        memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));
                        memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)system_cfg.Property.APN, sizeof(system_cfg.Property.APN));
                        memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)system_cfg.Property.APN2, sizeof(system_cfg.Property.APN2));
                        //写入参数
                        //System_DtuMajor_Write(&Dtu3Major);
                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    }

                    send_gprs_apn((u8 *)computer_id, (u8 *)computer_id, 0x08);
                }
                //查询RTC时间
                else if(0x09 == temp_data[9])
                {
                    send_rtc_time((u8 *)computer_id, (u8 *)computer_id, 0x09);
                }
                //设置RTC时间
                else if(0x0A == temp_data[9])
                {
                    calendar_obj calendar;
                    calendar.w_year     = 2000 + temp_data[10];
                    calendar.w_month    = temp_data[11];
                    calendar.w_date     = temp_data[12];
                    calendar.hour       = temp_data[13];
                    calendar.min        = temp_data[14];
                    calendar.sec        = temp_data[15];
                    RTC_Setsecond((DateToSec(calendar)) - 8 * 60 * 60);
                    /*设置RTC后设置RTC相关掉电保存寄存器*/
                    storge_historical_data_flg = 0xABCD;
                    BKP_Check = 1;
                    Memory_BKP_Save();
                    /*hzwang_20200424*/
                    send_rtc_time((u8 *)computer_id, (u8 *)computer_id, 0x0A);
                }
                //                //升级命令
                //                else if(0x0B == temp_data[9])
                //                {
                //                    send_update_command(computer_id, computer_id, 0x0B);
                //                    ClearDownloadFile();
                //                    USART1_DMA(1);
                //                    usart1_update = 1;
                //                    downfile_module = DTU_UPDATE;
                //                    upgrade = 1;
                //                }
                //查询卡号
                else if(0x0D == temp_data[9])
                {
                    send_sim_card((u8 *)computer_id, (u8 *)computer_id, 0x0D);
                }
                //设置DTU硬件小版本
                else if(0x0E == temp_data[9])
                {
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    system_cfg.Property.small_version = temp_data[10];
                    System_Cfg_Write((SystemCfgNew *)&system_cfg);
                    Dtu3Major.Property.DtuHw_Ver[1] = system_cfg.Property.small_version;
                    //写入参数
                    //System_DtuMajor_Write(&Dtu3Major);
                    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    send_small_version((u8 *)computer_id, (u8 *)computer_id, 0x0E);
                }
                //查询默认上网模式
                else if(0x0F == temp_data[9])
                {
                    //读出参数
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    //System_DtuMajor_Read(&Dtu3Major);
                    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);

                    if(Dtu3Major.Property.netmode_select != system_cfg.Property.netmode_select) //不一样
                    {
                        Dtu3Major.Property.netmode_select = system_cfg.Property.netmode_select;
                        //写入参数
                        //System_DtuMajor_Write(&Dtu3Major);
                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                    }

                    send_netmode_select((u8 *)computer_id, (u8 *)computer_id, 0x0F);
                }
                //设置默认上网模式
                else if(0x10 == temp_data[9])
                {
                    //读出参数
                    System_Cfg_Read((SystemCfgNew *)&system_cfg);
                    //System_DtuMajor_Read(&Dtu3Major);
                    System_Dtu_Info_Read((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);

                    //不一样
                    if(temp_data[10] != system_cfg.Property.netmode_select)
                    {
                        system_cfg.Property.netmode_select = temp_data[10];

                        if(Network_Mode_Check(1, system_cfg.Property.netmode_select) == 1)
                        {
                            System_Cfg_Write((SystemCfgNew *)&system_cfg);
                            Dtu3Major.Property.netmode_select = system_cfg.Property.netmode_select;
                            //写入参数
                            //System_DtuMajor_Write(&Dtu3Major);
                            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                        }
                    }

                    send_netmode_select((u8 *)computer_id, (u8 *)computer_id, 0x10);
                }
                //查询USB连接信息
                else if(0x11 == temp_data[9])
                {
                    send_usb_info((u8 *)computer_id, (u8 *)computer_id, 0x11);
                }
                //查询网络连接信息
                else if(0x12 == temp_data[9])
                {
                    send_eth_info((u8 *)computer_id, (u8 *)computer_id, 0x12);
                }
                //查询DRM控制信息
                else if(0x13 == temp_data[9])
                {
                    send_drm_control((u8 *)computer_id, (u8 *)computer_id, 0x13);
                }

                memset((u8 *)Dtu3Major.Property.Gprs_SIMNum, 0, sizeof(Gprs_SIMNum));
                memcpy((u8 *)Dtu3Major.Property.Gprs_SIMNum, (u8 *)Gprs_SIMNum, sizeof(Gprs_SIMNum));
            }
        }
    }
}
/******************************************END OF FILE**********************/
