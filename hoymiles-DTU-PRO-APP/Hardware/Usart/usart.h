#ifndef __USART_H
#define __USART_H

#include "stm32f4xx.h"
#include <stdio.h>
//生产测试系统轮询命令
#define REQ_TEST                    0x30
//生产测试系统应答命令
#define ANSWER_TEST                 0xb0
//定义最大接收字节数 200
#define USART_REC_LEN               1500
//使能（1）/禁止（0）串口1接收
#define EN_USART1_RX                1
//定义最大接收字节数 200
#define Usart_485_Len               200
#ifndef DEBUG
#define EN485
#else
#ifdef DEBUG
extern vu8 USART_RX_BUF[];
//接收状态标记
extern vu16 USART_RX_STA;
#endif
#endif

//接收缓冲,最大USART_REC_LEN个字节.
extern vu8 Usart_485_Tx_Buf[];
extern vu16 Usart_485_Tx_Cnt;
extern vu16 Usart_485_Tx_Rp;
//接收缓冲,最大USART_REC_LEN个字节.
extern vu8 Usart_485_Rx_Buf[];

extern u16 Usart_485_Rx_Rp;



//引脚定义
/*******************************************************/
#define Usart_485                             USART1
#define Usart_485_CLK                         RCC_APB2Periph_USART1
#ifdef DEBUG
#define Usart_485_BAUDRATE                    115200//1382400//115200  //串口波特率
#else
#define Usart_485_BAUDRATE                    9600//38400//115200//1382400//115200  //串口波特率
#endif

#define Usart_485_TX_GPIO_PORT                GPIOB
#define Usart_485_TX_GPIO_CLK                 RCC_AHB1Periph_GPIOB
#define Usart_485_TX_PIN                      GPIO_Pin_6
#define Usart_485_TX_AF                       GPIO_AF_USART1
#define Usart_485_TX_SOURCE                   GPIO_PinSource6

#define Usart_485_RX_GPIO_PORT                GPIOB
#define Usart_485_RX_GPIO_CLK                 RCC_AHB1Periph_GPIOB
#define Usart_485_RX_PIN                      GPIO_Pin_7
#define Usart_485_RX_AF                       GPIO_AF_USART1
#define Usart_485_RX_SOURCE                   GPIO_PinSource7

#define Usart_485_IRQHandler                  USART1_IRQHandler
#define Usart_485_IRQ                         USART1_IRQn

//#define Usart_485                             USART1
//#define Usart_485_CLK                         RCC_APB2Periph_USART1
//#define Usart_485_BAUDRATE                    115200//1382400//115200  //串口波特率

//#define Usart_485_TX_GPIO_PORT                GPIOA
//#define Usart_485_TX_GPIO_CLK                 RCC_AHB1Periph_GPIOA
//#define Usart_485_TX_PIN                      GPIO_Pin_8
//#define Usart_485_TX_AF                       GPIO_AF_USART1
//#define Usart_485_TX_SOURCE                   GPIO_PinSource8

//#define Usart_485_RX_GPIO_PORT                GPIOA
//#define Usart_485_RX_GPIO_CLK                 RCC_AHB1Periph_GPIOA
//#define Usart_485_RX_PIN                      GPIO_Pin_9
//#define Usart_485_RX_AF                       GPIO_AF_USART1
//#define Usart_485_RX_SOURCE                   GPIO_PinSource9

//#define Usart_485_IRQHandler                  USART1_IRQHandler
//#define Usart_485_IRQ                         USART1_IRQn
/************************************************************/
#define Usart_485_Send                          GPIO_ResetBits(GPIOB, GPIO_Pin_5)
#define Usart_485_Rece                          GPIO_SetBits(GPIOB, GPIO_Pin_5)
void Usart_485_Config(void);
void Usart_SendBytes(u8 *ch, u16 len);
void Usart_SendString(char *str);

void Usart_SendHalfWord(USART_TypeDef *pUSARTx, u16 ch);

int fputc(int ch, FILE *f);
void send_Read_meter_data(u16 adr, u8 len);
void send_cmp_owner_server(void);
void USART_Wait_Switch(void);
void Usart_Send(u8 len);
#endif


