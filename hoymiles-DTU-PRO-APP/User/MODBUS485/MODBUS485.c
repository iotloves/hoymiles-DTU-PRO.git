#include "MODBUS485.h"
#include "crc16.h"
#include "usart.h"
#include "string.h"
#include "malloc.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#include "AntiReflux.h"

#define MDBUS_CNTRL_ADDR        0xC006  //modbus控制寄存器地址
#define MDBUS_CNTRL_STEP        6       //每个MI寄存器长度
#define MDBUS_READ_ADDR         0x1000
#define MDBUS_READ_STEP         40
#define MDBUS_CONTROL_SCAN      60*1000
#define MDBUS_MI_STATUS         8

extern volatile DtuMajor Dtu3Major;
u8 *mi_control_buf = NULL;
vu8 Usart_Modbus_Rec_Data = 0;
vu32 Usart_Modbus_Rec_Time = 0;
vu32 mdbus_control_time = 0;
extern vu16 Usart_Modbus_Rx_Cnt;
extern vu16 Usart_485_Read_Cnt;
extern vu8 Uart_Rec_mybuf[Usart_485_Len];

vu8 mi_control_mod = 0;
vu8 mi_control_get = 0;
/*0x01 0x02 读取MI状态*/
void Device_Status_Read(u8 CMD, u8 *buf, u16 len)
{
    u8 *modbus_dat;
    vu8 i = 0;
    vu8 error = 0;
    vu8 modbus_data_offset = 0;
    vu8 modbus_control_seat = 0;
    vu8 modbus_id_num = 0;
    vu16 j = 0;
    vu16 modbus_crc = 0;
    vu16 modbus_data_len = 0;
    vu16 modbus_start_adr = 0;
    modbus_dat = mymalloc(Usart_485_Len);
    //    if(modbus_dat == NULL)
    //    {
    //        return;
    //    }
    memset(modbus_dat, 0, Usart_485_Len);
    modbus_start_adr = (u16)((buf[1] << 8) + buf[2]);

    if(CMD == 0x01)
    {
        modbus_data_len = (u16)(((buf[3] << 8) + buf[4]) * 2);
    }
    else if(CMD == 0x02)
    {
        /*数据长度*/
        modbus_data_len = (u16)(((buf[3] << 8) + buf[4]) * 2);
    }

    if(modbus_start_adr < MDBUS_CNTRL_ADDR)
    {
        modbus_start_adr = MDBUS_CNTRL_ADDR;
    }

    i = 0;
    j = 0;
    //第几个ID开始数据
    modbus_control_seat = (u8)((modbus_start_adr - MDBUS_CNTRL_ADDR) / MDBUS_CNTRL_STEP);
    //第几个ID位数据的第几个字节开始
    modbus_data_offset = (modbus_start_adr - MDBUS_CNTRL_ADDR) % MDBUS_CNTRL_STEP;
    //取多少个ID的数据
    modbus_id_num = (u8)(modbus_data_len / MDBUS_CNTRL_STEP + modbus_control_seat);

    if(Dtu3Detail.Property.PortNum != 0)
    {
        if((modbus_id_num + modbus_control_seat) > Dtu3Detail.Property.PortNum)
        {
            modbus_id_num = (u8)(Dtu3Detail.Property.PortNum - modbus_control_seat);
            modbus_data_len = modbus_id_num * MDBUS_CNTRL_STEP;
        }

        if(modbus_data_len > 170)
        {
            modbus_data_len = 170;
        }

        if(modbus_data_len > (Dtu3Detail.Property.PortNum * MDBUS_READ_STEP - modbus_control_seat * MDBUS_READ_STEP - modbus_data_offset))
        {
            modbus_data_len = (Dtu3Detail.Property.PortNum * MDBUS_READ_STEP - modbus_control_seat * MDBUS_READ_STEP - modbus_data_offset);
        }
    }
    else
    {
        error = 1;
    }

    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
    Usart_485_Tx_Buf[(i++)] = Dtu3Detail.Property.RS485Addr;

    if(error != 0)
    {
        Usart_485_Tx_Buf[(i++)] = CMD | 0x80;
        Usart_485_Tx_Buf[(i++)] = error;
    }
    else
    {
        Usart_485_Tx_Buf[(i++)] = CMD;
        Usart_485_Tx_Buf[(i++)] = (u8)modbus_data_len;

        for(j = 0; j < (Dtu3Detail.Property.PortNum * MDBUS_CNTRL_STEP); j++)
        {
            if((j % MDBUS_CNTRL_STEP == 0) && (PORT_NUMBER_CONFIRMATION(j / MDBUS_CNTRL_STEP)))
            {
                if(ANTI_BACKFLOW_1000W(i))
                {
                    if(mi_control_buf[(MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)) + (MDBUS_MI_STATUS - 2)] == 0)
                    {
                        if(MIReal[modbus_control_seat].Data.Run_Status[1] == 0x08)
                        {
                            mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x00;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 1)] = 0x00;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 2)] = 0x00;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 3)] = 0x00;
                        }
                        else
                        {
                            mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x01;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 1)] = 0x01;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 2)] = 0x01;
                            mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 3)] = 0x01;
                        }
                    }
                }
                else if(ANTI_BACKFLOW_500W(i))
                {
                    if(MIReal[modbus_control_seat].Data.Run_Status[1] == 0x08)
                    {
                        mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x00;
                        mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 1)] = 0x00;
                    }
                    else
                    {
                        mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x01;
                        mi_control_buf[MDBUS_MI_STATUS * ((j / MDBUS_CNTRL_STEP) + 1)] = 0x01;
                    }
                }
                else if(ANTI_BACKFLOW_250W(i))
                {
                    if(MIReal[modbus_control_seat].Data.Run_Status[1] == 0x08)
                    {
                        mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x00;
                    }
                    else
                    {
                        mi_control_buf[MDBUS_MI_STATUS * (j / MDBUS_CNTRL_STEP)] = 0x01;
                    }
                }
            }
        }

        for(j = modbus_data_offset; j < (modbus_data_offset + modbus_data_len); j++)
        {
            if(j % MDBUS_CNTRL_STEP == 0)
            {
                Usart_485_Tx_Buf[(i++)] = mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat];
            }
            else if(j % MDBUS_CNTRL_STEP == (MDBUS_CNTRL_STEP - 1))
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
                modbus_control_seat ++;
            }
            else
            {
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
        }
    }

    modbus_crc = 0;
    modbus_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
    Usart_485_Tx_Buf[(i++)] = (modbus_crc >> 8) & 0xFF;
    Usart_485_Tx_Buf[(i++)] = (modbus_crc & 0xFF);
    Usart_Send(i);
    myfree(modbus_dat);
}

/*0x03 读取DTU的数据*/
void Device_Data_Read(u8 CMD, u8 *buf, u16 len)
{
    u8 *modbus_dat = NULL;
    vu8 i = 0;
    vu8 error = 0;
    vu8 modbus_data_offset = 0;
    vu8 modbus_control_seat = 0;
    vu8 modbus_id_num = 0;
    vu16 j = 0;
    vu16 modbus_crc = 0;
    vu16 modbus_data_len = 0;
    vu16 modbus_start_adr = 0;
    modbus_dat = mymalloc(Usart_485_Len);
    //    if(modbus_dat == NULL)
    //    {
    //        return;
    //    }
    memset(modbus_dat, 0, Usart_485_Len);
    modbus_start_adr = (u16)((buf[1] << 8) + buf[2]);
    modbus_data_len = (u16)(((buf[3] << 8) + buf[4]) * 2);

    if(modbus_start_adr < MDBUS_READ_STEP)
    {
        modbus_start_adr = MDBUS_READ_STEP;
    }

    i = 0;
    j = 0;
    //第几个ID开始数据
    modbus_control_seat = (u8)((modbus_start_adr - MDBUS_READ_ADDR) / MDBUS_READ_STEP);
    //第几个ID位数据的第几个字节开始
    modbus_data_offset = (modbus_start_adr - MDBUS_READ_ADDR) % MDBUS_READ_STEP;
    //取多少个ID的数据
    modbus_id_num = (u8)((modbus_data_len) / MDBUS_READ_STEP + modbus_control_seat);

    if(Dtu3Detail.Property.PortNum != 0)
    {
        if((modbus_id_num + modbus_control_seat) > Dtu3Detail.Property.PortNum)
        {
            modbus_id_num = (u8)(Dtu3Detail.Property.PortNum - modbus_control_seat);
            modbus_data_len = modbus_id_num * MDBUS_READ_STEP;
        }

        if(modbus_data_len > 170)
        {
            modbus_data_len = 170;
        }

        if(modbus_data_len > (Dtu3Detail.Property.PortNum * MDBUS_READ_STEP - modbus_control_seat * MDBUS_READ_STEP - modbus_data_offset))
        {
            modbus_data_len = (Dtu3Detail.Property.PortNum * MDBUS_READ_STEP - modbus_control_seat * MDBUS_READ_STEP - modbus_data_offset);
        }
    }
    else
    {
        error = 1;
    }

    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
    Usart_485_Tx_Buf[(i++)] = Dtu3Detail.Property.RS485Addr;

    if(error != 0)
    {
        Usart_485_Tx_Buf[(i++)] = CMD | 0x80;
        Usart_485_Tx_Buf[(i++)] = error;
    }
    else
    {
        Usart_485_Tx_Buf[(i++)] = CMD;
        Usart_485_Tx_Buf[(i++)] = (u8)modbus_data_len;

        for(j = modbus_data_offset; j < (modbus_data_offset + modbus_data_len); j++)
        {
            if(j % MDBUS_READ_STEP == 0)
            {
                //数据类型
                Usart_485_Tx_Buf[(i++)] = 0x0C;
            }
            else if(j % MDBUS_READ_STEP == 1)
            {
                //SN-ID5
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Pre_Id[0];
            }
            else if(j % MDBUS_READ_STEP == 2)
            {
                //SN-ID4
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Pre_Id[1];
            }
            else if(j % MDBUS_READ_STEP == 3)
            {
                //SN-ID3
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Id[0];
            }
            else if(j % MDBUS_READ_STEP == 4)
            {
                //SN-ID2
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Id[1];
            }
            else if(j % MDBUS_READ_STEP == 5)
            {
                //SN-ID1
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Id[2];
            }
            else if(j % MDBUS_READ_STEP == 6)
            {
                //SN-ID0
                Usart_485_Tx_Buf[(i++)] = MIMajor[modbus_control_seat].Property.Id[3];
            }
            else if(j % MDBUS_READ_STEP == 7)
            {
                //微逆 端口号
                switch(MIMajor[modbus_control_seat].Property.Port)
                {
                    case MI_NO:
                        Usart_485_Tx_Buf[(i++)] = 0;
                        break;

                    case MI_250W:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case MI_500W_A:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case MI_500W_B:
                        Usart_485_Tx_Buf[(i++)] = 2;
                        break;

                    case MI_1000W_A:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case MI_1000W_B:
                        Usart_485_Tx_Buf[(i++)] = 2;
                        break;

                    case MI_1000W_C:
                        Usart_485_Tx_Buf[(i++)] = 3;
                        break;

                    case MI_1000W_D:
                        Usart_485_Tx_Buf[(i++)] = 4;
                        break;

                    case Pro_A:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case Pro_B:
                        Usart_485_Tx_Buf[(i++)] = 2;
                        break;

                    case Pro_C:
                        Usart_485_Tx_Buf[(i++)] = 3;
                        break;

                    case Pro_D:
                        Usart_485_Tx_Buf[(i++)] = 4;
                        break;

                    case HM_250W:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case HM_500W_A:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case HM_500W_B:
                        Usart_485_Tx_Buf[(i++)] = 2;
                        break;

                    case HM_1000W_A:
                        Usart_485_Tx_Buf[(i++)] = 1;
                        break;

                    case HM_1000W_B:
                        Usart_485_Tx_Buf[(i++)] = 2;
                        break;

                    case HM_1000W_C:
                        Usart_485_Tx_Buf[(i++)] = 3;
                        break;

                    case HM_1000W_D:
                        Usart_485_Tx_Buf[(i++)] = 4;
                        break;

                    default :
                        Usart_485_Tx_Buf[(i++)] = 0;
                        break;
                }
            }
            else if(j % MDBUS_READ_STEP == 8)
            {
                //PV-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVVol[0];
            }
            else if(j % MDBUS_READ_STEP == 9)
            {
                //PV-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVVol[1];
            }
            else if(j % MDBUS_READ_STEP == 10)
            {
                //PC-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVCur[0];
            }
            else if(j % MDBUS_READ_STEP == 11)
            {
                //PC-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVCur[1];
            }
            else if(j % MDBUS_READ_STEP == 12)
            {
                //GV-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.GridVol[0];
            }
            else if(j % MDBUS_READ_STEP == 13)
            {
                //GV-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.GridVol[1];
            }
            else if(j % MDBUS_READ_STEP == 14)
            {
                //FREQ-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Freque[0];
            }
            else if(j % MDBUS_READ_STEP == 15)
            {
                //FREQ-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Freque[1];
            }
            else if(j % MDBUS_READ_STEP == 16)
            {
                //PVP-H
                if(UsartNrf_GetInvterType((u8 *)MIMajor[modbus_control_seat].Property.Pre_Id) >= Inverter_Pro)
                {
                    Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVPower[0];
                }
                else
                {
                    Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Power[0];
                }
            }
            else if(j % MDBUS_READ_STEP == 17)
            {
                //PVP-H
                if(UsartNrf_GetInvterType((u8 *)MIMajor[modbus_control_seat].Property.Pre_Id) >= Inverter_Pro)
                {
                    Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.PVPower[1];
                }
                else
                {
                    Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Power[1];
                }
            }
            else if(j % MDBUS_READ_STEP == 18)
            {
                //PALL-H
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.DailyEnergy >> 8);
            }
            else if(j % MDBUS_READ_STEP == 19)
            {
                //PALL-L
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.DailyEnergy);
            }
            else if(j % MDBUS_READ_STEP == 20)
            {
                //HistryP-HH
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.HistoryEnergyH >> 8);
            }
            else if(j % MDBUS_READ_STEP == 21)
            {
                //HistryP-HL
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.HistoryEnergyH);
            }
            else if(j % MDBUS_READ_STEP == 22)
            {
                //HistryP-LH
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.HistoryEnergyL >> 8);
            }
            else if(j % MDBUS_READ_STEP == 23)
            {
                //HistryP-LL
                Usart_485_Tx_Buf[(i++)] = (u8)(MIReal[modbus_control_seat].Data.HistoryEnergyL);
            }
            else if(j % MDBUS_READ_STEP == 24)
            {
                //TEMPER-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Temper[0];
            }
            else if(j % MDBUS_READ_STEP == 25)
            {
                //TEMPER-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Temper[1];
            }
            else if(j % MDBUS_READ_STEP == 26)
            {
                //运行状态DS-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Run_Status[0];
            }
            else if(j % MDBUS_READ_STEP == 27)
            {
                //运行状态DS-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Run_Status[1];
            }
            else if(j % MDBUS_READ_STEP == 28)
            {
                //FUTNUB-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Fault_Code[0];
            }
            else if(j % MDBUS_READ_STEP == 29)
            {
                //FUTNUB-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Fault_Code[1];
            }
            else if(j % MDBUS_READ_STEP == 30)
            {
                //NOTICE-H
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Fault_Num[0];
            }
            else if(j % MDBUS_READ_STEP == 31)
            {
                //NOTICE-L
                Usart_485_Tx_Buf[(i++)] = MIReal[modbus_control_seat].Data.Fault_Num[1];
            }
            else if(j % MDBUS_READ_STEP == 32)
            {
                //STATE
                Usart_485_Tx_Buf[(i++)] = ((MIReal[modbus_control_seat].Data.LinkState) & MI_SERVER_CONNECT) > 0 ? 1 : 0;
            }
            else if(j % MDBUS_READ_STEP == 33)
            {
                //固定
                Usart_485_Tx_Buf[(i++)] = 0x07;
            }
            else if(j % MDBUS_READ_STEP == 34)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
            else if(j % MDBUS_READ_STEP == 35)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
            else if(j % MDBUS_READ_STEP == 36)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
            else if(j % MDBUS_READ_STEP == 37)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
            else if(j % MDBUS_READ_STEP == 38)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
            }
            else if(j % MDBUS_READ_STEP == 39)
            {
                //预留
                Usart_485_Tx_Buf[(i++)] = 0x00;
                modbus_control_seat++;
            }
        }
    }

    modbus_crc = 0;
    modbus_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
    Usart_485_Tx_Buf[(i++)] = (modbus_crc >> 8) & 0xFF;
    Usart_485_Tx_Buf[(i++)] = (modbus_crc & 0xFF);
    Usart_Send(i);
    myfree(modbus_dat);
}
/*0x05 0x0F MI控制开/关机*/
void Device_Status_Write(u8 CMD, u8 *buf, u16 len)
{
    u8 *modbus_dat = NULL;
    vu8 i = 0;
    vu8 j = 0;
    vu8 error = 0;
    vu8 modbus_data_offset = 0;
    vu8 modbus_control_seat = 0;
    vu16 modbus_crc = 0;
    vu16 modbus_data_len = 0;
    vu16 modbus_start_adr = 0;
    modbus_dat = mymalloc(Usart_485_Len);
    //    if(modbus_dat == NULL)
    //    {
    //        return;
    //    }
    memset(modbus_dat, 0, Usart_485_Len);
    modbus_start_adr = (u16)(buf[1] << 8) + (u16)buf[2];

    if(CMD == 0x05)
    {
        modbus_data_len = 2;
        memset(modbus_dat, 0, Usart_485_Len);
        memcpy(modbus_dat, &buf[3], modbus_data_len);
    }
    else if(CMD == 0X0F)
    {
        /*数据长度*/
        modbus_data_len = buf[5];
        memset(modbus_dat, 0, Usart_485_Len);
        memcpy(modbus_dat, &buf[6], modbus_data_len);
    }

    if((modbus_start_adr == 0XC000) || (modbus_start_adr == 0X9D9C))
    {
        modbus_data_len = 2;

        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
        {
            if(PORT_NUMBER_CONFIRMATION(i))
            {
                if(ANTI_BACKFLOW_1000W(i))
                {
                    if(buf[3] == 0x00)
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_OFF;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_ON;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }
                else if(ANTI_BACKFLOW_500W(i))
                {
                    if(buf[3] == 0x00)
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_OFF;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_ON;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }
                else if(ANTI_BACKFLOW_250W(i))
                {
                    if(buf[3] == 0x00)
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_OFF;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_ON;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }

                mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 2)] = 1;
            }
        }
    }
    else
    {
        if(modbus_start_adr < MDBUS_CNTRL_ADDR)
        {
            modbus_start_adr = MDBUS_CNTRL_ADDR;
        }

        modbus_data_offset = (modbus_start_adr - MDBUS_CNTRL_ADDR) % MDBUS_CNTRL_STEP; //第几个ID位数据的第几个字节开始
        modbus_control_seat = (u8)(i / MDBUS_CNTRL_STEP) + (u8)((modbus_start_adr - MDBUS_CNTRL_ADDR) / MDBUS_CNTRL_STEP);

        if(modbus_control_seat > 0)
        {
            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(PORT_NUMBER_CONFIRMATION(i))
                {
                    if((((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) +
                            ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) +
                            ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) +
                            ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) +
                            ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) +
                            (uint64_t)(MIMajor[i].Property.Id[3])) ==
                            (((uint64_t)(MIMajor[modbus_control_seat].Property.Pre_Id[0]) << 40) +
                             ((uint64_t)(MIMajor[modbus_control_seat].Property.Pre_Id[1]) << 32) +
                             ((uint64_t)(MIMajor[modbus_control_seat].Property.Id[0]) << 24) +
                             ((uint64_t)(MIMajor[modbus_control_seat].Property.Id[1]) << 16) +
                             ((uint64_t)(MIMajor[modbus_control_seat].Property.Id[2]) << 8) +
                             (uint64_t)(MIMajor[modbus_control_seat].Property.Id[3])))
                    {
                        for(j = modbus_data_offset; j < (modbus_data_len + modbus_data_offset); j++)
                        {
                            if(j % MDBUS_CNTRL_STEP == 0)  //控制开关机的 ID位置
                            {
                                if(*(modbus_dat + j) == 0x00)
                                {
                                    MIReal[i].Data.NetCmd = NET_TURN_OFF;
                                    MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;

                                    if(ANTI_BACKFLOW_1000W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                    else if(ANTI_BACKFLOW_500W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                    else if(ANTI_BACKFLOW_250W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                }
                                else
                                {
                                    MIReal[i].Data.NetCmd = NET_TURN_ON;
                                    MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;

                                    if(ANTI_BACKFLOW_1000W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                    else if(ANTI_BACKFLOW_500W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                        mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * (i + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                    else if(ANTI_BACKFLOW_250W(i))
                                    {
                                        mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                        mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] = 0;
                                    }
                                }

                                break;
                            }
                        }

                        break;
                    }
                }
            }
        }

        /*数据处理*/
        //offset
        for(i = modbus_data_offset; i < (modbus_data_len + modbus_data_offset); i++)
        {
            if(i % MDBUS_CNTRL_STEP == 0)  //控制开关机的 ID位置
            {
                //位于第几个ID
                modbus_control_seat = (u8)(i / MDBUS_CNTRL_STEP) + (u8)((modbus_start_adr - MDBUS_CNTRL_ADDR) / MDBUS_CNTRL_STEP);

                if(ANTI_BACKFLOW_1000W(modbus_control_seat))
                {
                    if(*(modbus_dat + i) == 0x00)
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_OFF;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 1)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 2)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 3)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_ON;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 1)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 2)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 2)) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 3)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 3)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }
                else if(ANTI_BACKFLOW_500W(modbus_control_seat))
                {
                    if(*(modbus_dat + i) == 0x00)
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_OFF;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 1)] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_ON;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                        mi_control_buf[MDBUS_MI_STATUS * (modbus_control_seat + 1)] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * (modbus_control_seat + 1)) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }
                else if(ANTI_BACKFLOW_250W(modbus_control_seat))
                {
                    if(*(modbus_dat + i) == 0x00)
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_OFF;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 0;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                    else
                    {
                        MIReal[modbus_control_seat].Data.NetCmd = NET_TURN_ON;
                        MIReal[modbus_control_seat].Data.NetStatus = NET_NOT_EXECUTED;
                        mi_control_buf[MDBUS_MI_STATUS * modbus_control_seat] = 1;
                        mi_control_buf[(MDBUS_MI_STATUS * modbus_control_seat) + (MDBUS_MI_STATUS - 1)] = 0;
                    }
                }

                mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 2)] = 1;
            }

            if(i > (Dtu3Detail.Property.PortNum * MDBUS_CNTRL_STEP))
            {
                break;
            }
        }
    }

    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
    i = 0;
    Usart_485_Tx_Buf[(i++)] = Dtu3Detail.Property.RS485Addr;

    if(error != 0)
    {
        Usart_485_Tx_Buf[(i++)] = CMD | 0x80;
        Usart_485_Tx_Buf[(i++)] = error;
    }
    else
    {
        mi_control_get = 1;
        Usart_485_Tx_Buf[(i++)] = CMD;
        Usart_485_Tx_Buf[(i++)] = (u8)(modbus_start_adr >> 8);
        Usart_485_Tx_Buf[(i++)] = (u8)modbus_start_adr;

        if(CMD == 0x05)
        {
            //数据
            Usart_485_Tx_Buf[(i++)] = modbus_dat[0];
            Usart_485_Tx_Buf[(i++)] = modbus_dat[1];
        }
        else if(CMD == 0x0f)
        {
            /*寄存器数量*/
            Usart_485_Tx_Buf[(i++)] = (modbus_data_len / 2) / 256;
            Usart_485_Tx_Buf[(i++)] = (u8)((modbus_data_len / 2) % (256));
        }
    }

    modbus_crc = 0;
    modbus_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
    Usart_485_Tx_Buf[(i++)] = (modbus_crc >> 8) & 0xFF;
    Usart_485_Tx_Buf[(i++)] = (modbus_crc & 0xFF);
    Usart_Send(i);
    myfree(modbus_dat);
}
void Modbus_Receive_process(u8 *buf, u16 len)
{
    vu8 Modbus_CMD = 0;
    vu16 modbus_start_adr = 0;
    Modbus_CMD = buf[0];
    modbus_start_adr = (u16)(buf[0] << 8) + (u16)buf[1];

    switch(Modbus_CMD)
    {
        /*0x01 单独读取MI的开/关机状态*/
        case 0x01:

        /*0x02 读取多设备(MI)开关机状态*/
        case 0x02:
            Device_Status_Read(Modbus_CMD, buf, len);
            break;

        /*0x03 0x04 读取DTU的数据*/
        case 0x03:
        case 0x04:
            Device_Data_Read(Modbus_CMD, buf, len);
            break;

        /*0x05 控制MI开/关机*/
        case 0x05:

        /*0x0F 控制多个MI的开/关机*/
        case 0x0F:
            Device_Status_Write(Modbus_CMD, buf, len);
            break;

        default:
            break;
    }
}
//Modbus处理
void Modbus_RTU_Process(void)
{
    vu8 i = 0;
    vu16  buf_len = 0;
    vu16 modbus_crc = 0;
    vu16 modbus_crc_get = 0;
    vu16 control_over = 0;

    if(Dtu3Detail.Property.RS485Mode == 1)
    {
        if(mi_control_mod == 0)
        {
            mi_control_buf = mymalloc(Dtu3Detail.Property.PortNum * MDBUS_MI_STATUS);
            //            if(mi_control_buf == NULL)
            //            {
            //                return;
            //            }
            memset(mi_control_buf, 0, Dtu3Detail.Property.PortNum * MDBUS_MI_STATUS);
            mi_control_mod = 1;
        }

        /*如果接收到新数据*/
        if(Usart_Modbus_Rec_Data == 1)
        {
            /*接收时间超过100ms串口接收超时结束接收*/
            if(LocalTime > (Usart_Modbus_Rec_Time + 100))
            {
                Usart_Modbus_Rec_Time = LocalTime;

                if(Dtu3Detail.Property.RS485Addr == Usart_485_Rx_Buf[0])
                {
                    memcpy((u8 *)&Uart_Rec_mybuf[0], (u8 *)&Usart_485_Rx_Buf[0], sizeof(Uart_Rec_mybuf));
                    /*传递接收数据长度*/
                    buf_len = Usart_Modbus_Rx_Cnt;

                    if(buf_len > 2)
                    {
                        modbus_crc = (u16)(Uart_Rec_mybuf[buf_len - 2] << 8) + (u16)Uart_Rec_mybuf[buf_len - 1];
                        modbus_crc_get = ModRTU_CRC((u8 *)Uart_Rec_mybuf, (buf_len - 2));

                        if(modbus_crc == modbus_crc_get)
                        {
                            Modbus_Receive_process((u8 *)&Uart_Rec_mybuf[1], (buf_len - 3));
                        }
                        else
                        {
                            memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
                            i = 0;
                            Usart_485_Tx_Buf[(i++)] = Dtu3Detail.Property.RS485Addr;
                            Usart_485_Tx_Buf[(i++)] = Uart_Rec_mybuf[1] | 0x80;
                            Usart_485_Tx_Buf[(i++)] = 1;
                            modbus_crc = 0;
                            modbus_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
                            Usart_485_Tx_Buf[(i++)] = (modbus_crc >> 8) & 0xFF;
                            Usart_485_Tx_Buf[(i++)] = (modbus_crc & 0xFF);
                            Usart_Send(i);
                        }
                    }
                }

                Usart_485_Read_Cnt = 0;
                Usart_Modbus_Rx_Cnt = 0;
                Usart_Modbus_Rec_Data = 0;
                memset((u8 *)Usart_485_Rx_Buf, 0, Usart_485_Len);
            }
        }

        if(LocalTime > (mdbus_control_time + MDBUS_CONTROL_SCAN))
        {
            mdbus_control_time = LocalTime;

            if(mi_control_get == 1)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        /*有网络命令执行失败*/
                        if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                        {
                            if(mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] > 3)
                            {
                                if(mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)] == 4)
                                {
                                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                                    {
                                        if((((u16)MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]) > (MY_MAX_PORT_LIMIT / 10))
                                        {
                                            if(ANTI_BACKFLOW_1000W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 1;
                                            }
                                            else if(ANTI_BACKFLOW_500W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                            }
                                            else if(ANTI_BACKFLOW_250W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                            }
                                        }
                                        else
                                        {
                                            if(ANTI_BACKFLOW_1000W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 0;
                                            }
                                            else if(ANTI_BACKFLOW_500W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                            }
                                            else if(ANTI_BACKFLOW_250W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if((((u16)MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]) > (MY_MAX_PORT_LIMIT / 10))
                                        {
                                            if(ANTI_BACKFLOW_1000W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 1;
                                            }
                                            else if(ANTI_BACKFLOW_500W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 1;
                                            }
                                            else if(ANTI_BACKFLOW_250W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 1;
                                            }
                                        }
                                        else
                                        {
                                            if(ANTI_BACKFLOW_1000W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 2)] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 3)] = 0;
                                            }
                                            else if(ANTI_BACKFLOW_500W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                                mi_control_buf[MDBUS_MI_STATUS * (i + 1)] = 0;
                                            }
                                            else if(ANTI_BACKFLOW_250W(i))
                                            {
                                                mi_control_buf[MDBUS_MI_STATUS * i] = 0;
                                            }
                                        }
                                    }

                                    mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)]++;
                                }
                            }
                            else
                            {
                                if(mi_control_buf[MDBUS_MI_STATUS * i] == 1)
                                {
                                    MIReal[i].Data.NetCmd = NET_TURN_ON;
                                }
                                else
                                {
                                    MIReal[i].Data.NetCmd = NET_TURN_OFF;
                                }

                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                mi_control_buf[(MDBUS_MI_STATUS * i) + (MDBUS_MI_STATUS - 1)]++;
                                control_over++;
                            }
                        }
                        /*有网络命令未执行*/
                        else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                        {
                            control_over++;
                        }
                    }
                }
            }

            if(control_over == 0)
            {
                mi_control_get = 0;
            }
        }
    }
    else
    {
        if(mi_control_mod == 1)
        {
            myfree(mi_control_buf);
        }
    }
}