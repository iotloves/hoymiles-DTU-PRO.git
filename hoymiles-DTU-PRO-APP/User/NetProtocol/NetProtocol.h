#ifndef __NETPROTOCOL_H
#define __NETPROTOCOL_H
#ifdef DTU3PRO
#include "stm32f4xx.h"

#else
#include "stm32f10x.h"
#endif
#include "usart_nrf.h"
#include "usart_nrf3.h"
/*| Server：0/App：1 |        产品种类        |req：0/res：1|           命令字        |*/
/*|       [15]       |[14][13][12]_[11][10][9]|     [8]     |[7][6][5][4]_[3][2][1][0]|*/
/*产品种类
*储能：0-15
*DTU ：16
*/
enum TAG_state
{
    TAG_InfoDataReq             = 0x2201,       //信息帧c->s
    TAG_InfoDataRes             = 0x2301,       //信息帧应答s->c

    TAG_HBReq                   = 0x2202,       //心跳帧c->s
    TAG_HBRes                   = 0x2302,       //心跳帧应答s->c

    TAG_RealDataReq             = 0x2203,       //实时数据帧c->s
    TAG_RealDataRes             = 0x2303,       //实时数据帧应答 s->c

    TAG_HistoryDataReq          = 0x2204,       //历史数据帧c->s
    TAG_HistoryDataRes          = 0x2304,       //历史数据帧应答 s->c

    TAG_CommandReq              = 0x2205,       //设备命令   c->s
    TAG_CommandRes              = 0x2305,       //设备命令回复s->c

    TAG_CommandStatusReq        = 0x2206,       //网络命令执行状态 c->s
    TAG_CommandStatusRes        = 0x2306,       //网络命令执行状态回复 s->c

    TAG_DevConfigFetchReq       = 0x2207,       //配置获取回复 c->s
    TAG_DevConfigFetchRes       = 0x2307,       //配置获取请求 s->c

    TAG_DevConfigPutReq         = 0x2208,       //配置设置回复 c->s
    TAG_DevConfigPutRes         = 0x2308,       //配置设置请求 s->c

    TAG_WarnInfoReq             = 0x2209,       //挂起告警信息帧请求 c->s
    TAG_WarnInfoRes             = 0x2309,       //挂起告警信息帧回复 s->c

    TAG_WaveReq                 = 0x220A,       //录波数据帧请求 c->s
    TAG_WaveRes                 = 0x230A,       //录波数据帧回复 s->c

    TAG_WarnReq                 = 0x220B,       //未上传告警信息帧请求 c->s
    TAG_WarnRes                 = 0x230B,       //未上传告警信息帧请求 s->c

    TAG_AppInfoReq              = 0xA201,       //App设备信息请求 s->c
    TAG_AppInfoRes              = 0xA301,       //App设备信息请求回复 c->s

    TAG_AppHBReq                = 0xA202,       //App心跳帧请求 s->c
    TAG_AppHBRes                = 0xA302,       //App心跳帧回复 c->s

    TAG_AppRealDataReq          = 0xA203,       //App实时数据请求 s->c
    TAG_AppRealDataRes          = 0xA303,       //App实时数据请求回复 c->s

    TAG_AppWarnReq              = 0xA204,       //App告警请求 s->c
    TAG_AppWarnRes              = 0xA304,       //App告警回复 c->s

    TAG_AppCommandReq           = 0xA205,       //App控制命令请求 s->c
    TAG_AppCommandRes           = 0xA305,       //App控制命令请求回复 c->s

    TAG_AppCommandStatusReq     = 0xA206,       //网络命令执行状态 s->c
    TAG_AppCommandStatusRes     = 0xA306,       //网络命令执行状态回复 c->s

    TAG_AppDevConfigFetchReq    = 0xA207,       //配置获取回复 c->s
    TAG_AppDevConfigFetchRes    = 0xA307,       //配置获取请求 s->c

    TAG_AppDevConfigPutReq      = 0xA208,       //配置设置回复 c->s
    TAG_AppDevConfigPutRes      = 0xA308,       //配置设置请求 s->c

    TAG_AppGetConfigReq         = 0xA209,       //配置获取回复 c->s
    TAG_AppGetConfigRes         = 0xA309,       //配置获取请求 s->c

    TAG_AppSetConfigReq         = 0xA210,       //配置设置回复 c->s
    TAG_AppSetConfigRes         = 0xA310,       //配置设置请求 s->c
};

enum Network_Command
{
    Cmd_DTURestart              = 0x01,         //重启DTU
    Cmd_DTUUpgrade              = 0x02,         //升级DTU
    Cmd_MIRestart               = 0x03,         //MI重启
    Cmd_AcqVNum                 = 0x04,         //采集版本号
    Cmd_AntiTheftSet            = 0x05,         //防盗设置
    Cmd_MIOpen                  = 0x06,         //MI开机控制
    Cmd_MIShutdown              = 0x07,         //MI关机控制
    Cmd_LimitedPower            = 0x08,         //限功率控制
    Cmd_AntiBackflow            = 0x09,         //防逆流控制
    Cmd_ClearGroundFault        = 0x0A,         //清除接地故障
    Cmd_CTSetting               = 0x0B,         //CT设置
    Cmd_Lock                    = 0x0C,         //加锁
    Cmd_Unlock                  = 0x0D,         //解锁
    Cmd_ConfigGridFile          = 0x0E,         //配置并网文件
    Cmd_MIUpgrade               = 0x0F,         //升级微逆程序
    Cmd_IDNetwork               = 0x10,         //ID组网
    Cmd_AntiBackflowNetwork     = 0x11,         //防逆流组网
    Cmd_AbortControl            = 0x12,         //中止控制命令执行
    Cmd_WiFiSettings            = 0x13,         //wifi设置
    Cmd_ServerSettings          = 0x14,         //设置服务器域名，端口号
    Cmd_GprsAPN                 = 0x15,         //设置GPRS APN
    Cmd_AntiTheftStatus         = 0x16,         //防盗开关
    Cmd_AppMultiCom             = 0x17,         //APP请求多微逆通讯状态
    Cmd_AppSingleCom            = 0x18,         //APP请求单微逆通讯状态
    Cmd_AppDTUUpgrade           = 0x19,         //APP升级DTU
    Cmd_AppMIUpgrade            = 0x20,         //APP升级微逆
    Cmd_AppCollectAllData       = 0x21,         //APP收集全部微逆数据
    Cmd_AppCollectSingleData    = 0x22,         //APP收集单个微逆数据
    Cmd_AppDTUCheck             = 0x23,         //APP升级DTU校验
    Cmd_RS485Config             = 0x24,         //RS485功能设置
    Cmd_ToleranceDiff           = 0x25,         //相容差设置

    Cmd_SunSpec                 = 0x27,         //SunSpec功能
};


#define wifi_ssid_location                      0
#define wifi_ssid_length                        40
#define wifi_passward_location                  45
#define wifi_passward_length                    30
#define ServerDomainName_location               80
#define ServerDomainName_length                 40
#define ServerPort_location                     125
#define ServerPort_length                       2
#define Server_send_time_location               130
#define Server_send_time_length                 1
#define APN_location                            135
#define APN_length                              25
#define netmode_select_location                 160
#define netmode_select_length                   1
#define Data_Size                               800
#define APP_HOLD_TIME                           60000//120000
void Nettest(void);
void Network_Function_Reset(void);
//心跳帧c->s
u16 pb_HBReq(char *buffer);
void pb_HBReq_Analysis(char *buffer, u16 *pb_len);
//心跳帧应答s->c
u16 pb_HBRes(char *buffer);
void pb_HBRes_Analysis(char *buffer, u16 *pb_len);
//信息帧c->s
#ifdef DTU3PRO
u16 pb_InfoDataReq(u8 package_nub, u8 package_now,
                   u32 time, u16 MeterCount,
                   u16 MeterOff, u16 RpCount, u16 RpOff,
                   u16 PvCount, u16 PvOff, char *buffer,
                   InverterDetail *mInverterDetail);
#else
u16 pb_InfoDataReq(u8 package_nub, u8 package_now,
                   u32 time, u16 RpCount, u16 RpOff,
                   u16 PvCount, u16 PvOff, char *buffer,
                   InverterDetail *mInverterDetail);
#endif
void pb_InfoDataReq_Analysis(char *buffer, u16 *pb_len);
//信息帧应答s->c
u16 pb_InfoDataRes(char *buffer, u32 package_now);
void pb_InfoDataRes_Analysis(char *buffer, u16 *pb_len);
//实时数据打包
#ifdef DTU3PRO
u32 RealDataReq_Encode(u8 package_nub, u8 package_now, u32 time,
                       u16 MeterCount, u16 MeterOff,
                       u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer);
#else
u32 RealDataReq_Encode(u8 package_nub, u8 package_now,
                       u32 time, u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer);
#endif
//实时数据解包
u32 RealDataReq_Decode(u8 package_nub, u8 package_now,
                       u16 *RpCount, u16 RpOff,
                       u16 *PvCount, u16 PvOff, char *buffer, u16 *pb_len);
//实时数据帧c->s
u16 pb_RealDataReq(char *buffer, char *pb_buffer, u16 pb_len);
void pb_RealDataReq_Analysis(char *buffer, u16 *pb_len);
//实时数据帧应答 s->c
u16 pb_RealDataRes(char *buffer, u32 package_now);
void pb_RealDataRes_Analysis(char *buffer, u16 *pb_len);
//历史数据帧c->s
u16 pb_HistoryDataReq(char *buffer, char *pb_buffer, u16 pb_len);
void pb_HistoryDataReq_Analysis(char *buffer, u16 *pb_len);
//历史数据帧应答 s->c
u16 pb_HistoryDataRes(char *buffer, u32 package_now);
void pb_HistoryDataRes_Analysis(char *buffer, u16 *pb_len);
//设备命令 s->c
u16 pb_CommandRes(char *buffer, u32 command, u8 package_nub, u8 package_now);
void pb_CommandRes_Analysis(char *buffer, u16 *pb_len);
//设备命令回复 c->s
u16 pb_CommandReq(char *buffer, u32 package_now);
void pb_CommandReq_Analysis(char *buffer, u16 *pb_len);
//网络命令执行状态 c->s
u16 pb_CommandStatusReq(char *buffer, u8 package_nub, u8 package_now);
void pb_CommandStatusReq_Analysis(char *buffer, u16 *pb_len);
//网络命令执行状态回复 s->c
u16 pb_CommandStatusRes(char *buffer, u32 package_now);
void pb_CommandStatusRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_DevConfigFetchReq(char *buffer, u8 *package_nub, u8 package_now);
void pb_DevConfigFetchReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_DevConfigFetchRes(char *buffer);
void pb_DevConfigFetchRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_DevConfigPutReq(char *buffer);
void pb_DevConfigPutReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_DevConfigPutRes(char *buffer, u8 package_nub, u8 package_now);
void pb_DevConfigPutRes_Analysis(char *buffer, u16 *pb_len);

/**********************************************
*                      App                    *
***********************************************/
u16 pb_AppGetConfigReq(char *buffer);
void pb_AppGetConfigReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppGetConfigRes(char *buffer);
void pb_AppGetConfigRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppSetConfigReq(char *buffer);
void pb_AppSetConfigReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppSetConfigRes(char *buffer);
void pb_AppSetConfigRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppInfoRes(char *buffer, u32 package_now);
#ifdef DTU3PRO
u16 pb_AppInfoReq(u8 package_nub, u8 package_now,
                  u32 time, u16 MeterCount,
                  u16 MeterOff, u16 RpCount, u16 RpOff,
                  u16 PvCount, u16 PvOff, char *buffer,
                  InverterDetail *mInverterDetail);
#else
u16 pb_AppInfoReq(u8 package_nub, u8 package_now,
                  u32 time, u16 RpCount, u16 RpOff,
                  u16 PvCount, u16 PvOff, char *buffer,
                  InverterDetail *mInverterDetail);
#endif

u16 pb_AppRealDataRes(char *buffer, u32 package_now);
#ifdef DTU3PRO
u16 pb_AppRealDataReq(u8 package_nub, u8 package_now, u32 time,
                      u16 MeterCount, u16 MeterOff,
                      u16 RpCount, u16 RpOff,
                      u16 PvCount, u16 PvOff, char *buffer);
#else
u16 pb_AppRealDataReq(u8 package_nub, u8 package_now, u32 time,
                      u16 RpCount, u16 RpOff,
                      u16 PvCount, u16 PvOff, char *buffer);
#endif
void pb_AppRealDataReq_Analysis(char *buffer, u16 *pb_len);

void pb_AppEventDataRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppCommandReq(char *buffer, u32 package_now);
void pb_AppCommandReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppCommandRes(char *buffer, u32 command, u8 package_nub, u8 package_now);
void pb_AppCommandRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppCommandStatusReq(char *buffer, u8 package_nub, u8 package_now);
void pb_AppCommandStatusReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppCommandResStatus(char *buffer, u32 package_now);
void pb_AppCommandResStatus_Analysis(char *buffer, u16 *pb_len);

u16 pb_AppHBReq(char *buffer);
void pb_AppHBReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppHBRes(char *buffer);
u16 pb_AppDevConfigFetchReq(char *buffer, u8 *package_nub, u8 package_now);
void pb_AppDevConfigFetchReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppDevConfigFetchRes(char *buffer);
void pb_AppDevConfigFetchRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppDevConfigPutReq(char *buffer, u8 package_now);
void pb_AppDevConfigPutReq_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppDevConfigPutRes(char *buffer);
void pb_AppDevConfigPutRes_Analysis(char *buffer, u16 *pb_len);
u16 pb_AppWarnReq(char *buffer, u8 Device);
//接收处理函数
void pb_receive(char *buffer);
void CommandRes(u32 command, u8 package_nub, u8 package_now);
void Memory_Timeout_Free(void);

u16 pb_WaveReq(char *buffer, u8 Device);
//未上传告警
u16 pb_WarnReq(char *buffer, u8 package_nub, u8 package_now, u32 Alarm_time, AlarmDataType *AlarmDataBuf, u16 Alarm_nub,  u8 Device);
//挂起告警
u16 pb_WarnInfoReq(char *buffer, u8 package_nub, u8 package_now, u32 Alarm_time, AlarmDataType *AlarmDataBuf, u16 Alarm_nub,  u8 Device);
#endif

