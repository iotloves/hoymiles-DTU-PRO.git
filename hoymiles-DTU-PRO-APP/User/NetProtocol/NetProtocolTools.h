#ifndef __NETPROTOCOLTOOLS_H
#define __NETPROTOCOLTOOLS_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#include "stdbool.h"
#include "pb_encode.h"
#include "pb_decode.h"

bool encode_bytes(pb_ostream_t *stream, const pb_field_t *field, void *const *arg);
bool decode_bytes(pb_istream_t *stream, const pb_field_t *field, void **arg);
bool encode_string(pb_ostream_t *stream, const pb_field_t *field, void *const *arg);
bool decode_string(pb_istream_t *stream, const pb_field_t *field, void **arg);
uint8_t NetURLResolution(char *WebAddr, uint8_t *domain, uint8_t *ip, uint16_t *port, uint8_t *url, uint8_t *filename); //��ַ����

#endif
