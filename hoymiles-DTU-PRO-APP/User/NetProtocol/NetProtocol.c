#include "pb_encode.h"
#include "pb_decode.h"
#include "GetConfig.pb.h"
#include "SetConfig.pb.h"
#include "CommandPB.pb.h"
#include "DevConfig.pb.h"
#include "HeartbeatPB.pb.h"
#include "InfomationData.pb.h"
#include "APPInfomationData.pb.h"
#include "RealData.pb.h"
#include "WarnData.pb.h"
#include "rtc.h"
#include "crc16.h"
#include "Memory.h"
#include "malloc.h"
#include "NetProtocol.h"
#include "Memory_Tools.h"
#include "NetProtocolTools.h"
#include "gprs.h"
#include "HW_Version.h"
#include "usart_nrf3.h"

#ifdef DTU3PRO
#include "tcp_echoserver.h"
#include "netconf.h"
#include "AntiReflux.h"
extern volatile MeterMajor Meter[];
extern volatile MeterInfomation MeterInfo[];
//电表数量
extern vu8 MeterLinkNum;
extern vu8 Network_Change;
extern vu32 Network_Change_Wait;
#endif
vu8 GetPackageNow = 0;
vu8 GetPackageNum = 0;
vu8 HttpMode = 0;
vu16 DTUSeq = 0;
vu16 ServerSeq;
//DTU网络命令
vu8 Command = 0;
vu8 Commandback = 0;
vu8 ResCommand = 0;
vu16  ReceivingSign;
vu8 err_code;
vu8 package_Num = 0;
//服务器回复当前包
vu8 PackageNow = 0;
//App请求包
vu8 AppPackageNow = 0;
volatile calendar_obj EventTime;
volatile int64_t tid = 0;
vu32 rule_id = 0;
volatile char operate_sn[14] = {0};
vu8 data_being = 0;
vu8 cfg_data_being = 0;
vu8 status = 0;
vu32 crc = 0;
vu32 data_len = 0;
vu32 cfg_crc = 0;
vu32 cfg_data_len = 0;
vu32 MemoryTime = 0;
volatile int64_t APPOperatSN = 0;

vu32 server_link_time = 0;
vu8 server_link_mode = 0;
vu8 server_signal_strength = 0;
unsigned char *data = NULL;
unsigned char *cfg_data = NULL;
extern vu32 storge_historical_data_flg;
//下载ip
extern vu8 up_pro_destip[4];
//端口号
extern vu16 up_pro_destport;
extern vu8 up_pro_domain[ROW_MAX_LEN];
extern vu8 up_pro_filename[ROW_MAX_LEN];
extern vu8 up_pro_url[ROW_MAX_LEN];
//连接的外网
extern volatile vu8 connect_net;
//连上服务器标志位
extern volatile vu8 connect_server;
//有服务器数据
extern volatile vu8 server_data;
//逆变器主要信息
extern volatile InverterMajor MIMajor[PORT_LEN];
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
//DTU主要信息
extern volatile DtuMajor Dtu3Major;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
extern vu8 IP_ADDR[4];
extern vu8 NETMASK_ADDR[4];
extern vu8 GW_ADDR[4];
extern __IO vu8 USE_DHCP;

//app 标志位
extern vu8 APP_Flg;

extern volatile App_DataPollState AppState;
extern vu16  PortNO_App;
extern vu8 mi_zero_data_timter;
vu32 app_switching_time_real = 0;
vu32 app_switching_time = 0;
vu16  rule_type = 0;
extern vu8 CT_set_flag;
struct Cache
{
    u32 len;
    u32 cfg_len;
};
#define CacheMax 20
struct Cache CommunicationCache[CacheMax];
/***********************************************
** Function name:       Network_Function_Reset
** Descriptions:        组网——>复位功能项
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Network_Function_Reset(void)
{
    UsartNrf_ClearMIReal();
    UsartNrf_ClearInverterMajor();
    Dtu3Detail.Property.PortNum = 0;
    //系统总组件数
    Dtu3Detail.Property.Reflux_SysPanelNum = 0;
    //系统下A相组件数
    Dtu3Detail.Property.Reflux_SysPanelNum_A = 0;
    //系统下B相组件数
    Dtu3Detail.Property.Reflux_SysPanelNum_B = 0;
    //系统下C相组件数
    Dtu3Detail.Property.Reflux_SysPanelNum_C = 0;
#ifdef DTU3PRO
    MeterLinkNum = 0;
    memset((MeterMajor *)Meter, 0, sizeof(MeterMajor)*MeterMaxNum);
    memset((MeterInfomation *)MeterInfo, 0, sizeof(MeterInfomation)*MeterMaxNum);
    MeterMajor_Delete();
    SunSpec_DeInit();
    Dtu3Detail.Property.Zero_Export_Switch = 0;
    //余电上网开关
    Dtu3Detail.Property.SurplusSwitch = 0;
    //余电上网限制功率
    Dtu3Detail.Property.SurplusPowerA = 0;
    //余电上网限制功率
    Dtu3Detail.Property.SurplusPowerB = 0;
    //余电上网限制功率
    Dtu3Detail.Property.SurplusPowerC = 0;
    //整体控制 0：总体控制 1：分相控制
    Dtu3Detail.Property.OverallControl = 0;
    //相间平衡控制
    Dtu3Detail.Property.Phase_Balance_Switch = 0;
    //相间容差值单位为w
    Dtu3Detail.Property.Tolerance_Between_Phases = 0;
    //SunSpec模式
    Dtu3Detail.Property.SunSpec_Switch = 0;
    //SunSpec起始地址
    Dtu3Detail.Property.SunSpec_Addr_Start = 0;
#endif
}

/***********************************************
** Function name:       Net_TimeAnalysis
** Descriptions:        时间解析
** input parameters:    u8 *GetTime, u32 timezone
** output parameters:   无
** Returned value:      无
*************************************************/
void Net_TimeAnalysis(vs32 GetTime, vs32 timezone)
{
    Dtu3Detail.Property.timezone = (u32)timezone;
    RTC_Setsecond((u32)GetTime);
#ifdef DEBUG
    printf("offset %d\n", timezone);
    printf("time %d\n", GetTime);
    calendar_obj calendar;
    RTC_GetWorldTime(&calendar, timezone);
    printf("%04d-%02d-%02d %02d:%02d:%02d\n\n",
           calendar.w_year, calendar.w_month, calendar.w_date, calendar.hour, calendar.min, calendar.sec);
#endif

    if(storge_historical_data_flg != 0xABCD)
    {
        Memory_BKP_Read();
        storge_historical_data_flg = 0xABCD;
        Memory_BKP_Save();
    }
}
/***********************************************
** Function name:       Net_ProtobufEncode
** Descriptions:        Protobuf封包
** input parameters:    u16 TAG, char *buffer,
**                      u16 *len, size_t bufsize,
**                      const pb_field_t fields[],
**                      const void *src_struct
** output parameters:   无
** Returned value:      无
*************************************************/
void Net_ProtobufEncode(u16 TAG, u16 Seq, char *buffer, u16 *len, size_t bufsize, const pb_field_t fields[], const void *src_struct)
{
    char *pb_buffer = NULL;
    vu16  pb_len = 0;
    vu16  crc16 = 0;
    bool pb_status;
    pb_ostream_t pb_ostream;
    pb_buffer = mymalloc(bufsize);
    //    if(pb_buffer == NULL)
    //    {
    //        return;
    //    }
    memset(pb_buffer, 0, bufsize);
    buffer[*len] = 'H';
    *len = *len + 1;
    buffer[*len] = 'M';
    *len = *len + 1;
    buffer[*len] = (char)(TAG / 256);
    *len = *len + 1;
    buffer[*len] = (char)(TAG % 256);
    *len = *len + 1;
    buffer[*len] = (char)(Seq / 256);
    *len = *len + 1;
    buffer[*len] = (char)(Seq % 256);
    *len = *len + 1;
    //发送端//把结构体的数据转换到发送缓存
    pb_ostream = pb_ostream_from_buffer((unsigned char *)pb_buffer, bufsize);
    pb_status = pb_encode(&pb_ostream, fields, src_struct);
    pb_len = (u16)pb_ostream.bytes_written;
    //增加校验PB包
    crc16 = (u16)MyCrc16((unsigned char *)pb_buffer, pb_len);
    buffer[*len] = (char)(crc16 / 256);
    *len = *len + 1;
    buffer[*len] = (char)(crc16 % 256);
    *len = *len + 1;
    buffer[*len] = (char)((pb_len + 10) / 256);
    *len = *len + 1;
    buffer[*len] = (char)((pb_len + 10) % 256);
    *len = *len + 1;
    memcpy((u8 *)&buffer[*len], pb_buffer, pb_len);
    *len = *len + pb_len;
#ifdef DTU3PRO
#ifdef DEBUG

    if(Netmode_Used  != CABLE_MODE)
    {
        vu32 a;
        printf("send: %d\r\n", *len);

        for(a = 0; a < *len; a++)
        {
            printf("%02x ", buffer[a]);
        }

        printf("\r\n");
    }

#endif
#endif
    myfree(pb_buffer);
}

/***********************************************
** Function name:       NetCmdAnalysis
** Descriptions:        cmd解析
** input parameters:    s32 *Cmd, s32 *CmdObject,
**                      void *ControlID, s32 *cmd_len,
**                      void *CmdData
** output parameters:   无
** Returned value:      无
*************************************************/
void NetCmdAnalysis(s32 *Action, s32 *DevKind, s32 sns_count, int64_t *sns,
                    s32 total_a, s32 total_b, s32 total_c, s32 a_count,
                    int64_t *item_a, s32 b_count, int64_t *item_b, s32 c_count,
                    int64_t *item_c,  char *ActionData, u8 package_nub, u8 package_now)
{
    vu16  i = 0, j = 0, k = 0, m = 0;
    volatile int64_t sn;
    vu8 get_sn[12] = {0};
    vu8 APN[40] = {0};
    s32 SymbolA = 1;
    s32 SymbolB = 1;
    s32 SymbolC = 1;
    volatile uint64_t Password = 0;
    volatile uint64_t Password_old = 0;
    volatile uint64_t Password_new = 0;
    vu32 time = 0;
    vu32 LimitedPowerA = 0;
    vu32 LimitedPowerB = 0;
    vu32 LimitedPowerC = 0;
    static vu32 MiNum = 0;
    vu32 CTNum = 0;
#ifdef DEBUG
    printf("Cmd: %d\n", *Action);
    printf("package_nub: %d\n", package_nub);
    printf("package_now: %d\n", package_now);
    printf("ActionData: %s\n", ActionData);
#endif
    Command = (u8) * Action;
    Commandback = (u8) * Action;
    err_code = 0;
    //    printf("Cmd: %d\n", Command);
    //    printf("package_nub: %d\n", package_nub);
    //    printf("package_now: %d\n", package_now);
    //    printf("ActionData: %s\n", ActionData);

    switch(Command)
    {
        /*重启DTU*/
        case Cmd_DTURestart:
            break;

        /*升级DTU*/
        case Cmd_DTUUpgrade:
            memset((u8 *)up_pro_url, 0, sizeof(up_pro_url));
            memset((u8 *)up_pro_filename, 0, sizeof(up_pro_filename));
            memset((u8 *)up_pro_domain, 0, sizeof(up_pro_domain));
            HttpMode = NetURLResolution(ActionData, (u8 *)up_pro_domain, (u8 *)up_pro_destip, (u16 *)&up_pro_destport, (u8 *)up_pro_url, (u8 *)up_pro_filename);
#ifdef DEBUG
            printf("up_pro_domain %s\n", up_pro_domain);
            printf("up_pro_destip %d.%d.%d.%d\n", up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3]);
            printf("up_pro_destport %d\n", up_pro_destport);
            printf("up_pro_url %s\n", up_pro_url);
            printf("up_pro_filename %s\n", up_pro_filename);
#endif
            break;

        /*MI重启*/
        case Cmd_MIRestart:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_RESTART;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_RESTART;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*采集版本号*/
        case Cmd_AcqVNum:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_INVERTER_HW_INFOR;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_INVERTER_HW_INFOR;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*防盗设置*/
        case Cmd_AntiTheftSet:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            for(i = 0; i < strlen(ActionData); i++)
            {
                if(strncmp(&ActionData[i], ",", 1) == 0)
                {
                    Password_old = 0;
                    Password_old = astr2int((char *)&ActionData[0], 4);
                    Password_old = Password_old * 10000 + astr2int((char *)&ActionData[4], 4);
                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if(strncmp(&ActionData[j + i], ",", 1) == 0)
                {
                    Password_new = 0;
                    Password_new = astr2int((char *)&ActionData[i], 4);
                    Password_new = Password_new * 10000 + astr2int((char *)&ActionData[i + 4], 4);
                    i = i + j;
                    i++;
                    break;
                }
            }

            err_code = 0;
            Password = ((uint64_t)Dtu3Detail.Property.LockNewPassword[0] * 1000000) +
                       ((uint64_t)Dtu3Detail.Property.LockNewPassword[1] * 10000) +
                       ((uint64_t)Dtu3Detail.Property.LockNewPassword[2] * 100) +
                       ((uint64_t)Dtu3Detail.Property.LockNewPassword[3]);

            if(Password == Password_old)
            {
                memset((u8 *)Dtu3Detail.Property.LockOldPassword, 0, 4);
                Dtu3Detail.Property.LockOldPassword[0] = (Password_old / 1000000) % 100;
                Dtu3Detail.Property.LockOldPassword[1] = (Password_old / 10000) % 100;
                Dtu3Detail.Property.LockOldPassword[2] = (Password_old / 100) % 100;
                Dtu3Detail.Property.LockOldPassword[3] = Password_old % 100;
                memset((u8 *)Dtu3Detail.Property.LockNewPassword, 0, 4);
                Dtu3Detail.Property.LockNewPassword[0] = (Password_new / 1000000) % 100;
                Dtu3Detail.Property.LockNewPassword[1] = (Password_new / 10000) % 100;
                Dtu3Detail.Property.LockNewPassword[2] = (Password_new / 100) % 100;
                Dtu3Detail.Property.LockNewPassword[3] = Password_new % 100;

                if(Password_new == 0)
                {
                    Dtu3Detail.Property.Anti_Theft_Switch = 0;
                }
                else if(Password_new > 0)
                {
                    Dtu3Detail.Property.Anti_Theft_Switch = 1;
                }
            }
            //强密码1016 5082
            else if(Password_old == 10165082)
            {
                memset((u8 *)Dtu3Detail.Property.LockOldPassword, 0, 4);
                Dtu3Detail.Property.LockOldPassword[0] = Dtu3Detail.Property.LockNewPassword[0];
                Dtu3Detail.Property.LockOldPassword[1] = Dtu3Detail.Property.LockNewPassword[1];
                Dtu3Detail.Property.LockOldPassword[2] = Dtu3Detail.Property.LockNewPassword[2];
                Dtu3Detail.Property.LockOldPassword[3] = Dtu3Detail.Property.LockNewPassword[3];
                memset((u8 *)Dtu3Detail.Property.LockNewPassword, 0, 4);
                Dtu3Detail.Property.LockNewPassword[0] = (Password_new / 1000000) % 100;
                Dtu3Detail.Property.LockNewPassword[1] = (Password_new / 10000) % 100;
                Dtu3Detail.Property.LockNewPassword[2] = (Password_new / 100) % 100;
                Dtu3Detail.Property.LockNewPassword[3] = Password_new % 100;

                if(Password_new == 0)
                {
                    Dtu3Detail.Property.Anti_Theft_Switch = 0;
                }
                else if(Password_new > 0)
                {
                    Dtu3Detail.Property.Anti_Theft_Switch = 1;
                }
            }
            else
            {
                err_code = 1;
                return;
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if(strncmp(&ActionData[j + i], "\r", 1) == 0)
                {
                    memset((u8 *)Dtu3Detail.Property.Lock_Time, 0, 4);
                    time = astr2int((char *)&ActionData[i], (u8)(j - i));
                    Dtu3Detail.Property.Lock_Time[0] = (u8)(time / 0x100);
                    Dtu3Detail.Property.Lock_Time[1] = (u8)(time % 0x100);
                    break;
                }
            }

            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(PORT_NUMBER_CONFIRMATION(i))
                {
                    if(Dtu3Detail.Property.Anti_Theft_Switch == 1)
                    {
                        MIReal[i].Data.NetCmd = NET_SET_PASSWORD;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                    else
                    {
                        MIReal[i].Data.NetCmd = NET_CANCEL_GUARD;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            //System_DtuDetail_Write(&Dtu3Detail);
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
#ifdef DEBUG
            printf("Dtu3Detail.Property.LockNewPassword %02X", Dtu3Detail.Property.LockNewPassword[0]);
            printf("%02X", Dtu3Detail.Property.LockNewPassword[1]);
            printf("%02X", Dtu3Detail.Property.LockNewPassword[2]);
            printf("%02X\n", Dtu3Detail.Property.LockNewPassword[3]);
            printf("Dtu3Detail.Property.Lock_Time %02X", Dtu3Detail.Property.Lock_Time[0]);
            printf("%02X\n", Dtu3Detail.Property.Lock_Time[1]);
#endif
            break;

        /*MI开机控制*/
        case Cmd_MIOpen:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_TURN_ON;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_ON;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*MI关机控制*/
        case Cmd_MIShutdown:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_TURN_OFF;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_TURN_OFF;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;
#ifdef DTU3PRO

        /*限功率控制*/
        case Cmd_LimitedPower:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            LimitedPowerA = 0;
            LimitedPowerB = 0;
            LimitedPowerC = 0;

            if(strncmp(&ActionData[i], "A:", 2) == 0)
            {
                i = i + 2;

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        LimitedPowerA = astr2int(&ActionData[i], (u8)j);
                        i += j + 1;
                        break;
                    }
                }
            }

            if(strncmp(&ActionData[i], "B:", 2) == 0)
            {
                i = i + 2;

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        LimitedPowerB = astr2int(&ActionData[i], (u8)j);
                        i += j + 1;
                        break;
                    }
                }
            }

            if(strncmp(&ActionData[i], "C:", 2) == 0)
            {
                i = i + 2;

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        LimitedPowerC = astr2int(&ActionData[i], (u8)j);
                        i += j + 1;
                        break;
                    }
                }
            }

            Dtu3Detail.Property.LimitPower_MyPower[0] = LimitedPowerA;
            Dtu3Detail.Property.LimitPower_MyPower[1] = LimitedPowerB;
            Dtu3Detail.Property.LimitPower_MyPower[2] = LimitedPowerC;

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            //System_DtuDetail_Write(&Dtu3Detail);
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            break;

        /*防逆流控制*/
        case Cmd_AntiBackflow:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            i = 0;

            if(strncmp(ActionData, "1", 1) == 0)
            {
                Dtu3Detail.Property.Zero_Export_Switch = 1;
                Dtu3Detail.Property.RS485Mode = 0;
            }
            else
            {
                Dtu3Detail.Property.Zero_Export_Switch = 0;
            }

            i++;

            if(strncmp(&ActionData[i], ",", 1) == 0)
            {
                i++;
            }

            if(strncmp(&ActionData[i], "A:", 2) == 0)
            {
                i = i + 2;

                if(strncmp(&ActionData[i], "-", 1) == 0)
                {
                    SymbolA = -1;
                    i++;
                }
                else
                {
                    SymbolA = 1;
                }

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        Dtu3Detail.Property.SurplusPowerA = (s32)astr2int(&ActionData[i], (u8)j) * SymbolA;
                        i += j + 1;
                        break;
                    }
                }
            }

            if(strncmp(&ActionData[i], "B:", 2) == 0)
            {
                i = i + 2;

                if(strncmp(&ActionData[i], "-", 1) == 0)
                {
                    SymbolB = -1;
                    i++;
                }
                else
                {
                    SymbolB = 1;
                }

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        Dtu3Detail.Property.SurplusPowerB = (s32)astr2int(&ActionData[i], (u8)j) * SymbolB;
                        i += j + 1;
                        break;
                    }
                }
            }

            if(strncmp(&ActionData[i], "C:", 2) == 0)
            {
                i = i + 2;

                if(strncmp(&ActionData[i], "-", 1) == 0)
                {
                    SymbolC = -1;
                    i++;
                }
                else
                {
                    SymbolC = 1;
                }

                for(j = 0; (i + j) < strlen(ActionData); j++)
                {
                    if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                    {
                        Dtu3Detail.Property.SurplusPowerC = (s32)astr2int(&ActionData[i], (u8)j) * SymbolC;
                        i += j + 1;
                        break;
                    }
                }
            }

            for(j = 0; (i + j) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                {
                    Dtu3Detail.Property.OverallControl = (u8)astr2int(&ActionData[i], (u8)j);
                    i += j + 1;
                    break;
                }
            }

            if((Dtu3Detail.Property.SurplusPowerA != 0) || (Dtu3Detail.Property.SurplusPowerB != 0) || (Dtu3Detail.Property.SurplusPowerC != 0))
            {
                Dtu3Detail.Property.SurplusSwitch = 1;
            }
            else
            {
                Dtu3Detail.Property.SurplusSwitch = 0;
            }

            if(sns_count != 0)
            {
                //                for(k = 0; k < sns_count; k++)
                //                {
                //                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                //                    {
                //                        if(PORT_NUMBER_CONFIRMATION(i))
                //                        {
                //                            sn = ((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]);
                //                            if(sn == sns[k])
                //                            {
                //#ifdef DEBUG
                //                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                //                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                //                                printf("%02x", MIMajor[i].Property.Id[0]);
                //                                printf("%02x", MIMajor[i].Property.Id[1]);
                //                                printf("%02x", MIMajor[i].Property.Id[2]);
                //                                printf("%02x", MIMajor[i].Property.Id[3]);
                //                                printf("\n");
                //#endif
                //                                MIReal[i].Data.NetCmd = NET_ZERO_EXPORT;
                //                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                //                                i = 0;
                //                                break;
                //                            }
                //                        }
                //                    }
                //                }
            }
            else if(sns_count == 0)
            {
                //                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                //                {
                //                    if(PORT_NUMBER_CONFIRMATION(i))
                //                    {
                //                        MIReal[i].Data.NetCmd = NET_ZERO_EXPORT;
                //                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                //                        if(Dtu3Detail.Property.Zero_Export_Switch == 1)
                //                        {
                //                            MIMajor[i].Property.Acq_Switch = 0;
                //                            MIMajor[i].Property.Power_Limit = 0;
                //                        }
                //#ifdef DTU3PRO
                //                        else
                //                        {
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                            MIMajor[i].Property.Power_Limit = MY_MIN_POWER_LIMIT * 10;
                //                        }
                //#endif
                //                    }
                //                }
            }

            //System_DtuDetail_Write(&Dtu3Detail);
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            break;
#endif

        /*清除接地故障*/
        case Cmd_ClearGroundFault:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_CLEAN_GFDI;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_CLEAN_GFDI;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*CT设置*/
        case Cmd_CTSetting:
            for(i = 0, j = 0; (i + j) < strlen(ActionData); j++)
            {
                if(strncmp(&ActionData[i + j], ":", 1) == 0)
                {
                    memset((u8 *)get_sn, 0, sizeof(get_sn));
                    Ascii2Hex((u8 *)&ActionData[i], (u8 *)get_sn, (u8)j);
                    i += j + 1;
                    break;
                }
            }

            for(j = 0; (i + j) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[i + j], ",", 1) == 0) || (strncmp(&ActionData[i + j], "\r", 1) == 0))
                {
                    CTNum = astr2int(&ActionData[i], (u8)j);
#ifdef DTU3PRO

                    for(m = 0; m < MeterLinkNum; m++)
                    {
                        if((Meter[m].Property.Pre_Id[0] == get_sn[0]) &&
                                (Meter[m].Property.Pre_Id[1] == get_sn[1]) &&
                                (Meter[m].Property.Id[0] == get_sn[2]) &&
                                (Meter[m].Property.Id[1] == get_sn[3]) &&
                                (Meter[m].Property.Id[2] == get_sn[4]) &&
                                (Meter[m].Property.Id[3] == get_sn[5]))
                        {
                            if((Meter[m].Property.Pre_Id[0] == 0x10) &&
                                    (Meter[m].Property.Pre_Id[1] == 0xC0))
                            {
                                MeterInfo[m].CT = (u16)(CTNum / 5);
                            }
                            else
                            {
                                MeterInfo[m].CT = (u16)(CTNum);
                            }
                        }
                    }

#endif
                    i += j + 1;
                    break;
                }
            }

            if(strncmp(&ActionData[i - 1], "\r", 1) == 0)
            {
#ifdef DTU3PRO
                CT_set_flag = 1;
#endif
            }

            break;

        /*加锁*/
        case Cmd_Lock:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_LOCK;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_LOCK;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*解锁*/
        case Cmd_Unlock:
            if(APP_Flg == 1)
            {
                AppState = OtherNetCmdState;
                PortNO_App = Dtu3Detail.Property.PortNum;
            }

            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_UNLOCK;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_UNLOCK;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            break;

        /*配置并网文件*/
        case Cmd_ConfigGridFile:
            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_WAIT_DOWNLOAD_DAT;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_WAIT_DOWNLOAD_DAT;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            memset((u8 *)up_pro_url, 0, sizeof(up_pro_url));
            memset((u8 *)up_pro_filename, 0, sizeof(up_pro_filename));
            memset((u8 *)up_pro_domain, 0, sizeof(up_pro_domain));
            HttpMode = NetURLResolution(ActionData, (u8 *)up_pro_domain, (u8 *)up_pro_destip, (u16 *)&up_pro_destport, (u8 *)up_pro_url, (u8 *)up_pro_filename);
#ifdef DEBUG
            printf("up_pro_domain %s\n", up_pro_domain);
            printf("up_pro_destip %d.%d.%d.%d\n", up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3]);
            printf("up_pro_destport %d\n", up_pro_destport);
            printf("up_pro_url %s\n", up_pro_url);
            printf("up_pro_filename %s\n", up_pro_filename);
#endif
            break;

        /*升级微逆程序*/
        case Cmd_MIUpgrade:
            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif
                                MIReal[i].Data.NetCmd = NET_WAIT_DOWNLOAD_PRO;
                                MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        MIReal[i].Data.NetCmd = NET_WAIT_DOWNLOAD_PRO;
                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                    }
                }
            }

            memset((u8 *)up_pro_url, 0, sizeof(up_pro_url));
            memset((u8 *)up_pro_filename, 0, sizeof(up_pro_filename));
            memset((u8 *)up_pro_domain, 0, sizeof(up_pro_domain));
            HttpMode = NetURLResolution(ActionData, (u8 *)up_pro_domain, (u8 *)up_pro_destip, (u16 *)&up_pro_destport, (u8 *)up_pro_url, (u8 *)up_pro_filename);
#ifdef DEBUG
            printf("up_pro_domain %s\n", up_pro_domain);
            printf("up_pro_destip %d.%d.%d.%d\n", up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3]);
            printf("up_pro_destport %d\n", up_pro_destport);
            printf("up_pro_url %s\n", up_pro_url);
            printf("up_pro_filename %s\n", up_pro_filename);
#endif
            break;

        /*ID组网*/
        case Cmd_IDNetwork:
            if(package_now == 0)
            {
                for(m = 0; m < CacheMax; m++)
                {
                    CommunicationCache[m].len = 0;
                }

                MiNum = 0;
                Network_Function_Reset();
                //系统总组件数
                Dtu3Detail.Property.Reflux_SysPanelNum = (u16)(total_a + total_b + total_c);
                //系统下A相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_A = (u16)(total_a);
                //系统下B相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_B = (u16)(total_b);
                //系统下C相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_C = (u16)(total_c);
            }

            //<ID>,<ID>,<ID>,…<CR>
            for(i = 0; i < a_count; i++)
            {
                MIMajor[MiNum].Property.Pre_Id[0] = (u8)(item_a[i] >> 40);
                MIMajor[MiNum].Property.Pre_Id[1] = (u8)(item_a[i] >> 32);
                MIMajor[MiNum].Property.Id[0] = (u8)(item_a[i] >> 24);
                MIMajor[MiNum].Property.Id[1] = (u8)(item_a[i] >> 16);
                MIMajor[MiNum].Property.Id[2] = (u8)(item_a[i] >> 8);
                MIMajor[MiNum].Property.Id[3] = (u8)(item_a[i]);
#ifdef DEBUG
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[2]);
                printf("%02x", MIMajor[MiNum].Property.Id[3]);
                printf("Id_In_Phase = 1");
                printf("\n");
#endif
                MIMajor[MiNum].Property.Id_In_Phase = 0;
                MiNum++;
            }

            CommunicationCache[package_now].len = (u32)a_count;

            if(package_nub == package_now + 1)
            {
                if((MiNum + CommunicationCache[package_now].len) > 0)
                {
                    //Dtu3Detail.Property.PortNum = k;
                    MIReal[0].Data.NetCmd = NET_REGISTER_ID;
                    MIReal[0].Data.NetStatus = NET_NOT_EXECUTED;
                }
            }

            err_code = 0;
            break;

        /*防逆流组网*/
        case Cmd_AntiBackflowNetwork:
#ifdef DTU3PRO
            if(package_now == 0)
            {
                for(m = 0; m < CacheMax; m++)
                {
                    CommunicationCache[m].len = 0;
                }

                MiNum = 0;
                Network_Function_Reset();
                //系统总组件数
                Dtu3Detail.Property.Reflux_SysPanelNum = (u16)(total_a + total_b + total_c);
                //系统下A相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_A = (u16)(total_a);
                //系统下B相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_B = (u16)(total_b);
                //系统下C相组件数
                Dtu3Detail.Property.Reflux_SysPanelNum_C = (u16)(total_c);
            }

            //<ID>,<ID>,<ID>,…<CR>
            for(i = 0; i < a_count; i++)
            {
                //0x 10 C0 00 00 00 00 00
                //if((((item_a[i] >> 40) & 0x10C0) == 0x10C0) || (((item_a[i] >> 40) & 0x10C1) == 0x10C1))
                if(((item_a[i] >> 44) & 0x10C) == 0x10C)
                {
                    Meter[MeterLinkNum].Property.Pre_Id[0] = (u8)(item_a[i] >> 48);
                    Meter[MeterLinkNum].Property.Pre_Id[1] = (u8)(item_a[i] >> 40);
                    Meter[MeterLinkNum].Property.Id[0] = (u8)(item_a[i] >> 32);
                    Meter[MeterLinkNum].Property.Id[1] = (u8)(item_a[i] >> 24);
                    Meter[MeterLinkNum].Property.Id[2] = (u8)(item_a[i] >> 16);
                    Meter[MeterLinkNum].Property.Id[3] = (u8)(item_a[i] >> 8);
                    Meter[MeterLinkNum].Property.place = (u8)(item_a[i]);
#ifdef DEBUG
                    printf("%02x", Meter[MeterLinkNum].Property.Pre_Id[0]);
                    printf("%02x", Meter[MeterLinkNum].Property.Pre_Id[1]);
                    printf("%02x", Meter[MeterLinkNum].Property.Id[0]);
                    printf("%02x", Meter[MeterLinkNum].Property.Id[1]);
                    printf("%02x", Meter[MeterLinkNum].Property.Id[2]);
                    printf("%02x", Meter[MeterLinkNum].Property.Id[3]);
                    printf("%02x", Meter[MeterLinkNum].Property.place);
                    printf("\n");
#endif
                    MeterInfo[MeterLinkNum].sn = (Meter[MeterLinkNum].Property.Id[2] % 16) * 100 + (Meter[MeterLinkNum].Property.Id[3] / 16) * 10 + (Meter[MeterLinkNum].Property.Id[3] % 16);
                    MeterLinkNum++;
                }
                else
                {
                    MIMajor[MiNum].Property.Pre_Id[0] = (u8)(item_a[i] >> 40);
                    MIMajor[MiNum].Property.Pre_Id[1] = (u8)(item_a[i] >> 32);
                    MIMajor[MiNum].Property.Id[0] = (u8)(item_a[i] >> 24);
                    MIMajor[MiNum].Property.Id[1] = (u8)(item_a[i] >> 16);
                    MIMajor[MiNum].Property.Id[2] = (u8)(item_a[i] >> 8);
                    MIMajor[MiNum].Property.Id[3] = (u8)(item_a[i]);
#ifdef DEBUG
                    printf("%02x", MIMajor[MiNum].Property.Pre_Id[0]);
                    printf("%02x", MIMajor[MiNum].Property.Pre_Id[1]);
                    printf("%02x", MIMajor[MiNum].Property.Id[0]);
                    printf("%02x", MIMajor[MiNum].Property.Id[1]);
                    printf("%02x", MIMajor[MiNum].Property.Id[2]);
                    printf("%02x", MIMajor[MiNum].Property.Id[3]);
                    printf("Id_In_Phase = 1");
                    printf("\n");
#endif
                    MIMajor[MiNum].Property.Id_In_Phase = 1;
                    MiNum++;
                }
            }

            //<ID>,<ID>,<ID>,…<CR>
            for(i = 0; i < b_count; i++)
            {
                MIMajor[MiNum].Property.Pre_Id[0] = (u8)(item_b[i] >> 40);
                MIMajor[MiNum].Property.Pre_Id[1] = (u8)(item_b[i] >> 32);
                MIMajor[MiNum].Property.Id[0] = (u8)(item_b[i] >> 24);
                MIMajor[MiNum].Property.Id[1] = (u8)(item_b[i] >> 16);
                MIMajor[MiNum].Property.Id[2] = (u8)(item_b[i] >> 8);
                MIMajor[MiNum].Property.Id[3] = (u8)(item_b[i]);
#ifdef DEBUG
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[2]);
                printf("%02x", MIMajor[MiNum].Property.Id[3]);
                printf("Id_In_Phase = 2");
                printf("\n");
#endif
                MIMajor[MiNum].Property.Id_In_Phase = 2;
                MiNum++;
            }

            for(i = 0; i < c_count; i++)
            {
                MIMajor[MiNum].Property.Pre_Id[0] = (u8)(item_c[i] >> 40);
                MIMajor[MiNum].Property.Pre_Id[1] = (u8)(item_c[i] >> 32);
                MIMajor[MiNum].Property.Id[0] = (u8)(item_c[i] >> 24);
                MIMajor[MiNum].Property.Id[1] = (u8)(item_c[i] >> 16);
                MIMajor[MiNum].Property.Id[2] = (u8)(item_c[i] >> 8);
                MIMajor[MiNum].Property.Id[3] = (u8)(item_c[i]);
#ifdef DEBUG
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Pre_Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[0]);
                printf("%02x", MIMajor[MiNum].Property.Id[1]);
                printf("%02x", MIMajor[MiNum].Property.Id[2]);
                printf("%02x", MIMajor[MiNum].Property.Id[3]);
                printf("Id_In_Phase = 3");
                printf("\n");
#endif
                MIMajor[MiNum].Property.Id_In_Phase = 3;
                MiNum++;
            }

            CommunicationCache[package_now].len = (u32)(a_count + b_count + c_count);

            if(package_nub == package_now + 1)
            {
                if((MiNum + CommunicationCache[package_now].len) > 0)
                {
                    //Dtu3Detail.Property.PortNum = k;
                    MIReal[0].Data.NetCmd = NET_REGISTER_ID;
                    MIReal[0].Data.NetStatus = NET_NOT_EXECUTED;
                }

                if((a_count + b_count + c_count) > 0)
                {
                    //Dtu3Detail.Property.PortNum = k;
                    //PortNO = NET_REGISTER_ID;
                    MIReal[0].Data.NetCmd = NET_REGISTER_ID;
                    MIReal[0].Data.NetStatus = NET_NOT_EXECUTED;
                }

                MeterMajor_Delete();
                MeterInfor_Write((MeterMajor *)Meter, MeterLinkNum);
            }

#endif

            for(i = 0, j = 0; (i + j) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[i + j], ",", 1) == 0) || ((strncmp(&ActionData[i + j], "\r", 1) == 0)))
                {
                    Dtu3Detail.Property.GridType = (u8)astr2int(&ActionData[i], (u8)j);
                    i += j + 1;
                    break;
                }
            }

            err_code = 0;
            break;

        /*中止控制命令执行*/
        case Cmd_AbortControl:
            if(sns_count != 0)
            {
                for(k = 0; k < sns_count; k++)
                {
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                            if(sn == sns[k])
                            {
#ifdef DEBUG
                                printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[0]);
                                printf("%02x", MIMajor[i].Property.Id[1]);
                                printf("%02x", MIMajor[i].Property.Id[2]);
                                printf("%02x", MIMajor[i].Property.Id[3]);
                                printf("\n");
#endif

                                if((MIReal[i].Data.NetStatus == 0) || (MIReal[i].Data.NetStatus > 100))
                                {
                                    MIReal[i].Data.NetCmd = NET_INIT;
                                    MIReal[i].Data.NetStatus = NET_NOCMD;
                                }

                                i = 0;
                                break;
                            }
                        }
                    }
                }
            }
            else if(sns_count == 0)
            {
                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        if((MIReal[i].Data.NetStatus == 0) || (MIReal[i].Data.NetStatus > 100))
                        {
                            MIReal[i].Data.NetCmd = NET_INIT;
                            MIReal[i].Data.NetStatus = NET_NOCMD;
                        }
                    }
                }
            }

            err_code = 0;
            break;

        /*wifi设置*/
        case Cmd_WiFiSettings:
            //设置标志位
            Dtu3Major.Property.wifi_ssid_save_set = 1;

            for(i = 0; i < strlen(ActionData); i++)
            {
                if(strncmp(&ActionData[i], ",", 1) == 0)
                {
                    memset((u8 *)Dtu3Major.Property.wifi_ssid, 0, sizeof(Dtu3Major.Property.wifi_ssid));
                    memcpy((u8 *)Dtu3Major.Property.wifi_ssid, ActionData, i);
                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if(strncmp(&ActionData[j + i], "\r", 1) == 0)
                {
                    memset((u8 *)Dtu3Major.Property.wifi_passward, 0, sizeof(Dtu3Major.Property.wifi_passward));
                    memcpy((u8 *)Dtu3Major.Property.wifi_passward, &ActionData[i], j);
                    break;
                }
            }

#ifdef DEBUG
            printf("Dtu3Major.Property.wifi_ssid %s\n", Dtu3Major.Property.wifi_ssid);
            printf("Dtu3Major.Property.wifi_passward %s\n", Dtu3Major.Property.wifi_passward);
#endif
            err_code = 0;
            break;

        /*设置服务器域名，端口号*/
        case Cmd_ServerSettings:
            for(i = 0; i < strlen(ActionData); i++)
            {
                if(strncmp(&ActionData[i], ",", 1) == 0)
                {
                    memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
                    memcpy((u8 *)Dtu3Major.Property.ServerDomainName, ActionData, i);
                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if(strncmp(&ActionData[j + i], "\r", 1) == 0)
                {
                    Dtu3Major.Property.ServerPort = (u16)astr2int(&ActionData[i], (u8)j);
                    break;
                }
            }

#ifdef DEBUG
            printf("Dtu3Major.Property.ServerDomainName %s\n", Dtu3Major.Property.ServerDomainName);
            printf("Dtu3Major.Property.ServerPort %d\n", Dtu3Major.Property.ServerPort);
#endif
            err_code = 0;
            break;

        /*设置GPRS APN*/
        case Cmd_GprsAPN:
            for(i = 0; i < strlen(ActionData); i++)
            {
                if((strncmp(&ActionData[i], ",", 1) == 0) || (strncmp(&ActionData[j + i], "\r", 1) == 0))
                {
                    err_code = 0;
                    memset((u8 *)APN, 0, sizeof(APN));
                    memcpy((u8 *)APN, ActionData, i);
                    memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
                    memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));
                    memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)APN, sizeof(Dtu3Major.Property.APN));
                    memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)&APN[sizeof(Dtu3Major.Property.APN)], sizeof(Dtu3Major.Property.APN2));
                    i++;
                    break;
                }
            }

            break;

        /*防盗开关*/
        case Cmd_AntiTheftStatus:
            if(strncmp(ActionData, "1", 1) == 0)
            {
                err_code = 0;
                Dtu3Detail.Property.Anti_Theft_Switch = 1;
            }
            else if(strncmp(ActionData, "0", 1) == 0)
            {
                err_code = 0;
                Dtu3Detail.Property.Anti_Theft_Switch = 0;
            }

            break;

        /*APP请求多微逆通讯状态*/
        case Cmd_AppMultiCom:
            if(APP_Flg == 1)
            {
                err_code = 0;
                AppState = AllMiLossRate;

                if((LocalTime - app_switching_time > APP_HOLD_TIME) || (app_switching_time == 0))
                {
                    PortNO_App = Dtu3Detail.Property.PortNum;

                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            MIReal[i].Data.NetCmd = NET_INIT;
                            MIReal[i].Data.NetStatus = NET_NOCMD;
                        }
                    }
                }
                else
                {
                    PortNO_App = Dtu3Detail.Property.PortNum + 1;
                }

                app_switching_time = LocalTime;
            }

            break;

        /*APP请求单微逆通讯状态*/
        case Cmd_AppSingleCom:
            if(APP_Flg == 1)
            {
                err_code = 0;
                AppState = ManuSigleLossRate;
                app_switching_time = LocalTime;

                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                        if(sn == sns[0])
                        {
                            APPOperatSN = sns[0];
                            PortNO_App = i;
                            MIReal[i].Data.Real_Time = RTC_Getsecond();
                            MIReal[i].Data.NetCmd = NET_INIT;
                            MIReal[i].Data.NetStatus = NET_NOCMD;
                            break;
                        }
                    }
                }
            }

            break;

        /*APP升级DTU*/
        case Cmd_AppDTUUpgrade:
            if(APP_Flg == 1)
            {
                err_code = 0;
            }

            break;

        /*APP升级微逆*/
        case Cmd_AppMIUpgrade:
            if(APP_Flg == 1)
            {
                err_code = 0;
            }

            break;

        /*APP收集全部微逆数据*/
        case Cmd_AppCollectAllData:
            if(APP_Flg == 1)
            {
                AlarmInfo_Delete();
                err_code = 0;
                AppState = PollAllMi;

                if((LocalTime - app_switching_time_real > APP_HOLD_TIME) || (app_switching_time_real == 0))
                {
                    PortNO_App = Dtu3Detail.Property.PortNum;
                }
                else
                {
                    PortNO_App = Dtu3Detail.Property.PortNum + 1;
                }
            }

            app_switching_time_real = LocalTime;
            break;

        /*APP收集单个微逆数据*/
        case Cmd_AppCollectSingleData:
            if(APP_Flg == 1)
            {
                AlarmInfo_Delete();
                err_code = 0;
                AppState = PollSigleMi;
                app_switching_time_real = LocalTime;

                for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                {
                    if(PORT_NUMBER_CONFIRMATION(i))
                    {
                        sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                        if(sn == sns[0])
                        {
                            APPOperatSN = sns[0];
                            PortNO_App = i;
                            MIReal[i].Data.NetCmd = NET_INIT;
                            MIReal[i].Data.NetStatus = NET_NOCMD;
                            break;
                        }
                    }
                }
            }

            break;

        /*APP升级DTU校验*/
        case Cmd_AppDTUCheck:
            if(APP_Flg == 1)
            {
                err_code = 0;
            }

            break;
#ifdef DTU3PRO

        /*RS485功能设置*/
        case Cmd_RS485Config:
            for(i = 0; i < strlen(ActionData); i++)
            {
                if((strncmp(&ActionData[i], ",", 1) == 0) || (strncmp(&ActionData[i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.RS485Mode = (u8)astr2int(&ActionData[i - 1], 1);

                    if(Dtu3Detail.Property.RS485Mode == 1)
                    {
                        Dtu3Detail.Property.Zero_Export_Switch = 0;
                    }
                    else
                    {
                        Dtu3Detail.Property.RS485Addr = 0;
                    }

                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[j + i], ",", 1) == 0) || (strncmp(&ActionData[j + i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.RS485Addr = (u8)astr2int(&ActionData[i], (u8)j);
                    break;
                }
            }

            //System_DtuDetail_Write(&Dtu3Detail);
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            break;

        //相容差设置
        case Cmd_ToleranceDiff:
            for(i = 0; i < strlen(ActionData); i++)
            {
                if((strncmp(&ActionData[i], ",", 1) == 0) || (strncmp(&ActionData[i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.Phase_Balance_Switch = (u8)astr2int(&ActionData[i - 1], 1);
                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[j + i], ",", 1) == 0) || (strncmp(&ActionData[j + i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.Tolerance_Between_Phases = (u16)astr2int(&ActionData[i], (u8)j);
                    break;
                }
            }

            //System_DtuDetail_Write(&Dtu3Detail);
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            break;

        //SunSpec
        case Cmd_SunSpec:
            for(i = 0; i < strlen(ActionData); i++)
            {
                if((strncmp(&ActionData[i], ",", 1) == 0) || (strncmp(&ActionData[i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.SunSpec_Switch = (u8)astr2int(&ActionData[i - 1], 1);

                    if(Dtu3Detail.Property.SunSpec_Switch == 0)
                    {
                        Dtu3Detail.Property.SunSpec_Addr_Start = 0;
                    }

                    i++;
                    break;
                }
            }

            for(j = 0; (j + i) < strlen(ActionData); j++)
            {
                if((strncmp(&ActionData[j + i], ",", 1) == 0) || (strncmp(&ActionData[j + i], "\r", 1) == 0))
                {
                    Dtu3Detail.Property.SunSpec_Addr_Start = (u16)astr2int(&ActionData[i], (u8)j);
                    break;
                }
            }

            SunSpec_DeInit();
            System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            break;
#endif

        default:
            err_code = 1;
            break;
    }

#ifdef DEBUG
    printf("err_code: %d\n", err_code);
    printf("Command: %d\n", Command);
#endif
}

/***********************************************
** Function name:       NetMIScanning
** Descriptions:        MI控制状态扫描
** input parameters:    u8 reqkind, s32 *CmdTotal, void *ControlID,
**                      s32 *Schedule, s32 *CmdSuccess,
**                      s32 *CmdFailure, s32 *CmdNotExecuted
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
void NetMIScanning(u8 reqkind, s32 *CmdTotal, char *ControlID, s32 *Schedule, s32 *CmdSuccess,
                   s32 *CmdFailure, s32 *CmdNotExecuted)
{
    vu16  i;

    if(APP_Flg == 1)
    {
        switch(AppState)
        {
            /*单个微逆扫描*/
            case PollSigleMi:
                {
                }
                break;

            /*多个微逆扫描*/
            case PollAllMi:
                {
                }
                break;

            /*多个丢包率统计*/
            case AllMiLossRate:
                {
                    //            AppState = PollAllMi;
                    //            PortNO_App = Dtu3Detail.Property.PortNum;
                    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i))
                        {
                            memset(ControlID, 0, 12);
                            Hex2Ascii((u8 *)&ControlID[0], (u8 *)MIMajor[i].Property.Pre_Id, 2);
                            Hex2Ascii((u8 *)&ControlID[4], (u8 *)MIMajor[i].Property.Id, 4);
                            *Schedule = 99;//MIReal[i].Data.NetStatus;
                            *CmdTotal = *CmdTotal + 1;
                        }
                    }
                }
                break;

            /*单个丢包率统计*/
            case ManuSigleLossRate:
                {
                    memset(ControlID, 0, 12);
                    Hex2Ascii((u8 *)&ControlID[0], (u8 *)MIMajor[PortNO_App].Property.Pre_Id, 2);
                    Hex2Ascii((u8 *)&ControlID[4], (u8 *)MIMajor[PortNO_App].Property.Id, 4);
                    *Schedule = MIReal[PortNO_App].Data.NetStatus;
                    *CmdTotal = *CmdTotal + 1;
                }
                break;

            /*网络命令模式*/
            case OtherNetCmdState:
                {
                }
                break;

            default:
                break;
        }
    }
    else
    {
        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
        {
            if(PORT_NUMBER_CONFIRMATION(i))
            {
                /*有网络命令执行失败*/
                if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                {
                    *CmdFailure = *CmdFailure + 1;
                    *CmdTotal = *CmdTotal + 1;
                }
                /*有网络命令未执行*/
                else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                {
                    *CmdNotExecuted = *CmdNotExecuted + 1;
                    *CmdTotal = *CmdTotal + 1;
                }
                /*有网络命令执行完成*/
                else if(MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED)
                {
                    *CmdSuccess = *CmdSuccess + 1;
                    *CmdTotal = *CmdTotal + 1;
                }
                /*有网络命令正在执行*/
                else if((MIReal[i].Data.NetStatus > NET_NOCMD) && (MIReal[i].Data.NetStatus < NET_EXECUTION_COMPLETED))
                {
                    memset(ControlID, 0, 12);
                    Hex2Ascii((u8 *)&ControlID[0], (u8 *)MIMajor[i].Property.Pre_Id, 2);
                    Hex2Ascii((u8 *)&ControlID[4], (u8 *)MIMajor[i].Property.Id, 4);
                    *Schedule = MIReal[i].Data.NetStatus;
                    *CmdTotal = *CmdTotal + 1;
                }
            }
        }
    }
}

/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_HBReq(u16 TAG, char *buffer)
{
    vu16  len = 0;
    vu16  Encodelen = 0;
    HBReqDTO *mHBReq = NULL;
    char Strdtu_ID[14] = {0};
    DTUSeq++;
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mHBReq = mymalloc(sizeof(HBReqDTO));
    //    if(mHBReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mHBReq, 0, sizeof(HBReqDTO));
    Encodelen = Encodelen + sizeof(HBReqDTO);
    {
        //mHBReq
        //绝对时间
        mHBReq->offset = Dtu3Detail.Property.timezone;
        mHBReq->time = (s32)RTC_Getsecond();
        //gprs信号强度
#ifdef DTU3PRO

        if(Netmode_Used == GPRS_MODE)
        {
            mHBReq->csq = Dtu3Major.Property.GPRS_CSQ;
        }
        else if(Netmode_Used == WIFI_MODE)
        {
            mHBReq->csq = Dtu3Major.Property.wifi_RSSI;
        }
        else
        {
            mHBReq->csq = 100;
        }

#else

        if(Dtu3Major.Property.netmode_select == GPRS_MODE)
        {
            mHBReq->csq = Dtu3Major.Property.GPRS_CSQ;
        }
        else if(Dtu3Major.Property.netmode_select == WIFI_MODE)
        {
            mHBReq->csq = Dtu3Major.Property.wifi_RSSI;
        }

#endif
        mHBReq->dtu_sn.funcs.encode = encode_bytes;
        mHBReq->dtu_sn.arg = Strdtu_ID;
        Encodelen = Encodelen + sizeof(Strdtu_ID);
    }
    //发送端//把结构体的数据转换到发送缓存
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, HBReqDTO_fields, mHBReq);
    myfree(mHBReq);
    return len;
}


/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_HBRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    HBResDTO *mHBRes = NULL;
    mHBRes = mymalloc(sizeof(HBResDTO));
    //    if(mHBRes == NULL)
    //    {
    //        return 0;
    //    }
    memset(mHBRes, 0, sizeof(HBResDTO));
    {
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, HBResDTO_fields, mHBRes);

    if(pb_status == 1)
    {
        Net_TimeAnalysis(mHBRes->time, mHBRes->offset);
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mHBRes);
    return ReturnNum;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
#ifdef DTU3PRO
u16 Encode_InfoDataReq(u16 TAG, u8 package_nub, u8 package_now,
                       u32 time, u16 MeterCount,
                       u16 MeterOff, u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer,
                       InverterDetail *mInverterDetail)
#else
u16 Encode_InfoDataReq(u16 TAG, u8 package_nub, u8 package_now,
                       u32 time, u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer,
                       InverterDetail *mInverterDetail)
#endif
{
    vu16  i = 0;
    vu16  len = 0;
    vu16  bufflen = 20;
    InfoDataReqDTO *mInfoDataReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(InfoDataReqDTO);
    calendar_obj calendar;
    DTUSeq++;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mInfoDataReq = mymalloc(sizeof(InfoDataReqDTO));
    //    if(mInfoDataReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mInfoDataReq, 0, sizeof(InfoDataReqDTO));
    {
        mInfoDataReq->dtu_sn.funcs.encode = encode_bytes;
        mInfoDataReq->dtu_sn.arg = Strdtu_ID;
        Encodelen = Encodelen + sizeof(Strdtu_ID);
        SecToDate(time, &calendar);
        mInfoDataReq->time = (s32)RTC_Getsecond();
        /*MI数量*/
        mInfoDataReq->device_nub = Dtu3Detail.Property.InverterNum;
        /*组件数量*/
        mInfoDataReq->pv_nub = Dtu3Detail.Property.PortNum;
        /*总包数量*/
        mInfoDataReq->package_nub = package_nub;
        /*当前包序*/
        mInfoDataReq->package_now = package_now;
        /*使用通道号*/
        mInfoDataReq->channel = 0x01;
#ifdef DTU3PRO

        if(package_now == 0)
        {
            for(i = 0; i < 10; i++)
            {
                mInfoDataReq->m_feature[i].value.arg = mymalloc(100);
                memset(mInfoDataReq->m_feature[i].value.arg, 0, 100);
            }

            if(Dtu3Detail.Property.Zero_Export_Switch == 1)
            {
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].key = 0x09;
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.funcs.encode = encode_bytes;
                sprintf(mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.arg,
                        "%d,A:%d,B:%d,C:%d,%d\r",
                        Dtu3Detail.Property.Zero_Export_Switch,
                        Dtu3Detail.Property.SurplusPowerA,
                        Dtu3Detail.Property.SurplusPowerB,
                        Dtu3Detail.Property.SurplusPowerC,
                        Dtu3Detail.Property.OverallControl);
                mInfoDataReq->m_feature_count++;
            }

            if(Dtu3Detail.Property.RS485Mode == 1)
            {
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].key = 0x24;
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.funcs.encode = encode_bytes;
                sprintf(mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.arg,
                        "%d,%d\r",
                        Dtu3Detail.Property.RS485Mode,
                        Dtu3Detail.Property.RS485Addr);
                mInfoDataReq->m_feature_count++;
            }

            if(Dtu3Detail.Property.Phase_Balance_Switch == 1)
            {
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].key = 0x25;
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.funcs.encode = encode_bytes;
                sprintf(mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.arg,
                        "%d,%d\r",
                        Dtu3Detail.Property.Phase_Balance_Switch,
                        Dtu3Detail.Property.Tolerance_Between_Phases);
                mInfoDataReq->m_feature_count++;
            }

            if(Dtu3Detail.Property.SunSpec_Switch == 1)
            {
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].key = 0x27;
                mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.funcs.encode = encode_bytes;
                sprintf(mInfoDataReq->m_feature[mInfoDataReq->m_feature_count].value.arg,
                        "%d,%d\r",
                        Dtu3Detail.Property.SunSpec_Switch,
                        Dtu3Detail.Property.SunSpec_Addr_Start);
                mInfoDataReq->m_feature_count++;
            }
        }

#endif
        /*系统信息*/
        //设备类型
        mInfoDataReq->mDtuInfo.device_kind = 0;
        //dtu连接服务器模式
#ifdef DTU3PRO
        mInfoDataReq->mDtuInfo.access_model = Netmode_Used;
#else
        mInfoDataReq->mDtuInfo.access_model = Dtu3Major.Property.netmode_select;
#endif
        //DTU硬件版本
        mInfoDataReq->mDtuInfo.dtu_hw = (s32)(((u32)(Dtu3Major.Property.DtuHw_Ver[0]) << 8) + (u32)(Dtu3Major.Property.DtuHw_Ver[1]));
        //DTU软件版本
        mInfoDataReq->mDtuInfo.dtu_sw = (s32)(((u32)(Dtu3Major.Property.DtuSw_Ver[0]) << 8) + (u32)(Dtu3Major.Property.DtuSw_Ver[1]));
        mInfoDataReq->mDtuInfo.dtu_step_time = Dtu3Detail.Property.Server_SendTimePeriod;
        //nRF硬件版本
        mInfoDataReq->mDtuInfo.dtu_rf_hw = (s32)(((u32)(Dtu3Major.Property.RfHw_Ver[0]) << 24) + ((u32)(Dtu3Major.Property.RfHw_Ver[1]) << 16) + ((u32)(Dtu3Major.Property.RfHw_Ver[2]) << 8) + (u32)(Dtu3Major.Property.RfHw_Ver[3]));
        //nRF软件版本
        mInfoDataReq->mDtuInfo.dtu_rf_sw = (s32)(((u32)(Dtu3Major.Property.RfFw_Ver[0]) << 24) + ((u32)(Dtu3Major.Property.RfFw_Ver[1]) << 16) + ((u32)(Dtu3Major.Property.RfFw_Ver[2]) << 8) + (u32)(Dtu3Major.Property.RfFw_Ver[3]));
        //GPRS版本号
        mInfoDataReq->mDtuInfo.gprs_vsn.funcs.encode = encode_bytes;
        mInfoDataReq->mDtuInfo.gprs_vsn.arg = mymalloc(bufflen);
        memset(mInfoDataReq->mDtuInfo.gprs_vsn.arg, 0, bufflen);
        Hex2Ascii(mInfoDataReq->mDtuInfo.gprs_vsn.arg, (u8 *)Dtu3Major.Property.Gprs_Ver, 4);
        Encodelen = Encodelen + bufflen;
        //GPRS卡号
        mInfoDataReq->mDtuInfo.ka_nub.funcs.encode = encode_bytes;
        mInfoDataReq->mDtuInfo.ka_nub.arg = mymalloc(bufflen * 5);
        memset(mInfoDataReq->mDtuInfo.ka_nub.arg, 0, bufflen * 5);
        Hex2Ascii(mInfoDataReq->mDtuInfo.ka_nub.arg, (u8 *)Dtu3Major.Property.Gprs_SIMNum, 11);
        Encodelen = Encodelen + bufflen * 5;
        //wifi版本号
        mInfoDataReq->mDtuInfo.wifi_vsn.funcs.encode = encode_bytes;
        mInfoDataReq->mDtuInfo.wifi_vsn.arg = mymalloc(bufflen);
        memset(mInfoDataReq->mDtuInfo.wifi_vsn.arg, 0, bufflen);
        Hex2Ascii(mInfoDataReq->mDtuInfo.wifi_vsn.arg, (u8 *)Dtu3Major.Property.Wifi_Vsn, 3);
        Encodelen = Encodelen + bufflen;
        mInfoDataReq->mDtuInfo.dtu_rule_id = Dtu3Major.Property.rule_id;
        mInfoDataReq->mDtuInfo.dtu_error_code = 0;
        mInfoDataReq->mDtuInfo.grid_type = Dtu3Detail.Property.GridType;
#ifdef DTU3PRO
        mInfoDataReq->mDtuInfo.zero_export_switch = Dtu3Detail.Property.Zero_Export_Switch;
        mInfoDataReq->mDtuInfo.surplus_power_a = Dtu3Detail.Property.SurplusPowerA;
        mInfoDataReq->mDtuInfo.surplus_power_b = Dtu3Detail.Property.SurplusPowerB;
        mInfoDataReq->mDtuInfo.surplus_power_c = Dtu3Detail.Property.SurplusPowerC;
        mInfoDataReq->mDtuInfo.zero_export_control = Dtu3Detail.Property.OverallControl;
        mInfoDataReq->mDtuInfo.phase_balance_switch = Dtu3Detail.Property.Phase_Balance_Switch;
        mInfoDataReq->mDtuInfo.tolerance_between_phases = Dtu3Detail.Property.Tolerance_Between_Phases;
#endif
        /*电表*/
        {
#ifdef DTU3PRO
            mInfoDataReq->mMeterInfo_count = MeterCount;

            for(i = 0; i < MeterCount; i++)
            {
                //设备类型
                mInfoDataReq->mMeterInfo[i].device_kind = 0;
                //电表序号--地址
                mInfoDataReq->mMeterInfo[i].meter_sn = (int64_t)(((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(Meter[i + MeterOff].Property.Id[0]) << 24) + ((uint64_t)(Meter[i + MeterOff].Property.Id[1]) << 16) + ((uint64_t)(Meter[i + MeterOff].Property.Id[2]) << 8) + (uint64_t)(Meter[i + MeterOff].Property.Id[3]));
                //电表型号
                mInfoDataReq->mMeterInfo[i].meter_model = (Meter[i + MeterOff].Property.Id[1] % 16) * 16 + (Meter[i + MeterOff].Property.Id[2]) / 16;
                //CT型号
                mInfoDataReq->mMeterInfo[i].meter_ct = MeterInfo[i + MeterOff].CT;
                //电表通讯方式
                mInfoDataReq->mMeterInfo[i].com_way = 1;
                //电表接入方式
                mInfoDataReq->mMeterInfo[i].access_mode = Meter[i + MeterOff].Property.place;
            }

#endif
        }
        /*中继*/
        {
            /*中继信息*/
            mInfoDataReq->mRpInfo_count = RpCount;

            for(i = 0; i < RpCount; i++)
            {
                //设备类型
                mInfoDataReq->mRpInfo[i].device_kind = 2;
                //电表序号--地址
                mInfoDataReq->mRpInfo[i].rp_sn = 1234;
                //中继硬件版本
                mInfoDataReq->mRpInfo[i].rp_hw = 123456;
                //中继软件版本
                mInfoDataReq->mRpInfo[i].rp_sw = 123456;
                mInfoDataReq->mRpInfo[i].rp_rule_id = 0xFFFF;
            }
        }
        /*单个组件的数据 包括故障数据*/
        mInfoDataReq->mpvInfo_count = PvCount;

        for(i = 0; i < PvCount; i++)
        {
            //设备类型
            mInfoDataReq->mpvInfo[i].device_kind = 1;
            //微逆 ID
            mInfoDataReq->mpvInfo[i].pv_sn = (int64_t)(((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[0]) << 40)
                                             + ((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[1]) << 32)
                                             + ((uint64_t)(MIMajor[i + PvOff].Property.Id[0]) << 24)
                                             + ((uint64_t)(MIMajor[i + PvOff].Property.Id[1]) << 16)
                                             + ((uint64_t)(MIMajor[i + PvOff].Property.Id[2]) << 8)
                                             + (uint64_t)(MIMajor[i + PvOff].Property.Id[3]));
            //引导程序版本
            mInfoDataReq->mpvInfo[i].pv_usfw        = mInverterDetail[i].Property.USFWBuild_VER;
            //应用程序版本
            mInfoDataReq->mpvInfo[i].pv_sw          = mInverterDetail[i].Property.AppFWBuild_VER;
            //硬件料号
            mInfoDataReq->mpvInfo[i].pv_hw_pn       = (s32)(((u32)(mInverterDetail[i].Property.HW_PNH) << 16) + (u32)(mInverterDetail[i].Property.HW_PNL));
            //硬件版本
            mInfoDataReq->mpvInfo[i].pv_hw          = mInverterDetail[i].Property.HW_VER;
            //并网保护文件代码
            mInfoDataReq->mpvInfo[i].pv_gpf_code    = mInverterDetail[i].Property.Country_Std;
            //并网保护文件版本
            mInfoDataReq->mpvInfo[i].pv_gpf         = mInverterDetail[i].Property.Save_Version;
            //nRF硬件版本
            mInfoDataReq->mpvInfo[i].pv_rf_hw       = (s32)(((u32)(mInverterDetail[i].Property.NRF_HardVersion[0]) << 24) + ((u32)(mInverterDetail[i].Property.NRF_HardVersion[1]) << 16) + ((u32)(mInverterDetail[i].Property.NRF_HardVersion[2]) << 8) + (u32)(mInverterDetail[i].Property.NRF_HardVersion[3]));
            //nRF软件版本
            mInfoDataReq->mpvInfo[i].pv_rf_sw       = (s32)(((u32)(mInverterDetail[i].Property.NRF_SoftVersion[0]) << 24) + ((u32)(mInverterDetail[i].Property.NRF_SoftVersion[1]) << 16) + ((u32)(mInverterDetail[i].Property.NRF_SoftVersion[2]) << 8) + (u32)(mInverterDetail[i].Property.NRF_SoftVersion[3]));
            mInfoDataReq->mpvInfo[i].mi_rule_id     = mInverterDetail[i].Property.rule_id;
        }
    }
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, InfoDataReqDTO_fields, mInfoDataReq);
#ifdef DTU3PRO

    if(package_now == 0)
    {
        for(i = 0; i < 10; i++)
        {
            myfree(mInfoDataReq->m_feature[i].value.arg);
        }
    }

#endif
    myfree(mInfoDataReq->mDtuInfo.gprs_vsn.arg);
    myfree(mInfoDataReq->mDtuInfo.ka_nub.arg);
    myfree(mInfoDataReq->mDtuInfo.wifi_vsn.arg);
    myfree(mInfoDataReq);
    return len;
}

/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_InfoDataRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    InfoDataResDTO *mInfoDataRes = NULL;
    pb_istream_t pb_istream;
    mInfoDataRes = mymalloc(sizeof(InfoDataResDTO));
    //    if(mInfoDataRes == NULL)
    //    {
    //        return 0;
    //    }
    memset(mInfoDataRes, 0, sizeof(InfoDataResDTO));
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, InfoDataResDTO_fields, mInfoDataRes);

    if(pb_status == 1)
    {
        err_code = (u8)mInfoDataRes->err_code;
        GetPackageNow = (u8)mInfoDataRes->package_now;
#ifdef DTU3PRO
#ifdef DEBUG
        printf("GetPackageNow %d\n", GetPackageNow);
        printf("mInfoDataRes->time %d\n", (u32)mInfoDataRes->time);
#endif
#endif
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mInfoDataRes);
    return ReturnNum;
}


/***********************************************
** Function name:       RealDataReq_Encode
** Descriptions:        c->s实时数据打包
** input parameters:    package_nub一共多少包 package_now 第几包 time 打包时间 MeterCount 电表数量
**                      RpCount 中继数量 RpOff 中继偏移 PvCount 组件数量  PvOff 组件偏移
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
#ifdef DTU3PRO
u32 RealDataReq_Encode(u8 package_nub, u8 package_now, u32 time,
                       u16 MeterCount, u16 MeterOff,
                       u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer)
#else
u32 RealDataReq_Encode(u8 package_nub, u8 package_now, u32 time,
                       u16 RpCount, u16 RpOff,
                       u16 PvCount, u16 PvOff, char *buffer)
#endif
{
    //RealDataReq
    bool pb_status;
    vu16  pb_len;
    vu16  i = 0;
    vu16  j = 0, k = 0;
    volatile int64_t sn = 0;
    pb_ostream_t pb_ostream;
    RealDataReqDTO *mRealDataReq = NULL;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(RealDataReqDTO);
    mRealDataReq = mymalloc(sizeof(RealDataReqDTO));
    //    if(mRealDataReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mRealDataReq, 0, sizeof(RealDataReqDTO));
    {
        mRealDataReq->dtu_sn.funcs.encode = encode_bytes;
        mRealDataReq->dtu_sn.arg = Strdtu_ID;
        Encodelen = Encodelen + sizeof(Strdtu_ID);
        mRealDataReq->time = (s32)time;
        /*总包数量*/
        mRealDataReq->package_nub = package_nub;
        /*当前包序*/
        mRealDataReq->package_now = package_now;
        /*MI数量*/
        mRealDataReq->device_nub = Dtu3Detail.Property.InverterNum;
        /*组件数量*/
        mRealDataReq->pv_nub = Dtu3Detail.Property.PortNum;
        //数据版本(默认从0开始,0表示微逆告警位置错误,1表示已修复)
        mRealDataReq->version = 1;
        //gprs信号强度
#ifdef DTU3PRO

        if(Netmode_Used == GPRS_MODE)
        {
            mRealDataReq->csq = Dtu3Major.Property.GPRS_CSQ;
        }
        else if(Netmode_Used == WIFI_MODE)
        {
            mRealDataReq->csq = Dtu3Major.Property.wifi_RSSI;
        }
        else
        {
            mRealDataReq->csq = 100;
        }

#else

        if(Dtu3Major.Property.netmode_select == GPRS_MODE)
        {
            mRealDataReq->csq = Dtu3Major.Property.GPRS_CSQ;
        }
        else if(Dtu3Major.Property.netmode_select == WIFI_MODE)
        {
            mRealDataReq->csq = Dtu3Major.Property.wifi_RSSI;
        }

#endif
#ifdef DTU3PRO
        mRealDataReq->mMeterData_count = MeterCount;

        for(i = 0; i < MeterCount; i++)
        {
            MeterInfo[i].Send_CNT = 0;
            mRealDataReq->mMeterData[i].device_kind = MeterInfo[i + MeterOff].phase;
            mRealDataReq->mMeterData[i].meter_sn = (int64_t)(((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(Meter[i + MeterOff].Property.Id[0]) << 24) + ((uint64_t)(Meter[i + MeterOff].Property.Id[1]) << 16) + ((uint64_t)(Meter[i + MeterOff].Property.Id[2]) << 8) + (uint64_t)(Meter[i + MeterOff].Property.Id[3]));
            //2位小数点 KW KWH
            mRealDataReq->mMeterData[i].meter_p_total = MeterInfo[i + MeterOff].power_total / 100;
            mRealDataReq->mMeterData[i].meter_p_tta = MeterInfo[i + MeterOff].power_a / 100;
            mRealDataReq->mMeterData[i].meter_p_ttb = MeterInfo[i + MeterOff].power_b / 100;
            mRealDataReq->mMeterData[i].meter_p_ttc = MeterInfo[i + MeterOff].power_c / 100;
            mRealDataReq->mMeterData[i].meter_factor = MeterInfo[i + MeterOff].factor / 100;
            mRealDataReq->mMeterData[i].meter_ep_total = MeterInfo[i + MeterOff].positive_energy / 10;
            mRealDataReq->mMeterData[i].meter_ep_tta = MeterInfo[i + MeterOff].positive_energy_a / 10;
            mRealDataReq->mMeterData[i].meter_ep_ttb = MeterInfo[i + MeterOff].positive_energy_b / 10;
            mRealDataReq->mMeterData[i].meter_ep_ttc = MeterInfo[i + MeterOff].positive_energy_c / 10;
            mRealDataReq->mMeterData[i].meter_en_total = MeterInfo[i + MeterOff].Reverse_energy / 10;
            mRealDataReq->mMeterData[i].meter_en_tta = MeterInfo[i + MeterOff].Reverse_energy_a / 10;
            mRealDataReq->mMeterData[i].meter_en_ttb = MeterInfo[i + MeterOff].Reverse_energy_b / 10;
            mRealDataReq->mMeterData[i].meter_en_ttc = MeterInfo[i + MeterOff].Reverse_energy_c / 10;
            mRealDataReq->mMeterData[i].meter_fault = MeterInfo[i + MeterOff].fault;
        }

#else
        mRealDataReq->mMeterData_count = 0;
#endif
        /*中继数据*/
        mRealDataReq->mRpDatas_count = RpCount;

        for(i = 0; i < RpCount; i++)
        {
            //rp-id号
            mRealDataReq->mRpDatas[i].rp_sn = (int64_t)(((uint64_t)(MIMajor[i + RpOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i + RpOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i + RpOff].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i + RpOff].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i + RpOff].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i + RpOff].Property.Id[3]));
            //中继 信号强度
            mRealDataReq->mRpDatas[i].rp_signal = 0;
            //中继通道号
            mRealDataReq->mRpDatas[i].rp_channel = 0;
            //中继带的组件个数
            mRealDataReq->mRpDatas[i].rp_link_nub = 0;
            //中继器连接状态
            mRealDataReq->mRpDatas[i].rp_link_status = 0;
        }

        if((APP_Flg == 1) && (AppState == PollSigleMi))
        {
            /*单个组件的数据 包括故障数据*/
            mRealDataReq->mpvDatas_count = 0;
            PvOff = 0;
            k = 0;

            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                if(sn == APPOperatSN)
                {
                    if(k == 0)
                    {
                        k = 1;
                        j = i;
                    }

                    //MI-id号
                    mRealDataReq->mpvDatas[i - j].pv_sn = (int64_t)(((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i + PvOff].Property.Id[3]));

                    //微逆 端口号
                    switch(MIMajor[i + PvOff].Property.Port)
                    {
                        case MI_NO:
                            mRealDataReq->mpvDatas[i - j].pv_port = 0;
                            break;

                        case MI_250W:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case MI_500W_A:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case MI_500W_B:
                            mRealDataReq->mpvDatas[i - j].pv_port = 2;
                            break;

                        case MI_1000W_A:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case MI_1000W_B:
                            mRealDataReq->mpvDatas[i - j].pv_port = 2;
                            break;

                        case MI_1000W_C:
                            mRealDataReq->mpvDatas[i - j].pv_port = 3;
                            break;

                        case MI_1000W_D:
                            mRealDataReq->mpvDatas[i - j].pv_port = 4;
                            break;

                        case Pro_A:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case Pro_B:
                            mRealDataReq->mpvDatas[i - j].pv_port = 2;
                            break;

                        case Pro_C:
                            mRealDataReq->mpvDatas[i - j].pv_port = 3;
                            break;

                        case Pro_D:
                            mRealDataReq->mpvDatas[i - j].pv_port = 4;
                            break;

                        case HM_250W:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case HM_500W_A:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case HM_500W_B:
                            mRealDataReq->mpvDatas[i - j].pv_port = 2;
                            break;

                        case HM_1000W_A:
                            mRealDataReq->mpvDatas[i - j].pv_port = 1;
                            break;

                        case HM_1000W_B:
                            mRealDataReq->mpvDatas[i - j].pv_port = 2;
                            break;

                        case HM_1000W_C:
                            mRealDataReq->mpvDatas[i - j].pv_port = 3;
                            break;

                        case HM_1000W_D:
                            mRealDataReq->mpvDatas[i - j].pv_port = 4;
                            break;

                        default :
                            mRealDataReq->mpvDatas[i - j].pv_port = 0;
                            break;
                    }

                    //MI PV 电压
                    mRealDataReq->mpvDatas[i - j].pv_vol            = ((u16)MIReal[i + PvOff].Data.PVVol[0] << 8) + MIReal[i + PvOff].Data.PVVol[1];
                    //MI PV 电流
                    mRealDataReq->mpvDatas[i - j].pv_cur            = ((u16)MIReal[i + PvOff].Data.PVCur[0] << 8) + MIReal[i + PvOff].Data.PVCur[1];

                    //MI PV 功率
                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i - j].pv_power      = ((u16)MIReal[i + PvOff].Data.PVPower[0] << 8) + MIReal[i + PvOff].Data.PVPower[1];
                    }
                    //二代微逆
                    else
                    {
                        mRealDataReq->mpvDatas[i - j].pv_power      = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                    }

                    //MI PV 当日发电量
                    mRealDataReq->mpvDatas[i - j].pv_energy         = MIReal[i + PvOff].Data.DailyEnergy;
                    //MI PV 历史累计发电量
                    mRealDataReq->mpvDatas[i - j].pv_energy_total   = (u32)(((u32)MIReal[i + PvOff].Data.HistoryEnergyH << 16) + MIReal[i + PvOff].Data.HistoryEnergyL);
                    //采集间隔交流电压最大值
                    mRealDataReq->mpvDatas[i - j].grid_vol_max      = 0;

                    //交流有功功率
                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i - j].grid_p        = ((u16)MIReal[i + PvOff].Data.GridActivePower[0] << 8) + MIReal[i + PvOff].Data.GridActivePower[1];
                    }
                    //二代微逆
                    else
                    {
                        if(PORT_NUMBER_CONFIRMATION(i + PvOff))
                        {
                            //微逆 端口号
                            switch(MIMajor[i + PvOff].Property.Port)
                            {
                                case MI_NO:
                                    mRealDataReq->mpvDatas[i - j].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;

                                case MI_250W:
                                    mRealDataReq->mpvDatas[i - j].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;

                                case MI_500W_A:
                                    mRealDataReq->mpvDatas[i - j].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1] +
                                            ((u16)MIReal[i + PvOff + 1].Data.Power[0] << 8) + MIReal[i + PvOff + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    mRealDataReq->mpvDatas[i - j].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1] +
                                            ((u16)MIReal[i + PvOff + 1].Data.Power[0] << 8) + MIReal[i + PvOff + 1].Data.Power[1] +
                                            ((u16)MIReal[i + PvOff + 2].Data.Power[0] << 8) + MIReal[i + PvOff + 2].Data.Power[1] +
                                            ((u16)MIReal[i + PvOff + 3].Data.Power[0] << 8) + MIReal[i + PvOff + 3].Data.Power[1];
                                    break;

                                default :
                                    mRealDataReq->mpvDatas[i - j].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;
                            }
                        }
                        else
                        {
                            mRealDataReq->mpvDatas[i - j].grid_p    = 0;
                        }
                    }

                    if(PORT_NUMBER_CONFIRMATION(i + PvOff))
                    {
                        //电网 电压
                        mRealDataReq->mpvDatas[i - j].grid_vol      = ((u16)MIReal[i + PvOff].Data.GridVol[0] << 8) + MIReal[i + PvOff].Data.GridVol[1];
                        //电网 频率
                        mRealDataReq->mpvDatas[i - j].grid_freq     = ((u16)MIReal[i + PvOff].Data.Freque[0] << 8) + MIReal[i + PvOff].Data.Freque[1];
                        //采集间隔交流电压最大值
                        mRealDataReq->mpvDatas[i - j].grid_vol_max  = 0;
                        //交流无功功率
                        mRealDataReq->mpvDatas[i - j].grid_q        = ((u16)MIReal[i + PvOff].Data.GridReactivePower[0] << 8) + MIReal[i + PvOff].Data.GridReactivePower[1];
                        //交流电流
                        mRealDataReq->mpvDatas[i - j].grid_i        = ((u16)MIReal[i + PvOff].Data.GridCurrent[0] << 8) + MIReal[i + PvOff].Data.GridCurrent[1];
                        //功率因数
                        mRealDataReq->mpvDatas[i - j].grid_pf       = ((u16)MIReal[i + PvOff].Data.PowerFactor[0] << 8) + MIReal[i + PvOff].Data.PowerFactor[1];
                    }
                    else
                    {
                        //电网 电压
                        mRealDataReq->mpvDatas[i - j].grid_vol      = 0;
                        //电网 频率
                        mRealDataReq->mpvDatas[i - j].grid_freq     = 0;
                        //采集间隔交流电压最大值
                        mRealDataReq->mpvDatas[i - j].grid_vol_max  = 0;
                        //交流无功功率
                        mRealDataReq->mpvDatas[i - j].grid_q        = 0;
                        //交流电流
                        mRealDataReq->mpvDatas[i - j].grid_i        = 0;
                        //功率因数
                        mRealDataReq->mpvDatas[i - j].grid_pf       = 0;
                    }

                    //温度
                    mRealDataReq->mpvDatas[i - j].pv_temp           = (int16_t)(((u16)MIReal[i + PvOff].Data.Temper[0] << 8) + (u16)MIReal[i + PvOff].Data.Temper[1]);

                    //运行状态
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i - j].pv_run_status     =  mRealDataReq->mpvDatas[i - j].pv_link_status == 1 ? 0x0003 : 0x0000;
                    }
                    else
                    {
                        mRealDataReq->mpvDatas[i - j].pv_run_status     = ((u16)MIReal[i + PvOff].Data.Run_Status[0] << 8) + MIReal[i + PvOff].Data.Run_Status[1];
                    }

                    //故障代码
                    mRealDataReq->mpvDatas[i - j].pv_fault_num      = ((u16)MIReal[i + PvOff].Data.Fault_Code[0] << 8) + MIReal[i + PvOff].Data.Fault_Code[1];
                    //故障次数
                    mRealDataReq->mpvDatas[i - j].pv_fault_cnt      = ((u16)MIReal[i + PvOff].Data.Fault_Num[0] << 8) + MIReal[i + PvOff].Data.Fault_Num[1];
                    //警告次数
                    mRealDataReq->mpvDatas[i - j].pv_warnning_cnt   = 0;

                    if(APP_Flg == 1)
                    {
                        //连接状态
                        mRealDataReq->mpvDatas[i - j].pv_link_status    = ((MIReal[i + PvOff].Data.LinkState) & MI_CONNECT) > 0 ? 1 : 0;
                    }
                    else
                    {
                        //连接状态
                        mRealDataReq->mpvDatas[i - j].pv_link_status    = ((MIReal[i + PvOff].Data.LinkState) & MI_SERVER_CONNECT) > 0 ? 1 : 0;
                    }//当天发送总数据帧(统计时段8:00-17:00)

                    //当天发送总数据帧(统计时段8:00-17:00)
                    mRealDataReq->mpvDatas[i - j].pv_send_p         = 0;
                    //当天接收总数据帧(统计时段8:00-17:00)
                    mRealDataReq->mpvDatas[i - j].pv_rev_p          = 0;
                    mRealDataReq->mpvDatas[i - j].pv_time           = (s32)MIReal[i + PvOff].Data.Real_Time;
                    mRealDataReq->mpvDatas_count ++;
                }
            }
        }
        else if(mi_zero_data_timter != 0)
        {
            /*单个组件的数据 包括故障数据*/
            mRealDataReq->mpvDatas_count = PvCount;

            for(i = 0; i < PvCount; i++)
            {
                if((i + PvOff) >= Dtu3Detail.Property.PortNum)
                {
                    mRealDataReq->mpvDatas_count = i - 1;
                    break;
                }

                //MI-id号
                mRealDataReq->mpvDatas[i].pv_sn = (int64_t)(((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i + PvOff].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i + PvOff].Property.Id[3]));

                //微逆 端口号
                switch(MIMajor[i + PvOff].Property.Port)
                {
                    case MI_NO:
                        mRealDataReq->mpvDatas[i].pv_port = 0;
                        break;

                    case MI_250W:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case MI_500W_A:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case MI_500W_B:
                        mRealDataReq->mpvDatas[i].pv_port = 2;
                        break;

                    case MI_1000W_A:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case MI_1000W_B:
                        mRealDataReq->mpvDatas[i].pv_port = 2;
                        break;

                    case MI_1000W_C:
                        mRealDataReq->mpvDatas[i].pv_port = 3;
                        break;

                    case MI_1000W_D:
                        mRealDataReq->mpvDatas[i].pv_port = 4;
                        break;

                    case Pro_A:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case Pro_B:
                        mRealDataReq->mpvDatas[i].pv_port = 2;
                        break;

                    case Pro_C:
                        mRealDataReq->mpvDatas[i].pv_port = 3;
                        break;

                    case Pro_D:
                        mRealDataReq->mpvDatas[i].pv_port = 4;
                        break;

                    case HM_250W:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case HM_500W_A:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case HM_500W_B:
                        mRealDataReq->mpvDatas[i].pv_port = 2;
                        break;

                    case HM_1000W_A:
                        mRealDataReq->mpvDatas[i].pv_port = 1;
                        break;

                    case HM_1000W_B:
                        mRealDataReq->mpvDatas[i].pv_port = 2;
                        break;

                    case HM_1000W_C:
                        mRealDataReq->mpvDatas[i].pv_port = 3;
                        break;

                    case HM_1000W_D:
                        mRealDataReq->mpvDatas[i].pv_port = 4;
                        break;

                    default :
                        mRealDataReq->mpvDatas[i].pv_port = 0;
                        break;
                }

                if(APP_Flg == 1)
                {
                    //连接状态
                    mRealDataReq->mpvDatas[i].pv_link_status    = ((MIReal[i + PvOff].Data.LinkState) & MI_CONNECT) > 0 ? 1 : 0;
                }
                else
                {
                    //连接状态
                    mRealDataReq->mpvDatas[i].pv_link_status    = ((MIReal[i + PvOff].Data.LinkState) & MI_SERVER_CONNECT) > 0 ? 1 : 0;
                }//当天发送总数据帧(统计时段8:00-17:00)

                if(((MIReal[i + PvOff].Data.LinkState) & MI_TOTAL_ENERGY_FAULT) == MI_TOTAL_ENERGY_FAULT)
                {
                    //MI PV 电压
                    mRealDataReq->mpvDatas[i].pv_vol                = 0;
                    //MI PV 电流
                    mRealDataReq->mpvDatas[i].pv_cur                = 0;
                    //MI PV 功率
                    //三代微逆
                    mRealDataReq->mpvDatas[i].pv_power              = 0;
                    //MI PV 当日发电量
                    mRealDataReq->mpvDatas[i].pv_energy             = 0;
                    //MI PV 历史累计发电量
                    mRealDataReq->mpvDatas[i].pv_energy_total       = 0;
                    //交流有功功率
                    mRealDataReq->mpvDatas[i].grid_p                = 0;
                    //电网 电压
                    mRealDataReq->mpvDatas[i].grid_vol              = 0;
                    //电网 频率
                    mRealDataReq->mpvDatas[i].grid_freq             = 0;
                    //采集间隔交流电压最大值
                    mRealDataReq->mpvDatas[i].grid_vol_max          = 0;
                    //交流无功功率
                    mRealDataReq->mpvDatas[i].grid_q                = 0;
                    //交流电流
                    mRealDataReq->mpvDatas[i].grid_i                = 0;
                    //功率因数
                    mRealDataReq->mpvDatas[i].grid_pf               = 0;
                    //温度
                    mRealDataReq->mpvDatas[i].pv_temp               = 0;
                    //故障代码
                    mRealDataReq->mpvDatas[i].pv_fault_num          = 0;
                    //故障次数
                    mRealDataReq->mpvDatas[i].pv_fault_cnt          = 0;
                    //警告次数
                    mRealDataReq->mpvDatas[i].pv_warnning_cnt       = 0;

                    //运行状态

                    //mRealDataReq->mpvDatas[i].pv_run_status       = ((u16)MIReal[i + PvOff].Data.Run_Status[0] << 8) + MIReal[i + PvOff].Data.Run_Status[1];
                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i].pv_run_status     =  mRealDataReq->mpvDatas[i].pv_link_status == 1 ? 0x0003 : 0x0000;
                    }
                    else
                    {
                        mRealDataReq->mpvDatas[i].pv_run_status     = ((u16)MIReal[i + PvOff].Data.Run_Status[0] << 8) + MIReal[i + PvOff].Data.Run_Status[1];
                    }

                    mRealDataReq->mpvDatas[i].pv_send_p             = 0;
                    //当天接收总数据帧(统计时段8:00-17:00)
                    mRealDataReq->mpvDatas[i].pv_rev_p              = 0;
                    mRealDataReq->mpvDatas[i].pv_time               = 0;
                }
                else
                {
                    //MI PV 电压
                    mRealDataReq->mpvDatas[i].pv_vol                = ((u16)MIReal[i + PvOff].Data.PVVol[0] << 8) + MIReal[i + PvOff].Data.PVVol[1];
                    //MI PV 电流
                    mRealDataReq->mpvDatas[i].pv_cur                = ((u16)MIReal[i + PvOff].Data.PVCur[0] << 8) + MIReal[i + PvOff].Data.PVCur[1];

                    //MI PV 功率
                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i].pv_power          = ((u16)MIReal[i + PvOff].Data.PVPower[0] << 8) + MIReal[i + PvOff].Data.PVPower[1];
                    }
                    //二代微逆
                    else
                    {
                        mRealDataReq->mpvDatas[i].pv_power          = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                    }

                    //MI PV 当日发电量
                    mRealDataReq->mpvDatas[i].pv_energy             = MIReal[i + PvOff].Data.DailyEnergy;
                    //MI PV 历史累计发电量
                    mRealDataReq->mpvDatas[i].pv_energy_total       = (u32)(((u32)MIReal[i + PvOff].Data.HistoryEnergyH << 16) + MIReal[i + PvOff].Data.HistoryEnergyL);
                    //交流有功功率

                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        if(PORT_NUMBER_CONFIRMATION(i + PvOff))
                        {
                            mRealDataReq->mpvDatas[i].grid_p        = ((u16)MIReal[i + PvOff].Data.GridActivePower[0] << 8) + MIReal[i + PvOff].Data.GridActivePower[1];
                        }
                        else
                        {
                            mRealDataReq->mpvDatas[i].grid_p        = 0;
                        }
                    }
                    //二代微逆
                    else
                    {
                        if(PORT_NUMBER_CONFIRMATION(i + PvOff))
                        {
                            //微逆 端口号
                            switch(MIMajor[i + PvOff].Property.Port)
                            {
                                case MI_NO:
                                    mRealDataReq->mpvDatas[i].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;

                                case MI_250W:
                                    mRealDataReq->mpvDatas[i].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;

                                case MI_500W_A:
                                    mRealDataReq->mpvDatas[i].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1] +
                                                                          ((u16)MIReal[i + PvOff + 1].Data.Power[0] << 8) + MIReal[i + PvOff + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    mRealDataReq->mpvDatas[i].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1] +
                                                                          ((u16)MIReal[i + PvOff + 1].Data.Power[0] << 8) + MIReal[i + PvOff + 1].Data.Power[1] +
                                                                          ((u16)MIReal[i + PvOff + 2].Data.Power[0] << 8) + MIReal[i + PvOff + 2].Data.Power[1] +
                                                                          ((u16)MIReal[i + PvOff + 3].Data.Power[0] << 8) + MIReal[i + PvOff + 3].Data.Power[1];
                                    break;

                                default :
                                    mRealDataReq->mpvDatas[i].grid_p    = ((u16)MIReal[i + PvOff].Data.Power[0] << 8) + MIReal[i + PvOff].Data.Power[1];
                                    break;
                            }
                        }
                        else
                        {
                            mRealDataReq->mpvDatas[i].grid_p            = 0;
                        }
                    }

                    if(PORT_NUMBER_CONFIRMATION(i + PvOff))
                    {
                        //电网 电压
                        mRealDataReq->mpvDatas[i].grid_vol              = ((u16)MIReal[i + PvOff].Data.GridVol[0] << 8) + MIReal[i + PvOff].Data.GridVol[1];
                        //电网 频率
                        mRealDataReq->mpvDatas[i].grid_freq             = ((u16)MIReal[i + PvOff].Data.Freque[0] << 8) + MIReal[i + PvOff].Data.Freque[1];
                        //采集间隔交流电压最大值
                        mRealDataReq->mpvDatas[i].grid_vol_max          = 0;
                        //交流无功功率
                        mRealDataReq->mpvDatas[i].grid_q                = ((u16)MIReal[i + PvOff].Data.GridReactivePower[0] << 8) + MIReal[i + PvOff].Data.GridReactivePower[1];
                        //交流电流
                        mRealDataReq->mpvDatas[i].grid_i                = ((u16)MIReal[i + PvOff].Data.GridCurrent[0] << 8) + MIReal[i + PvOff].Data.GridCurrent[1];
                        //功率因数
                        mRealDataReq->mpvDatas[i].grid_pf               = ((u16)MIReal[i + PvOff].Data.PowerFactor[0] << 8) + MIReal[i + PvOff].Data.PowerFactor[1];
                    }
                    else
                    {
                        //电网 电压
                        mRealDataReq->mpvDatas[i].grid_vol          = 0;
                        //电网 频率
                        mRealDataReq->mpvDatas[i].grid_freq         = 0;
                        //采集间隔交流电压最大值
                        mRealDataReq->mpvDatas[i].grid_vol_max      = 0;
                        //交流无功功率
                        mRealDataReq->mpvDatas[i].grid_q            = 0;
                        //交流电流
                        mRealDataReq->mpvDatas[i].grid_i            = 0;
                        //功率因数
                        mRealDataReq->mpvDatas[i].grid_pf           = 0;
                    }

                    //温度
                    mRealDataReq->mpvDatas[i].pv_temp               = (int16_t)(((u16)MIReal[i + PvOff].Data.Temper[0] << 8) + (u16)MIReal[i + PvOff].Data.Temper[1]);
                    //故障代码
                    mRealDataReq->mpvDatas[i].pv_fault_num          = ((u16)MIReal[i + PvOff].Data.Fault_Code[0] << 8) + MIReal[i + PvOff].Data.Fault_Code[1];
                    //故障次数
                    mRealDataReq->mpvDatas[i].pv_fault_cnt          = ((u16)MIReal[i + PvOff].Data.Fault_Num[0] << 8) + MIReal[i + PvOff].Data.Fault_Num[1];
                    //警告次数
                    mRealDataReq->mpvDatas[i].pv_warnning_cnt       = 0;

                    //运行状态

                    //mRealDataReq->mpvDatas[i].pv_run_status       = ((u16)MIReal[i + PvOff].Data.Run_Status[0] << 8) + MIReal[i + PvOff].Data.Run_Status[1];
                    //三代微逆
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i + PvOff].Property.Pre_Id) >= Inverter_Pro)
                    {
                        mRealDataReq->mpvDatas[i].pv_run_status     =  mRealDataReq->mpvDatas[i].pv_link_status == 1 ? 0x0003 : 0x0000;
                    }
                    else
                    {
                        mRealDataReq->mpvDatas[i].pv_run_status     = ((u16)MIReal[i + PvOff].Data.Run_Status[0] << 8) + MIReal[i + PvOff].Data.Run_Status[1];
                    }

                    mRealDataReq->mpvDatas[i].pv_send_p             = 0;
                    //当天接收总数据帧(统计时段8:00-17:00)
                    mRealDataReq->mpvDatas[i].pv_rev_p              = 0;
                    mRealDataReq->mpvDatas[i].pv_time               = (s32)MIReal[i + PvOff].Data.Real_Time;
                }
            }
        }
    }
    //发送端//把结构体的数据转换到发送缓存
    pb_ostream = pb_ostream_from_buffer((unsigned char *)buffer, Encodelen);
    pb_status = pb_encode(&pb_ostream, RealDataReqDTO_fields, mRealDataReq);
    pb_len = (u16)pb_ostream.bytes_written;
    myfree(mRealDataReq);
    return pb_len;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_RealDataRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    RealDataResDTO *mRealDataRes = NULL;
    mRealDataRes = mymalloc(sizeof(RealDataResDTO));
    //    if(mRealDataRes == NULL)
    //    {
    //        return 0;
    //    }
    memset(mRealDataRes, 0, sizeof(RealDataResDTO));
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, RealDataResDTO_fields, mRealDataRes);

    if(pb_status == 1)
    {
        GetPackageNow = (u8)mRealDataRes->package_now;
#ifdef DEBUG
        printf("mRealDataRes->offset %d\n", mRealDataRes->offset);
        printf("mRealDataRes->time %d\n", (u32)mRealDataRes->time);
        calendar_obj calendar;
        SecToDate((u32)mRealDataRes->time, (calendar_obj *)&calendar);
        printf("%04d-%02d-%02d %02d:%02d:%02d",
               calendar.w_year, calendar.w_month, calendar.w_date, calendar.hour, calendar.min, calendar.sec);
#endif
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mRealDataRes);
    return ReturnNum;
}

/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_CommandRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    CommandResDTO *mCommandResDTO = NULL;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, 14);
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mCommandResDTO = mymalloc(sizeof(CommandResDTO));
    //    if(mCommandResDTO == NULL)
    //    {
    //        return 0;
    //    }
    memset(mCommandResDTO, 0, sizeof(CommandResDTO));
    {
        /*命令内容*/
        mCommandResDTO->data.funcs.decode = decode_bytes;
        mCommandResDTO->data.arg = mymalloc(2048);
        //        if(mCommandResDTO->data.arg == NULL)
        //        {
        //            myfree(mCommandResDTO);
        //            return 0;
        //        }
        memset(mCommandResDTO->data.arg, 0, 2048);
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, CommandResDTO_fields, mCommandResDTO);

    if(pb_status == 1)
    {
        //if(strncmp(mCommandResDTO->dtu_sn.arg, Strdtu_ID, 12) == 0)
        {
#ifdef DEBUG
            //printf("mCommandResDTO->offset %d\n", mCommandResDTO->offset);
            printf("mCommandResDTO->time %d\n", (u32)mCommandResDTO->time);
            calendar_obj calendar;
            SecToDate((u32)mCommandResDTO->time, (calendar_obj *)&calendar);
            printf("%04d-%02d-%02d %02d:%02d:%02d",
                   calendar.w_year, calendar.w_month, calendar.w_date, calendar.hour, calendar.min, calendar.sec);
#endif
            //       mCommandResDTO->action
            //       mCommandResDTO->dev_kind
            package_Num = (u8)mCommandResDTO->package_nub;
            GetPackageNow = (u8)mCommandResDTO->package_now;
            //       mCommandResDTO->to_sn_count
            //       mCommandResDTO->to_sn[0]
            //       mCommandResDTO->tid
            tid = mCommandResDTO->tid;
            NetCmdAnalysis(&mCommandResDTO->action, &mCommandResDTO->dev_kind,  mCommandResDTO->mi_to_sn_count, mCommandResDTO->mi_to_sn,
                           mCommandResDTO->system_total_a, mCommandResDTO->system_total_b, mCommandResDTO->system_total_c,
                           mCommandResDTO->mi_sn_item_a_count, mCommandResDTO->mi_sn_item_a, mCommandResDTO->mi_sn_item_b_count, mCommandResDTO->mi_sn_item_b,
                           mCommandResDTO->mi_sn_item_c_count, mCommandResDTO->mi_sn_item_c, (char *)mCommandResDTO->data.arg,
                           (u8)mCommandResDTO->package_nub, (u8)mCommandResDTO->package_now);
        }
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mCommandResDTO->data.arg);
    myfree(mCommandResDTO);
    return ReturnNum;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_CommandReq(u16 TAG, char *buffer, u32 package_now)
{
    vu16  len = 0;
    CommandReqDTO *mCommandReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(CommandReqDTO);
    calendar_obj calendar;
    mCommandReq = mymalloc(sizeof(CommandReqDTO));
    //    if(mCommandReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mCommandReq, 0, sizeof(CommandReqDTO));
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        mCommandReq->dtu_sn.funcs.encode = encode_bytes;
        mCommandReq->dtu_sn.arg = Strdtu_ID;
        mCommandReq->time = (s32)RTC_Getsecond();
        mCommandReq->err_code = err_code;
        mCommandReq->action = Commandback;
        mCommandReq->package_now = (s32)package_now;
        mCommandReq->tid = tid;
        RTC_GetTimes(&calendar);
#ifdef DEBUG
        printf("package_now %d\n", package_now);
#endif
    }
    Net_ProtobufEncode(TAG, ServerSeq, buffer, (u16 *)&len, Encodelen, CommandReqDTO_fields, mCommandReq);
    err_code = 0;
    myfree(mCommandReq);
    return len;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_CommandStatusReq(u16 TAG, char *buffer, u8 package_nub, u8 package_now)
{
    vu16  i = 0;
    vu16  size = 0;
    vu16  len = 0;
    volatile int64_t sn = 0;
    CommandStatusReqDTO *mCommandStatusReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(CommandStatusReqDTO);
    calendar_obj calendar;
    mCommandStatusReq = mymalloc(sizeof(CommandStatusReqDTO));
    //    if(mCommandStatusReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mCommandStatusReq, 0, sizeof(CommandStatusReqDTO));
    char Strdtu_ID[14];
    DTUSeq++;
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        mCommandStatusReq->dtu_sn.funcs.encode = encode_bytes;
        mCommandStatusReq->dtu_sn.arg = Strdtu_ID;
        Encodelen = Encodelen + sizeof(Strdtu_ID);
        RTC_GetTimes(&calendar);
        mCommandStatusReq->time = (s32)RTC_Getsecond();
        //命令字
        mCommandStatusReq->action = Commandback;
        mCommandStatusReq->tid = tid;
        mCommandStatusReq->mi_sns_sucs_count = 0;
        mCommandStatusReq->mi_sns_sucs[0] = 0;
        mCommandStatusReq->mi_sns_failds_count = 0;
        mCommandStatusReq->mi_sns_failds[0] = 0;
        mCommandStatusReq->mi_mOperatingStatus_count = 0;
        mCommandStatusReq->mi_mOperatingStatus[0].mi_sn = 0;
        mCommandStatusReq->mi_mOperatingStatus[0].progress_rate = 0;
        mCommandStatusReq->package_nub = package_nub;
        mCommandStatusReq->package_now = package_now;

        if((Dtu3Detail.Property.PortNum / 100) < package_now)
        {
            myfree(mCommandStatusReq);
            return 0;
        }
        else
        {
            i = package_now * 100;
            size = (package_now + 1) * 100;

            if(size > Dtu3Detail.Property.PortNum)
            {
                size = Dtu3Detail.Property.PortNum;
            }
        }

        if(APP_Flg == 1)
        {
            switch(AppState)
            {
                /*单个微逆扫描*/
                case PollSigleMi:
                    {
                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                //if(sn == APPOperatSN)
                                {
                                    /*有网络命令执行失败*/
                                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    /*有网络命令未执行*/
                                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    else
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                }
                            }
                        }
                    }
                    break;

                /*多个微逆扫描*/
                case PollAllMi:
                    {
                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                //if(sn == APPOperatSN)
                                {
                                    /*有网络命令执行失败*/
                                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    /*有网络命令未执行*/
                                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    else
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                }
                            }
                        }
                    }
                    break;

                /*多个丢包率统计*/
                case AllMiLossRate:
                    {
                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                //if(sn == APPOperatSN)
                                {
                                    /*有网络命令执行失败*/
                                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    /*有网络命令未执行*/
                                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    else
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                }
                            }
                        }
                    }
                    break;

                /*单个丢包率统计*/
                case ManuSigleLossRate:
                    {
                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                                if(sn == APPOperatSN)
                                {
                                    /*有网络命令执行失败*/
                                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->tid = MIReal[i].Data.Real_Time;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    /*有网络命令未执行*/
                                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->tid = MIReal[i].Data.Real_Time;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    else
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                                        mCommandStatusReq->tid = MIReal[i].Data.Real_Time;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                }
                            }
                        }
                    }
                    break;

                /*网络命令模式*/
                case OtherNetCmdState:
                    {
                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                //if(sn == APPOperatSN)
                                {
                                    /*有网络命令执行失败*/
                                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    /*有网络命令未执行*/
                                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                    else
                                    {
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                                        mCommandStatusReq->mi_mOperatingStatus_count++;
                                    }
                                }
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        }
        else
        {
            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(PORT_NUMBER_CONFIRMATION(i))
                {
                    /*有网络命令执行失败*/
                    if(MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE)
                    {
                        mCommandStatusReq->mi_sns_failds[mCommandStatusReq->mi_sns_failds_count] = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                        mCommandStatusReq->mi_sns_failds_count ++;
                    }
                    /*有网络命令未执行*/
                    else if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                    {
                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                        if(MIReal[i].Data.NetStatus == NET_NOT_EXECUTED)
                        {
                            mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = 0;
                        }

                        mCommandStatusReq->mi_mOperatingStatus_count++;
                    }
                    /*有网络命令执行完成*/
                    else if(MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED)
                    {
                        mCommandStatusReq->mi_sns_sucs[mCommandStatusReq->mi_sns_sucs_count] = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                        mCommandStatusReq->mi_sns_sucs_count ++;
                    }
                    /*有网络命令正在执行*/
                    else if((MIReal[i].Data.NetStatus > NET_NOCMD) && (MIReal[i].Data.NetStatus < NET_EXECUTION_COMPLETED))
                    {
                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].mi_sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));
                        mCommandStatusReq->mi_mOperatingStatus[mCommandStatusReq->mi_mOperatingStatus_count].progress_rate = MIReal[i].Data.NetStatus;
                        mCommandStatusReq->mi_mOperatingStatus_count++;
                    }
                }
            }
        }
    }
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, CommandStatusReqDTO_fields, mCommandStatusReq);
    myfree(mCommandStatusReq);
    return len;
}
/***********************************************
** Function name:       Decode_CommandStatusResDTO
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_CommandStatusResDTO(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    CommandStatusResDTO *mCommandStatusResDTO = NULL;
    mCommandStatusResDTO = mymalloc(sizeof(CommandStatusResDTO));
    //    if(mCommandStatusResDTO == NULL)
    //    {
    //        return 0;
    //    }
    memset(mCommandStatusResDTO, 0, sizeof(CommandStatusResDTO));
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, CommandStatusResDTO_fields, mCommandStatusResDTO);

    if(pb_status == 1)
    {
        //       GetPackageNow = mCommandStatusResDTO->package_now;
#ifdef DEBUG
        printf("mCommandStatusResDTO->action %d\n", mCommandStatusResDTO->action);
        printf("mCommandStatusResDTO->time %d\n", mCommandStatusResDTO->time);
        printf("mCommandStatusResDTO->err_code %d\n", mCommandStatusResDTO->err_code);
        printf("GetPackageNow %d\n", GetPackageNow);
        printf("mCommandStatusResDTO->tid %d%d\n", (u32)(mCommandStatusResDTO->tid / 100000), (u32)(mCommandStatusResDTO->tid % 100000));
#endif
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mCommandStatusResDTO);
    return ReturnNum;
}
/***********************************************
** Function name:       Encode_DevConfigFetchReqDTO
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_DevConfigFetchReqDTO(u16 TAG, char *buffer, u8 *package_nub, u8 package_now)
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 APN[40] = {0};
    vu16  len = 0;
    DevConfigFetchReqDTO *mDevConfigFetchReqDTO;
    vu16  Encodelen = 0;
    char *DataBuf = NULL;
    char *CfgDataBuf = NULL;
    vu32 DataBufLen = 0;
    vu32 CfgDataBufLen = 0;
    Encodelen = Encodelen + sizeof(DevConfigFetchReqDTO);
    mDevConfigFetchReqDTO = mymalloc(sizeof(DevConfigFetchReqDTO));
    //    if(mDevConfigFetchReqDTO == NULL)
    //    {
    //        myfree(mDevConfigFetchReqDTO);
    //        myfree(DataBuf);
    //        myfree(CfgDataBuf);
    //        return 0;
    //    }
    memset(mDevConfigFetchReqDTO, 0, sizeof(DevConfigFetchReqDTO));
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        //DTU配置
        if(strncmp((char *)operate_sn, Strdtu_ID, 12) == 0)
        {
            if((Dtu3Major.Property.rule_id == 0) || (Dtu3Major.Property.rule_id == 0xFF))
            {
                set_rule_id();
            }

            DataBufLen = 1024;
            DataBuf = mymalloc(DataBufLen);
            //            if(DataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(DataBuf, 0, DataBufLen);

            if(strlen((char *)Dtu3Major.Property.wifi_ssid) == 0)
            {
                memcpy(&DataBuf[wifi_ssid_location], "Hoymiles", sizeof("Hoymiles"));
                memcpy(&DataBuf[wifi_passward_location], "Hoymiles", sizeof("Hoymiles"));
            }
            else
            {
                memcpy(&DataBuf[wifi_ssid_location], (u8 *)Dtu3Major.Property.wifi_ssid, wifi_ssid_length);
                memcpy(&DataBuf[wifi_passward_location], (u8 *)Dtu3Major.Property.wifi_passward, wifi_passward_length);
            }

            if(strlen((char *)Dtu3Major.Property.APN) == 0)
            {
                memcpy(&DataBuf[APN_location], "CMNET", sizeof("CMNET"));
            }
            else
            {
                memset((u8 *)APN, 0, sizeof(APN));
                memcpy((u8 *)APN, (u8 *)Dtu3Major.Property.APN, sizeof(Dtu3Major.Property.APN));
                memcpy((u8 *)&APN[sizeof(Dtu3Major.Property.APN)], (u8 *)Dtu3Major.Property.APN2, sizeof(Dtu3Major.Property.APN2));
                memcpy(&DataBuf[APN_location], (u8 *)APN, APN_length);
            }

            memcpy(&DataBuf[ServerDomainName_location], (u8 *)Dtu3Major.Property.ServerDomainName, ServerDomainName_length);
            DataBuf[ServerPort_location + 1] = (char)(Dtu3Major.Property.ServerPort / 256);
            DataBuf[ServerPort_location] = (char)(Dtu3Major.Property.ServerPort % 256);
            DataBuf[Server_send_time_location] = (char)Dtu3Major.Property.server_send_time;
            DataBuf[netmode_select_location] = Dtu3Major.Property.netmode_select;
            CfgDataBufLen = DTU_Cfg_Data_Get_Length();
            CfgDataBuf = mymalloc(CfgDataBufLen);
            //            if(CfgDataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(CfgDataBuf, 0, CfgDataBufLen);
            DTU_Cfg_Data_Read(CfgDataBuf, CfgDataBufLen, 0);
            //规则编号
            mDevConfigFetchReqDTO->rule_id          = (s32)Dtu3Major.Property.rule_id;
        }
        //微逆基本配置
        else if(rule_type == 0)
        {
            DataBufLen = MI_Data_Get_Length();
            DataBuf = mymalloc(DataBufLen);
            //            if(DataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(DataBuf, 0, DataBufLen);
            MI_Data_Read(DataBuf, DataBufLen, 0);
            CfgDataBufLen = MI_Cfg_Data_Get_Length();
            CfgDataBuf = mymalloc(CfgDataBufLen);
            //            if(CfgDataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(CfgDataBuf, 0, CfgDataBufLen);
            MI_Cfg_Data_Read(CfgDataBuf, CfgDataBufLen, 0);
            //规则编号
            mDevConfigFetchReqDTO->rule_id          = (s32)rule_id;
        }
        //微逆并网保护文件
        else if(rule_type == 1)
        {
            DataBufLen = Grid_Profiles_Data_Get_Length();
            DataBuf = mymalloc(DataBufLen);
            //            if(DataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(DataBuf, 0, DataBufLen);
            Grid_Profiles_Data_Read(DataBuf, DataBufLen, 0);
            CfgDataBufLen = Grid_Profiles_Cfg_Data_Get_Length();
            CfgDataBuf = mymalloc(CfgDataBufLen);
            //            if(CfgDataBuf == NULL)
            //            {
            //                myfree(mDevConfigFetchReqDTO);
            //                myfree(DataBuf);
            //                myfree(CfgDataBuf);
            //                return 0;
            //            }
            memset(CfgDataBuf, 0, CfgDataBufLen);
            Grid_Profiles_Cfg_Data_Read(CfgDataBuf, CfgDataBufLen, 0);
            //规则编号
            mDevConfigFetchReqDTO->rule_id          = (s32)rule_id;
        }

        if(package_now == 0)
        {
            data_len = 0;
            cfg_data_len = 0;

            for(i = 0; i < CacheMax; i++)
            {
                CommunicationCache[i].len           = 0;
                CommunicationCache[i].cfg_len       = 0;
            }

            *package_nub = (u8)((DataBufLen + CfgDataBufLen) / Data_Size);

            if((DataBufLen + CfgDataBufLen) % Data_Size)
            {
                *package_nub                        = *package_nub + 1;
            }

            for(i = 1; i < ((DataBufLen) / Data_Size) + 1; i++)
            {
                CommunicationCache[i].len           = Data_Size;
            }

            if((s32)((s32)DataBufLen - (s32)(Data_Size * (i - 1))) > 0)
            {
                CommunicationCache[i].len           = DataBufLen - Data_Size * (i - 1);
            }

            CommunicationCache[i].cfg_len = Data_Size - CommunicationCache[i].len;

            if(CommunicationCache[i].cfg_len >= CfgDataBufLen)
            {
                CommunicationCache[i].cfg_len = CfgDataBufLen;
            }
            else
            {
                for(j = i + 1; j < (i + (CfgDataBufLen) / Data_Size); j++)
                {
                    CommunicationCache[i].cfg_len   = Data_Size;
                }

                if((s32)((s32)CfgDataBufLen - (s32)(Data_Size * (j - (i + 1)) + CommunicationCache[i].cfg_len)) > 0)
                {
                    CommunicationCache[j].len       = CfgDataBufLen - ((Data_Size * (j - (i + 1)) + CommunicationCache[i].cfg_len));
                }
            }
        }

        //时间撮(秒)
        mDevConfigFetchReqDTO->time                 = (s32)RTC_Getsecond();
        //事务ID
        mDevConfigFetchReqDTO->tid                  = tid;

        //数据
        for(i = 0; i < package_now; i++)
        {
            data_len = data_len + CommunicationCache[package_now].len;
            cfg_data_len = cfg_data_len + CommunicationCache[package_now].cfg_len;
        }

        memset(mDevConfigFetchReqDTO->data.bytes, 0, sizeof(mDevConfigFetchReqDTO->data.bytes));
        memcpy(mDevConfigFetchReqDTO->data.bytes, &DataBuf[data_len], CommunicationCache[package_now + 1].len);
        mDevConfigFetchReqDTO->data.size = (pb_size_t)(CommunicationCache[package_now + 1].len);
        //数据CRC校验（总包校验）
        mDevConfigFetchReqDTO->crc                  = (s32)MyCrc16((unsigned char *)DataBuf, DataBufLen);
        //DTU SN
        mDevConfigFetchReqDTO->dtu_sn.funcs.encode  = encode_bytes;
        mDevConfigFetchReqDTO->dtu_sn.arg = Strdtu_ID;
        //设备SN
        mDevConfigFetchReqDTO->dev_sn.funcs.encode  = encode_bytes;
        mDevConfigFetchReqDTO->dev_sn.arg           = (char *)operate_sn;
        //配置
        memset(mDevConfigFetchReqDTO->cfg_data.bytes, 0, sizeof(mDevConfigFetchReqDTO->cfg_data.bytes));
        memcpy(mDevConfigFetchReqDTO->cfg_data.bytes, &CfgDataBuf[cfg_data_len], CommunicationCache[package_now + 1].cfg_len);
        mDevConfigFetchReqDTO->cfg_data.size = (pb_size_t)CommunicationCache[package_now + 1].cfg_len;
        //配置CRC校验（总包校验）
        mDevConfigFetchReqDTO->cfg_crc              = (s32)MyCrc16((unsigned char *)CfgDataBuf, CfgDataBufLen);
        //总包数量
        mDevConfigFetchReqDTO->package_nub          = *package_nub;
        //当前包序 从0开始计数
        mDevConfigFetchReqDTO->package_now          = package_now;
        mDevConfigFetchReqDTO->rule_type            = rule_type;
    }
    Net_ProtobufEncode(TAG, ServerSeq, buffer, (u16 *)&len, Encodelen, DevConfigFetchReqDTO_fields, mDevConfigFetchReqDTO);
    myfree(mDevConfigFetchReqDTO);
    myfree(DataBuf);
    myfree(CfgDataBuf);
    return len;
}
/***********************************************
** Function name:       Decode_DevConfigFetchResDTO
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_DevConfigFetchResDTO(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    DevConfigFetchResDTO *mDevConfigFetchResDTO = NULL;
    mDevConfigFetchResDTO = mymalloc(sizeof(DevConfigFetchResDTO));
    //    if(mDevConfigFetchResDTO == NULL)
    //    {
    //        return 0;
    //    }
    memset(mDevConfigFetchResDTO, 0, sizeof(DevConfigFetchResDTO));
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    memset((char *)operate_sn, 0, sizeof(operate_sn));
    {
        //DTU SN
        mDevConfigFetchResDTO->dtu_sn.funcs.decode  = decode_bytes;
        mDevConfigFetchResDTO->dtu_sn.arg = Strdtu_ID;
        //设备SN
        mDevConfigFetchResDTO->dev_sn.funcs.decode  = decode_bytes;
        mDevConfigFetchResDTO->dev_sn.arg = (char *)operate_sn;
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, DevConfigFetchResDTO_fields, mDevConfigFetchResDTO);

    if(pb_status == 1)
    {
        //事务id
        tid = mDevConfigFetchResDTO->tid;
        GetPackageNow = (u8)mDevConfigFetchResDTO->package_now;
        rule_type = (u16)mDevConfigFetchResDTO->rule_type;
#ifdef DEBUG
        printf("mDevConfigFetchResDTO->tid %d\n", (u32)mDevConfigFetchResDTO->tid);
        printf("mDevConfigFetchResDTO->dtu_sn %s\n", mDevConfigFetchResDTO->dtu_sn.arg);
        printf("mDevConfigFetchResDTO->dev_sn %s\n", mDevConfigFetchResDTO->dev_sn.arg);
        printf("mDevConfigFetchResDTO->package_now %d\n", mDevConfigFetchResDTO->package_now);
        printf("operate_sn %s\n", operate_sn);
#endif
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mDevConfigFetchResDTO->dev_sn.arg);
    myfree(mDevConfigFetchResDTO);
    return ReturnNum;
}
/***********************************************
** Function name:       Encode_DevConfigPutReqDTO
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_DevConfigPutReqDTO(u16 TAG, char *buffer, u8 Status, u8 package_now)
{
    vu16  len = 0;
    DevConfigPutReqDTO *mDevConfigPutReqDTO = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(DevConfigPutReqDTO);
    mDevConfigPutReqDTO = mymalloc(sizeof(DevConfigPutReqDTO));
    //    if(mDevConfigPutReqDTO == NULL)
    //    {
    //        return 0;
    //    }
    memset(mDevConfigPutReqDTO, 0, sizeof(DevConfigPutReqDTO));
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        //时间撮（秒)
        mDevConfigPutReqDTO->time                   = (s32)RTC_Getsecond();
        //事务ID
        mDevConfigPutReqDTO->tid                    = tid;
        //DTU SN
        mDevConfigPutReqDTO->dtu_sn.funcs.encode    = encode_bytes;
        mDevConfigPutReqDTO->dtu_sn.arg = Strdtu_ID;
        //设备SN
        mDevConfigPutReqDTO->dev_sn.funcs.encode    = encode_bytes;
        mDevConfigPutReqDTO->dev_sn.arg = (char *)operate_sn;
        //0 成功 1 失败
        mDevConfigPutReqDTO->status                 = status;
        //当前包序
        mDevConfigPutReqDTO->package_now            = package_now;
        //单/多微逆控制
        mDevConfigPutReqDTO->mi_to_sn_count         = 0;
        mDevConfigPutReqDTO->mi_to_sn[0]            = 0;
    }
    Net_ProtobufEncode(TAG, ServerSeq, buffer, (u16 *)&len, Encodelen, DevConfigPutReqDTO_fields, mDevConfigPutReqDTO);
    myfree(mDevConfigPutReqDTO);
    return len;
}
/***********************************************
** Function name:       Decode_DevConfigPutResDTO
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_DevConfigPutResDTO(char *buffer, u16 *pb_len)
{
    u8 APN[40] = {0};
    vu32 D_len = 0;
    vu32 C_len = 0;
    vu8 m = 0;
    vu8 change = 0;
    char Strdtu_ID[14];
    vu8 ReturnNum;
    vu16  bufflen = 50;
    bool pb_status;
    pb_istream_t pb_istream;
    DevConfigPutResDTO *mDevConfigPutResDTO;
    vu16  size = 1024;
    vu16  crc;
    vu32 i = 0;
    vu32 k = 0;
    volatile int64_t sn = 0;
    vu8 Dev_sn[6];
    volatile int64_t mi_sn = 0;
    char *DataBuf = NULL;
    char *CfgDataBuf = NULL;
    DataBuf = mymalloc(size);
    //    if(DataBuf == NULL)
    //    {
    //        myfree(mDevConfigPutResDTO->dtu_sn.arg);
    //        myfree(mDevConfigPutResDTO->dev_sn.arg);
    //        myfree(mDevConfigPutResDTO);
    //        myfree(DataBuf);
    //        myfree(CfgDataBuf);
    //        return 0;
    //    }
    memset(DataBuf, 0, size);
    CfgDataBuf = mymalloc(size);
    //    if(CfgDataBuf == NULL)
    //    {
    //        myfree(mDevConfigPutResDTO->dtu_sn.arg);
    //        myfree(mDevConfigPutResDTO->dev_sn.arg);
    //        myfree(mDevConfigPutResDTO);
    //        myfree(DataBuf);
    //        myfree(CfgDataBuf);
    //        return 0;
    //    }
    memset(CfgDataBuf, 0, size);
    mDevConfigPutResDTO = mymalloc(sizeof(DevConfigPutResDTO));
    //    if(mDevConfigPutResDTO == NULL)
    //    {
    //        myfree(mDevConfigPutResDTO->dtu_sn.arg);
    //        myfree(mDevConfigPutResDTO->dev_sn.arg);
    //        myfree(mDevConfigPutResDTO);
    //        myfree(DataBuf);
    //        myfree(CfgDataBuf);
    //        return 0;
    //    }
    memset(mDevConfigPutResDTO, 0, sizeof(DevConfigPutResDTO));
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    memset((char *)operate_sn, 0, sizeof(operate_sn));
    {
        mDevConfigPutResDTO->dtu_sn.funcs.decode = decode_bytes;
        mDevConfigPutResDTO->dtu_sn.arg = mymalloc(bufflen);
        //        if(mDevConfigPutResDTO->dtu_sn.arg == NULL)
        //        {
        //            myfree(mDevConfigPutResDTO->dtu_sn.arg);
        //            myfree(mDevConfigPutResDTO->dev_sn.arg);
        //            myfree(mDevConfigPutResDTO);
        //            myfree(DataBuf);
        //            myfree(CfgDataBuf);
        //            return 0;
        //        }
        memset(mDevConfigPutResDTO->dtu_sn.arg, 0, bufflen);
        mDevConfigPutResDTO->dev_sn.funcs.decode = decode_bytes;
        mDevConfigPutResDTO->dev_sn.arg = (char *)operate_sn;
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, DevConfigPutResDTO_fields, mDevConfigPutResDTO);

    if(pb_status == 1)
    {
        MemoryTime = LocalTime;
        rule_type = (u16)mDevConfigPutResDTO->rule_type;
        package_Num = (u8)mDevConfigPutResDTO->package_nub;
        GetPackageNow = (u8)mDevConfigPutResDTO->package_now;
        //事务id
        tid = mDevConfigPutResDTO->tid;
        //规则编号
        rule_id = (u32)mDevConfigPutResDTO->rule_id;
        mDevConfigPutResDTO->time = (s32)RTC_Getsecond();

        //配置文件
        if(mDevConfigPutResDTO->package_now == 0)
        {
            for(m = 0; m < CacheMax; m++)
            {
                CommunicationCache[m].len = 0;
                CommunicationCache[m].cfg_len = 0;
            }

            D_len = 0;
            C_len = 0;

            if(data == NULL)
            {
                data = mymalloc(4096);
                //                if(data == NULL)
                //                {
                //                    myfree(mDevConfigPutResDTO->dtu_sn.arg);
                //                    myfree(mDevConfigPutResDTO->dev_sn.arg);
                //                    myfree(mDevConfigPutResDTO);
                //                    myfree(DataBuf);
                //                    myfree(CfgDataBuf);
                //                    return 0;
                //                }
                memset(data, 0, 4096);
                data_being = 1;
            }

            if(cfg_data == NULL)
            {
                cfg_data = mymalloc(4096);
                //                if(cfg_data == NULL)
                //                {
                //                    myfree(mDevConfigPutResDTO->dtu_sn.arg);
                //                    myfree(mDevConfigPutResDTO->dev_sn.arg);
                //                    myfree(mDevConfigPutResDTO);
                //                    myfree(DataBuf);
                //                    myfree(CfgDataBuf);
                //                    return 0;
                //                }
                memset(cfg_data, 0, 4096);
                cfg_data_being = 1;
            }
        }

        for(m = 0; m < mDevConfigPutResDTO->package_now; m++)
        {
            D_len = D_len + CommunicationCache[m].len;
            C_len = C_len + CommunicationCache[m].cfg_len;
        }

        memcpy(&data[D_len], mDevConfigPutResDTO->data.bytes, mDevConfigPutResDTO->data.size);
        memcpy(&cfg_data[C_len], mDevConfigPutResDTO->cfg_data.bytes, mDevConfigPutResDTO->cfg_data.size);
        CommunicationCache[mDevConfigPutResDTO->package_now].len = mDevConfigPutResDTO->data.size;
        CommunicationCache[mDevConfigPutResDTO->package_now].cfg_len = mDevConfigPutResDTO->cfg_data.size;

        if((mDevConfigPutResDTO->package_now + 1) == mDevConfigPutResDTO->package_nub)
        {
            D_len = D_len + CommunicationCache[m].len;
            C_len = C_len + CommunicationCache[m].cfg_len;

            //DTU配置
            if(strncmp((char *)operate_sn, Strdtu_ID, 12) == 0)
            {
                if((u16)mDevConfigPutResDTO->crc == MyCrc16(data, D_len))
                {
                    memset((u8 *)Dtu3Major.Property.wifi_ssid, 0, sizeof(Dtu3Major.Property.wifi_ssid));
                    memcpy((u8 *)Dtu3Major.Property.wifi_ssid, &data[wifi_ssid_location], wifi_ssid_length);
                    memset((u8 *)Dtu3Major.Property.wifi_passward, 0, sizeof(Dtu3Major.Property.wifi_passward));
                    memcpy((u8 *)Dtu3Major.Property.wifi_passward, &data[wifi_passward_location], wifi_passward_length);
                    memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
                    memcpy((u8 *)Dtu3Major.Property.ServerDomainName, &data[ServerDomainName_location], ServerDomainName_length);
                    Dtu3Major.Property.ServerPort = (u16)(data[ServerPort_location + 1] * 256 + data[ServerPort_location]);
                    Dtu3Major.Property.server_send_time = data[Server_send_time_location];
                    memset((u8 *)APN, 0, sizeof(APN));
                    memcpy((u8 *)APN, &data[APN_location], APN_length);
                    memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
                    memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));
                    memcpy((u8 *)Dtu3Major.Property.APN, APN, sizeof(Dtu3Major.Property.APN));
                    memcpy((u8 *)Dtu3Major.Property.APN2, &APN[sizeof(Dtu3Major.Property.APN)], sizeof(Dtu3Major.Property.APN2));
                    Dtu3Major.Property.rule_id = (u8)mDevConfigPutResDTO->rule_id;
#ifdef DTU3PRO

                    if(Dtu3Major.Property.netmode_select != data[netmode_select_location])
                    {
                        Dtu3Major.Property.netmode_select = data[netmode_select_location];
                        Network_Change = 1;
                        Network_Change_Wait = LocalTime;
                    }

                    Network_Mode_Check(0, 0);
#endif
                    Dtu3Major.Property.wifi_ssid_save_set = 1;
                    //System_DtuMajor_Write(&Dtu3Major);
                    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                }

                if(mDevConfigPutResDTO->cfg_crc == (s32)MyCrc16(cfg_data, C_len))
                {
                    DTU_Cfg_Data_Delete();
                    DTU_Cfg_Data_Write((char *)cfg_data, C_len, 0);
                }
            }
            //微逆基本配置
            else if(rule_type == 0)
            {
                if(mDevConfigPutResDTO->crc == (s32)MyCrc16(data, D_len))
                {
                    MI_Data_Delete();
                    MI_Data_Write((char *)data, D_len, 0);
                }

                if(mDevConfigPutResDTO->cfg_crc == (s32)MyCrc16(cfg_data, C_len))
                {
                    MI_Cfg_Data_Delete();
                    MI_Cfg_Data_Write((char *)cfg_data, C_len, 0);
                }
            }
            //微逆并网保护文件
            else if(rule_type == 1)
            {
                if(mDevConfigPutResDTO->crc == (s32)MyCrc16(data, D_len))
                {
                    for(i = 0; i < D_len; i++)
                    {
                        change = data[i];
                        data[i] = data[i + 1];
                        data[i + 1] = change;
                        i++;
                    }

                    crc = (u16)UART_CRC16_Work((unsigned char *)data, D_len);
                    data[D_len] = (unsigned char)(crc / 256);
                    data[D_len + 1] = (unsigned char)(crc % 256);
                    Grid_Profiles_Data_Delete();
                    Grid_Profiles_Data_Write((char *)data, D_len + 2, 0);

                    if(mDevConfigPutResDTO->mi_to_sn_count != 0)
                    {
                        for(k = 0; k < mDevConfigPutResDTO->mi_to_sn_count; k++)
                        {
                            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                            {
                                if(PORT_NUMBER_CONFIRMATION(i))
                                {
                                    sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                                    if(sn == mDevConfigPutResDTO->mi_to_sn[k])
                                    {
#ifdef DEBUG
                                        printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                        printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                        printf("%02x", MIMajor[i].Property.Id[0]);
                                        printf("%02x", MIMajor[i].Property.Id[1]);
                                        printf("%02x", MIMajor[i].Property.Id[2]);
                                        printf("%02x", MIMajor[i].Property.Id[3]);
                                        printf("\n");
#endif
                                        MIReal[i].Data.NetCmd = NET_DOWNLOAD_DAT;
                                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                        i = 0;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else if(mDevConfigPutResDTO->mi_to_sn_count == 0)
                    {
                        memset((u8 *)Dev_sn, 0, sizeof(Dev_sn));
                        Ascii2Hex((u8 *)operate_sn, (u8 *)Dev_sn, 12);
                        mi_sn = (int64_t)(((uint64_t)(Dev_sn[0]) << 40) + ((uint64_t)(Dev_sn[1]) << 32) + ((uint64_t)(Dev_sn[2]) << 24) + ((uint64_t)(Dev_sn[3]) << 16) + ((uint64_t)(Dev_sn[4]) << 8) + (uint64_t)(Dev_sn[5]));

                        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
                        {
                            if(PORT_NUMBER_CONFIRMATION(i))
                            {
                                sn = (int64_t)(((uint64_t)(MIMajor[i].Property.Pre_Id[0]) << 40) + ((uint64_t)(MIMajor[i].Property.Pre_Id[1]) << 32) + ((uint64_t)(MIMajor[i].Property.Id[0]) << 24) + ((uint64_t)(MIMajor[i].Property.Id[1]) << 16) + ((uint64_t)(MIMajor[i].Property.Id[2]) << 8) + (uint64_t)(MIMajor[i].Property.Id[3]));

                                if(sn == mi_sn)
                                {
#ifdef DEBUG
                                    printf("%02x", MIMajor[i].Property.Pre_Id[0]);
                                    printf("%02x", MIMajor[i].Property.Pre_Id[1]);
                                    printf("%02x", MIMajor[i].Property.Id[0]);
                                    printf("%02x", MIMajor[i].Property.Id[1]);
                                    printf("%02x", MIMajor[i].Property.Id[2]);
                                    printf("%02x", MIMajor[i].Property.Id[3]);
                                    printf("\n");
#endif
                                    MIReal[i].Data.NetCmd = NET_DOWNLOAD_DAT;
                                    MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                    break;
                                }
                            }
                        }
                    }
                }

                if(mDevConfigPutResDTO->cfg_crc == (s32)MyCrc16(cfg_data, C_len))
                {
                    Grid_Profiles_Cfg_Data_Delete();
                    Grid_Profiles_Cfg_Data_Write((char *)cfg_data, C_len, 0);
                }
            }

            if(data != NULL)
            {
                myfree(data);
                data_being = 0;
            }

            if(cfg_data != NULL)
            {
                myfree(cfg_data);
                cfg_data_being = 0;
            }
        }

#ifdef DEBUG
        printf("mDevConfigPutResDTO->dtu_sn %s\n", mDevConfigPutResDTO->dtu_sn.arg);
        printf("mDevConfigPutResDTO->dev_sn %s\n", mDevConfigPutResDTO->dev_sn.arg);
        printf("mDevConfigPutResDTO->crc %d\n", (u32)mDevConfigPutResDTO->crc);
        printf("crc %d\n", MyCrc16(mDevConfigPutResDTO->data.bytes, mDevConfigPutResDTO->data.size));
        printf("data %s\n", mDevConfigPutResDTO->data.bytes);
        printf("D_len %d\n", mDevConfigPutResDTO->data.size);
        printf("mDevConfigPutResDTO->cfg_crc %d\n", (u32)mDevConfigPutResDTO->cfg_crc);
        printf("cfg_crc %d\n", MyCrc16(cfg_data, C_len));
        printf("C_len %d\n", C_len);
        printf("mDevConfigPutResDTO->tid %d\n", (u32)mDevConfigPutResDTO->tid);
        printf("mDevConfigPutResDTO->rule_id %d\n", mDevConfigPutResDTO->rule_id);
        printf("mDevConfigPutResDTO->data.bytes %s\n", mDevConfigPutResDTO->data.bytes);
        printf("mDevConfigPutResDTO->mi_to_sn_count %d\n", mDevConfigPutResDTO->mi_to_sn_count);
        printf("mDevConfigPutResDTO->mi_to_sn %d\n", (u32)mDevConfigPutResDTO->mi_to_sn[0]);
#endif
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mDevConfigPutResDTO->dtu_sn.arg);
    myfree(mDevConfigPutResDTO->dev_sn.arg);
    myfree(mDevConfigPutResDTO);
    myfree(DataBuf);
    myfree(CfgDataBuf);
    return ReturnNum;
}
/***********************************************
** Function name:       Memory_Timeout_Free
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
void Memory_Timeout_Free(void)
{
    if((LocalTime - MemoryTime) > 60000)
    {
        if(data_being == 1)
        {
            if(data != NULL)
            {
                myfree(data);
                data_being = 0;
            }
        }

        if(cfg_data_being == 1)
        {
            if(cfg_data != NULL)
            {
                myfree(cfg_data);
                cfg_data_being = 0;
            }
        }
    }
}

/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_WarnReq(u16 TAG, char *buffer, u8 package_nub, u8 package_now, u32 Alarm_time, AlarmDataType *AlarmDataBuf, u16 Alarm_nub,  u8 Device)
{
    vu8 i = 0;
    vu16 len = 0;
    vu16 Encodelen = 0;
    char Strdtu_ID[14];
    WarnReqDTO *mWarnReq = NULL;
    Encodelen = Encodelen + sizeof(WarnReqDTO);
    mWarnReq = mymalloc(sizeof(WarnReqDTO));
    //    if(mWarnReq == NULL)
    //    {
    //        myfree(mWarnReq);
    //        return 0;
    //    }
    memset(mWarnReq, 0, sizeof(WarnReqDTO));
    Encodelen = Encodelen + 20 * sizeof(AlarmDataType);
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        DTUSeq++;
        mWarnReq->dtu_sn.funcs.encode = encode_bytes;
        mWarnReq->dtu_sn.arg = Strdtu_ID;
        mWarnReq->package_nub = package_nub;
        mWarnReq->package_now = package_now;

        if(Device == 0)
        {
            mWarnReq->warn_device = Device;
        }
        else if(Device == 1)
        {
            mWarnReq->warn_device = Device;
            mWarnReq->warns_count = Alarm_nub;

            for(i = 0; i < Alarm_nub; i++)
            {
                mWarnReq->warns[i].pv_sn  = (int64_t)(((uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[0]) << 40) +
                                                      ((uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[1]) << 32) +
                                                      ((uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[2]) << 24) +
                                                      ((uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[3]) << 16) +
                                                      ((uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[4]) << 8) +
                                                      (uint64_t)(AlarmDataBuf[i].Data.Alarm_Id[5]));
                mWarnReq->warns[i].code  = AlarmDataBuf[i].Data.WCode;
                mWarnReq->warns[i].num   = ((u16)(AlarmDataBuf[i].Data.WNum[0]) << 8) +
                                           (u16)(AlarmDataBuf[i].Data.WNum[1]);
                mWarnReq->warns[i].s_time = (s32)(((u32)(AlarmDataBuf[i].Data.WTime1[0]) << 24) +
                                                  ((u32)(AlarmDataBuf[i].Data.WTime1[1]) << 16) +
                                                  ((u32)(AlarmDataBuf[i].Data.WTime1[2]) << 8) +
                                                  (u32)(AlarmDataBuf[i].Data.WTime1[3]));
                mWarnReq->warns[i].e_time = (s32)(((u32)(AlarmDataBuf[i].Data.WTime2[0]) << 24) +
                                                  ((u32)(AlarmDataBuf[i].Data.WTime2[1]) << 16) +
                                                  ((u32)(AlarmDataBuf[i].Data.WTime2[2]) << 8) +
                                                  (u32)(AlarmDataBuf[i].Data.WTime2[3]));
                mWarnReq->warns[i].w_data1 = ((u16)(AlarmDataBuf[i].Data.Data1[0]) << 8) +
                                             (u16)(AlarmDataBuf[i].Data.Data1[1]);
                mWarnReq->warns[i].w_data2 = ((u16)(AlarmDataBuf[i].Data.Data2[0]) << 8) +
                                             (u16)(AlarmDataBuf[i].Data.Data2[1]);
            }
        }

        mWarnReq->time = Alarm_time;
    }
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, WarnReqDTO_fields, mWarnReq);
    err_code = 0;
    myfree(mWarnReq);

    if(Alarm_nub == 0)
    {
        return 0;
    }
    else
    {
        return len;
    }
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_WarnRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    WarnResDTO *mWarnRes = NULL;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, 14);
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mWarnRes = mymalloc(sizeof(WarnResDTO));
    //    if(mWarnRes == NULL)
    //    {
    //        return 0;
    //    }
    memset(mWarnRes, 0, sizeof(WarnResDTO));
    {
        /*命令内容*/
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, WarnResDTO_fields, mWarnRes);

    if(pb_status == 1)
    {
        //        calendar_obj calendar;
        //        SecToDate((u32)mWarnRes->time, (calendar_obj *)&calendar);
        //        printf("%04d-%02d-%02d %02d:%02d:%02d",
        //               calendar.w_year, calendar.w_month, calendar.w_date, calendar.hour, calendar.min, calendar.sec);
        err_code = (u8)mWarnRes->err_code;
        GetPackageNow = mWarnRes->package_now;
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mWarnRes);
    return ReturnNum;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u16 Encode_WaveReq(u16 TAG, char *buffer, u32 package_now)
{
    vu16  len = 0;
    WaveReqDTO *mWaveReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(WaveReqDTO);
    calendar_obj calendar;
    mWaveReq = mymalloc(sizeof(WaveReqDTO));
    //    if(mWaveReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mWaveReq, 0, sizeof(WaveReqDTO));
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    {
        DTUSeq++;
        mWaveReq->dtu_sn.funcs.encode = encode_bytes;
        mWaveReq->dtu_sn.arg = Strdtu_ID;
        mWaveReq->time = (s32)RTC_Getsecond();
        mWaveReq->package_nub               = 3;        //总包数量          1
        mWaveReq->package_now               = 4;        //当前包序          1
        mWaveReq->pv_sn                     = 5;        //告警设备          8
        mWaveReq->code                     = 6;        //告警编码          2
        mWaveReq->num                      = 7;        //告警序号          2
        mWaveReq->warn_time                    = 8;        //告警时间1         6
        mWaveReq->data_len                   = 9;        //数据长度          2
        mWaveReq->pos                      = 10;       //告警点位置        2
        mWaveReq->warn_data.funcs.encode = encode_bytes;
        //        mWaveReq->mWVData.arg                   = 11;       //上报告警信息      900
        RTC_GetTimes(&calendar);
#ifdef DEBUG
        printf("package_now %d\n", package_now);
#endif
    }
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, WaveReqDTO_fields, mWaveReq);
    err_code = 0;
    myfree(mWaveReq);
    return len;
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
u8 Decode_WaveRes(char *buffer, u16 *pb_len)
{
    vu8 ReturnNum;
    bool pb_status;
    pb_istream_t pb_istream;
    WaveResDTO *mWaveRes = NULL;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, 14);
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mWaveRes = mymalloc(sizeof(WaveResDTO));
    //    if(mWaveRes == NULL)
    //    {
    //        return 0;
    //    }
    memset(mWaveRes, 0, sizeof(WaveResDTO));
    {
        /*命令内容*/
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, WaveResDTO_fields, mWaveRes);

    if(pb_status == 1)
    {
        err_code = (u8)mWaveRes->err_code;
        GetPackageNow = (u8)mWaveRes->package_now;
        ReturnNum = 1;
    }
    else
    {
        ReturnNum = 0;
    }

    myfree(mWaveRes);
    return ReturnNum;
}
/***********************************************
** Function name:       pb_HBReq
** Descriptions:        c->s心跳包打包
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_HBReq(char *buffer)
{
    return Encode_HBReq(TAG_HBReq, buffer);
}
/***********************************************
** Function name:       pb_HBRes_Analysis
** Descriptions:        s->c心跳包解析
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_HBRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_HBRes(buffer, pb_len) == 1)
    {
        ReceivingSign = TAG_HBRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_InfoDataReq
** Descriptions:        c->s信息帧打包
** input parameters:    package_nub一共多少包 package_now 第几包 time 打包时间
**                      MeterCount 电表数量 RpCount 中继数量 PvCount 组件数量
**                      InverterDetail *mInverterDetail
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
#ifdef DTU3PRO
u16 pb_InfoDataReq(u8 package_nub, u8 package_now,
                   u32 time, u16 MeterCount,
                   u16 MeterOff, u16 RpCount, u16 RpOff,
                   u16 PvCount, u16 PvOff, char *buffer,
                   InverterDetail *mInverterDetail)
#else
u16 pb_InfoDataReq(u8 package_nub, u8 package_now,
                   u32 time, u16 RpCount, u16 RpOff,
                   u16 PvCount, u16 PvOff, char *buffer,
                   InverterDetail *mInverterDetail)
#endif
{
#ifdef DTU3PRO
    return Encode_InfoDataReq(TAG_InfoDataReq, package_nub, package_now, time, MeterCount, MeterOff, RpCount,  RpOff,
                              PvCount,  PvOff, buffer, mInverterDetail);
#else
    return Encode_InfoDataReq(TAG_InfoDataReq, package_nub, package_now, time, RpCount,  RpOff,
                              PvCount,  PvOff, buffer, mInverterDetail);
#endif
}
/***********************************************
** Function name:       pb_InfoDataRes_Analysis
** Descriptions:        s->c信息帧回复解析
** input parameters:    *buffer 数据 *pb_len长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_InfoDataRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_InfoDataRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_InfoDataRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_RealDataReq
** Descriptions:        c->s发送实时数据
** input parameters:    *pb_buffer 打包后的数据 pb_len 包长度
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_RealDataReq(char *buffer, char *pb_buffer, u16 pb_len)
{
    vu16  len = 0;
    vu16  crc16;
    DTUSeq++;
    buffer[len++] = 'H';
    buffer[len++] = 'M';
    buffer[len++] = (char)(TAG_RealDataReq / 256);
    buffer[len++] = (char)(TAG_RealDataReq % 256);
    buffer[len++] = (char)(DTUSeq / 256);
    buffer[len++] = (char)(DTUSeq % 256);
    //增加校验PB包
    crc16 = (u16)MyCrc16((unsigned char *)pb_buffer, pb_len);
    buffer[len++] = (char)(crc16 / 256);
    buffer[len++] = (char)(crc16 % 256);
    buffer[len++] = (char)((pb_len + 10) / 256);
    buffer[len++] = (char)((pb_len + 10) % 256);
    memcpy((u8 *)&buffer[len], pb_buffer, pb_len);
    len += pb_len;
#ifdef DTU3PRO
#ifdef DEBUG

    if(Netmode_Used  != CABLE_MODE)
    {
        u32 a;
        printf("send: %d\r\n", len);

        for(a = 0; a < len; a++)
        {
            printf("%02x ", buffer[a]);
        }

        printf("\r\n");
    }

#endif
#endif
    return len;
}
/***********************************************
** Function name:       pb_RealDataRes_Analysis
** Descriptions:        s->c实时信息帧回复解析
** input parameters:    *buffer 数据 *pb_len长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_RealDataRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_RealDataRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_RealDataRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_HistoryDataReq
** Descriptions:        c->s发送实时数据
** input parameters:    *pb_buffer 打包后的数据 pb_len 包长度
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_HistoryDataReq(char *buffer, char *pb_buffer, u16 pb_len)
{
    vu16  len = 0;
    vu16  crc16;
    DTUSeq++;
    buffer[len++] = 'H';
    buffer[len++] = 'M';
    buffer[len++] = (u8)(TAG_HistoryDataReq / 256);
    buffer[len++] = (u8)(TAG_HistoryDataReq % 256);
    buffer[len++] = (u8)(DTUSeq / 256);
    buffer[len++] = (u8)(DTUSeq % 256);
    //增加校验PB包
    crc16 = (u16)MyCrc16((unsigned char *)pb_buffer, pb_len);
    buffer[len++] = (u8)(crc16 / 256);
    buffer[len++] = (u8)(crc16 % 256);
    buffer[len++] = (u8)((pb_len + 10) / 256);
    buffer[len++] = (u8)((pb_len + 10) % 256);
    memcpy((u8 *)&buffer[len], pb_buffer, pb_len);
    len += pb_len;
    return len;
}
/***********************************************
** Function name:       pb_HistoryDataRes_Analysis
** Descriptions:        s->c实时信息帧回复解析
** input parameters:    *buffer 数据 *pb_len长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_HistoryDataRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_RealDataRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_HistoryDataRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_CommandRes_Analysis
** Descriptions:
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_CommandRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_CommandRes(buffer, pb_len) == 1)
    {
        ReceivingSign = TAG_CommandRes;
        PackageNow = GetPackageNow;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_CommandReq
** Descriptions:        c->s
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_CommandReq(char *buffer, u32 package_now)
{
    return Encode_CommandReq(TAG_CommandReq, buffer, package_now);
}
/***********************************************
** Function name:       pb_CommandResStatus
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_CommandStatusReq(char *buffer, u8 package_nub, u8 package_now)
{
    return Encode_CommandStatusReq(TAG_CommandStatusReq, buffer, package_nub, package_now);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_CommandStatusRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_CommandStatusResDTO(buffer, pb_len) == 1)
    {
        ReceivingSign = TAG_CommandStatusRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_DevConfigFetchReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_DevConfigFetchReq(char *buffer, u8 *package_nub, u8 package_now)
{
    return Encode_DevConfigFetchReqDTO(TAG_DevConfigFetchReq, buffer, package_nub, package_now);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_DevConfigFetchRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_DevConfigFetchResDTO(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_DevConfigFetchRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_DevConfigPutReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_DevConfigPutReq(char *buffer)
{
    return Encode_DevConfigPutReqDTO(TAG_DevConfigPutReq, buffer, status, GetPackageNow);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_DevConfigPutRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_DevConfigPutResDTO(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_DevConfigPutRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_WarnReq
** Descriptions:        s->c
** input parameters:    Device 0：DTU 1：微逆
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_WarnReq(char *buffer, u8 package_nub, u8 package_now, u32 Alarm_time, AlarmDataType *AlarmDataBuf, u16 Alarm_nub,  u8 Device)
{
    return Encode_WarnReq(TAG_WarnReq, buffer, package_nub, package_now, Alarm_time, AlarmDataBuf, Alarm_nub, Device);
}
/***********************************************
** Function name:       pb_WarnRes_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_WarnRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_WarnRes(buffer, pb_len) == 1)
    {
        //package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_WarnRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}

/***********************************************
** Function name:       pb_HisWarnReq
** Descriptions:        s->c
** input parameters:    Device 0：DTU 1：微逆
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_WarnInfoReq(char *buffer, u8 package_nub, u8 package_now, u32 Alarm_time, AlarmDataType *AlarmDataBuf, u16 Alarm_nub,  u8 Device)
{
    return Encode_WarnReq(TAG_WarnInfoReq, buffer, package_nub, package_now, Alarm_time, AlarmDataBuf, Alarm_nub, Device);
}
/***********************************************
** Function name:       pb_HisWarnRes_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_WarnInfoRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_WarnRes(buffer, pb_len) == 1)
    {
        //package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_WarnInfoRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_WarnReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_WaveReq(char *buffer, u8 Device)
{
    return Encode_WaveReq(TAG_WaveReq, buffer, Device);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_WaveRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_WaveRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_WaveRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/**********************************************
*                      App                    *
***********************************************/
/***********************************************
** Function name:       pb_AppGetConfigReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppGetConfigReq(char *buffer)
{
    vu8 APN[40] = {0};
    vu16  len = 0;
    vu16  bufflen = 100;
    GetConfigReq *mGetConfigReq = NULL;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    ServerSeq++;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(GetConfigReq);
    mGetConfigReq = mymalloc(sizeof(GetConfigReq));
    //    if(mGetConfigReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mGetConfigReq, 0, sizeof(GetConfigReq));
    {
        //时区
        mGetConfigReq->offset = Dtu3Detail.Property.timezone;
        mGetConfigReq->time = (s32)RTC_Getsecond();
        //防盗密码
        mGetConfigReq->lock_password = (s32)((u32)(Dtu3Detail.Property.LockNewPassword[0] << 24) + (u32)(Dtu3Detail.Property.LockNewPassword[1] << 16)
                                             + (u32)(Dtu3Detail.Property.LockNewPassword[2] << 8) + (u32)(Dtu3Detail.Property.LockNewPassword[3]));
        //防盗时间
        mGetConfigReq->lock_time = (u16)(Dtu3Detail.Property.Lock_Time[0]) + (u16)(Dtu3Detail.Property.Lock_Time[1] << 8);
        //限功率
        mGetConfigReq->limit_power_mypower = (s32)Dtu3Detail.Property.LimitPower_MyPower[0];
        //防逆流 433发射器地址
        mGetConfigReq->zero_export_433_addr = (s32)((Dtu3Detail.Property.Reflux_WirelessSenderAddr[0]) + (u32)(Dtu3Detail.Property.Reflux_WirelessSenderAddr[1] << 8) + (u32)(Dtu3Detail.Property.Reflux_WirelessSenderAddr[2] << 16));
        //是否防逆流
        mGetConfigReq->zero_export_enable = Dtu3Detail.Property.Zero_Export_Switch;
        //上网模式选择  预留
#ifdef DTU3PRO
        mGetConfigReq->netmode_select = Netmode_Used;
#else
        mGetConfigReq->netmode_select = Dtu3Major.Property.netmode_select;
#endif
        //433通道号选择  ----Lite不需要
        mGetConfigReq->channel_select = 0;
        //服务器上传间隔
        mGetConfigReq->server_send_time = Dtu3Major.Property.server_send_time;
        //服务器端口号
        mGetConfigReq->serverport = Dtu3Major.Property.ServerPort;
        //DTU wifi 信号强度
        mGetConfigReq->wifi_rssi = Dtu3Major.Property.wifi_RSSI;
        //运营商设置---国内不需要
        mGetConfigReq->apn_set.funcs.encode = encode_bytes;
        memset((u8 *)APN, 0, sizeof(APN));
        memcpy((u8 *)APN, (u8 *)Dtu3Major.Property.APN, sizeof(Dtu3Major.Property.APN));
        memcpy((u8 *)&APN[sizeof(Dtu3Major.Property.APN)], (u8 *)Dtu3Major.Property.APN2, sizeof(Dtu3Major.Property.APN2));
        mGetConfigReq->apn_set.arg = (u8 *)APN;
        Encodelen = Encodelen + bufflen;
        //电表厂家 及  型号
        mGetConfigReq->meter_kind.funcs.encode = encode_bytes;
        mGetConfigReq->meter_kind.arg = (u8 *)Dtu3Major.Property.Wifi_Vsn;
        Encodelen = Encodelen + bufflen;
        //防逆流 接入方式
        mGetConfigReq->meter_interface.funcs.encode = encode_bytes;
        mGetConfigReq->meter_interface.arg = (u8 *)Dtu3Major.Property.Wifi_Vsn;
        Encodelen = Encodelen + bufflen;
        //dtu连接wifi的ssid （最大40字符）
        mGetConfigReq->wifi_ssid.funcs.encode = encode_bytes;
        mGetConfigReq->wifi_ssid.arg = (u8 *)Dtu3Major.Property.wifi_ssid;
        Encodelen = Encodelen + bufflen;
        //dtu连接wifi的密码 （最大30字符）
        mGetConfigReq->wifi_passward.funcs.encode = encode_bytes;
        mGetConfigReq->wifi_passward.arg = (u8 *)Dtu3Major.Property.wifi_passward;
        Encodelen = Encodelen + bufflen;
        //服务器域名设置
        mGetConfigReq->server_domain_name.funcs.encode = encode_bytes;
        mGetConfigReq->server_domain_name.arg = (u8 *)Dtu3Major.Property.ServerDomainName;
        Encodelen = Encodelen + bufflen;
        mGetConfigReq->inv_type = 0;
        mGetConfigReq->dtu_sn.funcs.encode = encode_bytes;
        mGetConfigReq->dtu_sn.arg = Strdtu_ID;
#ifdef DTU3PRO
        //DHCP开关
        mGetConfigReq->dhcp_switch = Dtu3Detail.Property.DHCP_Switch;
        //设置IP地址
        mGetConfigReq->ip_addr_0 = Dtu3Detail.Property.IP_ADDR[0];
        mGetConfigReq->ip_addr_1 = Dtu3Detail.Property.IP_ADDR[1];
        mGetConfigReq->ip_addr_2 = Dtu3Detail.Property.IP_ADDR[2];
        mGetConfigReq->ip_addr_3 = Dtu3Detail.Property.IP_ADDR[3];
        //子掩码
        mGetConfigReq->subnet_mask_0 = Dtu3Detail.Property.subnet_mask[0];
        mGetConfigReq->subnet_mask_1 = Dtu3Detail.Property.subnet_mask[1];
        mGetConfigReq->subnet_mask_2 = Dtu3Detail.Property.subnet_mask[2];
        mGetConfigReq->subnet_mask_3 = Dtu3Detail.Property.subnet_mask[3];
        //默认网关
        mGetConfigReq->default_gateway_0 = Dtu3Detail.Property.default_gateway[0];
        mGetConfigReq->default_gateway_1 = Dtu3Detail.Property.default_gateway[1];
        mGetConfigReq->default_gateway_2 = Dtu3Detail.Property.default_gateway[2];
        mGetConfigReq->default_gateway_3 = Dtu3Detail.Property.default_gateway[3];
#endif
    }
    Net_ProtobufEncode(TAG_AppGetConfigReq, ServerSeq, buffer, (u16 *)&len, Encodelen, GetConfigReq_fields, mGetConfigReq);
    //myfree(mGetConfigReq->meter_kind.arg);
    //myfree(mGetConfigReq->meter_interface.arg);
    //myfree(mGetConfigReq->wifi_ssid.arg);
    //myfree(mGetConfigReq->apn_set.arg);
    //myfree(mGetConfigReq->wifi_passward.arg);
    //myfree(mGetConfigReq->server_domain_name.arg);
    myfree(mGetConfigReq);
    return len;
}
/***********************************************
** Function name:       pb_AppGetConfigReq_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppGetConfigRes_Analysis(char *buffer, u16 *pb_len)
{
    bool pb_status;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, 14);
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    pb_istream_t pb_istream;
    GetConfigRes *mGetConfigRes = NULL;
    mGetConfigRes = mymalloc(sizeof(GetConfigRes));
    //    if(mGetConfigRes == NULL)
    //    {
    //        return;
    //    }
    memset(mGetConfigRes, 0, sizeof(GetConfigRes));
    {
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, GetConfigRes_fields, mGetConfigRes);

    if(pb_status == 1)
    {
#ifdef DEBUG
        printf("mGetConfigRes->offset:%d\n", mGetConfigRes->offset);
        printf("mGetConfigRes->time:%d\n", mGetConfigRes->time);
#endif
        ReceivingSign = TAG_AppGetConfigRes;
    }
    else
    {
        ReceivingSign = 0;
    }

    myfree(mGetConfigRes);
}
/***********************************************
** Function name:       pb_AppSetConfigReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppSetConfigReq(char *buffer)
{
    vu16  len = 0;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, 14);
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    SetConfigReq *mSetConfigReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(SetConfigReq);
    mSetConfigReq = mymalloc(sizeof(SetConfigReq));
    //    if(mSetConfigReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mSetConfigReq, 0, sizeof(SetConfigReq));
    {
        //时区
        mSetConfigReq->offset                = Dtu3Detail.Property.timezone;
        //绝对时间
        mSetConfigReq->time                  = (s32)RTC_Getsecond();
        //正确为0，其它为错误代码.
        mSetConfigReq->err_code              = err_code;
        Encodelen = Encodelen + 100;
    }
    Net_ProtobufEncode(TAG_AppSetConfigReq, ServerSeq, buffer, (u16 *)&len, Encodelen, SetConfigReq_fields, mSetConfigReq);
    err_code = 0;
    myfree(mSetConfigReq);
    return len;
}
/***********************************************
** Function name:       pb_AppSetConfigRes_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppSetConfigRes_Analysis(char *buffer, u16 *pb_len)
{
    vu8 APN[40] = {0};
    vu16  bufflen = 100;
    bool pb_status;
    pb_istream_t pb_istream;
    SetConfigRes *mSetConfigRes = NULL;
    mSetConfigRes = mymalloc(sizeof(SetConfigRes));
    //    if(mSetConfigRes == NULL)
    //    {
    //        return;
    //    }
    memset(mSetConfigRes, 0, sizeof(SetConfigRes));
    {
        mSetConfigRes->meter_kind.funcs.decode = decode_bytes;
        mSetConfigRes->meter_kind.arg = mymalloc(bufflen);
        memset(mSetConfigRes->meter_kind.arg, 0, bufflen);
        //防逆流 接入方式
        mSetConfigRes->meter_interface.funcs.decode = decode_bytes;
        mSetConfigRes->meter_interface.arg = mymalloc(bufflen);
        memset(mSetConfigRes->meter_interface.arg, 0, bufflen);
        mSetConfigRes->apn_set.funcs.decode = decode_bytes;
        mSetConfigRes->apn_set.arg = mymalloc(bufflen);
        memset(mSetConfigRes->apn_set.arg, 0, bufflen);
        //dtu连接wifi的ssid （最大40字符）
        mSetConfigRes->wifi_ssid.funcs.decode = decode_bytes;
        mSetConfigRes->wifi_ssid.arg = mymalloc(bufflen);
        memset(mSetConfigRes->wifi_ssid.arg, 0, bufflen);
        //dtu连接wifi的密码 （最大30字符）
        mSetConfigRes->wifi_passward.funcs.decode = decode_bytes;
        mSetConfigRes->wifi_passward.arg = mymalloc(bufflen);
        memset(mSetConfigRes->wifi_passward.arg, 0, bufflen);
        //服务器域名设置
        mSetConfigRes->server_domain_name.funcs.decode = decode_bytes;
        mSetConfigRes->server_domain_name.arg = mymalloc(bufflen);
        memset(mSetConfigRes->server_domain_name.arg, 0, bufflen);
    }
    //把发送缓存的数据转换到结构体
    pb_istream = pb_istream_from_buffer((unsigned char *)buffer, *pb_len);
    pb_status = pb_decode(&pb_istream, SetConfigRes_fields, mSetConfigRes);

    if(pb_status == 1)
    {
        //防盗密码Lock_Password
        Dtu3Detail.Property.LockOldPassword[0] = Dtu3Detail.Property.LockNewPassword[0];
        Dtu3Detail.Property.LockOldPassword[1] = Dtu3Detail.Property.LockNewPassword[1];
        Dtu3Detail.Property.LockOldPassword[2] = Dtu3Detail.Property.LockNewPassword[2];
        Dtu3Detail.Property.LockOldPassword[3] = Dtu3Detail.Property.LockNewPassword[3];
        Dtu3Detail.Property.LockNewPassword[0] = (u8)mSetConfigRes->lock_password;
        Dtu3Detail.Property.LockNewPassword[1] = (u8)(mSetConfigRes->lock_password >> 8);
        Dtu3Detail.Property.LockNewPassword[2] = (u8)(mSetConfigRes->lock_password >> 16);
        Dtu3Detail.Property.LockNewPassword[3] = (u8)(mSetConfigRes->lock_password >> 24);
        //防盗时间
        Dtu3Detail.Property.Lock_Time[0] = (u8)mSetConfigRes->lock_time;
        Dtu3Detail.Property.Lock_Time[1] = (u8)(mSetConfigRes->lock_time >> 8);
        //限功率
        Dtu3Detail.Property.LimitPower_MyPower[0] = (u32)mSetConfigRes->limit_power_mypower;
        //防逆流 433发射器地址
        Dtu3Detail.Property.Reflux_WirelessSenderAddr[0] = (u8)mSetConfigRes->zero_export_433_addr;
        Dtu3Detail.Property.Reflux_WirelessSenderAddr[1] = (u8)(mSetConfigRes->zero_export_433_addr >> 8);
        Dtu3Detail.Property.Reflux_WirelessSenderAddr[2] = (u8)(mSetConfigRes->zero_export_433_addr >> 16);
        //电表厂家 及  型号
        //mSetConfigRes->meter_kind.arg = &"";
        ////防逆流 接入方式
        //mSetConfigRes->meter_interface.arg = &"";
        //是否防逆流
        Dtu3Detail.Property.Zero_Export_Switch = (u8)mSetConfigRes->zero_export_enable;
        //上网模式选择  预留
#ifdef DTU3PRO

        if(Netmode_Used != mSetConfigRes->netmode_select)
        {
            server_link_time = 0;
            server_signal_strength = 0;
            connect_net = 0;
            connect_server = 0;
            server_data = 0;
        }

        Netmode_Used = (u8)mSetConfigRes->netmode_select;
        Dtu3Major.Property.netmode_select = (u8)mSetConfigRes->netmode_select;
#else

        if(Dtu3Major.Property.netmode_select != mSetConfigRes->netmode_select)
        {
            server_link_time = 0;
            server_signal_strength = 0;
            connect_net = 0;
            connect_server = 0;
            server_data = 0;
        }

        Dtu3Major.Property.netmode_select = mSetConfigRes->netmode_select;
#endif
        ////运营商设置---国内不需要
        //mSetConfigRes->apn_set = 0;
        ////433通道号选择  ----Lite不需要
        //mSetConfigRes->channel_select = 0;
        ////服务器上传间隔
        Dtu3Major.Property.server_send_time = (u8) mSetConfigRes->server_send_time;
        memset((u8 *)APN, 0, sizeof(APN));
        memcpy((u8 *)APN, mSetConfigRes->apn_set.arg, strlen(mSetConfigRes->apn_set.arg));
        memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
        memset((u8 *)Dtu3Major.Property.APN2, 0, sizeof(Dtu3Major.Property.APN2));
        memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)APN, sizeof(Dtu3Major.Property.APN));
        memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)&APN[sizeof(Dtu3Major.Property.APN)], sizeof(Dtu3Major.Property.APN2));
        //DTU连接WIFI的SSID （最大40字符）
        memset((u8 *)Dtu3Major.Property.wifi_ssid, 0, sizeof(Dtu3Major.Property.wifi_ssid));
        memcpy((u8 *)Dtu3Major.Property.wifi_ssid, mSetConfigRes->wifi_ssid.arg, strlen(mSetConfigRes->wifi_ssid.arg));
        //DTU连接WIFI的密码 （最大30字符）
        memset((u8 *)Dtu3Major.Property.wifi_passward, 0, sizeof(Dtu3Major.Property.wifi_passward));
        memcpy((u8 *)Dtu3Major.Property.wifi_passward, mSetConfigRes->wifi_passward.arg, strlen(mSetConfigRes->wifi_passward.arg));
        //服务器域名设置
        memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, mSetConfigRes->server_domain_name.arg, strlen(mSetConfigRes->server_domain_name.arg));
        //服务器端口号
        Dtu3Major.Property.ServerPort = (u16)mSetConfigRes->serverport;
#ifdef DTU3PRO
        Dtu3Detail.Property.DHCP_Switch = (u8)mSetConfigRes->dhcp_switch;

        if(Dtu3Detail.Property.DHCP_Switch == 1)
        {
            Dtu3Detail.Property.IP_ADDR[0] = (u8)mSetConfigRes->ip_addr_0;
            Dtu3Detail.Property.IP_ADDR[1] = (u8)mSetConfigRes->ip_addr_1;
            Dtu3Detail.Property.IP_ADDR[2] = (u8)mSetConfigRes->ip_addr_2;
            Dtu3Detail.Property.IP_ADDR[3] = (u8)mSetConfigRes->ip_addr_3;
            Dtu3Detail.Property.subnet_mask[0] = (u8)mSetConfigRes->subnet_mask_0;
            Dtu3Detail.Property.subnet_mask[1] = (u8)mSetConfigRes->subnet_mask_1;
            Dtu3Detail.Property.subnet_mask[2] = (u8)mSetConfigRes->subnet_mask_2;
            Dtu3Detail.Property.subnet_mask[3] = (u8)mSetConfigRes->subnet_mask_3;
            Dtu3Detail.Property.default_gateway[0] = (u8)mSetConfigRes->default_gateway_0;
            Dtu3Detail.Property.default_gateway[1] = (u8)mSetConfigRes->default_gateway_1;
            Dtu3Detail.Property.default_gateway[2] = (u8)mSetConfigRes->default_gateway_2;
            Dtu3Detail.Property.default_gateway[3] = (u8)mSetConfigRes->default_gateway_3;
        }

#endif
        //设置标志位
        Dtu3Major.Property.wifi_ssid_save_set = 1;
        //        System_DtuDetail_Write(&Dtu3Detail);
        //        System_DtuMajor_Write(&Dtu3Major);
        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
        ReceivingSign = TAG_AppSetConfigRes;
    }
    else
    {
        ReceivingSign = 0;
    }

    myfree(mSetConfigRes->meter_kind.arg);
    myfree(mSetConfigRes->meter_interface.arg);
    myfree(mSetConfigRes->apn_set.arg);
    myfree(mSetConfigRes->wifi_ssid.arg);
    myfree(mSetConfigRes->wifi_passward.arg);
    myfree(mSetConfigRes->server_domain_name.arg);
    myfree(mSetConfigRes);
}
/***********************************************
** Function name:       pb_AppInfoReq_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppInfoRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_InfoDataRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        AppPackageNow = GetPackageNow;
        ReceivingSign = TAG_AppInfoRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:
** Descriptions:
** input parameters:    无
** output parameters:
** Returned value:
*************************************************/
#ifdef DTU3PRO
u16 Encode_AppInfoDataReq(u16 TAG, u8 package_nub, u8 package_now,
                          u32 time, u16 MeterCount,
                          u16 MeterOff, u16 RpCount, u16 RpOff,
                          u16 PvCount, u16 PvOff, char *buffer,
                          InverterDetail *mInverterDetail)
#else
u16 Encode_AppInfoDataReq(u16 TAG, u8 package_nub, u8 package_now,
                          u32 time, u16 RpCount, u16 RpOff,
                          u16 PvCount, u16 PvOff, char *buffer,
                          InverterDetail *mInverterDetail)
#endif
{
    vu16  i;
    vu16  len = 0;
    vu16  bufflen = 20;
    APPInfoDataReqDTO *mAPPInfoDataReq = NULL;
    vu16  Encodelen = 0;
    Encodelen = Encodelen + sizeof(APPInfoDataReqDTO);
    calendar_obj calendar;
    DTUSeq++;
    char Strdtu_ID[14];
    memset(Strdtu_ID, 0, sizeof(Strdtu_ID));
    Hex2Ascii((u8 *)&Strdtu_ID[0], (u8 *)Dtu3Major.Property.Pre_Id, 2);
    Hex2Ascii((u8 *)&Strdtu_ID[4], (u8 *)Dtu3Major.Property.Id, 4);
    mAPPInfoDataReq = mymalloc(sizeof(APPInfoDataReqDTO));
    //    if(mAPPInfoDataReq == NULL)
    //    {
    //        return 0;
    //    }
    memset(mAPPInfoDataReq, 0, sizeof(APPInfoDataReqDTO));
    {
        mAPPInfoDataReq->dtu_sn.funcs.encode = encode_bytes;
        mAPPInfoDataReq->dtu_sn.arg = Strdtu_ID;
        Encodelen = Encodelen + sizeof(Strdtu_ID);
        SecToDate(time, &calendar);
        mAPPInfoDataReq->time = (s32)RTC_Getsecond();
        /*MI数量*/
        mAPPInfoDataReq->device_nub = Dtu3Detail.Property.InverterNum;
        /*组件数量*/
        mAPPInfoDataReq->pv_nub = Dtu3Detail.Property.PortNum;
        /*总包数量*/
        mAPPInfoDataReq->package_nub = package_nub;
        /*当前包序*/
        mAPPInfoDataReq->package_now = package_now;
        /*使用通道号*/
        mAPPInfoDataReq->channel = 0x01;
        /*系统信息*/
        //设备类型
        mAPPInfoDataReq->mAPPDtuInfo.device_kind = 0;
        //dtu连接服务器模式
#ifdef DTU3PRO
        mAPPInfoDataReq->mAPPDtuInfo.access_model = Netmode_Used;
#else
        mAPPInfoDataReq->mAPPDtuInfo.access_model =  Dtu3Major.Property.netmode_select;
#endif
        //DTU硬件版本
        mAPPInfoDataReq->mAPPDtuInfo.dtu_hw = (s32)(((u32)(Dtu3Major.Property.DtuHw_Ver[0]) << 8) + (u32)(Dtu3Major.Property.DtuHw_Ver[1]));
        //DTU软件版本
        mAPPInfoDataReq->mAPPDtuInfo.dtu_sw = (s32)(((u32)(Dtu3Major.Property.DtuSw_Ver[0]) << 8) + (u32)(Dtu3Major.Property.DtuSw_Ver[1]));
        mAPPInfoDataReq->mAPPDtuInfo.dtu_step_time = Dtu3Detail.Property.Server_SendTimePeriod;
        //nRF硬件版本
        mAPPInfoDataReq->mAPPDtuInfo.dtu_rf_hw = (s32)(((u32)(Dtu3Major.Property.RfHw_Ver[1]) << 16) + ((u32)(Dtu3Major.Property.RfHw_Ver[2]) << 8) + (u32)(Dtu3Major.Property.RfHw_Ver[3]));
        //nRF软件版本
        mAPPInfoDataReq->mAPPDtuInfo.dtu_rf_sw = (s32)(((u32)(Dtu3Major.Property.RfFw_Ver[1]) << 16) + ((u32)(Dtu3Major.Property.RfFw_Ver[2]) << 8) + (u32)(Dtu3Major.Property.RfFw_Ver[3]));
        //GPRS版本号
        mAPPInfoDataReq->mAPPDtuInfo.gprs_vsn.funcs.encode = encode_bytes;
        mAPPInfoDataReq->mAPPDtuInfo.gprs_vsn.arg = mymalloc(bufflen);
        memset(mAPPInfoDataReq->mAPPDtuInfo.gprs_vsn.arg, 0, bufflen);
        Hex2Ascii(mAPPInfoDataReq->mAPPDtuInfo.gprs_vsn.arg, (u8 *)Dtu3Major.Property.Gprs_Ver, 4);
        Encodelen = Encodelen + bufflen;
        //GPRS卡号
        mAPPInfoDataReq->mAPPDtuInfo.ka_nub.funcs.encode = encode_bytes;
        mAPPInfoDataReq->mAPPDtuInfo.ka_nub.arg = mymalloc(bufflen * 5);
        memset(mAPPInfoDataReq->mAPPDtuInfo.ka_nub.arg, 0, bufflen * 5);
        Hex2Ascii(mAPPInfoDataReq->mAPPDtuInfo.ka_nub.arg, (u8 *)Dtu3Major.Property.Gprs_SIMNum, 11);
        Encodelen = Encodelen + bufflen * 5;
        //wifi版本号
        mAPPInfoDataReq->mAPPDtuInfo.wifi_vsn.funcs.encode = encode_bytes;
        mAPPInfoDataReq->mAPPDtuInfo.wifi_vsn.arg = mymalloc(bufflen);
        memset(mAPPInfoDataReq->mAPPDtuInfo.wifi_vsn.arg, 0, bufflen);
        Hex2Ascii(mAPPInfoDataReq->mAPPDtuInfo.wifi_vsn.arg, (u8 *)Dtu3Major.Property.Wifi_Vsn, 3);
        Encodelen = Encodelen + bufflen;
        mAPPInfoDataReq->mAPPDtuInfo.dtu_rule_id = Dtu3Major.Property.rule_id;
        mAPPInfoDataReq->mAPPDtuInfo.dtu_error_code = 0;
        //////////
        mAPPInfoDataReq->mAPPDtuInfo.communication_time = (s32)(server_link_time);
        mAPPInfoDataReq->mAPPDtuInfo.signal_strength = server_signal_strength;
#ifdef DTU3PRO
        mAPPInfoDataReq->mAPPDtuInfo.dtu485_mode = Dtu3Detail.Property.RS485Mode;
        mAPPInfoDataReq->mAPPDtuInfo.dtu485_addr = Dtu3Detail.Property.RS485Addr;
        //////////
        /*电表*/
        {
            mAPPInfoDataReq->mAPPMeterInfo_count = MeterCount;

            for(i = 0; i < MeterCount; i++)
            {
                //设备类型
                mAPPInfoDataReq->mAPPMeterInfo[i].device_kind = 0;
                //电表序号--地址
                mAPPInfoDataReq->mAPPMeterInfo[i].meter_sn = (int64_t)(((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[0]) << 40) + ((uint64_t)(Meter[i + MeterOff].Property.Pre_Id[1]) << 32) + ((uint64_t)(Meter[i + MeterOff].Property.Id[0]) << 24) + ((uint64_t)(Meter[i + MeterOff].Property.Id[1]) << 16) + ((uint64_t)(Meter[i + MeterOff].Property.Id[2]) << 8) + (uint64_t)(Meter[i + MeterOff].Property.Id[3]));
                //电表型号
                mAPPInfoDataReq->mAPPMeterInfo[i].meter_model = (Meter[i + MeterOff].Property.Id[1] % 16) * 16 + (Meter[i + MeterOff].Property.Id[2]) / 16;
                //CT型号
                mAPPInfoDataReq->mAPPMeterInfo[i].meter_ct = 1;
                //电表通讯方式
                mAPPInfoDataReq->mAPPMeterInfo[i].com_way = 1;
                //电表接入方式
                mAPPInfoDataReq->mAPPMeterInfo[i].access_mode = Meter[i + MeterOff].Property.place;
            }
        }
#endif
        /*中继*/
        {
            /*中继信息*/
            mAPPInfoDataReq->mAPPRpInfo_count = RpCount;

            for(i = 0; i < RpCount; i++)
            {
                //设备类型
                mAPPInfoDataReq->mAPPRpInfo[i].device_kind = 2;
                //电表序号--地址
                mAPPInfoDataReq->mAPPRpInfo[i].rp_sn = 1234;
                //中继硬件版本
                mAPPInfoDataReq->mAPPRpInfo[i].rp_hw = 123456;
                //中继软件版本
                mAPPInfoDataReq->mAPPRpInfo[i].rp_sw = 123456;
                mAPPInfoDataReq->mAPPRpInfo[i].rp_rule_id = 0xFFFF;
            }
        }
        /*单个组件的数据 包括故障数据*/
        mAPPInfoDataReq->mAPPpvInfo_count = PvCount;

        for(i = 0; i < PvCount; i++)
        {
            //设备类型
            mAPPInfoDataReq->mAPPpvInfo[i].device_kind = 1;
            //微逆 ID
            mAPPInfoDataReq->mAPPpvInfo[i].pv_sn = (int64_t)(((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[0]) << 40)
                                                   + ((uint64_t)(MIMajor[i + PvOff].Property.Pre_Id[1]) << 32)
                                                   + ((uint64_t)(MIMajor[i + PvOff].Property.Id[0]) << 24)
                                                   + ((uint64_t)(MIMajor[i + PvOff].Property.Id[1]) << 16)
                                                   + ((uint64_t)(MIMajor[i + PvOff].Property.Id[2]) << 8)
                                                   + (uint64_t)(MIMajor[i + PvOff].Property.Id[3]));
            //引导程序版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_usfw        = (s32)(mInverterDetail[i].Property.USFWBuild_VER);
            //应用程序版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_sw          = (s32)(mInverterDetail[i].Property.AppFWBuild_VER);
            //硬件料号
            mAPPInfoDataReq->mAPPpvInfo[i].pv_hw_pn       = (s32)(((u32)(mInverterDetail[i].Property.HW_PNH) << 16) + (u32)(mInverterDetail[i].Property.HW_PNL));
            //硬件版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_hw          = (s32)(mInverterDetail[i].Property.HW_VER);
            //并网保护文件代码
            mAPPInfoDataReq->mAPPpvInfo[i].pv_gpf_code    = (s32)(mInverterDetail[i].Property.Country_Std);
            //并网保护文件版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_gpf         = (s32)(mInverterDetail[i].Property.Save_Version);
            //nRF硬件版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_rf_hw       = (s32)(((u32)(mInverterDetail[i].Property.NRF_HardVersion[0]) << 24) + ((u32)(mInverterDetail[i].Property.NRF_HardVersion[1]) << 16) + ((u32)(mInverterDetail[i].Property.NRF_HardVersion[2]) << 8) + (u32)(mInverterDetail[i].Property.NRF_HardVersion[3]));
            //nRF软件版本
            mAPPInfoDataReq->mAPPpvInfo[i].pv_rf_sw       = (s32)(((u32)(mInverterDetail[i].Property.NRF_SoftVersion[0]) << 24) + ((u32)(mInverterDetail[i].Property.NRF_SoftVersion[1]) << 16) + ((u32)(mInverterDetail[i].Property.NRF_SoftVersion[2]) << 8) + (u32)(mInverterDetail[i].Property.NRF_SoftVersion[3]));
            mAPPInfoDataReq->mAPPpvInfo[i].mi_rule_id     = mInverterDetail[i].Property.rule_id;
        }
    }
    Net_ProtobufEncode(TAG, DTUSeq, buffer, (u16 *)&len, Encodelen, APPInfoDataReqDTO_fields, mAPPInfoDataReq);
    myfree(mAPPInfoDataReq->mAPPDtuInfo.gprs_vsn.arg);
    myfree(mAPPInfoDataReq->mAPPDtuInfo.ka_nub.arg);
    myfree(mAPPInfoDataReq->mAPPDtuInfo.wifi_vsn.arg);
    myfree(mAPPInfoDataReq);
    return len;
}
/***********************************************
** Function name:       pb_AppInfoRes
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
#ifdef DTU3PRO
u16 pb_AppInfoReq(u8 package_nub, u8 package_now,
                  u32 time, u16 MeterCount,
                  u16 MeterOff, u16 RpCount, u16 RpOff,
                  u16 PvCount, u16 PvOff, char *buffer,
                  InverterDetail *mInverterDetail)
#else
u16 pb_AppInfoReq(u8 package_nub, u8 package_now,
                  u32 time, u16 RpCount, u16 RpOff,
                  u16 PvCount, u16 PvOff, char *buffer,
                  InverterDetail *mInverterDetail)
#endif
{
    app_switching_time = 0;
    app_switching_time_real = 0;
#ifdef DTU3PRO
    return Encode_AppInfoDataReq(TAG_AppInfoReq, package_nub, package_now, time, MeterCount, MeterOff, RpCount,  RpOff,
                                 PvCount,  PvOff, buffer, mInverterDetail);
#else
    return Encode_AppInfoDataReq(TAG_AppInfoReq, package_nub, package_now, time, RpCount,  RpOff,
                                 PvCount,  PvOff, buffer, mInverterDetail);
#endif
}
/***********************************************
** Function name:       pb_AppRealDataReq_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppRealDataRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_RealDataRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        AppPackageNow = GetPackageNow;
        ReceivingSign = TAG_AppRealDataRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_AppRealDataRes
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
#ifdef DTU3PRO
u16 pb_AppRealDataReq(u8 package_nub, u8 package_now, u32 time,
                      u16 MeterCount, u16 MeterOff,
                      u16 RpCount, u16 RpOff,
                      u16 PvCount, u16 PvOff, char *buffer)
#else
u16 pb_AppRealDataReq(u8 package_nub, u8 package_now, u32 time,
                      u16 RpCount, u16 RpOff,
                      u16 PvCount, u16 PvOff, char *buffer)
#endif
{
    char *pb_buffer = NULL;
    vu16  pb_len;
    vu16  len = 0;
    vu16  crc16;
    pb_buffer = mymalloc(4096);
    //    if(pb_buffer == NULL)
    //    {
    //        return 0;
    //    }
    memset(pb_buffer, 0, 4096);
    buffer[len++] = 'H';
    buffer[len++] = 'M';
    buffer[len++] = (char)(TAG_AppRealDataReq / 256);
    buffer[len++] = (char)(TAG_AppRealDataReq % 256);
    buffer[len++] = (char)(ServerSeq / 256);
    buffer[len++] = (char)(ServerSeq % 256);
#ifndef DTU3PRO
    pb_len = RealDataReq_Encode(package_nub, package_now, time, RpCount, RpOff, PvCount, PvOff, pb_buffer);
#else
    pb_len = (u16)RealDataReq_Encode(package_nub, package_now, time, MeterCount, MeterOff, RpCount, RpOff, PvCount, PvOff, pb_buffer);
#endif
    //增加校验PB包
    crc16 = (u16)MyCrc16((unsigned char *)pb_buffer, pb_len);
    buffer[len++] = (char)(crc16 / 256);
    buffer[len++] = (char)(crc16 % 256);
    buffer[len++] = (char)((pb_len + 10) / 256);
    buffer[len++] = (char)((pb_len + 10) % 256);
    memcpy((u8 *)&buffer[len], pb_buffer, pb_len);
    len += pb_len;
    myfree(pb_buffer);
    return len;
}
/***********************************************
** Function name:       pb_AppCommandReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppCommandReq(char *buffer, u32 package_now)
{
    return Encode_CommandReq(TAG_AppCommandReq, buffer, package_now);
}
/***********************************************
** Function name:       pb_AppConfigCommandRes_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppCommandRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_CommandRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        AppPackageNow = GetPackageNow;
        ReceivingSign = TAG_AppCommandRes;
    }
    else
    {
        ReceivingSign = 0;
    }

#ifdef DEBUG11
    printf("ReceivingSign:%x\r\n", ReceivingSign);
#endif
}
/***********************************************
** Function name:       pb_AppCommandResStatus
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppCommandStatusReq(char *buffer, u8 package_nub, u8 package_now)
{
    return Encode_CommandStatusReq(TAG_AppCommandStatusReq, buffer, package_nub, package_now);
}
/***********************************************
** Function name:       pb_AppCommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppCommandResStatus_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_CommandStatusResDTO(buffer, pb_len) == 1)
    {
        ReceivingSign = TAG_AppCommandStatusRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_AppHBReq
** Descriptions:        c->s心跳包打包
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppHBReq(char *buffer)
{
    return Encode_HBReq(TAG_AppHBReq, buffer);
}
/***********************************************
** Function name:       pb_AppHBRes_Analysis
** Descriptions:        s->c心跳包解析
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppHBRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_HBRes(buffer, pb_len) == 1)
    {
        ReceivingSign = TAG_AppHBRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_AppDevConfigFetchReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppDevConfigFetchReq(char *buffer, u8 *package_nub, u8 package_now)
{
    return Encode_DevConfigFetchReqDTO(TAG_AppDevConfigFetchReq, buffer, package_nub, package_now);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppDevConfigFetchRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_DevConfigFetchResDTO(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        AppPackageNow = GetPackageNow;
        ReceivingSign = TAG_AppDevConfigFetchRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_AppDevConfigPutReq
** Descriptions:        s->c
** input parameters:    无
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppDevConfigPutReq(char *buffer, u8 package_now)
{
    return Encode_DevConfigPutReqDTO(TAG_AppDevConfigPutReq, buffer, status, package_now);
}
/***********************************************
** Function name:       pb_CommandResStatus_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppDevConfigPutRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_DevConfigPutResDTO(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        AppPackageNow = GetPackageNow;
        ReceivingSign = TAG_AppDevConfigPutRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_AppWarnReq
** Descriptions:        s->c
** input parameters:    Device 0：DTU 1：微逆
** output parameters:   *buffer 打包后的数据
** Returned value:      长度
*************************************************/
u16 pb_AppWarnReq(char *buffer, u8 Device)
{
    return 0;//Encode_WarnReq(TAG_AppWarnReq, buffer, Device);
}
/***********************************************
** Function name:       pb_AppWarnRes_Analysis
** Descriptions:        s->c
** input parameters:    *buffer 数据 pb_len 长度
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_AppWarnRes_Analysis(char *buffer, u16 *pb_len)
{
    if(Decode_WarnRes(buffer, pb_len) == 1)
    {
        package_Num = GetPackageNum;
        PackageNow = GetPackageNow;
        ReceivingSign = TAG_AppWarnRes;
    }
    else
    {
        ReceivingSign = 0;
    }
}
/***********************************************
** Function name:       pb_receive
** Descriptions:        服务器总接收
** input parameters:    *buffer 数据
** output parameters:   无
** Returned value:      无
*************************************************/
void pb_receive(char *buffer)
{
    char *pb_buffer = NULL;
    vu16  pb_len = 0;
    vu16  len = 0;
    vu16  crc16;
    vu16  tag;

    //先判断HM开头
    if((buffer[0] != 'H') || (buffer[1] != 'M'))
    {
        return;
    }

    //计算接收包总长度
    len = buffer[8] * 256 + buffer[8 + 1];
    //计算PB包长度
    pb_len = len - 10;

    if(pb_len <= (strlen(buffer) - 10))
    {
        //MyCRC16
        crc16 = (u16)MyCrc16((u8 *)&buffer[10], pb_len);
    }
    else
    {
        return;
    }

    if(crc16 != buffer[6] * 256 + buffer[6 + 1])
    {
        return;
    }

    pb_buffer = mymalloc(pb_len);
    //    if(pb_buffer == NULL)
    //    {
    //        return;
    //    }
    memset(pb_buffer, 0, pb_len);
    memcpy(pb_buffer, &buffer[10], pb_len);
    //计算命令码
    tag = buffer[2] * 256 + buffer[2 + 1];
#ifdef DEBUG11
    printf("TAG:%x\r\n", tag);
#endif
#ifdef DTU3PRO
#ifdef DEBUG

    if(Netmode_Used  != CABLE_MODE)
    {
        vu32 a;
        printf("receive: %d\r\n", len);

        for(a = 0; a < len; a++)
        {
            printf("%02x ", buffer[a]);
        }

        printf("\r\n");
    }

#endif
#endif

    switch(tag)
    {
        case TAG_HBRes:
            pb_HBRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_InfoDataRes:
            pb_InfoDataRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_RealDataRes:
            pb_RealDataRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_HistoryDataRes:
            pb_HistoryDataRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_CommandRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_CommandRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_CommandStatusRes:
            pb_CommandStatusRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_DevConfigFetchRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_DevConfigFetchRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_DevConfigPutRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_DevConfigPutRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_WarnRes:
            pb_WarnRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_WarnInfoRes:
            pb_WarnInfoRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_WaveRes:
            pb_WaveRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppInfoRes:
            pb_AppInfoRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppRealDataRes:
            pb_AppRealDataRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppWarnRes:
            pb_AppWarnRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppCommandRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_AppCommandRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppCommandStatusRes:
            pb_AppCommandResStatus_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppHBRes:
            pb_AppHBRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppDevConfigFetchRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_AppDevConfigFetchRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppDevConfigPutRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_AppDevConfigPutRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppGetConfigRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_AppGetConfigRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        case TAG_AppSetConfigRes:
            //计算命令码
            ServerSeq = buffer[4] * 256 + buffer[4 + 1];
            pb_AppSetConfigRes_Analysis(pb_buffer, (u16 *)&pb_len);
            break;

        default :
            break;
    }

    myfree(pb_buffer);
}
