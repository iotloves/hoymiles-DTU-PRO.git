#include "stdio.h"
#include "malloc.h"
#include "string.h"
#include "NetProtocolTools.h"
#include "Memory_Tools.h"
/***********************************************
** Function name:       encode_bytes
** Descriptions:        字节型数据处理
** input parameters:    pb_ostream_t *stream, const pb_field_t *field, void *const *arg
** output parameters:   无
** Returned value:      无
*************************************************/
bool encode_bytes(pb_ostream_t *stream, const pb_field_t *field, void *const *arg)
{
    char *str = *arg;

    if(!pb_encode_tag_for_field(stream, field))
    {
        return false;
    }

    return pb_encode_string(stream, (u8 *) str, strlen(str));
}
/***********************************************
** Function name:       decode_bytes
** Descriptions:        字节型数据处理
** input parameters:    pb_istream_t *stream, const pb_field_t *field, void **arg
** output parameters:   无
** Returned value:      无
*************************************************/
bool decode_bytes(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
    volatile int i = 0;
    char *tmp = *arg;

    while(stream->bytes_left)
    {
        volatile uint64_t value;

        if(!pb_decode_varint(stream, (uint64_t *)&value))
        {
            return false;
        }

        *(tmp + i) = (char)value;
        i++;
    }

    return true;
}

/***********************************************
** Function name:       encode_string
** Descriptions:        字节型数据处理
** input parameters:    pb_ostream_t *stream, const pb_field_t *field, void *const *arg
** output parameters:   无
** Returned value:      无
*************************************************/
bool encode_string(pb_ostream_t *stream, const pb_field_t *field, void *const *arg)
{
    char *str = *arg;

    if(!pb_encode_tag_for_field(stream, field))
    {
        return false;
    }

    return pb_encode_string(stream, (u8 *) str, strlen(str));
}

/***********************************************
** Function name:       decode_string
** Descriptions:        字节型数据处理
** input parameters:    pb_istream_t *stream, const pb_field_t *field, void **arg
** output parameters:   无
** Returned value:      无
*************************************************/
bool decode_string(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
    volatile int i = 0;
    char *tmp = *arg;

    while(stream->bytes_left)
    {
        volatile uint64_t value;

        if(!pb_decode_varint(stream, (uint64_t *)&value))
        {
            return false;
        }

        *(tmp + i) = (char)value;
        i++;
    }

    return true;
}

u8 NetURLResolution(char *WebAddr, u8 *domain, u8 *ip, uint16_t *port, u8 *url, u8 *filename) //网址解析
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 k = 0;
    vu8 l = 0;
    vu8 urlmode = 0;
    vu8 getport = 0;
    volatile int port_new = 0;
    char *get = NULL;
    char *URL = NULL;
    vu8 IPorURL = 0;
    get = mymalloc(20 * sizeof(char));
    //    if(get == NULL)
    //    {
    //        return 0;
    //    }
    memset(get, 0, 20 * sizeof(char));
    URL = mymalloc(20 * sizeof(char));
    //    if(URL == NULL)
    //    {
    //        myfree(get);
    //        return 0;
    //    }
    memset(URL, 0, 20 * sizeof(char));
    memset(ip, 0, sizeof(*ip));
    memset(port, 0, sizeof(*port));
    memset(url, 0, sizeof(*url));
    memset(filename, 0, sizeof(*filename));
    memset(domain, 0, sizeof(*domain));
#ifdef DEBUG
    printf("WebAddr %s\n", WebAddr);
#endif

    if(strncmp(&WebAddr[i], "http://", 7) == 0)
    {
        i += 7;
        l = i;
    }

    if((strchr(WebAddr + i, ':') != NULL) || (strchr(WebAddr + i, '/') != NULL))
    {
        for(j = i; i < strlen(WebAddr); i++)
        {
            if((WebAddr[i] == '.') || (WebAddr[i] == ':') || (WebAddr[i] == '/'))
            {
                if(IPorURL == 0)
                {
                    sscanf(get, "%d", (int *)&ip[k]);
                    memset(get, 0, 20 * sizeof(char));
                    k++;
                    j = i + 1;

                    if((WebAddr[i] == ':') || (WebAddr[i] == '/'))
                    {
                        if(WebAddr[i] == ':')
                        {
                            i++;
                            getport = 1;
                            break;
                        }
                        else
                        {
                            getport = 0;
                            break;
                        }
                    }
                }
                else if((WebAddr[i] == ':') || (WebAddr[i] == '/'))
                {
                    memcpy(domain, &WebAddr[j], i - j);
                    urlmode = 1;

                    if(WebAddr[i] == ':')
                    {
                        i++;
                        getport = 1;
                        break;
                    }
                    else
                    {
                        getport = 0;
                        break;
                    }
                }
            }
            else if(WebAddr[i] >= '0' && WebAddr[i] <= '9')
            {
                IPorURL = 0;
                urlmode = 0;
                get[i - j] = WebAddr[i];
            }
            else
            {
                IPorURL = 1;
            }
        }
    }

    if(getport == 1)
    {
        for(j = i; i < strlen(WebAddr); i++)
        {
            if(WebAddr[i] == '/')
            {
                sscanf(get, "%d", &port_new);
                *port = (uint16_t)port_new;
                memset(get, 0, 20 * sizeof(char));
                break;
            }

            get[i - j] = WebAddr[i];
        }
    }
    else
    {
        *port = 80;
    }

    for(j = i; i < strlen(WebAddr); i++)
    {
        if(WebAddr[i] == ',')
        {
            memcpy(url, WebAddr + j, i - j);
            i++;
            break;
        }
    }

    for(j = i; i < strlen(WebAddr); i++)
    {
        if(WebAddr[i] == '\r')
        {
            break;
        }
    }

    if(WebAddr[i] == '\r')
    {
        memcpy(filename, WebAddr + j, i - j);
    }
    else
    {
        memcpy(filename, WebAddr + j, strlen(WebAddr) - j);
    }

    myfree(get);
    myfree(URL);
    return urlmode;
}
