#ifndef _MAIN_H_
#define _MAIN_H_

#include "stm32f4xx.h"
#include "stm32f4x7_eth.h"
#include "usart.h"
#include "netconf.h"
#include "SysTick.h"
#include "Ethernet.h"
#include "ff.h"
#include "led.h"
#include "key.h"
#include "pwr.h"
#include "rtc.h"
#include "stdio.h"
#include "string.h"
#include "malloc.h"
#include "Memory.h"
#include "sdio_sd.h"
#include "littlefs.h"
#include "BackupSRAM.h"
#include "http_client.h"
#include "tcp_echoclient.h"
#include "usart_nrf.h"
#include "usart_wifi.h"
#include "usart_gprs.h"
#include "wifi.h"
#include "gprs.h"
#include "HW_Version.h"
#include "NetProtocol.h"
#include "TimeProcessing.h"
#include "fatfs.h"
#include "AntiReflux.h"
#include "usbh_usr.h"
#include "usb_bsp.h"
#include "iwdg.h"
#include "MODBUS485.h"
#define ResetSwitch            true
#define ResetHours             1
#define ResetMinutes           1
#define ResetSeconds           1
#define IWDGTomes              5000
//#ifndef DTU_IAP
//#define TimeTest
//#endif

//#define USE_IAP_HTTP
//#define USE_IAP_TFTP
#endif
