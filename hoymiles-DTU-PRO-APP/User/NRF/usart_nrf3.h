#ifndef __USART_NRF3_H
#define __USART_NRF3_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#define UsartNrf USART3
#else
#include  "stm32f10x.h"
#endif
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <string.h>
#include "rtc.h"
#include "SysTick.h"

/* 轮询、轮询软件版本，升级程序，command，防盗轮询，防盗设置，锁MI,解锁MI,限功率，配置并网文件，空中波特率切换*/
#define ANSWER_EXCEPTION_ONE_MULTI 0XFF
#define ANSWER_EXCEPTION_MULTI_ONE 0xF1
#define REQ_ARW_DAT_ALL  0X15
#define PARASET_ALL                     0X52
#define ANSWER_PARASET_ALL    (PARASET_ALL|0X80)
#define DEVCONTROL_ALL         0x51
#define ANSWER_DEVCONTROL_ALL     (DEVCONTROL_ALL|0X80)
#define ANSWER_REQ_ARW_DAT_ALL  (REQ_ARW_DAT_ALL|0X80)
#define REQ_VERSISON                0x0f               //查询终端软硬件信息
#define ANSWER_REQ_VERSISON          (REQ_VERSISON | 0x80)            //8F//
#define MAX_SUB_DAT_DATA_LEN                                64
#define MAX_TRANS_DAT_DATA_LEN                              16
//微逆连接状态 用于本地
#define MI_CONNECT 0X02
#define MI_DISCONNECT 0XFD

//微逆连接状态用于历史发电量异常标志
#define MI_TOTAL_ENERGY_FAULT 0X04
#define MI_TOTAL_ENERGY_FAULTCLEAR 0XFB

//微逆连接状态 用于服务器端
#define MI_SERVER_CONNECT 0X01
#define MI_SERVER_DISCONNECT 0XFE

//微逆两包历史发电量之间的差值
#define PV_ONE_DAY_TOTAL_ENERGY 2000

extern vu32 LineCnt_Pro;
//多发单收
extern vu8 SubData3[MAX_TRANS_DAT_DATA_LEN];
extern vu8 CurSendRecLastPackageNO;
extern vu8 CurSendRecLostPackageNO;
//单发多收
typedef enum
{
    SendInitPackage = 0x00,
    SendFirstPackage = 0x01,
    SendLastPackage = 0x02,
    SendLostPackage = 0x03,
} SendPackageState;
extern vu8 CurRecSendPackageDataType;
extern vu8 TotalPackageNum;
extern vu8 CurRecSendLostPackageNO;
extern vu32 CurRecSendLostPackageState;
extern vu16 PortNO_App;
extern vu16 PortNO_Alarm_Server;
extern vu8 InformAlarmDataNO;

typedef union
{
    struct
    {
        //dong 20200520
        u16 WCode;
        u8 Alarm_Id[6] ;
        u8 WNum[2];
        u8 WTime1[4];
        u8 WTime2[4];
        u8 Data1[2];
        u8 Data2[2];
    } Data;
    u8 DataMsg[22];
} AlarmDataType;
typedef struct
{
    u8 Alarm_Id[6] ;
    //dong 20200520
    u16 WCode;
    u8 WNum[2];
    u8 WTime1[4];
    u8 WVDataL[2];
    u8 WPos[2];
    u8 WVData[1200];//录波数据暂定1200个
} WaveDataType;

//CurAlarmState -- 告警状态类型
typedef enum
{
    InitState = 0,
    //有新的告警记录
    HasNewAlarmRecord = 1,
    //有新的录波告警
    HasNewWaveRecord = 2,
    //无新的告警记录
    HasNONewAlarmRecord = 3,
    //无新的录波告警
    HasNONewWaveRecord = 4,
    //挂起告警
    AlarmInforUpdate_Server = 10,
    //APP 状态切换
    AlarmInforUpdate_App = 11,
} AlarmStateType;
extern volatile AlarmStateType CurAlarmState;
typedef enum
{
    InverterDevInform_Simple = 0,
    InverterDevInform_All = 1,
    GridOnProFilePara = 2,
    HardWareConfig = 3,
    SimpleCalibrationPara = 4,
    SystemConfigPara = 5,
    RealTimeRunData_Debug = 11,
    RealTimeRunData_Reality = 12,
    RealTimeRunData_A_Phase = 13,
    RealTimeRunData_B_Phase = 14,
    RealTimeRunData_C_Phase = 15,
    //告警数据-所有未发送告警
    AlarmData = 17,
    //告警数据-全部挂起告警
    AlarmUpdate = 18,
    RecordData = 19,
    InternalData = 20,
    GetLossRate = 21,
    //dong 2020-06-15
    GetSelfCheckState = 30,
    InitDataState = 0xff,

} DataType;


typedef enum
{
    Type_TurnOn = 0,
    Type_TurnOff = 1,
    Type_Restart = 2,
    Type_Lock = 3,
    Type_Unlock = 4,
    Type_ActivePowerContr = 11,
    Type_ReactivePowerContr = 12,
    Type_PFSet = 13,
    Type_CleanState_LockAndAlarm = 20,
    //dong 06-15
    Type_SelfInspection = 40,//并网保护文件自检
    Type_Init = 0xff,
} DevControlType;

typedef enum
{
    SysParaSet = 0,
    EleEnergySet = 1,
    TempSampleCalibration = 2,
    PortEleParaCalibration = 3,
    AntitheftParaSet = 4,
} ParaSetType;
// AppState
typedef enum
{
    //单个微逆扫描
    PollSigleMi = 1,
    //多个微逆扫描
    PollAllMi = 2,
    //dong ????
    SigleMiLossRate = 3,
    //多个丢包率统计
    AllMiLossRate = 4,
    //单个丢包率统计
    ManuSigleLossRate = 5,
    //网络命令模式
    OtherNetCmdState = 100,
} App_DataPollState;


//
typedef  union
{
    struct
    {
        vu16 code;                  //小数位数及自检结果编码
        vu16 dflt_val;              //法规保护点
        vu16 dflt_tim;              //法规保护动作时间
        vu16 rslt_val;              //实测保护点
        vu16 rslt_tim;              //实测保护动作时间
    }
    Property;
    vu8 Data[10];
} STVal;

typedef  union
{
    struct
    {
        vu8 pv_sn[6];               //微逆 ID
        vu16 ver;                   //数据版本
        vu16 st;                    //测试状态
        vu16 gpf;                   //并网规则文件代码
        vu16 gpf_ver;               //并网规则文件版本
        STVal hv1_stval;            //一级过压自检测试结果
        STVal lv1_stval;            //一级欠压自检测试结果
        STVal hv2_stval;            //二级过压自检测试结果
        STVal lv2_stval;            //二级欠压自检测试结果
        STVal hf1_stval;            //一级过频自检测试结果
        STVal lf1_stval;            //一级欠频自检测试结果
        STVal hf2_stval;            //二级过频自检测试结果
        STVal lf2_stval;            //二级欠频自检测试结果
    }
    Property;
    vu8 Data[94];
} GPSTVal;

extern volatile App_DataPollState AppState;
extern bool UsartNrf3_AlarmInformationSearch(void);
extern bool UsartNrf3_PollRealTimeWithVersion(void);

//三代协议轮序NRF版本号
u8 UsartNrf3_Send_Mi_Nrf_VER(u8 *target_adr, u8 *rout_adr);

//单包多收发送打包
extern u8 UsartNrf3_Send_PackPollSigleDataCommand(u8 *target_adr, u8 *rout_adr, u8 FrameNO);
//多发单收
extern u8 UsartNrf3_Send_PackPollMultiDataCommand(u8 *target_adr, u8 *rout_adr, u8 DataType, u8 Gap, u8 *Password);
extern bool UsartNrf3_HasPollCurInverterPort(void);
extern void UsartNrf3_Send_PackPollVersionCmd(void);
extern void UsartNrf3_Process_Pro(u8 *pBuffer);
extern bool HasSendCurFirstPackage;
extern bool UsartNrf3_HasSetCurrentInverterOk(void);
extern void UsartNrf3_Send_PackNrfCmd(void);
extern void UsartNrf3_Send_NetCmdToNrfCmd(void);

extern void UsartNrf3_Send_DownLoadDat(void);
extern void UsartNrf3_Send_DownLoadPro(void);
extern u8 UsartNrf_Send_PackBaseCommand(u8 *target_adr, u8 *rout_adr, u8 cmd, u8 dat);
extern bool UsartNrf3_SendLoop_ReplyOK(void);
extern void UsartNrf3_Send_ParaSet(void);
extern void UsartNrf3_SendLoop_SetTaskTimeOut(bool Stop);
extern void UsartNrf3_Process_One_MultiPackage(u8 *pBuffer);
//搜索下一个微逆
extern void UsartNrf3_Send_SearchNextPannelOrInv(void);

extern void UsartNrf3_Process_AlarmFlashWriteEvery15Min(void);
extern void UsartNrf3_Process_LedShow(bool RealTimeState);
extern void UsartNrf3_Process_ClearMIVersion(void);
//dong 2020-06-15
//微逆自检状态处理
void UsartNrf3_Process_Self_Check_State(u8 *pBuffer);
// Function name: 三代协议轮询全部告警传输应用层回执处理
void UsartNrf3_Process_DevInform_Alarm(u8 *pBuffer);
//告警数据-全部挂起告警
void UsartNrf3_Process_DevInform_InformUpdate(u8 *pBuffer);
//单发多收机制的接受传输层（合包）
uint16_t UsartNrf3_Process_GetDatatoProBuffer(u8 *pBuffer, u16 BufferSize);
// 处理实时数据
void  UsartNrf3_Process_DevRunReality(u8 *pBuffer);
//D1命令 设备控制应答处理
void UsartNrf3_Process_DevControl(u8 *pBuffer);
//三代协议设备控制应用层发送
void UsartNrf_Send_DevControlUpdate(u8 *target_adr, u8 *rout_adr);
//三代协议设备控制传输层发送
void UsartNrf3_Send_DevControlUpdate(u8 *target_adr, u8 *rout_adr);
//三代协议设备控制传输应用层发送
extern void UsartNrf3_Send_DevControl(void);
//三代协议设备控制打包
u8 UsartNrf3_Send_PackDevControl(u8 *target_adr, u8 *rout_adr, u16 ControlMode, u8 *ControlData, u8 nub, u8 Num);
//typedef enum
//{
//  IdleState = 0,
//  WriteFlashState = 1,
//  PollMulPackageState = 2,
//}FlashLockState;

typedef enum
{
    InitLayer = 0,
    ToTransLayer_Send = 1,
    ToAppLayer = 2,
    ToTransLayer_Process = 3,

} ProtocalLayerType;
typedef struct
{
    s16 MI_CF_Num_End;
    s16 MI_RF_Num_End;
    s16 MI_CF_Num_Start;
    s16 MI_RF_Num_Start;
} DataCntType;
extern ProtocalLayerType ProtocalLayer_Poll, ProtocalLayer_Cmd;
//extern  char TempRowData[];
extern volatile bool CurPollIsDebugDataTime;
extern volatile bool CurPollIsAllDataTime;
extern volatile AlarmDataType pInformAlarm[];
//extern vu16 CurInformAlarmNum;
extern vu8 CurRowTotalLen;
extern vu32 LocalTime_TaskTimeOut;
extern vu32 NeedLostPackageState;
extern bool UsartNrf3_AppStateSwitch(u8 TotalCnt, u8 SucCnt);
//extern bool LockFlag;
#endif
