#ifndef __USART_NRF_H
#define __USART_NRF_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#define UsartNrf USART3
#else
#include  "stm32f10x.h"
#define UsartNrf USART2
#endif
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <string.h>
#include "rtc.h"
#include "SysTick.h"
#define  DTUID_READ_CNT                                         5
/* 轮询、轮询软件版本，升级程序，command，防盗轮询，防盗设置，锁MI,解锁MI,限功率，配置并网文件，空中波特率切换*/
#ifdef DTU3PRO
#define PORT_LEN                            100//500
#else
#define PORT_LEN                            100
#endif

#define EVERY_PORT_POWER                    300
#define STX                                 0x7e
#define ETX                                 0x7f
#define UART_LEN                            35
#define MAX_PRO_PACKAGE_ROW                 21
#define MAX_SUB_DATA_LEN                    64
#define BROADCAST                           0x02                        //查询网络终端设备ID
#define ANSWER_REQHOST                      (BROADCAST | 0x80 | 0x40)
#define ANSWER_BROADCAST                    (BROADCAST | 0x80)

#define BROADCAST_ON                        0x18                        //查询终端A路状态
#define ANSWER_BROADCAST_ON                 (BROADCAST_ON | 0x80)

#define REQ_NUB_BROADCAST                   0x03                        // 请求广发命令次数
#define ANSWER_REQ_NUB_BROADCAST            (REQ_NUB_BROADCAST | 0x80)  //83

#define REQ_RF_SVERSISON                    0x06                        //查询终端RF ID 软硬件版本号
#define ANSWER_REQ_RF_SVERSISON             (REQ_RF_SVERSISON | 0x80)   //0x86

#define REQ_RF_RVERSISON                    0x07                        //
#define ANSWER_REQ_RF_RVERSISON             (REQ_RF_RVERSISON | 0x80)   //0x87

//#define REQ_A_EVENT                       0x08                        //查询A端事件

#define REQ_A_DAT                           0x09                        //查询A端数据
#define ANSWER_REQ_A_DAT                    (REQ_A_DAT | 0x80)          //89
#define ANSWER_REQ_A_EVENT                  (0x08 | 0x80)               //88


#define REQ_GRIDFILE                        0X10
#define ANSWER_REQ_GRIDFILE                 REQ_GRIDFILE|0X80           //轮询并网保护文件

#define DOWN_DAT                            0x0a                        //下载并网配置文件
#define ANSWER_DOWN_DAT                     (DOWN_DAT | 0x80)           //8A

#define ANSWER_GDFAULT                      (GDFAULT | 0x80)            //8D

#define DOWN_PRO                            0x0e                        //下载程序命令
#define ANSWER_DOWN_PRO                     (DOWN_PRO | 0x80)           //8E
#define REQ_VERSISON                        0x0f                        //查询终端软硬件信息
#define ANSWER_REQ_VERSISON                 (REQ_VERSISON | 0x80)       //8F//
#define REQ_PARA_VERSION                    0x10                        //查询并网配置文件名称、版本号
#define ANSWER_REQ_PARA_VERSION             (REQ_PARA_VERSION | 0x80)   //90

#define REQ_B_DAT                           0x11                        //查询B端数据
#define ANSWER_REQ_B_DAT                    (REQ_B_DAT | 0x80)          //91
#define ANSWER_REQ_B_EVENT                  (0x12 | 0x80)               //92
#define ANSWER_WRITE_SEND_RFID              (WRITE_SEND_RFID | 0x80)    //9a
#define ANSWER_WRITE_RECV_RFID              (WRITE_RECV_RFID | 0x80)    //9b

#define CONTROL_LOCK_MI__LIMIT_POWER_ONOFF  0x51                        //加锁&限制功率
#define CONTROL_LOCK_MI_SUB                 0xa5a5
#define CONTROL_LIMIT_POWER_SUB             0x5a5a
#define CONTROL_ON_SUB                      0x55aa
#define CONTROL_OFF_SUB                     0xaa55

#define CONTROL_LOCK_SUB1                   0X6699

#define CONTROL_LOCK_SUB2                   0X9966

#define CONTROL_UNLOCK_SUB                  0X6969

#define ANSWER_CONTROL_LOCK_MI__LIMIT_POEWR_ONOFF (CONTROL_LOCK_MI__LIMIT_POWER_ONOFF | 0x80)

#define RE_FISRT__CONFIRM_SET_ENERGY        0x52

#define RE_FISRT_SET_ENERGY_SUB             0xa5                        //首次设置发电量子命令
#define RE_CONFIRM_SET_ENERGY_SUB           0x5a                        //重设发电量子命令

#define ANSWER_SET__CONFIRM_ENERGY          (RE_FISRT_SET_ENERGY_SUB | 0x80)

#define CLEAR_GFDI                          0X0D                        //清除GFDI
#define CLEAR_GFDI_SUB                      0X00
#define ANSWER_CLEAR_GFDI                   (CLEAR_GFDI | 0x80)

#define CHANGE_MOD_2M_250K                  0x01
#define ANSWER_CHANGE_MOD_2M_250K           (CHANGE_MOD_2M_250K | 0x80)

#define REQ_A1_DAT_1000                     0X36
#define ANSWER_REQ_A1_DAT_1000              (REQ_A1_DAT_1000 | 0x80)

#define REQ_B1_DAT_1000                     0X38
#define ANSWER_REQ_B1_DAT_1000              (REQ_B1_DAT_1000 | 0x80)

#define REQ_A2_DAT_1000                     0X37
#define ANSWER_REQ_A2_DAT_1000              (REQ_A2_DAT_1000 | 0x80)

#define REQ_B2_DAT_1000                     0X39
#define ANSWER_REQ_B2_DAT_1000              (REQ_B2_DAT_1000 | 0x80)

#define REQ_GET_DTU_PARA_SUB                0X01
#define REQ_GET_DTU_PARA                    0X30
#define ANSWER_REQ_GET_DTU_PARA  (REQ_GET_DTU_PARA| 0X80)
#define ANSWER_REQ_GET_DTU_PARA_SUB         0x01
typedef enum
{
    InitInverterType = 0,
    Inverter_250 = 1,
    Inverter_500 = 2,
    Inverter_1000 = 4,
    Inverter_Pro = 7,
    Inverter_HM_OneToOne = 8,
    Inverter_HM_OneToTwo = 9,
    Inverter_HM_OneToFour = 10,
} InverterType;
typedef enum
{
    Device_250K = 1,
    Device_2M = 0,
    Device_INIT_BAUD = 3
} BaudType;
typedef enum
{
    RF = 0,
    REPEAT433 = 1
} DataSourceType;
typedef struct
{
    vu8 tm_year;
    vu8 tm_mon;
    vu8 tm_mday;
    vu8 tm_hour;
    vu8 tm_min;
    vu8 tm_sec;
} RTC_TIME;
typedef enum
{
    NET_NOCMD                   = 0,            //无网络命令
    NET_EXECUTION_COMPLETED     = 100,          //有网络命令执行完成
    NET_NOT_EXECUTED            = 200,          //有网络命令未执行
    NET_EXECUTION_FAILURE       = 201,          //有网络命令执行失败
    LOCAL_NOT_EXECUTED          = 202,
    //1-99为网络命令执行进度
} NETSTATUS;
typedef enum
{
    NET_INIT                    = 0,
    NET_TURN_ON                 = 1,            //开机
    NET_TURN_OFF                = 2,            //关机
    NET_LIMIT_POEWR             = 3,            //限功率
    NET_SET_PASSWORD            = 4,            //防盗设置
    NET_ZERO_EXPORT             = 5,            //防逆流广发
    NET_DOWNLOAD_DAT            = 6,            //下载并网保护文件
    NET_DOWNLOAD_PRO            = 7,            //下载rf程序
    NET_CLEAN_GFDI              = 8,            //清接地故障
    NET_SEARCH_ID               = 9,            //搜索ID
    NET_SET_ENERGY              = 10,           //设置发电量
    NET_WAIT_DOWNLOAD_DAT       = 11,           //等待下载并网保护文件
    NET_WAIT_DOWNLOAD_PRO       = 12,           //等待下载rf程序
    NET_CANCEL_GUARD            = 13,           //取消防盗
    NET_LOCK                    = 14,           //加锁
    NET_UNLOCK                  = 15,           //解锁
    NET_TERMINAL_INFOR          = 16,           //终端设备信息
    NET_INVERTER_HW_INFOR       = 17,           //逆变器硬件信息
    NET_RESTART                 = 18,           //重启
    NET_POLL_GRID_ON_FILE       = 19,
    NET_ABORTCONTROL            = 20,           //中止命令执行
    NET_LIMIT_ACTIVE_POEWR      = 24,
    NET_LIMIT_REACTIVE_POWER    = 25,
    NET_PF_SET                  = 26,
    NET_CLEAR_ALARM             = 27,
    //dong 2020-06-15 自检
    NET_SELF_INSPECTION         = 28,                       //微逆自检
    //NET_POLL_GRID_ON_PROFILE  = 28,
    NET_ELE_ENERGY              = 29,
    //NET_ANTI_THERFT_PARA      = 30,
    NET_SELF_STAUS              = 90,           //微逆自检状态
    NET_GET_LOSS_RATE           = 91,
    NET_POLL                    = 92,           //轮询
    NET_NOPORT                  = 93,           //无微逆id
    NET_ALARM_DATA              = 94,           //轮询完后要像版本轮询时一样清除完成标志
    NET_RECORD_DATA             = 95,
    NET_ALARM_UPDATE            = 96,
    NET_DTU_HW_INFOR            = 97,           //DTU硬件信息
    NET_SWITCH_BAUD             = 98,           //
    NET_DTU_HW_INFOR_RES        = 99,
    NET_REGISTER_ID             = 100,           //注册ID
    NET_MI_VERSION              = 101,

} NetCmdType;

typedef union
{
    struct
    {
        vu8  PVVol[2];                      //光伏电压
        vu8  PVCur[2];                      //光伏电流
        vu8  GridVol[2];                    //电网电压
        vu8  Freque[2];                     //频率
        vu8  Power[2];                      //功率
        vu8  Temper[2];                     //温度
        vu8  Collec_Time;                   //连续未采集到数据次数
        vu8  LinkState;                     //链接状态
        vu8  Run_Status[2];                 //微逆运行状态
        vu8  Fault_Code[2];                 //故障代码
        vu8  Fault_Num[2];                  //故障次数
        volatile NetCmdType NetCmd;         //网络命令
        vu32 Real_Time;                     //采样时间 时分秒
        vu8 NetStatus;                      //网络命令状态
        vu16 DailyEnergy;                   //日发电量
        vu16 HistoryEnergyH;                //历史总发电量高位
        vu16 HistoryEnergyL;                //历史总发电量低位
        vu16 DataVer[2];                    //
        vu8 PVPower[2];                     //
        vu8 GridActivePower[2];             //电网有功功率
        vu8 GridReactivePower[2];           //电网无功功率
        vu8 GridCurrent[2];                 //电网电流
        vu8 PowerFactor[2];                 //功率因数+

        vu8 BusVol[2];                      //
    } Data;
    vu8 DataMsg[52];
} InverterReal;


typedef enum
{
    MI_NO       = 0,
    MI_250W     = 0x01,
    MI_500W_A   = 0x02,
    MI_500W_B   = 0x03,
    MI_1000W_A  = 0x04,
    MI_1000W_B  = 0x05,
    MI_1000W_C  = 0x06,
    MI_1000W_D  = 0x07,

    Pro_A  = 0x08,
    Pro_B  = 0x09,
    Pro_C  = 0x0a,
    Pro_D  = 0x0b,

    HM_250W     = 0x0C,
    HM_500W_A   = 0x0D,
    HM_500W_B   = 0x0E,
    HM_1000W_A  = 0x0F,
    HM_1000W_B  = 0x10,
    HM_1000W_C  = 0x11,
    HM_1000W_D  = 0x12,
} PortType;


typedef union
{
    struct
    {
        vu16  USFWBuild_VER;                //bootloader 版本
        vu16  AppFWBuild_VER;               //应用程序版本
        vu16  AppFWBuild_YYYY;              //应用程序 创立时间
        vu16  AppFWBuild_MMDD;
        vu16  AppFWBuild_HHMM;
        vu16  AppFW_PNH;                    //软件料号
        vu16  AppFW_PNL;
        vu16  HW_VER;                       //硬件版本
        vu16  HW_PNH;                       //硬件料号
        vu16  HW_PNL;
        vu16  HW_FB_TLmValue;               //励磁电感的值
        vu16  HW_FB_ReSPRT;                 //谐振周期
        vu16  HW_GridSamp_ResValule;        //采用电阻值
        vu16  HW_ECapValue;                 //电解电容值
        vu16  Matching_AppFW_PNH;           //硬件匹配的软件料号
        vu16  Matching_AppFW_PNL;
        vu16  AppFW_MINVER;                 //硬件需要的软件最低版本号
        vu8   MIMod;                        //微逆型号
        vu16  Country_Std;                  //配置的国家
        vu16  Default_Version;              //配置的默认版本和保存版本
        vu16  Save_Version;                 //保存版本的版本号
        vu8   NRF_SoftVersion[4];           //逆变器NRF 软硬件版本号
        vu8   NRF_HardVersion[4];           //逆变器NRF 软硬件版本号
        vu16  HW_CRCValue;                  //校验值

        vu16  AppFWPN;                      //应用程序固件料号
        vu16  HWSPECVER;                    //硬件规格版本
        vu16  PDSPECL;                      //产品规格参数版本
        vu16 GPFCode;                       //并网保护文件代码
        vu16 GPFVer;                        //并网保护文件版本
        vu16 ReservedPara;
        vu16  CRC_ALL;
        vu8   rule_id;
    } Property;
    vu8 PropertyMsg[68];
} InverterDetail;

#pragma  pack(1)
typedef struct
{
    vu8 SetValut[2];
    vu8 Desc[2];
} PowerPFDevType;
typedef struct
{
    vu8 WCode[2];
} ClearAlarmDevType;
typedef struct
{
    vu8 Info[2];
    vu8 Data[16];
} EletricSetType;
typedef struct
{
    vu8 PWO[4];
    vu8 PWN[4];
    vu8 ATTime[2];
} PassWordSetType;
//dong 2020-06-15 并网保护文件自检参数
typedef struct
{
    u8 Ver[2];
    u8 GPF[2];
} SelfCheckParaType;
typedef union
{
    PowerPFDevType PowerPFDev;
    ClearAlarmDevType ClearAlarmDev;
    EletricSetType EletricSet;
    PassWordSetType PassWordSet;
    vu8 Data[18];                //参数设置
} PassValueType;
typedef union
{
    struct
    {
        vu8  Pre_Id[2];                     //
        vu8  Id[4];                         //网络命令指定Id +轮询Id+Search的Id
        PortType Port;                      //端口类型
        vu8  Id_In_Phase;                   //ID所在相位 1：A 2：B 3：C
        vu8  Acq_Switch;                    //微逆采集开关
        vu16 Power_Limit;                   //功率限制
        PassValueType Pass;
    } Property;                             //执行当前逆变器网络命令后，就会执行该逆变器的轮询
    vu8 PropertyMsg[30];
} InverterMajor;

#pragma  pack()
typedef union
{
    struct//开机时
    {
        //dtu连接服务器模式
        vu8  access_model;
        //旧密码
        vu8  LockOldPassword[4];
        //新密码
        vu8  LockNewPassword[4];
        //防盗设置时间时间
        vu8  Lock_Time[2];
        //防逆流
        vu8  Zero_Export_Switch;
        //无线发射地址
        vu8  Reflux_WirelessSenderAddr[3];
        //服务器上传间隔
        vu16 Server_SendTimePeriod;
        //功率值
        vu32 LimitPower_MyPower[3];
        //设置时间偏移
        vs32 timezone;
        //逆变器数量
        vu16 InverterNum;
        //组件数量
        vu16 PortNum;
        //DTU 当前发电总功率
        vu16 TotalPower;
        //DTU历史总发电量高位
        vu16 HistoryPowerH;
        //DTU历史总发电量低位
        vu16 HistoryPowerL;
        //该dtu下组件实时总功率
        vu16 Reflux_AllPanelPower;
        //DTU下的A相组件数
        vu16 Reflux_DtuPanelNum_A;
        //DTU下的B相组件数
        vu16 Reflux_DtuPanelNum_B;
        //DTU下的C相组件数
        vu16 Reflux_DtuPanelNum_C;
        //系统总组件数
        vu16 Reflux_SysPanelNum;
        //系统下A相组件数
        vu16 Reflux_SysPanelNum_A;
        //系统下B相组件数
        vu16 Reflux_SysPanelNum_B;
        //系统下C相组件数
        vu16 Reflux_SysPanelNum_C;
        //防盗开关
        vu8  Anti_Theft_Switch;

        vu8  CurMIRealTimePoll;
        vu8  LedState;
        //HasNotPolledVer = false;说明正在轮询版本号  HasNotPolledVer = true:说明轮询版本号结束
        vu8  PolledVerState;
        //防逆流电网类型
        vu8 GridType;
        //余电上网开关
        vu8 SurplusSwitch;
        //余电上网限制功率
        vs32 SurplusPowerA;
        //余电上网限制功率
        vs32 SurplusPowerB;
        //余电上网限制功率
        vs32 SurplusPowerC;
        //整体控制 0：总体控制 1：分相控制
        vu8 OverallControl;
        //存储位置
        vu8 StorageLocat;
#ifdef DTU3PRO
        //DHCP开关
        vu8 DHCP_Switch;
        //设置IP地址
        vu8 IP_ADDR[4];
        //子掩码
        vu8 subnet_mask[4];
        //默认网关
        vu8 default_gateway[4];
        //RS485模式
        vu8 RS485Mode;
        //RS485地址
        vu8 RS485Addr;
        //DRM限功率
        vu8 DRM_Limit_Switch;
        //相间平衡控制
        vu8 Phase_Balance_Switch;
        //相间容差值单位为w
        vu16 Tolerance_Between_Phases;
        //SunSpec模式
        vu8 SunSpec_Switch;
        //SunSpec起始地址
        vu8 SunSpec_Addr_Start;
    } Property;
    vu8 PropertyMsg[108];
#else
    } Property;
    vu8 PropertyMsg[84];
#endif
} DtuDetail;

typedef union
{
    struct
    {
        vu8 Pre_Id[2];
        vu8 Id[4];
        //DTU硬件版本
        vu8 DtuHw_Ver[2];
        //DTU软件版本
        vu8 DtuSw_Ver[2];
        //DTU RF 硬件版本号
        vu8 RfHw_Ver[4];
        //DTU RF 软件版本
        vu8 RfFw_Ver[4];
        //GPRS版本号
        vu8 Gprs_Ver[20];
        //GPRS卡号
        vu8 Gprs_SIMNum[28];
        //gprs信号强度
        vu8 GPRS_CSQ;
        //wifi版本号
        vu8 Wifi_Vsn[10];
        //路由器SSID
        vu8 wifi_ssid[40];
        //路由器密码
        vu8 wifi_passward[30];
        //wifi 信号强度
        vu8 wifi_RSSI;
        //服务器域名
        vu8 ServerDomainName[40];
        //APN
        vu8 APN[20];
        //服务器模式/app模式 是否成功
        vu8 LinkMode;

        vu8 DtuToolAddr[4];
        vu8 wifi_ssid_save_set;
        //服务器上传间隔
        vu8 server_send_time;
        //上网模式选择
        vu8 netmode_select;

        vu8 rule_id;
        //服务器端口号
        vu16 ServerPort;
        //APN2
        vu8 APN2[20];
    } Property;
    vu8 PropertyMsg[240];
} DtuMajor;




extern vu8  Uart_SendBuffer[UART_LEN];      //发送buffer
extern volatile InverterDetail MIDetail;                 //微型逆变器详细信息
extern volatile InverterReal MIReal[PORT_LEN];           //逆变器实时数据
extern volatile InverterMajor MIMajor[PORT_LEN];         //微逆组件
extern volatile DtuMajor Dtu3Major;                      //DTU主要信息
extern volatile DtuDetail Dtu3Detail;                    //DTU详细信息
extern vu16 PortNO;                         //组件编号
bool UsartNrf_NrfInit(void);                    //初始化nrf
void UsartNrf_MemInit(void);                    //初始化相关结构体和比阿娘
void UsartNrf_SendProcessLoop(void);            //发送接收轮询函数
void UsartNrf_ClearMIReal(void);                //清空MIReal结构体
void UsartNrf_ClearMI_DailyEnergy(void);        //清除当日发电量
void UsartNrf_ClearInverterMajor(void);         //清空InverterMajor结构体
void UsartNrf_Process_LedShow(bool PollRealTimeState, u8 NoConllectTime);
u16 UsartNrf_Get_crc_xor16(u16 *b_hex, u16 len);
u8 Get_crc_xor(u8 *b_hex, u16 len);
u8 ForwardSubstitution(u8 *result, u8 *source, u8 length);
u8 UsartNrf_Send_PackUpdateGridOnProFile(u8 *target_adr, u8 *rout_adr);
void UsartNrf3_Send_ConfigGridOnPre(void);
void UsartNrf_SendLoop_SetPackageTimeOut(bool Stop);
void UsartNrf_Send_PackUpdateMiProgram(u8 *target_adr, u8 *rout_adr);
u8 UsartNrf_Send_MiProgram_SigleFrame(u8 *target_adr, u8 *router_adr, u8 *dat, u8 nub, u8 len);
bool UsartNrf_HasSetCurrentInverterOk(void);
bool UsartNrf_SendLoop_GetNextSetCmd(void);
u8 UsartNrf_Backward_substitution1(u8 *result, u8 *source, u8 length);
void UsartNrf_GetDtu3PortNum(void);
void UsartNrf_InitPVPannelToMIMajor(void);
void UsartNrf_ClearMI_DailyEnergy(void);        //清除当日发电量
extern char *string;                            //并网保护文件数组
extern bool HasTempRegister;
extern NetCmdType CurNetCmd;
extern vu8  Uart_CurSendMainCmd;            //当前网络命令发送主命令
extern vu16 Uart_CurSendSubCmd;             //当前网络命令发送命令
extern vu8  Uart_CurRecMainCmd;             //当前网络命令回执主命令
extern vu16 Uart_CurRecSubCmd;              //当前网络命令回执子命令
extern vu8  Uart_SendBufferLen;             //当前发送字节
extern BaudType Uart_CurrentDtuBaud;            //当前DTU空中波特率
extern vu32  Uart_CurrentReplyState;         //当前回执状态
extern vu16 SubCmd;
extern vu8 MainCmd;
extern vu8  Uart_CurrentReplyAddr[4];       //下载的回执地址
extern vu8  Uart_CurSendAddr[4];

extern vu32 Index_Pro;
extern vu32 TotalIndex_Pro;
extern bool RightHexFile;                       //是否是hex文件偷
extern bool RightGridOnProFile;
extern u8 *CurRowData_Pro;
extern u8 *CurRowData_Dat;
extern vu32 LocalTime_TimeOut;
extern vu16 TotalIndex_Dat;
extern vu16 Index_Dat;
extern vu16  Uart_ReceiveBufferLen;         //当前接收字节
extern BaudType GetInveterBaudType(u8 *DeviceId);
extern u8 UsartNrf_Send_PackSetNrfBaudRateInTheAir(u8 *target_adr, u8 *rout_adr, u8 Cmd, BaudType Baud);
extern InverterType UsartNrf_GetInvterType(u8 *pId);
extern u8 UsartNrf_Send_PackGridOnProFile_SigleFrame(u8 *target_adr, u8 *router_adr, u8 *dat, u8 nub, u8 len);
extern u16 UsartNrf_Send_CRC16_Work(u8 *CRC_Buf, u16 CRC_Leni);
extern void UsartNrf_ClearVersionActionState(void);
extern void UsartNrf_Process_Version_InitInverterRf(void);
extern bool UsartNrf_Process_ExceptionFilter(u8 *pBuffer, u8 Head, u8 Tail, u32 MaxValue, s32 MinValue);
#define PORT_NUMBER_CONFIRMATION(x) (MIMajor[x].Property.Port == MI_250W) || (MIMajor[x].Property.Port == MI_500W_A) || (MIMajor[x].Property.Port == MI_1000W_A) ||(MIMajor[x].Property.Port == Pro_A)||(MIMajor[x].Property.Port == HM_250W)||(MIMajor[x].Property.Port == HM_500W_A)||(MIMajor[x].Property.Port == HM_1000W_A)
#define PORT_NUMBER_CONFIRMATION1(x) (MIMajor[x].Property.Port == MI_500W_B) || (MIMajor[x].Property.Port == MI_1000W_B) || (MIMajor[x].Property.Port == MI_1000W_C)|| (MIMajor[x].Property.Port == MI_1000W_D)
#define PORT_NUMBER_CONFIRMATION2(x) (MIMajor[x].Property.Port == MI_250W)||(MIMajor[x].Property.Port == MI_500W_B) || (MIMajor[x].Property.Port == MI_1000W_D) || (MIMajor[x].Property.Port == Pro_D)|| (MIMajor[x].Property.Port == HM_250W)||(MIMajor[x].Property.Port == HM_500W_B)||(MIMajor[x].Property.Port == HM_1000W_D)


#endif
