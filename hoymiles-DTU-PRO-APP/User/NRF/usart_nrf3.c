#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <string.h>
#include "rtc.h"
#include "SysTick.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#include "usart_nrfConfig.h"
#include "Memory.h"
#include <string.h>
#include "malloc.h"
#include "crc16.h"
//dong 2020-0515
vu8 RecieveDataCnt = 0;
vu8 AppPollCnt = 0;
//告警流水号
vu16 WarnSerNub[PORT_LEN];
vu16 DataAarnNub = 0;
//自检状态标志位 0为默认值，1为自检状态轮训中，2为自检状态轮训完成 3为准备打包回复状态
vu8 SelfCheckStateFlg = 0;
SelfCheckParaType SelfCheckPara = {0};
//
extern vu8 APP_Flg ;
//u8 *pProBuffer = NULL;
u8 pProBuffer[1500];
//实时告警包
volatile AlarmDataType pRealAlarm[20];
//挂起告警
volatile AlarmDataType pInformAlarm[20];
volatile AlarmStateType CurAlarmState = InitState;
//单发多收
volatile bool CurSendRecHasSupplyAgain = false;
vu8 TotalPackageNum = 0;


// APP 网络命令
volatile App_DataPollState AppState;


vu8 CurRecSendLostPackageNO = 0;
//多发单收
vu8 CurSendRecLostPackageNO = 0;
vu8 CurSendRecLastPackageNO = 0;
vu8 SubData3[MAX_TRANS_DAT_DATA_LEN];
vu8 CurRecSendPackageDataType = InitDataState;
vu8 RealAlarmDataNO = 0;
vu8 InformAlarmDataNO = 0;

vu8 CurRowTotalLen = 0;
vu8 TotalIndex_Para = 0;
vu8 Index_Para = 0;
//dong 2020-05-21 挂起告警的序号已未上传的告警序号为准
//vu16 CurRealAlarmNum = 0;
//vu16 CurInformAlarmNum = 0;
//告警多包机制
vu8 AlarmTwoPack = 0;
vu16 PortNO_App = 0;
vu16 PortNO_Alarm_Server = 0;
vu32 LineCnt_Pro = 0;
vu32 NeedLostPackageState = 0;
vu32 LocalTime_TaskTimeOut = 0;
vu32 CurRecSendLostPackageState = 0;
vu32 LocalTime_InformAlarmTimeOut = 0;
ProtocalLayerType ProtocalLayer_Poll;
ProtocalLayerType ProtocalLayer_Cmd;

volatile bool CurPollIsDebugDataTime = true;



//bool LockFlag  = false;
DataCntType *pDataCnt = NULL;

//char TempRowData[] =
//{
//    0x01, 0x22, 0x20, 0x02, 0x00, 0x00, 0x09, 0x60, 0x08, 0x40, 0x00, 0xD2, 0x0A, 0x50, 0x00, 0x82,
//    0x10, 0x00, 0x17, 0x70, 0x16, 0xda, 0x0b, 0xb8, 0x17, 0xa2, 0x0b, 0xb8, 0x30, 0x00, 0x00, 0x96,
//    0x40, 0x00, 0x07, 0xd0, 0x00, 0xc8, 0xb1, 0x12
//};
/**********************************************
** Function name:
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool FilterTotalEnergy(uint8_t u8min, uint8_t u8max, uint16_t PortNODebug)
{
    uint16_t i;
    int32_t TotalEnergyDiffTemp[2] = {0};
    TotalEnergyDiffTemp[0] = ((((uint32_t)MIReal[PortNODebug].Data.HistoryEnergyH) << 16) | ((uint32_t)MIReal[PortNODebug].Data.HistoryEnergyL));
    TotalEnergyDiffTemp[1] = (((uint32_t)(pProBuffer[u8min] << 8 | pProBuffer[u8min + 1]) << 16) | ((uint32_t)(pProBuffer[u8max - 1] << 8 | pProBuffer[u8max])));

    if((abs(TotalEnergyDiffTemp[1] - TotalEnergyDiffTemp[0])) > PV_ONE_DAY_TOTAL_ENERGY)
    {
        MIReal[PortNODebug].Data.LinkState = (MIReal[PortNODebug].Data.LinkState & MI_DISCONNECT) | MI_TOTAL_ENERGY_FAULT;
        MIReal[PortNODebug].Data.HistoryEnergyH = (uint32_t)pProBuffer[u8min] << 8 | pProBuffer[u8min + 1];
        MIReal[PortNODebug].Data.HistoryEnergyL = (uint32_t)pProBuffer[u8max - 1] << 8 | pProBuffer[u8max];
#ifdef DEBUG_ZHOULEI
        printf("error code3:");//发电量过大
        printf("HistoryEnergyHL:%x", ((((uint32_t)MIReal[PortNODebug].Data.HistoryEnergyH) << 16) | ((uint32_t)MIReal[PortNODebug].Data.HistoryEnergyL)));
        printf("\n");

        for(i = 0; i < 50; i++)
        {
            if(pProBuffer[i] <= 0x0f)
            {
                printf("0");
                printf("%x", pProBuffer[i]);
                printf(" ");
            }
            else
            {
                printf("%x", pProBuffer[i]);
                printf(" ");
            }
        }

        printf("\n");
#endif
        return false;
    }
    else
    {
        MIReal[PortNODebug].Data.LinkState = MIReal[PortNODebug].Data.LinkState & MI_TOTAL_ENERGY_FAULTCLEAR;
        return true;
    }

    return true;
}

/***********************************************
** Function name: 三代协议清微逆软件版本
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_ClearMIVersion(void)
{
    memset((u16 *) & (MIDetail.Property.USFWBuild_VER), 0, 2);
    memset((u16 *) & (MIDetail.Property.AppFWBuild_VER), 0, 2); //应用程序版本
    memset((u16 *) & (MIDetail.Property.AppFWBuild_YYYY), 0, 2);
    memset((u16 *) & (MIDetail.Property.AppFWBuild_MMDD), 0, 2);
    memset((u16 *) & (MIDetail.Property.AppFWBuild_HHMM), 0, 2);
    memset((u16 *) & (MIDetail.Property.AppFW_PNH), 0, 2);
    memset((u16 *) & (MIDetail.Property.AppFW_PNL), 0, 2);
    memset((u16 *) & (MIDetail.Property.HW_PNL), 0, 4); //硬件料号
    memset((u16 *) & (MIDetail.Property.HW_PNH), 0, 4); //硬件料号
    memset((u16 *) & (MIDetail.Property.HW_VER), 0, 2); //硬件版本
    memset((u16 *) & (MIDetail.Property.HW_FB_TLmValue), 0, 2);
    memset((u16 *) & (MIDetail.Property.HW_FB_ReSPRT), 0, 2);
    memset((u16 *) & (MIDetail.Property.HW_GridSamp_ResValule), 0, 2);
    memset((u16 *) & (MIDetail.Property.HW_ECapValue), 0, 2);
    memset((u16 *) & (MIDetail.Property.GPFCode), 0, 2); //并网保护文件代码
    memset((u16 *) & (MIDetail.Property.GPFVer), 0, 2); //并网保护文件版本
    memset((u16 *) & (MIDetail.Property.ReservedPara), 0, 2); //保留参数
    memset((u16 *) & (MIDetail.Property.AppFW_MINVER), 0, 2);
    memset((u16 *) & (MIDetail.Property.HW_CRCValue), 0, 2);
    memset((u16 *) & (MIDetail.Property.CRC_ALL), 0, 2);
}
/***********************************************
** Function name: 三代协议15min搜集微逆状态执行led状态
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
//void UsartNrf3_Process_LedShow(bool RealTimeState)
//{
//    u8 i;

//    if(RealTimeState == true)
//    {
//        Dtu3Detail.Property.LedState = 1;//搜不全
//    }
//    else
//    {
//        if(CurNetCmd == NET_NOPORT)
//        {
//            Dtu3Detail.Property.LedState = 0;//无组件
//        }
//        else
//        {
//            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
//            {
//                if((MIReal[i].Data.LinkState & MI_CONNECT) == 0)
//                {
//                    Dtu3Detail.Property.LedState = 1;//搜不全
//                    break;
//                }
//            }

//            if(i >= Dtu3Detail.Property.PortNum)
//            {
//                Dtu3Detail.Property.LedState = 2;    //搜全
//            }
//        }
//    }
//}

/***********************************************
** Function name: 三代协议单包多收接收机制
** Descriptions:
** input parameters:    接受的数据，最大缓存长度（多包和包）
** output parameters:   ?
** Returned value:  接受完成返回和包后的总长度，否则返回0
*************************************************/
u16 UsartNrf3_Process_GetDatatoProBuffer(u8 *pBuffer, u16 BufferSize)
{
    vu16 CurPackageNO;
    vu16 i;
    static vu16 TotalDataNum;
    vu16 TempDataNum = 0;

    if(CurRecSendLostPackageState == 0)
    {
        memset(pProBuffer, 0, (BufferSize * sizeof(u8)));
    }

    CurPackageNO = 0x0F & pBuffer[10];

    if(CurPackageNO > 0)
    {
        CurRecSendLostPackageState = CurRecSendLostPackageState | (1 << (CurPackageNO - 1));    //实际收取哪些包
    }
    else
    {
        return 0;
    }

    //第7位为1表示结束帧
    if((pBuffer[10] & 0x80) == 0x80)
    {
        TotalPackageNum = 0x0f & pBuffer[10]; //获取总包数

        for(i = 0; i < TotalPackageNum; i++)
        {
            NeedLostPackageState = NeedLostPackageState | (1 << i); //理应实现收取哪些包
        }

        for(i = 11; i < (16 + 11 + 2); i++)
        {
            //dong 2020-06-17  --- 遇到结束帧数据中有7F的提前结束了
            //            if((pBuffer[i] == ETX)
            if((pBuffer[i] == ETX) && (pBuffer[i + 1] == 0) && (pBuffer[i + 2] == 0))
            {
                *(pProBuffer + (16 * (CurPackageNO - 1)) + i - 11) = ETX;
                break;
            }
            else
            {
                *(pProBuffer + (16 * (CurPackageNO - 1)) + i - 11) = pBuffer[i];
            }
        }

        TotalDataNum = TotalDataNum + (i - 11);

        if(TotalDataNum >= 3)
        {
            TotalDataNum = TotalDataNum - 3;
        }
        //错误包
        else
        {
            NeedLostPackageState = 0;
            LocalTime_TaskTimeOut = LocalTime;
            CurRecSendLostPackageState = 0;
            TotalPackageNum = 0;
            CurRecSendPackageDataType = InitDataState;
            Uart_CurrentReplyState = 0;
            TotalDataNum = 0;
        }
    }
    //中间包
    else
    {
        memcpy(pProBuffer + (16 * (CurPackageNO - 1)), &pBuffer[11], 16);
        TotalDataNum = TotalDataNum + 16;
    }

    if(TotalPackageNum > 0)
    {
        if((NeedLostPackageState > 0) && (NeedLostPackageState == CurRecSendLostPackageState)) //是否收全
        {
            TempDataNum = TotalDataNum;
            TotalDataNum = 0;
            return TempDataNum;
        }
        else
        {
            CurRecSendLostPackageNO = TotalPackageNum;
        }
    }

    return 0;
}
/***********************************************
** Function name: 三代协议传输层数据信息crc校验
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_Process_DataCrc(u16 DataNum)
{
    vu16 i;
    vu16 DatCrc = 0xffff;
    vu16 TempCrc = 0;
    NeedLostPackageState = 0;
    CurRecSendLostPackageState = 0;

    for(i = 0; i < DataNum; i++)
    {
        if(i % 2 == 1)
        {
            TempCrc = (u16)((pProBuffer[i - 1] << 8) | (pProBuffer[i]));
            DatCrc =  CalcCRC16t(TempCrc, DatCrc);
        }
    }

    if(DatCrc == (((u16)pProBuffer[DataNum] << 8) | pProBuffer[DataNum + 1])) //crc校验
    {
        ProtocalLayer_Poll = ToAppLayer;
        TotalPackageNum = 0;
        CurRecSendPackageDataType = InitDataState;
        Uart_CurrentReplyState = 0;

        if((CurAlarmState != AlarmInforUpdate_App) && (CurNetCmd != NET_MI_VERSION))
        {
            CurNetCmd = NET_INIT;
        }

        return true;
    }
    else
    {
        ProtocalLayer_Poll = ToAppLayer;
        TotalPackageNum = 0;
        Uart_CurrentReplyState = 0x01 << 4; //高四位表示传输层回执异常状态
        return false;
    }

    return false;
}

/***********************************************
** Function name: 三代协议轮询实时数据实时版传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void  UsartNrf3_Process_DevRunReality(u8 *pBuffer)
{
    vu16 TempDataNum = 0;
    vu8 j;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 5 * 16);
    //dong
    vu8 alarm_nub = 0;

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToFour)
            {
                //                UsartNrf3_Process_LedShow(false);
                memcpy((u16 *) & (MIReal[PortNO].Data.DataVer[0]), &(pProBuffer[0]), 2); //数据版本

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVVol[0]), &(pProBuffer[2]), 2);  //PV1电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true)//PV电压
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVVol[0]), &(pProBuffer[2]), 2);  //PV2电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 4, 5, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVCur), &(pProBuffer[4]), 2);  //PV1电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 6, 7, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVCur), &(pProBuffer[6]), 2);  //PV2电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 8, 9, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVPower[0]), &(pProBuffer[8]), 2);  //PV1功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 10, 11, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVPower[0]), &(pProBuffer[10]), 2);  //PV2功率
                }
                else
                {
                    return;
                }

                if(FilterTotalEnergy(12, 15, PortNO) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 12, 15, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[12]) << 8 | pProBuffer[13]); //PV1历史累计发电量
                    MIReal[PortNO].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[14]) << 8 | pProBuffer[15]);
                }
                else
                {
                    return;
                }

                if(FilterTotalEnergy(16, 19, PortNO + 1) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 16, 19, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 1].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[16]) << 8 | pProBuffer[17]); //PV2历史累计发电量
                    MIReal[PortNO + 1].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[18]) << 8 | pProBuffer[19]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 20, 21, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.DailyEnergy = (u16)(((u16)pProBuffer[20]) << 8 | pProBuffer[21]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 22, 23, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 1].Data.DailyEnergy = (u16)(((u16)pProBuffer[22]) << 8 | pProBuffer[23]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 24, 25, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVVol[0]), &(pProBuffer[24]), 2); //PV3&PV4电压
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVVol[0]), &(pProBuffer[24]), 2); //PV3&PV4电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 26, 27, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVCur), &(pProBuffer[26]), 2);  //PV3电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 28, 29, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVCur), &(pProBuffer[28]), 2);  //PV4电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 30, 31, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVPower[0]), &(pProBuffer[30]), 2);  //PV3功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 32, 33, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVPower[0]), &(pProBuffer[32]), 2);  //PV4功率
                }
                else
                {
                    return;
                }

                if(FilterTotalEnergy(34, 37, PortNO + 2) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 34, 37, (u32)(pow(10, 9)), 0) == true)  //
                {
                    MIReal[PortNO + 2].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[34]) << 8 | pProBuffer[35]); //PV3历史累计发电量
                    MIReal[PortNO + 2].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[36]) << 8 | pProBuffer[37]);
                }
                else
                {
                    return;
                }

                if(FilterTotalEnergy(38, 41, PortNO + 3) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 38, 41, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 3].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[38]) << 8 | pProBuffer[39]); //PV4历史累计发电量
                    MIReal[PortNO + 3].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[40]) << 8 | pProBuffer[41]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 42, 43, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 2].Data.DailyEnergy = (u16)(((u16)pProBuffer[42]) << 8 | pProBuffer[43]); //PV3日发电量
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 44, 45, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 3].Data.DailyEnergy = (u16)(((u16)pProBuffer[44]) << 8 | pProBuffer[45]); //PV4日发电量
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 46, 47, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridVol[0]), &(pProBuffer[46]), 2);  //交流电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 48, 49, 20000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Freque[0]), &(pProBuffer[48]), 2);  //交流频率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 50, 51, 40000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridActivePower[0]), &(pProBuffer[50]), 2);  //交流有功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 52, 53, 40000, -40000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridReactivePower[0]), &(pProBuffer[52]), 2);  //无功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 54, 55, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridCurrent[0]), &(pProBuffer[54]), 2);  //交流电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 56, 57, 1000, -1000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PowerFactor[0]), &(pProBuffer[56]), 2);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 58, 59, 2000, -1000) == true) //温度
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Temper[0]), &(pProBuffer[58]), 2);
                }
                else
                {
                    return;
                }

                for(j = 0; j < 4; j++)
                {
                    MIReal[PortNO + j].Data.LinkState = MIReal[PortNO + j].Data.LinkState | MI_CONNECT;
                }

                //                if((APP_Flg != 1) && (((((u16)pProBuffer[60] << 8) | pProBuffer[61]) & 0x01) == 1))
                //dong 0528 如果数据帧中的告警流水号大于暂存的告警序号存在告警
                DataAarnNub = (((u16)pProBuffer[60] << 8) | pProBuffer[61]);

                if((APP_Flg != 1) && (DataAarnNub > WarnSerNub[PortNO]))
                {
                    //如果加起来大于20条就先存储告警池里面的告警
                    if(RealAlarmDataNO + DataAarnNub - WarnSerNub[PortNO] >= 20)
                    {
#ifdef DEBUG0619
                        printf("485-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        RealAlarmDataNO = 0;
                        memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);

                        //如果单个微逆的告警数量大于20，需要分包处理
                        if(DataAarnNub - WarnSerNub[PortNO] > 20)
                        {
                            AlarmTwoPack = 1;
                        }
                    }

                    CurAlarmState = HasNewAlarmRecord;
                }
            }
            else if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToTwo)
            {
                //                UsartNrf3_Process_LedShow(false);
                memcpy((u16 *) & (MIReal[PortNO].Data.DataVer[0]), &(pProBuffer[0]), 2); //数据版本

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVVol[0]), &(pProBuffer[2]), 2);  //PV1电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 4, 5, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVCur), &(pProBuffer[4]), 2);  //PV1电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 6, 7, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVPower[0]), &(pProBuffer[6]), 2);  //PV1功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 8, 9, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVVol[0]), &(pProBuffer[8]), 2); //PV2电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 10, 11, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVCur), &(pProBuffer[10]), 2); //PV2电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 12, 13, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVPower[0]), &(pProBuffer[12]), 2); //PV2功率
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(14, 17, PortNO) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 14, 17, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[14]) << 8 | pProBuffer[15]); //PV1历史累计发电量
                    MIReal[PortNO].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[16]) << 8 | pProBuffer[17]);
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(18, 21, PortNO + 1) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 18, 21, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 1].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[18]) << 8 | pProBuffer[19]); //PV2历史累计发电量
                    MIReal[PortNO + 1].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[20]) << 8 | pProBuffer[21]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 22, 23, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.DailyEnergy = (u16)(((u16)pProBuffer[22]) << 8 | pProBuffer[23]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 24, 25, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 1].Data.DailyEnergy = (u16)(((u16)pProBuffer[24]) << 8 | pProBuffer[25]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 26, 27, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridVol[0]), &(pProBuffer[26]), 2);  //交流电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 28, 29, 20000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Freque[0]), &(pProBuffer[28]), 2);  //交流频率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 30, 31, 40000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridActivePower[0]), &(pProBuffer[30]), 2);  //交流有功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 32, 33, 40000, -40000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridReactivePower[0]), &(pProBuffer[32]), 2);  //无功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 34, 35, 20000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridCurrent[0]), &(pProBuffer[34]), 2);  //交流电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 36, 37, 1000, -1000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PowerFactor[0]), &(pProBuffer[36]), 2); //功率因数
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 38, 39, 2000, -1000) == true) //温度
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Temper[0]), &(pProBuffer[38]), 2);
                }
                else
                {
                    return;
                }

                for(j = 0; j < 2; j++)
                {
                    MIReal[PortNO + j].Data.LinkState = MIReal[PortNO + j].Data.LinkState | MI_CONNECT;
                }

                //dong 20200529
                DataAarnNub = (((u16)pProBuffer[40] << 8) | pProBuffer[41]);

                if((APP_Flg != 1) && (DataAarnNub > WarnSerNub[PortNO]))
                {
                    //如果加起来大于20条就先存储告警池里面的告警
                    if(RealAlarmDataNO + DataAarnNub - WarnSerNub[PortNO] >= 20)
                    {
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        RealAlarmDataNO = 0;
                        memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);

                        //如果单个微逆的告警数量大于20，需要分包处理
                        if(DataAarnNub - WarnSerNub[PortNO] > 20)
                        {
                            AlarmTwoPack = 1;
                        }
                    }

                    CurAlarmState = HasNewAlarmRecord;
                }
            }
            else if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToOne)
            {
                MIReal[PortNO].Data.LinkState = MIReal[PortNO].Data.LinkState | MI_CONNECT;
                // UsartNrf3_Process_LedShow(false);
                memcpy((u16 *) & (MIReal[PortNO].Data.DataVer[0]), &(pProBuffer[0]), 2); //数据版本

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVVol[0]), &(pProBuffer[2]), 2);  //PV1电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 4, 5, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVCur), &(pProBuffer[4]), 2);  //PV1电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 6, 7, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVPower[0]), &(pProBuffer[6]), 2);  //PV1功率
                }
                else
                {
                    return;
                }

                if(FilterTotalEnergy(8, 11, PortNO) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 8, 11, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[8]) << 8 | pProBuffer[9]); //PV1历史累计发电量
                    MIReal[PortNO].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[10]) << 8 | pProBuffer[11]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 12, 13, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.DailyEnergy = (u16)(((u16)pProBuffer[12]) << 8 | pProBuffer[13]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 14, 15, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridVol[0]), &(pProBuffer[14]), 2);  //交流电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 16, 17, 20000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Freque[0]), &(pProBuffer[16]), 2);  //交流频率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 18, 19, 40000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridActivePower[0]), &(pProBuffer[18]), 2);  //交流有功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 20, 21, 40000, -40000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridReactivePower[0]), &(pProBuffer[20]), 2);  //无功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 22, 23, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridCurrent[0]), &(pProBuffer[22]), 2);  //交流电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 24, 25, 1000, -1000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PowerFactor[0]), &(pProBuffer[24]), 2); //功率因数
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 26, 27, 2000, -1000) == true) //温度
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Temper[0]), &(pProBuffer[26]), 2);
                }
                else
                {
                    return;
                }

                MIReal[PortNO].Data.LinkState = MIReal[PortNO].Data.LinkState | MI_CONNECT;
                //dong 20200529
                DataAarnNub = (((u16)pProBuffer[28] << 8) | pProBuffer[29]);

                if((APP_Flg != 1) && (DataAarnNub > WarnSerNub[PortNO]))
                {
                    //如果加起来大于20条就先存储告警池里面的告警
                    if(RealAlarmDataNO + DataAarnNub - WarnSerNub[PortNO] >= 20)
                    {
#ifdef DEBUG0619
                        printf("700-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        RealAlarmDataNO = 0;
                        memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);

                        //如果单个微逆的告警数量大于20，需要分包处理
                        if(DataAarnNub - WarnSerNub[PortNO] > 20)
                        {
                            AlarmTwoPack = 1;
                        }
                    }

                    CurAlarmState = HasNewAlarmRecord;
                }
            }
            else if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_Pro)
            {
                memcpy((u16 *) & (MIReal[PortNO].Data.DataVer[0]), &(pProBuffer[0]), 2); //数据版本

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true) //PV电压
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVVol[0]), &(pProBuffer[2]), 2);    //PV2电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 2, 3, 2000, 0) == true) //PV1电压
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVVol[0]), &(pProBuffer[2]), 2);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 4, 5, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVCur), &(pProBuffer[4]), 2);    //PV1电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 6, 7, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVCur), &(pProBuffer[6]), 2);    //PV2电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 8, 9, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PVPower[0]), &(pProBuffer[8]), 2);    //PV1功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 10, 11, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 1].Data.PVPower[0]), &(pProBuffer[10]), 2);    //PV2功率
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(12, 15, PortNO) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 12, 15, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[12]) << 8 | pProBuffer[13]); //PV1历史累计发电量
                    MIReal[PortNO].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[14]) << 8 | pProBuffer[15]);
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(16, 19, PortNO + 1) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 16, 19, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 1].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[16]) << 8 | pProBuffer[17]); //PV2历史累计发电量
                    MIReal[PortNO + 1].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[18]) << 8 | pProBuffer[19]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 20, 21, 2000, 0) == true) //PV电压
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVVol[0]), &(pProBuffer[20]), 2);    //PV3&PV4电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 20, 21, 2000, 0) == true) //PV电压
                {
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVVol[0]), &(pProBuffer[20]), 2);    //PV3&PV4电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 22, 23, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVCur), &(pProBuffer[22]), 2);    //PV3电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 24, 25, 2500, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVCur), &(pProBuffer[24]), 2);    //PV4电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 26, 27, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 2].Data.PVPower[0]), &(pProBuffer[26]), 2);    //PV3功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 28, 29, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO + 3].Data.PVPower[0]), &(pProBuffer[28]), 2);    //PV4功率
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(30, 34, PortNO + 2) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 30, 33, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 2].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[30]) << 8 | pProBuffer[31]); //PV3历史累计发电量
                    MIReal[PortNO + 2].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[32]) << 8 | pProBuffer[33]);
                }
                else
                {
                    return;
                }

                //////////////////////////////DEBUG信息/////////////    ////////
                if(FilterTotalEnergy(34, 37, PortNO + 3) == false)
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 34, 37, (u32)(pow(10, 9)), 0) == true)
                {
                    MIReal[PortNO + 3].Data.HistoryEnergyH = (u16)(((u16)pProBuffer[34]) << 8 | pProBuffer[35]); //PV4历史累计发电量
                    MIReal[PortNO + 3].Data.HistoryEnergyL = (u16)(((u16)pProBuffer[36]) << 8 | pProBuffer[37]);
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 38, 39, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.BusVol[0]), &(pProBuffer[38]), 2);    //直流母线电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 40, 41, 10000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridVol[0]), &(pProBuffer[40]), 2);    //交流电压
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 42, 43, 20000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Freque[0]), &(pProBuffer[42]), 2);    //交流频率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 44, 45, 40000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridActivePower[0]), &(pProBuffer[44]), 2);    //交流有功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 46, 47, 40000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridReactivePower[0]), &(pProBuffer[46]), 2);    //无功功率
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 48, 49, 2000, 0) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.GridCurrent[0]), &(pProBuffer[48]), 2);    //交流电流
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 50, 51, 100, -100) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.PowerFactor), &(pProBuffer[51]), 2);    //功率因数
                }
                else
                {
                    return;
                }

                if(UsartNrf_Process_ExceptionFilter(pProBuffer, 52, 53, 2000, -1000) == true)
                {
                    memcpy((u8 *) & (MIReal[PortNO].Data.Temper[0]), &(pProBuffer[52]), 2);    //温度
                }
                else
                    for(j = 0; j < 4; j++)
                    {
                        MIReal[PortNO + j].Data.LinkState = MIReal[PortNO + j].Data.LinkState | MI_CONNECT;
                    }

                DataAarnNub = (((u16)pProBuffer[54] << 8) | pProBuffer[55]);

                if((APP_Flg != 1) && (DataAarnNub > WarnSerNub[PortNO]))
                {
                    //如果加起来大于20条就先存储告警池里面的告警
                    if(RealAlarmDataNO + DataAarnNub - WarnSerNub[PortNO] >= 20)
                    {
#ifdef DEBUG0619
                        printf("485-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        RealAlarmDataNO = 0;
                        memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);

                        //如果单个微逆的告警数量大于20，需要分包处理
                        if(DataAarnNub - WarnSerNub[PortNO] > 20)
                        {
                            AlarmTwoPack = 1;
                        }
                    }

                    CurAlarmState = HasNewAlarmRecord;
                }
            }
        }
    }
}
/***********************************************
** Function name: 三代协议轮询调试版实时数据传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevRunDebug(u8 *pBuffer)
{
    UsartNrf3_Process_DevRunReality(pBuffer);
}
/***********************************************
** Function name: 三代协议轮询设备信息传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_Sample(u8 *pBuffer)
{
    vu16 TempDataNum = 0;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 2 * 16);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            InverterDetail_Read((InverterDetail *)&MIDetail, PortNO, 1);//逆变器RF软硬件版本
            MIDetail.Property.AppFWBuild_VER = (u16)(((u16)pProBuffer[0] << 8)  | pProBuffer[1]); //应用程序版本
            MIDetail.Property.HW_PNH = (u16)(((u16)pProBuffer[2] << 8)  | pProBuffer[3]); //硬件料号
            MIDetail.Property.HW_PNL = (u16)(((u16)pProBuffer[4] << 8)  | pProBuffer[5]); //硬件料号
            MIDetail.Property.HW_VER = (u16)(((u16)pProBuffer[6] << 8)  | pProBuffer[7]); //硬件版本
            MIDetail.Property.GPFCode = (u16)(((u16)pProBuffer[8] << 8)  | pProBuffer[9]); //并网保护文件代码
            MIDetail.Property.GPFVer = (u16)(((u16)pProBuffer[10] << 8)  | pProBuffer[11]); //并网保护文件版本
            MIDetail.Property.ReservedPara = (u16)(((u16)pProBuffer[12] << 8)  | pProBuffer[13]); //保留参数
            InverterDetail_Write((InverterDetail *)&MIDetail, PortNO, 1);//逆变器RF软硬件版本

            if(MIReal[PortNO].Data.NetStatus == NET_NOT_EXECUTED)
            {
                MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
                UsartNrf_ClearVersionActionState();
            }
            else
            {
                Dtu3Detail.Property.PolledVerState = 2;
            }
        }
    }
}
/***********************************************
** Function name: 三代协议轮询详细版本传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_All(u8 *pBuffer)
{
    vu16 TempDataNum = 0;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 2 * 16);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            InverterDetail_Read((InverterDetail *)&MIDetail, PortNO, 1);//逆变器RF软硬件版本
            //            MIDetail.Property.AppFWPN = (u16)(((u16)pProBuffer[0] << 8)  | pProBuffer[1]); //应用程序固件料号
            //            MIDetail.Property.AppFWBuild_VER = (u16)(((u16)pProBuffer[2] << 8)  | pProBuffer[3]); //应用程序固件版本
            //            MIDetail.Property.AppFWBuild_YYYY = (u16)(((u16)pProBuffer[4] << 8)  | pProBuffer[5]); //应用程序固件编译时间-年
            //            MIDetail.Property.AppFWBuild_MMDD = (u16)(((u16)pProBuffer[6] << 8)  | pProBuffer[7]); //应用程序固件编译时间-月.日
            //            MIDetail.Property.AppFWBuild_HHMM = (u16)(((u16)pProBuffer[8] << 8)  | pProBuffer[9]); //应用程序版本
            //            MIDetail.Property.USFWBuild_VER = (u16)(((u16)pProBuffer[10] << 8)  | pProBuffer[11]); //硬件料号
            //            MIDetail.Property.HW_PNH = (u16)(((u16)pProBuffer[12] << 8)  | pProBuffer[13]); //硬件料号
            //            MIDetail.Property.AppFW_PNL = (u16)(((u16)pProBuffer[14] << 8)  | pProBuffer[15]); //硬件料号
            //            MIDetail.Property.AppFW_PNL = (u16)(((u16)pProBuffer[16] << 8)  | pProBuffer[17]); //硬件料号
            //            MIDetail.Property.HWSPECVER = (u16)(((u16)pProBuffer[18] << 8)  | pProBuffer[19]); //硬件版本
            //            MIDetail.Property.GPFCode = (u16)(((u16)pProBuffer[20] << 8)  | pProBuffer[21]); //并网保护文件代码
            //            MIDetail.Property.GPFVer = (u16)(((u16)pProBuffer[22] << 8)  | pProBuffer[23]); //并网保护文件版本
            MIDetail.Property.AppFWBuild_VER = (u16)(((u16)pProBuffer[0] << 8)  | pProBuffer[1]); //应用程序固件版本
            MIDetail.Property.AppFWBuild_YYYY = (u16)(((u16)pProBuffer[2] << 8)  | pProBuffer[3]); //应用程序固件编译时间-年
            MIDetail.Property.AppFWBuild_MMDD = (u16)(((u16)pProBuffer[4] << 8)  | pProBuffer[5]); //应用程序固件编译时间-月.日
            MIDetail.Property.AppFWBuild_HHMM = (u16)(((u16)pProBuffer[6] << 8)  | pProBuffer[7]); //应用程序版本
            MIDetail.Property.USFWBuild_VER = (u16)(((u16)pProBuffer[8] << 8)  | pProBuffer[9]); //硬件料号
            MIDetail.Property.HW_PNH = (u16)(((u16)pProBuffer[10] << 8)  | pProBuffer[11]); //硬件料号
            MIDetail.Property.AppFW_PNL = (u16)(((u16)pProBuffer[12] << 8)  | pProBuffer[13]); //硬件料号
            MIDetail.Property.AppFW_PNL = (u16)(((u16)pProBuffer[14] << 8)  | pProBuffer[15]); //硬件料号
            MIDetail.Property.HWSPECVER = (u16)(((u16)pProBuffer[16] << 8)  | pProBuffer[17]); //硬件版本
            MIDetail.Property.GPFCode = (u16)(((u16)pProBuffer[18] << 8)  | pProBuffer[19]); //并网保护文件代码
            MIDetail.Property.GPFVer = (u16)(((u16)pProBuffer[20] << 8)  | pProBuffer[21]); //并网保护文件版本
            InverterDetail_Write((InverterDetail *)&MIDetail, PortNO, 1);//逆变器RF软硬件版本
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
        }
    }
}
/***********************************************
** Function name: 三代协议轮询并网保护文件传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_GridOnFile(u8 *pBuffer)
{
    vu16 TempDataNum = 0;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 15 * 16);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            if(CurRowData_Dat == NULL)
            {
                CurRowData_Dat = mymalloc(300 * sizeof(u8));
                //                if(CurRowData_Dat == NULL)
                //                {
                //                    return;
                //                }
            }

            memset(CurRowData_Dat, 0, 300 * sizeof(u8));
            memcpy(CurRowData_Dat, pProBuffer, TempDataNum);
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
        }
    }
}
/***********************************************
** Function name: 三代协议轮询全部告警传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_Alarm(u8 *pBuffer)
{
    //dong 0602 头两个字节为告警版本号
    //      vu16 AlarmVer = 0;
    vu16  TempDataNum = 0;
    vu16 i, j;
    vu32 AlarmTime;
    calendar_obj calendar;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 182 + 2);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            //dong 0602 头两个字节为告警版本号
            // AlarmVer = (u16)pProBuffer[0]*256+pProBuffer[1];
            for(i = 0; i < TempDataNum / 12; i++)
            {
                if(RealAlarmDataNO == 0)
                {
                    memset((AlarmDataType *)pRealAlarm, 0, 20 * sizeof(AlarmDataType));
                }

                memcpy((u8 *) & (pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[0]), (u8 *)MIMajor[PortNO].Property.Pre_Id, 2);
                memcpy((u8 *) & (pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[2]), (u8 *)MIMajor[PortNO].Property.Id, 4);
                //dong
                pRealAlarm[RealAlarmDataNO].Data.WCode = (u16)pProBuffer[i * 12 + 2] << 8 | pProBuffer[i * 12 + 3];

                if((((pRealAlarm[RealAlarmDataNO].Data.WCode >> 14) & 0x03)) == 0)
                {
                    MIReal[PortNO].Data.Run_Status[0] = 0x00;
                    MIReal[PortNO].Data.Run_Status[1] = 0x08;
                }
                else if(((pRealAlarm[RealAlarmDataNO].Data.WCode >> 14) & 0x03) == 1)
                {
                    MIReal[PortNO].Data.Run_Status[0] = 0x00;
                    MIReal[PortNO].Data.Run_Status[1] = 0x03;
                }

                // 有问题     >> 11 位应该错误  2020-05-11 dong 录波数据
                //                if(((((((uint16_t)pRealAlarm[RealAlarmDataNO].Data.WCode[0]) << 8) | pRealAlarm[RealAlarmDataNO].Data.WCode[1]) >> 11) & 0x01) == 1)
                //                if(((((((u16)pRealAlarm[RealAlarmDataNO].Data.WCode[0]) << 8) | pRealAlarm[RealAlarmDataNO].Data.WCode[1]) >> 11) & 0x01) == 1)
                //                              if(((pRealAlarm1[RealAlarmDataNO].Data.WCode << 8) | ((pRealAlarm1[RealAlarmDataNO].Data.WCode >> 11) & 0x01)) == 1)
                //                {
                //                    CurAlarmState = HasNewWaveRecord;
                //                }
                //                  else
                //                  CurAlarmState = HasNONewWaveRecord;
                //告警序号
                memcpy((u8 *) & (pRealAlarm[RealAlarmDataNO].Data.WNum), &(pProBuffer[i * 12 + 4]), 2);
                //dong 2020-05-20
                WarnSerNub[PortNO] = (u16)pProBuffer[i * 12 + 4] << 8 | (u16)pProBuffer[i * 12 + 5];
                RTC_GetWorldTime(&calendar, Dtu3Detail.Property.timezone);
                calendar.hour = 0;
                calendar.min = 0;
                calendar.sec = 0;

                //挂起告警下午
                if(((pRealAlarm[RealAlarmDataNO].Data.WCode >> 13) & 0x01) == 1)
                {
                    AlarmTime = 12 * 60 * 60 + (u32)(((u16)pProBuffer[i * 12 + 6] << 8) | ((u16)pProBuffer[i * 12 + 7])) + DateToSec(calendar);
                }
                //挂起告警上午
                else
                {
                    AlarmTime = (u32)((u16)pProBuffer[i * 12 + 6] << 8) | ((u16)pProBuffer[i * 12 + 7]) + DateToSec(calendar);
                }

                //告警起始时间
                pRealAlarm[RealAlarmDataNO].Data.WTime1[0] = AlarmTime >> 24;
                pRealAlarm[RealAlarmDataNO].Data.WTime1[1] = AlarmTime >> 16;
                pRealAlarm[RealAlarmDataNO].Data.WTime1[2] = AlarmTime >> 8;
                pRealAlarm[RealAlarmDataNO].Data.WTime1[3] = AlarmTime;

                //结束告警下午
                if(((pRealAlarm[RealAlarmDataNO].Data.WCode >> 12) & 0x01) == 1)
                {
                    AlarmTime = 12 * 60 * 60 + (u32)(((u16)pProBuffer[i * 12 + 8] << 8) | ((u16)pProBuffer[i * 12 + 9])) + DateToSec(calendar);
                }
                //结束告警上午
                else
                {
                    AlarmTime = (u32)((u16)pProBuffer[i * 12 + 8] << 8) | ((u16)pProBuffer[i * 12 + 9]) + DateToSec(calendar);
                }

                //告警结束时间
                pRealAlarm[RealAlarmDataNO].Data.WTime2[0] = AlarmTime >> 24;
                pRealAlarm[RealAlarmDataNO].Data.WTime2[1] = AlarmTime >> 16;
                pRealAlarm[RealAlarmDataNO].Data.WTime2[2] = AlarmTime >> 8;
                pRealAlarm[RealAlarmDataNO].Data.WTime2[3] = AlarmTime;
                //告警数据
                memcpy((u8 *) & (pRealAlarm[RealAlarmDataNO].Data.Data1[0]), &(pProBuffer[i * 12 + 10]), 2);
                //告警数据
                memcpy((u8 *) & (pRealAlarm[RealAlarmDataNO].Data.Data2[0]), &(pProBuffer[i * 12 + 12]), 2);
#ifdef DEBUG0619
                printf("SS --id:%lld--wcode:%d--wnub:%d -- WTime1:%d -- WTime2:%d--data1:%d--data2:%d\n\r", \
                       (((uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[0]) << 40) +
                        ((uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[1]) << 32) +
                        ((uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[2]) << 24) +
                        ((uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[3]) << 16) +
                        ((uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[4]) << 8) +
                        (uint64_t)(pRealAlarm[RealAlarmDataNO].Data.Alarm_Id[5])),
                       pRealAlarm[RealAlarmDataNO].Data.WCode, \
                       ((u16)pRealAlarm[RealAlarmDataNO].Data.WNum[0] << 8 |  pRealAlarm[RealAlarmDataNO].Data.WNum[1]), \
                       ((u32)pRealAlarm[RealAlarmDataNO].Data.WTime1[0] << 24 | (u32) pRealAlarm[RealAlarmDataNO].Data.WTime1[1] << 16 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[2] << 8 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[3]), \
                       ((u32) pRealAlarm[RealAlarmDataNO].Data.WTime2[0] << 24 | (u32) pRealAlarm[RealAlarmDataNO].Data.WTime2[1] << 16 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[2] << 8 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[3]), \
                       ((u16)pRealAlarm[RealAlarmDataNO].Data.Data1[0] << 8 |  pRealAlarm[RealAlarmDataNO].Data.Data1[1]), \
                       ((u16)pRealAlarm[RealAlarmDataNO].Data.Data2[0] << 8 | pRealAlarm[RealAlarmDataNO].Data.Data2[1])
                      );
#endif
                RealAlarmDataNO++;

                //dong 2020-05-20
                //CurRealAlarmNum = ((uint16_t)pRealAlarm[RealAlarmDataNO].Data.WNum[0]) | pRealAlarm[RealAlarmDataNO].Data.WNum[1];
                //dong 20200529 告警数据满20条
                if((APP_Flg != 1) && (RealAlarmDataNO >= 20))
                {
                    //一包的
                    if(AlarmTwoPack == 0)
                    {
#ifdef DEBUG0619
                        printf("1042-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                    }
                    //多包的还未接受完
                    else if((AlarmTwoPack == 1) && (DataAarnNub > WarnSerNub[PortNO]))
                    {
#ifdef DEBUG0619
                        printf("1050-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                        Alarm_Data_Write(0, 2, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        AlarmTwoPack = 2;
                    }
                    //多包已经接受完整--改为单包存储跳出循环
                    else if((AlarmTwoPack == 1) && (DataAarnNub <= WarnSerNub[PortNO]))
                    {
                        Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                        AlarmTwoPack = 0;
                        RealAlarmDataNO = 0;
                        memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);
                        break;
                    }

                    RealAlarmDataNO = 0;
                    memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);
                }
            }

            //未轮训完
            if(DataAarnNub > WarnSerNub[PortNO])
            {
                CurAlarmState = HasNewAlarmRecord;
            }
            //轮训完成
            else
            {
                if(AlarmTwoPack == 2)
                {
#ifdef DEBUG0619
                    printf("1081-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
                    Alarm_Data_Write(1, 2, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
                    RealAlarmDataNO = 0;
                    memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);
                }

                CurAlarmState = InitState;
                AlarmTwoPack = 0;
            }
        }
    }
}
/***********************************************
** Function name: 三代协议轮询挂起告警传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_InformUpdate(u8 *pBuffer)
{
    vu16  TempDataNum = 0;
    vu16 i;
    vu32 AlarmTime;
    calendar_obj calendar;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 182 + 2);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            for(i = 0; i < TempDataNum / 12; i++)
            {
                if(InformAlarmDataNO == 0)
                {
                    memset((AlarmDataType *)pInformAlarm, 0, 20 * sizeof(AlarmDataType));
                }

                memcpy((u8 *) & (pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[0]), (u8 *)MIMajor[PortNO].Property.Pre_Id, 2);
                memcpy((u8 *) & (pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[2]), (u8 *)MIMajor[PortNO].Property.Id, 4);
                pInformAlarm[InformAlarmDataNO].Data.WCode = (u16)pProBuffer[i * 12 + 2] << 8 | pProBuffer[i * 12 + 3];

                if(((pInformAlarm[InformAlarmDataNO].Data.WCode >> 14) & 0x03) == 0)
                {
                    MIReal[PortNO].Data.Run_Status[1] = 0x08;
                    MIReal[PortNO].Data.Run_Status[0] = 0x00;
                }
                else if(((pInformAlarm[InformAlarmDataNO].Data.WCode >> 14) & 0x03) == 1)
                {
                    MIReal[PortNO].Data.Run_Status[1] = 0x03;
                    MIReal[PortNO].Data.Run_Status[0] = 0x00;
                }

                //                  if(((((((u16)pAlarmData[AlarmDataNO]->Data.WCode[0]) << 8)|pAlarmData[AlarmDataNO]->Data.WCode[1]) >> 11)&0x01) == 1)
                //                  CurAlarmState = HasNewWaveRecord;
                //                  else
                //                  CurAlarmState = HasNONewWaveRecord;
                memcpy((u8 *) & (pInformAlarm[InformAlarmDataNO].Data.WNum), &(pProBuffer[i * 12 + 4]), 2); //告警序号
                RTC_GetWorldTime(&calendar, Dtu3Detail.Property.timezone);
                calendar.hour = 0;
                calendar.min = 0;
                calendar.sec = 0;

                //挂起告警下午
                if(((pInformAlarm[InformAlarmDataNO].Data.WCode >> 13) & 0x01) == 1)
                {
                    AlarmTime = 12 * 60 * 60 + (u32)(((u16)pProBuffer[i * 12 + 6] << 8) | ((u16)pProBuffer[i * 12 + 7])) + DateToSec(calendar);
                }
                //挂起告警上午
                else
                {
                    AlarmTime = (u32)((u16)pProBuffer[i * 12 + 6] << 8) | ((u16)pProBuffer[i * 12 + 7]) + DateToSec(calendar);
                }

                //告警起始时间
                pInformAlarm[InformAlarmDataNO].Data.WTime1[0] = AlarmTime >> 24;
                pInformAlarm[InformAlarmDataNO].Data.WTime1[1] = AlarmTime >> 16;
                pInformAlarm[InformAlarmDataNO].Data.WTime1[2] = AlarmTime >> 8;
                pInformAlarm[InformAlarmDataNO].Data.WTime1[3] = AlarmTime;

                //结束告警下午
                if((((pInformAlarm[InformAlarmDataNO].Data.WCode >> 12) & 0x01)) == 1)
                {
                    AlarmTime = 12 * 60 * 60 + (u32)(((u16)pProBuffer[i * 12 + 8] << 8) | ((u16)pProBuffer[i * 12 + 9])) + DateToSec(calendar);
                }
                //结束告警上午
                else
                {
                    AlarmTime = (u32)((u16)pProBuffer[i * 12 + 8] << 8) | ((u16)pProBuffer[i * 12 + 9]) + DateToSec(calendar);
                }

                //告警结束时间
                pInformAlarm[InformAlarmDataNO].Data.WTime2[0] = AlarmTime >> 24;
                pInformAlarm[InformAlarmDataNO].Data.WTime2[1] = AlarmTime >> 16;
                pInformAlarm[InformAlarmDataNO].Data.WTime2[2] = AlarmTime >> 8;
                pInformAlarm[InformAlarmDataNO].Data.WTime2[3] = AlarmTime;
                //告警数据
                memcpy((u8 *) & (pInformAlarm[InformAlarmDataNO].Data.Data1[0]), &(pProBuffer[i * 12 + 10]), 2);
                //告警数据2
                memcpy((u8 *) & (pInformAlarm[InformAlarmDataNO].Data.Data2[0]), &(pProBuffer[i * 12 + 12]), 2);
#ifdef DEBUG0619
                printf("GQ --id:%lld--wcode:%d--wnub:%d -- WTime1:%d -- WTime2:%d--data1:%d--data2:%d\n\r", \
                       (((uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[0]) << 40) +
                        ((uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[1]) << 32) +
                        ((uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[2]) << 24) +
                        ((uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[3]) << 16) +
                        ((uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[4]) << 8) +
                        (uint64_t)(pInformAlarm[InformAlarmDataNO].Data.Alarm_Id[5])),
                       pInformAlarm[InformAlarmDataNO].Data.WCode, \
                       ((u16) pInformAlarm[InformAlarmDataNO].Data.WNum[0] << 8 |  pInformAlarm[InformAlarmDataNO].Data.WNum[1]), \
                       ((u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[0] << 24 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[1] << 16 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[2] << 8 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime1[3]), \
                       ((u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[0] << 24 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[1] << 16 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[2] << 8 | (u32) pInformAlarm[InformAlarmDataNO].Data.WTime2[3]), \
                       ((u16) pInformAlarm[InformAlarmDataNO].Data.Data1[0] << 8 |  pInformAlarm[InformAlarmDataNO].Data.Data1[1]), \
                       ((u16) pInformAlarm[InformAlarmDataNO].Data.Data2[0] << 8 | pInformAlarm[InformAlarmDataNO].Data.Data2[1])
                      );
#endif
                InformAlarmDataNO++;

                // 告警数据写入flash
                if(InformAlarmDataNO >= 20)
                {
#ifdef DEBUG0619
                    printf("AlarmInfo_Write:%d", InformAlarmDataNO);
#endif
                    AlarmInfo_Write((AlarmDataType *)&pInformAlarm, 20);
                    InformAlarmDataNO = 0;
                    memset((AlarmDataType *)pInformAlarm, 0, 20 * sizeof(AlarmDataType));
                }
            }
        }
    }
}
/***********************************************
** Function name: 三代协议轮询录波数据传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_RecordWave(u8 *pBuffer)
{
    vu16  TempDataNum = 0;
    WaveDataType *pWaveData = NULL;
    u8 *pWVData = NULL;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 1200 + 2);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if(TempDataNum == 4)
            {
                UsartNrf3_Process_One_MultiPackage(pBuffer);
                return;
            }

            if(pWaveData == NULL)
            {
                pWaveData = mymalloc(1200 * sizeof(u8));
                //                if(pWaveData == NULL)
                //                {
                //                    return;
                //                }
            }

            memset(pWaveData, 0, 1200 * sizeof(u8));
            memcpy(&(pWaveData->Alarm_Id[0]), (u8 *)MIMajor[PortNO].Property.Pre_Id, 2);
            memcpy(&(pWaveData->Alarm_Id[2]), (u8 *)MIMajor[PortNO].Property.Id, 4);
            pWaveData->WCode = (u16)pProBuffer[0] << 8 | pProBuffer[1];
            memcpy(&(pWaveData->WNum), &(pProBuffer[2]), 2);//告警序号
            pWaveData->WTime1[0] = (u8)(RTC_GetWorldSecond(Dtu3Detail.Property.timezone) >> 24);
            pWaveData->WTime1[1] = (u8)(RTC_GetWorldSecond(Dtu3Detail.Property.timezone) >> 16);
            pWaveData->WTime1[2] = (u8)(RTC_GetWorldSecond(Dtu3Detail.Property.timezone) >> 8);
            pWaveData->WTime1[3] = (u8)(RTC_GetWorldSecond(Dtu3Detail.Property.timezone));
            memcpy(&(pWaveData->WVDataL), &(pProBuffer[4]), 2); //数据长度
            memcpy(&(pWaveData->WPos), &(pProBuffer[6]), 2);//告警位置

            if(pWVData == NULL)
            {
                pWVData = mymalloc((u16)(pWaveData->WVDataL) * sizeof(u8));
                //                if(pWVData == NULL)
                //                {
                //                    return;
                //                }
            }

            memset(pWVData, 0, (u16)(pWaveData->WVDataL)*sizeof(u8));
            pWVData = &(pProBuffer[8]);

            //录波数据写入flash
            if(pWaveData != NULL)
            {
                myfree(pWaveData);
                pWaveData = NULL;
            }

            if(pWVData != NULL)
            {
                myfree(pWVData);
                pWVData = NULL;
            }
        }
    }
}
/***********************************************
** Function name: 三代协议轮询所有数据信息通讯质量
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform_GetLossRate(u8 *pBuffer)
{
    vu16 TempDataNum = 0;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 2 * 16);

    if(TempDataNum != 0)
    {
        if(UsartNrf3_Process_DataCrc(TempDataNum))
        {
            if((pDataCnt->MI_CF_Num_Start == 0) && (pDataCnt->MI_RF_Num_Start == 0))
            {
                pDataCnt->MI_CF_Num_Start = (int16_t)(((u16)pProBuffer[0] << 8) | pProBuffer[1]); //微逆接收计数
                pDataCnt->MI_RF_Num_Start = (int16_t)(((u16)pProBuffer[2] << 8) | pProBuffer[3]); //微逆发送计数
            }
            else
            {
                pDataCnt->MI_CF_Num_End = (int16_t)(((u16)pProBuffer[0] << 8) | pProBuffer[1]); //微逆接收计数
                pDataCnt->MI_RF_Num_End = (int16_t)(((u16)pProBuffer[2] << 8) | pProBuffer[3]); //微逆发送计数
            }
        }
    }
}

//dong 2020-06-15
/***********************************************
** Function name: 三代协议自检状态应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_Self_Check_State(u8 *pBuffer)
{
    vu16  TempDataNum = 0;
    vu16 i;
    vu32 AlarmTime;
    calendar_obj calendar;
    TempDataNum = UsartNrf3_Process_GetDatatoProBuffer(pBuffer, 90 + 2);
    GPSTVal GPSTValBuf = {0};

    if(TempDataNum != 0)
    {
        //id
        memcpy((u8 *) & (GPSTValBuf.Property.pv_sn[0]), (u8 *)MIMajor[PortNO].Property.Pre_Id, 2);
        memcpy((u8 *) & (GPSTValBuf.Property.pv_sn[2]), (u8 *)MIMajor[PortNO].Property.Id, 4);
        //数据版本
        GPSTValBuf.Property.ver = (u16)pProBuffer[0] << 8 | pProBuffer[1];
        //测试状态
        GPSTValBuf.Property.st = (u16)pProBuffer[2] << 8 | pProBuffer[3];

        //收集完成
        if((GPSTValBuf.Property.st >= 2) && (GPSTValBuf.Property.st  <= 6))
        {
        }
        //收集中
        else
        {
            return;
        }

        //并网规则文件代码
        GPSTValBuf.Property.gpf = (u16)pProBuffer[4] << 8 | pProBuffer[5];
        //并网规则文件版本
        GPSTValBuf.Property.gpf_ver = (u16)pProBuffer[6] << 8 | pProBuffer[7];

        //4 到 6 没有后面的数据
        if((GPSTValBuf.Property.st >= 2) && (GPSTValBuf.Property.st <= 3))
        {
            //一级过压自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.hv1_stval.Data, (u8 *)&pProBuffer[8], 10);
            GPSTValBuf.Property.hv1_stval.Property.code = (u16)pProBuffer[8] << 8 | pProBuffer[9];
            GPSTValBuf.Property.hv1_stval.Property.dflt_val = (u16)pProBuffer[10] << 8 | pProBuffer[11];
            GPSTValBuf.Property.hv1_stval.Property.dflt_tim = (u16)pProBuffer[12] << 8 | pProBuffer[13];
            GPSTValBuf.Property.hv1_stval.Property.rslt_val = (u16)pProBuffer[14] << 8 | pProBuffer[15];
            GPSTValBuf.Property.hv1_stval.Property.rslt_tim = (u16)pProBuffer[16] << 8 | pProBuffer[17];
            //一级欠压自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.lv1_stval.Data, (u8 *)&pProBuffer[18], 10);
            GPSTValBuf.Property.lv1_stval.Property.code = (u16)pProBuffer[18] << 8 | pProBuffer[19];
            GPSTValBuf.Property.lv1_stval.Property.dflt_val = (u16)pProBuffer[20] << 8 | pProBuffer[21];
            GPSTValBuf.Property.lv1_stval.Property.dflt_tim = (u16)pProBuffer[22] << 8 | pProBuffer[23];
            GPSTValBuf.Property.lv1_stval.Property.rslt_val = (u16)pProBuffer[24] << 8 | pProBuffer[25];
            GPSTValBuf.Property.lv1_stval.Property.rslt_tim = (u16)pProBuffer[26] << 8 | pProBuffer[27];
            //二级过压自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.hv2_stval.Data, (u8 *)&pProBuffer[28], 10);
            GPSTValBuf.Property.hv2_stval.Property.code = (u16)pProBuffer[28] << 8 | pProBuffer[29];
            GPSTValBuf.Property.hv2_stval.Property.dflt_val = (u16)pProBuffer[30] << 8 | pProBuffer[31];
            GPSTValBuf.Property.hv2_stval.Property.dflt_tim = (u16)pProBuffer[32] << 8 | pProBuffer[33];
            GPSTValBuf.Property.hv2_stval.Property.rslt_val = (u16)pProBuffer[34] << 8 | pProBuffer[35];
            GPSTValBuf.Property.hv2_stval.Property.rslt_tim = (u16)pProBuffer[36] << 8 | pProBuffer[37];
            //二级欠压自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.lv2_stval.Data, (u8 *)&pProBuffer[38], 10);
            GPSTValBuf.Property.lv2_stval.Property.code = (u16)pProBuffer[38] << 8 | pProBuffer[39];
            GPSTValBuf.Property.lv2_stval.Property.dflt_val = (u16)pProBuffer[40] << 8 | pProBuffer[41];
            GPSTValBuf.Property.lv2_stval.Property.dflt_tim = (u16)pProBuffer[42] << 8 | pProBuffer[43];
            GPSTValBuf.Property.lv2_stval.Property.rslt_val = (u16)pProBuffer[44] << 8 | pProBuffer[45];
            GPSTValBuf.Property.lv2_stval.Property.rslt_tim = (u16)pProBuffer[46] << 8 | pProBuffer[47];
            //一级过频自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.hf1_stval.Data, (u8 *)&pProBuffer[48], 10);
            GPSTValBuf.Property.hf1_stval.Property.code = (u16)pProBuffer[48] << 8 | pProBuffer[49];
            GPSTValBuf.Property.hf1_stval.Property.dflt_val = (u16)pProBuffer[50] << 8 | pProBuffer[51];
            GPSTValBuf.Property.hf1_stval.Property.dflt_tim = (u16)pProBuffer[52] << 8 | pProBuffer[53];
            GPSTValBuf.Property.hf1_stval.Property.rslt_val = (u16)pProBuffer[54] << 8 | pProBuffer[55];
            GPSTValBuf.Property.hf1_stval.Property.rslt_tim = (u16)pProBuffer[56] << 8 | pProBuffer[57];
            //一级欠频自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.lf1_stval.Data, (u8 *)&pProBuffer[58], 10);
            GPSTValBuf.Property.lf1_stval.Property.code = (u16)pProBuffer[58] << 8 | pProBuffer[59];
            GPSTValBuf.Property.lf1_stval.Property.dflt_val = (u16)pProBuffer[60] << 8 | pProBuffer[61];
            GPSTValBuf.Property.lf1_stval.Property.dflt_tim = (u16)pProBuffer[62] << 8 | pProBuffer[63];
            GPSTValBuf.Property.lf1_stval.Property.rslt_val = (u16)pProBuffer[64] << 8 | pProBuffer[65];
            GPSTValBuf.Property.lf1_stval.Property.rslt_tim = (u16)pProBuffer[66] << 8 | pProBuffer[67];
            //二级过频自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.hf2_stval.Data, (u8 *)&pProBuffer[68], 10);
            GPSTValBuf.Property.hf2_stval.Property.code = (u16)pProBuffer[68] << 8 | pProBuffer[69];
            GPSTValBuf.Property.hf2_stval.Property.dflt_val = (u16)pProBuffer[70] << 8 | pProBuffer[71];
            GPSTValBuf.Property.hf2_stval.Property.dflt_tim = (u16)pProBuffer[72] << 8 | pProBuffer[73];
            GPSTValBuf.Property.hf2_stval.Property.rslt_val = (u16)pProBuffer[74] << 8 | pProBuffer[75];
            GPSTValBuf.Property.hf2_stval.Property.rslt_tim = (u16)pProBuffer[76] << 8 | pProBuffer[77];
            //二级欠频自检测试结果
            //            memcpy((u8 *)GPSTValBuf.Property.lf2_stval.Data, (u8 *)&pProBuffer[78], 10);
            GPSTValBuf.Property.lf2_stval.Property.code = (u16)pProBuffer[78] << 8 | pProBuffer[79];
            GPSTValBuf.Property.lf2_stval.Property.dflt_val = (u16)pProBuffer[80] << 8 | pProBuffer[81];
            GPSTValBuf.Property.lf2_stval.Property.dflt_tim = (u16)pProBuffer[82] << 8 | pProBuffer[83];
            GPSTValBuf.Property.lf2_stval.Property.rslt_val = (u16)pProBuffer[84] << 8 | pProBuffer[85];
            GPSTValBuf.Property.lf2_stval.Property.rslt_tim = (u16)pProBuffer[86] << 8 | pProBuffer[87];
        }

#ifdef solar
        printf("AlarmInfo_Write:%d", InformAlarmDataNO);
#endif
        //存储
        GPST_Data_Write((GPSTVal *)&GPSTValBuf, 1);
        //清除轮训状态
        MIReal[PortNO].Data.NetCmd = NET_INIT;
        MIReal[PortNO].Data.NetStatus = NET_NOCMD;

        //查询状态是否全部轮训结束
        for(u8 i = 0; i < Dtu3Detail.Property.PortNum; i++)
        {
            if(MIReal[i].Data.NetCmd == NET_SELF_STAUS)
            {
                return ;
            }
        }

        //全部轮训结束上传服务器
        SelfCheckStateFlg = 2;
    }
}
/***********************************************
** Function name: 三代协议轮询所有数据信息传输应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevInform(u8 *pBuffer)
{
    switch(CurRecSendPackageDataType)
    {
        case InverterDevInform_Simple://微逆设备信息-简版
            {
                UsartNrf3_Process_DevInform_Sample(pBuffer);
                break;
            }

        case InverterDevInform_All://微逆设备信息-全版
            {
                UsartNrf3_Process_DevInform_All(pBuffer);
                break;
            }

        case GridOnProFilePara: //并网保护文件参数
            {
                UsartNrf3_Process_DevInform_GridOnFile(pBuffer);
                break;
            }

        case HardWareConfig: //硬件配置信息
            {
                //UsartNrf3_Process_DevInform_SystemConfig(pBuffer);
                break;
            }

        case SimpleCalibrationPara:  //采样校准系数
            {
                // UsartNrf3_Process_DevInform_Calibration(pBuffer);
                break;
            }

        case SystemConfigPara:     //系统配置参数
            {
                // UsartNrf3_Process_DevInform_SystemConfig(pBuffer);
                break;
            }

        case RealTimeRunData_Debug://实时展示运行数据-系统调试阶段
            {
                UsartNrf3_Process_DevRunDebug(pBuffer);
                break;
            }

        case RealTimeRunData_Reality://展示运行数据-系统运行阶段
            {
                UsartNrf3_Process_DevRunReality(pBuffer);
                break;
            }

        case RealTimeRunData_A_Phase://展示运行数据-a相
            {
                break;
            }

        case RealTimeRunData_B_Phase:   //展示运行数据-b相
            {
                break;
            }

        case RealTimeRunData_C_Phase://展示运行数据-c相
            {
                break;
            }

        //所有未发送告警
        case AlarmData:
            {
                UsartNrf3_Process_DevInform_Alarm(pBuffer);
                break;
            }

        //全部挂起告警
        case AlarmUpdate:
            {
                UsartNrf3_Process_DevInform_InformUpdate(pBuffer);
                break;
            }

        case RecordData: //录波数据
            {
                UsartNrf3_Process_DevInform_RecordWave(pBuffer);
                break;
            }

        case InternalData://内部数据1（生产测试用，dtu不需要支持）
            {
                // UsartNrf3_Process_DevInform_InternalData(pBuffer);
                break;
            }

        case GetLossRate:
            {
                UsartNrf3_Process_DevInform_GetLossRate(pBuffer);
                break;
            }

        //dong 2020-06-15
        //自检状态处理
        case GetSelfCheckState:
            {
                UsartNrf3_Process_Self_Check_State(pBuffer);
                break;
            }

        default:
            break;
    }
}

//dong 2020-06-23
/***********************************************
** Function name: 三代协议轮序NRF版本号
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_Mi_Nrf_VER(u8 *target_adr, u8 *rout_adr)
{
    vu8 i = 0;
    vu8 temp_dat[UART_LEN];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Uart_SendBuffer, 0, UART_LEN);
    Uart_SendBuffer[0] = STX;//头
    temp_dat[0] = 0x06;   //command
    memcpy((u8 *)&temp_dat[1], target_adr, 4);//源地址
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);//目标地址
    temp_dat[9] = 0;
    temp_dat[10] = Get_crc_xor((u8 *)temp_dat, 10);
    i = ForwardSubstitution((u8 *)&Uart_SendBuffer[1], (u8 *)&temp_dat[0], 11); //正向替换
    Uart_SendBuffer[(i + 1)] = ETX; //尾
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    return (i + 2);
}
/***********************************************
** Function name: 三代协议单包多收轮询打包
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_PackPollSigleDataCommand(u8 *target_adr, u8 *rout_adr, u8 FrameNO)
{
    vu8 i = 0;
    vu8 temp_dat[UART_LEN];
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Uart_SendBuffer, 0, UART_LEN);
    Uart_SendBuffer[0] = STX;//头
    temp_dat[0] = 0x15;   //command
    memcpy((u8 *)&temp_dat[1], target_adr, 4);//源地址
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);//目标地址

    if(FrameNO > 127)
    {
        return 0;
    }

    temp_dat[9] = FrameNO | 0x80; //多帧标识
    temp_dat[10] = Get_crc_xor((u8 *)temp_dat, 10);
    i = ForwardSubstitution((u8 *)&Uart_SendBuffer[1], (u8 *)&temp_dat[0], 11); //正向替换
    Uart_SendBuffer[(i + 1)] = ETX; //尾
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    return (i + 2);
}

/***********************************************
** Function name: 三代协议单包多收轮询打包
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_PackPollMultiDataCommand(u8 *target_adr, u8 *rout_adr, u8 DataType, u8 Gap, u8 *Password)
{
    vu8 i = 0;
    vu16 TempCrc = 0;
    vu8 temp_dat[UART_LEN];
    vu16 DatCrc = 0xffff;
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Uart_SendBuffer, 0, UART_LEN);
    //20200514 dong
    vu32 time = RTC_GetWorldSecond(Dtu3Detail.Property.timezone);
    Uart_SendBuffer[0] = STX;//头
    temp_dat[0] = 0x15;   //command
    memcpy((u8 *)&temp_dat[1], target_adr, 4);//源地址
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);//目标地址
    temp_dat[9] = 0x80;//多帧标识
    temp_dat[10] = DataType;//用户数据:数据类型
    CurRecSendPackageDataType = DataType;//当前打包的数据类型
    temp_dat[11] = 0;//rev
    temp_dat[12] = (u8)(time >> 24);
    temp_dat[13] = (u8)(time >> 16);
    temp_dat[14] = (u8)(time >> 8);
    temp_dat[15] = (u8)(time);
    temp_dat[16] = Gap;//用户数据:数据上传服务器间隔
    temp_dat[17] = Gap >> 8;

    if(DataType == AlarmData)
    {
        //        temp_dat[18] = (u8)((CurRealAlarmNum + 1) / 0xff);
        //        temp_dat[19] = (u8)((CurRealAlarmNum + 1) % 0xff);
        temp_dat[18] = (u8)((WarnSerNub[PortNO]) / 0xff);
        temp_dat[19] = (u8)((WarnSerNub[PortNO]) % 0xff);
    }
    else
    {
        memset((u8 *) & (temp_dat[18]), 0, 2);  //用户数据:当天最新收到的告警序号
    }

    memcpy((u8 *)(&temp_dat[20]), Password, 4); //用户数据:防盗密码

    for(i = 10; i < 24; i++)
    {
        if((i - 10) % 2 == 1)
        {
            TempCrc = (u16)(temp_dat[i - 1] << 8) | (temp_dat[i]);
            DatCrc =  CalcCRC16t(TempCrc, DatCrc);
        }
    }

    temp_dat[24] = (u8)(DatCrc >> 8);
    temp_dat[25] = (u8)(DatCrc);
    temp_dat[26] = Get_crc_xor((u8 *)temp_dat, 26);
    i = ForwardSubstitution((u8 *)&Uart_SendBuffer[1], (u8 *)&temp_dat[0], 27); //正向替换
    Uart_SendBuffer[(i + 1)] = ETX; //尾
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    return (i + 2);
}
/***********************************************
** Function name: 三代协议设备控制打包
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_PackDevControl(u8 *target_adr, u8 *rout_adr, u16 ControlMode, u8 *ControlData, u8 nub, u8 Num)
{
    vu8 i = 0;
    vu16 TempCrc = 0;
    vu8 temp_dat[UART_LEN];
    vu16 DatCrc = 0xffff;
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    memset((u8 *)Uart_SendBuffer, 0, UART_LEN);
    Uart_SendBuffer[0] = STX;//头
    temp_dat[0] = DEVCONTROL_ALL;   //command
    memcpy((u8 *)&temp_dat[1], target_adr, 4);//源地址
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);//目标地址
    temp_dat[9] = 0x81;//

    if((nub & 0x0f) == 1)
    {
        temp_dat[10] = (u8)(ControlMode >> 8); //控制类型
        temp_dat[11] = (u8)(ControlMode);//控制类型
    }

    memcpy((u8 *) & (temp_dat[12]), ControlData, Num); //控制参数

    for(i = 10; i < 12 + Num + 1; i++)
    {
        if((i - 10) % 2 == 1)
        {
            TempCrc = (u16)((temp_dat[i - 1] << 8) | (temp_dat[i]));
            DatCrc =  CalcCRC16t(TempCrc, DatCrc);
        }
    }

    temp_dat[12 + Num] = (u8)(DatCrc >> 8);
    temp_dat[13 + Num] = (u8)(DatCrc);
    temp_dat[14 + Num] = Get_crc_xor((u8 *)&temp_dat[0], 15 + Num);
    i = ForwardSubstitution((u8 *)&Uart_SendBuffer[1], (u8 *)&temp_dat[0], (15 + Num)); //正向替换
    Uart_SendBuffer[(i + 1)] = ETX; //尾
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    return (i + 2);
}
/***********************************************
** Function name: 三代协议参数设置打包
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_PackSetPara(u8 *target_adr, u8 *rout_adr, u16 ControlMode, u8 *ControlData, u8 nub, u8 Num)
{
    vu8 i = 0, j = 0;
    vu16 TempCrc = 0;
    vu8 temp_dat[UART_LEN];
    vu16 DatCrc = 0xffff;
    memset((u8 *)Uart_SendBuffer, 0, UART_LEN);
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    Uart_SendBuffer[0] = STX;//头
    temp_dat[0] = PARASET_ALL;   //command
    memcpy((u8 *)&temp_dat[1], target_adr, 4);//源地址
    temp_dat[9] = nub;//
    memcpy((u8 *)&temp_dat[5], rout_adr, 4);//目标地址
    j = 10;

    if((nub & 0x0f) == 1)
    {
        temp_dat[j++] = (u8)(ControlMode >> 8); //控制类型
        temp_dat[j++] = (u8)(ControlMode);//控制类型
    }

    memcpy((u8 *) & (temp_dat[j]), ControlData, Num); //设置参数

    if((nub >> 7) == 1)
    {
        for(i = 0; i < TotalIndex_Para + 2; i++)
        {
            if(i % 2 == 1)
            {
                if(i > 2)
                {
                    TempCrc = (u16)(((u16)MIMajor[PortNO].Property.Pass.Data[i - 3] << 8) | (MIMajor[PortNO].Property.Pass.Data[i - 2]));
                    DatCrc =  CalcCRC16t(TempCrc, DatCrc);
                }
                else
                {
                    TempCrc = ControlMode;
                    DatCrc =  CalcCRC16t(TempCrc, DatCrc);
                }
            }
        }

        temp_dat[(j++) + Num] = (u8)(DatCrc >> 8);
        temp_dat[(j++) + Num] = (u8)(DatCrc);
    }

    temp_dat[j + Num] = Get_crc_xor((u8 *)&temp_dat[0], j + Num);
    i = ForwardSubstitution((u8 *)&Uart_SendBuffer[1], (u8 *)&temp_dat[0], (j + 1 + Num)); //正向替换
    Uart_SendBuffer[(i + 1)] = ETX; //尾
    memset((u8 *)temp_dat, 0, sizeof(temp_dat));
    return (i + 2);
}
/***********************************************
** Function name: 三代协议网络命令转为Nrf命令
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_NetCmdToNrfCmd(void)
{
    if((CurNetCmd >= NET_TURN_ON) && (CurNetCmd <= NET_ELE_ENERGY))
    {
        switch(CurNetCmd)
        {
            case NET_INVERTER_HW_INFOR:
                //case NET_TERMINAL_INFOR:
                SubCmd = InverterDevInform_Simple;
                MainCmd = REQ_ARW_DAT_ALL;
                break;

            case NET_POLL_GRID_ON_FILE:
                SubCmd = GridOnProFilePara;
                MainCmd = REQ_ARW_DAT_ALL;
                break;

            //////////////////////////////////////////////////////////////////////运行控制

            case NET_TURN_ON:
                SubCmd = Type_TurnOn;//开机
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_TURN_OFF:
                SubCmd = Type_TurnOff;//关机
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_LIMIT_ACTIVE_POEWR:
            case NET_LIMIT_POEWR:
                SubCmd = Type_ActivePowerContr ;//限制有功功率
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_LIMIT_REACTIVE_POWER:
                SubCmd = Type_ReactivePowerContr ;//限制无功功率
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_PF_SET:
                SubCmd = Type_PFSet ;//限制功率因数
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_CLEAN_GFDI:
            case NET_CLEAR_ALARM:
                SubCmd = Type_CleanState_LockAndAlarm ;//锁死告警清除
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_RESTART: //下载程序/复位
                MainCmd = DEVCONTROL_ALL;
                SubCmd = Type_Restart;
                break;

            case  NET_UNLOCK:
                SubCmd = Type_Unlock;
                MainCmd = DEVCONTROL_ALL;
                break;

            case NET_LOCK:
                SubCmd = Type_Lock;
                MainCmd = DEVCONTROL_ALL;
                break;

            //dong 2020-06-15
            case NET_SELF_INSPECTION:
                SubCmd = Type_SelfInspection;//自检
                MainCmd = DEVCONTROL_ALL;
                break;

            ////////////////////////////////////////////////////////////////参数配置
            case NET_ELE_ENERGY:
            case NET_SET_ENERGY:
                SubCmd = EleEnergySet;
                MainCmd = PARASET_ALL;
                break;

            case NET_SET_PASSWORD:
            case NET_CANCEL_GUARD:
                SubCmd = AntitheftParaSet;//设置密码
                MainCmd = PARASET_ALL;
                break;

            ////////////////////////////////////////////////////////////////文件多包更新文件
            case NET_DOWNLOAD_PRO:
                MainCmd = DOWN_PRO;
                SubCmd = Type_Init;
                break;

            case NET_DOWNLOAD_DAT://并网保护文件
                MainCmd = DOWN_DAT;
                SubCmd = Type_Init;
                break;

            default:
                break;
        }
    }
    else if(CurNetCmd == NET_INIT)
    {
        MainCmd = REQ_ARW_DAT_ALL;

        if(CurPollIsDebugDataTime == false)
        {
            SubCmd = RealTimeRunData_Reality;
        }
        else
        {
            SubCmd = RealTimeRunData_Debug;
        }
    }
    //dong 2020-06-15
    //    else if(CurNetCmd == NET_SELF_STAUS)
    //    {
    //        MainCmd = REQ_ARW_DAT_ALL;
    //        SubCmd = GetSelfCheckState;
    //    }
    //dong 2020-06-23
    //    else if(CurNetCmd == NET_GET_LOSS_RATE)
    //    {
    //        MainCmd = REQ_ARW_DAT_ALL;
    //        SubCmd = GetLossRate;
    //    }
    else if(CurNetCmd == NET_ALARM_DATA)
    {
        MainCmd = REQ_ARW_DAT_ALL;
        SubCmd = AlarmData;
    }
    else if(CurNetCmd == NET_RECORD_DATA)
    {
        MainCmd = REQ_ARW_DAT_ALL;
        SubCmd = RecordData;
    }
    else if(CurNetCmd == NET_ALARM_UPDATE)
    {
        MainCmd = REQ_ARW_DAT_ALL;
        SubCmd = AlarmUpdate;
    }
    else if(CurNetCmd == NET_MI_VERSION)
    {
        SubCmd = InverterDevInform_Simple;
        MainCmd = REQ_ARW_DAT_ALL;
    }
}


/***********************************************
** Function name: 三代协议设备控制应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf_Send_DevControlUpdate(u8 *target_adr, u8 *rout_adr)
{
    vu16 relative_value = 0;

    if((SubCmd >= Type_TurnOn) && (SubCmd <= Type_Unlock))
    {
        TotalIndex_Para = 0;
    }
    else if((SubCmd >= Type_ActivePowerContr) && (SubCmd <= Type_PFSet))
    {
        if(SubCmd == Type_ActivePowerContr)
        {
#ifdef DTU3PRO

            if((Dtu3Detail.Property.DRM_Limit_Switch == 1) ||
                    (Dtu3Detail.Property.SunSpec_Switch == 1))
            {
                if(MIMajor[PortNO].Property.Acq_Switch == 0)
                {
                    SubCmd = Type_TurnOff;
                    TotalIndex_Para = 0;
                }
                else
                {
                    TotalIndex_Para = 4;
                    relative_value = MIMajor[PortNO].Property.Power_Limit;

                    if(relative_value > FULL_CONTROL_VALUE)
                    {
                        relative_value = FULL_CONTROL_VALUE;
                    }

                    MIMajor[PortNO].Property.Pass.PowerPFDev.SetValut[0] = (u8)(relative_value >> 8);
                    MIMajor[PortNO].Property.Pass.PowerPFDev.SetValut[1] = (u8)(relative_value);
                    MIMajor[PortNO].Property.Pass.PowerPFDev.Desc[0] = 0;
                    MIMajor[PortNO].Property.Pass.PowerPFDev.Desc[1] = 1;
                }
            }
            else if((Dtu3Detail.Property.Zero_Export_Switch == 1) || (Dtu3Detail.Property.Phase_Balance_Switch == 1))
            {
                TotalIndex_Para = 4;
                MIMajor[PortNO].Property.Pass.PowerPFDev.SetValut[0] = (u8)(MIMajor[PortNO].Property.Power_Limit >> 8);
                MIMajor[PortNO].Property.Pass.PowerPFDev.SetValut[1] = (u8)(MIMajor[PortNO].Property.Power_Limit);
            }

#endif
        }
        else
        {
            TotalIndex_Para = 4;
        }
    }
    else if(SubCmd == Type_CleanState_LockAndAlarm)
    {
        TotalIndex_Para = 2;
    }
    //dong 2020-06-15 并网参数自检
    else if(SubCmd == Type_SelfInspection)
    {
        TotalIndex_Para = 4;
    }

    if(TotalIndex_Para <= 12)//只有1帧
    {
        Uart_SendBufferLen = UsartNrf3_Send_PackDevControl((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 0x81 + ((Index_Para + 4) / 16), TotalIndex_Para - Index_Para);

        if(TotalIndex_Para - 1 >= 0)
        {
            Index_Para = TotalIndex_Para - 1;
        }
        else
        {
            Index_Para = 0;
        }
    }
    else //多帧
    {
        if(((TotalIndex_Para - 1) - Index_Para) <= 14)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackDevControl((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 0x81 + ((Index_Para + 4) / 16), TotalIndex_Para - Index_Para);
            Index_Para = TotalIndex_Para - 1;
            ProtocalLayer_Cmd = ToTransLayer_Send;//传输层
        }
        else
        {
            if(Index_Para == 0)
            {
                Uart_SendBufferLen = UsartNrf3_Send_PackDevControl((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 1 + ((Index_Para + 4) / 16), 14);
                Index_Para = Index_Para + 14;
            }
            else
            {
                Uart_SendBufferLen = UsartNrf3_Send_PackDevControl((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 1 + ((Index_Para + 4) / 16), 16);
                Index_Para = Index_Para + 16;
            }
        }
    }

    if(TotalIndex_Para <= (Index_Para + 1))
    {
        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            ProtocalLayer_Cmd = ToTransLayer_Send;//传输层
            CurSendRecLastPackageNO = TotalIndex_Para / 16;
            memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
        }
    }
}
/***********************************************
** Function name: 三代协议设备控制传输层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_DevControlUpdate(u8 *target_adr, u8 *rout_adr)
{
    if(CurSendRecLostPackageNO >= CurSendRecLastPackageNO)
    {
        if(CurSendRecLastPackageNO > 0)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackDevControl(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data  + (14 + (CurSendRecLostPackageNO - 1) * 16), (CurSendRecLastPackageNO + 1) | 0x80, TotalIndex_Para - (14 + ((CurSendRecLostPackageNO - 1) * 16)));
        }
        else
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackDevControl(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data, (CurSendRecLastPackageNO + 1) | 0x80, TotalIndex_Para);
        }
    }
    else
    {
        if(CurSendRecLostPackageNO == 0)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackDevControl(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data, CurSendRecLostPackageNO + 1, 16);
        }
        else
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackDevControl(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + (14 + (CurSendRecLostPackageNO - 1) * 16), CurSendRecLostPackageNO + 1, 16);
        }
    }
}
/***********************************************
** Function name: 三代协议设备控制传输应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_DevControl(void)
{
    if(ProtocalLayer_Cmd == InitLayer)
    {
        if(UsartNrf_HasSetCurrentInverterOk())
        {
            UsartNrf_Send_DevControlUpdate((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //应用层
        }
    }
    else
    {
        if(UsartNrf3_HasSetCurrentInverterOk())
        {
            UsartNrf3_Send_DevControlUpdate((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //传输层
        }
    }
}



/***********************************************
** Function name: 三代协议并网保护文件传输层打包
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
u8 UsartNrf3_Send_PackUpdateGridOnProFile(u8 *target_adr, u8 *rout_adr)
{
    if(RightGridOnProFile == true)
    {
        if(CurSendRecLostPackageNO >= CurSendRecLastPackageNO)
        {
            if(CurSendRecLastPackageNO > 0)
            {
                Uart_SendBufferLen = UsartNrf_Send_PackGridOnProFile_SigleFrame(target_adr, rout_adr, CurRowData_Dat + (CurSendRecLostPackageNO * 16), (CurSendRecLostPackageNO + 1) | 0x80, (u8)(TotalIndex_Dat - (CurSendRecLostPackageNO * 16)));
            }
            else
            {
                Uart_SendBufferLen = UsartNrf_Send_PackGridOnProFile_SigleFrame(target_adr, rout_adr, CurRowData_Dat, (CurSendRecLostPackageNO + 1) | 0x80, (u8)(TotalIndex_Dat));
            }
        }
        else
        {
            Uart_SendBufferLen = UsartNrf_Send_PackGridOnProFile_SigleFrame(target_adr, rout_adr, CurRowData_Dat + (CurSendRecLostPackageNO * 16), CurSendRecLostPackageNO + 1, 16);
        }
    }

    return (u8)TotalIndex_Dat;
}
/***********************************************
** Function name: 三代协议传输应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_DownLoadDat(void)
{
    if(ProtocalLayer_Cmd == InitLayer)
    {
        if(UsartNrf_HasSetCurrentInverterOk())
        {
            UsartNrf_Send_PackUpdateGridOnProFile((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //应用层
        }
    }
    else
    {
        if(UsartNrf3_HasSetCurrentInverterOk())
        {
            UsartNrf3_Send_PackUpdateGridOnProFile((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //传输层
        }
    }
}
/***********************************************
** Function name: 三代协议参数设置应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf_Send_UpdatePara(u8 *target_adr, u8 *rout_adr)
{
    vu8 AdminPasswd[4] = {10, 16, 50, 82};

    if(SubCmd == EleEnergySet)
    {
        TotalIndex_Para = 18;
        //        MIMajor[PortNO].Property.Pass.EletricSet.Info[1] = 0x0f;
        //        MIMajor[PortNO].Property.Pass.EletricSet.Info[0] = 0x00;
    }
    else if(SubCmd == AntitheftParaSet)
    {
        TotalIndex_Para = 10;

        if(MIReal[PortNO].Data.NetCmd == NET_CANCEL_GUARD)
        {
            if(memcmp((u8 *)AdminPasswd, (u8 *)Dtu3Detail.Property.LockOldPassword, 4) == 0)
            {
                memset((u8 *)Dtu3Detail.Property.LockNewPassword, 0, 4);
                memset((u8 *)Dtu3Detail.Property.LockOldPassword, 0xff, 4);
                memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.PWO, (u8 *)Dtu3Detail.Property.LockOldPassword, 4);
                memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.PWN, (u8 *)Dtu3Detail.Property.LockNewPassword, 4);
                memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.ATTime, (u8 *)Dtu3Detail.Property.Lock_Time, 2);
            }
            else
            {
                MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
            }
        }
        else if(MIReal[PortNO].Data.NetCmd == NET_SET_PASSWORD)
        {
            memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.PWO, (u8 *)Dtu3Detail.Property.LockOldPassword, 4);
            memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.PWN, (u8 *)Dtu3Detail.Property.LockNewPassword, 4);
            memcpy((u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.ATTime, (u8 *)Dtu3Detail.Property.Lock_Time, 2);
        }
    }

    if(TotalIndex_Para <= 12)//只有1帧
    {
        Uart_SendBufferLen = UsartNrf3_Send_PackSetPara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data, 0x81, TotalIndex_Para);
        Index_Para = TotalIndex_Para - 1;
    }
    else //多帧
    {
        if(((TotalIndex_Para - 1) - Index_Para) <= 14)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackSetPara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 0x81 + ((Index_Para + 4) / 16), TotalIndex_Para - Index_Para);
            Index_Para = TotalIndex_Para - 1;
            ProtocalLayer_Cmd = ToTransLayer_Send;//传输层
        }
        else
        {
            if(Index_Para == 0)
            {
                Uart_SendBufferLen = UsartNrf3_Send_PackSetPara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 1 + ((Index_Para + 4) / 16), 14);
                Index_Para = Index_Para + 14;
            }
            else
            {
                Uart_SendBufferLen = UsartNrf3_Send_PackSetPara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + Index_Para, 1 + ((Index_Para + 4) / 16), 16);
                Index_Para = Index_Para + 16;
            }
        }
    }

    if(TotalIndex_Para <= (Index_Para + 1))
    {
        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            ProtocalLayer_Cmd = ToTransLayer_Send;//传输层
            CurSendRecLastPackageNO = TotalIndex_Para / 16;
            memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
        }
    }
}
/***********************************************
** Function name: 三代协议参数设置传输层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_UpdatePara(u8 *target_adr, u8 *rout_adr)
{
    if(CurSendRecLostPackageNO >= CurSendRecLastPackageNO)
    {
        if(CurSendRecLastPackageNO > 0)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackSetPara(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + (14 + (CurSendRecLostPackageNO - 1) * 16), (CurSendRecLastPackageNO + 1) | 0x80, TotalIndex_Para - (14 + ((CurSendRecLostPackageNO - 1) * 16)));
        }
        else
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackSetPara(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data, (CurSendRecLastPackageNO + 1) | 0x80, TotalIndex_Para);
        }
    }
    else
    {
        if(CurSendRecLostPackageNO == 0)
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackSetPara(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data, CurSendRecLostPackageNO + 1, 16);
        }
        else
        {
            Uart_SendBufferLen = UsartNrf3_Send_PackSetPara(target_adr, rout_adr, (u16)(SubCmd << 8), (u8 *)MIMajor[PortNO].Property.Pass.Data + (14 + (CurSendRecLostPackageNO - 1) * 16), CurSendRecLostPackageNO + 1, 16);
        }
    }
}
/***********************************************
** Function name: 三代协议参数设置传输应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_ParaSet(void)
{
    if(ProtocalLayer_Cmd == InitLayer)
    {
        if(UsartNrf_HasSetCurrentInverterOk())
        {
            UsartNrf_Send_UpdatePara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //应用层
        }
    }
    else
    {
        if(UsartNrf3_HasSetCurrentInverterOk())
        {
            UsartNrf3_Send_UpdatePara((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);    //传输层
        }
    }
}
/***********************************************
** Function name: 三代协议单发多收传输层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_One_MultiPackage(u8 *pBuffer)
{
    vu8 i = 0;
    vu16 TempCrc, DatCrc = 0xffff;

    for(i = 11; i < 16; i++)
    {
        if((i - 11) % 2 == 1)
        {
            TempCrc = (u16)(pBuffer[i - 1] << 8) | (pBuffer[i]);
            DatCrc =  CalcCRC16t(TempCrc, DatCrc);
        }
    }

    if((((u16)pBuffer[11 + 4] << 8) | pBuffer[11 + 5]) != DatCrc)
    {
        Uart_CurrentReplyState = 0x01 << 4; //高四位表示传输层回执异常状态
    }
    else
    {
        if(pBuffer[13] == 0x01)
        {
            ProtocalLayer_Poll = ToAppLayer;//不支持该项数据采集
            Uart_CurrentReplyState = 0x01 << 5; //高四位表示传输层回执异常状态
        }
        else if(pBuffer[13] == 0X02)
        {
            ProtocalLayer_Poll = ToAppLayer;//表示数据损坏 (如并网规则文件、硬配置信 如并网规则文件、硬配置信 如并网规则文件、硬配置信 如并网规则文件、硬配置信 如并网规则文件、硬配置信 如并网规则文件、硬配置信 如并网规则文件、硬配置信 息、校准参数系统等 息、校准参数系统等 息、校准参数系统等 息、校准参数系统等 息、校准参数系统等 息、校准参数系统等 )
            Uart_CurrentReplyState = 0x01 << 6; //高四位表示传输层回执异常状态
        }
        else if(pBuffer[13] == 0X07)
        {
            Uart_CurrentReplyState = 0x01 << 7;//表示命令长度错误
        }
        else if(pBuffer[13] == 0X08)
        {
            Uart_CurrentReplyState = 0x01 << 8;//表示固件 损坏
        }
        else if(pBuffer[13] == 0X09)
        {
            Uart_CurrentReplyState = 0x01 << 9;//表示固件 损坏//表示固件正常
        }
        else if(pBuffer[13] == 0X0a)
        {
            Uart_CurrentReplyState = 0x01 << 10;//表示诊断数据为空
        }
        else
        {
        }
    }
}
/***********************************************
** Function name: 三代协议多发单收传输层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_Multi_OnePackage(u8 *pBuffer)
{
    vu8 i = 0;
    vu16 TempCrc, DatCrc = 0xffff;

    for(i = 11; i < 11 + 14; i++)
    {
        if((i - 11) % 2 == 1)
        {
            TempCrc = (u16)(pBuffer[i - 1] << 8) | (pBuffer[i]);
            DatCrc =  CalcCRC16t(TempCrc, DatCrc);
        }
    }

    if((((u16)pBuffer[11 + 14] << 8) | pBuffer[11 + 15]) != DatCrc)
    {
        ProtocalLayer_Cmd = ToTransLayer_Process;//CRC错误
        Uart_CurrentReplyState = 0x01 << 4;
        memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
    }
    else
    {
        if((pBuffer[11] >> 7) == 0x01) //CRC错误
        {
            ProtocalLayer_Cmd = ToAppLayer;
            Uart_CurrentReplyState = 0x01 << 4; //高四位表示额外错误
        }
        else if((pBuffer[11] >> 7) == 0x00) //缺帧
        {
            ProtocalLayer_Cmd = ToTransLayer_Process;//传输层缺帧补发
            memcpy((u8 *)SubData3, &pBuffer[11], MAX_TRANS_DAT_DATA_LEN);
            SubData3[(14 - (CurSendRecLastPackageNO / 8)) - 1] = (u8)(~(1 << (CurSendRecLastPackageNO % 8))&SubData3[(14 - (CurSendRecLastPackageNO / 8)) - 1]);
        }
        else
        {
        }
    }
}

/***********************************************
** Function name: 三代协议微逆程序更新应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DownLoadPro(u8 *pBuffer)
{
    ProtocalLayer_Cmd = ToAppLayer;
    Uart_CurrentReplyState = pBuffer[15];
    memcpy((u8 *)Uart_CurrentReplyAddr, &(pBuffer[11]), 4);

    if(Uart_CurrentReplyState == 0)
    {
        MIReal[PortNO].Data.NetStatus = (u8)((Index_Pro * 100) / TotalIndex_Pro);

        if(MIReal[PortNO].Data.NetStatus == 0)
        {
            MIReal[PortNO].Data.NetStatus = 1;
        }

        if((memcmp((u8 *)Uart_CurSendAddr, (u8 *)Uart_CurrentReplyAddr, 4) == 0) &&
                ((Uart_CurrentReplyAddr[0] == 0) && (Uart_CurrentReplyAddr[1] == 0) && (Uart_CurrentReplyAddr[2] == 0) && (Uart_CurrentReplyAddr[3] == 0x01)))
        {
            memset((u8 *)Uart_CurSendAddr, 0, 4);
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
            UsartNrf3_Process_ClearMIVersion();
            InverterDetail_Write((InverterDetail *)&MIDetail, PortNO, 1);//逆变器RF软硬件版本
            Uart_CurrentReplyState = 0;
            RightHexFile = false;
            CurNetCmd = NET_INIT;
        }
        else
        {
            MIReal[PortNO].Data.NetStatus = (u8)((Index_Pro * 100) / TotalIndex_Pro);

            if(MIReal[PortNO].Data.NetStatus == 0)
            {
                MIReal[PortNO].Data.NetStatus = 1;
            }
        }
    }
    else
    {
        if((Uart_CurrentReplyState >= 1) && (Uart_CurrentReplyState <= 5))
        {
            Index_Dat = 0;
            RightHexFile = false;
        }
    }
}
/***********************************************
** Function name: 三代协议并网保护文件应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DownLoadDat(u8 *pBuffer)
{
    ProtocalLayer_Cmd = ToAppLayer;
    Uart_CurrentReplyState = pBuffer[11];

    if(Uart_CurrentReplyState == 0)
    {
        if(CurRowData_Dat != NULL)
        {
            myfree(CurRowData_Dat);
            CurRowData_Dat = NULL;
        }

        RightGridOnProFile = false;
        MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
        CurNetCmd = NET_INIT;
    }
    else if((Uart_CurrentReplyState == 2) || (Uart_CurrentReplyState == 3)) //2---文件不兼容，3 EEPROM读写错误
    {
        Index_Dat = 0;
        RightGridOnProFile = false;
    }
}
/***********************************************
** Function name:  三代协议微逆所以操作发送
** Descriptions:
** input parameters:
** output parameters:
** Returned value:
*************************************************/
void UsartNrf3_Send_PackNrfCmd(void)
{
    if(CurNetCmd == NET_DOWNLOAD_DAT)
    {
        UsartNrf3_Send_DownLoadDat();//升级并网保护程序
    }
    else if(CurNetCmd == NET_DOWNLOAD_PRO)
    {
        UsartNrf3_Send_DownLoadPro();//升级微逆固件
    }
    else if((CurNetCmd == NET_INVERTER_HW_INFOR) || (CurNetCmd == NET_TERMINAL_INFOR) || (CurNetCmd == NET_POLL_GRID_ON_FILE))
    {
        UsartNrf3_Send_PackPollVersionCmd();//三代协议打包轮询版本
    }
    else if((CurNetCmd == NET_TURN_ON) || (CurNetCmd == NET_TURN_OFF) || (CurNetCmd == NET_LIMIT_POEWR) ||
            (CurNetCmd == NET_RESTART) || (CurNetCmd == NET_LOCK) || (CurNetCmd == NET_UNLOCK) ||
            (CurNetCmd == NET_LIMIT_REACTIVE_POWER) || (CurNetCmd == NET_LIMIT_ACTIVE_POEWR) || (CurNetCmd == NET_PF_SET) ||
            (CurNetCmd == NET_CLEAR_ALARM) || (CurNetCmd == NET_CLEAN_GFDI)
            //dong 2020-06-15
            || (CurNetCmd == NET_SELF_INSPECTION))
    {
        UsartNrf3_Send_DevControl();//设备控制指令
    }
    else if((CurNetCmd == NET_SET_ENERGY) || (CurNetCmd == NET_ELE_ENERGY) || (CurNetCmd == NET_SET_PASSWORD) || (CurNetCmd == NET_CANCEL_GUARD))
    {
        UsartNrf3_Send_ParaSet();
    }
    else
    {
    }
}
/***********************************************
** Function name: 三代协议回执状态是否ok

** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_SendLoop_ReplyOK(void)
{
    if(Uart_CurSendMainCmd == REQ_RF_RVERSISON)
    {
        if(Uart_CurRecMainCmd == ANSWER_REQ_RF_RVERSISON)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if(Uart_CurSendMainCmd == REQ_ARW_DAT_ALL)
    {
        if((Uart_CurRecMainCmd == ANSWER_REQ_ARW_DAT_ALL) && (Uart_CurRecSubCmd == GetLossRate))
        {
            ProtocalLayer_Poll = InitLayer;
            Uart_CurRecSubCmd = 0;
            return true;
        }
        else
        {
            if((Uart_CurRecMainCmd == ANSWER_REQ_ARW_DAT_ALL) && (Uart_CurrentReplyState == 0) && (ProtocalLayer_Poll == ToAppLayer))
            {
                ProtocalLayer_Poll = InitLayer;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else if(Uart_CurSendMainCmd == DOWN_DAT)
    {
        if((Uart_CurRecMainCmd == ANSWER_DOWN_DAT) && (Uart_CurrentReplyState == 0) && (ProtocalLayer_Cmd == ToAppLayer))
        {
            ProtocalLayer_Cmd = InitLayer;
            //传输层与应用层都正确;
            return true;
        }
        else
        {
            return false;
        }
    }
    else if(Uart_CurSendMainCmd == DOWN_PRO)
    {
        if((Uart_CurRecMainCmd == ANSWER_DOWN_PRO) && (Uart_CurrentReplyState == 0) && (ProtocalLayer_Cmd == ToAppLayer))
        {
            ProtocalLayer_Cmd = InitLayer;
            //传输层与应用层都正确;
            return true;
        }
        else
        {
            return false;
        }
    }
    else if(Uart_CurSendMainCmd == CHANGE_MOD_2M_250K)
    {
        return true;
    }
    else
    {
        return false;
    }

    return true;
}
/***********************************************
** Function name: 三代协议单发多收传输层补传机制
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_Send_One_MultiPackage(u8 *pTransLayerPollCnt)
{
    vu8 j;
    CurRecSendLostPackageNO++;

    if(CurRecSendLostPackageNO >= TotalPackageNum)
    {
        CurRecSendLostPackageNO = 0;

        if(Uart_CurrentReplyState == (0x01 << 4))
        {
            (*pTransLayerPollCnt)++;
        }
    }

    if((*pTransLayerPollCnt) >= TRANSLAYER_CYCLES)//重发超时
    {
        Uart_SendBufferLen = 0;
        Uart_CurSendMainCmd = 0;
        Uart_CurRecMainCmd = 0;
        CurRecSendLostPackageState = 0;
        (*pTransLayerPollCnt)  = 0;
        TotalPackageNum = 0;
        return true;
    }
    else
    {
        if(TotalPackageNum <= 0)
        {
            return false;
        }

        for(j = CurRecSendLostPackageNO; j < TotalPackageNum; j++)
        {
            if(((CurRecSendLostPackageState >> j) & 0x01) == 0x00)
            {
                CurRecSendLostPackageNO = j;
                return false;
            }
        }

        for(j = 0; j < CurRecSendLostPackageNO; j++)
        {
            if(((CurRecSendLostPackageState >> j) & 0x01) == 0x00)
            {
                CurRecSendLostPackageNO = j;
                return false;
            }
        }
    }

    return false;
}
/***********************************************
** Function name: 三代协议多发单收传输层补传机制
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_Send_Multi_OnePackage(u8 *pTransLayerCnt)
{
    vs8 i, j;
    CurSendRecLostPackageNO++;

    if(CurSendRecLostPackageNO > CurSendRecLastPackageNO)
    {
        CurSendRecLostPackageNO = 0;

        if(Uart_CurrentReplyState == 0x10)
        {
            (*pTransLayerCnt)++;    // 重发循环计次
        }
    }

    if((*pTransLayerCnt) >= TRANSLAYER_CYCLES)//重发超时
    {
        Uart_SendBufferLen = 0;
        Uart_CurSendMainCmd = 0;
        Uart_CurRecMainCmd = 0;
        (*pTransLayerCnt)  = 0;
        Uart_CurrentReplyState = 0;
        return true;
    }
    else
    {
        j = CurSendRecLostPackageNO % 8;

        for(i = 14 - (CurSendRecLostPackageNO / 8) - 1; i >= 11; i--)
        {
            for(j = j; j < 8; j++)
            {
                if(((SubData3[i] >> j) & 0x01) == 0x00)
                {
                    CurSendRecLostPackageNO = (u8)(j + (14 - i - 1) * 8);
                    return false;
                }
            }

            j = 0;
        }
    }

    return false;
}
/***********************************************
** Function name: 三代协议设备控制和参数设置传输应用层重发机制
** Descriptions:
** input parameters:    ?
** Returned value:      ?
** output parameters:   ?
*************************************************/
bool UsartNrf3_HasSetCurrentInverterOk()
{
    static vu8 TransLayerCmdCnt = 0, AppLayerCmdCnt = 0;
    static vu8 TransLayerProCnt = 0;
    volatile bool HasOverTime = false;

    //  dong 2020-05-12修改 由大于修改成大于等于
    if((LocalTime >= LocalTime_TaskTimeOut) && (LocalTime_TaskTimeOut > 0))
    {
        AppLayerCmdCnt = APPLAYER_CYCLES + 1;
        ProtocalLayer_Cmd = ToAppLayer;
        HasOverTime = true;
        UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
    }

    if(UsartNrf3_SendLoop_ReplyOK() == true)
    {
        TransLayerCmdCnt = 0;
        AppLayerCmdCnt = 0;

        if((Uart_CurSendMainCmd != 0) && (Uart_CurSendMainCmd != CHANGE_MOD_2M_250K))
        {
            Uart_SendBufferLen = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            return UsartNrf_SendLoop_GetNextSetCmd();
        }
        else
        {
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            return true;//波特率收到回执后需要发送网络命令
        }
    }
    else
    {
        if((Uart_CurSendMainCmd != 0) && (Uart_CurSendMainCmd != CHANGE_MOD_2M_250K))
        {
            if((MIReal[PortNO].Data.NetCmd == NET_DOWNLOAD_DAT)
                    || ((MIReal[PortNO].Data.NetCmd == NET_ELE_ENERGY) || (MIReal[PortNO].Data.NetCmd == NET_SET_PASSWORD) || (MIReal[PortNO].Data.NetCmd == NET_CANCEL_GUARD))
                    || (((CurNetCmd == NET_TURN_ON) || (CurNetCmd == NET_TURN_OFF) || (CurNetCmd == NET_LIMIT_POEWR) || (CurNetCmd == NET_SET_ENERGY)
                         || (CurNetCmd == NET_RESTART) || (CurNetCmd == NET_LOCK) || (CurNetCmd == NET_UNLOCK) || (CurNetCmd == NET_LIMIT_REACTIVE_POWER) || (CurNetCmd == NET_LIMIT_ACTIVE_POEWR)
                         || (CurNetCmd == NET_PF_SET)  || (CurNetCmd == NET_CLEAR_ALARM) || (CurNetCmd == NET_CLEAN_GFDI)
                         //dong 2020-06-15
                         || (CurNetCmd == NET_SELF_INSPECTION)
                        )))
            {
                if(ProtocalLayer_Cmd == ToTransLayer_Process)
                {
                    if(UsartNrf3_Send_Multi_OnePackage((u8 *)&TransLayerCmdCnt))
                    {
                        MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                        CurNetCmd = NET_INIT;

                        if(MIReal[PortNO].Data.NetCmd == NET_DOWNLOAD_DAT)
                        {
                            RightGridOnProFile = false;

                            if(CurRowData_Dat != NULL)
                            {
                                myfree(CurRowData_Dat);
                                CurRowData_Dat = NULL;
                            }
                        }

                        ProtocalLayer_Cmd = InitLayer;
                        return UsartNrf_SendLoop_GetNextSetCmd();
                    }
                }
                else if(ProtocalLayer_Cmd == ToTransLayer_Send)
                {
                    CurSendRecLostPackageNO = CurSendRecLastPackageNO;
                }
                else if(ProtocalLayer_Cmd == ToAppLayer)//应用层
                {
                    if(((MIReal[PortNO].Data.NetCmd == NET_DOWNLOAD_DAT) && (((Uart_CurrentReplyState > 1) && (Uart_CurrentReplyState <= 3)) || (Uart_CurrentReplyState == 0x10))) //参数crc错误1,并网文件不兼容2,eeprom读写错误3
                            || (((MIReal[PortNO].Data.NetCmd == NET_ELE_ENERGY) || (MIReal[PortNO].Data.NetCmd == NET_SET_PASSWORD) || (MIReal[PortNO].Data.NetCmd == NET_CANCEL_GUARD)) && (((Uart_CurrentReplyState > 1) && (Uart_CurrentReplyState <= 3)) || (Uart_CurrentReplyState == 0x10)))
                            || (((CurNetCmd == NET_TURN_ON) || (CurNetCmd == NET_TURN_OFF) || (CurNetCmd == NET_LIMIT_POEWR) || (CurNetCmd == NET_SET_ENERGY)
                                 || (CurNetCmd == NET_RESTART) || (CurNetCmd == NET_LOCK) || (CurNetCmd == NET_UNLOCK) || (CurNetCmd == NET_LIMIT_REACTIVE_POWER) || (CurNetCmd == NET_LIMIT_ACTIVE_POEWR)
                                 || (CurNetCmd == NET_PF_SET)  || (CurNetCmd == NET_CLEAR_ALARM) || (CurNetCmd == NET_CLEAN_GFDI)
                                 || (CurNetCmd == NET_SELF_INSPECTION)) //dong 2020-06-15
                                && ((((Uart_CurrentReplyState == 3) && (Uart_CurrentReplyState == 4)) || (Uart_CurrentReplyState == 7)))))
                    {
                        TransLayerCmdCnt = 0;
                        memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
                        AppLayerCmdCnt++;
                        ProtocalLayer_Cmd = InitLayer;
                        Index_Dat = 0;
                        Index_Para = 0;
                        Uart_CurrentReplyState = 0;
                    }

                    if(AppLayerCmdCnt > APPLAYER_CYCLES)
                    {
                        AppLayerCmdCnt = 0;
                        Uart_SendBufferLen = 0;
                        Uart_CurSendMainCmd = 0;
                        Uart_CurRecMainCmd = 0;
                        memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
                        Index_Dat = 0;
                        Index_Para = 0;
                        Uart_CurrentReplyState = 0;
                        ProtocalLayer_Cmd = InitLayer;

                        if(MIReal[PortNO].Data.NetCmd == NET_DOWNLOAD_DAT)
                        {
                            //Grid_Profiles_Data_Delete();
                            if(CurRowData_Dat != NULL)
                            {
                                myfree(CurRowData_Dat);
                                CurRowData_Dat = NULL;
                            }
                        }

                        MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                        CurNetCmd = NET_INIT;
                        RightGridOnProFile = false;
                        return UsartNrf_SendLoop_GetNextSetCmd();
                    }

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if(MIReal[PortNO].Data.NetCmd == NET_DOWNLOAD_PRO)
            {
                if(ProtocalLayer_Cmd == ToTransLayer_Process)//传输层
                {
                    if(UsartNrf3_Send_Multi_OnePackage((u8 *)&TransLayerCmdCnt))
                    {
                        ProtocalLayer_Cmd = InitLayer;
                        TransLayerProCnt++;

                        if(TransLayerProCnt >= APPLAYER_CYCLES_PRO)
                        {
                            TransLayerProCnt = 0;
                            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                            CurNetCmd = NET_INIT;
                            RightHexFile = false;

                            if(CurRowData_Pro != NULL)
                            {
                                myfree(CurRowData_Pro);
                                CurRowData_Pro = NULL;
                            }

                            if(string != NULL)
                            {
                                myfree(string);
                                string = NULL;
                            }

                            return UsartNrf_SendLoop_GetNextSetCmd();
                        }
                    }
                }
                else if(ProtocalLayer_Cmd == ToTransLayer_Send)
                {
                    CurSendRecLostPackageNO = CurSendRecLastPackageNO;
                }
                else if(ProtocalLayer_Cmd == ToAppLayer)//应用层
                {
                    if(((Uart_CurrentReplyState >= 2) && (Uart_CurrentReplyState <= 7)) ||
                            (Uart_CurrentReplyState == 0x10) || (HasOverTime == true)) //参数crc错误1,并网文件不兼容2,eeprom读写错误3
                    {
                        TransLayerCmdCnt = 0;
                        memset((u8 *)SubData3, 0, MAX_TRANS_DAT_DATA_LEN);
                        AppLayerCmdCnt++;
                        ProtocalLayer_Cmd = InitLayer;
                        Index_Pro = 0;
                        RightHexFile = false;
                        Uart_CurrentReplyState = 0;

                        if(AppLayerCmdCnt >= APPLAYER_CYCLES)
                        {
                            AppLayerCmdCnt = 0;

                            if(CurRowData_Pro != NULL)
                            {
                                myfree(CurRowData_Pro);
                                CurRowData_Pro = NULL;
                            }

                            if(string != NULL)
                            {
                                myfree(string);
                                string = NULL;
                            }

                            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                            return UsartNrf_SendLoop_GetNextSetCmd();
                        }
                    }

                    return true;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            return true;
        }
    }

    return true;
}

/***********************************************
** Function name: 三代协议设置任务级超时时间
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_SendLoop_SetTaskTimeOut(bool Stop)
{
    if(Stop == true)
    {
        LocalTime_TaskTimeOut = LocalTime;
    }
    else
    {
        if((CurNetCmd >= NET_TURN_ON) && (CurNetCmd <= NET_ELE_ENERGY))//命令和配置
        {
            switch(CurNetCmd)
            {
                case NET_DOWNLOAD_DAT:
                case NET_POLL_GRID_ON_FILE:
                    //6S
                    LocalTime_TaskTimeOut = LocalTime + TASK_CONFIG_GRID_ON_FILE;
                    break;

                case NET_DOWNLOAD_PRO:
                    if(LineCnt_Pro == 1)
                    {
                        LocalTime_TaskTimeOut = LocalTime + TASK_DOWNLOAD_SECOND_LINE;
                    }
                    else if((TotalIndex_Pro - Index_Pro) <= 76)
                    {
                        LocalTime_TaskTimeOut = LocalTime + TASK_DOWNLOAD_LAST_LINE;
                    }
                    else
                    {
                        LocalTime_TaskTimeOut = LocalTime + TASK_DOWNLOAD_OTHER_LINE;
                    }

                    break;

                case NET_ELE_ENERGY:
                case NET_SET_ENERGY:
                    LocalTime_TaskTimeOut = LocalTime + TASK_CONFIG_ELE_ENERGY;
                    break;

                case NET_SET_PASSWORD:
                    LocalTime_TaskTimeOut = LocalTime + TASK_CONFIG_ANTITHEFT;
                    break;

                case NET_CLEAR_ALARM:
                case NET_CLEAN_GFDI:
                    LocalTime_TaskTimeOut = LocalTime + TASK_DEV_CONTROL_ALARM_CLEAR;
                    break;

                case NET_TURN_ON:
                case NET_TURN_OFF:
                    LocalTime_TaskTimeOut = LocalTime + TASK_DEV_CONTROL_TURN_ONOFF;
                    break;

                case NET_RESTART:
                    LocalTime_TaskTimeOut = LocalTime + TASK_DEV_CONTROL_RESTART;
                    break;

                case NET_LOCK:
                case NET_UNLOCK:
                    LocalTime_TaskTimeOut = LocalTime + TASK_DEV_CONTROL_LOCK_UNLOCK ;
                    break;

                case NET_LIMIT_ACTIVE_POEWR:
                case NET_PF_SET:
                case NET_LIMIT_REACTIVE_POWER:
                    LocalTime_TaskTimeOut = LocalTime + TASK_DEV_CONTROL_POWER_PF_ADJUST ;
                    break;

                //dong 2020-06-15
                case NET_SELF_INSPECTION:
                    LocalTime_TaskTimeOut = LocalTime + TASK_SELF_CHECK0;
                    break;

                default:
                    LocalTime_TaskTimeOut = LocalTime + LOCAL_TIME_1S;
                    break;
            }
        }
        else //数据轮询
        {
            switch(CurNetCmd)
            {
                case NET_REGISTER_ID:
                    LocalTime_TaskTimeOut = LocalTime + TASK_TRANS_DATA_RUNDATA_REALITY + LOCAL_TIME_1S;
                    break;

                case NET_INIT:
                    LocalTime_TaskTimeOut = LocalTime + TASK_TRANS_DATA_RUNDATA_REALITY;
                    break;

                case NET_ALARM_DATA:
                case NET_ALARM_UPDATE:
                    LocalTime_TaskTimeOut = LocalTime + TASK_TRANS_DATA_ALARM;
                    break;

                case NET_RECORD_DATA:
                    LocalTime_TaskTimeOut = LocalTime + TASK_TRANS_DATA_RECORD_DATA;
                    break;

                case NET_INVERTER_HW_INFOR:
                case NET_MI_VERSION:
                    LocalTime_TaskTimeOut = LocalTime + TASK_TRANS_DATA_INFORM_SIMPLE;
                    break;

                case NET_GET_LOSS_RATE:
                    LocalTime_TaskTimeOut = LocalTime + LOCAL_TIME_100MS;
                    break;

                default:
                    break;
            }
        }
    }
}
/***********************************************
** Function name: 搜索下个微逆或者组件序号
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_SearchNextPannelOrInv(void)
{
    Uart_SendBufferLen = 0;
    Uart_CurSendMainCmd = 0;
    Uart_CurRecMainCmd = 0;

    if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
    {
        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToFour)
        {
            PortNO = PortNO + 4;
        }
        else if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToTwo)
        {
            PortNO = PortNO + 2;
        }
        else if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) == Inverter_HM_OneToOne)
        {
            PortNO = PortNO + 1;
        }
        else
        {
            PortNO = PortNO + 4;
        }
    }
    else
    {
        PortNO++;
    }

    if(PortNO >= Dtu3Detail.Property.PortNum)
    {
        PortNO = 0;
    }

    //dong执行的意义
    if((CurAlarmState == AlarmInforUpdate_App) || ((CurAlarmState != AlarmInforUpdate_App) && (CurNetCmd != NET_INIT)))
    {
        return;
    }

    for(PortNO = PortNO; PortNO < Dtu3Detail.Property.PortNum; PortNO++)
    {
        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            if(CurPollIsDebugDataTime == true)
            {
                CurNetCmd = NET_INIT;
                return;
            }
            else
            {
                if(((MIReal[PortNO].Data.LinkState & MI_CONNECT) >> 1) != 1)
                {
                    CurNetCmd = NET_INIT;
                    return;
                }
            }
        }
        else
        {
            return;
        }
    }
}
/***********************************************
** Function name: 三代协议15min全部告警存储
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_AlarmFlashWriteEvery15Min(void)
{
    if(RealAlarmDataNO > 0)
    {
        //Alarm_Data_Write(&pRealAlarm, RealAlarmDataNO + 1); // 告警数据写入flash
        RealAlarmDataNO = 0;
        //        if(pRealAlarm != NULL)
        //        {
        //            myfree(pRealAlarm);
        //            pRealAlarm = NULL;
        //        }
    }
}
/***********************************************
** Function name: 三代协议app状态切换
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_AppStateSwitch(u8 TotalCnt, u8 SucCnt)
{
    //dong 2020-0515
    //    static vu8 AppPollCnt = 0;
    //    static vu8 RecieveDataCnt = 0;
    static vu16 PortNO_DataBreak = 0;
    static vu16 PortNO_RateBreak = 0;
    static vu16 PortNO_SigleStart = 0;
    vu16 i;

    if(AppState == PollAllMi)
    {
        if(CurNetCmd == NET_ALARM_UPDATE)
        {
            UsartNrf3_Send_SearchNextPannelOrInv();//遍历到下个 组件或微逆
            InverterDetail_Read((InverterDetail *)&MIDetail, PortNO, 1);

            if((MIDetail.Property.AppFWBuild_VER == 0) && (MIDetail.Property.HW_VER == 0))
            {
                CurNetCmd = NET_MI_VERSION;
            }
            else
            {
                CurNetCmd = NET_INIT;
            }
        }
        else if(CurNetCmd == NET_MI_VERSION)
        {
            CurNetCmd = NET_INIT;
        }
        else
        {
            if(PortNO_App == Dtu3Detail.Property.PortNum)
            {
                PortNO = Dtu3Detail.Property.PortNum - 1;
                PortNO_App = 0;
            }
            else if(PortNO_App == (Dtu3Detail.Property.PortNum + 1))
            {
                PortNO = PortNO_DataBreak;
                PortNO_App = 0;
            }
            else
            {
                PortNO_DataBreak = PortNO;
            }

#ifdef DEBUG0610
            printf("3174--NET_ALARM_UPDATE\n\r");
#endif
            CurNetCmd = NET_ALARM_UPDATE;
        }

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            CurRecSendLostPackageState = 0;
            return true;
        }
        else
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            return true;
        }
    }
    else if(AppState == PollSigleMi)
    {
        if(PortNO_App < Dtu3Detail.Property.PortNum)
        {
            PortNO = PortNO_App;
            PortNO_App = Dtu3Detail.Property.PortNum;
            PortNO_SigleStart = PortNO;
        }

        if(CurNetCmd == NET_ALARM_UPDATE)
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) < Inverter_Pro)
            {
                if(PORT_NUMBER_CONFIRMATION2(PortNO))
                {
                    PortNO = PortNO_SigleStart;
                }
                else
                {
                    UsartNrf3_Send_SearchNextPannelOrInv();//遍历到下个 组件或微逆
                }
            }

            if((MIDetail.Property.AppFWBuild_VER == 0) && (MIDetail.Property.HW_VER == 0))
            {
                CurNetCmd = NET_MI_VERSION;
            }
            else
            {
                CurNetCmd = NET_INIT;
            }
        }
        else if(CurNetCmd == NET_MI_VERSION)
        {
            CurNetCmd = NET_INIT;
        }
        else
        {
#ifdef DEBUG0610
            printf("3230--NET_ALARM_UPDATE\n\r");
#endif
            CurNetCmd = NET_ALARM_UPDATE;
        }

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            CurRecSendLostPackageState = 0;
            return true;
        }
        else
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            return true;
        }
    }
    //全部微逆的通讯状态统计
    else if(AppState == AllMiLossRate)
    {
        if(PortNO_App == Dtu3Detail.Property.PortNum)
        {
            PortNO = 0;
            PortNO_App = 0;
            AppPollCnt = 0;
            RecieveDataCnt = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;

            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                MIReal[i].Data.NetStatus = 0;
            }

            TotalCnt = 0;
            SucCnt = 0;
        }
        else if(PortNO_App == (Dtu3Detail.Property.PortNum + 1))
        {
            PortNO = PortNO_RateBreak;
            AppPollCnt = 0;
            RecieveDataCnt = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            PortNO_App = 0;
        }
        else
        {
            PortNO_RateBreak = PortNO;
        }

        if(AppPollCnt > 21)
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
            {
                /*hzwang_20200421 */
                //                if(((pDataCnt->MI_CF_Num_End != 0) && (pDataCnt->MI_CF_Num_Start != 0)) && (pDataCnt->MI_CF_Num_End != pDataCnt->MI_CF_Num_Start))
                //                {
                //                    MIReal[PortNO].Data.NetStatus = (abs(pDataCnt->MI_CF_Num_End - pDataCnt->MI_CF_Num_Start)) * 100 / abs(pDataCnt->MI_RF_Num_End - pDataCnt->MI_RF_Num_Start);
                //                }
                //                else
                //                {
                MIReal[PortNO].Data.NetStatus = RecieveDataCnt * 100 / 20;
                //                }

                /*修改三代微逆连接状态计数方式*/
                if(pDataCnt != NULL)
                {
                    myfree(pDataCnt);
                    pDataCnt = NULL;
                }
            }
            else
            {
                MIReal[PortNO].Data.NetStatus = RecieveDataCnt * 100 / 20;
            }

            RecieveDataCnt = 0;
            AppPollCnt = 0;
            UsartNrf3_Send_SearchNextPannelOrInv();//遍历到下个 组件或微逆

            for(i = 0; i < 4; i++)
            {
                if(PORT_NUMBER_CONFIRMATION(PortNO))
                {
                    break;
                }
                else
                {
                    UsartNrf3_Send_SearchNextPannelOrInv();    //遍历到下个 组件或微逆
                }
            }
        }
        else
        {
            //dong 2020-0515
            //            if(SucCnt >= 1)
            //            {
            //                RecieveDataCnt = RecieveDataCnt + SucCnt;
            //                AppPollCnt = AppPollCnt + TotalCnt;
            //            }
            //            else
            //            {
            //                AppPollCnt = AppPollCnt + TotalCnt;
            //            }
        }

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            CurNetCmd = NET_GET_LOSS_RATE;
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            CurRecSendLostPackageState = 0;

            if(pDataCnt == NULL)
            {
                pDataCnt = mymalloc(sizeof(DataCntType));
                //                if(pDataCnt == NULL)
                //                {
                //                    return 0;
                //                }
                pDataCnt->MI_CF_Num_End = 0;
                pDataCnt->MI_RF_Num_End = 0;
                pDataCnt->MI_CF_Num_Start = 0;
                pDataCnt->MI_RF_Num_Start = 0;
            }

            return true;
        }
        else
        {
            CurNetCmd = NET_INIT;
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            return true;
        }
    }
    //单个微逆的通讯状态统计
    else if(AppState == ManuSigleLossRate)
    {
        if(PortNO_App < Dtu3Detail.Property.PortNum)
        {
            PortNO = PortNO_App;
            AppPollCnt = 0;
            RecieveDataCnt = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            PortNO_App = Dtu3Detail.Property.PortNum;
        }

        if(AppPollCnt >= 100)
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
            {
                //                /*hzwang_20200421 */
                //                if(((pDataCnt->MI_CF_Num_End != 0) && (pDataCnt->MI_CF_Num_Start != 0)) && (pDataCnt->MI_CF_Num_End != pDataCnt->MI_CF_Num_Start))
                //                {
                //                    MIReal[PortNO].Data.NetStatus = (abs(pDataCnt->MI_CF_Num_End - pDataCnt->MI_CF_Num_Start)) * 100 / abs(pDataCnt->MI_RF_Num_End - pDataCnt->MI_RF_Num_Start);
                //                }
                //                else
                //                {
                MIReal[PortNO].Data.NetStatus = RecieveDataCnt;
                //                }

                /*修改三代微逆连接状态计数方式*/
                if(pDataCnt != NULL)
                {
                    myfree(pDataCnt);
                    pDataCnt = NULL;
                }
            }
            else
            {
                MIReal[PortNO].Data.NetStatus = RecieveDataCnt;
            }

            AppPollCnt = 0;
            RecieveDataCnt = 0;
            MIReal[PortNO].Data.Real_Time = RTC_Getsecond();
        }
        else
        {
            //dong 2020-0515
            //            if(SucCnt >= 1)
            //            {
            //                RecieveDataCnt = RecieveDataCnt + SucCnt;
            //                AppPollCnt = AppPollCnt + TotalCnt;
            //            }
            //            else
            //            {
            //                AppPollCnt = AppPollCnt + TotalCnt;
            //            }
        }

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            CurNetCmd = NET_GET_LOSS_RATE;
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            CurRecSendLostPackageState = 0;

            if(pDataCnt == NULL)
            {
                pDataCnt = mymalloc(sizeof(DataCntType));
                //                if(pDataCnt == NULL)
                //                {
                //                    return 0;
                //                }
                pDataCnt->MI_CF_Num_End = 0;
                pDataCnt->MI_RF_Num_End = 0;
                pDataCnt->MI_CF_Num_Start = 0;
                pDataCnt->MI_RF_Num_Start = 0;
            }

            return true;
        }
        else
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            CurNetCmd = NET_INIT;
            return true;
        }
    }
    else
    {
        AppPollCnt = 0;
        RecieveDataCnt = 0;
    }

    return false;
}
/***********************************************
** Function name:  三代协议联网时挂起告警搜索
** Descriptions:
** input parameters:    false-->下个循环 true-->下个组件
** output parameters:   ?
** Returned value:      ?
*************************************************/
bool UsartNrf3_AlarmInformationSearch(void)
{
    for(PortNO_Alarm_Server = PortNO_Alarm_Server; PortNO_Alarm_Server < Dtu3Detail.Property.PortNum ; PortNO_Alarm_Server++)
    {
        if((UsartNrf_GetInvterType((u8 *)MIMajor[PortNO_Alarm_Server].Property.Pre_Id) >= Inverter_Pro) && (PORT_NUMBER_CONFIRMATION(PortNO_Alarm_Server)))
        {
#ifdef DEBUG0610
            printf("3462--NET_ALARM_UPDATE\n\r");
#endif
            CurNetCmd = NET_ALARM_UPDATE;
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            PortNO = PortNO_Alarm_Server;
            PortNO_Alarm_Server++;
            CurRecSendLostPackageState = 0;
            TotalPackageNum = 0;
            Uart_CurrentReplyState = 0;
#if DEBUG0610
            printf("PortNO_Alarm:%d--PortNum:%d\n\r", PortNO_Alarm_Server, Dtu3Detail.Property.PortNum);
#endif
            return true;
        }
    }

    if(PortNO_Alarm_Server >= Dtu3Detail.Property.PortNum)
    {
        CurAlarmState = InitState;
        CurNetCmd = NET_INIT;
        PortNO_Alarm_Server = 0;
        CurRecSendLostPackageState = 0;
        //dong 2020-05-20 挂起告警的序号是否需要同步记录
        //        CurInformAlarmNum = ((u16)pInformAlarm[InformAlarmDataNO].Data.WNum[0]) | pInformAlarm[InformAlarmDataNO].Data.WNum[1];
        //        CurRealAlarmNum = WarnSerNub[PortNO];
        //轮训结束不够20条也要存储
#ifdef DEBUG0619
        printf("AlarmInfo_Write:%d", InformAlarmDataNO);
#endif
        AlarmInfo_Write((AlarmDataType *)&pInformAlarm, InformAlarmDataNO); // 告警数据写入flash
        InformAlarmDataNO = 0;
        memset((AlarmDataType *)pInformAlarm, 0, 20 * sizeof(AlarmDataType));
        PortNO = PortNO_Alarm_Server;
        return true;
    }

    return false;
}
/***********************************************
** Function name: 三代协议强制轮询版本和实时数据
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
************************************************/
bool UsartNrf3_PollRealTimeWithVersion(void)
{
    vu8 i;

    if(CurNetCmd == NET_GET_LOSS_RATE)
    {
        return true;
    }

    if(CurNetCmd == NET_MI_VERSION)
    {
        CurNetCmd = NET_INIT;

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            Uart_SendBufferLen = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            CurRecSendLostPackageState = 0;
            NeedLostPackageState = 0;
            TotalPackageNum = 0;
            Uart_CurrentReplyState = 0;
            return true;
        }
        else
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            return true;
        }
    }
    else
    {
        for(i = 0; i < 4; i++)
        {
            UsartNrf3_Send_SearchNextPannelOrInv();//遍历到下个 组件或微逆

            if((PORT_NUMBER_CONFIRMATION(PortNO)) || (PORT_NUMBER_CONFIRMATION1(PortNO)))
            {
                break;
            }
        }

        InverterDetail_Read((InverterDetail *)&MIDetail, PortNO, 1);

        if((MIDetail.Property.AppFWBuild_VER == 0) && (MIDetail.Property.HW_VER == 0))
        {
            CurNetCmd = NET_MI_VERSION;
        }
        else
        {
            CurNetCmd = NET_INIT;
        }

        if(UsartNrf_GetInvterType((u8 *)MIMajor[PortNO].Property.Pre_Id) >= Inverter_Pro)
        {
            UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
            //            UsartNrf3_Process_LedShow(false);
            Uart_SendBufferLen = 0;
            Uart_CurSendMainCmd = 0;
            Uart_CurRecMainCmd = 0;
            CurRecSendLostPackageState = 0;
            NeedLostPackageState = 0;
            TotalPackageNum = 0;
            Uart_CurrentReplyState = 0;
            return true;
        }
        else
        {
            UsartNrf_Process_LedShow(false, 10);
            UsartNrf3_SendLoop_SetTaskTimeOut(true);//设置任务超时时间
            return true;
        }
    }

    return false;
}
/***********************************************
** Function name: 三代协议传输应用层轮询重发机制
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
************************************************/
bool UsartNrf3_HasPollCurInverterPort(void)
{
    static vu8 TransLayerPollCnt = 0, AppLayerPollCnt = 0;
    vu16 i;

    //  dong 2020-05-12修改 由大于修改成大于等于
    if((LocalTime >= LocalTime_TaskTimeOut) && (LocalTime_TaskTimeOut > 0))
    {
        //APP
        if(CurAlarmState == AlarmInforUpdate_App)
        {
            //dong 2020-0515
            UsartNrf3_AppStateSwitch(1, 0);
            //            return UsartNrf3_AppStateSwitch(1, 0);
        }
        //挂起告警
        else if(CurAlarmState == AlarmInforUpdate_Server)
        {
#ifdef DEBUG0610
            printf("AlarmInforUpdate_Server -- 3634\n\r");
#endif
            return UsartNrf3_AlarmInformationSearch();
        }
        else
        {
            CurAlarmState = InitState;

            if(CurNetCmd == NET_INVERTER_HW_INFOR)
            {
                if(MIReal[PortNO].Data.NetStatus == NET_NOT_EXECUTED)
                {
                    MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                    UsartNrf_ClearVersionActionState();
                }

                memset((InverterDetail *)&MIDetail, 0, sizeof(InverterDetail)); //清空MIDetail
                InverterDetail_Read((InverterDetail *)&MIDetail, PortNO, 1); //读
                UsartNrf_Process_Version_InitInverterRf();//填0
                InverterDetail_Write((InverterDetail *)&MIDetail, PortNO, 1); //写0
                UsartNrf3_PollRealTimeWithVersion();
                return false;
            }
            else if(CurNetCmd == NET_POLL_GRID_ON_FILE)
            {
                MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
                UsartNrf3_PollRealTimeWithVersion();
                LocalTime_TaskTimeOut = 0;
                return false;
            }
            else
            {
                return UsartNrf3_PollRealTimeWithVersion();
            }
        }
    }
    else
    {
        if(((MIReal[PortNO].Data.NetCmd != NET_INVERTER_HW_INFOR) && (MIReal[PortNO].Data.NetStatus != NET_NOT_EXECUTED)) ||
                ((MIReal[PortNO].Data.NetCmd != NET_POLL_GRID_ON_FILE) && (MIReal[PortNO].Data.NetStatus != NET_NOT_EXECUTED)))
        {
            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(((MIReal[i].Data.NetCmd >= NET_TURN_ON) && (MIReal[i].Data.NetCmd <= NET_ELE_ENERGY)
                        && ((MIReal[i].Data.NetCmd != NET_WAIT_DOWNLOAD_DAT)
                            && (MIReal[i].Data.NetCmd != NET_WAIT_DOWNLOAD_PRO))) && ((MIReal[i].Data.NetStatus == NET_NOT_EXECUTED) || (MIReal[i].Data.NetStatus == LOCAL_NOT_EXECUTED)))
                {
                    PortNO = i;
                    CurNetCmd = MIReal[i].Data.NetCmd;
                    CurRecSendLostPackageState = 0;
                    TotalPackageNum = 0;
                    UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
                    return false;
                }
            }
        }
    }

    if(UsartNrf3_SendLoop_ReplyOK() == true)
    {
        if((Uart_CurSendMainCmd != 0) && (Uart_CurSendMainCmd != CHANGE_MOD_2M_250K) && (Uart_CurSendMainCmd != REQ_RF_RVERSISON))
        {
            if(CurAlarmState == HasNewAlarmRecord)
            {
                CurNetCmd = NET_ALARM_DATA;
                UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
                return true;
            }
            else if(CurAlarmState == HasNewWaveRecord)
            {
                CurNetCmd = NET_RECORD_DATA;
                UsartNrf3_SendLoop_SetTaskTimeOut(false);//设置任务超时时间
                return true;
            }
            else if(CurAlarmState == AlarmInforUpdate_App)
            {
#ifdef DEBUG0610
                printf("AlarmInforUpdate_Server -- 3712\n\r");
#endif
                return UsartNrf3_AppStateSwitch(1, 1);
            }
            else if(CurAlarmState == AlarmInforUpdate_Server)
            {
                return UsartNrf3_AlarmInformationSearch();
            }
            else
            {
                AppLayerPollCnt = 0;
                TransLayerPollCnt = 0;
                return UsartNrf3_PollRealTimeWithVersion();
            }
        }
        else
        {
            return true;    //下个组件
        }
    }
    else
    {
        if((((ProtocalLayer_Poll == ToTransLayer_Send) || (ProtocalLayer_Poll == ToTransLayer_Process)) && (UsartNrf3_Send_One_MultiPackage((u8 *)&TransLayerPollCnt)))
                || (ProtocalLayer_Poll == ToAppLayer))
        {
            ProtocalLayer_Poll = InitLayer;
            TransLayerPollCnt = 0;

            if((Uart_CurrentReplyState == 0x10) || (Uart_CurrentReplyState == 0x20) || (Uart_CurrentReplyState == (0x01 << 6)))
            {
                AppLayerPollCnt++;
            }

            Uart_CurrentReplyState = 0;

            if(AppLayerPollCnt > APPLAYER_CYCLES)
            {
                AppLayerPollCnt = 0;
                CurAlarmState = InitState;
                return UsartNrf3_PollRealTimeWithVersion();
            }
        }
    }

    return true;
}
/***********************************************
** Function name: 三代协议轮询软件版本发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_PackPollVersionCmd(void)
{
    if(UsartNrf3_HasPollCurInverterPort())
    {
        if((TotalPackageNum <= 0) && (CurRecSendLostPackageState != 0))
        {
            Uart_SendBufferLen  = UsartNrf3_Send_PackPollSigleDataCommand((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, 0x7f);
        }
        else
        {
            if(CurRecSendLostPackageState != 0)
            {
                Uart_SendBufferLen  = UsartNrf3_Send_PackPollSigleDataCommand((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, CurRecSendLostPackageNO + 1);
            }
            else
            {
                Uart_SendBufferLen  = UsartNrf3_Send_PackPollMultiDataCommand((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id, (u8)(SubCmd), 0, (u8 *)Dtu3Detail.Property.LockNewPassword);
            }
        }

        ProtocalLayer_Poll = ToTransLayer_Send;
    }
    else
    {
        return;
    }
}
/***********************************************
** Function name: 三代协议设备控制应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_DevControl(u8 *pBuffer)
{
    Uart_CurrentReplyState = pBuffer[11];

    if(Uart_CurSendSubCmd != (((u16)pBuffer[14] << 8) | pBuffer[13]))
    {
        return;
    }

    switch(Uart_CurrentReplyState)
    {
        case 0: //命令执行成功

            //dong 2020-06-15  并网保护文件自检命令单独处理
            if(Uart_CurSendSubCmd ==  Type_SelfInspection)
            {
                //自检状态应答不当做网络命令处理
                MIReal[PortNO].Data.NetCmd = NET_SELF_STAUS;
                MIReal[PortNO].Data.NetStatus = NET_NOCMD;
                SelfCheckStateFlg = 1;
            }
            else
            {
                MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
                ProtocalLayer_Cmd = InitLayer;
                Index_Para = 0;
                CurNetCmd = NET_INIT;
            }

            break;

        case 3:  //eeprom读写错误
            Index_Para = 0;
            break;

        case 4:  //不支持该命令
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
            ProtocalLayer_Cmd = InitLayer;
            Index_Para = 0;
            CurNetCmd = NET_INIT;
            break;

        case 7:  //配置参数长度异常
            Index_Para = 0;
            break;

        default:
            break;
    }
}
/***********************************************
** Function name: 三代协议微逆参数设置应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_ParaSet(u8 *pBuffer)
{
    Uart_CurrentReplyState = pBuffer[11];

    if(Uart_CurSendSubCmd != (((u16)pBuffer[14] << 8) | pBuffer[13]))
    {
        return;
    }

    switch(Uart_CurrentReplyState)
    {
        case 0: //命令执行成功
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_COMPLETED;
            ProtocalLayer_Cmd = InitLayer;
            Index_Para = 0;

            if((CurNetCmd == NET_SET_PASSWORD) || (CurNetCmd == NET_CANCEL_GUARD))
            {
                memcpy((u8 *)Dtu3Detail.Property.LockNewPassword, (u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.PWN, 4);
                memcpy((u8 *)Dtu3Detail.Property.Lock_Time, (u8 *)MIMajor[PortNO].Property.Pass.PassWordSet.ATTime, 2);
                System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
            }

            CurNetCmd = NET_INIT;
            break;

        case 3:  //eeprom读写错误
            Index_Para = 0;
            break;

        case 4:  //不支持该命令
            MIReal[PortNO].Data.NetStatus = NET_EXECUTION_FAILURE;
            CurNetCmd = NET_INIT;
            Index_Para = 0;
            break;

        case 6:  //参数错误
            Index_Para = 0;
            break;

        case 7: //配置参数长度异常
            Index_Para = 0;
            break;

        default:
            break;
    }
}
/***********************************************
** Function name: 三代协议微逆程序更新传输层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_PackUpdateMiProgram(u8 *target_adr, u8 *rout_adr)
{
    if(RightHexFile == true)
    {
        if(CurSendRecLostPackageNO >= CurSendRecLastPackageNO)
        {
            if(CurRowTotalLen % 16 == 0)
            {
                Uart_SendBufferLen = UsartNrf_Send_MiProgram_SigleFrame(target_adr, rout_adr, CurRowData_Pro + CurSendRecLostPackageNO * 16, 0x80 | (CurSendRecLostPackageNO + 1), 2);
            }
            else
            {
                Uart_SendBufferLen = UsartNrf_Send_MiProgram_SigleFrame(target_adr, rout_adr, CurRowData_Pro + CurSendRecLostPackageNO * 16, 0x80 | (CurSendRecLostPackageNO + 1), CurRowTotalLen % 16);
            }
        }
        else
        {
            Uart_SendBufferLen = UsartNrf_Send_MiProgram_SigleFrame(target_adr, rout_adr, CurRowData_Pro + CurSendRecLostPackageNO * 16, 0x00 | (CurSendRecLostPackageNO + 1), 16);
        }
    }
}
/***********************************************
** Function name: 三代协议微逆程序更新传输应用层发送
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Send_DownLoadPro(void)
{
    if((ProtocalLayer_Cmd == InitLayer) || (ProtocalLayer_Cmd == ToAppLayer))
    {
        if(UsartNrf_HasSetCurrentInverterOk())
        {
            UsartNrf_Send_PackUpdateMiProgram((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);
        }
    }
    else
    {
        if(UsartNrf3_HasSetCurrentInverterOk())
        {
            UsartNrf3_Send_PackUpdateMiProgram((u8 *)MIMajor[PortNO].Property.Id, (u8 *)MIMajor[PortNO].Property.Id);
        }
    }
}
/***********************************************
** Function name: 三代协议应用层回执处理
** Descriptions:
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*************************************************/
void UsartNrf3_Process_Pro(u8 *pBuffer)
{
    switch(pBuffer[1])
    {
        //dong 2020-06-23
        case 0x86:
            if((AppState == AllMiLossRate) || (AppState == ManuSigleLossRate))
            {
                //dong
                if(RecieveDataCnt < 100)
                {
                    RecieveDataCnt++;
#ifdef DEBUGSIGNAL
                    printf("-RRR:T:%d-D:%d-S:%d-R:%d\n\r", LocalTime, PortNO, AppPollCnt, RecieveDataCnt);
#endif
                }
            }

            break;

        case ANSWER_REQ_ARW_DAT_ALL:
            UsartNrf3_Process_DevInform(pBuffer);
            break;

        case ANSWER_DOWN_DAT:
            UsartNrf3_Process_DownLoadDat(pBuffer);
            break;

        case ANSWER_DOWN_PRO:
            UsartNrf3_Process_DownLoadPro(pBuffer);
            break;

        case ANSWER_DEVCONTROL_ALL:
            UsartNrf3_Process_DevControl(pBuffer);
            break;

        case ANSWER_PARASET_ALL:
            UsartNrf3_Process_ParaSet(pBuffer);
            break;

        //        case ANSWER_EXCEPTION_ONE_MULTI:
        //            UsartNrf3_Process_One_MultiPackage(pBuffer);
        //            break;

        case ANSWER_EXCEPTION_MULTI_ONE:
            UsartNrf3_Process_Multi_OnePackage(pBuffer);
            break;

        default:
            break;
    }
}
