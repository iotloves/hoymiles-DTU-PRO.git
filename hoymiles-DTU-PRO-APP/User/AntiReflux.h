#ifndef __ANTIBACKFLOW_H
#define __ANTIBACKFLOW_H
#include "stm32f4xx.h"
#define MY_MIN_POWER_LIMIT              350
#define MY_MIN_POWER_LIMIT_THREE        70
#define MY_SHUTDOWN_VOLTAGE             200
#define MY_MAX_PORT_LIMIT               3750
#define MeterMaxNum                     10
#define DIFFERENCE_MARGIN               100
#define SUBCONTRACT_MAX_NUM             25
#define OFFLINE_TIMES                   1
#define CTRL_DIFF                       30
#define GRID_SIDE_VOLTAGE_LIMIT         600
#define CONTROL_INTERVAL_TIME           1000
#define FULL_CONTROL_VALUE              1000

#define ANTI_BACKFLOW_250W(x)           ((MIMajor[x].Property.Port == MI_250W)||\
        (MIMajor[x].Property.Port == HM_250W))
#define ANTI_BACKFLOW_500W(x)           (((MIMajor[x].Property.Port == MI_500W_A) && \
        (MIMajor[x + 1].Property.Port == MI_500W_B)) || \
        ((MIMajor[x].Property.Port == HM_500W_A) && \
         (MIMajor[x + 1].Property.Port == HM_500W_B)))
#define ANTI_BACKFLOW_1000W(x)          (((MIMajor[x].Property.Port == MI_1000W_A) && \
        (MIMajor[x + 1].Property.Port == MI_1000W_B) && \
        (MIMajor[x + 2].Property.Port == MI_1000W_C) && \
        (MIMajor[x + 3].Property.Port == MI_1000W_D)) || \
        ((MIMajor[x].Property.Port == Pro_A) && \
         (MIMajor[x + 1].Property.Port == Pro_B) && \
         (MIMajor[x + 2].Property.Port == Pro_C) && \
         (MIMajor[x + 3].Property.Port == Pro_D)) || \
        ((MIMajor[x].Property.Port == HM_1000W_A) && \
         (MIMajor[x + 1].Property.Port == HM_1000W_B) && \
         (MIMajor[x + 2].Property.Port == HM_1000W_C) && \
         (MIMajor[x + 3].Property.Port == HM_1000W_D)))

typedef union
{
    struct
    {
        vu8 Pre_Id[2];
        vu8 Id[4];
        vu8 place;
    } Property;
    vu8 PropertyMsg[7];
} MeterMajor;

enum Meter_Place
{
    //电网侧
    Meter_Grid_Side = 0x00,
    //负载侧
    Meter_Load_Side = 0x01,
    //光伏侧
    Meter_Photovoltaic_Side = 0x02,
};

enum Meter_Vendor
{
    //正泰
    Chint       = 0xC0,
    //WattNode
    WattNode    = 0xC1,
};
typedef struct
{
    vu8 sn;
    vu8 phase;
    vs32 power_total;
    vs32 power_a;               //防逆流控制微逆 功率  w 不是用百分比去做
    vs32 power_b;
    vs32 power_c;
    vs32 factor;
    vs32 positive_energy;
    vs32 positive_energy_a;
    vs32 positive_energy_b;
    vs32 positive_energy_c;
    vs32 Reverse_energy;
    vs32 Reverse_energy_a;
    vs32 Reverse_energy_b;
    vs32 Reverse_energy_c;
    vs32 fault;
    vu16 CT;
    vu8 Send_CNT;
} MeterInfomation;

void Meter_Data_Write(u8 Vendor, u8 MeterAddr, u16 adr, u16 date);
void process_req_meter_data(void);
void ANTI_REFLUX3_process(void);
void Meter_Data_Read(u8 Vendor, u8 MeterAddr, u16 adr, u8 len);
void Meter_Report_Slave(u8 Vendor, u8 MeterAddr);
void Meter_Write_CT(void);
void LWHV_Process(void);
void Power_Sweep_Control(void);
void Relative_Power_Sweep_Control(void);
#endif

