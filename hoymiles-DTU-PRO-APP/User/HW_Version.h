#ifndef __HW_VERSION_H
#define __HW_VERSION_H
#include "stm32f4xx.h"

//硬件版本地址
#define BasicVersion   0x91

#define DTU_Pro_MI                  (0x11 | 0x80)
#define DTU_Pro_433                 (0x12 | 0x80)
#define DTU_Pro_gprs                (0x13 | 0x80)
#define DTU_Pro_433_gprs            (0x14 | 0x80)
#define DTU_Pro_zeroexport          (0x15 | 0x80)
#define DTU_Pro_gprs_zeroexport     (0x16 | 0x80)

#define ADAPTIVE_TIME_MAX           1000*60*5
#define SW_VER                      0x0101//0xF1F1//  
enum
{
    //gprs+wifi版本
    HW_WIFI_GPRS    = 0,
    //单wifi版本
    HW_WIFI         = 1,
    //默认值
    HW_DEFAULT      = 9
};

//和服务器通讯的方式
enum mode_to_server
{
    //gprs
    GPRS_MODE       = 0,
    //wifi
    WIFI_MODE       = 1,
    //网线
    CABLE_MODE      = 2,
    //自适应
    Adaptive_MODE   = 3,
    //本地串口
    local_usart
};
enum Network_Status
{
    Net_Connect     = 0,
    Net_Server      = 1,
    Net_Data        = 2,
    Net_TimeOut     = 3,
};

extern vu8 Netmode_Used;
void Version_Init(void);
u8 Network_Mode_Check(u8 Check_Mode, u8 Network_Mode);
void set_rule_id(void);
void Initialization(u8 mode);
void Internet_Adaptive(void);
#endif
