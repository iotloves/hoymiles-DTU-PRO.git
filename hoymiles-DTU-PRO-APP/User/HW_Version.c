#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#ifndef Bootloader
#include "HW_Version.h"
#include "usart_nrf.h"
#include "Memory.h"
#include "wifi.h"
#include "gprs.h"
#include "netconf.h"
#include "Memory_Tools.h"

#define NETWORK_WAIT_TIME       5*60*1000

vu8 LargeVersion = 0;
vu8 Netmode_Used = 2;
vu32 AdaptiveTime = 0;
vu8 AdaptiveState = 0;
extern volatile DtuMajor Dtu3Major;
extern volatile DtuDetail Dtu3Detail;
extern volatile SystemCfgNew system_cfg;
//网络标志位  1未连接网络  2 连接网络
extern vu8 connect_net;
//连接服务器标志位 1未连接服务器 2 连接服务器
extern vu8 connect_server;
//有服务器数据
extern vu8 server_data;
extern vu8 Wifi_state;
extern vu8 GPRS_state;
extern vu8 ETH_State;
extern char pre_ssid[];
extern volatile char DTUName[];

vu8 Network_Change = 0;
vu32 Network_Change_Wait = 0;

//网络协议版本判断  0为三代协议 1为二代协议
vu8 NetProVer = 0;
/***********************************************
** Function name:
** Descriptions:  硬件判断GPIO初始化
** input parameters:
** output parameters:
** Returned value:
*************************************************/
void HW_GPIO_Config(void)
{
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
}

/***********************************************
** Function name:
** Descriptions:  硬件版本判断
** input parameters:
** output parameters:
** Returned value:
*************************************************/
u8 HW_VER(void)
{
    //DTU型号 默认值
    vu8 HW_Ver = 0;
    HW_Ver |=  GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);
    HW_Ver |= (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15) << 1);
    HW_Ver |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_0)  << 2);
    HW_Ver |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_1)  << 3);
    return HW_Ver;
}

u8 Network_Mode_Check(u8 Check_Mode, u8 Network_Mode)
{
    if(Check_Mode == 0)
    {
        if((LargeVersion == DTU_Pro_gprs) || (LargeVersion == DTU_Pro_433_gprs) || (LargeVersion == DTU_Pro_gprs_zeroexport))
        {
            if((system_cfg.Property.netmode_select != GPRS_MODE) && (system_cfg.Property.netmode_select != CABLE_MODE) && (system_cfg.Property.netmode_select != Adaptive_MODE))
            {
                system_cfg.Property.netmode_select = GPRS_MODE;
            }

            if((Dtu3Major.Property.netmode_select != GPRS_MODE) && (Dtu3Major.Property.netmode_select != CABLE_MODE) && (Dtu3Major.Property.netmode_select != Adaptive_MODE))
            {
                Dtu3Major.Property.netmode_select = GPRS_MODE;
            }
        }
        else if((LargeVersion == DTU_Pro_MI) || (LargeVersion == DTU_Pro_433) || (LargeVersion == DTU_Pro_zeroexport))
        {
            if((system_cfg.Property.netmode_select != WIFI_MODE) && (system_cfg.Property.netmode_select != CABLE_MODE) && (system_cfg.Property.netmode_select != Adaptive_MODE))
            {
                system_cfg.Property.netmode_select = WIFI_MODE;
            }

            if((Dtu3Major.Property.netmode_select != WIFI_MODE) && (Dtu3Major.Property.netmode_select != CABLE_MODE) && (Dtu3Major.Property.netmode_select != Adaptive_MODE))
            {
                Dtu3Major.Property.netmode_select = WIFI_MODE;
            }
        }
    }
    else
    {
        if((LargeVersion == DTU_Pro_gprs) || (LargeVersion == DTU_Pro_433_gprs) || (LargeVersion == DTU_Pro_gprs_zeroexport))
        {
            if((Network_Mode != GPRS_MODE) && (Network_Mode != CABLE_MODE))
            {
                return 0;
            }
        }
        else if((LargeVersion == DTU_Pro_MI) || (LargeVersion == DTU_Pro_433) || (LargeVersion == DTU_Pro_zeroexport))
        {
            if((Network_Mode != WIFI_MODE) && (Network_Mode != CABLE_MODE))
            {
                return 0;
            }
        }
    }

    return 1;
}

void Version_Init(void)
{
    vu8 SmallVersion = 0;
    vu16 Sw_Ver = SW_VER;
    HW_GPIO_Config();
    LargeVersion = BasicVersion + HW_VER();
    System_Cfg_Read((SystemCfgNew *)&system_cfg);
    SmallVersion = system_cfg.Property.small_version;

    if((SmallVersion == 0x00) || (SmallVersion == 0xFF))
    {
        SmallVersion = 0x01;
    }

    Dtu3Major.Property.DtuHw_Ver[0] = LargeVersion;
    Dtu3Major.Property.DtuHw_Ver[1] = SmallVersion;
    Dtu3Major.Property.DtuSw_Ver[0] = (u8)(Sw_Ver / 256);
    Dtu3Major.Property.DtuSw_Ver[1] = (u8)(Sw_Ver % 256);
    Network_Mode_Check(0, 0);
    memset((char *)DTUName, 0, sizeof(DTUName[0]));
    strncpy((char *)&DTUName[0], pre_ssid, 5);
    hex_ascii((u8 *)Dtu3Major.Property.Pre_Id, (u8 *)&DTUName[5], 2);
    hex_ascii((u8 *)Dtu3Major.Property.Id, (u8 *)&DTUName[9], 4);
}

void set_rule_id(void)
{
    if((Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_gprs) || (Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_433_gprs) || (Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_gprs_zeroexport))
    {
        Dtu3Major.Property.rule_id = 17;
    }
    else if((Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_MI) || (Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_433) || (Dtu3Major.Property.DtuHw_Ver[0] == DTU_Pro_zeroexport))
    {
        Dtu3Major.Property.rule_id = 21;
    }
}

void Initialization(u8 mode)
{
    memset((u8 *)system_cfg.Data, 0, sizeof(SystemCfgNew));
    System_Cfg_Read((SystemCfgNew *)&system_cfg);

    if(mode == 1)
    {
        Dtu3Major.Property.netmode_select = system_cfg.Property.netmode_select;
        Network_Mode_Check(0, 0);
        set_rule_id();
    }

    if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id, (char *)system_cfg.Property.ServerDomainName, (u16)strlen((char *)system_cfg.Property.ServerDomainName), system_cfg.Property.ServerPort) == 0)
    {
        memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, (u8 *)system_cfg.Property.ServerDomainName, sizeof(system_cfg.Property.ServerDomainName));
        Dtu3Major.Property.ServerPort = system_cfg.Property.ServerPort;
        memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)system_cfg.Property.APN, sizeof(system_cfg.Property.APN));
    }

    if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id, (char *)Dtu3Major.Property.ServerDomainName, (u16)strlen((char *)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
    {
        Reset_Domain_Config((u8 *)Dtu3Major.Property.Pre_Id);
    }

    if((Dtu3Major.Property.rule_id == 0) || (Dtu3Major.Property.rule_id == 0xFF))
    {
        set_rule_id();
    }

    //System_DtuMajor_Write(&Dtu3Major);
    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
}

void Internet_Adaptive(void)
{
    static vu8 start = 0;

    if(start == 0)
    {
        start = 1;
        AdaptiveTime = LocalTime;
    }

    if(Dtu3Major.Property.netmode_select == Adaptive_MODE)
    {
        switch(AdaptiveState)
        {
            case Net_Connect:
                if(connect_net == 1)
                {
                    AdaptiveState = Net_Server;
                    AdaptiveTime = LocalTime;
                }
                else if((LocalTime - AdaptiveTime) > ADAPTIVE_TIME_MAX)
                {
                    AdaptiveState = Net_TimeOut;
                    AdaptiveTime = LocalTime;
                }

                break;

            case Net_Server:
                if(connect_server == 1)
                {
                    AdaptiveState = Net_Data;
                    AdaptiveTime = LocalTime;
                }
                else if((LocalTime - AdaptiveTime) > ADAPTIVE_TIME_MAX)
                {
                    AdaptiveState = Net_TimeOut;
                    AdaptiveTime = LocalTime;
                }

                break;

            case Net_Data:
                if((connect_net == 0) && (connect_server == 0) && (server_data == 0))
                {
                    AdaptiveState = Net_Connect;
                    AdaptiveTime = LocalTime;
                }

                break;

            case Net_TimeOut:
                Netmode_Used = (Netmode_Used + 1) % 3;

                if((LargeVersion == DTU_Pro_gprs) || (LargeVersion == DTU_Pro_433_gprs) || (LargeVersion == DTU_Pro_gprs_zeroexport))
                {
                    if(Netmode_Used == WIFI_MODE)
                    {
                        Netmode_Used = (Netmode_Used + 1) % 3;
                    }
                }
                else if((LargeVersion == DTU_Pro_MI) || (LargeVersion == DTU_Pro_433) || (LargeVersion == DTU_Pro_zeroexport))
                {
                    if(Netmode_Used == GPRS_MODE)
                    {
                        Netmode_Used = (Netmode_Used + 1) % 3;
                    }
                }

                Network_Mode_Check(0, 0);
                AdaptiveTime = LocalTime;

                if(Netmode_Used == GPRS_MODE)
                {
                    GPRS_state = GS_CONTROL_POFF;
                }
                else if(Netmode_Used == WIFI_MODE)
                {
                    Wifi_state = WIFI_POWER_OFF;
                }
                else if(Netmode_Used == CABLE_MODE)
                {
                }

                AdaptiveState = Net_Connect;
                break;

            default:
                break;
        }
    }
    else
    {
        if((Netmode_Used != Dtu3Major.Property.netmode_select) &&
                ((Network_Change == 0) || ((LocalTime - Network_Change_Wait) > NETWORK_WAIT_TIME)))
        {
            Network_Change = 0;
            Network_Change_Wait = 0;
            Netmode_Used = Dtu3Major.Property.netmode_select;
            Network_Mode_Check(0, 0);
        }
    }
}
#endif