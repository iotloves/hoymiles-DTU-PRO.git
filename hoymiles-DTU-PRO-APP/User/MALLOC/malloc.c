#include "malloc.h"
#include "string.h"

//内存池(64字节对齐)
//内存管理表
__attribute__((aligned(4))) vu8 membase[MEM_MAX_SIZE]__attribute__((section(".ARM.__at_0x20000000")));                                                  //内部SRAM内存池

//内部SRAM内存池MAP
vu16 memmapbase[MEM_ALLOC_TABLE_SIZE];


//内存管理参数
//内存表大小
const u32 memtblsize = MEM_ALLOC_TABLE_SIZE;
//内存分块大小
const u32 memblksize = MEM_BLOCK_SIZE;
//内存总大小
const u32 memsize = MEM_MAX_SIZE;


//内存管理控制器
struct _m_mallco_dev mallco_dev =
{
    //内存初始化
    my_mem_init,
    //内存使用率
    my_mem_perused,
    //内存池
    (u8 *)membase,
    //内存管理状态表
    (u16 *)memmapbase,
    //内存管理未就绪
    0,

};
/***********************************************
** Function name:
** Descriptions:
** input parameters:
** output parameters:
** Returned value:
*************************************************/
//复制内存
//*des:目的地址
//*src:源地址
//n:需要复制的内存长度(字节为单位)
void mymemcpy(void *des, void *src, u32 n)
{
    u8 *xdes = des;
    u8 *xsrc = src;

    while(n--)
    {
        *xdes++ = *xsrc++;
    }
}
//设置内存
//*s:内存首地址
//c :要设置的值
//count:需要设置的内存大小(字节为单位)
void mymemset(void *s, u8 c, u32 count)
{
    vu8 *xs = s;

    while(count--)
    {
        *xs++ = c;
    }
}
//内存管理初始化
//memx:所属内存块
void my_mem_init(void)
{
    //内存状态表数据清零
    mymemset(mallco_dev.memmap, 0, memtblsize * 2);
    //内存池所有数据清零
    mymemset(mallco_dev.membase, 0, memsize);
    //内存管理初始化OK
    mallco_dev.memrdy = 1;
}
//获取内存使用率
//memx:所属内存块
//返回值:使用率(0~100)
u8 my_mem_perused(void)
{
    vu32 used = 0;
    vu32 i = 0;

    for(i = 0; i < memtblsize; i++)
    {
        if(mallco_dev.memmap[i])
        {
            used++;
        }
    }

    return (u8)((used * 100) / (memtblsize));
}
//内存分配(内部调用)
//memx:所属内存块
//size:要分配的内存大小(字节)
//返回值:0XFFFFFFFF,代表错误;其他,内存偏移地址
u32 my_mem_malloc(u32 size)
{
    volatile signed long offset = 0;
    //需要的内存块数
    vu32 nmemb = 0;
    //连续空内存块数
    vu32 cmemb = 0;
    vu32 i = 0;

    if(!mallco_dev.memrdy)
    {
        //未初始化,先执行初始化
        mallco_dev.init();
    }

    if(size == 0)
    {
        //不需要分配
        return 0XFFFFFFFF;
    }

    //获取需要分配的连续内存块数
    nmemb = size / memblksize;

    if(size % memblksize)
    {
        nmemb++;
    }

    //搜索整个内存控制区
    for(offset = memtblsize - 1; offset >= 0; offset--)
    {
        if(!mallco_dev.memmap[offset])
        {
            //连续空内存块数增加
            cmemb++;
        }
        else
        {
            //连续内存块清零
            cmemb = 0;
        }

        //找到了连续nmemb个空内存块
        if(cmemb == nmemb)
        {
            //标注内存块非空
            for(i = 0; i < nmemb; i++)
            {
                mallco_dev.memmap[(unsigned long)offset + i] = (uint16_t)nmemb;
            }

            //返回偏移地址
            return ((unsigned long)offset * memblksize);
        }
    }

    //未找到符合分配条件的内存块
    return 0XFFFFFFFF;
}
//释放内存(内部调用)
//memx:所属内存块
//offset:内存地址偏移
//返回值:0,释放成功;1,释放失败;
u8 my_mem_free(u32 offset)
{
    vu16 i;

    //未初始化,先执行初始化
    if(!mallco_dev.memrdy)
    {
        mallco_dev.init();
        //未初始化
        return 1;
    }

    //偏移在内存池内.
    if(offset < memsize)
    {
        //偏移所在内存块号码
        vu32 index = offset / memblksize;
        //内存块数量
        vu32 nmemb = mallco_dev.memmap[index];

        //内存块清零
        for(i = 0; i < nmemb; i++)
        {
            mallco_dev.memmap[index + i] = 0;
        }

        return 0;
    }
    else
    {
        //偏移超区了.
        return 2;
    }
}
//释放内存(外部调用)
//memx:所属内存块
//ptr:内存首地址
void myfree(void *ptr)
{
    vu32 offset;

    if(ptr == NULL)
    {
        //地址为0.
        return;
    }

    offset = (u32)ptr - (u32)mallco_dev.membase;
    //释放内存
    my_mem_free(offset);
}
//分配内存(外部调用)
//memx:所属内存块
//size:内存大小(字节)
//返回值:分配到的内存首地址.
void *mymalloc(u32 size)
{
    vu32 offset;
    offset = my_mem_malloc(size + MemoryInterval);

    if(offset == 0XFFFFFFFF)
    {
        return NULL;
    }
    else
    {
        memset((void *)((u32)mallco_dev.membase + offset), 0, size + MemoryInterval);
        return (void *)((u32)mallco_dev.membase + offset);
    }
}
//重新分配内存(外部调用)
//memx:所属内存块
//*ptr:旧内存首地址
//size:要分配的内存大小(字节)
//返回值:新分配到的内存首地址.
void *myrealloc(void *ptr, u32 size)
{
    vu32 offset = 0;
    offset = my_mem_malloc(size);

    if(offset == 0XFFFFFFFF)
    {
        return NULL;
    }
    else
    {
        //拷贝旧内存内容到新内存
        mymemcpy((void *)((u32)mallco_dev.membase + offset), ptr, size);
        //释放旧内存
        myfree(ptr);
        //返回新内存首地址
        return (void *)((u32)mallco_dev.membase + offset);
    }
}
