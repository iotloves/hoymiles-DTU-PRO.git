#include "AntiReflux.h"
#include "usart.h"
#include "string.h"
#include "crc16.h"
#include "SysTick.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#include "stdlib.h"
#include "TimeProcessing.h"
#define SINGLE_PHASE_LIMIT          30*1000*10
#define THREE_PHASE_LIMIT           200*1000*10
//a=abs(-10); 绝对值
//电表轮询时间
#define MeterPollingTime            15000
//绝对值控制
#define Absolute_Value_Control      0
//差值控制
#define DifferenceControl           1

#define PH_NULL        0
#define PH_A           1
#define PH_B           2
#define PH_C           3
//电表类型
#define Chint_Read_Kind         0x0003
/*三相防逆流*/
//合相有功功率
#define Chint_Read_Adr_PT       0x2012
//A相有功功率，单位 k W
#define Chint_Read_Adr_PA       0x2014
//B相有功功率，单位 k W
#define Chint_Read_Adr_PB       0x2016
//C相有功功率，单位 k W
#define Chint_Read_Adr_PC       0x2018
//正向有功电量
#define Chint_Read_Adr_EP       0x101E
//A 相正向有功总电能
#define Chint_Read_Adr_EPA      0x1020
//B 相正向有功总电能
#define Chint_Read_Adr_EPB      0x1022
//C 相正向有功总电能
#define Chint_Read_Adr_EPC      0x1024
//反向有功电量
#define Chint_Read_Adr_EN       0x1028
//A 相反向有功总电能
#define Chint_Read_Adr_ENA      0x102A
//B 相反向有功总电能
#define Chint_Read_Adr_ENB      0x102C
//C 相反向有功总电能
#define Chint_Read_Adr_ENC      0x102E

/*单相防逆流*/
#define Chint_Read_Adr_PT1      0x2004
//正向有功电量
#define Chint_Read_Adr_EP1      0x4000
//反向有功电量
#define Chint_Read_Adr_EN1      0x400A

/*三相版本号*/
#define Chint_Read_REV          0x0000
/*电流互感器倍率*/
#define Chint_Read_CtAmps       0x0006
/*单相软件版本号*/
#define Chint_Read_REV1         0x0001

/*三相防逆流*/
//合相有功功率
#define WattNode_Read_Adr_PT   (1009 - 1)
//A相有功功率，单位 k W
#define WattNode_Read_Adr_PA   (1011 - 1)
//B相有功功率，单位 k W
#define WattNode_Read_Adr_PB   (1013 - 1)
//C相有功功率，单位 k W
#define WattNode_Read_Adr_PC   (1015 - 1)

//正向有功电量
#define WattNode_Read_Adr_EP   (1003 - 1)
//A 相正向有功总电能
#define WattNode_Read_Adr_EPA  (1107 - 1)
//B 相正向有功总电能
#define WattNode_Read_Adr_EPB  (1109 - 1)
//C 相正向有功总电能
#define WattNode_Read_Adr_EPC  (1111 - 1)

//反向有功电量
#define WattNode_Read_Adr_EN   (1113 - 1)
//A 相反向有功总电能
#define WattNode_Read_Adr_ENA  (1117 - 1)
//B 相反向有功总电能
#define WattNode_Read_Adr_ENB  (1119 - 1)
//C 相反向有功总电能
#define WattNode_Read_Adr_ENC  (1121 - 1)

#define WattNode_Read_CtAmps   (1603 - 1)
//差值控制电流方向
vs8 current_direction_a = 1;
vs8 current_direction_b = 1;
vs8 current_direction_c = 1;

vu8 EN_Anti_Reflux = 0;
vu8 EN_Anti_Reflux_w = 0;
vu8 getdata = 0;
vu8 rev_buf[10] = {0};
vu8 reflux_adr[3] = {0x0b, 0xd3, 0xfa};
vu8 anti_reflux_drop_times = 0;
//电表连接状态
vu8 Meter_Link = 0;
vu8 F_id_in_phase = 0;
vu8 dtu_device_nub = 0;
//本机发送数据标志
vu8 Locally_Send = 0;
vu8 Usart_485_Rec_owner = 0;
vu8 Anti_Reflux_Kind = 0;
vu8 Usart_485_Rec_Data = 0;
vu8 Uart_Rec_mybuf[Usart_485_Len];
//竞主时 用ID的位置
vu8 cmp_owner_nub = 0;
//主机状态0：从机1：主机
vu8 Dtu_Host_Status = 0;
//发送轮训  功率    电量
vu8 Read_meter_state = 0;
//电表重复读取次数  后 读另外一种
vu8 Read_meter_Repeat = 0;
//错误的时候
vu8 Anti_Reflux_Drop_Times_3w;
//电表数量
vu8 MeterLinkNum = 0;
vu8 ControlMode  = 0;
vu8 ControlPlace = 0;
vu8 ControlPhase = 0;
//广发标志
vu8 BatchDelivery = 0;
vu8 MeterNetwork = 1;
vu8 orientation_calibration = 0;
//电表CT设置标志位
vu8 CT_set_flag = 0;

volatile MeterMajor Meter[MeterMaxNum];
volatile MeterInfomation MeterInfo[MeterMaxNum];

vu16 Meter_send_time = 0;
vu16 sys_device_nub = 0;
vu16 p_port = 0;
vu32 Anti_Reflux_Time = 0;
vs32 P_max_reflux = 0;
vs32 last_control_power_a = 0;
vs32 last_control_power_b = 0;
vs32 last_control_power_c = 0;
vs32 consumption_power_total = 0;
//防逆流控制微逆 功率  w 不是用百分比去做
vs32 consumption_power_a = 0;
vs32 consumption_power_b = 0;
vs32 consumption_power_c = 0;
vs32 consumption_power_a_last = 0;
vs32 consumption_power_b_last = 0;
vs32 consumption_power_c_last = 0;
vu32 TFM_rec_send_delay = 0;
vu32 Usart_485_Rec_Time = 0;
//电表 正向 电量
vu32 consumption_forward_energy = 0;
//查询数据 时间
vu32 Anti_Reflux_Meter_Read_Time = 0;
//竞主时间
vu32 Anti_Reflux_Actual_Owner_Time = 0;
//主机多少时间没数据后  释放主机标志
vu32 Anti_Reflux_Owner_Time_Out = 0;
vu32 Anti_Reflux_3w_Time = 0;
vu32 Anti_LWHV_Time = 0;
vu8 phase_balance_switch_last = 0;
vu8 f_phase = 0;
vu8 nub_power_pha = 0;
vu8 nub_power_phb = 0;
vu8 nub_power_phc = 0;
vu8 three_phase_control_times = 0;
vu32 Cntrl_Pa = 0;
vu32 Cntrl_Pb = 0;
vu32 Cntrl_Pc = 0;
vu32 Real_Pa = 0;
vu32 Real_Pb = 0;
vu32 Real_Pc = 0;
vu16 my_tolerance_between_phases = 0;
extern vu16 PortNO;
//逆变器主要信息
extern volatile InverterMajor MIMajor[PORT_LEN];
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
//微型逆变器详细信息
extern volatile InverterDetail MIDetail;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
extern vu16 Usart_485_Read_Cnt;
extern RTC_TimeTypeDef myRTC_TimeStructure;
extern RTC_DateTypeDef myRTC_DateStruct;
extern vu8 Command;
extern vu8 downfile_module;
//发送电表数据
void Meter_Data_Write(u8 Vendor, u8 MeterAddr, u16 adr, u16 date)
{
    vu16 temp_crc = 0;
    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);

    switch(Vendor)
    {
        //正泰
        case Chint:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x10;
                /*高地址*/
                Usart_485_Tx_Buf[2] = (u8)(adr >> 8);
                /*低地址*/
                Usart_485_Tx_Buf[3] = (u8)(adr & 0x00FF);
                //寄存器数量
                /*高地址*/
                Usart_485_Tx_Buf[4] = 0x00;
                /*低地址*/
                Usart_485_Tx_Buf[5] = 0x01;
                //写入字节数
                /*低地址*/
                Usart_485_Tx_Buf[6] = 0x02;
                Usart_485_Tx_Buf[7] = (u8)(date >> 8);
                Usart_485_Tx_Buf[8] = (u8)(date & 0x00FF);
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 9);
                Usart_485_Tx_Buf[9] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[10] = (u8)(temp_crc >> 8);
                Usart_Send(11);
            }
            break;

        //WattNode
        case WattNode:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x10;
                /*高地址*/
                Usart_485_Tx_Buf[2] = (u8)(adr >> 8);
                /*低地址*/
                Usart_485_Tx_Buf[3] = (u8)(adr & 0x00FF);
                //寄存器数量
                /*高地址*/
                Usart_485_Tx_Buf[4] = 0x00;
                /*低地址*/
                Usart_485_Tx_Buf[5] = 0x01;
                //写入字节数
                /*低地址*/
                Usart_485_Tx_Buf[6] = 0x02;
                Usart_485_Tx_Buf[7] = (u8)(date >> 8);
                Usart_485_Tx_Buf[8] = (u8)(date & 0x00FF);
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 9);
                Usart_485_Tx_Buf[9] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[10] = (u8)(temp_crc >> 8);
                Usart_Send(11);
            }
            break;
    }
}
//发送电表数据
void Meter_Data_Read(u8 Vendor, u8 MeterAddr, u16 adr, u8 len)
{
    vu16 temp_crc = 0;
    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);

    switch(Vendor)
    {
        //正泰
        case Chint:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x03;
                /*高地址*/
                Usart_485_Tx_Buf[2] = (u8)(adr >> 8);
                /*低地址*/
                Usart_485_Tx_Buf[3] = (u8)(adr & 0x00FF);
                /*len 高地址 为 0*/
                Usart_485_Tx_Buf[4] = 0x00;
                Usart_485_Tx_Buf[5] = len;
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 6);
                Usart_485_Tx_Buf[6] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[7] = (u8)(temp_crc >> 8);
                Usart_Send(8);
            }
            break;

        //WattNode
        case WattNode:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x03;
                /*高地址*/
                Usart_485_Tx_Buf[2] = (u8)(adr >> 8);
                /*低地址*/
                Usart_485_Tx_Buf[3] = (u8)(adr & 0x00FF);
                /*len 高地址 为 0*/
                Usart_485_Tx_Buf[4] = 0x00;
                Usart_485_Tx_Buf[5] = len;
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 6);
                Usart_485_Tx_Buf[6] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[7] = (u8)(temp_crc >> 8);
                Usart_Send(8);
            }
            break;
    }
}
//获取电表自报数据
void Meter_Report_Get(u8 Vendor, u8 MeterAddr)
{
    vu16 temp_crc = 0;
    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);

    switch(Vendor)
    {
        //正泰
        case Chint:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x11;
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 2);
                Usart_485_Tx_Buf[2] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[3] = (u8)(temp_crc >> 8);
                Usart_Send(8);
            }
            break;

        //WattNode
        case WattNode:
            {
                Usart_485_Tx_Buf[0] = MeterAddr;
                Usart_485_Tx_Buf[1] = 0x11;
                temp_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, 2);
                Usart_485_Tx_Buf[2] = (u8)(temp_crc & 0x00FF);
                Usart_485_Tx_Buf[3] = (u8)(temp_crc >> 8);
                Usart_Send(8);
            }
            break;
    }
}
void Meter_Write_CT(void)
{
    static vu8 CT_place = 0;

    if(LocalTime > (Anti_Reflux_Meter_Read_Time + Meter_send_time))
    {
        Anti_Reflux_Meter_Read_Time = LocalTime;
        Locally_Send = 1;

        switch(Meter[CT_place].Property.Pre_Id[1])
        {
            //正泰
            case Chint:
                {
                    Meter_Data_Write(Chint, MeterInfo[CT_place].sn, Chint_Read_CtAmps, MeterInfo[CT_place].CT);
                }
                break;

            //WattNode
            case WattNode:
                {
                    Meter_Data_Write(WattNode, MeterInfo[CT_place].sn, WattNode_Read_CtAmps, MeterInfo[CT_place].CT);
                }
                break;
        }

        CT_place++;

        if(CT_place >= MeterLinkNum)
        {
            CT_place = 0;
            CT_set_flag = 0;
        }
    }
}


//竟主
void Send_Actual_Owner(void)
{
    memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
    Locally_Send = 1;
    Usart_485_Tx_Buf[0] = 0x5a;
    Usart_Send(1);
}
//电表值读取
void Meter_Value_Read(void)
{
    static vu8 Value_cnt = 0;

    //延迟1000us切换 485使能为 接收
    if((Usart_485_Tx_Cnt == 0) && (LocalTime - TFM_rec_send_delay > 20))
    {
        TFM_rec_send_delay = LocalTime;
        //使能接收
        Usart_485_Rece;
    }

    //共10s 采集一个命令 读取电表值
    if(LocalTime > (Anti_Reflux_Meter_Read_Time + Meter_send_time))
    {
        Anti_Reflux_Meter_Read_Time = LocalTime;
        //清除接收缓存
        memset((u8 *)Usart_485_Rx_Buf, 0, Usart_485_Len);
        Usart_485_Read_Cnt = 0;
        //未收到数据的重复发多少次后 发另外的命令
        Read_meter_Repeat ++;

        if(Read_meter_Repeat > 3)
        {
            Read_meter_Repeat = 0;
            Value_cnt ++;
            Value_cnt = Value_cnt % MeterLinkNum;
        }

        //发送 命令
        Locally_Send = 1;

        switch(Meter[Value_cnt].Property.Pre_Id[1])
        {
            //正泰
            case Chint:
                {
                    //Meter_Link = 0;
                    switch(Read_meter_state)
                    {
                        case 0:
                            Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Kind, 1);
                            break;

                        case 1:
                            if(MeterInfo[Value_cnt].phase == 3)
                            {
                                //读取电表  功率( 总--A--B--C )
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Adr_PT, 36);
                            }
                            else if(MeterInfo[Value_cnt].phase == 2)
                            {
                                //读取电表  功率( 总 )
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Adr_PT1, 12);
                            }
                            else
                            {
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Kind, 1);
                                Read_meter_state = 0;
                            }

                            break;

                        case 2:
                            if(MeterInfo[Value_cnt].phase == 3)
                            {
                                //读取总电量 ( 总 )
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Adr_EP, 32);
                            }
                            else if(MeterInfo[Value_cnt].phase == 2)
                            {
                                //读取总电量 ( 总 )
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Adr_EP1, 20);
                            }
                            else
                            {
                                Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Kind, 1);
                                Read_meter_state = 0;
                            }

                            break;

                        default:
                            Meter_Data_Read(Chint, MeterInfo[Value_cnt].sn, Chint_Read_Kind, 1);
                            Read_meter_state = 0;
                            break;
                    }
                }
                break;

            //WattNode
            case WattNode:
                {
                    //Meter_Link = 0;
                    switch(Read_meter_state)
                    {
                        case 0:
                            Meter_Data_Read(WattNode, MeterInfo[Value_cnt].sn, WattNode_Read_Adr_PT, 8);//4
                            break;

                        case 1:
                            //读取电表  功率( 总--A--B--C )
                            Meter_Data_Read(WattNode, MeterInfo[Value_cnt].sn, WattNode_Read_Adr_EP, 2); //2
                            break;

                        case 2:
                            //读取总电量 ( 总 )
                            Meter_Data_Read(WattNode, MeterInfo[Value_cnt].sn, WattNode_Read_Adr_EPA, 16);
                            break;

                        default:
                            Read_meter_state = 0;
                            break;
                    }
                }
                break;
        }

        if(Read_meter_state >= 2)
        {
            Value_cnt ++;
            Value_cnt = Value_cnt % MeterLinkNum;
        }
    }
}


void Meter_Info_Read(void)
{
    static vu8 Info_cnt = 0;
    static vu8 Info_poll = 0;

    //延迟1000us切换 485使能为 接收
    if((Usart_485_Tx_Cnt == 0) && (LocalTime - TFM_rec_send_delay > 20))
    {
        TFM_rec_send_delay = LocalTime;
        //使能接收
        Usart_485_Rece;
    }

    //共10s 采集一个命令 读取电表值
    if(LocalTime > (Anti_Reflux_Meter_Read_Time + Meter_send_time))
    {
        Anti_Reflux_Meter_Read_Time = LocalTime;
        //清除接收缓存
        memset((u8 *)Usart_485_Rx_Buf, 0, Usart_485_Len);
        Usart_485_Read_Cnt = 0;
        //发送 命令
        Locally_Send = 1;

        switch(Meter[Info_cnt].Property.Pre_Id[1])
        {
            //正泰
            case Chint:
                {
                    //Meter_Link = 0;
                    switch(Read_meter_state)
                    {
                        case 0:
                            Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_Kind, 1);
                            break;

                        case 1:
                            if(MeterInfo[Info_cnt].phase == 3)
                            {
                                //读取电表软件版本号
                                Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_REV, 1);
                            }
                            else if(MeterInfo[Info_cnt].phase == 2)
                            {
                                //读取电表软件版本号
                                Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_REV1, 1);
                            }
                            else
                            {
                                Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_Kind, 1);
                                Read_meter_state = 0;
                            }

                            break;

                        case 2:
                            if(MeterInfo[Info_cnt].phase == 3)
                            {
                                //读取总电量 ( 总 )
                                Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_CtAmps, 1);
                            }
                            else if(MeterInfo[Info_cnt].phase == 2)
                            {
                                Read_meter_state++;
                            }
                            else
                            {
                                Meter_Data_Read(Chint, MeterInfo[Info_cnt].sn, Chint_Read_Kind, 1);
                                Read_meter_state = 0;
                            }

                            break;

                        default:
                            Read_meter_state = 0;
                            break;
                    }
                }
                break;

            //WattNode
            case WattNode:
                {
                    //Meter_Link = 0;
                    switch(Read_meter_state)
                    {
                        case 0:
                            //读取电表设备类型
                            Meter_Report_Get(WattNode, MeterInfo[Info_cnt].sn);
                            break;

                        case 1:
                            //读取电表CT值
                            Meter_Data_Read(WattNode, MeterInfo[Info_cnt].sn, WattNode_Read_CtAmps, 1);
                            break;

                        case 2:
                            Read_meter_state++;
                            break;

                        default:
                            Read_meter_state = 0;
                            break;
                    }
                }
                break;
        }

        //未收到数据的重复发多少次后 发另外的命令
        Read_meter_Repeat ++;

        if(Read_meter_Repeat > 3)
        {
            Read_meter_Repeat = 0;
            Info_cnt ++;
            Info_cnt = Info_cnt % MeterLinkNum;
            Info_poll++;

            if(Info_poll > 5)
            {
                Info_cnt = 0;
                Info_poll = 0;
                MeterNetwork  = 0;
            }
        }

        if(Read_meter_state >= 2)
        {
            Info_cnt ++;
            Read_meter_Repeat = 0;
            Info_cnt = Info_cnt % MeterLinkNum;
            Info_poll++;

            if(Info_poll > 5)
            {
                Info_cnt = 0;
                Info_poll = 0;
                MeterNetwork  = 0;
            }
        }
    }
}



//电表数据处理
void Meter_Data_Process(void)
{
    if(Dtu3Detail.Property.RS485Mode == 0)
    {
        //该DTU为主机
        if(Dtu_Host_Status == 1)
        {
            if(MeterLinkNum >= 5)
            {
                Meter_send_time = 500;
            }
            else
            {
                Meter_send_time  = 2000;
            }

            if(CT_set_flag == 1)
            {
                Meter_Write_CT();
            }
            else
            {
                if(MeterNetwork == 1)
                {
                    Meter_Info_Read();
                }
                else
                {
                    Meter_Value_Read();
                }
            }
        }
        //如果不是主机竞主时间差使用 DTU ID 来区分
        //超时没有收到数据认为主机失败 -->竞 主
        else if(LocalTime > (Anti_Reflux_Actual_Owner_Time + (6000 + Dtu3Major.Property.Id[cmp_owner_nub])))
        {
            //轮询 数据时间   竞主时候不轮询
            Anti_Reflux_Meter_Read_Time = LocalTime;
            //竞主时间
            Anti_Reflux_Actual_Owner_Time = LocalTime;

            if(cmp_owner_nub > 3)
            {
                cmp_owner_nub = 0;
            }

            if(Usart_485_Rec_owner == 2)
            {
                //在竞主之后未收到数据
                Dtu_Host_Status = 1;
            }

            Send_Actual_Owner();
            //在竞主的时候发送了竞主数据之后的2次内 没收到任何数据  则可以为主机
            Usart_485_Rec_owner = 2;
        }

        //释放主机  3分钟 之内 没收到 正确数据
        if(LocalTime > (Anti_Reflux_Owner_Time_Out + 60000 * 3))
        {
            Anti_Reflux_Owner_Time_Out = LocalTime;
            //开始竞主
            Anti_Reflux_Actual_Owner_Time = LocalTime;
            Dtu_Host_Status = 0;
            //15s 掉线时已经清0
            consumption_power_a = 0;
            consumption_power_b = 0;
            consumption_power_c = 0;
            consumption_power_total = 0;
            P_max_reflux = 0;
        }
    }
}


//////////////////////////////////////////
//控制功率  MIMajor[i].Property.Power_Limit
//微逆型号  DTU_All_Status[i].pre_id
//所在相位  DTU_All_Status[i].id_in_phase
//组件数-DTU    Dtu3Detail.Property.Reflux_DtuPanelNum_A    Dtu3Detail.Property.Reflux_DtuPanelNum_B    Dtu3Detail.Property.Reflux_DtuPanelNum_C[DTU]
//组件数-SYS    Dtu3Detail.Property.Reflux_SysPanelNum_A    Dtu3Detail.Property.Reflux_SysPanelNum_B    Dtu3Detail.Property.Reflux_SysPanelNum_C[SYS]
//耗电功率      consumption_power_a  consumption_power_b  consumption_power_c
////////////////////////////////////////
void power_control(u8 control_mode, u16 dtu_modul, u16 sys_modul, int32_t consumption_power, u8 phase)
{
    vu8 k = 0;
    vu8 num = 0;
    vu8 PortNumStart = 0;
    vu16 i = 0;
    vu16 control_num = 0;
    vu16 control_num_three = 0;
    vu16 temp;
    vu16 end_link_nub;
    vu16 PortNumMax = 0;
    vu16 PortMaxThree = 0;
    vu16 average_component_power = 0;
    vs32 get_power = 0;
    vs32 local_control_power = 0;
    vu32 p_temp;

    switch(phase)
    {
        case PH_NULL:
            sys_modul = Dtu3Detail.Property.PortNum - 1;
            dtu_modul = sys_modul;
            PortNumStart = 0;
            end_link_nub = Dtu3Detail.Property.PortNum;
            break;

        case PH_A:
            PortNumStart = 0;
            end_link_nub = Dtu3Detail.Property.Reflux_DtuPanelNum_A;
            break;

        case PH_B:
            //start
            PortNumStart = (u8)Dtu3Detail.Property.Reflux_DtuPanelNum_A;
            end_link_nub = Dtu3Detail.Property.Reflux_DtuPanelNum_A + Dtu3Detail.Property.Reflux_DtuPanelNum_B;
            break;

        case PH_C:
            PortNumStart = (u8)Dtu3Detail.Property.Reflux_DtuPanelNum_A + (u8)Dtu3Detail.Property.Reflux_DtuPanelNum_B;
            end_link_nub = Dtu3Detail.Property.Reflux_DtuPanelNum_A + Dtu3Detail.Property.Reflux_DtuPanelNum_B + Dtu3Detail.Property.Reflux_DtuPanelNum_C;
            break;
    }

    //绝对值控制
    if(control_mode == Absolute_Value_Control)
    {
        switch(phase)
        {
            case PH_NULL:
                if(Dtu3Detail.Property.SurplusSwitch == 1)
                {
                    //余电上网限制功率
                    //u16 SurplusPower = 0;
                    local_control_power = consumption_power + Dtu3Detail.Property.SurplusPowerA;
                }
                else
                {
                    local_control_power = consumption_power;
                }

                last_control_power_a = local_control_power;
                break;

            case PH_A:
                if(Dtu3Detail.Property.SurplusSwitch == 1)
                {
                    local_control_power = consumption_power + Dtu3Detail.Property.SurplusPowerA;
                }
                else
                {
                    local_control_power = consumption_power;
                }

                last_control_power_a = local_control_power;
                break;

            case PH_B:
                if(Dtu3Detail.Property.SurplusSwitch == 1)
                {
                    local_control_power = consumption_power + Dtu3Detail.Property.SurplusPowerB;
                }
                else
                {
                    local_control_power = consumption_power;
                }

                last_control_power_b = local_control_power;
                break;

            case PH_C:
                if(Dtu3Detail.Property.SurplusSwitch == 1)
                {
                    local_control_power = consumption_power + Dtu3Detail.Property.SurplusPowerC;
                }
                else
                {
                    local_control_power = consumption_power;
                }

                last_control_power_c = local_control_power;
                break;
        }
    }
    //差值控制
    else if(control_mode == DifferenceControl)
    {
        switch(phase)
        {
            case PH_NULL:
                last_control_power_a = 0;

                for(k = PortNumStart; k < end_link_nub; k++)
                {
                    if(PORT_NUMBER_CONFIRMATION(k))
                    {
                        //三代微逆
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[k].Property.Pre_Id) >= Inverter_Pro)
                        {
                            last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.GridActivePower[0] << 8) + MIReal[k].Data.GridActivePower[1];
                        }
                        //二代微逆
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[k].Property.Port)
                            {
                                case MI_500W_A:
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1] +
                                                           ((u16)MIReal[k + 2].Data.Power[0] << 8) + MIReal[k + 2].Data.Power[1] +
                                                           ((u16)MIReal[k + 3].Data.Power[0] << 8) + MIReal[k + 3].Data.Power[1];
                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1];
                                    break;
                            }
                        }
                    }
                }

                break;

            case PH_A:
                last_control_power_a = 0;

                for(k = PortNumStart; k < end_link_nub; k++)
                {
                    if(PORT_NUMBER_CONFIRMATION(k))
                    {
                        //三代微逆
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[k].Property.Pre_Id) >= Inverter_Pro)
                        {
                            last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.GridActivePower[0] << 8) + MIReal[k].Data.GridActivePower[1];
                        }
                        //二代微逆
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[k].Property.Port)
                            {
                                case MI_500W_A:
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1] +
                                                           ((u16)MIReal[k + 2].Data.Power[0] << 8) + MIReal[k + 2].Data.Power[1] +
                                                           ((u16)MIReal[k + 3].Data.Power[0] << 8) + MIReal[k + 3].Data.Power[1];
                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    last_control_power_a = last_control_power_a + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1];
                                    break;
                            }
                        }
                    }
                }

                break;

            case PH_B:
                last_control_power_b = 0;

                for(k = PortNumStart; k < end_link_nub; k++)
                {
                    if(PORT_NUMBER_CONFIRMATION(k))
                    {
                        //三代微逆
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[k].Property.Pre_Id) >= Inverter_Pro)
                        {
                            last_control_power_b = last_control_power_b + ((u16)MIReal[k].Data.GridActivePower[0] << 8) + MIReal[k].Data.GridActivePower[1];
                        }
                        //二代微逆
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[k].Property.Port)
                            {
                                case MI_500W_A:
                                    last_control_power_b = last_control_power_b + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    last_control_power_b = last_control_power_b + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1] +
                                                           ((u16)MIReal[k + 2].Data.Power[0] << 8) + MIReal[k + 2].Data.Power[1] +
                                                           ((u16)MIReal[k + 3].Data.Power[0] << 8) + MIReal[k + 3].Data.Power[1];
                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    last_control_power_b = last_control_power_b + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1];
                                    break;
                            }
                        }
                    }
                }

                break;

            case PH_C:
                last_control_power_c = 0;

                for(k = PortNumStart; k < end_link_nub; k++)
                {
                    if(PORT_NUMBER_CONFIRMATION(k))
                    {
                        //三代微逆
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[k].Property.Pre_Id) >= Inverter_Pro)
                        {
                            last_control_power_c = last_control_power_c + ((u16)MIReal[k].Data.GridActivePower[0] << 8) + MIReal[k].Data.GridActivePower[1];
                        }
                        //二代微逆
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[k].Property.Port)
                            {
                                case MI_500W_A:
                                    last_control_power_c = last_control_power_c + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1];
                                    break;

                                case MI_1000W_A:
                                    last_control_power_c = last_control_power_c + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1] +
                                                           ((u16)MIReal[k + 1].Data.Power[0] << 8) + MIReal[k + 1].Data.Power[1] +
                                                           ((u16)MIReal[k + 2].Data.Power[0] << 8) + MIReal[k + 2].Data.Power[1] +
                                                           ((u16)MIReal[k + 3].Data.Power[0] << 8) + MIReal[k + 3].Data.Power[1];
                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    last_control_power_c = last_control_power_c + ((u16)MIReal[k].Data.Power[0] << 8) + MIReal[k].Data.Power[1];
                                    break;
                            }
                        }
                    }
                }

                break;
        }

        switch(phase)
        {
            case PH_NULL:
            case PH_A:
                if(consumption_power > DIFFERENCE_MARGIN || consumption_power < (int32_t)(DIFFERENCE_MARGIN) * (-1))
                {
                    if(Dtu3Detail.Property.SurplusSwitch == 1)
                    {
                        get_power = consumption_power + Dtu3Detail.Property.SurplusPowerA;
                    }
                    else
                    {
                        get_power = consumption_power;
                    }

                    local_control_power = last_control_power_a - get_power;
                }
                else
                {
                    local_control_power =  last_control_power_a;
                }

                break;

            case PH_B:
                if(consumption_power > DIFFERENCE_MARGIN || consumption_power < (int32_t)(DIFFERENCE_MARGIN) * (-1))
                {
                    if(Dtu3Detail.Property.SurplusSwitch == 1)
                    {
                        get_power = consumption_power + Dtu3Detail.Property.SurplusPowerB;
                    }
                    else
                    {
                        get_power = consumption_power;
                    }

                    local_control_power = last_control_power_b - get_power;
                }
                else
                {
                    local_control_power =  last_control_power_b;
                }

                break;

            case PH_C:
                if(consumption_power > DIFFERENCE_MARGIN || consumption_power < (int32_t)(DIFFERENCE_MARGIN) * (-1))
                {
                    if(Dtu3Detail.Property.SurplusSwitch == 1)
                    {
                        get_power = consumption_power + Dtu3Detail.Property.SurplusPowerC;
                    }
                    else
                    {
                        get_power = consumption_power;
                    }

                    local_control_power = last_control_power_c - get_power;
                }
                else
                {
                    local_control_power =  last_control_power_c;
                }

                break;
        }
    }

    if(sys_modul == 0)
    {
        sys_modul = dtu_modul;
    }

    //本DTU控制功率
    local_control_power = local_control_power * (dtu_modul + 1) / (sys_modul + 1);

    if(local_control_power <= 0)
    {
        local_control_power = 0;
    }

    //按最小限功率需多少台
    PortNumMax  = (u16)(local_control_power / MY_MIN_POWER_LIMIT);
    PortMaxThree = (u16)(local_control_power / MY_MIN_POWER_LIMIT_THREE);

    //NET_ZERO_EXPORT  //防逆流广发
    //使用功率 > 每台 30w时 ，= 开启每台 限功率    当超过全额时开全额
    if(PortNumMax > (dtu_modul + 1))
    {
        //组件平均功率  = 此DTU相的功率/ 整相组件
        average_component_power = (u16)(local_control_power / (dtu_modul + 1));

        if(average_component_power > MY_MAX_PORT_LIMIT)
        {
            average_component_power = MY_MAX_PORT_LIMIT;
        }

        for(i = PortNumStart; i < end_link_nub; i++)
        {
            if((phase == PH_NULL) || (MIMajor[i].Property.Id_In_Phase == phase))
            {
                if((ANTI_BACKFLOW_250W(i)) || (ANTI_BACKFLOW_500W(i)) || (ANTI_BACKFLOW_1000W(i)))
                {
                    if((MIMajor[i].Property.Acq_Switch != 1) || ((MIMajor[i].Property.Power_Limit + CTRL_DIFF) < average_component_power) || (MIMajor[i].Property.Power_Limit > (average_component_power + CTRL_DIFF)))
                    {
                        //发限功率命令
                        if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                        {
                            //发限功率命令
                            MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                            MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                        }
                    }

                    MIMajor[i].Property.Acq_Switch = 1;

                    //限功率 实时控制 功率
                    if(ANTI_BACKFLOW_1000W(i))
                    {
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                        {
                            p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 1].Data.PVPower[0] << 8) + MIReal[i + 1].Data.PVPower[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 2].Data.PVPower[0] << 8) + MIReal[i + 2].Data.PVPower[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 3].Data.PVPower[0] << 8) + MIReal[i + 3].Data.PVPower[1]);
                        }
                        else
                        {
                            p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 1].Data.Power[0] << 8) + MIReal[i + 1].Data.Power[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 2].Data.Power[0] << 8) + MIReal[i + 2].Data.Power[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 3].Data.Power[0] << 8) + MIReal[i + 3].Data.Power[1]);
                        }

                        //每台限功率值
                        MIMajor[i].Property.Power_Limit = average_component_power * 4;
                    }
                    else if(ANTI_BACKFLOW_500W(i))
                    {
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                        {
                            p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 1].Data.PVPower[0] << 8) + MIReal[i + 1].Data.PVPower[1]);
                        }
                        else
                        {
                            p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
                            p_temp = p_temp + (u32)((MIReal[i + 1].Data.Power[0] << 8) + MIReal[i + 1].Data.Power[1]);
                        }

                        //每台限功率值
                        MIMajor[i].Property.Power_Limit = average_component_power * 2;
                    }
                    else
                    {
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                        {
                            p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
                        }
                        else
                        {
                            p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
                        }

                        //每台限功率值
                        MIMajor[i].Property.Power_Limit = average_component_power;
                    }

                    //发限功率命令
                    if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                            (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                            (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    {
                        //差值超过4后
                        if((p_temp > (average_component_power + CTRL_DIFF)) || ((p_temp + CTRL_DIFF) < average_component_power))
                        {
                            //发限功率命令
                            MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                            MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                            MIMajor[i].Property.Acq_Switch = 1;
                        }
                    }

                    if(ANTI_BACKFLOW_1000W(i))
                    {
                        i = i + 3;
                    }
                    else if(ANTI_BACKFLOW_500W(i))
                    {
                        i = i + 1;
                    }
                }
            }
        }
    }
    //不够每台30w时 因为限功率不住，所以需要关闭一些台数来让剩下的限功率
    else
    {
        p_temp = 0;
        control_num = 0;

        for(i = PortNumStart; i < end_link_nub; i++)
        {
            if((phase == PH_NULL) || (MIMajor[i].Property.Id_In_Phase == phase))
            {
                if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) < Inverter_Pro)
                {
                    if(ANTI_BACKFLOW_1000W(i))
                    {
                        p_temp = p_temp + (MY_MIN_POWER_LIMIT * 4);

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        i = i + 3;
                        control_num = control_num + 4;
                    }
                    else if(ANTI_BACKFLOW_500W(i))
                    {
                        p_temp = p_temp + (MY_MIN_POWER_LIMIT * 2);

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        i = i + 1;
                        control_num = control_num + 2;
                    }
                    else if(ANTI_BACKFLOW_250W(i))
                    {
                        p_temp = p_temp + MY_MIN_POWER_LIMIT;

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        control_num = control_num + 1;
                    }
                }
            }
        }

        p_temp = 0;
        control_num_three = 0;

        for(i = PortNumStart; i < end_link_nub; i++)
        {
            if((phase == PH_NULL) || (MIMajor[i].Property.Id_In_Phase == phase))
            {
                if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                {
                    if(ANTI_BACKFLOW_1000W(i))
                    {
                        p_temp = p_temp + (MY_MIN_POWER_LIMIT_THREE * 4);

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        i = i + 3;
                        control_num_three = control_num_three + 4;
                    }
                    else if(ANTI_BACKFLOW_500W(i))
                    {
                        p_temp = p_temp + (MY_MIN_POWER_LIMIT_THREE * 2);

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        i = i + 1;
                        control_num_three = control_num_three + 2;
                    }
                    else if(ANTI_BACKFLOW_250W(i))
                    {
                        p_temp = p_temp + MY_MIN_POWER_LIMIT_THREE;

                        if(p_temp > local_control_power)
                        {
                            break;
                        }

                        control_num_three = control_num_three + 1;
                    }
                }
            }
        }

        if(control_num_three > control_num)
        {
            PortNumMax = control_num_three;
            average_component_power = (u16)(local_control_power / PortNumMax);
            control_num_three = 0;

            for(i = PortNumStart; i < end_link_nub; i++)
            {
                if((phase == PH_NULL) || (MIMajor[i].Property.Id_In_Phase == phase))
                {
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    {
                        temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    }
                    else
                    {
                        temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    }

                    num = 0;

                    if((ANTI_BACKFLOW_1000W(i)) || (ANTI_BACKFLOW_500W(i)) || (ANTI_BACKFLOW_250W(i)))
                    {
                        if(ANTI_BACKFLOW_250W(i))
                        {
                            num = 1;
                        }
                        else if(ANTI_BACKFLOW_500W(i))
                        {
                            num = 2;
                        }
                        else if(ANTI_BACKFLOW_1000W(i))
                        {
                            num = 4;
                        }

                        if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                        {
                            if((((UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro) && (control_num_three < PortNumMax)) &&
                                    ((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != average_component_power * num))) ||
                                    ((UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) < Inverter_Pro) &&
                                     ((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))))
                            {
                                //发限功率命令
                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                            }
                        }

                        //开机
                        MIMajor[i].Property.Acq_Switch = 1;

                        if((UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro) && (control_num_three < PortNumMax))
                        {
                            //每台限功率值
                            MIMajor[i].Property.Power_Limit = average_component_power * num;
                            control_num_three = control_num_three + num;
                        }
                        else
                        {
                            MIMajor[i].Property.Power_Limit = 0;

                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) < Inverter_Pro)
                            {
                                //关机
                                MIMajor[i].Property.Acq_Switch = 0;
                            }
                        }
                    }

                    //                    if((UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro) && (control_num_three < PortNumMax))
                    //                    {
                    //                        if(ANTI_BACKFLOW_1000W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != average_component_power * 4))
                    //                            {
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            //开机
                    //                            MIMajor[i].Property.Acq_Switch = 1;
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = average_component_power * 4;
                    //                            control_num_three = control_num_three + 4;
                    //                        }
                    //                        else if(ANTI_BACKFLOW_500W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != average_component_power * 2))
                    //                            {
                    //                                //发限功率命令
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            //开机
                    //                            MIMajor[i].Property.Acq_Switch = 1;
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = average_component_power * 2;
                    //                            control_num_three = control_num_three + 2;
                    //                        }
                    //                        else if(ANTI_BACKFLOW_250W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != MY_MIN_POWER_LIMIT))
                    //                            {
                    //                                //发限功率命令
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            //开机
                    //                            MIMajor[i].Property.Acq_Switch = 1;
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = average_component_power;
                    //                            control_num_three = control_num_three + 1;
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        if(ANTI_BACKFLOW_1000W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            //关机的时候 还大于20w功率 则重新关机
                    //                            if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                    //                            {
                    //                                //发限功率命令
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                //开机
                    //                                MIMajor[i].Property.Acq_Switch = 1;
                    //                            }
                    //                            else
                    //                            {
                    //                                //关机
                    //                                MIMajor[i].Property.Acq_Switch = 0;
                    //                            }
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = 0;
                    //                        }
                    //                        else if(ANTI_BACKFLOW_500W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            //关机的时候 还大于20w功率 则重新关机
                    //                            if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                    //                            {
                    //                                //发限功率命令
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                //开机
                    //                                MIMajor[i].Property.Acq_Switch = 1;
                    //                            }
                    //                            else
                    //                            {
                    //                                //关机
                    //                                MIMajor[i].Property.Acq_Switch = 0;
                    //                            }
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = 0;
                    //                        }
                    //                        else if(ANTI_BACKFLOW_250W(i))
                    //                        {
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    //                            }
                    //                            else
                    //                            {
                    //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    //                            }
                    //                            //关机的时候 还大于20w功率 则重新关机
                    //                            if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                    //                            {
                    //                                //发限功率命令
                    //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                    //                                {
                    //                                    //发限功率命令
                    //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                    //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                    //                                }
                    //                            }
                    //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    //                            {
                    //                                //开机
                    //                                MIMajor[i].Property.Acq_Switch = 1;
                    //                            }
                    //                            else
                    //                            {
                    //                                //关机
                    //                                MIMajor[i].Property.Acq_Switch = 0;
                    //                            }
                    //                            //每台限功率值
                    //                            MIMajor[i].Property.Power_Limit = 0;
                    //                        }
                    //                    }
                }
            }
        }
        else
        {
            PortNumMax = control_num;
            average_component_power = (u16)(local_control_power / PortNumMax);

            for(i = PortNumStart; i < end_link_nub; i++)
            {
                if((phase == PH_NULL) || (MIMajor[i].Property.Id_In_Phase == phase))
                {
                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    {
                        temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                    }
                    else
                    {
                        temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                    }

                    num = 0;

                    if((ANTI_BACKFLOW_1000W(i)) || (ANTI_BACKFLOW_500W(i)) || (ANTI_BACKFLOW_250W(i)))
                    {
                        if(ANTI_BACKFLOW_250W(i))
                        {
                            num = 1;
                        }
                        else if(ANTI_BACKFLOW_500W(i))
                        {
                            num = 2;
                        }
                        else if(ANTI_BACKFLOW_1000W(i))
                        {
                            num = 4;
                        }

                        if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                                (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                        {
                            if(((i < (PortNumStart + PortNumMax)) && (temp > (average_component_power + CTRL_DIFF)) ||
                                    (MIMajor[i].Property.Power_Limit != average_component_power * num)) ||
                                    ((i >= (PortNumStart + PortNumMax)) && ((temp > MY_SHUTDOWN_VOLTAGE) ||
                                            (MIMajor[i].Property.Power_Limit != 0))))
                            {
                                //发限功率命令
                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                            }
                        }

                        //开机
                        MIMajor[i].Property.Acq_Switch = 1;

                        if(i < (PortNumStart + PortNumMax))
                        {
                            //每台限功率值
                            MIMajor[i].Property.Power_Limit = average_component_power * num;
                        }
                        else
                        {
                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) < Inverter_Pro)
                            {
                                //关机
                                MIMajor[i].Property.Acq_Switch = 0;
                            }

                            //每台限功率值
                            MIMajor[i].Property.Power_Limit = 0;
                        }
                    }
                }

                //                    //保证发电小于用电   最小限功率值
                //                    if(i < (PortNumStart + PortNumMax))
                //                    {
                //                        if(ANTI_BACKFLOW_1000W(i))
                //                        {
                //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                            {
                //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                            }
                //                            else
                //                            {
                //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                            }
                //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != average_component_power * 4))
                //                            {
                //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                                {
                //                                    //发限功率命令
                //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                                }
                //                            }
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                            //每台限功率值
                //                            MIMajor[i].Property.Power_Limit = average_component_power * 4;
                //                        }
                //                        else if(ANTI_BACKFLOW_500W(i))
                //                        {
                //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                            {
                //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                            }
                //                            else
                //                            {
                //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                            }
                //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != average_component_power * 2))
                //                            {
                //                                //发限功率命令
                //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                                {
                //                                    //发限功率命令
                //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                                }
                //                            }
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                            //每台限功率值
                //                            MIMajor[i].Property.Power_Limit = average_component_power * 2;
                //                        }
                //                        else if(ANTI_BACKFLOW_250W(i))
                //                        {
                //                            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                            {
                //                                temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                            }
                //                            else
                //                            {
                //                                temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                            }
                //                            if((temp > (average_component_power + CTRL_DIFF)) || (MIMajor[i].Property.Power_Limit != MY_MIN_POWER_LIMIT))
                //                            {
                //                                //发限功率命令
                //                                if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                        (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                                {
                //                                    //发限功率命令
                //                                    MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                    MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                                }
                //                            }
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                            //每台限功率值
                //                            MIMajor[i].Property.Power_Limit = average_component_power;
                //                        }
                //                    }
                //                    else
                //                {
                //                    if(ANTI_BACKFLOW_1000W(i))
                //                    {
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                        }
                //                        else
                //                        {
                //                            temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                        }
                //                        //关机的时候 还大于20w功率 则重新关机
                //                        if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                //                        {
                //                            //发限功率命令
                //                            if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                            {
                //                                //发限功率命令
                //                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                            }
                //                        }
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                        }
                //                        else
                //                        {
                //                            //关机
                //                            MIMajor[i].Property.Acq_Switch = 0;
                //                        }
                //                        //每台限功率值
                //                        MIMajor[i].Property.Power_Limit = 0;
                //                    }
                //                    else if(ANTI_BACKFLOW_500W(i))
                //                    {
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                        }
                //                        else
                //                        {
                //                            temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                        }
                //                        //关机的时候 还大于20w功率 则重新关机
                //                        if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                //                        {
                //                            //发限功率命令
                //                            if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                            {
                //                                //发限功率命令
                //                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                            }
                //                        }
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                        }
                //                        else
                //                        {
                //                            //关机
                //                            MIMajor[i].Property.Acq_Switch = 0;
                //                        }
                //                        //每台限功率值
                //                        MIMajor[i].Property.Power_Limit = 0;
                //                    }
                //                    else if(ANTI_BACKFLOW_250W(i))
                //                    {
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            temp = (u16)(MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1];
                //                        }
                //                        else
                //                        {
                //                            temp = (u16)(MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1];
                //                        }
                //                        //关机的时候 还大于20w功率 则重新关机
                //                        if((temp > MY_SHUTDOWN_VOLTAGE) || (MIMajor[i].Property.Power_Limit != 0))
                //                        {
                //                            //发限功率命令
                //                            if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                //                                    (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
                //                            {
                //                                //发限功率命令
                //                                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                //                                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
                //                            }
                //                        }
                //                        if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                //                        {
                //                            //开机
                //                            MIMajor[i].Property.Acq_Switch = 1;
                //                        }
                //                        else
                //                        {
                //                            //关机
                //                            MIMajor[i].Property.Acq_Switch = 0;
                //                        }
                //                        //每台限功率值
                //                        MIMajor[i].Property.Power_Limit = 0;
                //                    }
                //                    }
            }
        }
    }
}


long FloatTohex(float HEX)
{
    return *(long *)&HEX;
}
float hexToFloat(long HEX)
{
    return *(float *)&HEX;
}

void meter_process_0(u8 offsetadr)
{
    vu8 i = 0;
    vu32 temp_hex;
    volatile float temp_float;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                //正泰
                case Chint:
                    {
                        //电表类型地址
                        if(Uart_Rec_mybuf[4 + offsetadr] == 0xa6)
                        {
                            //单相 01 03 02 00 a6 38 3e
                            MeterInfo[i].phase = 2;
                        }
                        else
                        {
                            //三相 01 03 02 00 00 b8 44
                            MeterInfo[i].phase = 3;
                        }
                    }
                    break;

                //WattNode
                case WattNode:
                    {
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[0 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[0 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[0 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[0 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);

                        //单位 0.1w
                        if(((3 * temp_float * 10) < (THREE_PHASE_LIMIT)) && ((3 * temp_float * 10) > (THREE_PHASE_LIMIT * (-1))))
                        {
                            MeterInfo[i].power_total = (int32_t)(temp_float * 10);    //temp_float;
                        }

                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[1 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[1 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[1 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[1 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);

                        //单位 0.1w
                        if(((temp_float * 10) < (THREE_PHASE_LIMIT)) && ((temp_float * 10) > (THREE_PHASE_LIMIT * (-1))))
                        {
                            MeterInfo[i].power_a = (int32_t)(temp_float * 10);    //temp_float;
                        }

                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[2 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[2 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[2 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[2 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);

                        //单位 0.1w
                        if(((temp_float * 10) < (THREE_PHASE_LIMIT)) && ((temp_float * 10) > (THREE_PHASE_LIMIT * (-1))))
                        {
                            MeterInfo[i].power_b = (int32_t)(temp_float * 10);    //temp_float;
                        }

                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[3 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[3 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[3 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[3 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);

                        //单位 0.1w
                        if(((temp_float * 10) < (THREE_PHASE_LIMIT)) && ((temp_float * 10) > (THREE_PHASE_LIMIT * (-1))))
                        {
                            MeterInfo[i].power_c = (int32_t)(temp_float * 10);    //temp_float;
                        }
                    }
                    break;
            }
        }
    }
}

void meter_process_1(u8 offsetadr)
{
    vu8  i = 0;
    vu32 temp_hex;
    volatile float temp_float;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                //正泰
                case Chint:
                    {
                        if(MeterInfo[i].phase == 3)
                        {
                            //功率
                            temp_hex = (u32)((Uart_Rec_mybuf[3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[6 + offsetadr]);
                            //1.89kw = 1890w
                            temp_float = hexToFloat((long)temp_hex);

                            //单位 0.1w
                            if((temp_float < (3 * THREE_PHASE_LIMIT)) && (temp_float > (3 * THREE_PHASE_LIMIT * (-1))))
                            {
                                MeterInfo[i].power_total = (int32_t)temp_float;
                            }

                            temp_hex = (u32)((Uart_Rec_mybuf[1 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[1 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[1 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[1 * 4 + 6 + offsetadr]);
                            temp_float = hexToFloat((long)temp_hex);

                            //单位 0.1w
                            if((temp_float < (THREE_PHASE_LIMIT)) && (temp_float > (THREE_PHASE_LIMIT * (-1))))
                            {
                                MeterInfo[i].power_a = (int32_t)temp_float;
                            }

                            temp_hex = (u32)((Uart_Rec_mybuf[2 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[2 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[3 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[2 * 4 + 6 + offsetadr]);
                            temp_float =  hexToFloat((long)temp_hex);

                            //单位 0.1w
                            if((temp_float < (THREE_PHASE_LIMIT)) && (temp_float > (THREE_PHASE_LIMIT * (-1))))
                            {
                                MeterInfo[i].power_b = (int32_t)temp_float;
                            }

                            temp_hex = (u32)((Uart_Rec_mybuf[3 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[3 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[4 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[3 * 4 + 6 + offsetadr]);
                            temp_float = hexToFloat((long)temp_hex);

                            //单位 0.1w
                            if((temp_float < (THREE_PHASE_LIMIT)) && (temp_float > (THREE_PHASE_LIMIT * (-1))))
                            {
                                MeterInfo[i].power_c = (int32_t)temp_float;
                            }

                            temp_hex = (u32)((Uart_Rec_mybuf[12 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[12 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[12 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[12 * 4 + 6 + offsetadr]);
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].factor = (int32_t)temp_float;
                        }
                        else
                        {
                            temp_hex = (u32)((Uart_Rec_mybuf[3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[6 + offsetadr]);
                            //kw        0.0104=10.4w
                            temp_float = hexToFloat((long)temp_hex);

                            //单位 0.1w
                            if((temp_float * 10000 < SINGLE_PHASE_LIMIT) && (temp_float * 10000 > (SINGLE_PHASE_LIMIT) * (-1)))
                            {
                                MeterInfo[i].power_total = (int32_t)(temp_float * 10000);
                                //单位 0.1w
                                MeterInfo[i].power_a = MeterInfo[i].power_total;
                            }

                            temp_hex = (u32)((Uart_Rec_mybuf[3 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[3 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[3 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[3 * 4 + 6 + offsetadr]);
                            //kw        0.0104=10.4w
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].factor = (int32_t)(temp_float * 10000);
                        }
                    }
                    break;

                //WattNode
                case WattNode:
                    {
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[0 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[0 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[0 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[0 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].positive_energy = (int32_t)(temp_float * 10000);
                    }
                    break;
            }

            break;
        }
    }
}
void meter_process_2(u8 offsetadr)
{
    vu8 i = 0;
    vu32 temp_hex;
    volatile float temp_float;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                /*正泰*/
                case Chint:
                    {
                        // 3or 2 is same
                        if(MeterInfo[i].phase == 3)
                        {
                            temp_hex = (u32)((Uart_Rec_mybuf[3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].positive_energy = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[1 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[1 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[1 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[1 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].positive_energy_a = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[2 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[2 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[2 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[2 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].positive_energy_b = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[3 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[3 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[3 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[3 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].positive_energy_c = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[5 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[5 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[5 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].Reverse_energy = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[6 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[6 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[6 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[6 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].Reverse_energy_a = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[7 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[7 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[7 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[7 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].Reverse_energy_b = (int32_t)(temp_float * 1000);
                            temp_hex = (u32)((Uart_Rec_mybuf[8 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[8 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[8 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[8 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].Reverse_energy_c = (int32_t)(temp_float * 1000);
                        }
                        else
                        {
                            temp_hex = (u32)((Uart_Rec_mybuf[3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].positive_energy = (int32_t)(temp_float * 1000);
                            MeterInfo[i].positive_energy_a = MeterInfo[i].positive_energy;
                            temp_hex = (u32)((Uart_Rec_mybuf[5 * 4 + 3 + offsetadr] << 24) +
                                             (Uart_Rec_mybuf[5 * 4 + 4 + offsetadr] << 16) +
                                             (Uart_Rec_mybuf[5 * 4 + 5 + offsetadr] << 8) +
                                             Uart_Rec_mybuf[5 * 4 + 6 + offsetadr]);
                            //(1.89KWh = 1.9KWh) 保留1位小数
                            temp_float = hexToFloat((long)temp_hex);
                            MeterInfo[i].Reverse_energy = (int32_t)(temp_float * 1000);
                            MeterInfo[i].Reverse_energy_a = MeterInfo[i].Reverse_energy;
                        }
                    }
                    break;

                /*WattNode*/
                case WattNode:
                    {
                        /*A 相正向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[0 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[0 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[0 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[0 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].positive_energy_a = (int32_t)(temp_float * 1000);
                        /*B 相正向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[1 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[1 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[1 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[1 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].positive_energy_b = (int32_t)(temp_float * 1000);
                        /*C 相正向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[2 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[2 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[2 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[2 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].positive_energy_c = (int32_t)(temp_float * 1000);
                        /*反向有功电量*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[3 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[3 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[3 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[3 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].Reverse_energy = (int32_t)(temp_float * 1000);
                        /*A 相反向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[5 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[5 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[5 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[5 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].Reverse_energy_a = (int32_t)(temp_float * 1000);
                        /*B 相反向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[6 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[6 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[6 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[6 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].Reverse_energy_b = (int32_t)(temp_float * 1000);
                        /*C 相反向有功总电能*/
                        temp_hex = 0;
                        temp_float = 0;
                        temp_hex = (u32)((Uart_Rec_mybuf[7 * 4 + 5 + offsetadr] << 24) +
                                         (Uart_Rec_mybuf[7 * 4 + 6 + offsetadr] << 16) +
                                         (Uart_Rec_mybuf[7 * 4 + 3 + offsetadr] << 8) +
                                         Uart_Rec_mybuf[7 * 4 + 4 + offsetadr]);
                        temp_float = hexToFloat((long)temp_hex);
                        MeterInfo[i].Reverse_energy_c = (int32_t)(temp_float * 1000);
                    }
                    break;
            }

            break;
        }
    }
}
void meter_info_0(u8 offsetadr)
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 len = 0;
    vu8 meter_slave_id = 0;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                /*正泰*/
                case Chint:
                    {
                        /*电表类型地址*/
                        if(Uart_Rec_mybuf[4 + offsetadr] == 0xa6)
                        {
                            /*单相 01 03 02 00 a6 38 3e*/
                            MeterInfo[i].phase = 2;
                        }
                        else
                        {
                            /*三相 01 03 02 00 00 b8 44*/
                            MeterInfo[i].phase = 3;
                        }
                    }
                    break;

                /*WattNode*/
                case WattNode:
                    {
                        len = Uart_Rec_mybuf[2 + offsetadr];
                        meter_slave_id = Uart_Rec_mybuf[3 + offsetadr];

                        if(Uart_Rec_mybuf[4 + offsetadr] == 0xFF)
                        {
                            for(i = 0, j = 0; i < len; i++)
                            {
                                if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + i], ",", 1) == 0)
                                {
                                    j = i + 1;
                                }
                            }

                            if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3Y-208-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3Y-400-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3Y-480-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3Y-600-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3D-400-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WNC-3D-480-MB", 13) == 0)
                            {
                            }
                            else if(strncmp((char *)&Uart_Rec_mybuf[4 + offsetadr + j + 1], "WND-WR-MB", 9) == 0)
                            {
                            }
                        }
                    }
                    break;
            }

            break;
        }
    }
}
void meter_info_1(u8 offsetadr)
{
    vu8 i = 0;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                /*正泰*/
                case Chint:
                    {
                        //                        //电表类型地址
                        //                        if(Uart_Rec_mybuf[4 + offsetadr] == 0xa6)
                        //                        {
                        //                            //单相 01 03 02 00 a6 38 3e
                        //                            MeterInfo[i].phase = 2;
                        //                        }
                        //                        else
                        //                        {
                        //                            //三相 01 03 02 00 00 b8 44
                        //                            MeterInfo[i].phase = 3;
                        //                        }
                    }
                    break;

                /*WattNode*/
                case WattNode:
                    {
                        MeterInfo[i].CT = (u16)(Uart_Rec_mybuf[3 + offsetadr] << 8) + (u16)Uart_Rec_mybuf[4 + offsetadr];
                    }
                    break;
            }

            break;
        }
    }
}
void meter_info_2(u8 offsetadr)
{
    vu8 i = 0;

    for(i = 0; i < MeterLinkNum; i++)
    {
        if(MeterInfo[i].sn == Uart_Rec_mybuf[offsetadr])
        {
            MeterInfo[i].Send_CNT++;

            switch(Meter[i].Property.Pre_Id[1])
            {
                /*正泰*/
                case Chint:
                    {
                        MeterInfo[i].CT = (u16)(Uart_Rec_mybuf[3 + offsetadr] << 8) + (u16) Uart_Rec_mybuf[4 + offsetadr];
                    }
                    break;

                /*WattNode*/
                case WattNode:
                    {
                        //                        MeterInfo[i].CT = (Uart_Rec_mybuf[3 + offsetadr] << 8) + Uart_Rec_mybuf[4 + offsetadr];
                    }
                    break;
            }

            break;
        }
    }
}
void ANTI_REFLUX3_process(void)
{
    vu8 i = 0;
    vu8  offset_adr;
    vu8  buf_len;
    vu16 uart6_rev_crc;
    vu16 uart6_crc;
    static vu16 Offline_cnt = 0;
    static vu32 PortNOLast = 0;
    static vu8 Get_Info_cnt = 0;

    if(MeterLinkNum != 0)
    {
        /*电表数据发送逻辑*/
        Meter_Data_Process();

        /*如果接收到新数据*/
        if(Usart_485_Rec_Data == 1)
        {
            if(Locally_Send != 1)
            {
                /*不是本机发送但收到数据则不是主机*/
                Dtu_Host_Status = 0;
            }

            /*接收时间超过100ms串口接收超时结束接收*/
            if(LocalTime > (Usart_485_Rec_Time + 100))
            {
                Locally_Send = 0;
                Usart_485_Rec_Time = LocalTime;
                memcpy((u8 *)&Uart_Rec_mybuf[0], (u8 *)&Usart_485_Rx_Buf[0], sizeof(Uart_Rec_mybuf));
                /*传递接收数据长度*/
                buf_len = (u8)Usart_485_Read_Cnt;

                if(buf_len > 2)
                {
                    if(Dtu_Host_Status == 1)
                    {
                        uart6_crc = (u16)Uart_Rec_mybuf[buf_len - 2]  + (u16)(Uart_Rec_mybuf[buf_len - 1] << 8);
                        uart6_rev_crc = ModRTU_CRC((u8 *)Uart_Rec_mybuf, (buf_len - 2));

                        if(uart6_crc == uart6_rev_crc)
                        {
                            /*重发统计清零*/
                            Read_meter_Repeat = 0;
                            /*有线控*/
                            Anti_Reflux_Kind = 1;
                            /*收到正确包，主机连接状态正常，超时状态时间重置*/
                            Anti_Reflux_Owner_Time_Out = LocalTime;
                            /*有数据收到有主机竞主时间重计*/
                            Anti_Reflux_Actual_Owner_Time = LocalTime;
                            /*掉线计数清零*/
                            Anti_Reflux_Drop_Times_3w = 0;
                            /*电表已连接*/
                            Meter_Link = 1;

                            if(MeterNetwork == 1)
                            {
                                if(Read_meter_state == 0)
                                {
                                    if(MeterInfo[0].sn == Uart_Rec_mybuf[0])
                                    {
                                        if(Get_Info_cnt >= 1)
                                        {
                                            Get_Info_cnt = 0;
                                            MeterNetwork = 0;
                                        }
                                        else
                                        {
                                            Get_Info_cnt++;
                                        }
                                    }
                                }

                                switch(Read_meter_state)
                                {
                                    case  0:
                                        meter_info_0(0);
                                        Read_meter_state++;
                                        break;

                                    case  1:
                                        meter_info_1(0);
                                        Read_meter_state++;
                                        break;

                                    case  2:
                                        meter_info_2(0);
                                        Read_meter_state = 0;
                                        break;

                                    default:
                                        Read_meter_state = 0;
                                        break;
                                }
                            }
                            else
                            {
                                switch(Read_meter_state)
                                {
                                    case  0:
                                        meter_process_0(0);
                                        Read_meter_state++;
                                        break;

                                    case  1:
                                        meter_process_1(0);
                                        Read_meter_state++;
                                        break;

                                    case  2:
                                        meter_process_2(0);
                                        Read_meter_state = 0;
                                        break;

                                    default:
                                        Read_meter_state = 0;
                                        break;
                                }
                            }
                        }
                        /*判断是否是竞主信息*/
                        else if(Uart_Rec_mybuf[0] == 0x5a)
                        {
                            /*收到竞主信息，竟主计时清零*/
                            Anti_Reflux_Actual_Owner_Time = LocalTime;
                            /*收到竞主信息，主机标志清零*/
                            Dtu_Host_Status = 0;
                        }

                        memset((u8 *)Uart_Rec_mybuf, 0, sizeof(Uart_Rec_mybuf));
                    }
                    else
                    {
                        offset_adr = 8;
                        uart6_crc = (u16)Uart_Rec_mybuf[buf_len - 2]  + (u16)(Uart_Rec_mybuf[buf_len - 1] << 8);
                        uart6_rev_crc = ModRTU_CRC((u8 *)&Uart_Rec_mybuf[offset_adr], (buf_len - 2 - offset_adr));

                        if(uart6_crc == uart6_rev_crc)
                        {
                            /*重发统计清零*/
                            Read_meter_Repeat = 0;
                            /*有线控*/
                            Anti_Reflux_Kind = 1;
                            /*收到正确包，主机连接状态正常，超时状态时间重置*/
                            Anti_Reflux_Owner_Time_Out = LocalTime;
                            /*有数据收到有主机竞主时间重计*/
                            Anti_Reflux_Actual_Owner_Time = LocalTime;
                            /*掉线计数清零*/
                            Anti_Reflux_Drop_Times_3w = 0;
                            /*电表已连接*/
                            Meter_Link = 1;

                            if(Uart_Rec_mybuf[1] == 0x11)
                            {
                                MeterNetwork = 1;
                                Read_meter_state = 0;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_Kind >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_Kind & 0xFF)))
                            {
                                Read_meter_state = 0;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_REV >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_REV & 0xFF)))
                            {
                                MeterNetwork = 1;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_REV1 >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_REV1 & 0xFF)))
                            {
                                MeterNetwork = 1;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_CtAmps >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_CtAmps & 0xFF)))
                            {
                                MeterNetwork = 1;
                                Read_meter_state = 2;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(WattNode_Read_CtAmps >> 8)) && (Uart_Rec_mybuf[3] == (u8)(WattNode_Read_CtAmps & 0xFF)))
                            {
                                MeterNetwork = 1;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_Adr_PT >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_Adr_PT & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_Adr_PT1 >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_Adr_PT1 & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_Adr_EP >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_Adr_EP & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 2;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(Chint_Read_Adr_EP1 >> 8)) && (Uart_Rec_mybuf[3] == (u8)(Chint_Read_Adr_EP1 & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 2;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(WattNode_Read_Adr_PT >> 8)) && (Uart_Rec_mybuf[3] == (u8)(WattNode_Read_Adr_PT & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 0;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(WattNode_Read_Adr_EP >> 8)) && (Uart_Rec_mybuf[3] == (u8)(WattNode_Read_Adr_EP & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 1;
                            }
                            else if((Uart_Rec_mybuf[2] == (u8)(WattNode_Read_Adr_EPA >> 8)) && (Uart_Rec_mybuf[3] == (u8)(WattNode_Read_Adr_EPA & 0xFF)))
                            {
                                MeterNetwork = 0;
                                Read_meter_state = 2;
                            }

                            if(MeterNetwork == 1)
                            {
                                if((MeterInfo[MeterLinkNum - 1].sn == Uart_Rec_mybuf[0]) && (Read_meter_state >= 2))
                                {
                                    MeterNetwork = 0;
                                }

                                switch(Read_meter_state)
                                {
                                    case  0:
                                        meter_info_0(8);
                                        Read_meter_state++;
                                        break;

                                    case  1:
                                        meter_info_1(8);
                                        Read_meter_state++;
                                        break;

                                    case  2:
                                        meter_info_2(8);
                                        Read_meter_state = 0;
                                        break;

                                    default:
                                        Read_meter_state = 0;
                                        break;
                                }
                            }
                            else
                            {
                                switch(Read_meter_state)
                                {
                                    case  0:
                                        meter_process_0(8);
                                        Read_meter_state++;
                                        break;

                                    case  1:
                                        meter_process_1(8);
                                        Read_meter_state++;
                                        break;

                                    case  2:
                                        meter_process_2(8);
                                        Read_meter_state = 0;
                                        break;

                                    default:
                                        Read_meter_state = 0;
                                        break;
                                }
                            }
                        }
                        /*判断是否是竞主信息*/
                        else if(Uart_Rec_mybuf[0] == 0x5a)
                        {
                            /*收到竞主信息，竟主计时清零*/
                            Anti_Reflux_Actual_Owner_Time = LocalTime;
                            /*收到竞主信息，主机标志清零*/
                            Dtu_Host_Status = 0;
                        }

                        memset((u8 *)Uart_Rec_mybuf, 0, sizeof(Uart_Rec_mybuf));
                    }
                }
                else
                {
                    /*收到别人的竞主信息*/
                    if(Uart_Rec_mybuf[0] == 0x5a)
                    {
                        /*收到竞主信息，竟主计时清零*/
                        Anti_Reflux_Actual_Owner_Time = LocalTime;
                        /*收到竞主信息，主机标志清零*/
                        Dtu_Host_Status = 0;
                    }
                }

                Usart_485_Rec_Data = 0;
                /*轮询 数据时间   竞主时候不轮询*/
                Anti_Reflux_Meter_Read_Time = LocalTime;
                /*竞主时间*/
                Anti_Reflux_Actual_Owner_Time = LocalTime;
                Usart_485_Read_Cnt = 0;
            }
        }

        /*15s中 计算控制一次*/
        if((LocalTime > (Anti_Reflux_3w_Time + 15000)) && (Anti_Reflux_Kind == 1))
        {
            if(PortNOLast > PortNO)
            {
                ControlMode ++;
                ControlMode %= 2;
            }

            PortNOLast = PortNO;

            if((Dtu3Detail.Property.PortNum < SUBCONTRACT_MAX_NUM) || (ControlMode == 0))
            {
                Anti_Reflux_3w_Time = LocalTime;
                Anti_Reflux_Drop_Times_3w ++;
                P_max_reflux = 0;
                consumption_power_a = 0;
                consumption_power_b = 0;
                consumption_power_c = 0;
                ControlPlace = Meter_Load_Side;

                /*防逆流电网类型*/
                if(Dtu3Detail.Property.GridType == 2)
                {
                    /*整体控制 0：总体控制 1：分相控制*/
                    if(Dtu3Detail.Property.OverallControl == 0)
                    {
                        ControlPhase = 1;
                    }
                    else
                    {
                        ControlPhase = 3;
                    }
                }
                else
                {
                    ControlPhase = 1;
                }

                for(i = 0; i < MeterLinkNum; i++)
                {
                    if(MeterInfo[i].Send_CNT >= OFFLINE_TIMES)
                    {
                        MeterInfo[i].fault = 1;
                    }
                    else
                    {
                        MeterInfo[i].fault = 0;
                    }

                    MeterInfo[i].Send_CNT = 0;

                    if(Meter[i].Property.place == Meter_Grid_Side)
                    {
                        ControlPlace = Meter_Grid_Side;
                    }
                }

                for(i = 0; i < MeterLinkNum; i++)
                {
                    /*不在光伏侧的电表读取参数进行控制*/
                    if(Meter[i].Property.place != Meter_Photovoltaic_Side)
                    {
                        /*电网侧*/
                        if(ControlPlace == Meter_Grid_Side)
                        {
                            P_max_reflux = P_max_reflux + (MeterInfo[i].power_total);
                            consumption_power_a = consumption_power_a + (MeterInfo[i].power_a);
                            consumption_power_b = consumption_power_b + (MeterInfo[i].power_b);
                            consumption_power_c = consumption_power_c + (MeterInfo[i].power_c);
                        }
                        else
                        {
                            P_max_reflux = P_max_reflux + abs(MeterInfo[i].power_total);
                            consumption_power_a = consumption_power_a + abs(MeterInfo[i].power_a);
                            consumption_power_b = consumption_power_b + abs(MeterInfo[i].power_b);
                            consumption_power_c = consumption_power_c + abs(MeterInfo[i].power_c);
                        }
                    }
                }

                /*5分钟掉线关闭微逆*/
                if(Anti_Reflux_Drop_Times_3w > 20)
                {
                    Anti_Reflux_Drop_Times_3w = 20;
                    consumption_power_a = 0;
                    consumption_power_b = 0;
                    consumption_power_c = 0;
                    consumption_power_total = 0;
                    P_max_reflux = 0;
                    Offline_cnt++;

                    /*发送5次关闭，停止微逆控制*/
                    if(Offline_cnt >= 5)
                    {
                        Anti_Reflux_Kind = 0;
                        Offline_cnt = 0;
                    }
                }

                /*电网侧*/
                if(ControlPlace == Meter_Grid_Side)
                {
                    /*差值控制*/
                    ControlMode = DifferenceControl;
                }
                /*负载侧*/
                else if(ControlPlace == Meter_Load_Side)
                {
                    /*绝对值控制*/
                    ControlMode = Absolute_Value_Control;
                }

                /*0电表断线  不控制*/
                if((Meter_Link == 1) && (Dtu3Detail.Property.Zero_Export_Switch != 0))
                {
                    if(ControlPhase == 3)
                    {
                        power_control(ControlMode, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A, consumption_power_a, PH_A);
                        power_control(ControlMode, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B, consumption_power_b, PH_B);
                        power_control(ControlMode, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C, consumption_power_c, PH_C);
                    }
                    else
                    {
                        /*网络不控制防逆流*/
                        power_control(ControlMode, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A, P_max_reflux, PH_NULL);
                    }
                }
            }
        }
    }
}


//////////=========================================================////////三相平衡控制
void if_trouble_phase(void)
{
    vu16 i, a, b, c;
    a = 0;
    b = 0;
    c = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        //电网电压小于60V为异常
        if((((u16)MIReal[i].Data.GridVol[0] << 8) + MIReal[i].Data.GridVol[1]) < GRID_SIDE_VOLTAGE_LIMIT)
        {
            if(MIMajor[i].Property.Id_In_Phase == PH_A)
            {
                a++;

                if(a == Dtu3Detail.Property.Reflux_DtuPanelNum_A)
                {
                    Cntrl_Pa = 0;
                    Cntrl_Pb = 0;
                    Cntrl_Pc = 0;
                    break;
                }
            }
            else if(MIMajor[i].Property.Id_In_Phase == PH_B)
            {
                b++;

                if(b == Dtu3Detail.Property.Reflux_DtuPanelNum_B)
                {
                    Cntrl_Pa = 0;
                    Cntrl_Pb = 0;
                    Cntrl_Pc = 0;
                    break;
                }
            }
            else if(MIMajor[i].Property.Id_In_Phase == PH_C)
            {
                c++;

                if(c == Dtu3Detail.Property.Reflux_DtuPanelNum_C)
                {
                    Cntrl_Pa = 0;
                    Cntrl_Pb = 0;
                    Cntrl_Pc = 0;
                    break;
                }
            }
        }
    }
}

void if_offline_phase(void)
{
    vu16 i = 0;
    vu16 a = 0;
    vu16 b = 0;
    vu16 c = 0;
    vu32 MAX_Pa = 0;
    vu32 MAX_Pb = 0;
    vu32 MAX_Pc = 0;
    MAX_Pa = Dtu3Detail.Property.Reflux_DtuPanelNum_A * MY_MAX_PORT_LIMIT;//(MMY_MAX_PORT_LIMIT;//Y_MAX_PORT_LIMIT / 10);
    MAX_Pb = Dtu3Detail.Property.Reflux_DtuPanelNum_B * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
    MAX_Pc = Dtu3Detail.Property.Reflux_DtuPanelNum_C * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(MIReal[i].Data.LinkState == 0)  //MI离线
        {
            if(MIMajor[i].Property.Id_In_Phase == PH_A)
            {
                a++;

                if(a == Dtu3Detail.Property.Reflux_DtuPanelNum_A)
                {
                    Cntrl_Pa = MAX_Pa;
                    Cntrl_Pb = MAX_Pb;
                    Cntrl_Pc = MAX_Pc;
                    break;
                }
            }
            else if(MIMajor[i].Property.Id_In_Phase == PH_B)
            {
                b++;

                if(b == Dtu3Detail.Property.Reflux_DtuPanelNum_B)
                {
                    Cntrl_Pa = MAX_Pa;
                    Cntrl_Pb = MAX_Pb;
                    Cntrl_Pc = MAX_Pc;
                    break;
                }
            }
            else if(MIMajor[i].Property.Id_In_Phase == PH_C)
            {
                c++;

                if(c == Dtu3Detail.Property.Reflux_DtuPanelNum_C)
                {
                    Cntrl_Pa = MAX_Pa;
                    Cntrl_Pb = MAX_Pb;
                    Cntrl_Pc = MAX_Pc;
                    break;
                }
            }
        }
    }
}




//获取功率
vu16 get_id_power(vu8 *mpower)
{
    vu16 midpower = 0;
    midpower = ((*mpower) << 8) + *(mpower + 1);
    midpower = midpower;// / 10;
    return midpower;
}

//根据条件，获取控制 各相的最大控制功率
vu32 get_my_cntrl_power(vu32 cntrl_pdst, vu32 real_pdst, vu32 real_psrc, vu8 nub_power_ph, u16 dtu_modul, u16 sys_modul)
{
    vs32 temp_ddt1 = 0;
    my_tolerance_between_phases = (Dtu3Detail.Property.Tolerance_Between_Phases * 10) * (dtu_modul / sys_modul);

    ///////////////////////////////// x/(reals+dt) = cntrld/reald

    if(nub_power_ph == 0)
    {
        cntrl_pdst = dtu_modul * MY_MAX_PORT_LIMIT;
    }
    else
    {
        if((real_pdst > (real_psrc + my_tolerance_between_phases)) || (real_pdst < (real_psrc + my_tolerance_between_phases / 2)))
        {
            temp_ddt1 = (vs32)real_psrc + (vs32)my_tolerance_between_phases - (vs32)real_pdst;

            if(temp_ddt1 > 0)
            {
                cntrl_pdst = (cntrl_pdst + temp_ddt1 / 2) * dtu_modul  / nub_power_ph;   //cntrl_pdst/real_pdst;
            }
            else
            {
                cntrl_pdst = (real_psrc + my_tolerance_between_phases) * dtu_modul  / nub_power_ph;    //cntrl_pdst/real_pdst;
            }
        }
    }

    /////////////////////////////////
    if(cntrl_pdst  >= dtu_modul * MY_MAX_PORT_LIMIT)    //控制功率大于最大功率-->不限
    {
        cntrl_pdst  = dtu_modul * MY_MAX_PORT_LIMIT;
    }

    return cntrl_pdst;
}
void get_real_power(void)
{
    vu16 i = 0;
    Real_Pa = 0;
    Real_Pb = 0;
    Real_Pc = 0;
    nub_power_pha = 0;
    nub_power_phb = 0;
    nub_power_phc = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(MIMajor[i].Property.Id_In_Phase == PH_A)
        {
            //三代微逆
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                Real_Pa = Real_Pa + get_id_power(MIReal[i].Data.PVPower);

                if(get_id_power(MIReal[i].Data.PVPower) > 10)
                {
                    nub_power_pha ++;
                }
            }
            else
                //二代微逆
            {
                Real_Pa = Real_Pa + get_id_power(MIReal[i].Data.Power);

                if(get_id_power(MIReal[i].Data.Power) > 10)
                {
                    nub_power_pha ++;
                }
            }
        }
        else if(MIMajor[i].Property.Id_In_Phase == PH_B)
        {
            //三代微逆
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                Real_Pb = Real_Pb + get_id_power(MIReal[i].Data.PVPower);

                if(get_id_power(MIReal[i].Data.PVPower) > 10)
                {
                    nub_power_phb ++;
                }
            }
            else
                //二代微逆
            {
                Real_Pb = Real_Pb + get_id_power(MIReal[i].Data.Power);

                if(get_id_power(MIReal[i].Data.Power) > 10)
                {
                    nub_power_phb ++;
                }
            }
        }
        else if(MIMajor[i].Property.Id_In_Phase == PH_C)
        {
            //三代微逆
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                Real_Pc = Real_Pc + get_id_power(MIReal[i].Data.PVPower);

                if(get_id_power(MIReal[i].Data.PVPower) > 10)
                {
                    nub_power_phc ++;
                }
            }
            else
                //二代微逆
            {
                Real_Pc = Real_Pc + get_id_power(MIReal[i].Data.Power);

                if(get_id_power(MIReal[i].Data.Power) > 10)
                {
                    nub_power_phc ++;
                }
            }
        }
    }
}

//
vu32 get_max(vu32 a, vu32 b, vu32 c)
{
    if((a >= b) && (a >= c))
    {
        return a;
    }
    else if((b >= a) && (b >= c))
    {
        return b;
    }
    else //if((c >= b) && (c >= a))
    {
        return c;
    }
}

vu32 get_min(vu32 a, vu32 b, vu32 c)
{
    if((a <= b) && (a <= c))
    {
        f_phase = 1;
        return a;
    }
    else if((b <= a) && (b <= c))
    {
        f_phase = 2;
        return b;
    }
    else //if((c <= b) && (c <= a))
    {
        f_phase = 3;
        return c;
    }
}
//
vu8 get_my_control_base(void)
{
    vu8 base_phase = 0;
    vu32 MAX_Pa = 0;
    vu32 MAX_Pb = 0;
    vu32 MAX_Pc = 0;
    MAX_Pa = Dtu3Detail.Property.Reflux_DtuPanelNum_A * MY_MAX_PORT_LIMIT;//(MMY_MAX_PORT_LIMIT;//Y_MAX_PORT_LIMIT / 10);
    MAX_Pb = Dtu3Detail.Property.Reflux_DtuPanelNum_B * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
    MAX_Pc = Dtu3Detail.Property.Reflux_DtuPanelNum_C * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);

    //返回1=A  2=B  3=C
    //都未限功率
    if((Cntrl_Pa >= MAX_Pa) && (Cntrl_Pb >= MAX_Pb) && (Cntrl_Pc >= MAX_Pc))
    {
        if((Real_Pc >= Real_Pa) && (Real_Pb >= Real_Pa))
        {
            //A 采样最小
            base_phase = 1;
        }
        else if((Real_Pa >= Real_Pb) && (Real_Pc >= Real_Pb))
        {
            //B 采样最小
            base_phase = 2;
        }
        else if((Real_Pa >= Real_Pc) && (Real_Pb >= Real_Pc))
        {
            //C 采样最小
            base_phase = 3;
        }
        else
        {
            //A 采样最小
            base_phase = 1;
        }
    }
    //2 相未 限功率
    else if((Cntrl_Pa >= MAX_Pa) && (Cntrl_Pb >= MAX_Pb))
    {
        if(Real_Pb >= Real_Pa)
        {
            //A 采样最小
            base_phase = 1;
        }
        else
        {
            //B 采样最小
            base_phase = 2;
        }

        return base_phase;
    }
    else if((Cntrl_Pa >= MAX_Pa) && (Cntrl_Pc >= MAX_Pc))
    {
        if(Real_Pc >= Real_Pa)
        {
            //A 采样最小
            base_phase = 1;
        }
        else
        {
            //C 采样最小
            base_phase = 3;
        }
    }
    else if((Cntrl_Pb >= MAX_Pb) && (Cntrl_Pc >= MAX_Pc))
    {
        if(Real_Pc >= Real_Pb)
        {
            //B 采样最小
            base_phase = 2;
        }
        else
        {
            //C 采样最小
            base_phase = 3;
        }
    }
    //仅一相不限功率
    else if(Cntrl_Pa >= MAX_Pa)
    {
        //A 采样最小
        base_phase = 1;
    }
    else if(Cntrl_Pb >= MAX_Pb)
    {
        //B 采样最小
        base_phase = 2;
    }
    else if(Cntrl_Pc >= MAX_Pc)
    {
        //C 采样最小
        base_phase = 3;
    }

    return base_phase;
}
//获取各相实际功率 并算出各相的控制功率  PV_POWER--电池板的最大功率=350.0w
void process_my_cntrl_power(void)
{
    vu8 i = 0;
    vu8 my_base_phase = 0;
    vu32 MAX_Pa = 0;
    vu32 MAX_Pb = 0;
    vu32 MAX_Pc = 0;
    vu32 temp_dt1 = 0;
    vu32 temp_dt2 = 0;
    MAX_Pa = Dtu3Detail.Property.Reflux_DtuPanelNum_A * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
    MAX_Pb = Dtu3Detail.Property.Reflux_DtuPanelNum_B * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
    MAX_Pc = Dtu3Detail.Property.Reflux_DtuPanelNum_C * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
    //计算各相功率总和
    get_real_power();

    //三相都是未限功率  =  没有基准的时候
    if((Cntrl_Pa < MAX_Pa) && (Cntrl_Pb < MAX_Pb) && (Cntrl_Pc < MAX_Pc))      //  A B C
    {
        temp_dt1 = get_min(MAX_Pa, MAX_Pb, MAX_Pc); //取实际最小控制相 开全功率
        Cntrl_Pa = temp_dt1;
        Cntrl_Pb = temp_dt1;
        Cntrl_Pc = temp_dt1;
    }
    ///////////////////////////////////////////////////////////////////////
    //判断最小相 并以此给出 控制功率 CBA BCA
    else
    {
        //获取基准相
        my_base_phase = get_my_control_base();

        if((Real_Pc >= Real_Pa) && (Real_Pb >= Real_Pa)) //实际值A相最小
        {
            //A相最小
            if(Cntrl_Pa >= MAX_Pa)   //并且A相不限功率  基准A
            {
                //A相
                Cntrl_Pa  = MAX_Pa; //每个组件可以限最大功率-->不限 1位小数
                //B相
                Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pa, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
                //C相
                Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pa, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
            }
            else //A相未限到最大值
            {
                if(my_base_phase == 2) // 2 3
                {
                    temp_dt1 = Real_Pb - Real_Pa;
                    Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pb, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
                    Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pb, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                }
                else
                {
                    temp_dt1 = Real_Pc - Real_Pa;
                    Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pc, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
                    Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pc, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                }
            }
        }

        //ACB  CAB
        if((Real_Pa >= Real_Pb) && (Real_Pc >= Real_Pb))
        {
            //B相最小
            if(Cntrl_Pb >= MAX_Pb)   //b相切到最大值
            {
                //B相
                Cntrl_Pb = MAX_Pb;
                //A相
                Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pb, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                //C相
                Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pb, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
            }
            else //b相未限到最大值
            {
                if(my_base_phase == 1) //1 3
                {
                    temp_dt1 = Real_Pa - Real_Pb;
                    Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pa, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
                    Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pa, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
                }
                else
                {
                    temp_dt1 = Real_Pc - Real_Pb;
                    Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pc, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                    Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pc, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
                }
            }
        }

        //ABC BAC
        if((Real_Pa >= Real_Pc) && (Real_Pb >= Real_Pc))
        {
            //C相最小
            if(Cntrl_Pc >= MAX_Pc)   //c相切到最大值
            {
                //C相
                Cntrl_Pc = MAX_Pc;
                //A相
                Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pc, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                //B相
                Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pc, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
            }
            else //c相未限到最大值
            {
                if(my_base_phase == 1)  //1 2
                {
                    temp_dt1 = Real_Pa - Real_Pb;
                    Cntrl_Pb = get_my_cntrl_power(Cntrl_Pb, Real_Pb, Real_Pa, nub_power_phb, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B);
                    Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pa, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
                }
                else
                {
                    temp_dt1 = Real_Pc - Real_Pb;
                    Cntrl_Pa = get_my_cntrl_power(Cntrl_Pa, Real_Pa, Real_Pb, nub_power_pha, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A);
                    Cntrl_Pc = get_my_cntrl_power(Cntrl_Pc, Real_Pc, Real_Pb, nub_power_phc, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C);
                }
            }
        }
    }

    //并且A相不限功率
    if(Cntrl_Pa >= MAX_Pa)
    {
        Cntrl_Pa = MAX_Pa;
    }

    //并且B相不限功率
    if(Cntrl_Pb >= MAX_Pb)
    {
        Cntrl_Pb = MAX_Pb;
    }

    //并且C相不限功率
    if(Cntrl_Pc >= MAX_Pc)
    {
        Cntrl_Pc = MAX_Pc;
    }

    //是否全部关掉
    if_trouble_phase();
    //是否有单相全部离线  ---不限功率
    if_offline_phase();
}

void LWHV_Process(void)
{
    //三相平衡系统
    if(MI_UPDATE != downfile_module)  //MI升级程序的时候 不控制
    {
        //三相平衡 计算一次控制功率
        if(LocalTime > (Anti_LWHV_Time + (Dtu3Detail.Property.PortNum * CONTROL_INTERVAL_TIME)))
        {
            //////////////////////
            Anti_LWHV_Time = LocalTime;

            //三相平衡控制 开
            if(Dtu3Detail.Property.Phase_Balance_Switch == 1)
            {
                three_phase_control_times = 0;
                phase_balance_switch_last = 1;
                //获取控制功率 Cntrl_Pa Cntrl_Pb Cntrl_Pc
                process_my_cntrl_power();
                power_control(Absolute_Value_Control, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A, Cntrl_Pa, PH_A);
                power_control(Absolute_Value_Control, Dtu3Detail.Property.Reflux_DtuPanelNum_B, Dtu3Detail.Property.Reflux_SysPanelNum_B, Cntrl_Pb, PH_B);
                power_control(Absolute_Value_Control, Dtu3Detail.Property.Reflux_DtuPanelNum_C, Dtu3Detail.Property.Reflux_SysPanelNum_C, Cntrl_Pc, PH_C);
            }
            else if((phase_balance_switch_last == 1) &&
                    (Dtu3Detail.Property.Phase_Balance_Switch == 0))
                //三相平衡控制 关，或者不控制-->控制到最大功率
            {
                //控制2遍 2 3 4
                if(three_phase_control_times <= 4)
                {
                    three_phase_control_times++;
                    Cntrl_Pa = Dtu3Detail.Property.Reflux_DtuPanelNum_A * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
                    Cntrl_Pb = Dtu3Detail.Property.Reflux_DtuPanelNum_B * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
                    Cntrl_Pc = Dtu3Detail.Property.Reflux_DtuPanelNum_C * MY_MAX_PORT_LIMIT;//(MY_MAX_PORT_LIMIT / 10);
                    power_control(Absolute_Value_Control, Dtu3Detail.Property.Reflux_DtuPanelNum_A, Dtu3Detail.Property.Reflux_SysPanelNum_A, (Cntrl_Pa + Cntrl_Pb + Cntrl_Pc), PH_NULL);  //统一控制到最大
                }
            }
        }
    }
}

void Power_Sweep_Control(void)
{
    vu8 i = 0;
    vu32 p_temp = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        //限功率 实时控制 功率
        if(ANTI_BACKFLOW_1000W(i))
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
                p_temp = p_temp + (u32)((MIReal[i + 1].Data.PVPower[0] << 8) + MIReal[i + 1].Data.PVPower[1]);
                p_temp = p_temp + (u32)((MIReal[i + 2].Data.PVPower[0] << 8) + MIReal[i + 2].Data.PVPower[1]);
                p_temp = p_temp + (u32)((MIReal[i + 3].Data.PVPower[0] << 8) + MIReal[i + 3].Data.PVPower[1]);
            }
            else
            {
                p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
                p_temp = p_temp + (u32)((MIReal[i + 1].Data.Power[0] << 8) + MIReal[i + 1].Data.Power[1]);
                p_temp = p_temp + (u32)((MIReal[i + 2].Data.Power[0] << 8) + MIReal[i + 2].Data.Power[1]);
                p_temp = p_temp + (u32)((MIReal[i + 3].Data.Power[0] << 8) + MIReal[i + 3].Data.Power[1]);
            }
        }
        else if(ANTI_BACKFLOW_500W(i))
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
                p_temp = p_temp + (u32)((MIReal[i + 1].Data.PVPower[0] << 8) + MIReal[i + 1].Data.PVPower[1]);
            }
            else
            {
                p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
                p_temp = p_temp + (u32)((MIReal[i + 1].Data.Power[0] << 8) + MIReal[i + 1].Data.Power[1]);
            }
        }
        else
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
            {
                p_temp = (u32)((MIReal[i].Data.PVPower[0] << 8) + MIReal[i].Data.PVPower[1]);
            }
            else
            {
                p_temp = (u32)((MIReal[i].Data.Power[0] << 8) + MIReal[i].Data.Power[1]);
            }
        }

        //发限功率命令
        if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
        {
            //差值超过4后
            if((p_temp > (MIMajor[i].Property.Power_Limit  + CTRL_DIFF)) || ((p_temp + CTRL_DIFF) < MIMajor[i].Property.Power_Limit))
            {
                //发限功率命令
                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
            }
        }
    }
}

void Relative_Power_Sweep_Control(void)
{
    vu8 i = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(PORT_NUMBER_CONFIRMATION(i))
        {
            //发限功率命令
            if((MIReal[i].Data.NetStatus == NET_NOCMD) ||
                    (MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) ||
                    (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
            {
                //发限功率命令
                MIReal[i].Data.NetCmd = NET_LIMIT_POEWR;
                MIReal[i].Data.NetStatus = LOCAL_NOT_EXECUTED;
            }
        }
    }
}