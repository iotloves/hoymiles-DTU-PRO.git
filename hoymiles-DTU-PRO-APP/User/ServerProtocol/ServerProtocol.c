#include "ServerProtocol.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#include "TimeProcessing.h"
#include "gprs.h"
#include "crc16.h"
#include "NetProtocol.h"
#include "rtc.h"
#include "Memory.h"
//服务器流水号
vu8 sever_serial_nub;
//DTU流水号
vu8 dtu_serial_nub;
//命令控制码
vu8 server_command;
//回复命令控制码
vu8 re_server_command;
//透传帧控制命令
vu8 command_kind;
//透传帧错误标志位
vu8 error_kind;
//时区
vu16 time_zone_select;
//逆变器主要信息
//逆变器实时数据
extern vu32 mi_self_check_status;
extern volatile InverterReal MIReal[PORT_LEN];
extern volatile InverterMajor MIMajor[PORT_LEN];//;逆变器主要信息
extern volatile InverterDetail MIDetail;//微型逆变器详细信息
extern volatile DtuMajor Dtu3Major;//DTU主要信息
extern volatile DtuDetail Dtu3Detail;//DTU详细信息
extern SelfCheckParaType SelfCheckPara;
extern vu32 storge_historical_data_flg;
//一包数据组件的个数
extern vu8 ONEC_SOCKET_SEND_DEVICE;
//C-S的当前包数
extern vu8 package_to_server;
//当前请求帧数
extern vu8 PackageNow;
//DTU网络命令
extern vu8 Command;
//socket状态
extern vu32 socket_state;
extern vu16 ReceivingSign;
// 下载ip
extern vu8 up_pro_destip[4];
//端口号
extern vu16 up_pro_destport;
//文件名
extern vu8 up_pro_filename[ROW_MAX_LEN];
//URL
extern vu8 up_pro_url[ROW_MAX_LEN];
extern vu8 restart_dtu;
extern vu8 McuAsciiToHex(u8 ascii);
extern void change_MI_id(u8 *source, u8 *pro_id, u8 *id);

/*暂时支持 组网，数据，心跳，信息，采集器升级*/
/***********************************************
** Function name:       pb_receive
** Descriptions:        服务器总接收
** input parameters:    *buffer 数据
** output parameters:   无
** Returned value:      无
*************************************************/
void Process_Server_Data(char *buffer)
{
    volatile calendar_obj calendar;
    vu8 temp_buff[100] = {0};
    volatile char temp_file[ROW_MAX_LEN] = {0};
    vu8 command = 0;
    vu16 i, j, k, n, sum, len;
    u8  p_id[2];//pre_id
    u8  t_id[4];//id
    u8 m_temp[4] = {0};
    //长度
    len = (u16)(buffer[1] + (buffer[2] << 8));

    //判断头尾
    if((0xA5 == buffer[0]) && (0x15 == buffer[len + 18]))
    {
        //透传命令长度
        //AT_len = buffer[26] + (buffer[27]<<8);
        //校验
        sum = buffer[len + 17];

        if(package_crc((u8 *)&buffer[1], len + 16) == sum)
        {
            //服务器流水号
            sever_serial_nub = buffer[SOCKET_SERVER_NUB_ADR];
            //低四位命令
            server_command = (buffer[4] & 0x0f);
#ifdef DEBUG
            printf("CSQ:%d\r\n", Dtu3Major.Property.GPRS_CSQ);
#endif

            //透传帧
            if(TRANSPARENT_FRAME == server_command)
            {
                //回复服务器的控制码
                re_server_command = 0x15;

                if((Dtu3Major.Property.Id[0] == buffer[10]) && (Dtu3Major.Property.Id[1] == buffer[9]) && (Dtu3Major.Property.Id[2] == buffer[8])\
                        && (Dtu3Major.Property.Id[3] == buffer[7]) && (Dtu3Major.Property.Pre_Id[0] == buffer[12]) && (Dtu3Major.Property.Pre_Id[1] == buffer[11]))
                {
                    //回复服务器
                    // socket_state |= NET_COMMAND_PACK;
                    //服务器控制命令
                    ReceivingSign = TAG_CommandRes;
                    //透传帧控制命令
                    command_kind = buffer[SOCKET_DAT_CMD_ADR];
                    //时间位置 透传帧时间位置在18位
                    i = 18;
                    calendar.w_year              = ((u16)buffer[i++] + 2000);
                    calendar.w_month             = (buffer[i++] % 0x13);
                    calendar.w_date              = (buffer[i++] % 0x32);
                    calendar.hour                = (buffer[i++] % 24);
                    calendar.min                 = (buffer[i++] % 60);
                    calendar.sec                 = (buffer[i++] % 60);
                    //低8位
                    time_zone_select = buffer[i++];
                    //高8位
                    time_zone_select = time_zone_select + buffer[i++] * 256;
                    NetTime_TO_RTC(calendar);

                    //单ID
                    if(0x01 == command_kind)
                    {
                    }
                    //多ID
                    else if(0x02 == command_kind)
                    {
                        i = SOCKET_DATID_CMD_ADR;
                        k = 0;
                        j = 27 + (buffer[26] * 6);
                        k = j + 2;

                        while((buffer[i] != 0x15) && (buffer[i] != '\r') && (i < (len + 16)))
                        {
                            i = k;

                            if(strncmp((char *)&buffer[i], "AT+RESET", 8) == 0)
                            {
                                command = 0x02;
                                break;
                            }

                            //并网保护文件自检
                            if(strncmp((char *)&buffer[i], "AT+IPS=", 7) == 0)
                            {
                                //每次下发删除自检状态存储
                                GPST_Data_Delete();
                                mi_self_check_status = LocalTime;
                                memcpy(m_temp, &buffer[i + 7], 4);
                                get_form_value_4hex(&m_temp[0], &SelfCheckPara.Ver[0]);
                                memset(m_temp, 0, sizeof(m_temp));
                                memcpy(m_temp, &buffer[i + 12], 4);
                                get_form_value_4hex(&m_temp[0], &SelfCheckPara.GPF[0]);

                                for(k = 0; k < buffer[26]; k++)
                                {
                                    t_id[0] = buffer[27 + 3 + (6 * k)];
                                    t_id[1] = buffer[27 + 2 + (6 * k)];
                                    t_id[2] = buffer[27 + 1 + (6 * k)];
                                    t_id[3] = buffer[27 + 0 + (6 * k)];
                                    p_id[0] = buffer[27 + 5 + (6 * k)];
                                    p_id[1] = buffer[27 + 4 + (6 * k)];
                                    i = search_adr_for_link(&t_id[0], &p_id[0], Dtu3Detail.Property.PortNum);

                                    if(i < Dtu3Detail.Property.PortNum)
                                    {
                                        MIReal[i].Data.NetCmd = NET_SELF_INSPECTION;
                                        MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                                    }
                                }

                                break;
                            }

                            //                                                      //7：开机
                            //                            else if(strncmp((char *)&buffer[i], "AT+POWER=ON", 11) == 0)
                            //                            {
                            //                                command = 0x07;
                            //                                break;
                            //                            }
                            //                                                      //6：关机
                            //                            else if(strncmp((char *)&buffer[i], "AT+POWER=OFF", 12) == 0)
                            //                            {
                            //                                command = 0x06;
                            //                                break;
                            //                            }
                            //                                                      //d: 清楚接地故障
                            //                            else if(strncmp((char *)&buffer[i], "AT+CLEARGFDI", 12) == 0)
                            //                            {
                            //                                command = 0x0d;//GDFAULT;
                            //                                break;
                            //                            }
                            //                                                      //更新配置文件
                            //                            else if(strncmp((char *)&buffer[i], "AT+UPURL=2", 10) == 0)
                            //                            {
                            //                                command = 0x08;
                            //                                //拿  文件路径和文件名
                            //                                k = buffer[i - 1] << 8;
                            //                                k = k + buffer[i - 2];
                            //                                if(k > MAX_PRO_PACKAGE_ROW * 2)
                            //                                {
                            //                                    error_kind = 0;
                            //                                    k = MAX_PRO_PACKAGE_ROW;
                            //                                }
                            //                                i = i + 10;
                            //                                k = 0;
                            //                                n = 0;
                            //                                memset(temp_file, 0, sizeof(temp_file));
                            //                                memset(up_pro_destip, 0, sizeof(up_pro_destip));
                            //                                memset(up_pro_url, 0, sizeof(up_pro_url));
                            //                                memset(up_pro_filename, 0, sizeof(up_pro_filename));
                            //                                while((buffer[i] != 0x15) && (i < (len + 16)))
                            //                                {
                            //                                    i++;
                            //                                    if(buffer[i] == 0x15)
                            //                                    {
                            //                                        break;
                            //                                    }
                            //                                    if(buffer[i] != ',')
                            //                                    {
                            //                                        memset(temp_buff, 0, sizeof(temp_buff));
                            //                                        for(j = 0; j < MAX_PRO_PACKAGE_ROW; j++) //路径和文件名
                            //                                        {
                            //                                            temp_buff[j] = buffer[i];
                            //                                            i++;
                            //                                            n++;
                            //                                            if(buffer[i] == ',')
                            //                                            {
                            //                                                break;
                            //                                            }
                            //                                            if(buffer[i] == '\r')
                            //                                            {
                            //                                                break;
                            //                                            }
                            //                                        }
                            //                                        if(k == 0)                                     //URL
                            //                                        {
                            //                                            memcpy(&temp_file, &buffer[i - n], n);
                            //                                            process_ip_prot(temp_file, up_pro_destip, up_pro_url, &up_pro_destport);
                            //                                            n = 0;
                            //                                        }
                            //                                        if(k == 1)                                     //file name
                            //                                        {
                            //                                            strncpy((char *)&up_pro_filename[strlen((char *)up_pro_filename)], (char *)&buffer[i - n], n);
                            //                                        }
                            //                                        k++;
                            //                                        if(k > 1)
                            //                                        {
                            //                                            //                                            Tcp_Http_Flg = 1;        //置下载标志位
                            //                                            //                                            Http_config_file = 1; //并网保护文件标志位
                            //                                            break;
                            //                                        }
                            //                                    }
                            //                                }
                            //                                break;
                            //                            }
                            //                        }
                            //                        for(k = 0; k < buffer[26]; k++)
                            //                        {
                            //                            t_id[0] = buffer[27 + 3 + (6 * k)];
                            //                            t_id[1] = buffer[27 + 2 + (6 * k)];
                            //                            t_id[2] = buffer[27 + 1 + (6 * k)];
                            //                            t_id[3] = buffer[27 + 0 + (6 * k)];
                            //                            temp = search_adr_for_link(&t_id[0], wnb_link_nub);
                            //                            if(temp < wnb_link_nub)
                            //                            {
                            //                                DTU_All_Status[temp].parasent_command = command;
                            //                            }
                        }
                    }
                    //网关
                    else if(0x03 == command_kind)
                    {
                        //更新DTU程序
                        if(strncmp((char *)&buffer[SOCKET_DATID_CMD_ADR], "AT+UPURL=1", 10) == 0)
                        {
                            i = SOCKET_DATID_CMD_ADR + 10;
                            k = 0;
                            n = 0;
                            memset((char *)temp_file, 0, sizeof(temp_file));
                            memset((u8 *)up_pro_destip, 0, sizeof(up_pro_destip));
                            memset((u8 *)up_pro_url, 0, sizeof(up_pro_url));
                            memset((u8 *)up_pro_filename, 0, sizeof(up_pro_filename));

                            while((buffer[i] != 0x15) && (i < (len + 16)))
                            {
                                i++;

                                if(buffer[i] == 0x15)
                                {
                                    break;
                                }

                                if(buffer[i] != ',')
                                {
                                    memset((u8 *)temp_buff, 0, sizeof(temp_buff));

                                    //路径和文件名
                                    for(j = 0; j < 50; j++)
                                    {
                                        temp_buff[j] = buffer[i];
                                        i++;
                                        n++;

                                        if(buffer[i] == ',')
                                        {
                                            break;
                                        }

                                        if(buffer[i] == '\r')
                                        {
                                            break;
                                        }
                                    }

                                    if(k == 0)
                                    {
                                        memcpy((char *)&temp_file, &buffer[i - n], n);
                                        process_Ip_Prot((char *)temp_file, (u8 *)up_pro_destip, (u8 *)up_pro_url, (u16 *)&up_pro_destport);
                                        n = 0;
                                    }

                                    if(k == 1)
                                    {
                                        memcpy((u8 *)&up_pro_filename, &buffer[i - n], n);
                                    }

                                    k++;

                                    if(k > 1)
                                    {
                                        break;
                                    }
                                }
                            }

                            Command = Cmd_DTUUpgrade;
                        }
                        else
                        {
                            //删除原有的，再增加命令中后面的ID
                            if(strncmp((char *)&buffer[SOCKET_DATID_CMD_ADR], "AT+YZSETID=s", 12) == 0)
                            {
                                Network_Function_Reset();
                                //                                UsartNrf_ClearMIReal();
                                //                                UsartNrf_ClearInverterMajor();
                                //                                Dtu3Detail.Property.PortNum = 0;
                                i = 40;
                                k = 0;

                                while((buffer[i] != 0x15) && (i < (len + 16)))
                                {
                                    if(buffer[i] == 0x15)
                                    {
                                        break;
                                    }

                                    //有多少个,就有多少个id
                                    if(buffer[i] == ',')
                                    {
                                        i++;
                                        memset((u8 *)temp_buff, 0, 12);

                                        //微逆号
                                        for(j = 0; j < 12; j++)
                                        {
                                            temp_buff[j] = buffer[i];
                                            i++;

                                            if(buffer[i] == ',')
                                            {
                                                break;
                                            }
                                        }

                                        //赋值ID
                                        change_MI_id((u8 *)&temp_buff[0], (u8 *)&MIMajor[k].Property.Pre_Id[0], (u8 *)&MIMajor[k].Property.Id[0]);
                                        k++;

                                        if(k >= (50))
                                        {
                                            break;
                                        }
                                    }
                                }

                                Dtu3Detail.Property.PortNum = k;
                                //给微逆赋值组网命令
                                MIReal[0].Data.NetCmd = NET_REGISTER_ID;
                                MIReal[0].Data.NetStatus = NET_NOT_EXECUTED;
                            }
                            //复位DTU
                            else if(strncmp((char *)&buffer[SOCKET_DATID_CMD_ADR], "AT+DTURESET", 10) == 0)
                            {
                                //暂停喂狗
                                restart_dtu = 1;
                            }
                        }
                    }
                }
            }
            //信息帧
            else if(INFORMATION_FRAME == server_command)
            {
                //回复错误代码(无错误)
                if((0x00 == buffer[28]) && (0x00 == buffer[29]))
                {
                    PackageNow = (buffer[SOCKET_PS_ADR] - 1);
                    //信息帧应答
                    ReceivingSign = TAG_InfoDataRes;
                    i = SOCKET_DATE_ADR;
                    calendar.w_year              = ((u16)buffer[i++] + 2000);
                    calendar.w_month             = (buffer[i++] % 0x13);
                    calendar.w_date              = (buffer[i++] % 0x32);
                    calendar.hour                = (buffer[i++] % 24);
                    calendar.min                 = (buffer[i++] % 60);
                    calendar.sec                 = (buffer[i++] % 60);
                    //低8位
                    time_zone_select = buffer[i++];
                    //高8位
                    time_zone_select = time_zone_select + buffer[i++] * 256;
                    //设置RTC
                    NetTime_TO_RTC(calendar);
                }
            }
            //数据帧
            else if(DATA_FRAME == server_command)
            {
                //回复错误代码(无错误)
                if((0x00 == buffer[28]) && (0x00 == buffer[29]))
                {
                    //帧数
                    PackageNow = (buffer[SOCKET_PS_ADR] - 1);

                    if(0x01 == buffer[17]) //实时数据
                    {
                        //实时数据应答
                        ReceivingSign = TAG_RealDataRes;
                    }
                    //历史数据
                    else if(0x81 == buffer[17])
                    {
                        //历史数据应答
                        ReceivingSign = TAG_HistoryDataRes;
                    }

                    i = SOCKET_DATE_ADR;
                    calendar.w_year              = ((u16)buffer[i++] + 2000);
                    calendar.w_month             = (buffer[i++] % 0x13);
                    calendar.w_date              = (buffer[i++] % 0x32);
                    calendar.hour                = (buffer[i++] % 24);
                    calendar.min                 = (buffer[i++] % 60);
                    calendar.sec                 = (buffer[i++] % 60);
                    //低8位
                    time_zone_select = buffer[i++];
                    //高8位
                    time_zone_select = time_zone_select + buffer[i++] * 256;
                    NetTime_TO_RTC(calendar);
                }
            }
            //心跳帧
            else if(HEARTBEAT_FRAME == server_command)
            {
                //心跳帧应答
                ReceivingSign = TAG_HBRes;
#ifdef DEBUG
                printf("HEARTBEAT-RECEIVE\n\r");
#endif
            }
            //自检状态应答
            else if(GRID_CHECK_STATUS == server_command)
            {
                ReceivingSign = TAG_CommandStatusRes;
            }
            else
            {
            }
        }
    }

    memset(buffer, 0, sizeof(*buffer));
}

/***********************************************
** Function name:
** Descriptions:        服务器IP、URL和端口号解析
** input parameters:    需要解析的下载链接，IP,URL,port
** output parameters:   无
** Returned value:      无
*************************************************/
void process_Ip_Prot(char *buff, u8 *ip, u8 *url, u16 *port)
{
    vu16 i, temp, n, j, len;
    i = 0;
    j = 0;
    temp = 0;
    len = 0;

    while(i < 50)
    {
        *port = 0;

        if(strncmp((char *)&buff[i], "http://", 7) == 0)
        {
            i = i + 7;

            //分割IP
            for(j = 0; j < 4; j++)
            {
                ip[j] = 0;

                for(n = 0; n < 3; n++)
                {
                    if(buff[i] == ':')
                    {
                        break;
                    }

                    if(buff[i] == '.')
                    {
                        i++;
                        break;
                    }

                    ip[j] = ip[j] * 10;
                    temp = 0;

                    if(buff[i] >= 0x30)
                    {
                        temp = buff[i] - 0x30;
                    }

                    ip[j] = ip[j] + (u8)temp;
                    i++;
                }

                if(buff[i] == ':')
                {
                    break;
                }

                if(n >= 3)
                {
                    i++;
                }
            }

            //端口号
            while(i < 50)
            {
                if(buff[i] == ':')
                {
                    i++;
                    *port = 0;

                    for(n = 0; n < 5; n++)
                    {
                        if(buff[i] == '/')
                        {
                            // i++;
                            break;
                        }

                        *port = *port * 10;
                        temp = 0;

                        if(buff[i] >= 0x30)
                        {
                            temp = buff[i] - 0x30;
                        }

                        *port = *port + temp;
                        i++;
                    }

                    break;
                }
                else
                {
                    i++;
                }
            }

            len = (u16)(strlen((char *)buff) - i);
            memcpy(url, &buff[i], len);
            break;
        }

        i++;
    }
}



/***********************************************
** Function name:
** Descriptions:        回复透传帧
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u16 Reply_Server_Command(u8 *dat, u8 packager_now)
{
    vu32 time = 0;
    volatile calendar_obj calendar;
    vu8 i = 0;
    //start
    *(dat + (i++)) = 0xa5;
    //data len l
    *(dat + (i++)) = 0x00;
    //data len h
    *(dat + (i++)) = 0x00;
    //command
    //control c1
    *(dat + (i++)) = COMMAND_C1;
    //control c2
    *(dat + (i++)) = TRANSPARENT_FRAME | 0x10;
    //ls 服务器
    *(dat + (i++)) = sever_serial_nub;
    //ls DTU 流水号
    *(dat + (i++)) = DTU_SERIAL_BUB(dtu_serial_nub);
    //DTU id
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    //DN L  共多少设备
    *(dat + (i++)) = (u8)Dtu3Detail.Property.PortNum;
    //DN H
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum >> 8);
    //PN共多少帧   最大25个ID一帧
    *(dat + (i++)) = 1;
    //PS  第几帧
    *(dat + (i++)) = 1;
    //对网关的透传帧
    *(dat + (i++)) = command_kind;

    if(error_kind == 0)
    {
        *(dat + (i++)) = 1;
    }
    else
    {
        *(dat + (i++)) = 0;
    }

    //获取时间
    time = RTC_Getsecond();
    SecToDate(time, (calendar_obj *)&calendar);
    *(dat + (i++)) = (u8)(calendar.w_year % 100);
    *(dat + (i++)) = calendar.w_month;
    *(dat + (i++)) = calendar.w_date;
    *(dat + (i++)) = calendar.hour;
    *(dat + (i++)) = calendar.min;
    *(dat + (i++)) = calendar.sec;
    //time zone
    *(dat + (i++)) = (u8)time_zone_select;
    *(dat + (i++)) = (u8)(time_zone_select >> 8);

    if(error_kind == 0)
    {
        //cmd len L
        *(dat + (i++)) = 5;
        //cmd len h
        *(dat + (i++)) = 0;
        *(dat + (i++)) = '+';
        *(dat + (i++)) = 'o';
        *(dat + (i++)) = 'k';
        *(dat + (i++)) = 0x0d;
        *(dat + (i++)) = 0x0a;
    }
    else
    {
        *(dat + (i++)) = 6;
        *(dat + (i++)) = 0;
        *(dat + (i++)) = '+';
        *(dat + (i++)) = 'e';
        *(dat + (i++)) = 'r';
        *(dat + (i++)) = 'r';
        *(dat + (i++)) = 0x0d;
        *(dat + (i++)) = 0x0a;
    }

    //data len L
    *(dat + 1) = (u8)(i - 17);
    //data len H
    *(dat + 2) = (u8)((i - 17) >> 8);
    *(dat + i) = package_crc((dat + 1), (i - 1));
    i++;
    //end
    *(dat + (i++)) = 0x15;
    return(i);
}


/***********************************************
** Function name:
** Descriptions:        信息帧 开机第一次传送。
** input parameters:    总包数,第几包,时间,当前包MI数量,相对偏移量,发送buff,微逆数据
** output parameters:   无
** Returned value:      包的长度
*************************************************/
u16 Pack_Information(u8 package_nub, u8 package_now, u32 time, u16 RpCount, u16 RpOff, u8 mi_nub, u16 mi_offset, u8 *dat, InverterDetail *mInverterDetail)
{
    vu16 i = 0;
    vu16 j = 0;
    volatile calendar_obj calendar;
    //start
    *(dat + (i++)) = 0xa5;
    //data len l
    *(dat + (i++)) = 0x00;
    //data len h
    *(dat + (i++)) = 0x00;
    //command
    //control c1
    *(dat + (i++)) = COMMAND_C1; //control c1
    *(dat + (i++)) = INFORMATION_FRAME | 0x40; //control c2      bit7 无后续帧   bit6 是否需要应答
    //ls 服务器
    *(dat + (i++)) = sever_serial_nub;
    //ls DTU 流水号
    *(dat + (i++)) = DTU_SERIAL_BUB(dtu_serial_nub);
    //DTU id
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    *(dat + (i++)) = (u8)Dtu3Detail.Property.PortNum;
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum >> 8);
    // PN  共多少帧   最大20个ID一帧
    *(dat + (i++)) = package_nub;
    //PS  第几帧
    *(dat + (i++)) = package_now + 1;
    //总信息
    *(dat + (i++)) = 0x0a;
    //获取时间
    SecToDate(time, (calendar_obj *)&calendar);
    *(dat + (i++)) = (u8)(calendar.w_year % 100);
    *(dat + (i++)) = calendar.w_month;
    *(dat + (i++)) = calendar.w_date;
    *(dat + (i++)) = calendar.hour;
    *(dat + (i++)) = calendar.min;
    *(dat + (i++)) = calendar.sec;
    //time zone
    *(dat + (i++)) = (u8)time_zone_select;
    *(dat + (i++)) = (u8)(time_zone_select >> 8);
    //当前包MI数量
    *(dat + (i++)) = mi_nub;
    //DTU信息    DATA_X
    *(dat + (i++)) = 0x01;
    //DTU id
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    //DTU硬件版本号
    *(dat + (i++)) = Dtu3Major.Property.DtuHw_Ver[1];
    *(dat + (i++)) = Dtu3Major.Property.DtuHw_Ver[0];
    //DTU软件版本号
    *(dat + (i++)) = Dtu3Major.Property.DtuSw_Ver[1];
    *(dat + (i++)) = Dtu3Major.Property.DtuSw_Ver[0];
    //DTU_RF硬件版本号
    *(dat + (i++)) = Dtu3Major.Property.RfHw_Ver[3];
    *(dat + (i++)) = Dtu3Major.Property.RfHw_Ver[2];
    //DTU_RF软件版本号
    *(dat + (i++)) = Dtu3Major.Property.RfFw_Ver[3];
    *(dat + (i++)) = Dtu3Major.Property.RfFw_Ver[2];

    for(j = 0; j < mi_nub; j++)
    {
        //数据类型
        *(dat + (i++)) = 0x02;
        //mi    id
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[3];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[2];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[1];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[0];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Pre_Id[1];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Pre_Id[0];
        //硬件料号
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_PNL);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_PNL >> 8);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_PNH);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_PNH >> 8);
        //硬件版本
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_VER);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.HW_VER >> 8);
        //软件料号
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFW_PNL);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFW_PNL >> 8);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFW_PNH);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFW_PNH >> 8);
        //软件版本
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFWBuild_VER);
        *(dat + (i++)) = (u8)(mInverterDetail[j].Property.AppFWBuild_VER >> 8);
        //RF硬件版本
        *(dat + (i++)) = mInverterDetail[j].Property.NRF_HardVersion[3];
        *(dat + (i++)) = mInverterDetail[j].Property.NRF_HardVersion[2];
        //RF软件版本
        *(dat + (i++)) = mInverterDetail[j].Property.NRF_SoftVersion[3];
        *(dat + (i++)) = mInverterDetail[j].Property.NRF_SoftVersion[2];
        //V1.0.23增加  数据类型改为05
        //        //并网保护文件代码
        //        *(dat + (i++)) = mInverterDetail[j].Property.GPFCode;
        //        *(dat + (i++)) = mInverterDetail[j].Property.GPFCode >> 8;
        //        //并网保护文件版本
        //        *(dat + (i++)) = mInverterDetail[j].Property.GPFVer;
        //        *(dat + (i++)) = mInverterDetail[j].Property.GPFVer >> 8;
    }

    //data len L
    *(dat + 1) = (u8)(i - 17);
    //data len H
    *(dat + 2) = (u8)((i - 17) >> 8);
    //check sum
    *(dat + i) = package_crc((dat + 1), (i - 1));
    i++;
    //end
    *(dat + (i++)) = 0x15;
    return(i);
}

/***********************************************
** Function name:
** Descriptions:        数据帧。
** input parameters:    发送buff
** input parameters:    DTU 流水号
** input parameters:    第几帧
** output parameters:   无
** Returned value:      无
*************************************************/
u16 Pack_Realdata(u8 package_nub, u8 package_now, u32 time, u16 RpCount, u16 RpOff, u8 mi_nub, u16 mi_offset, u8 *dat)
{
    volatile calendar_obj calendar;
    vu16 i;
    vu16 j;
    i = 0;
    j = 0;
    //start
    *(dat + (i++)) = 0xa5;
    //data len l
    *(dat + (i++)) = 0x00;
    //data len h
    *(dat + (i++)) = 0x00;
    //command
    //control c1
    *(dat + (i++)) = COMMAND_C1;
    //control c2
    *(dat + (i++)) = DATA_FRAME | 0x40;
    //ls 服务器
    *(dat + (i++)) = sever_serial_nub;
    //ls DTU 流水号
    *(dat + (i++)) = DTU_SERIAL_BUB(dtu_serial_nub);
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum);
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum >> 8);
    // PN  共多少帧   最大20个ID一帧
    *(dat + (i++)) = package_nub;
    //PS  第几帧
    *(dat + (i++)) = package_now + 1;
    //发送实时数据
    *(dat + (i++)) = 0x01;
    //time stamp;
    //获取时间
    SecToDate(time, (calendar_obj *)&calendar);
    *(dat + (i++)) = (u8)(calendar.w_year % 100);
    *(dat + (i++)) = calendar.w_month;
    *(dat + (i++)) = calendar.w_date;
    *(dat + (i++)) = calendar.hour;
    *(dat + (i++)) = calendar.min;
    *(dat + (i++)) = calendar.sec;
    //time zone
    *(dat + (i++)) = (u8)(time_zone_select);
    *(dat + (i++)) = (u8)(time_zone_select >> 8);
    //当前帧发送的设备数
    *(dat + (i++)) = mi_nub;

    //微逆数据
    for(j = 0; j < mi_nub; j++)
    {
        *(dat + (i++)) = 0x0a;
        //mi    id
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[3];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[2];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[1];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Id[0];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Pre_Id[1];
        *(dat + (i++)) = MIMajor[j + mi_offset].Property.Pre_Id[0];

        if(MIMajor[j + mi_offset].Property.Port == MI_1000W_D)
        {
            *(dat + (i++)) = 4;
        }
        else if(MIMajor[j + mi_offset].Property.Port == MI_1000W_C)
        {
            *(dat + (i++)) = 3;
        }
        else if(MIMajor[j + mi_offset].Property.Port == MI_1000W_B)
        {
            *(dat + (i++)) = 2;
        }
        else if(MIMajor[j + mi_offset].Property.Port == MI_1000W_A)
        {
            *(dat + (i++)) = 1;
        }
        else if(MIMajor[j + mi_offset].Property.Port == MI_500W_B)
        {
            *(dat + (i++)) = 2;
        }
        else if(MIMajor[j + mi_offset].Property.Port == MI_500W_A)
        {
            *(dat + (i++)) = 1;
        }
        //250
        else
        {
            *(dat + (i++)) = 0;
        }

        //MI PV 电压
        *(dat + (i++)) = MIReal[j + mi_offset].Data.PVVol[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.PVVol[0];
        //MI PV 电流
        *(dat + (i++)) = MIReal[j + mi_offset].Data.PVCur[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.PVCur[0];
        //电网电压
        *(dat + (i++)) = MIReal[j + mi_offset].Data.GridVol[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.GridVol[0];
        //电网频率f
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Freque[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Freque[0];
        //功率//pv p
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Power[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Power[0];
        //电量//pv e
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.DailyEnergy);
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.DailyEnergy >> 8);
        // 历史电量
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.HistoryEnergyL);
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.HistoryEnergyL >> 8);
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.HistoryEnergyH);
        *(dat + (i++)) = (u8)(MIReal[j + mi_offset].Data.HistoryEnergyH >> 8);
        //温度
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Temper[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Temper[0];
        //状态机
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Run_Status[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Run_Status[0];
        //故障代码
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Fault_Code[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Fault_Code[0];
        //故障累积次数
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Fault_Num[1];
        *(dat + (i++)) = MIReal[j + mi_offset].Data.Fault_Num[0];
        //连接状态
        *(dat + (i++)) = MIReal[j + mi_offset].Data.LinkState & 0x01;
        *(dat + (i++)) = 0; //预留
        *(dat + (i++)) = 0; //预留
        *(dat + (i++)) = 0; //预留
        *(dat + (i++)) = 0; //预留
        *(dat + (i++)) = 0; //预留
    }

    //data len L
    *(dat + 1) = (u8)(i - 17);
    //data len H
    *(dat + 2) = (u8)((i - 17) >> 8);
    *(dat + i) = package_crc((dat + 1), (i - 1));
    i++;
    *(dat + (i++)) = 0x15; //end
    return(i);
}

/***********************************************
** Function name:
** Descriptions:        打包历史数据
** input parameters:    无
** output parameters:   无
** Returned value:      有历史数据返回1 没有历史数据返回0
*************************************************/
u16 package_history_data(u8 package_now, u8 *package_nub, u8 *dat)
{
    vu16 len = 0;
    //取出历史数据
    len = (u16)(History_Data_Unpack(package_now, package_nub, (char *)dat));

    if(len == 0)
    {
        return 0;
    }

    *(dat + 5) = sever_serial_nub;
    *(dat + 6) = DTU_SERIAL_BUB(dtu_serial_nub);
    //当前帧加一
    //      *(dat + 16) = *(dat + 16) + 1;
    //历史数据
    *(dat + 17) = 0x81;
    *(dat + len - 2) = package_crc((dat + 1), (len - 3));
    return len;
}

/***********************************************
** Function name:
** Descriptions:        并网保护文件自检状态回复
** input parameters:    发送buff
** input parameters:    DTU 流水号
** input parameters:    第几帧
** output parameters:   无
** Returned value:      无
*************************************************/
u16 Pack_Self_Inspection(u8 *dat)
{
    GPSTVal GPSTValBuf[10] = {0};
    volatile calendar_obj calendar;
    vu16 i;
    vu16 j;
    //未发送的总数
    vu8 UnsentNub = 0;
    //此次要发送的设备数
    vu8 sentNub = 0;
    //获取未发送数量
    UnsentNub = GPST_Data_Num();

    if(UnsentNub == 0)
    {
        return 0;
    }

    i = 0;
    j = 0;
    //start
    *(dat + (i++)) = 0xa5;
    //data len l
    *(dat + (i++)) = 0x00;
    //data len h
    *(dat + (i++)) = 0x00;
    //command
    //control c1
    *(dat + (i++)) = COMMAND_C1;
    //control c2
    *(dat + (i++)) = GRID_CHECK_STATUS | 0x40;
    //ls 服务器
    *(dat + (i++)) = sever_serial_nub;
    //ls DTU 流水号
    *(dat + (i++)) = DTU_SERIAL_BUB(dtu_serial_nub);
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum);
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum >> 8);
    // PN  共多少帧   最大10个ID一帧
    *(dat + (i++)) = 1;
    //PS  第几帧
    *(dat + (i++)) = 1;
    //应答子命令
    *(dat + (i++)) = 0x0b;
    //获取时间
    RTC_GetTimes((calendar_obj *)&calendar);
    *(dat + (i++)) = (u8)(calendar.w_year % 100);
    *(dat + (i++)) = calendar.w_month;
    *(dat + (i++)) = calendar.w_date;
    *(dat + (i++)) = calendar.hour;
    *(dat + (i++)) = calendar.min;
    *(dat + (i++)) = calendar.sec;
    //time zone
    *(dat + (i++)) = (u8)(time_zone_select);
    *(dat + (i++)) = (u8)(time_zone_select >> 8);

    //当前帧发送的设备数
    if(UnsentNub > 10)
    {
        sentNub = 10;
    }
    else
    {
        sentNub = UnsentNub;
    }

    *(dat + (i++)) = sentNub;

    if(GPST_Data_Read((GPSTVal *)&GPSTValBuf, sentNub) == 0)
    {
        return 0;
    }

    for(j = 0; j < sentNub ; j++)
    {
        //id
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[5];
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[4];
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[3];
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[2];
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[1];
        *(dat + (i++)) = GPSTValBuf[j].Property.pv_sn[0];
        //数据长度
        *(dat + (i++)) = sizeof(GPSTVal) - 6;
        //数据
        memcpy((dat + i), (u8 *)&GPSTValBuf[j].Data[6], sizeof(GPSTVal) - 6);
        i = i + sizeof(GPSTVal) - 6;
    }

    //data len L
    *(dat + 1) = (u8)(i - 17);
    //data len H
    *(dat + 2) = (u8)((i - 17) >> 8);
    *(dat + i) = package_crc((dat + 1), (i - 1));
    i++;
    *(dat + (i++)) = 0x15; //end
    return(i);
}
/***********************************************
** Function name:
** Descriptions:        心跳帧
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u16 Pack_Heartbeat(u8 *dat)
{
    vu16 i = 0;
    //start
    *(dat + (i++)) = 0xa5;
    //data len l
    *(dat + (i++)) = 0x00;
    //data len h
    *(dat + (i++)) = 0x00;
    //command
    //control c1
    *(dat + (i++)) = COMMAND_C1;
    //control c2
    *(dat + (i++)) = HEARTBEAT_FRAME | 0x40;
    //ls 服务器
    *(dat + (i++)) = sever_serial_nub;
    //ls DTU 流水号
    *(dat + (i++)) = DTU_SERIAL_BUB(dtu_serial_nub);
    //DTU id
    *(dat + (i++)) = Dtu3Major.Property.Id[3];
    *(dat + (i++)) = Dtu3Major.Property.Id[2];
    *(dat + (i++)) = Dtu3Major.Property.Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Id[0];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[1];
    *(dat + (i++)) = Dtu3Major.Property.Pre_Id[0];
    // DN L  共多少设备
    *(dat + (i++)) = (u8)Dtu3Detail.Property.PortNum;
    // DN H
    *(dat + (i++)) = (u8)(Dtu3Detail.Property.PortNum >> 8);
    // PN共多少帧   最大25个ID一帧
    *(dat + (i++)) = 1;
    //PS  第几帧
    *(dat + (i++)) = 1;
    //data len L
    *(dat + 1) = 0;
    //data len H
    *(dat + 2) = 0;
    //check sum
    *(dat + i) = package_crc((dat + 1), (i - 1));
    i++;
    //end
    *(dat + (i++)) = 0x15;
    return(i);
}
/***********************************************
** Function name:
** Descriptions:        同步网络时间到RTC
** input parameters:    时间,时区
** output parameters:   无
** Returned value:      无
*************************************************/
void NetTime_TO_RTC(calendar_obj calendar)
{
    //设置RTC
    RTC_SetTimes(calendar);
    Dtu3Detail.Property.timezone = 0;

    if(storge_historical_data_flg != 0xABCD)
    {
        Memory_BKP_Read();
        storge_historical_data_flg = 0xABCD;
        Memory_BKP_Save();
    }
}

/***********************************************
** Function name:
** Descriptions:        在链路表里找
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u16 search_adr_for_link(u8 *id, u8 *p_id, u16 len)
{
    u16 i;

    for(i = 0; i < len; i++)
    {
        if((memcmp((u8 *)&MIMajor[i].Property.Id[0], id, 4) == 0) && (memcmp((u8 *)&MIMajor[i].Property.Pre_Id[0], p_id, 2) == 0)) //如果目标地址和链路表首地址相同
        {
            return(i);
        }
    }

    return(len + 1);
}


uint16_t get_form_value_4hex(uint8_t *s, uint8_t *dat)
{
    char j;
    char str[4];
    uint16_t temp;
    memset(str, 0, sizeof(str));

    for(j = 0; j < 4; j++)
    {
        str[j] = 0;
    }

    temp = 0;

    for(j = 0; j < 4; j++)
    {
        if((s[j] >= 0x30) && (s[j] <= 0x39))
        {
            str[j] = s[j] - 0x30;        //
        }
        else if((s[j] >= 0x61) && (s[j] <= 0x66))
        {
            str[j] = s[j] - 0x57;        //
        }
        else if((s[j] >= 0x41) && (s[j] <= 0x46))
        {
            str[j] = s[j] - 0x37;        //
        }
        else if(j > 0)
        {
            break;
        }

        temp = (temp << 4);
        temp = temp + str[j];
    }

    (*dat) = ((temp >> 8) & 0x00ff);
    (*(dat + 1)) = (temp & 0x00ff);
    return (temp);
}
