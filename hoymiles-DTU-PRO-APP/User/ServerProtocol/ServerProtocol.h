#ifndef __SERVERPROTOCOL_H
#define __SERVERPROTOCOL_H

#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#include "usart_nrf.h"
//DTU 流水号
#define DTU_SERIAL_BUB(nub) ((nub) < 0XFF ? (nub++) : (nub = 0))
#define SOCKET_SERVER_NUB_ADR 5         //服务器流水号地址

#define COMMAND_C1  0x10            // 控制码1 

#define INFORMATION_FRAME  0x01     //信息帧    传输采集器，逆变器的信息
#define DATA_FRAME         0x02     //数据帧    传输设备的信息 
#define GRID_CHECK_STATUS  0x03     //并网自检状态上传
//#define                   0x04    //召唤帧    读设备的信息，数据等
#define TRANSPARENT_FRAME 0x05      //透传帧     透传设备，采集器的信息
//#define                   0x06    //设置帧    设置采集器的参数
#define HEARTBEAT_FRAME   0x07      //心跳帧    保持连接的心跳帧
//#define                   0x08    //事件帧    上传事件记录
//#define                   0x09    //升级帧    用于升级网关和逆变器


#define SOCKET_PS_ADR  16        //帧数地址
#define SOCKET_DAT_CMD_ADR 17    //透传数据帧类型   
#define SOCKET_DATE_ADR  19      //时间帧地址
#define SOCKET_DATID_CMD_ADR 28  // 网关透传命令开始

void Process_Server_Data(char *buffer);
void process_Ip_Prot(char *buff, u8 *ip, u8 *url, u16 *port);
u16 Reply_Server_Command(u8 *dat, u8 packager_now);
u16 Pack_Information(u8 package_nub, u8 package_now, u32 time, u16 RpCount, u16 RpOff, u8 mi_nub, u16 mi_offset, u8 *dat, InverterDetail *mInverterDetail);
u16 Pack_Realdata(u8 package_nub, u8 package_now, u32 time, u16 RpCount, u16 RpOff, u8 mi_nub, u16 mi_offset, u8 *dat);
u16 Pack_Heartbeat(u8 *dat);
void Process_Server_Data(char *buffer);
void NetTime_TO_RTC(calendar_obj calendar);
//void change_MI_id(u8 *source, u8 *pro_id, u8 *id);
u16 package_history_data(u8 package_now, u8 *package_nub, u8 *dat);
u16 search_adr_for_link(u8 *id, u8 *p_id, u16 len);
u16 Pack_Self_Inspection(u8 *dat);
uint16_t get_form_value_4hex(uint8_t *s, uint8_t *dat);

#endif
