#ifndef __SYSTICK_H
#define __SYSTICK_H
#ifdef DTU3PRO

#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
extern vu32 LocalTime;

void SysTick_Init(void);
void Delay_us(u32 us);
void Delay_ms(u32 ms);
void Delay_s(u32 s);
void hardware_delay_ms(u32 time);
#endif /* __SYSTICK_H */
