#include "SunSpec.h"
#include "crc16.h"
#include "usart.h"
#include "string.h"
#include "malloc.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#include "AntiReflux.h"
#include "Memory.h"
#define SUNSPEC_BUF_SIZE                210
#define SUNSPEC_ADDR_LEN                1
#define SUNSPEC_ADDR_START              40000



extern vu8 Usart_Modbus_Rec_Data;
extern vu32 Usart_Modbus_Rec_Time;
extern vu16 Usart_Modbus_Rx_Cnt;
extern vu16 Usart_485_Read_Cnt;
extern vu8 Uart_Rec_mybuf[Usart_485_Len];
extern volatile InverterMajor MIMajor[PORT_LEN];
extern volatile InverterReal MIReal[PORT_LEN];
SUNSPEC_INFO *SunSpec_Info = NULL;

volatile float_u sunspec_temp_f;
volatile float temp_f = 0;
volatile InverterDetail MI_Detail;
vu8 mi_place = 0;
vu8 sunspec_contrl = 0;
vu32 sunspec_time = 0;
//03（0x03）-读取保持寄存器
void SunSpec_Read_Reg(u8 *buf, u16 len)
{
    u8 *sunspec_buf = NULL;
    vu8 i = 0;
    vu8 j = 0;
    vu8 command = 0;
    vu8 data_len = 0;
    vs16 sunspec_temp = 0;
    vu16 crc = 0;
    vu16 x = 0;
    vu16 run_state = 0;
    vu16 reg_num = 0;
    vu16 reg_addr = 0;
    vu16 reg_addr_off = 0;
    vu16 device_addr = 0;
    vu16 limit_switch = 0;
    vu16 sunspec_enum_temp = 0;
    vu16 sunspec_version = 0;
    vs16 grid_active_power = 0;
    vu32 temp = 0;
    vu32 HW_PN = 0;
    sunspec_buf = mymalloc(SUNSPEC_BUF_SIZE * 2);
    //    if(sunspec_buf == NULL)
    //    {
    //        return;
    //    }
    memset(sunspec_buf, 0, SUNSPEC_BUF_SIZE * 2);

    if(SUNSPEC_ADDR_LEN == 1)
    {
        device_addr = Usart_485_Rx_Buf[0];
    }
    else if(SUNSPEC_ADDR_LEN == 2)
    {
        device_addr = (u16)(Usart_485_Rx_Buf[0] << 8) + (u16)(Usart_485_Rx_Buf[1]);
    }

    command = buf[SUNSPEC_ADDR_LEN];
    reg_addr = (u16)(buf[SUNSPEC_ADDR_LEN + 1] << 8) + (u16)(buf[SUNSPEC_ADDR_LEN + 2]);
    reg_num = (u16)(buf[SUNSPEC_ADDR_LEN + 3] << 8) + (u16)(buf[SUNSPEC_ADDR_LEN + 4]);

    if(MIMajor[mi_place].Property.Power_Limit == 2 * FULL_CONTROL_VALUE)
    {
        limit_switch = 0;
    }
    else
    {
        limit_switch = 1;
    }

    /*
    1:设备未运行
    2:设备正在休眠/自动关机
    3:设备正在启动
    4:设备正在自动跟踪最大功率点
    5:设备以降低的功率输出运行
    6:设备正在关闭
    7:存在一个或多个故障
    8:设备处于待机模式
    */
    if(MIReal[mi_place].Data.LinkState == 0)
    {
        run_state = 1;
    }
    else
    {
        if(MIMajor[mi_place].Property.Acq_Switch == 0)
        {
            run_state = 6;
        }
        else
        {
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                if(MIMajor[mi_place].Property.Power_Limit >= FULL_CONTROL_VALUE)
                {
                    run_state = 4;
                }
                else
                {
                    run_state = 5;
                }
            }
            else
            {
                //微逆 端口号
                switch(MIMajor[mi_place].Property.Port)
                {
                    case MI_500W_A:
                        if(MIMajor[mi_place].Property.Power_Limit >= MY_MAX_PORT_LIMIT * 2)
                        {
                            run_state = 4;
                        }
                        else
                        {
                            run_state = 5;
                        }

                        break;

                    case MI_1000W_A:
                        if(MIMajor[mi_place].Property.Power_Limit >= MY_MAX_PORT_LIMIT * 4)
                        {
                            run_state = 4;
                        }
                        else
                        {
                            run_state = 5;
                        }

                        break;

                    case MI_NO:
                    case MI_250W:
                    default :
                        if(MIMajor[mi_place].Property.Power_Limit >= MY_MAX_PORT_LIMIT)
                        {
                            run_state = 4;
                        }
                        else
                        {
                            run_state = 5;
                        }

                        break;
                }
            }
        }
    }

    if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
    {
        grid_active_power = (u16)(MIReal[mi_place].Data.GridActivePower[0] << 8) + (u16)(MIReal[mi_place].Data.GridActivePower[1]);
    }
    else
    {
        //微逆 端口号
        switch(MIMajor[mi_place].Property.Port)
        {
            case MI_NO:
                grid_active_power    = ((u16)MIReal[mi_place].Data.Power[0] << 8) + MIReal[mi_place].Data.Power[1];
                break;

            case MI_250W:
                grid_active_power    = ((u16)MIReal[mi_place].Data.Power[0] << 8) + MIReal[mi_place].Data.Power[1];
                break;

            case MI_500W_A:
                grid_active_power    = ((u16)MIReal[mi_place].Data.Power[0] << 8) + MIReal[mi_place].Data.Power[1] +
                                       ((u16)MIReal[mi_place + 1].Data.Power[0] << 8) + MIReal[mi_place + 1].Data.Power[1];
                break;

            case MI_1000W_A:
                grid_active_power    = ((u16)MIReal[mi_place].Data.Power[0] << 8) + MIReal[mi_place].Data.Power[1] +
                                       ((u16)MIReal[mi_place + 1].Data.Power[0] << 8) + MIReal[mi_place + 1].Data.Power[1] +
                                       ((u16)MIReal[mi_place + 2].Data.Power[0] << 8) + MIReal[mi_place + 2].Data.Power[1] +
                                       ((u16)MIReal[mi_place + 3].Data.Power[0] << 8) + MIReal[mi_place + 3].Data.Power[1];
                break;

            default :
                grid_active_power    = ((u16)MIReal[mi_place].Data.Power[0] << 8) + MIReal[mi_place].Data.Power[1];
                break;
        }
    }

    {
        x = 0;
        /*1*/
        {
            //SunS 0
            sunspec_buf[x * 2 + 0] = 0x53;
            sunspec_buf[x * 2 + 1] = 0x75;
            //1
            x++;
            sunspec_buf[x * 2 + 0] = 0x6e;
            sunspec_buf[x * 2 + 1] = 0x53;
            x++;
            //ID  2
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x01;
            x++;
            //L   3
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 66;
            x++;
            //Manufacturer
            sprintf((char *)&sunspec_buf[x * 2 + 0], "Hoymiles");
            x = 20;
            //Model
            {
                //逆变器RF软硬件版本
                InverterDetail_Read((InverterDetail *)&MI_Detail, mi_place, 1);
                HW_PN = ((u32)MI_Detail.Property.HW_PNH << 16) + MI_Detail.Property.HW_PNL;

                switch(HW_PN)
                {
                    case 0x0001000F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-250T");
                        break;

                    case 0x0003000F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300T");
                        break;

                    case 0x0011000F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-500T");
                        break;

                    case 0x0013000F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600T");
                        break;

                    case 0x0001118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-250");
                        break;

                    case 0x0003118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300");
                        break;

                    case 0x0004118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350");
                        break;

                    case 0x0011118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-500");
                        break;

                    case 0x0013118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600");
                        break;

                    case 0x0013218F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600LV");
                        break;

                    case 0x0014218F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700LV");
                        break;

                    case 0x0014118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700");
                        break;

                    case 0x0021118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000");
                        break;

                    case 0x0023118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200");
                        break;

                    case 0x0025118F:
                    case 0x0024118F:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500");
                        break;

                    case 0x10123000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500");
                        break;

                    case 0x10123004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500T");
                        break;

                    case 0x10123002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500N");
                        break;

                    case 0x10123003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500NT");
                        break;

                    case 0x10123001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500(R)");
                        break;

                    case 0x10123005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1500(R)T");
                        break;

                    case 0x10123010:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M1600(R)");
                        break;

                    case 0x10123011:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M1600(Q)(R)");
                        break;

                    case 0x10426001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HME-1350(R)-AU");
                        break;

                    case 0x10426002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HME-1350(R)T-AU");
                        break;

                    case 0x10121000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200");
                        break;

                    case 0x10121004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200T");
                        break;

                    case 0x10121002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200N");
                        break;

                    case 0x10121003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200NT");
                        break;

                    case 0x10121001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200(R)");
                        break;

                    case 0x10121005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1200(R)T");
                        break;

                    case 0x10421001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HME-1200(R)-AU");
                        break;

                    case 0x10421002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HME-1200(R)T-AU");
                        break;

                    case 0x10120000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000");
                        break;

                    case 0x10120004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000T");
                        break;

                    case 0x10120002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000N");
                        break;

                    case 0x10120003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000NT");
                        break;

                    case 0x10120001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000(R)");
                        break;

                    case 0x10120005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-1000(R)T");
                        break;

                    case 0x10023000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500");
                        break;

                    case 0x10023003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500T");
                        break;

                    case 0x10023002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500(LC)");
                        break;

                    case 0x10023001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500(R)");
                        break;

                    case 0x10023005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500(R)T");
                        break;

                    case 0x10023004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500N");
                        break;

                    case 0x10023006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1500NT");
                        break;

                    case 0x10021000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200");
                        break;

                    case 0x10021001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200T");
                        break;

                    case 0x10021002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200(R)");
                        break;

                    case 0x10021005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200(R)T");
                        break;

                    case 0x10021004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200N");
                        break;

                    case 0x10021006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200NT");
                        break;

                    case 0x10021003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1200(L)");
                        break;

                    case 0x10020000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000");
                        break;

                    case 0x10020003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000T");
                        break;

                    case 0x10020001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000N");
                        break;

                    case 0x10020004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000NT");
                        break;

                    case 0x10020002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000(R)");
                        break;

                    case 0x10020005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-1000(R)T");
                        break;

                    case 0x10113000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800");
                        break;

                    case 0x10113003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800T");
                        break;

                    case 0x10113001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800N");
                        break;

                    case 0x10113004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800NT");
                        break;

                    case 0x10113002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800(R)");
                        break;

                    case 0x10113005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-800(R)T");
                        break;

                    case 0x10112000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700");
                        break;

                    case 0x10112003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700T");
                        break;

                    case 0x10112002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700N");
                        break;

                    case 0x10112004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700NT");
                        break;

                    case 0x10112001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700(R)");
                        break;

                    case 0x10112005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700(R)T");
                        break;

                    case 0x10112006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700LV");
                        break;

                    case 0x10112008:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700LT");
                        break;

                    case 0x10112007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700LV(R)");
                        break;

                    case 0x10112009:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-700LT(R)");
                        break;

                    case 0x10112010:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M800(R)");
                        break;

                    case 0x10112011:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M800(Q)(R)");
                        break;

                    case 0x10111000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600");
                        break;

                    case 0x10111003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600T");
                        break;

                    case 0x10111002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600N");
                        break;

                    case 0x10111004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600NT");
                        break;

                    case 0x10111001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600(R)");
                        break;

                    case 0x10111005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600(R)T");
                        break;

                    case 0x10111006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600LV");
                        break;

                    case 0x10111008:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600LT");
                        break;

                    case 0x10111007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600LV(R)");
                        break;

                    case 0x10111009:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-600LT(R)");
                        break;

                    case 0x10013000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800");
                        break;

                    case 0x10013003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800T");
                        break;

                    case 0x10013001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800N");
                        break;

                    case 0x10013004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800NT");
                        break;

                    case 0x10013002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800(R)");
                        break;

                    case 0x10013005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-800(R)T");
                        break;

                    case 0x10012000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700");
                        break;

                    case 0x10012003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700T");
                        break;

                    case 0x10012002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700N");
                        break;

                    case 0x10012004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700NT");
                        break;

                    case 0x10012001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700(R)");
                        break;

                    case 0x10012005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700(R)T");
                        break;

                    case 0x10012006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700LV");
                        break;

                    case 0x10012008:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700LT");
                        break;

                    case 0x10012007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700LV(R)");
                        break;

                    case 0x10012009:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-700LT(R)");
                        break;

                    case 0x10011000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600");
                        break;

                    case 0x10011003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600T");
                        break;

                    case 0x10011002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600N");
                        break;

                    case 0x10011004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600NT");
                        break;

                    case 0x10011001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600(R)");
                        break;

                    case 0x10011005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600(R)T");
                        break;

                    case 0x10011006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600LV");
                        break;

                    case 0x10011008:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600LT");
                        break;

                    case 0x10011007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600LV(R)");
                        break;

                    case 0x10011009:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-600LT(R)");
                        break;

                    case 0x10010000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-500");
                        break;

                    case 0x10010001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-500(R)");
                        break;

                    case 0x10103000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400");
                        break;

                    case 0x10103003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400T");
                        break;

                    case 0x10103001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400N");
                        break;

                    case 0x10103004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400NT");
                        break;

                    case 0x10103002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400(R)");
                        break;

                    case 0x10103005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-400(R)T");
                        break;

                    case 0x10102000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350");
                        break;

                    case 0x10102003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350T");
                        break;

                    case 0x10102002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350N");
                        break;

                    case 0x10102004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350NT");
                        break;

                    case 0x10102001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350(R)");
                        break;

                    case 0x10102006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-350(R)T");
                        break;

                    case 0x10102010:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M400(R)");
                        break;

                    case 0x10102011:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M400(Q)(R)");
                        break;

                    case 0x10101000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300");
                        break;

                    case 0x10101003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300T");
                        break;

                    case 0x10101002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300N");
                        break;

                    case 0x10101004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300NT");
                        break;

                    case 0x10101001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300(R)");
                        break;

                    case 0x10101006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "HM-300(R)T");
                        break;

                    case 0x10101010:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M350(R)");
                        break;

                    case 0x10101011:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "M350(Q)(R)");
                        break;

                    case 0x10003000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400");
                        break;

                    case 0x10003003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400T");
                        break;

                    case 0x10003001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400N");
                        break;

                    case 0x10003004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400NT");
                        break;

                    case 0x10003002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400(R)");
                        break;

                    case 0x10003005:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-400(R)T");
                        break;

                    case 0x10002000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350");
                        break;

                    case 0x10002003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350T");
                        break;

                    case 0x10002002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350N");
                        break;

                    case 0x10002004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350NT");
                        break;

                    case 0x10002001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350(R)");
                        break;

                    case 0x10002006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350(R)T");
                        break;

                    case 0x10002007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350(R)(SO)");
                        break;

                    case 0x10002008:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-350MC");
                        break;

                    case 0x10001000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300");
                        break;

                    case 0x10001003:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300T");
                        break;

                    case 0x10001002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300N");
                        break;

                    case 0x10001004:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300NT");
                        break;

                    case 0x10001001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300(R)");
                        break;

                    case 0x10001006:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300(R)T");
                        break;

                    case 0x10001007:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-300(R)(SO)");
                        break;

                    case 0x10000000:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-250");
                        break;

                    case 0x10000001:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-250(R)");
                        break;

                    case 0x10000002:
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "MI-250(R)(SO)");
                        break;

                    default :
                        sprintf((char *)&sunspec_buf[x * 2 + 0], "---");
                        break;
                }
            }
            x = 36;
            //Options
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            sunspec_buf[x * 2 + 2] = 0x06;
            sunspec_buf[x * 2 + 3] = 0x21;
            x = 44;
            //Version
            sunspec_version = MI_Detail.Property.AppFWBuild_VER;

            if(sunspec_version == 0)
            {
                sprintf((char *)&sunspec_buf[x * 2 + 0], "---");
            }
            else
            {
                sprintf((char *)&sunspec_buf[x * 2 + 0], "V%02d.%02d.%02d", sunspec_version / 10000, sunspec_version / 100, sunspec_version % 100);
            }

            x = 52;
            //Serial Number
            sprintf((char *)&sunspec_buf[x * 2 + 0], "%02X%02X%02X%02X%02X%02X",
                    MIMajor[mi_place].Property.Pre_Id[0],
                    MIMajor[mi_place].Property.Pre_Id[1],
                    MIMajor[mi_place].Property.Id[0],
                    MIMajor[mi_place].Property.Id[1],
                    MIMajor[mi_place].Property.Id[2],
                    MIMajor[mi_place].Property.Id[3]);
            x = 68;
            //Device Address
            sunspec_buf[x * 2 + 0] = (u8)(device_addr >> 8);
            sunspec_buf[x * 2 + 1] = (u8)(device_addr);
            x++;
            //Pad
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
        }
        /*101*/
        {
            //ID
            sunspec_buf[x * 2 + 0] = 0;
            sunspec_buf[x * 2 + 1] = 101;
            x++;
            //L
            sunspec_buf[x * 2 + 0] = 0;
            sunspec_buf[x * 2 + 1] = 50;
            x++;

            //Amps
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_buf[x * 2 + 0] = MIReal[mi_place].Data.GridCurrent[0];
                sunspec_buf[x * 2 + 1] = MIReal[mi_place].Data.GridCurrent[1];
            }
            else
            {
                sunspec_buf[x * 2 + 0] = 0xFF;
                sunspec_buf[x * 2 + 1] = 0xFF;
            }

            x++;

            //Amps PhaseA
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_buf[x * 2 + 0] = MIReal[mi_place].Data.GridCurrent[0];
                sunspec_buf[x * 2 + 1] = MIReal[mi_place].Data.GridCurrent[1];
            }
            else
            {
                sunspec_buf[x * 2 + 0] = 0xFF;
                sunspec_buf[x * 2 + 1] = 0xFF;
            }

            x++;
            //Amps PhaseB
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Amps PhaseC
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //A_SF
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = -2;
            x++;
            //Phase Voltage AB
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Phase Voltage BC
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Phase Voltage CA
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Phase Voltage AN
            sunspec_buf[x * 2 + 0] = MIReal[mi_place].Data.GridVol[0];
            sunspec_buf[x * 2 + 1] = MIReal[mi_place].Data.GridVol[1];
            x++;
            //Phase Voltage BN
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Phase Voltage CN
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //V_SF
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = -1;
            x++;
            //AC Power
            sunspec_temp = grid_active_power;
            sunspec_buf[x * 2 + 0] = (u8)(sunspec_temp >> 8);
            sunspec_buf[x * 2 + 1] = (u8)(sunspec_temp);
            x++;
            //W_SF
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = -1;
            x++;
            //Line Frequency
            sunspec_buf[x * 2 + 0] = MIReal[mi_place].Data.Freque[0];
            sunspec_buf[x * 2 + 1] = MIReal[mi_place].Data.Freque[1];
            x++;
            //Hz_SF
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = -2;
            x++;
            //AC Apparent Power
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //VA_SF
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //AC Reactive Power
            sunspec_temp = (s16)((u16)(MIReal[mi_place].Data.GridReactivePower[0] << 8) + (u16)(MIReal[mi_place].Data.GridReactivePower[1]));
            sunspec_buf[x * 2 + 0] = (u8)(sunspec_temp >> 8);
            sunspec_buf[x * 2 + 1] = (u8)(sunspec_temp);
            x++;
            //VAr_SF
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = -2;
            x++;

            //AC Power Factor
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_temp = (s16)((u16)(MIReal[mi_place].Data.PowerFactor[0] << 8) + (u16)(MIReal[mi_place].Data.PowerFactor[1]));
                sunspec_buf[x * 2 + 0] = (u8)(sunspec_temp >> 8);
                sunspec_buf[x * 2 + 1] = (u8)(sunspec_temp);
            }
            else
            {
                sunspec_buf[x * 2 + 0] = 0x80;
                sunspec_buf[x * 2 + 1] = 0x00;
            }

            x++;

            //PF_SF

            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_buf[x * 2 + 0] = 0xFF;
                sunspec_buf[x * 2 + 1] = -2;
            }
            else
            {
                sunspec_buf[x * 2 + 0] = 0x80;
                sunspec_buf[x * 2 + 1] = 0x00;
            }

            x++;

            if((MIReal[mi_place].Data.HistoryEnergyH == 0) && (MIReal[mi_place].Data.HistoryEnergyL == 0))
            {
                //AC Energy H
                sunspec_buf[x * 2 + 0] = 0;
                sunspec_buf[x * 2 + 1] = 0;
                x++;
                //AC Energy L
                sunspec_buf[x * 2 + 0] = 0;
                sunspec_buf[x * 2 + 1] = 1;
                x++;
            }
            else
            {
                //AC Energy H
                sunspec_buf[x * 2 + 0] = (u8)(MIReal[mi_place].Data.HistoryEnergyH >> 8);
                sunspec_buf[x * 2 + 1] = (u8)(MIReal[mi_place].Data.HistoryEnergyH);
                x++;
                //AC Energy L
                sunspec_buf[x * 2 + 0] = (u8)(MIReal[mi_place].Data.HistoryEnergyL >> 8);
                sunspec_buf[x * 2 + 1] = (u8)(MIReal[mi_place].Data.HistoryEnergyL);
                x++;
            }

            //WH_SF
            sunspec_buf[x * 2 + 0] = 0;
            sunspec_buf[x * 2 + 1] = 0;
            x++;
            //DC Current
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //DCA_SF
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //DC Voltage
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //DCV_SF
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //DC Power
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //DCW_SF
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Cabinet Temperature
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Heat Sink Temperature
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Transformer Temperature
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Other Temperature
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Tmp_SF
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Enumerated value.  Operating state
            sunspec_buf[x * 2 + 0] = (u8)(run_state >> 8);
            sunspec_buf[x * 2 + 1] = (u8)run_state;
            x++;
            //Vendor specific operating state code
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Bitmask value. Event fields
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Reserved for future use
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
        }
        /*111*/
        {
            //Include this model for single phase inverter monitoring using float values
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 111;
            x++;
            //Model Length
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 60;
            x++;

            //AC Current
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.GridCurrent[0] << 8) + (MIReal[mi_place].Data.GridCurrent[1])) / 100;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
                x++;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
                x++;
            }
            else
            {
                sunspec_temp_f.a = 0;
                sunspec_buf[x * 2 + 0] = 0x7F;
                sunspec_buf[x * 2 + 1] = 0xC0;
                x++;
                sunspec_buf[x * 2 + 0] = 0x00;
                sunspec_buf[x * 2 + 1] = 0x00;
                x++;
            }

            //Phase A Current
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.GridCurrent[0] << 8) + (MIReal[mi_place].Data.GridCurrent[1])) / 100;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
                x++;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
                x++;
            }
            else
            {
                sunspec_temp_f.a = 0;
                sunspec_buf[x * 2 + 0] = 0x7F;
                sunspec_buf[x * 2 + 1] = 0xC0;
                x++;
                sunspec_buf[x * 2 + 0] = 0x00;
                sunspec_buf[x * 2 + 1] = 0x00;
                x++;
            }

            //Phase B Current
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase C Current
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase Voltage AB
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase Voltage BC
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase Voltage CA
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase Voltage AN
            sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.GridVol[0] << 8) + (MIReal[mi_place].Data.GridVol[1])) / 10.0;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
            x++;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
            x++;
            //Phase Voltage BN
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Phase Voltage CN
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //AC Power
            sunspec_temp_f.a = (float)(grid_active_power) / 10;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
            x++;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
            x++;
            //Line Frequency
            sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.Freque[0] << 8) + (MIReal[mi_place].Data.Freque[1])) / 100;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
            x++;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
            x++;
            //AC Apparent Power
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //AC Reactive Power
            sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.GridReactivePower[0] << 8) + (MIReal[mi_place].Data.GridReactivePower[1])) / 100;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
            x++;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
            x++;

            //AC Power Factor
            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_temp_f.a = (float)(((u16)MIReal[mi_place].Data.PowerFactor[0] << 8) + (MIReal[mi_place].Data.PowerFactor[1])) / 100;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
                x++;
                sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
                sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
                x++;
            }
            else
            {
                sunspec_temp_f.a = 0;
                sunspec_buf[x * 2 + 0] = 0x7F;
                sunspec_buf[x * 2 + 1] = 0xC0;
                x++;
                sunspec_buf[x * 2 + 0] = 0x00;
                sunspec_buf[x * 2 + 1] = 0x00;
                x++;
            }

            //AC Energy
            sunspec_temp_f.a = (float)(((u32)MIReal[mi_place].Data.HistoryEnergyH << 16) + (MIReal[mi_place].Data.HistoryEnergyL));
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[3];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[2];
            x++;
            sunspec_buf[x * 2 + 0] = sunspec_temp_f.b[1];
            sunspec_buf[x * 2 + 1] = sunspec_temp_f.b[0];
            x++;
            //DC Current
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //DC Voltage
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //DC Power
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Cabinet Temperature
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Heat Sink Temperature
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Transformer Temperature
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Other Temperature
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0x7F;
            sunspec_buf[x * 2 + 1] = 0xC0;
            x++;
            sunspec_buf[x * 2 + 0] = 0x00;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Enumerated value.  Operating state
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = (u8)(run_state >> 8);
            sunspec_buf[x * 2 + 1] = (u8)run_state;
            x++;
            //Vendor specific operating state code
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Bitmask value. Event fields
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Reserved for future use
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Vendor defined events
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            sunspec_temp_f.a = 0;
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
        }
        /*123*/
        {
            //ID
            sunspec_buf[x * 2 + 0] = 0;
            sunspec_buf[x * 2 + 1] = 123;
            x++;
            //L
            sunspec_buf[x * 2 + 0] = 0;
            sunspec_buf[x * 2 + 1] = 24;
            x++;
            //Time window for connect/disconnect.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Timeout period for connect/disconnect.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Enumerated valued.  Connection control.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;

            if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
            {
                sunspec_temp = MIMajor[mi_place].Property.Power_Limit;

                if(sunspec_temp >= FULL_CONTROL_VALUE)
                {
                    sunspec_temp = FULL_CONTROL_VALUE;
                }
            }
            else
            {
                //Set power output to specified level.

                //微逆 端口号
                switch(MIMajor[mi_place].Property.Port)
                {
                    case MI_500W_A:
                        if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT * 2) + FULL_CONTROL_VALUE))
                        {
                            limit_switch = 0;
                            sunspec_temp = 1000;
                        }
                        else
                        {
                            limit_switch = 1;
                            sunspec_temp = (MIMajor[mi_place].Property.Power_Limit * 1000) / (MY_MAX_PORT_LIMIT * 2);
                        }

                        break;

                    case MI_1000W_A:
                        if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT * 4) + FULL_CONTROL_VALUE))
                        {
                            limit_switch = 0;
                            sunspec_temp = 1000;
                        }
                        else
                        {
                            limit_switch = 1;
                            sunspec_temp = (MIMajor[mi_place].Property.Power_Limit * 1000) / (MY_MAX_PORT_LIMIT * 4);
                        }

                        break;

                    case MI_NO:
                    case MI_250W:
                    default :
                        if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT) + FULL_CONTROL_VALUE))
                        {
                            limit_switch = 0;
                            sunspec_temp = 1000;
                        }
                        else
                        {
                            limit_switch = 1;
                            sunspec_temp = (MIMajor[mi_place].Property.Power_Limit * 1000) / (MY_MAX_PORT_LIMIT);
                        }

                        break;
                }
            }

            sunspec_buf[x * 2 + 0] = (u8)(sunspec_temp >> 8);
            sunspec_buf[x * 2 + 1] = (u8)(sunspec_temp);
            x++;
            //Time window for power limit change.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Timeout period for power limit.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Ramp time for moving from current setpoint to new setpoint.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Enumerated valued.  Throttle enable/disable control.
            sunspec_buf[x * 2 + 0] = (u8)(limit_switch >> 8);
            sunspec_buf[x * 2 + 1] = (u8)(limit_switch);
            x++;
            //Set power factor to specific value - cosine of angle.
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Time window for power factor change.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Timeout period for power factor.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Ramp time for moving from current setpoint to new setpoint.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Enumerated valued.  Fixed power factor enable/disable control.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Reactive power in percent of WMax.
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Reactive power in percent of VArMax.
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Reactive power in percent of VArAval.
            sunspec_buf[x * 2 + 0] = 0x80;
            sunspec_buf[x * 2 + 1] = 0x00;
            x++;
            //Time window for VAR limit change.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Timeout period for VAR limit.
            //Ramp time for moving from current setpoint to new setpoint.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Enumerated value. VAR percent limit mode.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //No VAR limit
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //VAR limit as a % of Wmax
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //VAR limit as a % of VArMax
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //VAR limit as a % of VArAval
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Enumerated valued.  Percent limit VAr enable/disable control.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Scale factor for power output percent.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Scale factor for power factor.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
            //Scale factor for reactive power percent.
            sunspec_buf[x * 2 + 0] = 0xFF;
            sunspec_buf[x * 2 + 1] = 0xFF;
            x++;
        }
    }

    if((reg_addr >= SUNSPEC_ADDR_START) && (reg_addr < (SUNSPEC_ADDR_START + 10000)))
    {
        reg_addr_off = reg_addr - SUNSPEC_ADDR_START;
        memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
        Usart_485_Tx_Buf[0] = device_addr;
        Usart_485_Tx_Buf[1] = command;
        i = 3;
        data_len = 0;

        for(j = 0; j < reg_num; j++)
        {
            if((j + reg_addr_off) < x)
            {
                Usart_485_Tx_Buf[(i++)] = sunspec_buf[(j + reg_addr_off) * 2 + 0];
                data_len ++;
                Usart_485_Tx_Buf[(i++)] = sunspec_buf[(j + reg_addr_off) * 2 + 1];
                data_len ++;
            }
            else
            {
                break;
            }
        }

        Usart_485_Tx_Buf[2] = data_len;
        crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
        Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
        Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
        Usart_Send(i);
    }
    else
    {
        memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
        Usart_485_Tx_Buf[0] = device_addr;
        Usart_485_Tx_Buf[1] = (command | 0x80);
        Usart_485_Tx_Buf[2] = 0x02;
        i = 3;
        crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
        Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
        Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
        Usart_Send(i);
    }

    myfree(sunspec_buf);
}
//06（0x06）-写入单个寄存器
//16（0x10）-写入多个寄存器
void SunSpec_Write_Reg(u8 *buf, u16 len)
{
    u8 *data_buf = NULL;
    vu8 i = 0;
    vu8 j = 0;
    vu8 error = 0;
    vu8 command = 0;
    vu8 send_data_len = 0;
    vu16 crc = 0;
    vu16 x = 0;
    vu16 reg_num = 0;
    vu16 reg_addr = 0;
    vu16 data_len = 0;
    vu16 reg_addr_off = 0;
    vu16 device_addr = 0;
    vu16 limit_value = 0;

    if(SUNSPEC_ADDR_LEN == 1)
    {
        device_addr = Usart_485_Rx_Buf[0];
    }
    else if(SUNSPEC_ADDR_LEN == 2)
    {
        device_addr = (u16)(Usart_485_Rx_Buf[0] << 8) + (u16)(Usart_485_Rx_Buf[1]);
    }

    command = buf[SUNSPEC_ADDR_LEN];
    reg_addr = (u16)(buf[SUNSPEC_ADDR_LEN + 1] << 8) + (u16)(buf[SUNSPEC_ADDR_LEN + 2]);

    if(((command == 0x06) || (command == 0x10)) && ((reg_addr >= SUNSPEC_ADDR_START) && (reg_addr < (SUNSPEC_ADDR_START + 10000))))
    {
        if(command == 0x06)
        {
            reg_num = 1;
            data_len = 2;
        }
        else
        {
            reg_num = (u16)(buf[SUNSPEC_ADDR_LEN + 3] << 8) + (u16)(buf[SUNSPEC_ADDR_LEN + 4]);
            data_len = buf[SUNSPEC_ADDR_LEN + 5];
        }

        data_buf = mymalloc(data_len * sizeof(u8));
        //        if(data_buf == NULL)
        //        {
        //            return;
        //        }
        memset(data_buf, 0, (data_len * sizeof(u8)));

        for(i = 0; i < reg_num; i++)
        {
            if(command == 0x06)
            {
                data_buf[i * 2 + 0] = buf[i * 2 + SUNSPEC_ADDR_LEN + 3];
                data_buf[i * 2 + 1] = buf[i * 2 + SUNSPEC_ADDR_LEN + 4];
            }
            else
            {
                data_buf[i * 2 + 0] = buf[i * 2 + SUNSPEC_ADDR_LEN + 6];
                data_buf[i * 2 + 1] = buf[i * 2 + SUNSPEC_ADDR_LEN + 7];
            }
        }

        reg_addr_off = reg_addr - SUNSPEC_ADDR_START;
        memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);

        for(j = 0; j < reg_num; j++)
        {
            switch(j + reg_addr_off)
            {
                //Device Address
                case 68:
                    SunSpec_Info[mi_place].Property.addr = (u8)((u16)(data_buf[j * 2 + 0] << 8) + (u16)(data_buf[j * 2 + 1]));
                    SunSpec_Info_Write(SunSpec_Info, Dtu3Detail.Property.InverterNum);
                    break;

                //Set power output to specified level.
                case 189:
                    limit_value = ((u16)(data_buf[j * 2 + 0] << 8) + (u16)(data_buf[j * 2 + 1]));

                    if(limit_value >= 1000)
                    {
                        limit_value = 1000;
                    }

                    if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
                    {
                        MIMajor[mi_place].Property.Power_Limit = limit_value;
                    }
                    else
                    {
                        //微逆 端口号
                        switch(MIMajor[mi_place].Property.Port)
                        {
                            case MI_500W_A:
                                MIMajor[mi_place].Property.Power_Limit = (limit_value * (MY_MAX_PORT_LIMIT * 2)) / 1000;
                                break;

                            case MI_1000W_A:
                                MIMajor[mi_place].Property.Power_Limit = (limit_value * (MY_MAX_PORT_LIMIT * 4)) / 1000;
                                break;

                            case MI_NO:
                            case MI_250W:
                            default :
                                MIMajor[mi_place].Property.Power_Limit = (limit_value * (MY_MAX_PORT_LIMIT)) / 1000;
                                break;
                        }
                    }

                    sunspec_contrl = 1;
                    break;

                //Enumerated valued.  Throttle enable/disable control.
                case 193:
                    if(((u16)(data_buf[j * 2 + 0] << 8) + (u16)(data_buf[j * 2 + 1])) == 0)
                    {
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
                        {
                            MIMajor[mi_place].Property.Power_Limit = FULL_CONTROL_VALUE * 2;
                        }
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[mi_place].Property.Port)
                            {
                                case MI_500W_A:
                                    MIMajor[mi_place].Property.Power_Limit = ((MY_MAX_PORT_LIMIT * 2) + FULL_CONTROL_VALUE);
                                    break;

                                case MI_1000W_A:
                                    MIMajor[mi_place].Property.Power_Limit = ((MY_MAX_PORT_LIMIT * 4) + FULL_CONTROL_VALUE);
                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    MIMajor[mi_place].Property.Power_Limit = ((MY_MAX_PORT_LIMIT) + FULL_CONTROL_VALUE);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if(UsartNrf_GetInvterType((u8 *)MIMajor[mi_place].Property.Pre_Id) >= Inverter_Pro)
                        {
                            if(MIMajor[mi_place].Property.Power_Limit >= FULL_CONTROL_VALUE)
                            {
                                MIMajor[mi_place].Property.Power_Limit = FULL_CONTROL_VALUE;
                            }
                        }
                        else
                        {
                            //微逆 端口号
                            switch(MIMajor[mi_place].Property.Port)
                            {
                                case MI_500W_A:
                                    if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT * 2) + FULL_CONTROL_VALUE))
                                    {
                                        MIMajor[mi_place].Property.Power_Limit = (MY_MAX_PORT_LIMIT * 2);
                                    }

                                    break;

                                case MI_1000W_A:
                                    if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT * 4) + FULL_CONTROL_VALUE))
                                    {
                                        MIMajor[mi_place].Property.Power_Limit = (MY_MAX_PORT_LIMIT * 4);
                                    }

                                    break;

                                case MI_NO:
                                case MI_250W:
                                default :
                                    if(MIMajor[mi_place].Property.Power_Limit >= ((MY_MAX_PORT_LIMIT) + FULL_CONTROL_VALUE))
                                    {
                                        MIMajor[mi_place].Property.Power_Limit = (MY_MAX_PORT_LIMIT);
                                    }

                                    break;
                            }
                        }
                    }

                    sunspec_contrl = 1;
                    break;

                default:
                    error = 2;
                    break;
            }
        }

        if(MIMajor[mi_place].Property.Power_Limit == 0)
        {
            MIMajor[mi_place].Property.Acq_Switch = 0;
        }
        else
        {
            MIMajor[mi_place].Property.Acq_Switch = 1;
        }

        if(sunspec_contrl == 1)
        {
            System_MI_SN_Write((InverterMajor *)MIMajor);
        }

        Usart_485_Tx_Buf[0] = device_addr;

        if(error == 0)
        {
            Usart_485_Tx_Buf[1] = command;
            Usart_485_Tx_Buf[2] = (u8)(reg_addr >> 8);
            Usart_485_Tx_Buf[3] = (u8)(reg_addr);
            i = 4;

            if(command == 0x06)
            {
                Usart_485_Tx_Buf[(i++)] = data_buf[0];
                Usart_485_Tx_Buf[(i++)] = data_buf[1];
                crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
                Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
                Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
                Usart_Send(i);
            }
            else
            {
                Usart_485_Tx_Buf[(i++)] = (u8)(reg_num >> 8);
                Usart_485_Tx_Buf[(i++)] = (u8)(reg_num);
                crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
                Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
                Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
                Usart_Send(i);
            }
        }
        else
        {
            Usart_485_Tx_Buf[1] = (command | 0x80);
            Usart_485_Tx_Buf[2] = error;
            i = 3;
            crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
            Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
            Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
            Usart_Send(i);
        }
    }
    else
    {
        memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
        Usart_485_Tx_Buf[0] = device_addr;
        Usart_485_Tx_Buf[1] = (command | 0x80);
        Usart_485_Tx_Buf[2] = 0x01;
        i = 3;
        crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
        Usart_485_Tx_Buf[(i++)] = (crc & 0xFF);
        Usart_485_Tx_Buf[(i++)] = (crc >> 8) & 0xFF;
        Usart_Send(i);
    }

    myfree(data_buf);
}


//SunSpec Modbus处理函数
void SunSpec_RTU_Process(void)
{
    vu8 i = 0;
    vu8 find = 0;
    vu8 error = 0;
    vu8 Modbus_CMD = 0;
    vu16 buf_len = 0;
    vu16 modbus_crc = 0;
    vu16 modbus_crc_get = 0;
    vu16 device_addr = 0;

    if(Dtu3Detail.Property.SunSpec_Switch == 1)
    {
        if(SunSpec_Info == NULL)
        {
            SunSpec_Init();
        }

        /*如果接收到新数据*/
        if(Usart_Modbus_Rec_Data == 1)
        {
            /*接收时间超过100ms串口接收超时结束接收*/
            if(LocalTime > (Usart_Modbus_Rec_Time + 50))
            {
                Usart_Modbus_Rec_Time = LocalTime;

                if(SUNSPEC_ADDR_LEN == 1)
                {
                    device_addr = Usart_485_Rx_Buf[0];
                }
                else if(SUNSPEC_ADDR_LEN == 2)
                {
                    device_addr = (u16)(Usart_485_Rx_Buf[0] << 8) + (u16)(Usart_485_Rx_Buf[1]);
                }

                mi_place = 0;
                find = 0;

                for(i = 0; i < Dtu3Detail.Property.InverterNum; i++)
                {
                    if(device_addr == SunSpec_Info[i].Property.addr)
                    {
                        mi_place = SunSpec_Info[i].Property.place;
                        find = 1;
                    }
                }

                if(find == 1)
                {
                    memcpy((u8 *)&Uart_Rec_mybuf[0], (u8 *)&Usart_485_Rx_Buf[0], sizeof(Uart_Rec_mybuf));
                    /*传递接收数据长度*/
                    buf_len = Usart_Modbus_Rx_Cnt;

                    if(buf_len > 2)
                    {
                        modbus_crc = (u16)(Uart_Rec_mybuf[buf_len - 1] << 8) + (u16)Uart_Rec_mybuf[buf_len - 2];
                        modbus_crc_get = ModRTU_CRC((u8 *)Uart_Rec_mybuf, (buf_len - 2));

                        if(modbus_crc == modbus_crc_get)
                        {
                            Modbus_CMD = Uart_Rec_mybuf[SUNSPEC_ADDR_LEN];

                            switch(Modbus_CMD)
                            {
                                /*0x03 -读取保持寄存器*/
                                case 0x03:
                                    error = 0;
                                    //03（0x03）-读取保持寄存器
                                    SunSpec_Read_Reg((u8 *)Uart_Rec_mybuf, buf_len);
                                    break;

                                //06（0x06）-写入单个寄存器
                                case 0x06:

                                //16（0x10）-写入多个寄存器
                                case 0x10:
                                    error = 0;
                                    //06（0x06）-写入单个寄存器
                                    //16（0x10）-写入多个寄存器
                                    SunSpec_Write_Reg((u8 *)Uart_Rec_mybuf, buf_len);
                                    break;

                                default:
                                    error = 1;
                                    break;
                            }
                        }
                        else
                        {
                            error = 1;
                        }
                    }

                    if(error == 1)
                    {
                        error = 0;
                        memset((u8 *)Usart_485_Tx_Buf, 0, Usart_485_Len);
                        i = 0;
                        Usart_485_Tx_Buf[(i++)] = (vu8)(device_addr >> 8);
                        Usart_485_Tx_Buf[(i++)] = (vu8)(device_addr);
                        Usart_485_Tx_Buf[(i++)] = Modbus_CMD | 0x80;
                        Usart_485_Tx_Buf[(i++)] = 1;
                        modbus_crc = 0;
                        modbus_crc = ModRTU_CRC((u8 *)Usart_485_Tx_Buf, i);
                        Usart_485_Tx_Buf[(i++)] = (vu8)(modbus_crc >> 8);
                        Usart_485_Tx_Buf[(i++)] = (vu8)(modbus_crc);
                        Usart_Send(i);
                    }
                }

                Usart_485_Read_Cnt = 0;
                Usart_Modbus_Rx_Cnt = 0;
                Usart_Modbus_Rec_Data = 0;
                memset((u8 *)Usart_485_Rx_Buf, 0, Usart_485_Len);
            }
        }

        if((LocalTime > (sunspec_time + (Dtu3Detail.Property.PortNum * CONTROL_INTERVAL_TIME))) || (sunspec_contrl == 1))
        {
            sunspec_time = LocalTime;
            sunspec_contrl = 0;
            Relative_Power_Sweep_Control();
        }
    }
}
//SunSpec 初始化函数
void SunSpec_Init(void)
{
    vu8 i = 0;
    vu8 j = 0;

    if(Dtu3Detail.Property.SunSpec_Switch == 1)
    {
        SunSpec_Info = mymalloc(100 * sizeof(SUNSPEC_INFO));
        //        if(SunSpec_Info == NULL)
        //        {
        //            return;
        //        }
        memset(SunSpec_Info, 0, 100 * sizeof(SUNSPEC_INFO));

        if(SunSpec_Info_Get_size() == 0)
        {
            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(PORT_NUMBER_CONFIRMATION(i))
                {
                    MIMajor[i].Property.Acq_Switch = 1;

                    if(UsartNrf_GetInvterType((u8 *)MIMajor[i].Property.Pre_Id) >= Inverter_Pro)
                    {
                        MIMajor[i].Property.Power_Limit = FULL_CONTROL_VALUE * 2;
                    }
                    else
                    {
                        //微逆 端口号
                        switch(MIMajor[i].Property.Port)
                        {
                            case MI_500W_A:
                                MIMajor[i].Property.Power_Limit = ((MY_MAX_PORT_LIMIT * 2) + FULL_CONTROL_VALUE);
                                break;

                            case MI_1000W_A:
                                MIMajor[i].Property.Power_Limit = ((MY_MAX_PORT_LIMIT * 4) + FULL_CONTROL_VALUE);
                                break;

                            case MI_NO:
                            case MI_250W:
                            default :
                                MIMajor[i].Property.Power_Limit = ((MY_MAX_PORT_LIMIT) + FULL_CONTROL_VALUE);
                                break;
                        }
                    }

                    SunSpec_Info[j].Property.addr = Dtu3Detail.Property.SunSpec_Addr_Start + j;
                    SunSpec_Info[j].Property.place = i;
                    j++;
                }
            }

            SunSpec_Info_Write(SunSpec_Info, j);
            System_MI_SN_Write((InverterMajor *)MIMajor);
        }
        else
        {
            SunSpec_Info_Read(SunSpec_Info);
        }
    }
}
//SunSpec 释放资源
void SunSpec_DeInit(void)
{
    SunSpec_Info_Delete();

    if(Dtu3Detail.Property.SunSpec_Switch == 1)
    {
        if(SunSpec_Info != NULL)
        {
            myfree(SunSpec_Info);
            SunSpec_Info = NULL;
        }
    }
}