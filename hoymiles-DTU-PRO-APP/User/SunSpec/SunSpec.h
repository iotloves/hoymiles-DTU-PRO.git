#ifndef __SUNSPEC_H
#define __SUNSPEC_H
#include "stm32f4xx.h"

typedef union
{
    struct
    {
        vu8 addr;
        vu8 place;
    } Property;
    vu8 Data[2];
} SUNSPEC_INFO;

typedef union
{
    volatile float a;
    vu8 b[4];
} float_u;

void SunSpec_RTU_Process(void);
//SunSpec 初始化函数
void SunSpec_Init(void);
//SunSpec 释放资源
void SunSpec_DeInit(void);
#endif
