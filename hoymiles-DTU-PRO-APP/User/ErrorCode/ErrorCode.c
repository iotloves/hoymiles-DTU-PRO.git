#include "ErrorCode.h"
#include "Memory.h"
extern volatile SystemCfg system_cfg;
extern volatile DtuDetail Dtu3Detail;
extern volatile InverterMajor MIMajor[PORT_LEN];
u16 DTU_ErrorCode = 0;

void SystemCheck(void)
{
    if((system_cfg.Property.netmode_select == 0xFF) &&
            ((system_cfg.Property.ServerDomainName[0] == 0xFF) || (system_cfg.Property.ServerDomainName[1] == 0xFF)) &&
            (system_cfg.Property.ServerPort == 0xFFFF))
    {
        DTU_ErrorCode = ERROR_EEPROM_UNUSUAL;
    }

    if((Dtu3Detail.Property.PortNum == 0) && ((MIMajor[0].Property.Pre_Id[0] != 0) && (MIMajor[0].Property.Pre_Id[1] != 0) && (MIMajor[0].Property.Pre_Id[0] != 0xFF) && (MIMajor[0].Property.Pre_Id[1] != 0xFF)))
    {
        DTU_ErrorCode = ERROR_FILESYS_PORTNUM;
    }

    if((Dtu3Detail.Property.PortNum != 0) && ((MIMajor[0].Property.Pre_Id[0] == 0) && (MIMajor[0].Property.Pre_Id[1] == 0) && (MIMajor[0].Property.Pre_Id[0] == 0xFF) && (MIMajor[0].Property.Pre_Id[1] == 0xFF)))
    {
        DTU_ErrorCode = ERROR_FILESYS_SN;
    }
}

