#ifndef __ERRORCODE
#define __ERRORCODE
enum ERROR_CODE
{
    ERROR_EEPROM_UNUSUAL    = 0x01,    //EEPROM异常
    ERROR_EEPROM_DELETE     = 0x02,    //EEPROM删除
    ERROR_EEPROM_WRITE      = 0x03,    //EEPROM写入
    ERROR_FILESYS_FORMAT    = 0x04,    //文件系统异常格式化
    ERROR_FILESYS_PORTNUM   = 0x05,    //sn存在组件数不存在
    ERROR_FILESYS_SN        = 0x06,    //sn不存在组件数存在

};
void SystemCheck(void);
#endif
