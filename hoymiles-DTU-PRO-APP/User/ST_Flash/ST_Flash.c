#include "stm32f4xx.h"
#include "ST_Flash.h"
#include "string.h"
#include "Memory.h"
#include "malloc.h"
#include "crc16.h"

//获取某个地址所在的flash扇区
//addr:flash地址
//返回值:0~11,即addr所在的扇区
u16 FLASH_GetFlashSector(u32 addr)
{
    if(addr < ADDR_FLASH_SECTOR_1)
    {
        return FLASH_Sector_0;
    }
    else if(addr < ADDR_FLASH_SECTOR_2)
    {
        return FLASH_Sector_1;
    }
    else if(addr < ADDR_FLASH_SECTOR_3)
    {
        return FLASH_Sector_2;
    }
    else if(addr < ADDR_FLASH_SECTOR_4)
    {
        return FLASH_Sector_3;
    }
    else if(addr < ADDR_FLASH_SECTOR_5)
    {
        return FLASH_Sector_4;
    }
    else if(addr < ADDR_FLASH_SECTOR_6)
    {
        return FLASH_Sector_5;
    }
    else if(addr < ADDR_FLASH_SECTOR_7)
    {
        return FLASH_Sector_6;
    }
    else if(addr < ADDR_FLASH_SECTOR_8)
    {
        return FLASH_Sector_7;
    }
    else if(addr < ADDR_FLASH_SECTOR_9)
    {
        return FLASH_Sector_8;
    }
    else if(addr < ADDR_FLASH_SECTOR_10)
    {
        return FLASH_Sector_9;
    }
    else if(addr < ADDR_FLASH_SECTOR_11)
    {
        return FLASH_Sector_10;
    }

    return FLASH_Sector_11;
}
//读取指定地址的半字(16位数据)
//faddr:读地址
//返回值:对应数据.
u32 FLASH_ReadWord(u32 faddr)
{
    return *(u32 *)faddr;
}

/***********************************************
** Function name:
** Descriptions:        擦除flash
** input parameters:    startAddress 开始地址  count 多少页
** output parameters:   无
** Returned value:      无
*************************************************/
void FLASH_ERASURE(u32 startAddress, u32 endAddress)
{
    FLASH_Status status = FLASH_COMPLETE;
    vu32 Address = startAddress;
    vu16 SectorNow = 0;
    vu16 SectorNext = 0;
    //解锁写保护
    FLASH_Unlock();
    FLASH_DataCacheCmd(DISABLE);
    //VCC=2.7~3.6V之间!!
    SectorNow = FLASH_GetFlashSector(startAddress);
    SectorNext = SectorNow;
    status = FLASH_EraseSector(SectorNow, VoltageRange_3);

    if(endAddress != 0)
    {
        //扫清一切障碍.(对非FFFFFFFF的地方,先擦除)
        while(Address < endAddress)
        {
            SectorNext = FLASH_GetFlashSector(Address);

            //有非0XFFFFFFFF的地方,要擦除这个扇区
            if(SectorNow != SectorNext)
            {
                SectorNow = SectorNext;
                //VCC=2.7~3.6V之间!!
                status = FLASH_EraseSector(SectorNow, VoltageRange_3);

                if(status != FLASH_COMPLETE)
                {
                    //发生错误了
                    break;
                }
            }
            else
            {
                Address += 0x4000;
            }
        }
    }

    //FLASH擦除结束,开启数据缓存
    FLASH_DataCacheCmd(ENABLE);
    FLASH_Lock();//上锁写保护
}


//从指定地址开始写入指定长度的数据
//特别注意:因为STM32F4的扇区实在太大,没办法本地保存扇区数据,所以本函数
//         写地址如果非0XFF,那么会先擦除整个扇区且不保存扇区数据.所以
//         写非0XFF的地址,将导致整个扇区数据丢失.建议写之前确保扇区里
//         没有重要数据,最好是整个扇区先擦除了,然后慢慢往后写.
//该函数对OTP区域也有效!可以用来写OTP区!
//OTP区域地址范围:0X1FFF7800~0X1FFF7A0F
//WriteAddr:起始地址(此地址必须为4的倍数!!)
//pBuffer:数据指针
//NumToWrite:字(32位)数(就是要写入的32位数据的个数.)
void FLASH_Write(u32 WriteAddr, u32 *pBuffer, u32 NumToWrite)
{
    vu32 addrx = 0;

    if(WriteAddr < STM32_FLASH_BASE || WriteAddr % 4)
    {
        //非法地址
        return;
    }

    //解锁
    FLASH_Unlock();
    //写入的起始地址
    addrx = WriteAddr;

    //写数据
    while(addrx < (WriteAddr + NumToWrite))
    {
        //写入数据
        if(FLASH_ProgramWord(addrx, *pBuffer) != FLASH_COMPLETE)
        {
            //写入异常
            break;
        }

        addrx += 4;
        pBuffer++;
    }

    //上锁
    FLASH_Lock();
}

//从指定地址开始读出指定长度的数据
//ReadAddr:起始地址
//pBuffer:数据指针
//NumToRead:字(4位)数
void FLASH_Read(u32 ReadAddr, u32 *pBuffer, u32 NumToRead)
{
    vu32 i;

    for(i = 0; i < NumToRead; i++)
    {
        //读取4个字节.
        pBuffer[i] = FLASH_ReadWord(ReadAddr);
        //偏移4个字节.
        ReadAddr += 4;
    }
}

u32 idAddr[] =
{
    /*STM32F0唯一ID起始地址*/
    0x1FFFF7AC,
    /*STM32F1唯一ID起始地址*/
    0x1FFFF7E8,
    /*STM32F2唯一ID起始地址*/
    0x1FFF7A10,
    /*STM32F3唯一ID起始地址*/
    0x1FFFF7AC,
    /*STM32F4唯一ID起始地址*/
    0x1FFF7A10,
    /*STM32F7唯一ID起始地址*/
    0x1FF0F420,
    /*STM32L0唯一ID起始地址*/
    0x1FF80050,
    /*STM32L1唯一ID起始地址*/
    0x1FF80050,
    /*STM32L4唯一ID起始地址*/
    0x1FFF7590,
    /*STM32H7唯一ID起始地址*/
    0x1FF0F420
};

/*获取MCU的唯一ID*/
void GetSTM32MCUID(u32 *id, MCUTypedef type)
{
    if(id != NULL)
    {
        id[0] = *(u32 *)(idAddr[type]);
        id[1] = *(u32 *)(idAddr[type] + 4);
        id[2] = *(u32 *)(idAddr[type] + 8);
    }
}

u16 GetSTM32FlashSize(void)
{
    return (*(__IO u16 *)(0x1FFF7A22));
}
