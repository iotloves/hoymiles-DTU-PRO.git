#include "littlefs.h"
#include "malloc.h"
#include "spi_flash.h"
#include "string.h"
#include "stdio.h"
#include "SysTick.h"
#include "Memory_Tools.h"
#include "Memory.h"
#ifdef DTU3PRO
#include "iwdg.h"
#else
#include "stm32_wdg.h"
#endif
lfs_t lfs;
#ifdef DTU3PRO
lfs_t lfs1;
#endif
#define FS_PATH_MAX     20
#define FS_PATH_LEN     80
// Read a region in a block. Negative error codes are propogated
// to the user.
int block_device_read(const struct lfs_config *c, lfs_block_t block,
                      lfs_off_t off, void *buffer, u32 size)
{
    if(size == 0)
    {
        return LFS_ERR_NOATTR;
    }
    else
    {
        SPI_FLASH_BufferRead(0, (u8 *)buffer, (block * c->block_size + off), (u16)size);
        return 0;
    }
}

// Program a region in a block. The block must have previously
// been erased. Negative error codes are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int block_device_prog(const struct lfs_config *c, lfs_block_t block,
                      lfs_off_t off, const void *buffer, u32 size)
{
    SPI_FLASH_BufferWrite(0, (u8 *)buffer, (block * c->block_size + off), (u16)size);
    return 0;
}

// Erase a block. A block must be erased before being programmed.
// The state of an erased block is undefined. Negative error codes
// are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int block_device_erase(const struct lfs_config *c, lfs_block_t block)
{
    SPI_FLASH_SectorErase(0, block * c->block_size);
    return 0;
}

// Sync the state of the underlying block device. Negative error codes
// are propogated to the user.
int block_device_sync(const struct lfs_config *c)
{
    return 0;
}
#ifdef DTU3PRO
// Read a region in a block. Negative error codes are propogated
// to the user.
int block_device_read1(const struct lfs_config *c, lfs_block_t block,
                       lfs_off_t off, void *buffer, u32 size)
{
    SPI_FLASH_BufferRead(1, (u8 *)buffer, (block * c->block_size + off), (u16)size);
    return 0;
}

// Program a region in a block. The block must have previously
// been erased. Negative error codes are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int block_device_prog1(const struct lfs_config *c, lfs_block_t block,
                       lfs_off_t off, const void *buffer, u32 size)
{
    SPI_FLASH_BufferWrite(1, (u8 *)buffer, (block * c->block_size + off), (u16)size);
    return 0;
}

// Erase a block. A block must be erased before being programmed.
// The state of an erased block is undefined. Negative error codes
// are propogated to the user.
// May return LFS_ERR_CORRUPT if the block should be considered bad.
int block_device_erase1(const struct lfs_config *c, lfs_block_t block)
{
    SPI_FLASH_SectorErase(1, block * c->block_size);
    return 0;
}

// Sync the state of the underlying block device. Negative error codes
// are propogated to the user.
int block_device_sync1(const struct lfs_config *c)
{
    return 0;
}
#endif
// configuration of the filesystem is provided by this struct
const struct lfs_config lfs_cfg =
{
    // block device operations
    .read = block_device_read,
    .prog = block_device_prog,
    .erase = block_device_erase,
    .sync = block_device_sync,

    // block device configuration
    .read_size = 128,
    .prog_size = 128,
    .block_size = 4096,
    /*分出一个块区用于flash直接存储*/
    .block_count = 4080,
    //.block_count = 4095,
    /*hzwang_20200424*/
    .cache_size = 128,
    .lookahead_size = 128,
    .block_cycles = 100,
};
#ifndef Bootloader
#ifdef DTU3PRO
const struct lfs_config lfs_cfg1 =
{
    // block device operations
    .read = block_device_read1,
    .prog = block_device_prog1,
    .erase = block_device_erase1,
    .sync = block_device_sync1,

    // block device configuration
    .read_size = 128,
    .prog_size = 128,
    .block_size = 4096,
    .block_count = 4096,
    .cache_size = 128,
    .lookahead_size = 128,
    .block_cycles = 100,
};
#endif
#endif
/* Open or create a file */
int lf_open(u8 DiskNum, lfs_file_t *file, const char *path, int flags)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_open(&lfs, file, path, flags);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_open(&lfs1, file, path, flags);
    }

#endif
#endif
}
/* Close an open file object */
int lf_close(u8 DiskNum, lfs_file_t *file)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_close(&lfs, file);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_close(&lfs1, file);
    }

#endif
#endif
}
/* Read data from a file */
int lf_read(u8 DiskNum, lfs_file_t *file, void *buffer, u32 size, u32 *br)
{
    if(DiskNum == 0)
    {
        *br = (u32)lfs_file_read(&lfs, file, buffer, (u16)size);
    }

#ifdef DTU3PRO
#ifndef Bootloader
    else
    {
        *br = (u32)lfs_file_read(&lfs1, file, buffer, (u16)size);
    }

#endif
#endif
    return (int) * br;
}
/* Write data to a file */
int lf_write(u8 DiskNum, lfs_file_t *file, const void *buffer, u32 size, u32 *bw)
{
    if(DiskNum == 0)
    {
        *bw = (u32)lfs_file_write(&lfs, file, buffer, size);
    }

#ifndef Bootloader
#ifdef DTU3PRO
    else
    {
        *bw = (u32)lfs_file_write(&lfs1, file, buffer, size);
    }

#endif
#endif
    return (int) * bw;
}
int lf_sync(u8 DiskNum, lfs_file_t *file)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_sync(&lfs, file);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_sync(&lfs1, file);
    }

#endif
#endif
}
/* Move file pointer of a file object */
int lf_seek(u8 DiskNum, lfs_file_t *file, u32 off)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_seek(&lfs, file, (int)off, LFS_SEEK_SET);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_seek(&lfs1, file, (int)off, LFS_SEEK_SET);
    }

#endif
#endif
}
int lf_tell(u8 DiskNum, lfs_file_t *file)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_tell(&lfs, file);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_tell(&lfs1, file);
    }

#endif
#endif
}
int lf_seekend(u8 DiskNum, lfs_file_t *file)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_file_seek(&lfs, file, (int)lf_fsize(DiskNum, file), LFS_SEEK_SET);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_file_seek(&lfs1, file, (int)lf_fsize(DiskNum, file), LFS_SEEK_SET);
    }

#endif
#endif
}
u32 lf_fsize(u8 DiskNum, lfs_file_t *file)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return (u32)lfs_file_size(&lfs, file);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return (u32)lfs_file_size(&lfs1, file);
    }

#endif
#endif
}
/* Open a directory */
int lf_opendir(u8 DiskNum, lfs_dir_t *dir, const char *path)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_dir_open(&lfs, dir, path);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_dir_open(&lfs1, dir, path);
    }

#endif
#endif
}
/* Close an open directory */
int lf_closedir(u8 DiskNum, lfs_dir_t *dir)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_dir_close(&lfs, dir);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_dir_close(&lfs1, dir);
    }

#endif
#endif
}
/* Read a directory item */
int lf_readdir(u8 DiskNum, lfs_dir_t *dir, struct lfs_info *info)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_dir_read(&lfs, dir, info);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_dir_read(&lfs1, dir, info);
    }

#endif
#endif
}
/* Create a sub directory */
int lf_mkdir(u8 DiskNum, const char *path)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_mkdir(&lfs, path);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_mkdir(&lfs1, path);
    }

#endif
#endif
}
/* Delete an existing file or directory */
int lf_unlink(u8 DiskNum, const char *path)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_remove(&lfs, path);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_remove(&lfs1, path);
    }

#endif
#endif
}
/* Rename/Move a file or directory */
int lf_rename(u8 DiskNum, const char *oldpath, const char *newpath)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_rename(&lfs, oldpath, newpath);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_rename(&lfs1, oldpath, newpath);
    }

#endif
#endif
}
/* Get file status */
int lf_stat(u8 DiskNum, const char *path, struct lfs_info *info)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_stat(&lfs, path, info);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_stat(&lfs1, path, info);
    }

#endif
#endif
}
/* Mount a logical drive */
int lf_mount(u8 DiskNum)

{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_mount(&lfs, &lfs_cfg);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_mount(&lfs1, &lfs_cfg1);
    }

#endif
#endif
}
int lf_format(u8 DiskNum)
{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_format(&lfs, &lfs_cfg);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_format(&lfs1, &lfs_cfg1);
    }

#endif
#endif
}
/* Unmount a logical drive */
int lf_unmount(u8 DiskNum)

{
#ifndef Bootloader
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
#endif
        return lfs_unmount(&lfs);
#ifndef Bootloader
#ifdef DTU3PRO
    }
    else
    {
        return lfs_unmount(&lfs1);
    }

#endif
#endif
}

#ifndef Bootloader
//打印所有文件夹
void lf_PrintfAllFiles(u8 DiskNum, char *path)
{
    char *DIRPath[FS_PATH_MAX] = {NULL};
    char *FilePath = NULL;
    struct lfs_info info;
    vu8 i = 0;
    vu8 j = 0;
    vu8 k = 0;
    vu8 Exist = 0;

    for(i = 0; i < FS_PATH_MAX; i++)
    {
        DIRPath[i] = mymalloc(FS_PATH_LEN * sizeof(char));
        //        if(DIRPath[i] == NULL)
        //        {
        //            return;
        //        }
        memset(DIRPath[i], 0, FS_PATH_LEN * sizeof(char));
    }

    i = 0;
    sprintf(DIRPath[i], "%s", path);
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_REG)
        {
            memset(FilePath, 0, PathLen * sizeof(char));
            sprintf(FilePath, "%s/%s", DIRPath[i], info.name);
            printf("File: %s size:%d \n\n", FilePath, info.size);
        }
        else if(info.type == LFS_TYPE_DIR)
        {
            if((strncmp(info.name, ".", 1) != 0) && (strncmp(info.name, "..", 2) != 0) && (strncmp(info.name, path, strlen(path)) != 0))
            {
                Exist = 0;
                memset(FilePath, 0, PathLen * sizeof(char));
                sprintf(FilePath, "%s/%s size:%d \n\n", DIRPath[i], info.name, info.size);

                for(k = 0; k < j; k++)
                {
                    if(strncmp(FilePath, DIRPath[k], strlen(FilePath)) == 0)
                    {
                        Exist = 1;
                    }
                }

                if(Exist == 0)
                {
                    j++;

                    if(j < FS_PATH_MAX)
                    {
                        memset(DIRPath[j], 0, FS_PATH_LEN * sizeof(char));
                        sprintf(DIRPath[j], "%s/%s", DIRPath[i], info.name);
                        printf("dir  %s size:%d\n\n", DIRPath[j], info.size);
                    }
                }
            }
        }
        else if(info.type == 0)
        {
            if(i >= j)
            {
                break;
            }
            else if(i < j)
            {
                i++;
                lf_closedir(DiskNum, &dir);
                lf_opendir(DiskNum, &dir, DIRPath[i]);
            }
        }
    }

    for(i = 0; i < FS_PATH_MAX; i++)
    {
        myfree(DIRPath[i]);
    }

    lf_closedir(DiskNum, &dir);
    myfree(FilePath);
}

//罗列所有文件
void lf_ListAllFiles(u8 DiskNum, char *path, char *FileName[], u16 *Num, u16 Total)
{
    char *DIRPath[FS_PATH_MAX] = {NULL};
    char *FilePath = NULL;
    struct lfs_info info;
    vu16 i = 0;
    vu16 j = 0;
    DIRPath[i] = mymalloc(FS_PATH_LEN * sizeof(char));
    //    if(DIRPath[i] == NULL)
    //    {
    //        return;
    //    }
    memset(DIRPath[i], 0, FS_PATH_LEN * sizeof(char));
    sprintf(DIRPath[i], "%s", path);
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_REG)
        {
            sprintf(FileName[*Num], "%s", info.name);
            *Num = *Num + 1;

            if(*Num >= Total)
            {
                break;
            }
        }
        else if(info.type == LFS_TYPE_DIR)
        {
            if((strncmp(info.name, ".", 1) != 0) && (strncmp(info.name, "..", 2) != 0) && (strncmp(info.name, path, strlen(path)) != 0))
            {
                j++;

                if(j >= FS_PATH_MAX)
                {
                    break;
                }

                DIRPath[j] = mymalloc(FS_PATH_LEN * sizeof(char));
                //                if(DIRPath[j] == NULL)
                //                {
                //                    return;
                //                }
                memset(DIRPath[j], 0, FS_PATH_LEN * sizeof(char));
                sprintf(DIRPath[j], "%s/%s", path, info.name);
                //printf("dir  %s \n", DIRPath[j]);
            }
        }
        else if(info.type == 0)
        {
            if(i >= j)
            {
                for(i = 0; i < FS_PATH_MAX; i++)
                {
                    if(DIRPath[i] != NULL)
                    {
                        myfree(DIRPath[i]);
                    }
                }

                break;
            }
            else if(i < j)
            {
                i++;
                lf_closedir(DiskNum, &dir);
                lf_opendir(DiskNum, &dir, DIRPath[i]);
            }
        }
    }

    for(i = 0; i < FS_PATH_MAX; i++)
    {
        if(DIRPath[i] != NULL)
        {
            myfree(DIRPath[i]);
        }
    }

    lf_closedir(DiskNum, &dir);
    myfree(FilePath);
}

//罗列文件夹中文件
void lf_ListDirFiles(u8 DiskNum, char *path, u32 *FileName, u16 *Num, u16 Total, u8 NameLength)
{
    char *FilePath = NULL;
    struct lfs_info info;
    vu8 FileCopy = 0;
    vu32 FileNameNum = 0;
    vu16 i = 0;
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_REG)
        {
            FileNameNum = 0;
            FileNameNum = astr2int(info.name, NameLength);
            FileCopy = 1;

            for(i = 0; i < *Num; i++)
            {
                if(FileNameNum == 0)
                {
                    FileCopy = 0;
                    break;
                }
                else if(FileName[i] == FileNameNum)
                {
                    FileCopy = 0;
                    break;
                }
            }

            if(FileCopy == 1)
            {
                FileName[*Num] = FileNameNum;
                *Num = *Num + 1;
            }

            if(*Num >= Total)
            {
                break;
            }
        }
        else if(info.type == 0)
        {
            break;
        }
    }

    lf_closedir(DiskNum, &dir);
    myfree(FilePath);
}



//罗列文件夹中文件
void lf_ListDirFilesChar(u8 DiskNum, char *path, char *Same_Buf, u16 Same_len, char **FileName, u16 *Num, u16 Total)
{
    char *FilePath = NULL;
    struct lfs_info info;
    vu16 i = 0;
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_REG)
        {
            if(strncmp(info.name, Same_Buf, Same_len) == 0)
            {
                memcpy(FileName[*Num], info.name, strlen(info.name));
                *Num = *Num + 1;

                if(*Num >= Total)
                {
                    break;
                }
            }
        }
        else if(info.type == 0)
        {
            break;
        }
    }

    lf_closedir(DiskNum, &dir);
    myfree(FilePath);
}
//罗列路径中的全部文件夹
void lf_ListDirs(u8 DiskNum, char *path, u32 *DirName, u16 *Num)
{
    u8 DirMax = 20;
    struct lfs_info info;
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);
    *Num = 0;

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_DIR)
        {
            if((strncmp(info.name, ".", 1) != 0) && (strncmp(info.name, "..", 2) != 0) && (strncmp(info.name, path, strlen(path)) != 0))
            {
                DirName[*Num] = astr2int(info.name, 12);
                *Num = *Num + 1;
            }
        }
        else if(info.type == 0)
        {
            break;
        }
    }

    lf_closedir(DiskNum, &dir);
}

////罗列文件夹中文件
//void lf_ListDirFiles(u8 DiskNum, char *path, char *FileName[], u16 *Num, u16 Total)
//{
//    char *DIRPath;
//    char *FilePath;
//    struct lfs_info info;
//    u16 i = 0;
//    u16 j = 0;
//    DIRPath = mymalloc(100 * sizeof(char));
//    memset(DIRPath, 0, 100 * sizeof(char));
//    sprintf(DIRPath, "%s", path);
//    FilePath = mymalloc(200 * sizeof(char));
//    lfs_dir_t dir;
//    lf_opendir(DiskNum, &dir, path);

//    while(1)
//    {
//#ifdef DTU3PRO
//#ifdef DTU_IAP
//    IWDG_Feed();
//#endif
//#else
//    WDG_Feed(); //喂狗
//#endif
//        lf_readdir(DiskNum, &dir, &info);

//        if(info.type == LFS_TYPE_REG)
//        {
//            sprintf(FileName[*Num], "%s", info.name);
//            *Num = *Num + 1;

//            if(*Num >= Total)
//            {
//                break;
//            }
//        }
//        else if(info.type == 0)
//        {
//            break;
//        }
//    }

//    myfree(DIRPath);
//    lf_closedir(DiskNum, &dir);
//    myfree(FilePath);
//}



////删除目录下所有内容
//void lf_DeleteAllFiles(u8 DiskNum, char *path)
//{
//    char *DIRPath[10];
//    char *FilePath;
//    struct lfs_info info;
//    u16 i = 0;
//    u16 j = 0;
//    DIRPath[i] = mymalloc(100 * sizeof(char));
//    memset(DIRPath[i], 0, 100 * sizeof(char));
//    sprintf(DIRPath[i], "%s", path);
//    FilePath = mymalloc(200 * sizeof(char));
//    lfs_dir_t dir;
//    lf_opendir(DiskNum, &dir, path);

//    while(1)
//    {
//#ifdef DTU3PRO
//#ifdef DTU_IAP
//        IWDG_Feed();
//#endif
//#else
//        WDG_Feed(); //喂狗
//#endif
//        lf_readdir(DiskNum, &dir, &info);
//        if(info.type == LFS_TYPE_REG)
//        {
//            memset(FilePath, 0, 200 * sizeof(char));
//            sprintf(FilePath, "%s/%s", DIRPath[i], info.name);
//            lf_unlink(DiskNum, FilePath);
//        }
//        else if(info.type == LFS_TYPE_DIR)
//        {
//            if((strncmp(info.name, ".", 1) != 0) && (strncmp(info.name, "..", 2) != 0) && (strncmp(info.name, path, strlen(path)) != 0))
//            {
//                j++;
//                DIRPath[j] = mymalloc(100 * sizeof(char));
//                memset(DIRPath[j], 0, 100 * sizeof(char));
//                sprintf(DIRPath[j], "%s/%s", path, info.name);
//            }
//        }
//        else if(info.type == 0)
//        {
//            if(i >= j)
//            {
//                for(i = 0; i < j; i++)
//                {
//                    if(lf_opendir(DiskNum, &dir, DIRPath[i]) != LFS_ERR_OK)
//                    {
//                        lf_closedir(DiskNum, &dir);
//                        lf_unlink(DiskNum, DIRPath[i]);
//                    }
//                }

//                for(i = 0; i < 10; i++)
//                {
//                    myfree(DIRPath[i]);
//                }

//                break;
//            }
//            else if(i < j)
//            {
//                i++;
//                lf_closedir(DiskNum, &dir);
//                lf_opendir(DiskNum, &dir, DIRPath[i]);
//            }
//        }
//    }

//    lf_closedir(DiskNum, &dir);
//    myfree(FilePath);
//}


//删除目录下所有内容
void lf_DeleteAllFiles(u8 DiskNum, char *path)
{
    char *DIRPath = NULL;
    char *FilePath = NULL;
    struct lfs_info info;
    DIRPath = mymalloc(FS_PATH_LEN * sizeof(char));
    //    if(DIRPath == NULL)
    //    {
    //        return;
    //    }
    memset(DIRPath, 0, FS_PATH_LEN * sizeof(char));
    sprintf(DIRPath, "%s", path);
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        if(info.type == LFS_TYPE_REG)
        {
            memset(FilePath, 0, PathLen * sizeof(char));
            sprintf(FilePath, "%s/%s", DIRPath, info.name);
            lf_unlink(DiskNum, FilePath);
        }
        else if(info.type == 0)
        {
            break;
        }
    }

    lf_closedir(DiskNum, &dir);
    myfree(DIRPath);
    myfree(FilePath);
}

//目录大小
void lf_DirectorySize(u8 DiskNum, char *path, u32 *Size, u16 *Num)
{
    char *DIRPath[FS_PATH_MAX] = {NULL};
    char *FilePath = NULL;
    struct lfs_info info;
    vu16 i = 0;
    vu16 j = 0;

    for(i = 0; i < FS_PATH_MAX; i++)
    {
        DIRPath[i] = mymalloc(FS_PATH_LEN * sizeof(char));
        //        if(DIRPath[i] == NULL)
        //        {
        //            return;
        //        }
        memset(DIRPath[i], 0, FS_PATH_LEN * sizeof(char));
    }

    i = 0;
    sprintf(DIRPath[0], "%s", path);
    FilePath = mymalloc(PathLen * sizeof(char));
    //    if(FilePath == NULL)
    //    {
    //        return;
    //    }
    lfs_dir_t dir;
    lf_opendir(DiskNum, &dir, path);

    while(1)
    {
        IWDG_Feed();
        lf_readdir(DiskNum, &dir, &info);

        //    LFS_TYPE_REG = 0x11,
        //    LFS_TYPE_DIR = 0x22,
        //    LFS_TYPE_SUPERBLOCK = 0x2e,
        if(info.type == LFS_TYPE_REG)
        {
            memset(FilePath, 0, PathLen * sizeof(char));
            sprintf(FilePath, "%s/%s", DIRPath[i], info.name);
            *Size = *Size + info.size;
            *Num = *Num + 1;
        }
        else if(info.type == LFS_TYPE_DIR)
        {
            if((strncmp(info.name, ".", 1) != 0) && (strncmp(info.name, "..", 2) != 0) && (strncmp(info.name, path, strlen(path)) != 0))
            {
                j++;
                memset(DIRPath[j], 0, FS_PATH_LEN * sizeof(char));
                sprintf(DIRPath[j], "%s/%s", DIRPath[i], info.name);
            }
        }
        else if(info.type == 0)
        {
            lf_closedir(DiskNum, &dir);

            if(i >= j)
            {
                break;
            }
            else if(i < j)
            {
                i++;
                lf_opendir(DiskNum, &dir, DIRPath[i]);
            }
        }
    }

    lf_closedir(DiskNum, &dir);

    for(i = 0; i < FS_PATH_MAX; i++)
    {
        memset(DIRPath[i], 0, FS_PATH_LEN * sizeof(char));
        myfree(DIRPath[i]);
    }

    myfree(FilePath);
}
//剩余容量
void lf_LaveCapacity(u8 DiskNum, char *path, u32 *capacity, u16 *Num)
{
    vu32 Size = 0;
    lf_DirectorySize(DiskNum, path, (u32 *)&Size, Num);
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
        *capacity = (lfs_cfg.block_size * lfs_cfg.block_count - (1024 + 512) * 1024) - Size;
#ifdef DTU3PRO
    }
    else
    {
        *capacity = (lfs_cfg1.block_size * lfs_cfg1.block_count - (1024 + 512) * 1024) - Size;
    }

#endif
}
u32 lf_Total_Capacity(u8 DiskNum)
{
#ifdef DTU3PRO

    if(DiskNum == 0)
    {
#endif
        return lfs_cfg.block_size * lfs_cfg.block_count;
#ifdef DTU3PRO
    }
    else
    {
        return lfs_cfg1.block_size * lfs_cfg1.block_count;
    }

#endif
}
#endif
#ifdef TEST_M
///*测试*/
//int lfs_test(void)
//{
//    lfs_file_t lfs_file;
//    // read current count
//    u32 boot_count = 0;
//    u32 num;
//    int err = lf_mount(0);

//    // reformat if we can't mount the filesystem
//    // this should only happen on the first boot
//    if(err)
//    {
//#ifdef DEBUG
//        printf("lfs need formart flash to mount filesystem: %d\n", 0);
//#endif
//        lf_format(0);
//        lf_mount(0);
//    }

//    lf_open(0, &lfs_file, "boot_count", LFS_O_RDWR | LFS_O_CREAT);
//    lf_read(0, &lfs_file, &boot_count, sizeof(boot_count), &num);
//    // update boot count
//    boot_count += 1;
//    lf_seek(0, &lfs_file, 0);
//    lf_write(0, &lfs_file, &boot_count, sizeof(boot_count), &num);
//    // remember the storage is not updated until the file is closed successfully
//    lf_close(0, &lfs_file);
//    // release any resources we were using
//    lfs_unmount(0);
//    // print the boot count
//#ifdef DEBUG
//    printf("boot_count: %d\r\n", boot_count);
//#endif
//    return 0;
//}


//void lfs_test_demo(void)
//{
//    static lfs_file_t lfs_file[3];
//    char *test_buffer;
//    lfs_dir_t g_dir[2];
//    u32 lfs_free_spcae_size = 0;
//    int lfs_dir_pos = 0;
//    struct lfs_info lfs_file_info = { 0 };
//    int err;
//    u32 num;
//    test_buffer = mymalloc(1024 * sizeof(char));
//    memset(test_buffer, 0, 1024 * sizeof(char));
//    // mount the filesystem
//    //    err = lf_mount(0);
//    //    // reformat if we can't mount the filesystem
//    //    // this should only happen on the first boot
//    //    if(err)
//    //    {
//    //#ifdef DEBUG
//    //        printf("lfs need formart flash to mount filesystem: %d\n", 0);
//    //#endif
//    //        lf_format(0);
//    //        lf_mount(0);
//    //    }
//    err = lf_opendir(0, &g_dir[0], "0:");

//    if(err == LFS_ERR_NOENT)
//    {
//        lf_mkdir(0, "0:");
//#ifdef DEBUG
//        printf("lfs makedir\n");
//#endif
//    }

//    lf_fsize(0, lfs_file);
//    lf_closedir(0, &g_dir[0]);
//    // read current count
//    err = lf_open(0, &lfs_file[0], "0:/boot1", LFS_O_RDWR | LFS_O_CREAT);

//    if(err)
//    {
//#ifdef DEBUG
//        printf("lfs open1 error:%d \n", err);
//#endif
//    }

//    //if not set cursor will read data error
//    lf_seek(0, &lfs_file[0], 0);
//    memset(test_buffer, 0, 1024 * sizeof(char));
//    lf_seekend(0, &lfs_file[0]);
//    sprintf(test_buffer, "hello word\n");
//    err = lf_write(0, &lfs_file[0], test_buffer, strlen(test_buffer), &num);

//    if(err)
//    {
//#ifdef DEBUG
//        printf("lfs write1 len:%d \n", err);
//#endif
//    }

//    lf_seek(0, &lfs_file[0], 0);
//    err = lf_read(0, &lfs_file[0], test_buffer, 1024, &num);

//    if(err)
//    {
//        printf("lfs read len:%d \n", err);
//    }

//    printf("lfs read data: %s\n", test_buffer);
//    memset(test_buffer, 0, 1024 * sizeof(char));
//    // remember the storage is not updated until the file is closed successfully
//    //err = lf_close(&lfs_file[0]);
//    //printf("lfs close error:%d \n", err);
//    err = lf_open(0, &lfs_file[1], "0:/boot1", LFS_O_RDWR | LFS_O_CREAT);

//    if(err)
//    {
//        printf("lfs open1 error:%d \n", err);
//    }

//    err = lf_read(0, &lfs_file[1], test_buffer, 1024, &num);

//    if(err)
//    {
//        printf("lfs read len:%d \n", err);
//    }

//    printf("lfs read data: %s\n", test_buffer);
//    err = lf_close(0, &lfs_file[0]);
//    printf("lfs close error:%d \n", err);
//    err = lf_close(0, &lfs_file[1]);
//    printf("lfs close error:%d \n", err);
//    //f_stat("0:/boot1", &lfs_file_info);
//    //printf("lfs get 0:/boot1 file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    //memset(&lfs_file_info, 0x00, sizeof(lfs_file_info));
//    //f_stat("0:", &lfs_file_info);
//    //printf("lfs get 0: file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    //lf_DeleteAllFiles("0:");
//    //    printf("capacity %d\n", lf_RemainingCapacity());
//    //    printf("size %d\n", lf_DirectorySize("0:"));
//    lf_PrintfAllFiles(0, "0:");
//    //    lf_opendir(&g_dir[1], "0:/System");
//    //    lfs_dir_pos = lf_tell(&lfs_file[1]);
//    //    printf("lfs dir tell = %d \n", lfs_dir_pos);
//    //    lf_readdir(&g_dir[1], &lfs_file_info);
//    //    printf("lfs get 0:/data file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    //    lf_readdir(&g_dir[1], &lfs_file_info);
//    //    printf("lfs get 0:/data file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    //    lf_readdir(&g_dir[1], &lfs_file_info);
//    //    printf("lfs get 0:/data file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    //    lf_readdir(&g_dir[1], &lfs_file_info);
//    //    printf("lfs get 0:/data file info type = %d size = %d  filename = %s\n", lfs_file_info.type, lfs_file_info.size, lfs_file_info.name);
//    // release any resources we were using
//    //    err = lfs_unmount(&lfs);
//    //    if(err)
//    //    {
//    //        printf("lfs ummount error: %d\n", 0);
//    //    }
//    myfree(test_buffer);
//}
#endif
