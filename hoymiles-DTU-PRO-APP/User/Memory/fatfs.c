#include "fatfs.h"
#include "malloc.h"
#include "string.h"
#include "stdio.h"
#include "Memory.h"
#include "sdio_sd.h"
#ifdef DTU3PRO
/* FatFs文件系统对象 */
FATFS fs;

FIL fnew;
FRESULT res_flash;
FILINFO fno;
#endif

void Fatfs_Init(void)
{
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", Peripheral);
    res_flash = f_mount(&fs, path, 1);
    //    if(res_flash == FR_NO_FILESYSTEM)
    //    {
    //        printf("》FLASH还没有文件系统，即将进行格式化...\r\n");
    //        res_flash = f_mkfs(path, 0, 0);
    //        if(res_flash == FR_OK)
    //        {
    //            printf("》FLASH已成功格式化文件系统。\r\n");
    //            /* 格式化后，先取消挂载 */
    //            res_flash = f_mount(NULL, path, 1);
    //            /* 重新挂载   */
    //            res_flash = f_mount(&fs, path, 1);
    //        }
    //        else
    //        {
    //            printf("《《格式化失败。》》\r\n");
    //        }
    //    }
    //    else if(res_flash != FR_OK)
    //    {
    //        printf("！！外部Flash挂载文件系统失败。(%d)\r\n", res_flash);
    //        printf("！！可能原因：SPI Flash初始化不成功。\r\n");
    //    }
    //    else
    //    {
    //        printf("》文件系统挂载成功，可以进行测试\r\n");
    //    }
    myfree(path);
}


u32 Get_Disk_Free(u8 Disk)
{
    char *path = NULL;
    FATFS *pfs = NULL;
    //文件操作结果
    FRESULT res;
    DWORD fre_clust;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", Disk);

    if(SD_GetState() != SD_CARD_ERROR)
    {
        /* 获取设备信息和空簇大小 */
        res = f_getfree((char *)path, &fre_clust, &pfs);
        myfree(path);
#ifdef DEBUG
        printf("res %d\n", res);
#endif

        if(res == FR_OK)
        {
            return (u32)((fre_clust * pfs->csize * (pfs->ssize / 512) >> 1));
        }
    }

    return 0;
}

