#ifndef _MEMORY_TOOL_H
#define _MEMORY_TOOL_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "usart.h"
#else
#include "stm32f10x.h"
#include "usart_debug.h"
#endif

typedef struct
{
    vu8 disk;
    vu32 size;
    vu32 off;
    void *path;
    void *buffer;

} fs_write;

void Queue_init(void);
void Queue_enter(fs_write data);
u8 Queue_Out(fs_write *data);
u8 Queue_IsEmpty(void);
u32 astr2int(char *buf, u8 len);
u32 hstr2int(char *buf, u8 len);
u16 CalcCRC8(u8 *TargetAddr, u32 nub);
u8 Queue_IsSameFile(fs_write data);
u8 Ascii2Hex(u8 *ascii, u8 *hex, u8 asciiLen);
u8 Hex2Ascii(u8 *ascii, u8 *hex, u8 hexLen);
#ifdef DTU3PRO
void hex_ascii(u8 *b_hex, u8 *b_asc2, u8 len);
u8 McuAsciiToHex(u8 ascii); //ascii 转化16进制数
void ascii_to_hex(u8 *sour, u8 *dest, u8 len);
#endif
#endif
