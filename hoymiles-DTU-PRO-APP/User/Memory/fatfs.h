#ifndef __FATFS_H
#define __FATFS_H

#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "ff.h"

#endif


u32 Get_Disk_Free(u8 Disk);
void Fatfs_Init(void);


#endif