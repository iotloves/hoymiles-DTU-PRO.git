#ifndef __DISKOPERATION_H
#define __DISKOPERATION_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif

uint8_t USB_Mount(void);

uint32_t USB_Get_Free(uint32_t *total, uint32_t *free);
uint32_t USB_Get_Program_Size(void);
uint32_t USB_Read_Program(char *buffer, uint32_t Offset, uint32_t len);
uint8_t USB_DTU_Program_Check(u16 Hardware);
uint8_t USB_DTU_Program_Copy(void);
uint8_t USB_Comparison(void);
#endif