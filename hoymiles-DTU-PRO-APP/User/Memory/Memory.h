#ifndef __MEMORY_H
#define __MEMORY_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "spi_flash.h"
#include "SunSpec.h"
#ifndef Bootloader
#include "usart.h"
#include "AntiReflux.h"
#endif
#else
#include "stm32f10x.h"
#include "stm32f10x_pwr.h"
#include "usart_debug.h"
#define FS_MAX_DIR 1
#endif
#ifndef Bootloader
#include "stdbool.h"
#include "rtc.h"
#include "usart_nrf.h"
#include "usart_nrf3.h"
#endif
//#define MEMORY_DEBUG_TEXT
#ifdef MEMORY_DEBUG_TEXT
#define MEMORY_DAY_MAX_SIZE  96
#endif



#define DefaultDisk        0
#ifdef DTU3PRO
#define ExpansionDisk      1
#define Peripheral         2
#endif
#define DownloadSize       2097152
#define SystemSize         400
#ifndef DTU3PRO
#define HardwareSize       9000
#else
#define HardwareSize       3672016
#endif
#define DataAverageSize    40
#define RemainQuant        5
#define DayNum             ((24*60)/15)
#ifndef DTU3PRO
#define ReadMaxSize        10*1024
#else
#define ReadMaxSize        15*1024
#endif
#define InverterDetailSize 120
#define InverterMajorSize  60
#define DtuDetailSize      100
#define DtuMajorSize       300


#define PathLen            200
#define BufferLen          2048
//设备状态标准 0：无设备
#define NODISK             0
//1：设备初格式化不可用
#define UNFORMATTED        1
//2：设备挂载成功可用
#define DISKOK             2
#define ERROR              0xF7
#define FATFSOK            0xEE
#define DOWNSIZE           2048
#define SYSTEMSIZE         512
#define HARDWARESIZE       1024

#define START_LEN           6
#define PACK_NO_LEN         3
#define PACK_ALL_LEN        2
#define HOUR_LEN            3
#define MIN_LEN             3
#define SEC_LEN             2
#define DATA_LEN            4
#define CRC_LEN             4
#define END_LEN             3
#define MARK_LEN            5
#define MARK_RES_LEN        7
#define DATA_SAVE_ALL       (START_LEN+PACK_NO_LEN+PACK_ALL_LEN+HOUR_LEN+MIN_LEN+SEC_LEN+DATA_LEN+CRC_LEN+DATA_LEN+END_LEN)


#define LIST_START_LEN      10
#define START_ADDR_LEN      10
#define END_ADDR_LEN        10
//#define DIR_OFF_LEN         10
#define ABSOLUTE_TIME       10
#define RESERVED_SPACE      40
#define LIST_END_LEN        7
#define LIST_NEXT           (LIST_START_LEN + PACK_NO_LEN + PACK_ALL_LEN + START_ADDR_LEN + \
                             END_ADDR_LEN + DATA_LEN+MARK_LEN + MARK_RES_LEN + LIST_END_LEN + \
                             ABSOLUTE_TIME + RESERVED_SPACE)
#define MAX_ONE_LINE_LEN    100

#define ALARM_LIST_NEXT     (LIST_START_LEN + PACK_NO_LEN + PACK_ALL_LEN + START_ADDR_LEN + \
                             END_ADDR_LEN + DATA_LEN + MARK_LEN + MARK_RES_LEN + LIST_END_LEN + \
                             ABSOLUTE_TIME)

#define DIR_START           9
#define DIR_STYLE           7
#define DIR_END             6
#define DIR_NEXT            (DIR_START + DIR_STYLE + START_ADDR_LEN + END_ADDR_LEN + DATA_LEN + \
                             MARK_LEN + MARK_RES_LEN+ DIR_END + ABSOLUTE_TIME + RESERVED_SPACE)

#define History_Style        1
#define AlarmDt_Style        2


//DTU的hex文件一行去掉：为42个 微逆的hex文件为74个  //74//42 + 32
#define MAX_PRO_PACKAGE     42
#define SCAN_TIME_HOURS     23
#define SCAN_TIME_MINUTES   0
#define SCAN_TIME_SECONDS   0
#define RESERVE_SPACE       (2*1024*1024+512*1024)
#define DOWN_SPACE          (2*1024*1024)
#define LIST_DAY_NUM        5

#define DEFAULT_MAX_NUM     20
#define EXPANSION_MAX_NUM   30

#define DTU_INFO_MAX        512
#define DTU_DETAIL_MAX      160
#define DTU_MAJOR_MAX       280
#define MI_SN_MAX           4096
#define MI_SN_MAX_SIZE      40
#define SAVE_NUM_MAX        65520

#define GPST_SIZE_MAX       150
#define SUNSPEC_SIZE_MAX    10
#ifdef DTU3PRO
#define SERIAL_NUM          0x0A00

typedef union
{
    struct
    {
        //升级标志
        vu8 UpgradeSign[10];
        //服务器域名
        vu8 ServerDomainName[40];
        //服务器端口号
        vu16 ServerPort;
        //APN
        vu8 APN[20];
        //网络模式
        vu8 netmode_select;
        //硬件小版本号
        vu8 small_version;
        //CRC
        vu16 cfg_crc;

    } Property;
    vu8 Data[76];
} SystemCfg;

typedef union
{
    struct
    {
        //升级标志
        vu8 UpgradeSign[10];
        //服务器域名
        vu8 ServerDomainName[40];
        //服务器端口号
        vu16 ServerPort;
        //APN
        vu8 APN[20];
        //网络模式
        vu8 netmode_select;
        //硬件小版本号
        vu8 small_version;
        //APN2
        vu8 APN2[20];
        //预留
        vu8 reserve[100];
        //CRC
        vu16 cfg_crc;

    } Property;
    vu8 Data[196];
} SystemCfgNew;
#endif


enum fs_state
{
    fp_init     = 0,
    fp_idle     = 1,
    fp_open     = 2,
    fp_write    = 3,
    fp_close    = 4,
    fp_check0   = 10,
    fp_check1   = 11,
    fp_check2   = 12,
    fp_check3   = 13,
    fp_check4   = 14,
    fp_check5   = 15,
    fp_check6   = 16,
    fp_check7   = 17,
    fp_delete   = 20,
};
enum Delete_state
{
    Dele_Default    = 0,
    Dele_Expansion  = 1,
    Dele_Compare    = 2,
    Dele_Dat        = 3,
    Dele_List       = 4,
    Dele_DIR        = 5,
    Dele_Alarm_List = 6,
    Dele_Alarm      = 7,
    Dele_End        = 8,
};

typedef  union
{
    struct
    {
        //当前读取位置
        vu8 Warn_Read_Place;
        //微逆告警传输成功数量
        vu16 Warn_Read_Num[100];
        //当前读取日期
        vu32 Warn_Read_Data;
    } Property;
    vu8 Data[205];
} WarnIndex;

/*文件初始化及写操作*/
void File_Processing(void);
/*MI硬件信息保存*/
#ifndef Bootloader
//删除InverterMajor文件
void InverterMajor_Delete(void);
//写InverterMajor文件
u16 InverterMajor_Write(InverterMajor *InverterMajorBuf, u16 Pv_nub);
u16 InverterMajor_Read(InverterMajor *InverterMajorBuf);

void InverterDetail_Delete(void);
u16 InverterDetail_Write(InverterDetail *InverterDetailBuf, u16 InverterDetailOff, u16 InverterNum);
u16 InverterDetail_Read(InverterDetail *InverterDetailBuf, u16 InverterDetailOff, u16 InverterNum);

void InverterReal_Delete(void);
u16 InverterReal_Write(InverterReal *InverterRealBuf, u16 Pv_nub);
u16 InverterReal_Read(InverterReal *InverterRealBuf);

#ifdef DTU3PRO
void MeterMajor_Delete(void);
u16 MeterInfor_Write(MeterMajor *MeterMajorBuf, u16 Meter_nub);
u16 MeterMajor_Read(MeterMajor *MeterMajorBuf);
#endif

void System_DtuDetail_Delete(void);
u16 System_DtuDetail_Write(DtuDetail *DtuDetailbuf);
u16 System_DtuDetail_Read(DtuDetail *DtuDetailbuf);

void System_DtuMajor_Delete(void);
u16 System_DtuMajor_Write(DtuMajor *DtuMajorbuf);
u16 System_DtuMajor_Read(DtuMajor *DtuMajorbuf);

void ClearDownloadFile(void);


u32 RealTime_Data_Pack(u8 PackNum, u8 PackAll, char *Data, u32 Len);
u32 RealTime_Data_Unpack(u8 *PackNum, u8 *PackAll, char *Data);

u32 History_Data_Unpack(u8 PackNum, u8 *PackAll, char *Data);
//u8 History_Data_Unpack(u32 Date, u32 Time, u8 PackNum, u8 PackAll, char *Data);
void TransmissionMark(u8 mark);
u8 Historical_Presence(void);
//文件系统状态读取 空闲返回0
u8 Get_FileStatus(void);
void Download_GridProfilesList_Delete(void);
void Download_MIProgram_Delete(void);

u32 MI_Program_Get_Length(void);
u16 MI_Program_Write_Length(char *Buffer, u32 Offset, u16 len);
u16 MI_Program_Read_Length(char *Buffer, u32 Offset, u16 len);
u32 Grid_Profiles_List_Get_Length(void);
u16 Grid_Profiles_List_Write_Length(char *Buffer, u32 Offset, u16 len);
u16 Grid_Profiles_List_Read_Length(char *Buffer, u32 Offset, u16 len);
#endif
void Download_DTUProgram_Delete(void);
u32 DTU_Program_Get_Length(void);
u16 DTU_Program_Write(char *Buffer, u32 Offset, u16 len);
u16 DTU_Program_Write_Length(char *Buffer, u32 Offset, u16 len);
u16 DTU_Program_Read_Length(char *Buffer, u32 Offset, u16 len);

u8 MI_Program_Check(void);
u8 CheckProgramA(void);
u8 DTU_Program_Check(u16 Hardware);
// 临时增加++
u8 Grid_Program_Check(void);
void Download_DirCheck(void);
u32 Download_Get_Dir_Size(void);

void System_DirCheck(void);
u32 System_Get_Dir_Size(void);

u32 Hardware_Get_Dir_Size(void);
void Hardware_DirCheck(void);
void RealTime_DirCheck(u8 Disk);
u16 Local_Historical_Data_Read(u8 Disk, u32 Date, char *Buffer, u16 Offset, u16 len);
u16 Local_RealTime_Data_Write(u8 Disk, char *Buffer, u16 Offset, u16 len);
u16 Local_RealTime_Data_Read(u8 Disk, char *Buffer, u16 Offset, u16 len);

void DTU_Cfg_Data_Delete(void);
u32 DTU_Cfg_Data_Get_Length(void);
u32 DTU_Cfg_Data_Write(char *DTU_Cfg_Data_Buf, u32 DTU_Cfg_Data_Len, u32 DTU_Cfg_Data_Off);
u16 DTU_Cfg_Data_Read(char *DTU_Cfg_Data_Buf, u32 DTU_Cfg_Data_Len, u32 DTU_Cfg_Data_Off);

void MI_Data_Delete(void);
u32 MI_Data_Get_Length(void);
u32 MI_Data_Write(char *MI_Data_Buf, u32 MI_Data_Len, u32 MI_Data_Off);
u16 MI_Data_Read(char *MI_Data_Buf, u32 MI_Data_Len, u32 MI_Data_Off);

void MI_Cfg_Data_Delete(void);
u32 MI_Cfg_Data_Get_Length(void);
u32 MI_Cfg_Data_Write(char *MI_Cfg_Data_Buf, u32 MI_Cfg_Data_Len, u32 MI_Cfg_Data_Off);
u16 MI_Cfg_Data_Read(char *MI_Cfg_Data_Buf, u32 MI_Cfg_Data_Len, u32 MI_Cfg_Data_Off);

void Grid_Profiles_Data_Delete(void);
u32 Grid_Profiles_Data_Get_Length(void);
u32 Grid_Profiles_Data_Write(char *Grid_Profiles_Data_Buf, u32 Grid_Profiles_Data_Len, u32 Grid_Profiles_Data_Off);
u16 Grid_Profiles_Data_Read(char *Grid_Profiles_Data_Buf, u32 Grid_Profiles_Data_Len, u32 Grid_Profiles_Data_Off);

void Grid_Profiles_Cfg_Data_Delete(void);
u32 Grid_Profiles_Cfg_Data_Get_Length(void);
u32 Grid_Profiles_Cfg_Data_Write(char *Grid_Profiles_Cfg_Data_Buf, u32 Grid_Profiles_Cfg_Data_Len, u32 Grid_Profiles_Cfg_Data_Off);
u16 Grid_Profiles_Cfg_Data_Read(char *Grid_Profiles_Cfg_Data_Buf, u32 Grid_Profiles_Cfg_Data_Len, u32 Grid_Profiles_Cfg_Data_Off);

void DeleteOldestData(void);
void test(void);
void DownTest(void);
void Downwrite(void);
void DownRead(void);
void StatusMark(u8 mark);
u8 Fs_Init(u8 format);
void DeleteHistoryData(void);
void Memory_BKP_Save(void);
void Memory_BKP_Read(void);
void Memory_BKP_Check(void);
void AlarmData_NumCheck(void);
void AlarmData_Success(void);
void AlarmData_Empty(void);
u16 AlarmData_Num(void);

//实时数据发送结果标记
void Data_List_RealTime_Mark(u8 mark);
//历史数据发送结果标记
void Data_List_History_Mark(u8 mark);
//总目录写
u32 DIR_Write(u8 Style, u32 StartAddr, u32 EndAddr, u32 DataLen, u8 Mark, u32 AbsoluteTime);
//本地总目录读取
u32 DIR_Local_Read(u8 DIR_Disk, u32 DIR_Date, u32 DIR_Offset, u8 *Style, u32 *StartAddr, u32 *EndAddr, u32 *DataLen, u8 *Mark, u32 *AbsoluteTime);
//总目录读取
u32 DIR_Read(u8 *Style, u32 *StartAddr, u32 *EndAddr, u32 *DataLen, u8 *Mark, u32 *AbsoluteTime);
//总目录重标记
void DIR_Mark_Replace(u8 Disk, u32 ReplaceDate, u32 ReplaceOffset, u8 DIR_Style, u32 DIR_StartAddr, u32 DIR_EndAddr, u32 DIR_DataLen, u8 DIR_Mark, u32 AbsoluteTime);
#if 0
//获取告警目录大小
u32 Alarm_List_Get_size(u8 Disk, u32 Date);
//告警目录写
u32 Alarm_List_Write(u8 Disk, u32 Date, u32 *Offset, u32 StartAddr, u32 EndAddr, u32 Len, u8 Mark, u32 AbsoluteTime);
//告警目录读
u32 Alarm_List_Read(u8 Disk, u32 Alarm_Date, u32 Alarm_Offset, u32 *StartAddr, u32 *EndAddr, u32 *Len, u8 *Mark, u32 *AbsoluteTime);
//报警数据读取
u16 Alarm_Data_Read(u32 *Alarm_ReadDate, u32 *Alarm_ReadOffset, u32 *LastAddr, u32 *LastAddrTimes, AlarmDataType *AlarmDataBuf, u32 *Alarm_time);
/*************告警数据***************/
//告警实时数据标记
void Alarm_List_RealTime_Mark(u8 mark);
//告警历史数据标记
void Alarm_List_History_Mark(u8 mark);
//报警数据更新标记
void Alarm_Real_Data_Update(void);
//报警数据写入
u16 Alarm_Data_Write(AlarmDataType *AlarmDataBuf, u16 Alarm_num);
//实时报警数据读取
u16 Alarm_Real_Data_Read(AlarmDataType *AlarmDataBuf, u32 *Alarm_time);
//历史报警数据读取
u16 Alarm_History_Data_Read(AlarmDataType *AlarmDataBuf, u32 *Alarm_time);
#else
//目录校验,目录不存在则创建
void Alarm_DirCheck(u32 Alarm_Save_Date);
//报警数据写入
vu16 Alarm_Data_Write(u8 PackNum, u8 PackAll, AlarmDataType *AlarmDataBuf, u16 Alarm_num);
//实时报警数据读取
//u8 Alarm_Data_Read(AlarmDataType *AlarmDataBuf);
vu16 Alarm_Data_Read(u8 PackNum, u8 *PackAll, AlarmDataType *AlarmDataBuf, u32 *Alarm_time);
//报警数据成功标记
void Alarm_Data_Success(void);
//报警数据失败偏移
void Alarm_Data_Failure(void);
//读取未发送报警数  0为发送完成，1为有报警未发送
vu16 Alarm_Data_Num(void);
//清空实时报警数据
void Alarm_Data_Delete(void);
//删除过期报警数据
void Alarm_Data_Expired_Delete(void);
//报警索引写入
void Alarm_Index_Write(volatile WarnIndex *Warn_Index);
//报警索引读取
void Alarm_Index_Read(volatile WarnIndex *Warn_Index);
#endif

/*************临时存储***************/
//告警信息临时存储写入
u8 AlarmInfo_Write(AlarmDataType *AlarmDataBuf, u8 Alarm_num);
//告警信息临时存储读取
u8 AlarmInfo_Read(AlarmDataType *AlarmDataBuf);
//读取未发送临时报警包数
u8 AlarmInfo_Num(void);
//删除发送成功临时报警
void AlarmInfo_Success(void);
//清空临时报警
void AlarmInfo_Delete(void);
//告警序号存储
void Alarm_Serial_Num_Write(u16 *AlarmSN, calendar_obj calendar);
//告警序号读取
u8 Alarm_Serial_Num_Read(u16 *AlarmSN, calendar_obj *calendar);
/*************告警历史、历史数据扫描***************/
//历史目录扫描
u8 History_Scanning(void);

u16 System_Dtu_Info_Get_Save_Num(void);
u16 System_Dtu_Info_Read(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf);
u16 System_Dtu_Info_Write(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf);
u16 System_MI_SN_Get_Save_Num(void);
u16 System_MI_SN_Read(InverterMajor *InverterMajorBuf);
u16 System_MI_SN_Write(InverterMajor *InverterMajorBuf);
void Data_Migration(void);
void DTU_Network_Info_Check(void);
u16 System_Info_Read_Uncheck(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf);

/*************自检数据***************/
//自检数据写入
void GPST_Data_Write(GPSTVal *GPSTValBuf, u8 GPST_num);
//自检数据读取
vu16 GPST_Data_Read(GPSTVal *GPSTValBuf, u8 GPST_num);
//自检数据成功标记
void GPST_Data_Success(void);
//自检数据未发送报警数
vu16 GPST_Data_Num(void);
//清空自检数据
void GPST_Data_Delete(void);
//域名校验
u8 Domain_Check(u8 *Pre_Id, char *DomainName, u16 DomainLen, u16 ServerPort);

#ifdef DTU3PRO
void System_Cfg_Read(SystemCfgNew *cfg);
void System_Cfg_Write(SystemCfgNew *cfg);

//设置为默认配置
void Reset_Domain_Config(u8 *Pre_Id);

/*************SunSpec***************/
//获取自检数据大小
u32 SunSpec_Info_Get_size(void);
//数据写入
void SunSpec_Info_Write(SUNSPEC_INFO *SunSpec_Info_Buf, u8 Info_num);
//数据读取
vu16 SunSpec_Info_Read(SUNSPEC_INFO *SunSpec_Info_Buf);
//清空SunSpec数据
void SunSpec_Info_Delete(void);
#endif
#endif
