#include "Memory.h"
#include "rtc.h"
#include "string.h"
#include "stdio.h"
#include "SysTick.h"
#include "spi_flash.h"
#include "malloc.h"
#include "littlefs.h"
#include "Memory_Tools.h"
#include "crc16.h"
#include "ST_Flash.h"
#ifndef Bootloader
#include "HW_Version.h"
#include "NetProtocol.h"
#include "NetProtocolTools.h"
#include "RealData.pb.h"
#include "TimeProcessing.h"
#include "usart_wifi.h"
//#include "ErrorCode.h"
#ifdef DTU3PRO
#include "fatfs.h"
#include "AntiReflux.h"
#include "iwdg.h"
#include "main.h"

#else
#include "stm32_wdg.h"
#endif
#define SCATTER             1
#define WriteLimit          512
#define WarnDataMax         30
#define WarnIndexMax        300
#define Alarm_List_End      255
//存储重读次数
#define STORE_REREAD_TIMES  3
extern vu8 DTUError;
extern vu8 ONEC_SOCKET_SEND_DEVICE;
extern vu32 storge_historical_data_flg;
extern vu32 DownOffset;
extern volatile DtuDetail Dtu3Detail;
//DTU主要信息
extern volatile DtuMajor Dtu3Major;
#endif
////实时数据存储位置
//u8 StorageLocat = DefaultDisk;
#ifdef DTU3PRO
//电表数量
extern vu8 MeterLinkNum;
#endif
extern vu8 NetProVer;
vu8 memory_down_mode = 0;
//文件对象
lfs_file_t lfnew;
static struct lfs_info lfno = { 0 };
/*FatfsDisk[0]:TF卡 FatfsDisk[1]:FLASH0 FatfsDisk[2]:FLASH1*/
vu8 FatfsDisk[3] = { 0 };
vu32 MIProgram_Offset = 0;
vu32 DTUProgram_Offset = 0;
vu32 GridProfilesList_Offset = 0;
vu16 fp_state = 0;
vu8 Write_Luck = 0;

volatile SystemCfgNew system_cfg;

#ifndef Bootloader
vu32 UploadedDate = 0;
vu32 UploadedOffset = 0;
vu32 InsideOffset = 0;
vu32 WriteDate = 0;
vu32 WriteOffset = 0;
vu32 StartAddrLast = 0;
vu32 StartAddrTimes = 0;
vu32 DIR_WriteOffset = 0;
vu32 DIR_ReadDate = 0;
vu32 DIR_ReadOffset = 0;
vu32 Alarm_WriteOffset = 0;
vu32 Alarm_Real_ReadDate = 0;
vu32 Alarm_Real_ReadOffset = 0;
vu32 Alarm_History_ReadDate = 0;
vu32 Alarm_History_ReadOffset = 0;
vu32 alarm_read_date_snap = 0;
vu32 alarm_read_offset_snap = 0;
#ifdef DTU3PRO

//是否存储历史数据标志位
vu32 *storge_historical_data_addr   = (u32 *)(BKPSRAM_BASE + 0 * sizeof(u32));
vu32 *WriteDateSave                 = (u32 *)(BKPSRAM_BASE + 2 * sizeof(u32));
vu32 *WriteOffsetSave               = (u32 *)(BKPSRAM_BASE + 4 * sizeof(u32));
vu32 *DIR_WriteOffsetSave           = (u32 *)(BKPSRAM_BASE + 6 * sizeof(u32));
vu32 *DIR_ReadDateSave              = (u32 *)(BKPSRAM_BASE + 8 * sizeof(u32));
vu32 *DIR_ReadOffsetSave            = (u32 *)(BKPSRAM_BASE + 10 * sizeof(u32));
vu32 *Alarm_WriteOffsetSave         = (u32 *)(BKPSRAM_BASE + 12 * sizeof(u32));
vu32 *Alarm_ReadDateSave            = (u32 *)(BKPSRAM_BASE + 14 * sizeof(u32));
vu32 *Alarm_ReadOffsetSave          = (u32 *)(BKPSRAM_BASE + 16 * sizeof(u32));
//u32 *UploadedDateSave              = (u32 *)BKPSRAM_BASE;
//u32 *UploadedOffsetSave            = (u32 *)(BKPSRAM_BASE + 2 * sizeof(u32));
//u32 *Alarm_Real_ReadDateSave       = (u32 *)(BKPSRAM_BASE + 18 * sizeof(u32));
//u32 *Alarm_Real_ReadOffsetSave     = (u32 *)(BKPSRAM_BASE + 20 * sizeof(u32));
//u32 *Alarm_History_ReadDateSave    = (u32 *)(BKPSRAM_BASE + 22 * sizeof(u32));
//u32 *Alarm_History_ReadOffsetSave  = (u32 *)(BKPSRAM_BASE + 24 * sizeof(u32));

extern MeterInfomation MeterInfo[];
#else
//u32 *UploadedDateSave = (u32 *)(BKP_BASE + BKP_DR2);
//u32 *UploadedOffsetSave = (u32 *)(BKP_BASE + BKP_DR4);
//u32 *WriteDateSave = (u32 *)(BKP_BASE + BKP_DR6);
//u32 *WriteOffsetSave = (u32 *)(BKP_BASE + BKP_DR8);


vu32 *storge_historical_data_addr   = (u32 *)(BKP_BASE + BKP_DR2);
vu32 *WriteDateSave                 = (u32 *)(BKP_BASE + BKP_DR4);
vu32 *WriteOffsetSave               = (u32 *)(BKP_BASE + BKP_DR6);
vu32 *DIR_WriteOffsetSave           = (u32 *)(BKP_BASE + BKP_DR8);
vu32 *DIR_ReadDateSave              = (u32 *)(BKP_BASE + BKP_DR10);
vu32 *DIR_ReadOffsetSave            = (u32 *)(BKP_BASE + BKP_DR12);
vu32 *Alarm_WriteOffsetSave         = (u32 *)(BKP_BASE + BKP_DR14);
vu32 *Alarm_ReadDateSave            = (u32 *)(BKP_BASE + BKP_DR16);
vu32 *Alarm_ReadOffsetSave          = (u32 *)(BKP_BASE + BKP_DR18);
////是否存储历史数据标志位
//u32 *storge_historical_data_addr = (u32 *)(BKP_BASE + BKP_DR10);
#endif
vu32 AlarmReadNum;

vu8 WriteMode = 0;
vu16 File_WriteOffset = 0;
vs16 File_res = 0;
vu32 IdleTimes = 0;
vu32 IdleTime = 0;
vu32 CheckTime = 0;
vu32 module_num = 0;
vu32 pv_length = 0;
vu32 Meter_length = 0;
vu32 Rp_length = 0;
//实时数据最大可存储数量
vu16 AvailableQuan = 0;
//实时数据已存数量
vu16 SavedQuan = 0;
#ifdef DTU3PRO
//实时数据最大可存储数量
vu16 AvailableQuanExp = 0;
//实时数据已存数量
vu16 SavedQuanExp = 0;
#endif
fs_write File_fp_get;
vu8 DIR_Scan_Times = 0;
vu32 DIR_ReadDate_Last = 0;
vu32 DIR_ReadOffset_Last = 0;

vu32 UploadedDate_Last = 0;
vu32 UploadedOffset_Last = 0;

vu8 Uploaded_PackNum_Last = 0;
vu8 Uploaded_PackAll_Last = 0;
vu8 Uploaded_Times = 0;
vu8 DeleteDataState = 0;
vu8 BKP_Check = 0;
vu32 GPST_Write_Offset = 0;
vu32 GPST_Read_Offset = 0;
vu32 GPST_Read_Offset_Temp = 0;
/***********************************************
** Function name:       Memory_BKP_Save
** Descriptions:        掉电保存参数保存
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Memory_BKP_Save(void)
{
    *storge_historical_data_addr        = storge_historical_data_flg;
    WriteDateSave[0]                    = WriteDate       % 0x10000;
    WriteDateSave[1]                    = WriteDate       / 0x10000;
    WriteOffsetSave[0]                  = WriteOffset     % 0x10000;
    WriteOffsetSave[1]                  = WriteOffset     / 0x10000;
    DIR_WriteOffsetSave[0]              = DIR_WriteOffset % 0x10000;
    DIR_WriteOffsetSave[1]              = DIR_WriteOffset / 0x10000;
    DIR_ReadDateSave[0]                 = DIR_ReadDate % 0x10000;
    DIR_ReadDateSave[1]                 = DIR_ReadDate / 0x10000;
    DIR_ReadOffsetSave[0]               = DIR_ReadOffset % 0x10000;
    DIR_ReadOffsetSave[1]               = DIR_ReadOffset / 0x10000;
    Alarm_WriteOffsetSave[0]            = Alarm_WriteOffset % 0x10000;
    Alarm_WriteOffsetSave[1]            = Alarm_WriteOffset / 0x10000;
    Alarm_ReadDateSave[0]               = Alarm_Real_ReadDate % 0x10000;
    Alarm_ReadDateSave[1]               = Alarm_Real_ReadDate / 0x10000;
    Alarm_ReadOffsetSave[0]             = Alarm_Real_ReadOffset % 0x10000;
    Alarm_ReadOffsetSave[1]             = Alarm_Real_ReadOffset / 0x10000;
    //UploadedDateSave[0]             = UploadedDate    % 0x10000;
    //UploadedDateSave[1]             = UploadedDate    / 0x10000;
    //UploadedOffsetSave[0]           = UploadedOffset  % 0x10000;
    //UploadedOffsetSave[1]           = UploadedOffset  / 0x10000;
    //Alarm_History_ReadDateSave[0]   = Alarm_History_ReadDate % 0x10000;
    //Alarm_History_ReadDateSave[1]   = Alarm_History_ReadDate / 0x10000;
    //Alarm_History_ReadOffsetSave[0] = Alarm_History_ReadOffset % 0x10000;
    //Alarm_History_ReadOffsetSave[1] = Alarm_History_ReadOffset / 0x10000;
}
/***********************************************
** Function name:       Memory_BKP_Read
** Descriptions:        读掉电保存区内存
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Memory_BKP_Read(void)
{
    WriteDate                           = WriteDateSave[0]                  + WriteDateSave[1]                  * 0x10000;
    WriteOffset                         = WriteOffsetSave[0]                + WriteOffsetSave[1]                * 0x10000;
    storge_historical_data_flg          = *storge_historical_data_addr;
    DIR_WriteOffset                     = DIR_WriteOffsetSave[0]            + DIR_WriteOffsetSave[1]            * 0x10000;
    DIR_ReadDate                        = DIR_ReadDateSave[0]               + DIR_ReadDateSave[1]               * 0x10000;
    DIR_ReadOffset                      = DIR_ReadOffsetSave[0]             + DIR_ReadOffsetSave[1]             * 0x10000;
    Alarm_WriteOffset                   = Alarm_WriteOffsetSave[0]          + Alarm_WriteOffsetSave[1]          * 0x10000;
    Alarm_Real_ReadDate                 = Alarm_ReadDateSave[0]             + Alarm_ReadDateSave[1]             * 0x10000;
    Alarm_Real_ReadOffset               = Alarm_ReadOffsetSave[0]           + Alarm_ReadOffsetSave[1]           * 0x10000;
    //UploadedDate                = UploadedDateSave[0]               + UploadedDateSave[1]               * 0x10000;
    //UploadedOffset              = UploadedOffsetSave[0]             + UploadedOffsetSave[1]             * 0x10000;
    //Alarm_Real_ReadDate         = Alarm_Real_ReadDateSave[0]        + Alarm_Real_ReadDateSave[1]        * 0x10000;
    //Alarm_Real_ReadOffset       = Alarm_Real_ReadOffsetSave[0]      + Alarm_Real_ReadOffsetSave[1]      * 0x10000;
    //Alarm_History_ReadDate      = Alarm_History_ReadDateSave[0]     + Alarm_History_ReadDateSave[1]     * 0x10000;
    //Alarm_History_ReadOffset    = Alarm_History_ReadOffsetSave[0]   + Alarm_History_ReadOffsetSave[1]   * 0x10000;
}
/***********************************************
** Function name:       Memory_BKP_Check
** Descriptions:        掉电保存区校验
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Memory_BKP_Check(void)
{
    Memory_BKP_Read();

    if(storge_historical_data_flg != 0xABCD)
    {
        WriteDate                   = 0;
        WriteOffset                 = 0;
        storge_historical_data_flg  = 0;
        DIR_WriteOffset             = 0;
        DIR_ReadDate                = 0;
        DIR_ReadOffset              = 0;
        Alarm_WriteOffset           = 0;
        Alarm_Real_ReadDate         = 0;
        Alarm_Real_ReadOffset       = 0;
        Memory_BKP_Save();
        BKP_Check = 0;
    }
    else
    {
        BKP_Check = 1;
    }
}


#endif
/***********************************************
** Function name:       Domain_Check
** Descriptions:        域名校验
** input parameters:    无
** output parameters:   无
** Returned value:      1:错误 0:正常
*************************************************/
u8 Domain_Check(u8 *Pre_Id, char *DomainName, u16 DomainLen, u16 ServerPort)
{
    if(((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xD4)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xD5)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xF9)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xFA)))
    {
        NetProVer = 1;

        /*腾圣*/
        if((strncmp(DomainName, "data.tsun-ess.com", DomainLen) == 0) && (ServerPort == 10002))
        {
            return 0;
        }

        //英臻测试
        if((strncmp(DomainName, "datatest.igen.solarmandata.com", DomainLen) == 0) && (ServerPort == 14000))
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        NetProVer = 0;

        /*中国区域*/
        if(((strncmp(DomainName, "datacn.hoymiles.com", DomainLen) != 0)
                /*欧洲*/
                && (strncmp(DomainName, "dataeu.hoymiles.com", DomainLen) != 0)
                /*亚洲除中国*/
                && (strncmp(DomainName, "dataas.hoymiles.com", DomainLen) != 0)
                /*非洲*/
                && (strncmp(DomainName, "dataaf.hoymiles.com", DomainLen) != 0)
                /*大洋洲*/
                && (strncmp(DomainName, "dataoa.hoymiles.com", DomainLen) != 0)
                /*北美*/
                && (strncmp(DomainName, "datana.hoymiles.com", DomainLen) != 0)
                /*南美*/
                && (strncmp(DomainName, "datasa.hoymiles.com", DomainLen) != 0)
                /*南极*/
                && (strncmp(DomainName, "dataan.hoymiles.com", DomainLen) != 0)
                /*测试服务器*/
                && (strncmp(DomainName, "data3.cktd.net", DomainLen) != 0)
                /*测试服务器*/
                && (strncmp(DomainName, "dev.redtide.vip", DomainLen) != 0)
                /*测试服务器*/
                && (strncmp(DomainName, "data2.cktd.net", DomainLen) != 0)
           ) || ((ServerPort != 10081)
                ))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    return 1;
}
/***********************************************
** Function name:       Reset_Domain_Config
** Descriptions:        设置为默认配置
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
void Reset_Domain_Config(u8 *Pre_Id)
{
    memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
    memset((u8 *)Dtu3Major.Property.APN, 0, sizeof(Dtu3Major.Property.APN));
    memcpy((u8 *)Dtu3Major.Property.APN, "CMNET", sizeof("CMNET"));

    if(((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xD4)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xD5)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xF9)) ||
            ((Pre_Id[0] == 0x10) && (Pre_Id[1] == 0xFA)))
    {
        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, "data.tsun-ess.com", sizeof("data.tsun-ess.com"));
        Dtu3Major.Property.ServerPort = 10002;
        //        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, "data2.cktd.net", sizeof("data2.cktd.net"));
        //        //        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, "data.tsun-ess.com", sizeof("data.tsun-ess.com"));
        //        Dtu3Major.Property.ServerPort = 10017;
    }
    else
    {
        memcpy((u8 *)Dtu3Major.Property.ServerDomainName, "datacn.hoymiles.com", sizeof("datacn.hoymiles.com"));
        Dtu3Major.Property.ServerPort = 10081;
    }
}
/***********************************************
** Function name:       Date_Add
** Descriptions:        日期增加
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
void Date_Add(u32 *date)
{
    volatile calendar_obj calendar;
    calendar.w_year = (u16)(*date / 10000);
    calendar.w_month = (u8)((*date / 100) % 100);
    calendar.w_date = (u8)(*date % 100);
    calendar.w_date = calendar.w_date + 1;
    RTC_Correction((calendar_obj *)&calendar);
    *date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
}
/***********************************************
** Function name:       Date_Minus
** Descriptions:        日期增加
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
void Date_Minus(u32 *date)
{
    volatile calendar_obj calendar;
    calendar.w_year = (u16)(*date / 10000);
    calendar.w_month = (u8)((*date / 100) % 100);
    calendar.w_date = (u8)(*date % 100);
    calendar.w_date = calendar.w_date - 1;
    RTC_Correction((calendar_obj *)&calendar);
    *date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
}
/***********************************************
** Function name:       Fs_Init
** Descriptions:        Littlefs文件系统初始化
** input parameters:    无
** output parameters:   无
** Returned value:      1:成功 0:失败
*************************************************/
u8 Fs_Init(u8 format)
{
    vs16 err;
    lfs_dir_t g_dir;
    char *path = NULL;
    volatile calendar_obj calendar;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
#ifndef Bootloader
    Queue_init();
#endif
    //spi_flash初始化
    SPI_FLASH_Init();
    //    if(format == 1)
    //    {
    //        WriteDateSave                 = 0;
    //        WriteOffsetSave               = 0;
    //        DIR_WriteOffsetSave           = 0;
    //        DIR_ReadDateSave              = 0;
    //        DIR_ReadOffsetSave            = 0;
    //        Alarm_WriteOffsetSave         = 0;
    //        Memory_BKP_Save();
    //#ifdef DEBUG
    //        printf("\r\nlf_format---1\r\n");
    //#endif
    //        lf_format(0);
    //#ifdef DTU3PRO
    //        lf_format(1);
    //#endif
    //        //        SPI_FLASH_BulkErase(0);
    //        //        SPI_FLASH_BulkErase(1);
    //    }
    //
    //挂载文件系统
    err = lf_mount(DefaultDisk);
    sprintf(path, "%d:", DefaultDisk);

    //littlefs初始化失败
    if(err != LFS_ERR_OK)
    {
#ifdef DEBUG
        printf("\r\nlf_format---2\r\n");
#endif
        //格式化
        lf_format(DefaultDisk);
        //重新挂载文件系统
        lf_mount(DefaultDisk);
        err = lf_opendir(DefaultDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            lf_mkdir(DefaultDisk, path);
        }

        lf_closedir(DefaultDisk, &g_dir);
    }
    else
    {
        err = lf_opendir(DefaultDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
#ifdef DEBUG
            printf("\r\nlf_format---3\r\n");
#endif
            //格式化
            lf_format(DefaultDisk);
            //重新挂载文件系统
            lf_mount(DefaultDisk);
            lf_mkdir(DefaultDisk, path);
            //printf("lfs makedir\n");
        }
        else
        {
            lf_closedir(DefaultDisk, &g_dir);
        }
    }

#ifndef Bootloader
#ifdef DTU3PRO
    //挂载文件系统
    err = lf_mount(ExpansionDisk);
    sprintf(path, "%d:", ExpansionDisk);

    //littlefs初始化失败
    if(err != LFS_ERR_OK)
    {
        //格式化
        lf_format(ExpansionDisk);
        //重新挂载文件系统
        lf_mount(ExpansionDisk);
        err = lf_opendir(ExpansionDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            lf_mkdir(ExpansionDisk, path);
        }

        lf_closedir(ExpansionDisk, &g_dir);
    }
    else
    {
        err = lf_opendir(ExpansionDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            //格式化
            lf_format(ExpansionDisk);
            //重新挂载文件系统
            lf_mount(ExpansionDisk);
            lf_mkdir(ExpansionDisk, path);
            //printf("lfs makedir\n");
        }
        else
        {
            lf_closedir(ExpansionDisk, &g_dir);
        }

        RealTime_DirCheck(ExpansionDisk);
    }

#endif
#endif
#ifndef Bootloader
    Hardware_DirCheck();
    RealTime_DirCheck(DefaultDisk);
    Download_DirCheck();
    System_DirCheck();
#endif
    myfree(path);
    return 1;
}

/***********************************************
** Function name:       File_Write
** Descriptions:        文件系统实时写
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 File_Write(u8 Disk, char *FilePath, char *Buffer, u32 Offset, u32 len)
{
    vu32 fnum = 0;

    if(len > 4096)
    {
        return (u16)fnum;
    }

#if SCATTER
    lfs_file_t file_new;
    lf_open(Disk, &file_new, FilePath, LFS_O_RDWR | LFS_O_CREAT);
    lf_seek(Disk, &file_new, Offset);
    lf_write(Disk, &file_new, Buffer, len, (u32 *)&fnum);
    lf_close(Disk, &file_new);
#else
    lf_open(Disk, &lfnew, FilePath, LFS_O_RDWR | LFS_O_CREAT);
    lf_seek(Disk, &lfnew, Offset);
    lf_write(Disk, &lfnew, Buffer, len, &fnum);
    lf_close(Disk, &lfnew);
#endif
    return (u16)fnum;
}
#ifndef Bootloader
/***********************************************
** Function name:       File_Write_Que
** Descriptions:        文件系统队列写
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      无效
*************************************************/
u16 File_Write_Que(u8 Disk, char *FilePath, char *Buffer, u32 Offset, u32 len)
{
    fs_write write_data;
    vu32 fnum = 0;

    if(len > 4096)
    {
        return (u16)fnum;
    }

    write_data.path = FilePath;
    write_data.buffer = Buffer;
    write_data.off = Offset;
    write_data.size = len;
    write_data.disk = Disk;
    Queue_enter(write_data);
    //printf("Disk %d\n",Disk);
    //printf("FilePath %s\n",FilePath);
    //printf("Buffer %s\n",Buffer);
    //printf("Offset %d\n",Offset);
    //printf("len %d\n",len);
    return (u16)fnum;
}
#endif

/***********************************************
** Function name:       File_Read
** Descriptions:        文件系统实时读
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 File_Read(u8 Disk, char *FilePath, char *Buffer, u32 Offset, u32 len)
{
    vs16 res;
    vu32 fnum = 0;
#if SCATTER
    lfs_file_t file_new;

    if(Write_Luck == 0)
    {
        res = lf_open(Disk, &file_new, FilePath, LFS_O_RDWR);

        if(res == LFS_ERR_OK)
        {
            res = lf_seek(Disk, &file_new, Offset);
            res = lf_read(Disk, &file_new, Buffer, len, (u32 *)&fnum);
            lf_close(Disk, &file_new);
        }
    }
    else
    {
        fnum = 0;
    }

#else

    if(Write_Luck == 0)
    {
        res = lf_open(Disk, &lfnew, FilePath, LFS_O_RDWR);

        if(res == LFS_ERR_OK)
        {
            res = lf_seek(Disk, &lfnew, Offset);
            res = lf_read(Disk, &lfnew, Buffer, len, &fnum);
            lf_close(Disk, &lfnew);
        }
    }
    else
    {
        fnum = 0;
    }

#endif
    return (u16)fnum;
}

/***********************************************
** Function name:       Dir_Check
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Dir_Check(u8 Disk, char *FilePath)
{
    vs16 res;
    char *path = NULL;
    lfs_dir_t dir;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    memcpy(path, FilePath, strlen(FilePath));
    res = lf_opendir(Disk, &dir, path);

    /* 获取文件信息 */
    if(res == LFS_ERR_NOENT)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(Disk, path);
    }
    else
    {
        lf_closedir(Disk, &dir);
    }

    myfree(path);
}
/***********************************************
** Function name:       Read_File_Length
** Descriptions:        读取文件大小
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      文件大小
*************************************************/
u32 Read_File_Length(u8 Disk, char *FilePath)
{
    vs16 res;
    char *path = NULL;
    vu32 size = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    memcpy(path, FilePath, strlen(FilePath));
    /* 获取文件信息 */
    res = lf_stat(Disk, FilePath, &lfno);

    if(res == LFS_ERR_OK)
    {
        size = lfno.size;
    }
    else
    {
        size = 0;
    }

    myfree(path);
    return size;
}
/***********************************************
** Function name:       Download_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_DirCheck(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
#ifndef Bootloader
/***********************************************
** Function name:       ClearDownloadFile
** Descriptions:        清空下载目录中的文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void ClearDownloadFile(void)
{
    lfs_dir_t dir;
    vs16 res;
    char *path = NULL;
    MIProgram_Offset = 0;
    DTUProgram_Offset = 0;
    GridProfilesList_Offset = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        lf_DeleteAllFiles(DefaultDisk, path);
    }

    myfree(path);
}

/***********************************************
** Function name:       Download_GridProfilesList_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_GridProfilesList_Delete(void)
{
    //System/SystemDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/GridProfilesList.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       Download_MIProgram_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_MIProgram_Delete(void)
{
    //System/SystemDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/MIProgram.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
#endif
/***********************************************
** Function name:       Download_DTUProgram_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_DTUProgram_Delete(void)
{
    //System/SystemDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
#ifndef Bootloader
    DownOffset = 0;
#endif

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
#ifndef Bootloader
/***********************************************
** Function name:       Download_Write_Length
** Descriptions:        在Download目录下指定位置写指定长度
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 Download_Write_Length(char *FilePath, char *Buffer, u32 Offset, u16 len)
{
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    Download_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return (u16)fnum;
}
#endif
/***********************************************
** Function name:       Download_Read_Length
** Descriptions:        在Download目录下指定位置读指定长度
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 Download_Read_Length(char *FilePath, char *Buffer, u32 Offset, u16 len)
{
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    fnum = File_Read(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return (u16)fnum;
}
/***********************************************
** Function name:       Download_Get_Length
** Descriptions:        获取Download目录下指定文件的大小
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      文件大小
*************************************************/
u32 Download_Get_Length(char *FilePath)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
#ifndef Bootloader
/***********************************************
** Function name:       Download_Get_Dir_Size
** Descriptions:        获取Download目录的大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 Download_Get_Dir_Size(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/GridProfilesList.txt", DefaultDisk);
    length += Download_Get_Length(path);
    sprintf(path, "%d:/Download/MIProgram.hex", DefaultDisk);
    length += Download_Get_Length(path);
    sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
    length += Download_Get_Length(path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       Grid_Profiles_List_Get_Length
** Descriptions:        GridProfilesList.txt文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
u32 Grid_Profiles_List_Get_Length(void)
{
    vu32 ReturnNum = 0;
    GridProfilesList_Offset = 0;
    ReturnNum = Download_Get_Length("GridProfilesList.txt");
    return ReturnNum;
}
/***********************************************
** Function name:       Grid_Profiles_List_Write_Length
** Descriptions:        在GridProfilesList.txt文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 Grid_Profiles_List_Write_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/GridProfilesList.txt
    return Download_Write_Length("GridProfilesList.txt", Buffer, Offset, len);
}
/***********************************************
** Function name:       Grid_Profiles_List_Read_Length
** Descriptions:        在GridProfilesList.txt文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 Grid_Profiles_List_Read_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/GridProfilesList.txt
    return Download_Read_Length("GridProfilesList.txt", Buffer, Offset, len);
}
/***********************************************
** Function name:       MI_Program_Get_Length
** Descriptions:        MIProgram.hex文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
u32 MI_Program_Get_Length(void)
{
    vu32 ReturnNum = 0;
    MIProgram_Offset = 0;
    ReturnNum = Download_Get_Length("MIProgram.hex");
    return ReturnNum;
}
/***********************************************
** Function name:       MI_Program_Write_Length
** Descriptions:        在MIProgram.hex文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 MI_Program_Write_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/MIProgram.hex
    return Download_Write_Length("MIProgram.hex", Buffer, Offset, len);
}
/***********************************************
** Function name:       MI_Program_Read_Length
** Descriptions:        在MIProgram.hex文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 MI_Program_Read_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/MIProgram.hex
    return Download_Read_Length("MIProgram.hex", Buffer, Offset, len);
}
#endif
/***********************************************
** Function name:       DTU_Program_Get_Length
** Descriptions:        DTUProgram.hex文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
u32 DTU_Program_Get_Length(void)
{
    vu32 ReturnNum = 0;
    DTUProgram_Offset = 0;
    ReturnNum = Download_Get_Length("DTUProgram.hex");
    return ReturnNum;
}

u16 DTU_Program_Write(char *Buffer, u32 Offset, u16 len)
{
    //Download/DTUProgram.hex
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return (u16)fnum;
}
#ifndef Bootloader
/***********************************************
** Function name:       DTU_Program_Write_Length
** Descriptions:        在DTUProgram.hex文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 DTU_Program_Write_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/DTUProgram.hex
    return Download_Write_Length("DTUProgram.hex", Buffer, Offset, len);
}
#endif
/***********************************************
** Function name:       DTU_Program_Read_Length
** Descriptions:        在DTUProgram.hex文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 DTU_Program_Read_Length(char *Buffer, u32 Offset, u16 len)
{
    //Download/MIProgram.hex
    return Download_Read_Length("DTUProgram.hex", Buffer, Offset, len);
}
#ifndef Bootloader
/***********************************************
** Function name:       System_Get_Dir_Size
** Descriptions:        System目录大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 System_Get_Dir_Size(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       System_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void System_DirCheck(void)
{
    //Hardware/HardwareDate.dat
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
///***********************************************
//** Function name:       System_DtuDetail_Delete
//** Descriptions:        删除文件
//** input parameters:    无
//** output parameters:   无
//** Returned value:      无
//*************************************************/
//void System_DtuDetail_Delete(void)
//{
//    //System/SystemDate.dat
//    vs16 res;
//    lfs_dir_t dir;
//    char *path= NULL;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System", DefaultDisk);
//    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

//    if(res != LFS_ERR_OK)
//    {
//        /* 打开目录失败，就创建目录 */
//        res = lf_mkdir(DefaultDisk, (const char *)path);
//    }
//    else
//    {
//        /* 如果目录已经存在，关闭它 */
//        res = lf_closedir(DefaultDisk, &dir);
//        sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
//        res = lf_stat(DefaultDisk, path, &lfno);

//        if(res == LFS_ERR_OK)
//        {
//            /* 删除文件 */
//            lf_unlink(DefaultDisk, path);
//        }
//    }

//    myfree(path);
//}

///***********************************************
//** Function name:       System_DtuDetail_Write
//** Descriptions:        将结构体写入flash
//** input parameters:    *DtuDetailbuf 结构体指针
//** output parameters:   无
//** Returned value:      成功个数
//*************************************************/
//u16 System_DtuDetail_Write(DtuDetail *DtuDetailbuf)
//{
//    //System/SystemDate.dat
//    vs16 res;
//    char *path= NULL;
//    char *Buffer= NULL;
//    u32 fnum = 0;
//    u16 crc = 0;
//    Buffer = mymalloc(DtuDetailSize + 2);
//    memset(Buffer, 0, DtuDetailSize + 2);
//    memcpy(Buffer, DtuDetailbuf->PropertyMsg, sizeof(DtuDetail));
//    crc = MyCrc16((unsigned char *)Buffer, DtuDetailSize);
//    Buffer[(DtuDetailSize)] = (u8)crc;
//    Buffer[(DtuDetailSize) + 1] = (u8)(crc >> 8);
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
//    System_DtuDetail_Delete();
//    fnum = File_Write(DefaultDisk, path, Buffer, 0, DtuDetailSize + 2);
//    myfree(path);
//    myfree(Buffer);
//    return fnum;
//}
///***********************************************
//** Function name:       System_DtuDetail_Write
//** Descriptions:        读出结构体
//** input parameters:    无
//** output parameters:   *DtuDetailbuf 结构体指针
//** Returned value:      成功个数
//*************************************************/
//u16 System_DtuDetail_Read(DtuDetail *DtuDetailbuf)
//{
//    //System/SystemDate.dat
//    char *path= NULL;
//    char *Buffer= NULL;
//    u16 crc = 0;
//    u16 crc_read = 0;
//    u32 fnum = 0;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
//    Buffer = mymalloc(DtuDetailSize + 2);
//    memset(Buffer, 0, DtuDetailSize + 2);
//    fnum = File_Read(DefaultDisk, path, Buffer, 0, DtuDetailSize + 2);
//    crc_read = (u16)Buffer[DtuDetailSize] + (u16)(Buffer[DtuDetailSize + 1] << 8);
//    crc = MyCrc16((unsigned char *)Buffer, DtuDetailSize);

//    if(crc == crc_read)
//    {
//        memcpy(DtuDetailbuf->PropertyMsg, Buffer, sizeof(DtuDetail));
//    }

//    myfree(path);
//    myfree(Buffer);
//    return fnum;
//}
///***********************************************
//** Function name:       System_DtuMajor_Delete
//** Descriptions:        删除文件
//** input parameters:    无
//** output parameters:   无
//** Returned value:      无
//*************************************************/
//void System_DtuMajor_Delete(void)
//{
//    //System/SystemDate.dat
//    vs16 res;
//    lfs_dir_t dir;
//    char *path= NULL;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System", DefaultDisk);
//    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

//    if(res != LFS_ERR_OK)
//    {
//        /* 打开目录失败，就创建目录 */
//        res = lf_mkdir(DefaultDisk, (const char *)path);
//    }
//    else
//    {
//        /* 如果目录已经存在，关闭它 */
//        res = lf_closedir(DefaultDisk, &dir);
//        sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
//        res = lf_stat(DefaultDisk, path, &lfno);

//        if(res == LFS_ERR_OK)
//        {
//            /* 删除文件 */
//            lf_unlink(DefaultDisk, path);
//        }
//    }

//    myfree(path);
//}

///***********************************************
//** Function name:       System_DtuMajor_Write
//** Descriptions:        将结构体写入flash
//** input parameters:    *DtuMajorbuf 结构体指针
//** output parameters:   无
//** Returned value:      成功个数
//*************************************************/
//u16 System_DtuMajor_Write(DtuMajor *DtuMajorbuf)
//{
//    //System/SystemDate.dat
//    vs16 res;
//    char *path = NULL;
//    char *buffer = NULL;
//    u32 fnum = 0;
//    u16 crc = 0;
//    Buffer = mymalloc(DtuMajorSize + 2);
//    memset(Buffer, 0, DtuMajorSize + 2);
//    memcpy(Buffer, DtuMajorbuf->PropertyMsg, sizeof(DtuMajor));
//    crc = MyCrc16((unsigned char *)Buffer, DtuMajorSize);
//    Buffer[(DtuMajorSize)] = (u8)crc;
//    Buffer[(DtuMajorSize) + 1] = (u8)(crc >> 8);
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
//    System_DtuMajor_Delete();
//    fnum = File_Write(DefaultDisk, path, Buffer, 0, DtuMajorSize + 2);
//    myfree(path);
//    myfree(Buffer);
//    return fnum;
//}
///***********************************************
//** Function name:       System_DtuMajor_Read
//** Descriptions:        读出结构体
//** input parameters:    无
//** output parameters:   *DtuMajorbuf 结构体指针
//** Returned value:      成功个数
//*************************************************/
//u16 System_DtuMajor_Read(DtuMajor *DtuMajorbuf)
//{
//    //System/SystemDate.dat
//    char *path = NULL;
//    char *buffer = NULL;
//    u16 crc = 0;
//    u16 crc_read = 0;
//    u32 fnum = 0;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
//    Buffer = mymalloc(DtuMajorSize + 2);
//    memset(Buffer, 0, DtuMajorSize + 2);
//    fnum = File_Read(DefaultDisk, path, Buffer, 0, DtuMajorSize + 2);
//    crc_read = (u16)Buffer[DtuMajorSize] + (u16)(Buffer[DtuMajorSize + 1] << 8);
//    crc = MyCrc16((unsigned char *)Buffer, DtuMajorSize);

//    if(crc == crc_read)
//    {
//        memcpy(DtuMajorbuf->PropertyMsg, Buffer, sizeof(DtuMajor));
//    }

//    if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id,(char *)Dtu3Major.Property.ServerDomainName, strlen((char *)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
//    {
//#ifdef DTU3PRO
//        System_Cfg_Read((SystemCfgNew *)&system_cfg);
//#else
//        ReadSystemConfig(&system_cfg);
//#endif

//        if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id,(char *)system_cfg.Property.ServerDomainName, strlen((char *)system_cfg.Property.ServerDomainName), system_cfg.Property.ServerPort) == 0)
//        {
//            memset(Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
//            memcpy(Dtu3Major.Property.ServerDomainName, system_cfg.Property.ServerDomainName, sizeof(system_cfg.Property.ServerDomainName));
//            Dtu3Major.Property.ServerPort = system_cfg.Property.ServerPort;
//            memcpy(Dtu3Major.Property.APN, system_cfg.Property.APN, sizeof(system_cfg.Property.APN));
//        }

//        if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id,(char *)Dtu3Major.Property.ServerDomainName, strlen((char *)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
//        {
//            Reset_Domain_Config((u8 *)Dtu3Major.Property.Pre_Id);
//        }

//        //写入参数
//        //System_DtuMajor_Write(&Dtu3Major);
//        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
//    }

//    myfree(path);
//    myfree(Buffer);
//    return fnum;
//}
/***********************************************
** Function name:       Hardware_Get_Dir_Size
** Descriptions:        Hardware目录大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 Hardware_Get_Dir_Size(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       Hardware_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Hardware_DirCheck(void)
{
    //Hardware/HardwareDate.dat
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
///***********************************************
//** Function name:       InverterMajor_Delete
//** Descriptions:        删除文件
//** input parameters:    无
//** output parameters:   无
//** Returned value:      无
//*************************************************/
//void InverterMajor_Delete(void)
//{
//    //Hardware/HardwareDate.dat
//    vs16 res;
//    lfs_dir_t dir;
//    char *path = NULL;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/Hardware", DefaultDisk);
//    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

//    if(res != LFS_ERR_OK)
//    {
//        /* 打开目录失败，就创建目录 */
//        res = lf_mkdir(DefaultDisk, (const char *)path);
//    }
//    else
//    {
//        /* 如果目录已经存在，关闭它 */
//        res = lf_closedir(DefaultDisk, &dir);
//        sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
//        res = lf_stat(DefaultDisk, path, &lfno);

//        if(res == LFS_ERR_OK)
//        {
//            /* 删除文件 */
//            lf_unlink(DefaultDisk, path);
//        }
//    }

//    myfree(path);
//}

///***********************************************
//** Function name:       InverterMajor_Write
//** Descriptions:        将结构体写入flash
//** input parameters:    *InverterMajorBuf 结构体指针  Pv_num个数
//** output parameters:   无
//** Returned value:      成功个数
//*************************************************/
//u16 InverterMajor_Write(InverterMajor *InverterMajorBuf, u16 Pv_num)
//{
//    //Hardware/InverterMajor.dat
//    char *path = NULL;
//    char *buffer = NULL;
//    u16 i, j;
//    u32 fnum = 0;
//    u16 crc = 0;
//    Buffer = mymalloc((Pv_num * InverterMajorSize) + 4);
//    memset(Buffer, 0, (Pv_num * InverterMajorSize) + 4);
//    Buffer[0] = (u8)Pv_num;
//    Buffer[1] = (u8)(Pv_num >> 8);

//    for(i = 0; i < Pv_num; i++)
//    {
//        for(j = 0; j < sizeof(InverterMajor); j++)
//        {
//            Buffer[(i * InverterMajorSize) + 2 + j] = InverterMajorBuf[i].PropertyMsg[j];
//        }
//    }

//    crc = MyCrc16((unsigned char *)&Buffer[2], i * InverterMajorSize);
//    Buffer[(i * InverterMajorSize) + 2] = (u8)crc;
//    Buffer[(i * InverterMajorSize) + 3] = (u8)(crc >> 8);
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
//    Hardware_DirCheck();
//    //fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, (Pv_num * InverterMajorSize) + 4);
//    fnum = File_Write(DefaultDisk, path, Buffer, 0, (Pv_num * InverterMajorSize) + 4);
//    myfree(path);
//    myfree(Buffer);
//    return fnum;
//}
///***********************************************
//** Function name:       InverterMajor_Read
//** Descriptions:        读出结构体
//** input parameters:    无
//** output parameters:   *InverterMajorBuf 结构体指针
//** Returned value:      成功个数
//*************************************************/
//u16 InverterMajor_Read(InverterMajor *InverterMajorBuf)
//{
//    //Hardware/InverterMajor.dat
//    char *path = NULL;
//    char *buffer = NULL;
//    u16 i, j;
//    u16 Pv_num = 0;
//    u16 crc = 0;
//    u16 crc_read = 0;
//    u32 fnum = 0;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
//    Buffer = mymalloc((PORT_LEN * InverterMajorSize) + 4);
//    memset(Buffer, 0, ((PORT_LEN * InverterMajorSize) + 4));
//    fnum = File_Read(DefaultDisk, path, Buffer, 0, ((PORT_LEN * InverterMajorSize) + 4));
//    Pv_num = (u16)Buffer[0] + (u16)(Buffer[1] << 8);

//    if(Pv_num <= PORT_LEN)
//    {
//        crc_read = (u16)Buffer[Pv_num * InverterMajorSize + 2] + (u16)(Buffer[Pv_num * InverterMajorSize + 3] << 8);
//        crc = MyCrc16((unsigned char *)&Buffer[2], Pv_num * InverterMajorSize);

//        if(crc == crc_read)
//        {
//            for(i = 0; i < Pv_num; i++)
//            {
//                for(j = 0; j < sizeof(InverterMajor); j++)
//                {
//                    InverterMajorBuf[i].PropertyMsg[j] = Buffer[(i * InverterMajorSize) + 2 + j];
//                }
//            }
//        }
//    }
//    else
//    {
//        Pv_num = 0;
//    }

//    myfree(path);
//    myfree(Buffer);
//    return Pv_num;
//}
/***********************************************
** Function name:       InverterDetail_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void InverterDetail_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       InverterDetail_Write
** Descriptions:        将结构体写入flash
** input parameters:    *InverterDetailBuf 结构体指针 InverterDetailOff偏移 InverterNum读的个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u16 InverterDetail_Write(InverterDetail *InverterDetailBuf, u16 InverterDetailOff, u16 InverterNum)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    vu32 off;
    vu16 i, j = 0;
    vu32 fnum = 0;
    vu32 crc = 0;
    char *path = NULL;
    char *buffer = NULL;
    buffer = mymalloc(InverterNum * (InverterDetailSize + 2));
    memset(buffer, 0, InverterNum * (InverterDetailSize + 2));

    for(i = 0; i < InverterNum; i++)
    {
        for(j = 0; j < sizeof(InverterDetail); j++)
        {
            buffer[i * (InverterDetailSize + 2) + j] = InverterDetailBuf[i].PropertyMsg[j];
        }

        crc = MyCrc16((unsigned char *)&buffer[i * (InverterDetailSize + 2)], InverterDetailSize);
        buffer[i * (InverterDetailSize + 2) + InverterDetailSize] = (u8)crc;
        buffer[i * (InverterDetailSize + 2) + InverterDetailSize + 1] = (u8)(crc >> 8);
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    off = InverterDetailOff * (InverterDetailSize + 2);
    Hardware_DirCheck();
    fnum = File_Write(DefaultDisk, path, buffer, off, InverterNum * (InverterDetailSize + 2));
    myfree(path);
    myfree(buffer);
    return (u16)fnum;
}
/***********************************************
** Function name:       InverterDetail_Read
** Descriptions:        读取结构体
** input parameters:    无
** output parameters:   *InverterDetailBuf 结构体指针 InverterDetailOff偏移 InverterNum读的个数
** Returned value:      成功个数
*************************************************/
u16 InverterDetail_Read(InverterDetail *InverterDetailBuf, u16 InverterDetailOff, u16 InverterNum)
{
    //Hardware/HardwareDate.dat
    char *path = NULL;
    char *buffer = NULL;
    vu32 off;
    vu16 i, j;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    buffer = mymalloc(InverterDetailSize + 2);
    memset(buffer, 0, InverterDetailSize + 2);
    off = InverterDetailOff * (InverterDetailSize + 2);

    for(i = 0; i < InverterNum; i++)
    {
        fnum = File_Read(DefaultDisk, path, buffer, off + i * (InverterDetailSize + 2), InverterDetailSize + 2);
        crc_read = (u16)buffer[InverterDetailSize] + (u16)(buffer[InverterDetailSize + 1] << 8);
        crc = (u16)MyCrc16((unsigned char *)buffer, InverterDetailSize);

        if(crc == crc_read)
        {
            for(j = 0; j < sizeof(InverterDetail); j++)
            {
                InverterDetailBuf[i].PropertyMsg[j] = buffer[j];
            }
        }

        memset(buffer, 0, InverterDetailSize + 2);
    }

    myfree(path);
    myfree(buffer);
    return (u16)fnum;
}

/***********************************************
** Function name:       InverterReal_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void InverterReal_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       InverterReal_Write
** Descriptions:        将结构体写入flash
** input parameters:    *InverterRealBuf 结构体指针  Pv_num个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u16 InverterReal_Write(InverterReal *InverterRealBuf, u16 Pv_num)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu32 fnum = 0;
    vu16 crc = 0;
    buffer = mymalloc(Pv_num * sizeof(u32) + 4);
    memset(buffer, 0, Pv_num * sizeof(u32) + 4);
    buffer[0] = (u8)Pv_num;
    buffer[1] = (u8)(Pv_num >> 8);

    for(i = 0; i < Pv_num; i++)
    {
        if(((InverterRealBuf[i].Data.LinkState) & MI_TOTAL_ENERGY_FAULT) == MI_TOTAL_ENERGY_FAULT)
        {
            buffer[(i * sizeof(u32)) + 2 + 0] = 0;
            buffer[(i * sizeof(u32)) + 2 + 1] = 0;
            buffer[(i * sizeof(u32)) + 2 + 2] = 0;
            buffer[(i * sizeof(u32)) + 2 + 3] = 0;
        }
        else
        {
            buffer[(i * sizeof(u32)) + 2 + 0] = (u8)InverterRealBuf[i].Data.HistoryEnergyL;
            buffer[(i * sizeof(u32)) + 2 + 1] = (u8)(InverterRealBuf[i].Data.HistoryEnergyL >> 8);
            buffer[(i * sizeof(u32)) + 2 + 2] = (u8)InverterRealBuf[i].Data.HistoryEnergyH;
            buffer[(i * sizeof(u32)) + 2 + 3] = (u8)(InverterRealBuf[i].Data.HistoryEnergyH >> 8);
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], i * sizeof(u32));
    buffer[(i * sizeof(u32)) + 2] = (u8)crc;
    buffer[(i * sizeof(u32)) + 3] = (u8)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
    Hardware_DirCheck();
    fnum = File_Write(DefaultDisk, path, buffer, 0, (Pv_num * sizeof(u32)) + 4);
    myfree(path);
    myfree(buffer);
    return (u16)fnum;
}
/***********************************************
** Function name:       InverterReal_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *InverterRealBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 InverterReal_Read(InverterReal *InverterRealBuf)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu16 Pv_num = 0;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
    buffer = mymalloc((PORT_LEN * sizeof(u32)) + 4);
    memset(buffer, 0, ((PORT_LEN * sizeof(u32)) + 4));
    fnum = File_Read(DefaultDisk, path, buffer, 0, ((PORT_LEN * sizeof(u32)) + 4));
    Pv_num = (u16)buffer[0] + (u16)(buffer[1] << 8);

    if((Pv_num > 0) && (Pv_num <= PORT_LEN))
    {
        crc_read = (u16)buffer[Pv_num * sizeof(u32) + 2] + (u16)(buffer[Pv_num * sizeof(u32) + 3] << 8);
        crc = (u16)MyCrc16((unsigned char *)&buffer[2], Pv_num * sizeof(u32));

        if(crc == crc_read)
        {
            for(i = 0; i < Pv_num; i++)
            {
                InverterRealBuf[i].Data.HistoryEnergyL = (u16)(buffer[(i * sizeof(u32)) + 2 + 0] + (u16)((u16)buffer[(i * sizeof(u32)) + 2 + 1] << 8));
                InverterRealBuf[i].Data.HistoryEnergyH = (u16)(buffer[(i * sizeof(u32)) + 2 + 2] + (u16)((u16)buffer[(i * sizeof(u32)) + 2 + 3] << 8));
            }
        }
    }
    else
    {
        Pv_num = 0;
    }

    myfree(path);
    myfree(buffer);
    return Pv_num;
}

#ifdef DTU3PRO
/***********************************************
** Function name:       MeterMajor_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MeterMajor_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       MeterMajor_Write
** Descriptions:        将结构体写入flash
** input parameters:    *MeterMajorBuf 结构体指针  Pv_num个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u16 MeterInfor_Write(MeterMajor *MeterMajorBuf, u16 Meter_num)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu32 fnum = 0;
    vu16 crc = 0;
    buffer = mymalloc(Meter_num * sizeof(u32) + 4);
    memset(buffer, 0, Meter_num * sizeof(u32) + 4);
    buffer[0] = (u8)Meter_num;
    buffer[1] = (u8)(Meter_num >> 8);

    for(i = 0; i < Meter_num; i++)
    {
        for(j = 0; j < sizeof(MeterMajor); j++)
        {
            buffer[(i * sizeof(MeterMajor)) + 2 + j] = (u8)MeterMajorBuf[i].PropertyMsg[j];
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], i * sizeof(MeterMajor));
    buffer[(i * sizeof(MeterMajor)) + 2] = (u8)crc;
    buffer[(i * sizeof(MeterMajor)) + 3] = (u8)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    Hardware_DirCheck();
    fnum = File_Write(DefaultDisk, path, buffer, 0, (Meter_num * sizeof(MeterMajor)) + 4);
    myfree(path);
    myfree(buffer);
    return (u16)fnum;
}
/***********************************************
** Function name:       MeterMajor_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 MeterMajor_Read(MeterMajor *MeterMajorBuf)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu16 Meter_num = 0;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    buffer = mymalloc((PORT_LEN * sizeof(MeterMajor)) + 4);
    memset(buffer, 0, ((PORT_LEN * sizeof(MeterMajor)) + 4));
    fnum = File_Read(DefaultDisk, path, buffer, 0, ((PORT_LEN * sizeof(MeterMajor)) + 4));
    Meter_num = (u16)buffer[0] + (u16)(buffer[1] << 8);

    if(Meter_num <= PORT_LEN)
    {
        crc_read = (u16)buffer[Meter_num * sizeof(MeterMajor) + 2] + (u16)(buffer[Meter_num * sizeof(MeterMajor) + 3] << 8);
        crc = (u16)MyCrc16((unsigned char *)&buffer[2], Meter_num * sizeof(MeterMajor));

        if(crc == crc_read)
        {
            for(i = 0; i < Meter_num; i++)
            {
                for(j = 0; j < sizeof(MeterMajor); j++)
                {
                    MeterMajorBuf[i].PropertyMsg[j] = (u8)buffer[(i * sizeof(MeterMajor)) + 2 + j];
                }

                MeterInfo[i].sn = (MeterMajorBuf[i].Property.Id[2] % 16) * 100 + (MeterMajorBuf[i].Property.Id[3] / 16) * 10 + (MeterMajorBuf[i].Property.Id[3] % 16);
            }
        }
    }
    else
    {
        Meter_num = 0;
    }

    myfree(path);
    myfree(buffer);
    return Meter_num;
}

#endif


/***********************************************
** Function name:       DTU_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void DTU_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       DTU_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 DTU_Cfg_Data_Get_Length(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       DTU_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u32 DTU_Cfg_Data_Write(char *DTU_Cfg_Data_Buf, u32 DTU_Cfg_Data_Len, u32 DTU_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, DTU_Cfg_Data_Buf, DTU_Cfg_Data_Off, DTU_Cfg_Data_Len);
    myfree(path);
    return fnum;
}
/***********************************************
** Function name:       DTU_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 DTU_Cfg_Data_Read(char *DTU_Cfg_Data_Buf, u32 DTU_Cfg_Data_Len, u32 DTU_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, DTU_Cfg_Data_Buf, DTU_Cfg_Data_Off, DTU_Cfg_Data_Len);
    myfree(path);
    return (u16)fnum;
}

/***********************************************
** Function name:       MI_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MI_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       MI_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 MI_Data_Get_Length(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       MI_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u32 MI_Data_Write(char *MI_Data_Buf, u32 MI_Data_Len, u32 MI_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, MI_Data_Buf, MI_Data_Off, MI_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       MI_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 MI_Data_Read(char *MI_Data_Buf, u32 MI_Data_Len, u32 MI_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, MI_Data_Buf, MI_Data_Off, MI_Data_Len);
    myfree(path);
    return (u16)fnum;
}

/***********************************************
** Function name:       MI_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MI_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       MI_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 MI_Cfg_Data_Get_Length(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       MI_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u32 MI_Cfg_Data_Write(char *MI_Cfg_Data_Buf, u32 MI_Cfg_Data_Len, u32 MI_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, MI_Cfg_Data_Buf, MI_Cfg_Data_Off, MI_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       MI_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 MI_Cfg_Data_Read(char *MI_Cfg_Data_Buf, u32 MI_Cfg_Data_Len, u32 MI_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, MI_Cfg_Data_Buf, MI_Cfg_Data_Off, MI_Cfg_Data_Len);
    myfree(path);
    return (u16)fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Grid_Profiles_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       Grid_Profiles_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 Grid_Profiles_Data_Get_Length(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u32 Grid_Profiles_Data_Write(char *Grid_Profiles_Data_Buf, u32 Grid_Profiles_Data_Len, u32 Grid_Profiles_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    //fnum = File_Write_Que(DefaultDisk, path, Grid_Profiles_Data_Buf, Grid_Profiles_Data_Off, Grid_Profiles_Data_Len);
    fnum = File_Write(DefaultDisk, path, Grid_Profiles_Data_Buf, Grid_Profiles_Data_Off, Grid_Profiles_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 Grid_Profiles_Data_Read(char *Grid_Profiles_Data_Buf, u32 Grid_Profiles_Data_Len, u32 Grid_Profiles_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, Grid_Profiles_Data_Buf, Grid_Profiles_Data_Off, Grid_Profiles_Data_Len);
    myfree(path);
    return (u16)fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Grid_Profiles_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
u32 Grid_Profiles_Cfg_Data_Get_Length(void)
{
    vu32 length = 0;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       Grid_Profiles_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u32 Grid_Profiles_Cfg_Data_Write(char *Grid_Profiles_Cfg_Data_Buf, u32 Grid_Profiles_Cfg_Data_Len, u32 Grid_Profiles_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, Grid_Profiles_Cfg_Data_Buf, Grid_Profiles_Cfg_Data_Off, Grid_Profiles_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u16 Grid_Profiles_Cfg_Data_Read(char *Grid_Profiles_Cfg_Data_Buf, u32 Grid_Profiles_Cfg_Data_Len, u32 Grid_Profiles_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, Grid_Profiles_Cfg_Data_Buf, Grid_Profiles_Cfg_Data_Off, Grid_Profiles_Cfg_Data_Len);
    myfree(path);
    return (u16)fnum;
}

/***********************************************
** Function name:       ListDate_Check
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    Disk 硬盘盘符
** output parameters:   无
** Returned value:      无
*************************************************/
void ListDate_Check(void)
{
    vu32 Second = 0;
    vu32 limit_date = 0;
    //读取实时时间，更新总目录、实时数据目录、告警目录存储位置，并将偏移重置
    volatile calendar_obj calendar;
    volatile calendar_obj limit_calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);

    if(WriteDate != (calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date))
    {
        WriteDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        WriteOffset = 0;
        DIR_WriteOffset = 0;
        Alarm_WriteOffset = 0;
    }

    Second = DateToSec(calendar);
    SecToDate((Second - 60 * 60 * 24 * 30), (calendar_obj *)&limit_calendar);
    limit_date = limit_calendar.w_year * 10000 + limit_calendar.w_month * 100 + limit_calendar.w_date;

    if((DIR_ReadDate == 0) || (DIR_ReadDate > WriteDate) || (DIR_ReadDate < limit_date))
    {
        DIR_ReadDate = WriteDate;
        DIR_ReadOffset = 0;
        StartAddrLast = 0;
        StartAddrTimes = 0;
    }

    Memory_BKP_Save();
}
/***********************************************
** Function name:       DIR_list_Get_size
** Descriptions:
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 DIR_list_Get_size(u8 Disk, u32 Date)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    //    lf_open(Disk, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
    //    BuffLen = lf_fsize(Disk, &lfnew);
    //    lf_close(Disk, &lfnew);
    BuffLen = Read_File_Length(Disk, path);
    myfree(path);
    return BuffLen;
}

/***********************************************
** Function name:       DIR_Write
** Descriptions:        总目录写
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 DIR_Write(u8 Style, u32 StartAddr, u32 EndAddr, u32 DataLen, u8 Mark, u32 AbsoluteTime)
{
    char *path = NULL;
    char *pSaveBuffer;
    vs16 res;
    vu32 BuffLen = 0;
    vu32 length = 256;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    ListDate_Check();

    if(DIR_WriteOffset < DIR_list_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate))
    {
        DIR_WriteOffset = DIR_list_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate);
        DIR_WriteOffset = ((DIR_WriteOffset / DIR_NEXT) + ((DIR_WriteOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
    }

    sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));
    pSaveBuffer = mymalloc(length);
    memset(pSaveBuffer, 0, length);
    /*包头*/
    sprintf(pSaveBuffer, "DIRStart:");
    BuffLen = BuffLen + DIR_START;

    /*类型*/
    if(Style == History_Style)
    {
        sprintf(&pSaveBuffer[BuffLen], "History");
    }
    else if(Style == AlarmDt_Style)
    {
        sprintf(&pSaveBuffer[BuffLen], "AlarmDt");
    }

    BuffLen = BuffLen + DIR_STYLE;
    /*起始地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", StartAddr);
    BuffLen = BuffLen + START_ADDR_LEN;
    /*结束地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", EndAddr);
    BuffLen = BuffLen + END_ADDR_LEN;
    /*4字节数据长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", DataLen);
    BuffLen = BuffLen + DATA_LEN;

    /*数据包发送状态*/
    switch(Mark)
    {
        case 0:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Failure");
            break;

        case 1:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Success");
            break;

        case 2:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Between");
            break;
    }

    BuffLen = BuffLen + MARK_LEN + MARK_RES_LEN;
    /*绝对时间戳*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", AbsoluteTime);
    BuffLen = BuffLen + ABSOLUTE_TIME;
    /*预留*/
    memset(&pSaveBuffer[BuffLen], 0, RESERVED_SPACE);
    BuffLen = BuffLen + RESERVED_SPACE;
    /*包尾*/
    sprintf(&pSaveBuffer[BuffLen], "DIREnd");
    BuffLen = BuffLen + DIR_END;

    if(BuffLen != DIR_NEXT)
    {
        BuffLen = DIR_NEXT;
    }

    //    lf_open(Dtu3Detail.Property.StorageLocat, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
    //    lf_seek(Dtu3Detail.Property.StorageLocat, &lfnew, DIR_WriteOffset);
    //    lf_write(Dtu3Detail.Property.StorageLocat, &lfnew, pSaveBuffer, BuffLen, &fnum);
    //    lf_close(Dtu3Detail.Property.StorageLocat, &lfnew);
    File_Write(Dtu3Detail.Property.StorageLocat, path, pSaveBuffer, DIR_WriteOffset, BuffLen);
    //printf("Write FilePath:%s\n",FilePath);
    //printf("Write Buffer:%s\n",Buffer);
    //printf("Write Offset:%d\n",Offset);
    //printf("Write len:%d\n",len);
    //printf("fnum:%d\n",fnum);
    DIR_WriteOffset = DIR_WriteOffset + BuffLen;
    DIR_WriteOffset = ((DIR_WriteOffset / DIR_NEXT) + ((DIR_WriteOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
    Memory_BKP_Save();
    myfree(path);
    myfree(pSaveBuffer);
    return BuffLen;
}

/***********************************************
** Function name:       DIR_Local_Read
** Descriptions:        本地总目录读取
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 DIR_Local_Read(u8 DIR_Disk, u32 DIR_Date, u32 DIR_Offset, u8 *Style, u32 *StartAddr, u32 *EndAddr, u32 *DataLen, u8 *Mark, u32 *AbsoluteTime)
{
    vu16 res;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 read_len = 0;
    vu32 i = 0;
    vu32 j = 0;
    vu32 BuffLen = 0;
    vu32 length = 2048;
    vu32 fnum = 0;
    vu8 UseStorage = DefaultDisk;
    vu32 DIR_ReadSec = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", DIR_Disk, (u16)(DIR_Date / 10000), (u8)((DIR_Date % 10000) / 100), (DIR_Date % 100));
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    read_len = File_Read(DIR_Disk, path, pReadBuffer, DIR_Offset, length);

    if(read_len == 0)
    {
        if(DIR_ReadDate < WriteDate)
        {
            Date_Add((u32 *)&DIR_ReadDate);
            DIR_ReadOffset = 0;
            StartAddrLast = 0;
            StartAddrTimes = 0;
            DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
            Memory_BKP_Save();
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }
        else if(DIR_ReadDate == WriteDate)
        {
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }
        else
        {
            DIR_ReadDate = WriteDate;
            DIR_ReadOffset = 0;
            StartAddrLast = 0;
            StartAddrTimes = 0;
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "DIRStart:", DIR_START) == 0)
        {
            i += DIR_START;

            if(strncmp(&pReadBuffer[i], "History", DIR_STYLE) == 0)
            {
                *Style = History_Style;
            }
            else if(strncmp(&pReadBuffer[i], "AlarmDt", DIR_STYLE) == 0)
            {
                *Style = AlarmDt_Style;
            }
            else
            {
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }

            i += DIR_STYLE;
            /*起始地址*/
            *StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            *EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            *DataLen = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 0;
                /*绝对时间戳*/
                *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;

                if((DIR_ReadDate > DIR_ReadDate_Last) || (DIR_ReadOffset > DIR_ReadOffset_Last))
                {
                    DIR_ReadDate_Last = DIR_ReadDate;
                    DIR_ReadOffset_Last = DIR_ReadOffset;
                    DIR_Scan_Times = 0;
                }
                else
                {
                    DIR_Scan_Times++;
                }

                if(DIR_Scan_Times > STORE_REREAD_TIMES)
                {
                    *Mark = 1;
                    /*绝对时间戳*/
                    DIR_ReadDate_Last = DIR_ReadDate;
                    DIR_ReadOffset_Last = DIR_ReadOffset + read_len;
                    DIR_ReadOffset_Last = ((DIR_ReadOffset_Last / DIR_NEXT) + ((DIR_ReadOffset_Last % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                }

                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 1;
                /*绝对时间戳*/
                *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 2;
                /*绝对时间戳*/
                *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                DIR_ReadOffset = DIR_ReadOffset + i - j;
                DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                Memory_BKP_Save();
                j = i;
            }

            i = i + LIST_END_LEN;
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return i;
}

/***********************************************
** Function name:       DIR_Read
** Descriptions:        总目录读取
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 DIR_Read(u8 *Style, u32 *StartAddr, u32 *EndAddr, u32 *DataLen, u8 *Mark, u32 *AbsoluteTime)
{
    vu16 res;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 read_len = 0;
    vu32 i = 0;
    vu32 length = 128;
    vu8 UseStorage = DefaultDisk;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);

    if(DIR_ReadDate == 0)
    {
        DIR_ReadDate = WriteDate;
        DIR_ReadOffset = 0;
        StartAddrLast = 0;
        StartAddrTimes = 0;
    }
    else if(DIR_ReadDate > WriteDate)
    {
        DIR_ReadDate = WriteDate;
        DIR_ReadOffset = 0;
        StartAddrLast = 0;
        StartAddrTimes = 0;
    }

    if(DIR_ReadDate == WriteDate)
    {
        if(DIR_ReadOffset > DIR_WriteOffset)
        {
            if(WriteOffset == 0)
            {
                DIR_ReadOffset = 0;
                StartAddrLast = 0;
                StartAddrTimes = 0;
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                DIR_ReadOffset = DIR_WriteOffset;
                DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                Memory_BKP_Save();
            }
        }
    }

#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if(i == 1)
        {
            UseStorage = ExpansionDisk;
        }

        sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", UseStorage, (u16)(DIR_ReadDate / 10000), (u8)((DIR_ReadDate % 10000) / 100), (DIR_ReadDate % 100));
        res = File_Read(UseStorage, path, pReadBuffer, 0, length);

        if(res != 0)
        {
            break;
        }
    }

#endif
    myfree(path);
    myfree(pReadBuffer);
    read_len = DIR_Local_Read(UseStorage, DIR_ReadDate, DIR_ReadOffset, Style, StartAddr, EndAddr, DataLen, Mark, AbsoluteTime);
    return read_len;
}

/***********************************************
** Function name:       DIR_Mark_Replace
** Descriptions:        总目录重标记
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void DIR_Mark_Replace(u8 Disk, u32 ReplaceDate, u32 ReplaceOffset, u8 DIR_Style, u32 DIR_StartAddr, u32 DIR_EndAddr, u32 DIR_DataLen, u8 DIR_Mark, u32 AbsoluteTime)
{
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu8 Mark;
    vu8 Style;
    vu32 StartAddr;
    vu32 EndAddr;
    vu32 DataLen;
    vs32 Offset = 0;
    vu32 i = 0;
    vu32 Read_len = 0;
    vu32 find = 0;
    vu16 res;
    vu32 read_len = 0;
    vu32 length = 2048;
    vu8 UseStorage = DefaultDisk;
    vu8 error_times = 0;
    vu32 AbsoluteTimeGet = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if(i == 1)
        {
            UseStorage = ExpansionDisk;
        }

        sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", UseStorage, (u16)(ReplaceDate / 10000), (u8)((ReplaceDate % 10000) / 100), (ReplaceDate % 100));
        res = File_Read(UseStorage, path, pReadBuffer, 0, 256);

        if(res != 0)
        {
            break;
        }
    }

#endif
    sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", UseStorage, (u16)(ReplaceDate / 10000), (u8)((ReplaceDate % 10000) / 100), (ReplaceDate % 100));

    for(;;)
    {
        memset(pReadBuffer, 0, length);
        read_len = File_Read(UseStorage, path, pReadBuffer, ReplaceOffset + (u16)(Offset), length);

        if(read_len == 0)
        {
            myfree(path);
            myfree(pReadBuffer);
            return;
        }

        for(i = 0; i < read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "DIRStart:", DIR_START) == 0)
            {
                i += DIR_START;

                if(strncmp(&pReadBuffer[i], "History", DIR_STYLE) == 0)
                {
                    Style = History_Style;
                }
                else if(strncmp(&pReadBuffer[i], "AlarmDt", DIR_STYLE) == 0)
                {
                    Style = AlarmDt_Style;
                }
                else
                {
                    myfree(path);
                    myfree(pReadBuffer);
                    return;
                }

                i += DIR_STYLE;
                /*起始地址*/
                StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
                i += START_ADDR_LEN;
                /*结束地址*/
                EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
                i += END_ADDR_LEN;
                DataLen = astr2int(&pReadBuffer[i], DATA_LEN);
                i += DATA_LEN;

                if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
                {
                    i = i + MARK_LEN + MARK_RES_LEN;
                    AbsoluteTimeGet = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                    i = i + ABSOLUTE_TIME;
                    Mark = 0;
                }
                else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
                {
                    i = i + MARK_LEN + MARK_RES_LEN;
                    AbsoluteTimeGet = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                    i = i + ABSOLUTE_TIME;
                    Mark = 1;
                }
                else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
                {
                    i = i + MARK_LEN + MARK_RES_LEN;
                    AbsoluteTimeGet = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                    i = i + ABSOLUTE_TIME;
                    Mark = 2;
                }

                if(AbsoluteTime == 0)
                {
                    myfree(data);
                    myfree(path);
                    myfree(pReadBuffer);
                    return;
                }

                if(AbsoluteTimeGet == AbsoluteTime)
                {
                    if((Style == DIR_Style) && (StartAddr == DIR_StartAddr) && (EndAddr == DIR_EndAddr))
                    {
                        find = 1;
                        break;
                    }
                    else if((i + DIR_NEXT) > read_len)
                    {
                        find = 0;
                        Offset = (s32)(Offset + (s32)i - (s32)DIR_NEXT);
                        break;
                    }
                }
                else if(AbsoluteTimeGet > (AbsoluteTime))
                {
                    if((ReplaceOffset + (u16)Offset - 1536) > 0)
                    {
                        Offset = Offset - 1536;
                        error_times++;
                        find = 0;
                    }
                    else
                    {
                        Offset = (s32)ReplaceOffset * (-1);
                        find = 0;
                    }

                    if(error_times > STORE_REREAD_TIMES)
                    {
                        myfree(data);
                        myfree(path);
                        myfree(pReadBuffer);
                        return;
                    }

                    break;
                }
                else if((AbsoluteTimeGet) < AbsoluteTime)
                {
                    if((i + DIR_NEXT) > read_len)
                    {
                        find = 0;
                        Offset = (s32)(Offset + (s32)i - (s32)DIR_NEXT);
                        break;
                    }
                    else
                    {
                        i = i + MARK_LEN + MARK_RES_LEN;
                    }
                }

                //                if((Style == DIR_Style) && (StartAddr == DIR_StartAddr) && (EndAddr == DIR_EndAddr) && (DataLen == DIR_DataLen))
                //                {
                //                    find = 1;
                //                    break;
                //                }
                //                else if((Style == DIR_Style) && (StartAddr > DIR_StartAddr) && (EndAddr > DIR_EndAddr))
                //                {
                //                    if((ReplaceOffset + Offset - 1024) > 0)
                //                    {
                //                        Offset = Offset - 1024;
                //                        error_times++;
                //                    }
                //                    else
                //                    {
                //                        Offset = (s32)ReplaceOffset * (-1);
                //                    }
                //                    if(error_times > 3)
                //                    {
                //                        myfree(data);
                //                        myfree(path);
                //                        myfree(pReadBuffer);
                //                        return;
                //                    }
                //                    find = 0;
                //                    break;
                //                }
                //                else if((Style == DIR_Style) && (StartAddr < DIR_StartAddr) && (EndAddr < DIR_EndAddr))
                //                {
                //                    if((i + DIR_NEXT) > read_len)
                //                    {
                //                        Offset = Offset + 1024;
                //                        find = 0;
                //                        break;
                //                    }
                //                    else
                //                    {
                //                        i = i + MARK_LEN + MARK_RES_LEN + RESERVED_SPACE;
                //                    }
                //                }
            }
        }

        if(find == 1)
        {
            break;
        }
        else if((AbsoluteTimeGet) == AbsoluteTime)
        {
            Offset = Offset - DIR_NEXT;
        }
    }

    find = 0;
    sprintf(path, "%d:/Data/DIR_%04d%02d%02d_list.dat", UseStorage, (u16)(ReplaceDate / 10000), (u8)((ReplaceDate % 10000) / 100), (ReplaceDate % 100));
    Read_len = File_Read(UseStorage, path, pReadBuffer, (u16)((s32)ReplaceOffset + Offset), length);
    data = mymalloc(100);
    memset(data, 0, 100);

    if(DIR_Mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(DIR_Mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(DIR_Mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    if(Read_len != 0)
    {
        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                Offset = Offset + DIR_NEXT;
                memset(pReadBuffer, 0, length);
                Read_len = File_Read(UseStorage, path, pReadBuffer, (u32)Offset, length);
                i = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        if(find == 1)
        {
            if(strncmp(&pReadBuffer[i], data, MARK_LEN + MARK_RES_LEN) != 0)
            {
                //                lf_open(Disk, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
                //                lf_seek(Disk, &lfnew, Offset + i);
                //                lf_write(Disk, &lfnew, data, strlen(data), &fnum);
                //                lf_close(Disk, &lfnew);
                File_Write(UseStorage, path, data, (u16)((s32)ReplaceOffset + Offset + (s32)i), strlen(data));
            }

            i = i + MARK_LEN + MARK_RES_LEN;
        }
    }

    Memory_BKP_Save();
    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}

/***********************************************
** Function name:       RealTime_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    Disk 硬盘盘符
** output parameters:   无
** Returned value:      无
*************************************************/
void RealTime_DirCheck(u8 Disk)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", Disk);
    Dir_Check(Disk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/List", Disk);
    Dir_Check(Disk, path);
    myfree(path);
}
/***********************************************
** Function name:       Data_List_Get_size
** Descriptions:
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Data_List_Get_size(u8 Disk, u32 Date)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    //    lf_open(Disk, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
    //    BuffLen = lf_fsize(Disk, &lfnew);
    //    lf_close(Disk, &lfnew);
    BuffLen = Read_File_Length(Disk, path);
    myfree(path);
    return BuffLen;
}

/***********************************************
** Function name:       Data_List_Write
** Descriptions:
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Data_List_Write(u8 Disk, u32 Date, u32 *Offset, u8 PackNum, u8 PackAll, u32 StartAddr, u32 EndAddr, u32 Len, u8 Mark, u32 AbsoluteTime)
{
    char *path = NULL;
    char *pSaveBuffer;
    vu32 BuffLen = 0;
    vu32 length = 256;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    pSaveBuffer = mymalloc(length);
    memset(pSaveBuffer, 0, length);
    /*包头*/
    sprintf(pSaveBuffer, "ListStart:");
    BuffLen = BuffLen + LIST_START_LEN;
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = BuffLen + PACK_NO_LEN + PACK_ALL_LEN;
    /*起始地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", StartAddr);
    BuffLen = BuffLen + START_ADDR_LEN;
    /*结束地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", EndAddr);
    BuffLen = BuffLen + END_ADDR_LEN;
    /*4字节数据长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = BuffLen + DATA_LEN;

    /*数据包发送状态*/
    switch(Mark)
    {
        case 0:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Failure");
            break;

        case 1:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Success");
            break;

        case 2:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Between");
            break;
    }

    BuffLen = BuffLen + MARK_LEN + MARK_RES_LEN;
    //    /*目录地址偏移*/
    //    sprintf(&pSaveBuffer[BuffLen], "%010d", DirOff);
    //    BuffLen = BuffLen + DIR_OFF_LEN;
    /*绝对时间戳*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", AbsoluteTime);
    BuffLen = BuffLen + ABSOLUTE_TIME;
    /*预留*/
    memset(&pSaveBuffer[BuffLen], 0, RESERVED_SPACE);
    BuffLen = BuffLen + RESERVED_SPACE;
    /*包尾*/
    sprintf(&pSaveBuffer[BuffLen], "ListEnd");
    BuffLen  = BuffLen + LIST_END_LEN;

    if(BuffLen != LIST_NEXT)
    {
        BuffLen = LIST_NEXT;
    }

    //    lf_open(Disk, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
    //    lf_seek(Disk, &lfnew, *Offset);
    //    lf_write(Disk, &lfnew, pSaveBuffer, BuffLen, &fnum);
    //    lf_close(Disk, &lfnew);
    File_Write(Disk, path, pSaveBuffer, *Offset, BuffLen);
    DIR_Write(History_Style, *Offset, *Offset + BuffLen, BuffLen, Mark, AbsoluteTime);
    //printf("Write FilePath:%s\n",FilePath);
    //printf("Write Buffer:%s\n",Buffer);
    //printf("Write Offset:%d\n",Offset);
    //printf("Write len:%d\n",len);
    //printf("fnum:%d\n",fnum);
    *Offset = *Offset + BuffLen;
    Memory_BKP_Save();
    myfree(path);
    myfree(pSaveBuffer);
    return BuffLen;
}

u32 Data_List_Read(u8 Disk, u32 Date, u32 Offset, u8 *PackNum, u8 *PackAll, u32 *StartAddr, u32 *EndAddr, u32 *Len, u8 *Mark, u32 *AbsoluteTime)
{
    vs16 res;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 read_len = 0;
    vu32 i = 0;
    vu32 BuffLen = 0;
    vu32 length = 512;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    read_len = File_Read(Disk, path, pReadBuffer, Offset, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "ListStart:", LIST_START_LEN) == 0)
        {
            i += LIST_START_LEN;
            /*获取第几包*/
            *PackNum = (u8)astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = (u8)astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*起始地址*/
            *StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            *EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            *Len = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 1;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 2;
            }

            //            /*目录地址偏移*/
            //            *DirOff = astr2int(&pReadBuffer[i], DIR_OFF_LEN);
            //            i = i + DIR_OFF_LEN;
            /*绝对时间戳*/
            *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
            i = i + ABSOLUTE_TIME;
            i = i + LIST_END_LEN;
            break;
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return read_len;
}

void Data_List_RealTime_Mark(u8 mark)
{
    static vu32 FailureOffset = 0;
    static vu8 FailureTimes = 0;
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vs32 Offset = 0;
    vu32 i = 0;
    vu32 j = 0;
    vu32 length = 256;
    vu32 Read_len = 0;
    vu32 find = 0;
    vu32 DirOff;
    vu32 AbsoluteTime = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
    Memory_BKP_Read();
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    find = 0;

    for(;;)
    {
        Read_len = File_Read(Dtu3Detail.Property.StorageLocat, path, pReadBuffer, ((s32)WriteOffset - Offset), length);

        if(Read_len == 0)
        {
            if(Offset < WriteOffset)
            {
                Offset = Offset + LIST_NEXT;
            }
            else
            {
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                //                /*目录地址偏移*/
                //                DirOff = astr2int(&pReadBuffer[i], DIR_OFF_LEN);
                //                i = i + DIR_OFF_LEN;
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                find = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                Offset = Offset - LIST_NEXT;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        if(find == 1)
        {
            break;
        }
    }

    if(find == 1)
    {
        if(strncmp(&pReadBuffer[j], data, MARK_LEN + MARK_RES_LEN) != 0)
        {
            //            lf_open(Dtu3Detail.Property.StorageLocat, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
            //            lf_seek(Dtu3Detail.Property.StorageLocat, &lfnew, WriteOffset - Offset + i);
            //            lf_write(Dtu3Detail.Property.StorageLocat, &lfnew, data, strlen(data), &fnum);
            //            lf_close(Dtu3Detail.Property.StorageLocat, &lfnew);
            File_Write(Dtu3Detail.Property.StorageLocat, path, data, (s32)WriteOffset - Offset + (s32)j, strlen(data));
            DIR_Mark_Replace(Dtu3Detail.Property.StorageLocat, WriteDate, DIR_WriteOffset - (DIR_NEXT), History_Style, (WriteOffset - Offset + j + MARK_LEN + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN) - LIST_NEXT,
                             ((s32)WriteOffset - Offset + (s32)j + MARK_LEN + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN), LIST_NEXT, mark, AbsoluteTime);
        }

        i = i + MARK_LEN + MARK_RES_LEN;
    }

    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}

void Data_List_History_Mark(u8 mark)
{
    static vu32 FailureOffset = 0;
    static vu8 FailureTimes = 0;
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu8 UseStorage = DefaultDisk;
    vu32 fnum = 0;
    vu32 Offset = 0;
    vu32 i = 0;
    vu32 j = 0;
    vu32 length = 2048;
    vu32 Read_len = 0;
    vu32 find = 0;
    vu32 DirOff = 0;
    vu32 AbsoluteTime = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
    Memory_BKP_Read();
#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if(i == 1)
        {
            UseStorage = ExpansionDisk;
        }

        sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", UseStorage, (u16)(UploadedDate / 10000), (u8)((UploadedDate % 10000) / 100), (UploadedDate % 100));
        Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset, length);

        if(Read_len != 0)
        {
            break;
        }
    }

#else
    UseStorage = DefaultDisk;
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", UseStorage, (u16)(UploadedDate / 10000), (u8)((UploadedDate % 10000) / 100), (UploadedDate % 100));
    Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset, length);
#endif

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    if(Read_len != 0)
    {
        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                //                /*目录地址偏移*/
                //                DirOff = astr2int(&pReadBuffer[i], DIR_OFF_LEN);
                //                i = i + DIR_OFF_LEN;
                /*绝对时间戳*/
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                //printf("find head\n");
                //printf("%d\n", i);
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                if(Read_len < (i + LIST_NEXT))
                {
                    Offset = Offset + LIST_NEXT;
                    memset(pReadBuffer, 0, (length));
                    Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset + Offset, length);
                    i = 0;
                }
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                UploadedOffset = UploadedOffset + InsideOffset + Offset + i;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        if(find == 1)
        {
            //                lf_open(UseStorage, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
            //                lf_seek(UseStorage, &lfnew, UploadedOffset + Offset + i);
            //                lf_write(UseStorage, &lfnew, data, strlen(data), &fnum);
            //                lf_close(UseStorage, &lfnew);
            File_Write(UseStorage, path, data, UploadedOffset + Offset + j, strlen(data));
            DIR_Mark_Replace(UseStorage, UploadedDate, DIR_ReadOffset, History_Style, (UploadedOffset - Offset + j + MARK_LEN + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN) - LIST_NEXT,
                             (UploadedOffset - Offset + j + MARK_LEN + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN), LIST_NEXT, mark, AbsoluteTime);
        }
    }

    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
    Memory_BKP_Save();
    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}

u8 Historical_Presence(void)
{
    vu32 DirOff = 0;
    vu32 UploadedSec = 0;
    vu32 ScanningTimes = 0;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 read_len = 0;
    vu32 BetweenLen = 0;
    vu32 backlen = 0;
    vu32 length = 256;
    vs32 offset = 0;
    vu32 i = 0, j = 0;
    vu16 crc;
    vu16 crc_read;
    vu8 UseStorage = DefaultDisk;
    vu8 find = 0;
    vu8 hour = 0;
    vu8 min = 0;
    vu8 sec = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    vu8 GetPackNum = 0;
    vu8 GetPackAll = 0;
    vu32 GetStartAddr = 0;
    vu32 GetEndAddr = 0;
    vu32 GetLen = 0;
    vu32 DateRange = 0;
    vu8 GetMark = 0;
    vu8 Mark = 0;
    vu32 res = 0;
    vu32 AbsoluteTime = 0;
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    Memory_BKP_Read();
    pReadBuffer = mymalloc(length);
    ListDate_Check();
    calendar.w_month  = calendar.w_month - 1;
    RTC_Correction((calendar_obj *)&calendar);
    DateRange = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;

    if(UploadedDate == 0)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate > WriteDate)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate < DateRange)
    {
        UploadedDate = DateRange;
        UploadedOffset = 0;
        InsideOffset = 0;
    }

    if(UploadedDate == WriteDate)
    {
        if(UploadedOffset > WriteOffset)
        {
            if(WriteOffset == 0)
            {
                UploadedOffset = 0;
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                InsideOffset = 0;
                return 0;
            }
            else
            {
                UploadedOffset = WriteOffset;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
                InsideOffset = 0;
            }
        }
    }

    for(;;)
    {
#ifdef DTU3PRO

        for(i = 0;; i++)
        {
            if((i % 2) == 0)
            {
                UseStorage = DefaultDisk;
                res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

                if(res != 0)
                {
                    break;
                }
            }
            else if((i % 2) == 1)
            {
                UseStorage = ExpansionDisk;
                res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

                if(res != 0)
                {
                    break;
                }
            }

            if((i % 2) == 1)
            {
                if(UploadedDate < WriteDate)
                {
                    Date_Add((u32 *)&UploadedDate);
                    UploadedOffset = 0;
                    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                    Memory_BKP_Save();
                }
                else if(UploadedDate == WriteDate)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    UploadedDate = WriteDate;
                    UploadedOffset = 0;
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }

#else
        UseStorage = DefaultDisk;
        res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);
#endif

        if(res == 0)
        {
            if(UploadedDate < WriteDate)
            {
                Date_Add((u32 *)&UploadedDate);
                UploadedOffset = 0;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
            }
            else if(UploadedDate == WriteDate)
            {
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                UploadedDate = WriteDate;
                UploadedOffset = 0;
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
        }
        else
        {
            break;
        }
    }

    for(;;)
    {
        if(GetMark == 0)
        {
            if((GetPackNum == 0) && (GetPackAll == 0) && (GetStartAddr == 0) && (GetEndAddr == 0))
            {
                if(UploadedDate < WriteDate)
                {
                    Date_Add((u32 *)&UploadedDate);
                    UploadedOffset = 0;
                    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                    Memory_BKP_Save();
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            if(GetMark == 1)
            {
                UploadedOffset = (u32)((s32)UploadedOffset + offset);
                offset = 0;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
                ScanningTimes++;
#ifndef DEBUG
                IWDG_Feed();
#endif

                if(ScanningTimes >= 20)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }

            offset += LIST_NEXT;
            res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

            if(res == 0)
            {
                if(UploadedDate < WriteDate)
                {
                    Date_Add((u32 *)&UploadedDate);
                    UploadedOffset = 0;
                    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                    Memory_BKP_Save();
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return 1;
}
/***********************************************
** Function name:       Local_Historical_Data_Read
** Descriptions:        读取历史数据
** input parameters:    Disk 盘符 Date日期  Offset读偏移  len长度
** output parameters:   *Buffer读出的数据
** Returned value:      成功长度
*************************************************/
u16 Local_Historical_Data_Read(u8 Disk, u32 Date, char *Buffer, u16 Offset, u16 len)
{
    //Data/20190124/HistoricalData.dat
    char *path = NULL;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    fnum = File_Read(Disk, path, Buffer, Offset, len);
    myfree(path);
    return (u16)fnum;
}
/***********************************************
** Function name:       RealTime_Data_Pack
** Descriptions:        实时数据打包成存储格式
** input parameters:    PackNum当前包数 PackAll总包数 *Data数据 len长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u32 RealTime_Data_Pack(u8 PackNum, u8 PackAll, char *Data, u32 Len)
{
    char *path = NULL;
    char *pSaveBuffer = NULL;
    vu8 LastMark = 0;
    vu8 Mark = 0;
    vu8 LastPackNum = 0;
    vu8 LastPackAll = 0;
    vu16 crc = 0;
    vu32 res = 0;
    vu32 DirOff = 0;
    vu32 fsize = 0;
    vu32 BuffLen = 0;
    vu32 LastStartAddr = 0;
    vu32 LastEndAddr = 0;
    vu32 LastLen = 0;
    vu32 AbsoluteTime = 0;
    vu32 AbsoluteTimeGet = 0;
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    AbsoluteTime = RTC_Getsecond();

    if(Len == 0)
    {
        myfree(path);
        myfree(pSaveBuffer);
        return 0;
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pSaveBuffer = mymalloc((Len + 256));
    memset(pSaveBuffer, 0, (Len + 256));
    Memory_BKP_Read();
    ListDate_Check();

    if(WriteOffset < Data_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate))
    {
        WriteOffset = Data_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate);
        WriteOffset = ((WriteOffset / LIST_NEXT) + ((WriteOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
    }

    if(WriteOffset >= LIST_NEXT)
    {
        res = Data_List_Read(Dtu3Detail.Property.StorageLocat, WriteDate, WriteOffset - LIST_NEXT, (u8 *)&LastPackNum, (u8 *)&LastPackAll, (u32 *)&LastStartAddr, (u32 *)&LastEndAddr, (u32 *)&LastLen, (u8 *)&LastMark, (u32 *)&AbsoluteTimeGet);
    }

    if(res == 0)
    {
        LastPackNum = 0;
        LastPackAll = 0;
        LastStartAddr = 0;
        LastEndAddr = 0;
        LastLen = 0;
        LastMark = 0;
    }

    BuffLen = 0;
    /*包头Start*/
    sprintf(pSaveBuffer, "Start:");
    BuffLen = BuffLen + START_LEN;
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = BuffLen + PACK_NO_LEN + PACK_ALL_LEN;
    /*xx:xx:xx时间*/
    sprintf(&pSaveBuffer[BuffLen], "%02d:%02d:%02d", calendar.hour, calendar.min, calendar.sec);
    BuffLen = BuffLen + HOUR_LEN + MIN_LEN + SEC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = BuffLen + DATA_LEN;
    /*数据*/
    memcpy(&pSaveBuffer[BuffLen], Data, Len);
    crc = (u16)MyCrc16((unsigned char *)&pSaveBuffer[BuffLen], Len);
    BuffLen  = BuffLen + Len;
    /*数据校验*/
    sprintf(&pSaveBuffer[BuffLen], "%04x", crc);
    BuffLen = BuffLen + CRC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", strlen(pSaveBuffer));
    BuffLen  = BuffLen + DATA_LEN;
    /*包尾End*/
    sprintf(&pSaveBuffer[BuffLen], "End");
    BuffLen  = BuffLen + END_LEN;
    //printf("\n%s\n",pSaveBuffer);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));
    //printf("%s\n",path);
    RealTime_DirCheck(Dtu3Detail.Property.StorageLocat);
    File_Write_Que(Dtu3Detail.Property.StorageLocat, path, pSaveBuffer, LastEndAddr, BuffLen);

    if((PackNum + 1) == PackAll)
    {
        Mark = 0;
    }
    else
    {
        Mark = 2;
    }

    Data_List_Write(Dtu3Detail.Property.StorageLocat, WriteDate, (u32 *)&WriteOffset, PackNum, PackAll, LastEndAddr, LastEndAddr + BuffLen, BuffLen, Mark, AbsoluteTime);
    WriteOffset = ((WriteOffset / LIST_NEXT) + ((WriteOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
    Memory_BKP_Save();
    myfree(path);
    myfree(pSaveBuffer);
    return 0;
}

/***********************************************
** Function name:       History_Data_Unpack
** Descriptions:        历史数据解包
** input parameters:    无
** output parameters:   PackNum当前包数 PackAll总包数 *Data数据
** Returned value:      成功长度
*************************************************/
u32 History_Data_Unpack(u8 PackNum, u8 *PackAll, char *Data)
{
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu8 sec = 0;
    vu8 min = 0;
    vu8 hour = 0;
    vu8 find = 0;
    vu8 Mark = 0;
    vu8 GetMark = 0;
    vu8 GetPackNum = 0;
    vu8 GetPackAll = 0;
    vu8 unpack_times = 0;
    vu8 UseStorage = DefaultDisk;
    vu16 crc;
    vu16 crc_read;
    vu32 UploadedSec = 0;
    vu32 ScanningTimes = 0;
    vu32 read_len = 0;
    vu32 BetweenLen = 0;
    vu32 backlen = 0;
    vu32 length = 256;
    vs32 offset = 0;
    vu32 i = 0, j = 0;
    vu32 GetStartAddr = 0;
    vu32 GetEndAddr = 0;
    vu32 GetLen = 0;
    vu32 res = 0;
    vu32 DirOff = 0;
    vu32 AbsoluteTime = 0;
    Memory_BKP_Read();
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    ListDate_Check();

    if(UploadedDate == 0)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate > WriteDate)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }

    if(UploadedDate == WriteDate)
    {
        if(UploadedOffset > WriteOffset)
        {
            if(WriteOffset == 0)
            {
                UploadedOffset = 0;
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                InsideOffset = 0;
                return 0;
            }
            else
            {
                UploadedOffset = WriteOffset;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
                InsideOffset = 0;
            }
        }
    }

    if(PackNum == 0)
    {
        InsideOffset = 0;
    }

    for(;;)
    {
#ifdef DTU3PRO

        for(i = 0; i < 2; i++)
        {
#ifndef DEBUG
            IWDG_Feed();
#endif

            if(i == 0)
            {
                UseStorage = DefaultDisk;
            }
            else if(i == 1)
            {
                UseStorage = ExpansionDisk;
            }

            res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

            if(res != 0)
            {
                break;
            }
        }

#else
        UseStorage = DefaultDisk;
        res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);
#endif

        if(res == 0)
        {
            if(UploadedDate < WriteDate)
            {
                Date_Add((u32 *)&UploadedDate);
                UploadedOffset = 0;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
            }
            else if(UploadedDate == WriteDate)
            {
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                UploadedDate = WriteDate;
                UploadedOffset = 0;
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
        }
        else
        {
            break;
        }
    }

    if((UploadedDate_Last != UploadedDate) ||
            (UploadedOffset > UploadedOffset_Last) ||
            (Uploaded_PackNum_Last != PackNum) ||
            (Uploaded_PackAll_Last != *PackAll))
    {
        Uploaded_Times = 0;
    }
    else
    {
        Uploaded_Times++;
    }

    if(Uploaded_Times > 3)
    {
        //        DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
        //        DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(;;)
    {
#ifndef DEBUG
        IWDG_Feed();
#endif

        if(GetMark == 0)
        {
            if((GetPackNum == 0) && (GetPackAll == 0) && (GetStartAddr == 0) && (GetEndAddr == 0))
            {
                if(UploadedDate < WriteDate)
                {
                    Date_Add((u32 *)&UploadedDate);
                    UploadedOffset = 0;
                    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                    Memory_BKP_Save();
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            if(GetMark == 1)
            {
                UploadedOffset = (u32)((s32)UploadedOffset + offset);
                //DIR_Mark_Replace(UseStorage, UploadedDate, DIR_ReadOffset, History_Style, GetStartAddr, GetEndAddr, GetLen, GetMark, AbsoluteTime);
                offset = 0;
                UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                Memory_BKP_Save();
                ScanningTimes++;
#ifndef DEBUG
                IWDG_Feed();
#endif

                if(ScanningTimes >= 20)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }

            offset += LIST_NEXT;
            res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

            if(res == 0)
            {
                if(UploadedDate < WriteDate)
                {
                    Date_Add((u32 *)&UploadedDate);
                    UploadedOffset = 0;
                    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
                    Memory_BKP_Save();
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }
    }

    for(;;)
    {
#ifndef DEBUG
        IWDG_Feed();
#endif

        if(GetPackNum == PackNum)
        {
            if(GetPackNum == 0)
            {
                UploadedOffset = (u32)((s32)(UploadedOffset + InsideOffset) + offset);
            }

            break;
        }
        else
        {
            unpack_times++;

            if(unpack_times >= 20)
            {
                myfree(path);
                myfree(pReadBuffer);
                //                DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
                //                DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                return 0;
            }

            if(GetPackNum > PackNum)
            {
                offset = offset - LIST_NEXT;
            }
            else if(GetPackNum < PackNum)
            {
                offset = offset + LIST_NEXT;
            }
        }

        if((s32)((s32)UploadedOffset + (s32)InsideOffset + offset) < 0)
        {
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }

        res = Data_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, (u8 *)&GetPackNum, (u8 *)&GetPackAll, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTime);

        if(res == 0)
        {
            GetPackNum = 0;
            GetPackAll = 0;
            GetStartAddr = 0;
            GetEndAddr = 0;
            GetLen = 0;
            GetMark = 0;
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }
    }

    length = 256;
    find = 0;
    memset(pReadBuffer, 0, (length));
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", UseStorage, (u16)(UploadedDate / 10000), (u8)((UploadedDate % 10000) / 100), (UploadedDate % 100));
    read_len = File_Read(UseStorage, path, pReadBuffer, GetStartAddr, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
        {
            i += START_LEN;
            /*获取第几包*/
            GetPackNum = (u8)astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = (u8)astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*获取时*/
            hour = (u8)astr2int(&pReadBuffer[i], HOUR_LEN);
            i += HOUR_LEN;
            /*获取分*/
            min = (u8)astr2int(&pReadBuffer[i], MIN_LEN);
            i += MIN_LEN;
            /*获取秒*/
            sec = (u8)astr2int(&pReadBuffer[i], SEC_LEN);
            i += SEC_LEN;
            /*获取长度*/
            length = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;
            myfree(pReadBuffer);
            pReadBuffer = mymalloc(length + CRC_LEN + END_LEN);
            memset(pReadBuffer, 0, (length + CRC_LEN + END_LEN));
            j = i;
            File_Read(UseStorage, path, pReadBuffer, GetStartAddr + i, length + CRC_LEN + END_LEN);
            crc = (u16)MyCrc16((unsigned char *)&pReadBuffer[i - j], length);
            crc_read = hstr2int(&pReadBuffer[i - j + length], CRC_LEN);

            if(StartAddrTimes < STORE_REREAD_TIMES)
            {
                if(GetStartAddr <= StartAddrLast)
                {
                    StartAddrTimes++;
                }
                else
                {
                    StartAddrTimes = 0;
                    StartAddrLast = GetStartAddr;
                }
            }
            else
            {
                StartAddrTimes = 0;
                InsideOffset = 0;
                StartAddrLast = 0;
                //                DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
                //                DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }

            if(crc == crc_read)
            {
                memcpy(Data, &pReadBuffer[i - j], length);
            }
            else
            {
                //                DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
                //                DIR_ReadOffset = ((DIR_ReadOffset / DIR_NEXT) + ((DIR_ReadOffset % DIR_NEXT) > 0 ? 1 : 0)) * DIR_NEXT;
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }

            i += length;
            i += CRC_LEN;
            i += END_LEN;
            InsideOffset = InsideOffset + LIST_NEXT;
            UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
            Memory_BKP_Save();
            myfree(path);
            myfree(pReadBuffer);
            return length;
        }
    }

    UploadedOffset = ((UploadedOffset / LIST_NEXT) + ((UploadedOffset % LIST_NEXT) > 0 ? 1 : 0)) * LIST_NEXT;
    Memory_BKP_Save();
    myfree(path);
    myfree(pReadBuffer);
    return 0;
}

void memset32_t(u32 *a, u32 b, u8 c)
{
    while(c--)
    {
        a[c] = b;
    }
}
void DeleteOldestData(void)
{
    static vu8 Delete_i;
    static vu8 DeleteFlag = 0;
    static vu16 Num = 0;
    static vu16 Max = 10;
    static vu8 j;
    /*将就小文件名设为最大值*/
    static vu32 MinNum[5];
    static vu32 MinNumExp[5];
    static vu32 Dele_Num = 0;
    char *path = NULL;

    switch(DeleteDataState)
    {
        case Dele_Default:
            Num = 0;
            memset32_t((u32 *)MinNum, 0xFFFFFFFF, RemainQuant);
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data", DefaultDisk);
            /*通过扫描找到所有文件名*/
            lf_ListDirFiles(DefaultDisk, path, (u32 *)MinNum, (u16 *)&Num, RemainQuant, 8);
            myfree(path);
            DeleteDataState = Dele_Expansion;
            break;

        case Dele_Expansion:
#ifdef DTU3PRO
            Num = 0;
            memset32_t((u32 *)MinNumExp, 0xFFFFFFFF, RemainQuant);
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data", ExpansionDisk);
            /*通过扫描找到所有文件名*/
            lf_ListDirFiles(ExpansionDisk, path, (u32 *)MinNumExp, (u16 *)&Num, RemainQuant, 8);
            myfree(path);
#endif
            Delete_i = 0;
            DeleteDataState = Dele_Compare;
            break;

        case Dele_Compare:
            if(Delete_i < RemainQuant)
            {
                DeleteFlag = 0;
#ifndef DTU3PRO
                Dele_Num = MinNum[Delete_i];
#else

                /*比较两组目录，找到最小的文件名所在的目录并删除*/
                for(j = 0; j < RemainQuant; j++)
                {
                    if(MinNumExp[j] < MinNum[Delete_i])
                    {
                        Dele_Num = MinNumExp[j];
                        MinNumExp[j] = 0xFFFFFFFF;
                        DeleteFlag = 1;
                        break;
                    }
                }

                if(DeleteFlag == 0)
                {
                    Dele_Num = MinNum[Delete_i];
                }

#endif
                DeleteDataState = Dele_Dat;
            }
            else
            {
                DeleteDataState = Dele_End;
            }

            Delete_i++;
            break;

        case Dele_Dat:
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);

            if(DeleteFlag == 0)
            {
                sprintf(path, "%d:/Data/%08d.dat", DefaultDisk, Dele_Num);
                lf_unlink(DefaultDisk, path);
            }

#ifdef DTU3PRO
            else
            {
                sprintf(path, "%d:/Data/%08d.dat", ExpansionDisk, Dele_Num);
                lf_unlink(ExpansionDisk, path);
            }

#endif
            myfree(path);
            DeleteDataState = Dele_List;
            break;

        case Dele_List:
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);

            if(DeleteFlag == 0)
            {
                sprintf(path, "%d:/Data/%08d_list.dat", DefaultDisk, Dele_Num);
                lf_unlink(DefaultDisk, path);
            }

#ifdef DTU3PRO
            else
            {
                sprintf(path, "%d:/Data/%08d_list.dat", ExpansionDisk, Dele_Num);
                lf_unlink(ExpansionDisk, path);
            }

#endif
            myfree(path);
            DeleteDataState = Dele_DIR;
            break;

        case Dele_DIR:
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);

            if(DeleteFlag == 0)
            {
                sprintf(path, "%d:/Data/DIR_%08d_list.dat", DefaultDisk, Dele_Num);
                lf_unlink(DefaultDisk, path);
            }

#ifdef DTU3PRO
            else
            {
                sprintf(path, "%d:/Data/DIR_%08d_list.dat", ExpansionDisk, Dele_Num);
                lf_unlink(ExpansionDisk, path);
            }

#endif
            myfree(path);
            DeleteDataState = Dele_Alarm_List;
            break;

        case Dele_Alarm_List:
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);

            if(DeleteFlag == 0)
            {
                sprintf(path, "%d:/Data/Alarm_%08d_list", DefaultDisk, Dele_Num);
                lf_unlink(DefaultDisk, path);
            }

#ifdef DTU3PRO
            else
            {
                sprintf(path, "%d:/Data/Alarm_%08d_list", ExpansionDisk, Dele_Num);
                lf_unlink(ExpansionDisk, path);
            }

#endif
            myfree(path);
            DeleteDataState = Dele_Alarm;
            break;

        case Dele_Alarm:
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);

            if(DeleteFlag == 0)
            {
                sprintf(path, "%d:/Data/Alarm_%08d.dat", DefaultDisk, Dele_Num);
                lf_unlink(DefaultDisk, path);
            }

#ifdef DTU3PRO
            else
            {
                sprintf(path, "%d:/Data/Alarm_%08d.dat", ExpansionDisk, Dele_Num);
                lf_unlink(ExpansionDisk, path);
            }

#endif
            myfree(path);
            DeleteDataState = Dele_Compare;
            break;

        case Dele_End:
            break;
    }
}


void DeleteHistoryData(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", DefaultDisk);
    lf_DeleteAllFiles(DefaultDisk, path);
#ifdef DTU3PRO
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", ExpansionDisk);
    lf_DeleteAllFiles(ExpansionDisk, path);
#endif
    UploadedDate = 0;
    UploadedOffset = 0;
    InsideOffset = 0;
    WriteDate = 0;
    WriteOffset = 0;
    StartAddrLast = 0;
    StartAddrTimes = 0;
    DIR_WriteOffset = 0;
    DIR_ReadDate = 0;
    DIR_ReadOffset = 0;
    Alarm_WriteOffset = 0;
    Alarm_Real_ReadOffset = 0;
    Alarm_History_ReadDate = 0;
    Alarm_History_ReadOffset = 0;
    Memory_BKP_Save();
    myfree(path);
}
#if 0
/***********************************************
** Function name:       Alarm_List_Get_size
** Descriptions:        获取告警目录大小
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Alarm_List_Get_size(u8 Disk, u32 Date)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    BuffLen = Read_File_Length(Disk, path);
    myfree(path);
    return BuffLen;
}

/***********************************************
** Function name:       Alarm_List_Write
** Descriptions:        告警目录写
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Alarm_List_Write(u8 Disk, u32 Alarm_Date, u32 *Alarm_Offset, u32 StartAddr, u32 EndAddr, u32 Len, u8 Mark, u32 AbsoluteTime)
{
    char *path = NULL;
    char *pSaveBuffer = NULL;
    vs16 res;
    vu32 BuffLen = 0;
    vu32 length = 256;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Alarm_Date / 10000), (u8)((Alarm_Date % 10000) / 100), (Alarm_Date % 100));
    pSaveBuffer = mymalloc(length);
    memset(pSaveBuffer, 0, length);
    /*包头*/
    sprintf(pSaveBuffer, "ListStart:");
    BuffLen = BuffLen + LIST_START_LEN;
    /*起始地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", StartAddr);
    BuffLen = BuffLen + START_ADDR_LEN;
    /*结束地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", EndAddr);
    BuffLen = BuffLen + END_ADDR_LEN;
    /*4字节数据长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = BuffLen + DATA_LEN;

    /*数据包发送状态*/
    switch(Mark)
    {
        case 0:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Failure");
            break;

        case 1:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Success");
            break;

        case 2:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Between");
            break;
    }

    BuffLen = BuffLen + MARK_LEN + MARK_RES_LEN;
    /*绝对时间戳*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", AbsoluteTime);
    BuffLen = BuffLen + ABSOLUTE_TIME;
    /*预留*/
    memset(&pSaveBuffer[BuffLen], 0, RESERVED_SPACE);
    BuffLen = BuffLen + RESERVED_SPACE;
    /*包尾*/
    sprintf(&pSaveBuffer[BuffLen], "ListEnd");
    BuffLen  = BuffLen + LIST_END_LEN;

    if(BuffLen != ALARM_LIST_NEXT)
    {
        BuffLen = ALARM_LIST_NEXT;
    }

    File_Write(Disk, path, pSaveBuffer, *Alarm_Offset, BuffLen);
    DIR_Write(AlarmDt_Style, *Alarm_Offset, *Alarm_Offset + BuffLen, BuffLen, Mark, AbsoluteTime);
    *Alarm_Offset = *Alarm_Offset + BuffLen;
    myfree(path);
    myfree(pSaveBuffer);
    return BuffLen;
}
/***********************************************
** Function name:       Alarm_List_Read
** Descriptions:        告警目录读
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Alarm_List_Read(u8 Disk, u32 Alarm_Date, u32 Alarm_Offset, u32 *StartAddr, u32 *EndAddr, u32 *Len, u8 *Mark, u32 *AbsoluteTime)
{
    vs16 res;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 read_len = 0;
    vu32 i = 0;
    vu32 BuffLen = 0;
    vu32 length = 256;
    vu32 fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Alarm_Date / 10000), (u8)((Alarm_Date % 10000) / 100), (Alarm_Date % 100));
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    read_len = File_Read(Disk, path, pReadBuffer, Alarm_Offset, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "ListStart:", LIST_START_LEN) == 0)
        {
            i += LIST_START_LEN;
            /*起始地址*/
            *StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            *EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            *Len = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 2;
                break;
            }

            /*绝对时间戳*/
            *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
            i = i + ABSOLUTE_TIME;
            i = i + LIST_END_LEN;
            break;
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return read_len;
}
/***********************************************
** Function name:       Alarm_List_RealTime_Mark
** Descriptions:        告警实时数据标记
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_List_RealTime_Mark(u8 mark)
{
    static vu32 FailureOffset = 0;
    static vu8 FailureTimes = 0;
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu32 AbsoluteTime;
    vu32 fnum = 0;
    vs32 Offset = 0;
    vu32 i = 0;
    vu32 j = 0;
    vu32 length = 256;
    vu32 Read_len = 0;
    vu32 find = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    find = 0;

    for(;;)
    {
        Read_len = File_Read(Dtu3Detail.Property.StorageLocat, path, pReadBuffer, (Alarm_Real_ReadOffset - Offset), length);

        if(Read_len == 0)
        {
            if(Offset < Alarm_Real_ReadOffset)
            {
                Offset = Offset + ALARM_LIST_NEXT;
            }
            else
            {
                return;
            }
        }

        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                /*绝对时间戳*/
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 0;
                i = i + MARK_LEN + MARK_RES_LEN;
                /*绝对时间戳*/
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                Offset = Offset - ALARM_LIST_NEXT;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 0;
                //i = i + MARK_LEN + MARK_RES_LEN;
                /*绝对时间戳*/
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                // i = i + ABSOLUTE_TIME;
                Alarm_Real_ReadOffset = Alarm_Real_ReadOffset + ALARM_LIST_NEXT;
                Alarm_Real_ReadOffset = ((Alarm_Real_ReadOffset / ALARM_LIST_NEXT) + ((Alarm_Real_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
                return;
            }
        }

        if(find == 1)
        {
            break;
        }
    }

    if(find == 1)
    {
        if(strncmp(&pReadBuffer[j], data, MARK_LEN + MARK_RES_LEN) != 0)
        {
            //            lf_open(Dtu3Detail.Property.StorageLocat, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
            //            lf_seek(Dtu3Detail.Property.StorageLocat, &lfnew, AlarmOffset - Offset + i);
            //            lf_write(Dtu3Detail.Property.StorageLocat, &lfnew, data, strlen(data), &fnum);
            //            lf_close(Dtu3Detail.Property.StorageLocat, &lfnew);
            File_Write(Dtu3Detail.Property.StorageLocat, path, data, Alarm_Real_ReadOffset - Offset + j, strlen(data));
            DIR_Mark_Replace(Dtu3Detail.Property.StorageLocat, WriteDate, DIR_WriteOffset - (DIR_NEXT), AlarmDt_Style, (Alarm_Real_ReadOffset - Offset + j + MARK_LEN
                             + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN) - ALARM_LIST_NEXT, (Alarm_Real_ReadOffset - Offset + j + MARK_LEN +
                                     MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN), ALARM_LIST_NEXT, mark, AbsoluteTime);
            Alarm_Real_ReadOffset = Alarm_Real_ReadOffset + ALARM_LIST_NEXT;
            Alarm_Real_ReadOffset = ((Alarm_Real_ReadOffset / ALARM_LIST_NEXT) + ((Alarm_Real_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
        }

        i = i + MARK_LEN + MARK_RES_LEN;
    }

    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}
/***********************************************
** Function name:       Alarm_List_History_Mark
** Descriptions:        告警历史数据标记
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_List_History_Mark(u8 mark)
{
    static vu32 FailureOffset = 0;
    static vu8 FailureTimes = 0;
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu8 UseStorage = DefaultDisk;
    vu32 fnum = 0;
    vu32 Offset = 0;
    vu32 i = 0;
    vu32 j = 0;
    vu32 length = 256;
    vu32 Read_len = 0;
    vu32 find = 0;
    vu32 DirOff = 0;
    vu32 AbsoluteTime = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if(i == 1)
        {
            UseStorage = ExpansionDisk;
        }

        sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", UseStorage, (u16)(Alarm_History_ReadDate / 10000), (u8)((Alarm_History_ReadDate % 10000) / 100), (Alarm_History_ReadDate % 100));
        Read_len = File_Read(UseStorage, path, pReadBuffer, Alarm_History_ReadOffset, length);

        if(Read_len != 0)
        {
            break;
        }
    }

#else
    UseStorage = DefaultDisk;
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", UseStorage, (u16)(Alarm_History_ReadDate / 10000), (u8)((Alarm_History_ReadDate % 10000) / 100), (Alarm_History_ReadDate % 100));
    Read_len = File_Read(UseStorage, path, pReadBuffer, Alarm_History_ReadOffset, length);
#endif

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    if(Read_len != 0)
    {
        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                /*绝对时间戳*/
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                i = i + ABSOLUTE_TIME;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                Offset = Offset + ALARM_LIST_NEXT;
                memset(pReadBuffer, 0, (length));
                Read_len = File_Read(UseStorage, path, pReadBuffer, Alarm_History_ReadOffset + Offset, length);
                i = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
                DIR_Mark_Replace(UseStorage, Alarm_History_ReadDate, DIR_ReadOffset, AlarmDt_Style, (Alarm_History_ReadOffset + Offset + j + MARK_LEN
                                 + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN) - ALARM_LIST_NEXT, (Alarm_History_ReadOffset + Offset + j + MARK_LEN
                                         + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN), ALARM_LIST_NEXT, mark, AbsoluteTime);
                Alarm_History_ReadOffset = Alarm_History_ReadOffset + Offset + i;
                Alarm_History_ReadOffset = ((Alarm_History_ReadOffset / ALARM_LIST_NEXT) + ((Alarm_History_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        if(find == 1)
        {
            if(strncmp(&pReadBuffer[j], data, MARK_LEN + MARK_RES_LEN) != 0)
            {
                File_Write(UseStorage, path, data, Alarm_History_ReadOffset + Offset + j, strlen(data));
                DIR_Mark_Replace(UseStorage, Alarm_History_ReadDate, DIR_ReadOffset, AlarmDt_Style, (Alarm_History_ReadOffset + Offset + j + MARK_LEN
                                 + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN) - ALARM_LIST_NEXT, (Alarm_History_ReadOffset + Offset + j + MARK_LEN
                                         + MARK_RES_LEN + ABSOLUTE_TIME + RESERVED_SPACE + LIST_END_LEN), ALARM_LIST_NEXT, mark, AbsoluteTime);
            }

            i = i + MARK_LEN + MARK_RES_LEN;

            if(mark == 1)
            {
                Alarm_History_ReadOffset = Alarm_History_ReadOffset + InsideOffset + i;
                InsideOffset = 0;
                FailureOffset = 0;
                FailureTimes = 0;
            }
            else if(mark == 0)
            {
                if(Alarm_History_ReadOffset == FailureOffset)
                {
                    if(FailureTimes >= STORE_REREAD_TIMES)
                    {
                        Alarm_History_ReadOffset = Alarm_History_ReadOffset + InsideOffset + i;
                        InsideOffset = 0;
                        FailureOffset = 0;
                        FailureTimes = 0;
                    }
                    else
                    {
                        FailureTimes++;
                        InsideOffset = 0;
                    }
                }
                else
                {
                    FailureOffset = Alarm_History_ReadOffset;
                    FailureTimes++;
                }
            }
        }
    }

    Alarm_History_ReadOffset = ((Alarm_History_ReadOffset / ALARM_LIST_NEXT) + ((Alarm_History_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}

/***********************************************
** Function name:       AlarmData_Write
** Descriptions:        报警数据写入
** input parameters:    存储结构体AlarmDataType *AlarmDataBuf, 结构体数量u16 Alarm_num
** output parameters:   无
** Returned value:      成功长度
*************************************************/
u16 Alarm_Data_Write(AlarmDataType *AlarmDataBuf, u16 Alarm_num)
{
    char *path = NULL;
    char *buffer = NULL;
    char *pSaveBuffer = NULL;
    vu8 Mark = 0;
    vu8 LastMark = 0;
    vu16 i, j;
    vu16 crc = 0;
    vu16 length = 0;
    vu32 res = 0;
    vu32 fsize = 0;
    vu32 BuffLen = 0;
    vu32 LastStartAddr = 0;
    vu32 LastEndAddr = 0;
    vu32 LastLen = 0;
    vu32 DirOff = 0;
    vu32 AbsoluteTime = 0;
    vu32 AbsoluteTimeGet = 0;
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    AbsoluteTime = RTC_Getsecond();

    if(Alarm_num == 0)
    {
        return 0;
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    buffer = mymalloc((Alarm_num * sizeof(AlarmDataType)) + 4);
    memset(buffer, 0, (Alarm_num * sizeof(AlarmDataType)) + 4);
    length = Alarm_num * sizeof(AlarmDataType) + 4;
    pSaveBuffer = mymalloc((length + 512));
    memset(pSaveBuffer, 0, (length + 512));
    Memory_BKP_Read();
    ListDate_Check();

    if(Alarm_WriteOffset < Alarm_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate))
    {
        Alarm_WriteOffset = Alarm_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate);
        Alarm_WriteOffset = ((Alarm_WriteOffset / ALARM_LIST_NEXT) + ((Alarm_WriteOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
    }

    if(Alarm_WriteOffset >= ALARM_LIST_NEXT)
    {
        res = Alarm_List_Read(Dtu3Detail.Property.StorageLocat, WriteDate, Alarm_WriteOffset - ALARM_LIST_NEXT, (u32 *)&LastStartAddr, (u32 *)&LastEndAddr, (u32 *)&LastLen, (u8 *)&LastMark, (u32 *)&AbsoluteTimeGet);
    }

    if(res == 0)
    {
        LastStartAddr = 0;
        LastEndAddr = 0;
        LastLen = 0;
        LastMark = 0;
    }

    buffer[0] = (u8)Alarm_num;
    buffer[1] = (u8)(Alarm_num >> 8);

    for(i = 0; i < Alarm_num; i++)
    {
        for(j = 0; j < sizeof(AlarmDataType); j++)
        {
            buffer[(i * sizeof(AlarmDataType)) + 2 + j] = AlarmDataBuf[i].DataMsg[j];
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], i * sizeof(AlarmDataType));
    buffer[(i * sizeof(AlarmDataType)) + 2] = (u8)crc;
    buffer[(i * sizeof(AlarmDataType)) + 3] = (u8)(crc >> 8);
    BuffLen = 0;
    /*包头Start*/
    sprintf(pSaveBuffer, "Start:");
    BuffLen = BuffLen + START_LEN;
    /*xx:xx:xx时间*/
    sprintf(&pSaveBuffer[BuffLen], "%02d:%02d:%02d", calendar.hour, calendar.min, calendar.sec);
    BuffLen = BuffLen + HOUR_LEN + MIN_LEN + SEC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", length);
    BuffLen = BuffLen + DATA_LEN;
    /*数据*/
    memcpy(&pSaveBuffer[BuffLen], buffer, length);
    crc = (u16)MyCrc16((unsigned char *)&pSaveBuffer[BuffLen], length);
    BuffLen  = BuffLen + length;
    /*数据校验*/
    sprintf(&pSaveBuffer[BuffLen], "%04x", crc);
    BuffLen = BuffLen + CRC_LEN;
    /*包尾End*/
    sprintf(&pSaveBuffer[BuffLen], "End");
    BuffLen  = BuffLen + END_LEN;
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));
    RealTime_DirCheck(Dtu3Detail.Property.StorageLocat);
    Alarm_WriteOffset = ((Alarm_WriteOffset / ALARM_LIST_NEXT) + ((Alarm_WriteOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
    Memory_BKP_Save();
    File_Write_Que(Dtu3Detail.Property.StorageLocat, path, pSaveBuffer, LastEndAddr, BuffLen);
    Alarm_List_Write(Dtu3Detail.Property.StorageLocat, WriteDate, (u32 *)&Alarm_WriteOffset, LastEndAddr, LastEndAddr + BuffLen, BuffLen, Mark, AbsoluteTime);
    myfree(path);
    myfree(buffer);
    myfree(pSaveBuffer);
    return 0;
}

/***********************************************
** Function name:       Alarm_Data_Read
** Descriptions:        报警数据读取
** input parameters:    无
** output parameters:   *Data数据
** Returned value:      成功长度
*************************************************/
u16 Alarm_Data_Read(u32 *Alarm_ReadDate, u32 *Alarm_ReadOffset, u32 *LastAddr, u32 *LastAddrTimes, AlarmDataType *AlarmDataBuf, u32 *Alarm_time)
{
    char *path = NULL;
    char *buffer = NULL;
    char *pReadBuffer = NULL;
    vu32 Alarm_Real_Sec = 0;
    vu32 ScanningTimes = 0;
    vu32 read_len = 0;
    vu32 BetweenLen = 0;
    vu32 backlen = 0;
    vu32 length = 256;
    vs32 offset = 0;
    vu32 i = 0, j = 0;
    vu16 crc;
    vu16 crc_read;
    vu8 UseStorage = DefaultDisk;
    vu8 find = 0;
    vu8 hour = 0;
    vu8 min = 0;
    vu8 sec = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    vu32 GetStartAddr = 0;
    vu32 GetEndAddr = 0;
    vu32 GetLen = 0;
    vu8 GetMark = 0;
    vu8 Mark = 0;
    vu32 res = 0;
    vu8 Alarm_max = 20;
    vu16 Alarm_num = 0;
    vu32 DirOff = 0;
    vu32 AbsoluteTime = 0;
    vu32 AbsoluteTimeGet = 0;
    volatile calendar_obj calendar_get;
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    buffer = mymalloc((Alarm_max * sizeof(AlarmDataType)) + 4);
    memset(buffer, 0, (Alarm_max * sizeof(AlarmDataType)) + 4);
    ListDate_Check();
    *Alarm_time = 0;

    if(*Alarm_ReadDate == 0)
    {
        *Alarm_ReadDate = WriteDate;
        *Alarm_ReadOffset = 0;
    }
    else if(*Alarm_ReadDate > WriteDate)
    {
        *Alarm_ReadDate = WriteDate;
        *Alarm_ReadOffset = 0;
    }

    if(*Alarm_ReadDate == WriteDate)
    {
        if(*Alarm_ReadOffset > Alarm_WriteOffset)
        {
            if(Alarm_WriteOffset == 0)
            {
                *Alarm_ReadOffset = 0;
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                *Alarm_ReadOffset = Alarm_WriteOffset - ALARM_LIST_NEXT;
                *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
            }
        }
    }

    for(;;)
    {
#ifdef DTU3PRO

        for(i = 0; i < 2; i++)
        {
            if(i == 0)
            {
                UseStorage = DefaultDisk;
            }
            else if(i == 1)
            {
                UseStorage = ExpansionDisk;
            }

            res = Alarm_List_Read(UseStorage, *Alarm_ReadDate, 0, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

            if(res != 0)
            {
                break;
            }
        }

#endif
        res = Alarm_List_Read(UseStorage, *Alarm_ReadDate, *Alarm_ReadOffset, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(res == 0)
        {
            if(*Alarm_ReadDate < WriteDate)
            {
                Date_Add(Alarm_ReadDate);
                *Alarm_ReadOffset = 0;
                *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
            }
            else if(*Alarm_ReadDate == WriteDate)
            {
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                *Alarm_ReadDate = WriteDate;
                *Alarm_ReadOffset = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }
        }
        else
        {
            break;
        }
    }

    length = 256;
    find = 0;
    memset(pReadBuffer, 0, (length));
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", UseStorage, (u16)(*Alarm_ReadDate / 10000), (u8)((*Alarm_ReadDate % 10000) / 100), (*Alarm_ReadDate % 100));
    read_len = File_Read(UseStorage, path, pReadBuffer, GetStartAddr, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
        {
            i += START_LEN;
            /*获取时*/
            hour = (u8)astr2int(&pReadBuffer[i], HOUR_LEN);
            i += HOUR_LEN;
            /*获取分*/
            min = (u8)astr2int(&pReadBuffer[i], MIN_LEN);
            i += MIN_LEN;
            /*获取秒*/
            sec = (u8)astr2int(&pReadBuffer[i], SEC_LEN);
            i += SEC_LEN;
            /*获取长度*/
            length = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;
            myfree(pReadBuffer);
            pReadBuffer = mymalloc(length + CRC_LEN + END_LEN);
            memset(pReadBuffer, 0, (length + CRC_LEN + END_LEN));
            j = i;
            File_Read(UseStorage, path, pReadBuffer, GetStartAddr + i, length + CRC_LEN + END_LEN);
            crc = (u16)MyCrc16((unsigned char *)&pReadBuffer[i - j], length);
            crc_read = (u16)hstr2int(&pReadBuffer[i - j + length], CRC_LEN);

            if(*LastAddrTimes < 5)
            {
                if(GetStartAddr <= *LastAddr)
                {
                    *LastAddrTimes = *LastAddrTimes + 1;
                }
                else
                {
                    *LastAddrTimes = 0;
                    *LastAddr = GetStartAddr;
                }
            }
            else
            {
                *LastAddrTimes = 0;
                *Alarm_ReadOffset = *Alarm_ReadOffset + ALARM_LIST_NEXT;
                *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
            }

            if(crc == crc_read)
            {
                memcpy(buffer, &pReadBuffer[i - j], length);
                Alarm_num = (u16)buffer[0] + (u16)(buffer[1] << 8);

                if(Alarm_num <= Alarm_max)
                {
                    crc_read = (u16)buffer[Alarm_num * sizeof(AlarmDataType) + 2] + (u16)(buffer[Alarm_num * sizeof(AlarmDataType) + 3] << 8);
                    crc = (u16)MyCrc16((unsigned char *)&buffer[2], Alarm_num * sizeof(AlarmDataType));

                    if(crc == crc_read)
                    {
                        calendar_get.w_year = (u16)(*Alarm_ReadDate / 10000);
                        calendar_get.w_month = (u8)((*Alarm_ReadDate / 100) % 100);
                        calendar_get.w_date = (u8)(*Alarm_ReadDate % 100);
                        calendar_get.hour = hour;
                        calendar_get.min = min;
                        calendar_get.sec = sec;
                        *Alarm_time = DateToSec(calendar_get);

                        for(i = 0; i < Alarm_num; i++)
                        {
                            for(j = 0; j < sizeof(AlarmDataType); j++)
                            {
                                AlarmDataBuf[i].DataMsg[j] = buffer[(i * sizeof(AlarmDataType)) + 2 + j];
                            }
                        }
                    }
                }
                else
                {
                    Alarm_num = 0;
                }
            }
            else
            {
                *LastAddrTimes = 0;
                *LastAddr = GetStartAddr;
                *Alarm_ReadOffset = *Alarm_ReadOffset + ALARM_LIST_NEXT;
                *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }

            i += length;
            i += CRC_LEN;
            i += END_LEN;
            *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
            myfree(path);
            myfree(buffer);
            myfree(pReadBuffer);
            return Alarm_num;
        }
    }

    *Alarm_ReadOffset = ((*Alarm_ReadOffset / ALARM_LIST_NEXT) + ((*Alarm_ReadOffset % ALARM_LIST_NEXT) > 0 ? 1 : 0)) * ALARM_LIST_NEXT;
    myfree(path);
    myfree(buffer);
    myfree(pReadBuffer);
    return 0;
}
/***********************************************
** Function name:       Alarm_Real_Data_Update
** Descriptions:        实时数据更新标志
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Real_Data_Update(void)
{
    Alarm_Real_ReadOffset = Alarm_WriteOffset;
}
/***********************************************
** Function name:       Alarm_Real_Data_Read
** Descriptions:        实时报警数据读取
** input parameters:    无
** output parameters:   *Data数据
** Returned value:      成功长度
*************************************************/
u16 Alarm_Real_Data_Read(AlarmDataType *AlarmDataBuf, u32 *Alarm_time)
{
    vu16 Alarm_num = 0;
    static vu32 Alarm_RealTimes = 0;
    static vu32 Alarm_RealAddrLast = 0;
    Memory_BKP_Read();

    if(Alarm_WriteOffset > ALARM_LIST_NEXT)
    {
        Alarm_Real_ReadOffset = Alarm_WriteOffset - ALARM_LIST_NEXT;
    }
    else
    {
        Alarm_Real_ReadOffset = 0;
    }

    Alarm_num = Alarm_Data_Read((u32 *)&WriteDate, (u32 *)&Alarm_Real_ReadOffset, (u32 *)&Alarm_RealAddrLast, (u32 *)&Alarm_RealTimes, AlarmDataBuf, Alarm_time);
    Memory_BKP_Save();
    return Alarm_num;
}

/***********************************************
** Function name:       Alarm_History_Data_Read
** Descriptions:        历史报警数据读取
** input parameters:    无
** output parameters:   *Data数据
** Returned value:      成功长度
*************************************************/
u16 Alarm_History_Data_Read(AlarmDataType *AlarmDataBuf, u32 *Alarm_time)
{
    vu16 Alarm_num = 0;
    static vu32 Alarm_HistoryTimes = 0;
    static vu32 Alarm_HistoryAddrLast = 0;
    Memory_BKP_Read();
    Alarm_num = Alarm_Data_Read((u32 *)&Alarm_History_ReadDate, (u32 *)&Alarm_History_ReadOffset, (u32 *)&Alarm_HistoryAddrLast, (u32 *)&Alarm_HistoryTimes, AlarmDataBuf, Alarm_time);

    if((Alarm_HistoryTimes == 0) && (Alarm_HistoryAddrLast == 0))
    {
        DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
    }

    return Alarm_num;
}
#else
#if 0

/***********************************************
** Function name:       Alarm_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_DirCheck(u32 Alarm_Save_Date)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/AlarmData", DefaultDisk);
    Dir_Check(DefaultDisk, path);

    if(Alarm_Save_Date != 0)
    {
        memset(path, 0, (PathLen));
        sprintf(path, "%d:/AlarmData/%04d%02d%02d", DefaultDisk, (u16)(Alarm_Save_Date / 10000), (u8)((Alarm_Save_Date % 10000) / 100), (u8)(Alarm_Save_Date % 100));
        Dir_Check(DefaultDisk, path);
    }

    myfree(path);
}

/***********************************************
** Function name:       Alarm_Index_Write
** Descriptions:        报警索引写入
** input parameters:    WarnIndex *Warn_Index
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Index_Write(volatile WarnIndex *warn_index_p)
{
    vu16 crc = 0;
    char *path = NULL;
    char *pSaveBuf = NULL;
    vu32 Alarm_Index_Offset = 0;
    Alarm_DirCheck(0);
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/AlarmData/WarnIndex.dat", DefaultDisk);
    pSaveBuf = mymalloc(WarnIndexMax);
    memset(pSaveBuf, 0, WarnIndexMax);
    memcpy(pSaveBuf, (u8 *)warn_index_p->Data, sizeof(WarnIndex));
    crc = (u16)MyCrc16((u8 *)pSaveBuf, sizeof(WarnIndex));
    pSaveBuf[WarnIndexMax - 2] = (u8)crc;
    pSaveBuf[WarnIndexMax - 1] = (u8)(crc >> 8);
    Alarm_Index_Offset = Read_File_Length(DefaultDisk, path);
    //File_Write_Que(DefaultDisk, path, pSaveBuf, Alarm_Index_Offset, WarnIndexMax);
    File_Write(DefaultDisk, path, pSaveBuf, Alarm_Index_Offset, WarnIndexMax);
    myfree(path);
    myfree(pSaveBuf);
}
/***********************************************
** Function name:       Alarm_Index_Read
** Descriptions:        报警索引读取
** input parameters:    WarnIndex *Warn_Index
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Index_Read(volatile WarnIndex *warn_index_p)
{
    vu16 crc = 0;
    vu16 crc_read = 0;
    char *path = NULL;
    char *pReadBuf = NULL;
    vu32 Alarm_Index_Offset = 0;
    Alarm_DirCheck(0);
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/AlarmData/WarnIndex.dat", DefaultDisk);
    pReadBuf = mymalloc(WarnIndexMax);
    memset(pReadBuf, 0, WarnIndexMax);
    Alarm_Index_Offset = Read_File_Length(DefaultDisk, path);

    if(Alarm_Index_Offset > 0)
    {
        File_Read(DefaultDisk, path, pReadBuf, (Alarm_Index_Offset - WarnIndexMax), WarnIndexMax);
        crc_read = (u16)(pReadBuf[WarnIndexMax - 2]) + (u16)(pReadBuf[WarnIndexMax - 1] << 8);
        crc = (u16)MyCrc16((u8 *)pReadBuf, sizeof(WarnIndex));

        if(crc == crc_read)
        {
            memcpy((u8 *)warn_index_p->Data, pReadBuf, sizeof(WarnIndex));
        }
    }

    myfree(path);
    myfree(pReadBuf);
}

/***********************************************
** Function name:       Alarm_Data_Write
** Descriptions:        报警数据写入
** input parameters:    AlarmDataType *AlarmDataBuf, u8 Alarm_num
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Write(AlarmDataType *AlarmDataBuf, u8 Alarm_num)
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 Write_Num = 0;
    vu16 crc = 0;
    vu32 Alarm_Data_Offset = 0;
    vu32 Alarm_Write_Offset = 0;
    vu32 time = 0;
    char *path = NULL;
    char *pSaveBuf = NULL;
    volatile calendar_obj calendar;

    if(alarm_date == 0)
    {
        RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
        alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        Alarm_DirCheck(alarm_date);
    }

    path = mymalloc(PathLen);
    pSaveBuf = mymalloc(WarnDataMax * 20);
    memset(pSaveBuf, 0, WarnDataMax * 20);

    for(i = 0; i < Alarm_num; i++)
    {
        if((i == 0) || ((AlarmDataBuf[i].Data.Alarm_Id[0] != AlarmDataBuf[i + 1].Data.Alarm_Id[0]) &&
                        (AlarmDataBuf[i].Data.Alarm_Id[1] != AlarmDataBuf[i + 1].Data.Alarm_Id[1]) &&
                        (AlarmDataBuf[i].Data.Alarm_Id[2] != AlarmDataBuf[i + 1].Data.Alarm_Id[2]) &&
                        (AlarmDataBuf[i].Data.Alarm_Id[3] != AlarmDataBuf[i + 1].Data.Alarm_Id[3]) &&
                        (AlarmDataBuf[i].Data.Alarm_Id[4] != AlarmDataBuf[i + 1].Data.Alarm_Id[4]) &&
                        (AlarmDataBuf[i].Data.Alarm_Id[5] != AlarmDataBuf[i + 1].Data.Alarm_Id[5])))
        {
            memset(path, 0, (PathLen));
            sprintf(path, "%d:/AlarmData/%04d%02d%02d/%02x%02x%02x%02x%02x%02x.dat", DefaultDisk,
                    (u16)(alarm_date / 10000),
                    (u8)((alarm_date % 10000) / 100),
                    (u8)(alarm_date % 100),
                    AlarmDataBuf[i].Data.Alarm_Id[0],
                    AlarmDataBuf[i].Data.Alarm_Id[1],
                    AlarmDataBuf[i].Data.Alarm_Id[2],
                    AlarmDataBuf[i].Data.Alarm_Id[3],
                    AlarmDataBuf[i].Data.Alarm_Id[4],
                    AlarmDataBuf[i].Data.Alarm_Id[5]);
            Write_Num = 0;
            Alarm_Write_Offset = 0;
            Alarm_Data_Offset = 0;
            Alarm_Data_Offset = Read_File_Length(DefaultDisk, path);
            memset(pSaveBuf, 0, WarnDataMax * 20);
        }

        //        for(j = 0; j < sizeof(AlarmDataType); j++)
        //        {
        //            pSaveBuf[j] = AlarmDataBuf[i].DataMsg[j];
        //        }
        memcpy((char *)&pSaveBuf[Alarm_Write_Offset], (u8 *)AlarmDataBuf[i].DataMsg, sizeof(AlarmDataType));
        crc = (u16)MyCrc16((u8 *)&pSaveBuf[Alarm_Write_Offset], sizeof(AlarmDataType));
        pSaveBuf[Alarm_Write_Offset + WarnDataMax - 2] = (u8)crc;
        pSaveBuf[Alarm_Write_Offset + WarnDataMax - 1] = (u8)(crc >> 8);
        Alarm_Write_Offset =  Alarm_Write_Offset + WarnDataMax;
        Write_Num ++;

        if((i == (Alarm_num - 1)) || ((AlarmDataBuf[i].Data.Alarm_Id[0] != AlarmDataBuf[i + 1].Data.Alarm_Id[0]) &&
                                      (AlarmDataBuf[i].Data.Alarm_Id[1] != AlarmDataBuf[i + 1].Data.Alarm_Id[1]) &&
                                      (AlarmDataBuf[i].Data.Alarm_Id[2] != AlarmDataBuf[i + 1].Data.Alarm_Id[2]) &&
                                      (AlarmDataBuf[i].Data.Alarm_Id[3] != AlarmDataBuf[i + 1].Data.Alarm_Id[3]) &&
                                      (AlarmDataBuf[i].Data.Alarm_Id[4] != AlarmDataBuf[i + 1].Data.Alarm_Id[4]) &&
                                      (AlarmDataBuf[i].Data.Alarm_Id[5] != AlarmDataBuf[i + 1].Data.Alarm_Id[5])))
        {
            File_Write_Que(DefaultDisk, path, pSaveBuf, Alarm_Data_Offset, WarnDataMax * Write_Num);
        }

        //File_Write(DefaultDisk, path, pSaveBuf, Alarm_Data_Offset, WarnDataMax);
    }

    myfree(path);
    myfree(pSaveBuf);
}
/***********************************************
** Function name:       Alarm_Data_Read
** Descriptions:        实时报警数据读取
** input parameters:    AlarmDataType *AlarmDataBuf
** output parameters:   无
** Returned value:      无
*************************************************/
u8 Alarm_Data_Read(AlarmDataType *AlarmDataBuf)
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 k = 0;
    vu8 read_num = 0;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu32 alarm_date_last = 0;
    vu32 alarm_read_offset = 0;
    vu32 alarm_read_max = 0;
    char *path = NULL;
    char *pReadBuf = NULL;
    volatile calendar_obj calendar;
    path = mymalloc(PathLen);
    pReadBuf = mymalloc(WarnDataMax);

    if(alarm_date == 0)
    {
        RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
        alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        Alarm_DirCheck(alarm_date);
    }

    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);

    if(warnindex_p == NULL)
    {
        warnindex_p = mymalloc(sizeof(WarnIndex));
        memset((u8 *)warnindex_p->Data, 0, sizeof(WarnIndex));
    }

    if((Warn_Index.Property.Warn_Read_Data != alarm_date) && (Warn_Index.Property.Warn_Read_Data != alarm_date_last))
    {
        Alarm_Index_Read(&Warn_Index);
    }

    if((Warn_Index.Property.Warn_Read_Data != alarm_date) && (Warn_Index.Property.Warn_Read_Data != alarm_date_last))
    {
        Warn_Index.Property.Warn_Read_Data = alarm_date;
        Date_Minus((u32 *)&Warn_Index.Property.Warn_Read_Data);
        Warn_Index.Property.Warn_Read_Place = 0;

        for(i = 0; i < 100; i++)
        {
            Warn_Index.Property.Warn_Read_Num[i] = 0;
        }
    }

    memcpy((u8 *)warnindex_p->Data, (u8 *)Warn_Index.Data, sizeof(WarnIndex));
    i = 0;
    j = 0;

    for(; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(PORT_NUMBER_CONFIRMATION(i))
        {
            if((warnindex_p->Property.Warn_Read_Place) == j)
            {
                break;
            }

            j++;
        }
    }

    for(read_num = 0; read_num < 20;)
    {
        //        printf("%02x", MIMajor[i].Property.Pre_Id[0]);
        //        printf("%02x", MIMajor[i].Property.Pre_Id[1]);
        //        printf("%02x", MIMajor[i].Property.Id[0]);
        //        printf("%02x", MIMajor[i].Property.Id[1]);
        //        printf("%02x", MIMajor[i].Property.Id[2]);
        //        printf("%02x\n", MIMajor[i].Property.Id[3]);
        memset(path, 0, (PathLen));
        sprintf(path, "%d:/AlarmData/%04d%02d%02d/%02x%02x%02x%02x%02x%02x.dat", DefaultDisk,
                (u16)(warnindex_p->Property.Warn_Read_Data / 10000),
                (u8)((warnindex_p->Property.Warn_Read_Data % 10000) / 100),
                (u8)(warnindex_p->Property.Warn_Read_Data % 100),
                MIMajor[i].Property.Pre_Id[0],
                MIMajor[i].Property.Pre_Id[1],
                MIMajor[i].Property.Id[0],
                MIMajor[i].Property.Id[1],
                MIMajor[i].Property.Id[2],
                MIMajor[i].Property.Id[3]);
        alarm_read_offset = warnindex_p->Property.Warn_Read_Num[j];
        alarm_read_max = Read_File_Length(DefaultDisk, path);

        while(alarm_read_max > alarm_read_offset)
        {
            memset(pReadBuf, 0, WarnDataMax);
            File_Read(DefaultDisk, path, pReadBuf, alarm_read_offset, WarnDataMax);
            alarm_read_offset = alarm_read_offset + WarnDataMax;
            warnindex_p->Property.Warn_Read_Place = j;
            warnindex_p->Property.Warn_Read_Num[j] = alarm_read_offset;
            crc_read = (u16)(pReadBuf[WarnDataMax - 2]) + (u16)(pReadBuf[WarnDataMax - 1] << 8);
            crc = (u16)MyCrc16((u8 *)pReadBuf, sizeof(AlarmDataType));

            if(crc == crc_read)
            {
                memcpy((u8 *)AlarmDataBuf[read_num].DataMsg, pReadBuf, sizeof(AlarmDataType));
                read_num ++;
            }

            if(read_num >= 20)
            {
                break;
            }
        }

        i++;

        for(; i < Dtu3Detail.Property.PortNum; i++)
        {
            if(PORT_NUMBER_CONFIRMATION(i))
            {
                j++;
                break;
            }
        }

        if(i >= Dtu3Detail.Property.PortNum)
        {
            i = 0;
            j = 0;
            k++;

            if(k >= 2)
            {
                if(warnindex_p->Property.Warn_Read_Data == alarm_date)
                {
                    break;
                }
                else
                {
                    k = 0;
                    Date_Add((u32 *)&warnindex_p->Property.Warn_Read_Data);
                    warnindex_p->Property.Warn_Read_Place = 0;

                    for(i = 0; i < 100; i++)
                    {
                        warnindex_p->Property.Warn_Read_Num[i] = 0;
                    }

                    i = 0;
                }
            }
        }
    }

    myfree(path);
    myfree(pReadBuf);
    return read_num;
}
/***********************************************
** Function name:       Alarm_Data_Success
** Descriptions:        报警数据更新标记
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Success(void)
{
    memcpy((u8 *)Warn_Index.Data, (u8 *)warnindex_p->Data, sizeof(WarnIndex));
    Alarm_Index_Write(&Warn_Index);
    myfree((WarnIndex *)warnindex_p);
    warnindex_p = NULL;
}
/***********************************************
** Function name:       Alarm_Data_Num
** Descriptions:        读取未发送报警数
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 Alarm_Data_Num(void)
{
    vu8 i = 0;
    vu16 num = 0;
    vu32 alarm_date_last = 0;
    vu32 alarm_read_max = 0;
    vu32 size = 0;
    vu32 size_all = 0;
    vu32 size_success = 0;
    char *path = NULL;
    volatile calendar_obj calendar;
    path = mymalloc(PathLen);

    if(alarm_date == 0)
    {
        RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
        alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        Alarm_DirCheck(alarm_date);
    }

    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);

    if((Warn_Index.Property.Warn_Read_Data != alarm_date) && (Warn_Index.Property.Warn_Read_Data != alarm_date_last))
    {
        Alarm_Index_Read(&Warn_Index);
    }

    if((Warn_Index.Property.Warn_Read_Data != alarm_date) && (Warn_Index.Property.Warn_Read_Data != alarm_date_last))
    {
        Warn_Index.Property.Warn_Read_Data = alarm_date;
        Date_Minus((u32 *)&Warn_Index.Property.Warn_Read_Data);
        Warn_Index.Property.Warn_Read_Place = 0;

        for(i = 0; i < 100; i++)
        {
            Warn_Index.Property.Warn_Read_Num[i] = 0;
        }
    }

    for(i = 0; i < 100; i++)
    {
        size_success = size_success + Warn_Index.Property.Warn_Read_Num[i];
    }

    memset(path, 0, (PathLen));
    sprintf(path, "%d:/AlarmData/%04d%02d%02d/", DefaultDisk,
            (u16)(alarm_date / 10000),
            (u8)((alarm_date % 10000) / 100),
            (u8)(alarm_date % 100));
    lf_DirectorySize(DefaultDisk, path, (u32 *)&size, (u16 *)&num);
    size_all = size;

    if(Warn_Index.Property.Warn_Read_Data != alarm_date)
    {
        memset(path, 0, (PathLen));
        sprintf(path, "%d:/AlarmData/%04d%02d%02d/", DefaultDisk,
                (u16)(alarm_date_last / 10000),
                (u8)((alarm_date_last % 10000) / 100),
                (u8)(alarm_date_last % 100));
        lf_DirectorySize(DefaultDisk, path, (u32 *)&size, (u16 *)&num);
        size_all = size_all + size;
    }

    //printf("Size:%d Num:%d\n", (size_all - size_success), num);
    myfree(path);
    return (size_all - size_success);
}
/***********************************************
** Function name:       Alarm_Data_Delete
** Descriptions:        清空实时报警数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Delete(void)
{
    vu16 i = 0;
    vu16 Num = 0;
    vu32 DirName[20] = {0};
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    //告警数据存储文件夹
    sprintf(path, "%d:/AlarmData/", DefaultDisk);
    lf_ListDirs(DefaultDisk, path, (u32 *)DirName, (u16 *)&Num);

    for(i = 0; i < Num; i++)
    {
        memset(path, 0, PathLen);
        //告警数据存储文件夹
        sprintf(path, "%d:/AlarmData/%d", DefaultDisk, DirName[i]);
        lf_DeleteAllFiles(DefaultDisk, path);
        lf_unlink(DefaultDisk, path);
    }

    myfree(path);
}
/***********************************************
** Function name:       Alarm_Data_Expired_Delete
** Descriptions:        删除过期报警数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Expired_Delete(void)
{
    vu16 i = 0;
    vu16 Num = 0;
    vu32 DirName[20] = {0};
    vu32 alarm_date_last = 0;
    char *path = NULL;
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    //告警数据存储文件夹
    sprintf(path, "%d:/AlarmData/", DefaultDisk);
    lf_ListDirs(DefaultDisk, path, (u32 *)DirName, (u16 *)&Num);

    for(i = 0; i < Num; i++)
    {
        if((DirName[i] != alarm_date) && (DirName[i] != alarm_date_last))
        {
            memset(path, 0, PathLen);
            //告警数据存储文件夹
            sprintf(path, "%d:/AlarmData/%d", DefaultDisk, DirName[i]);
            lf_DeleteAllFiles(DefaultDisk, path);
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
#else
/***********************************************
** Function name:       Alarm_List_Get_size
** Descriptions:        获取告警目录大小
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Alarm_List_Get_size(u8 Disk, u32 Date)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Date / 10000), (u8)((Date % 10000) / 100), (Date % 100));
    BuffLen = Read_File_Length(Disk, path);
    myfree(path);
    return BuffLen;
}

/***********************************************
** Function name:       Alarm_List_Write
** Descriptions:        告警目录写
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 Alarm_List_Write(u8 Disk, u32 Alarm_Date, u8 PackNum, u8 PackAll, u32 StartAddr, u32 EndAddr, u32 Len, u8 Mark, u32 AbsoluteTime)
{
    char *path = NULL;
    char *pSaveBuffer = NULL;
    vs16 res;
    vu32 BuffLen = 0;
    vu32 length = 256;
    vu32 Alarm_Offset = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Alarm_Date / 10000), (u8)((Alarm_Date % 10000) / 100), (Alarm_Date % 100));
    pSaveBuffer = mymalloc(length);
    memset(pSaveBuffer, 0, length);
    /*包头*/
    sprintf(pSaveBuffer, "ListStart:");
    BuffLen = BuffLen + LIST_START_LEN;
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = BuffLen + PACK_NO_LEN + PACK_ALL_LEN;
    /*起始地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", StartAddr);
    BuffLen = BuffLen + START_ADDR_LEN;
    /*结束地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", EndAddr);
    BuffLen = BuffLen + END_ADDR_LEN;
    /*4字节数据长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = BuffLen + DATA_LEN;

    /*数据包发送状态*/
    switch(Mark)
    {
        case 0:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Failure");
            break;

        case 1:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Success");
            break;

        case 2:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Between");
            break;
    }

    BuffLen = BuffLen + MARK_LEN + MARK_RES_LEN;
    /*绝对时间戳*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", AbsoluteTime);
    BuffLen = BuffLen + ABSOLUTE_TIME;
    //    /*预留*/
    //    memset(&pSaveBuffer[BuffLen], 0, RESERVED_SPACE);
    //    BuffLen = BuffLen + RESERVED_SPACE;
    /*包尾*/
    sprintf(&pSaveBuffer[BuffLen], "ListEnd");
    BuffLen  = BuffLen + LIST_END_LEN;

    if(BuffLen != ALARM_LIST_NEXT)
    {
        BuffLen = ALARM_LIST_NEXT;
    }

    Alarm_Offset = Read_File_Length(Disk, path);
    File_Write(Disk, path, pSaveBuffer, Alarm_Offset, BuffLen);
    //DIR_Write(AlarmDt_Style, *Alarm_Offset, *Alarm_Offset + BuffLen, BuffLen, Mark, AbsoluteTime);
    myfree(path);
    myfree(pSaveBuffer);
    return BuffLen;
}
/***********************************************
** Function name:       Alarm_List_Read
** Descriptions:        告警目录读
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
vu16 Alarm_List_Read(u8 Disk, u32 Alarm_Date, u32 Alarm_Offset, u8 *PackNum, u8 *PackAll, u32 *StartAddr, u32 *EndAddr, u32 *Len, u8 *Mark, u32 *AbsoluteTime)
{
    vs16 res;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu16 read_len = 0;
    vu32 i = 0;
    vu32 length = 512;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", Disk, (u16)(Alarm_Date / 10000), (u8)((Alarm_Date % 10000) / 100), (Alarm_Date % 100));
    memset(pReadBuffer, 0, length);
    read_len = File_Read(Disk, path, pReadBuffer, Alarm_Offset, length);

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "ListStart:", LIST_START_LEN) == 0)
        {
            i += LIST_START_LEN;
            /*获取第几包*/
            *PackNum = (u8)astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = (u8)astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*起始地址*/
            *StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            *EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            *Len = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 2;
                break;
            }

            /*绝对时间戳*/
            *AbsoluteTime = astr2int(&pReadBuffer[i], ABSOLUTE_TIME);
            i = i + ABSOLUTE_TIME;
            i = i + LIST_END_LEN;
            break;
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return read_len;
}
/***********************************************
** Function name:       AlarmData_Write
** Descriptions:        报警数据写入
** input parameters:    存储结构体AlarmDataType *AlarmDataBuf, 结构体数量u16 Alarm_num
** output parameters:   无
** Returned value:      成功长度
*************************************************/
vu16 Alarm_Data_Write(u8 PackNum, u8 PackAll, AlarmDataType *AlarmDataBuf, u16 Alarm_num)
{
    char *path = NULL;
    char *buffer = NULL;
    char *pSaveBuffer = NULL;
    vu8 Mark = 0;
    vu8 LastMark = 0;
    vu8 PackNumRead = 0;
    vu8 PackAllRead = 0;
    vu16 i = 0;
    vu16 j = 0;
    vu16 crc = 0;
    vu16 length = 0;
    vu32 res = 0;
    vu32 BuffLen = 0;
    vu32 Alarm_Read_Offset = 0;
    vu32 LastStartAddr = 0;
    vu32 LastEndAddr = 0;
    vu32 LastLen = 0;
    vu32 AbsoluteTime = 0;
    vu32 AbsoluteTimeGet = 0;
    vu32 time = 0;
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    AbsoluteTime = RTC_GetWorldSecond(Dtu3Detail.Property.timezone);

    if(Alarm_num == 0)
    {
        return 0;
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    buffer = mymalloc((Alarm_num * sizeof(AlarmDataType)) + 4);
    memset(buffer, 0, (Alarm_num * sizeof(AlarmDataType)) + 4);
    length = Alarm_num * sizeof(AlarmDataType) + 4;
    pSaveBuffer = mymalloc((length + 512));
    memset(pSaveBuffer, 0, (length + 512));
    Memory_BKP_Read();
    ListDate_Check();

    if(Alarm_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate) > ALARM_LIST_NEXT)
    {
        Alarm_Read_Offset = Alarm_List_Get_size(Dtu3Detail.Property.StorageLocat, WriteDate) - ALARM_LIST_NEXT;
    }
    else
    {
        Alarm_Read_Offset = 0;
    }

    res = Alarm_List_Read(Dtu3Detail.Property.StorageLocat, WriteDate, Alarm_Read_Offset, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&LastStartAddr, (u32 *)&LastEndAddr, (u32 *)&LastLen, (u8 *)&LastMark, (u32 *)&AbsoluteTimeGet);

    if(res == 0)
    {
        LastStartAddr = 0;
        LastEndAddr = 0;
        LastLen = 0;
        LastMark = 0;
    }

    if((PackNum + 1) == PackAll)
    {
        Mark = 2;
    }
    else
    {
        Mark = 0;
    }

    buffer[0] = (u8)Alarm_num;
    buffer[1] = (u8)(Alarm_num >> 8);

    for(i = 0; i < Alarm_num; i++)
    {
        for(j = 0; j < sizeof(AlarmDataType); j++)
        {
            buffer[(i * sizeof(AlarmDataType)) + 2 + j] = AlarmDataBuf[i].DataMsg[j];
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], Alarm_num * sizeof(AlarmDataType));
    length = Alarm_num * sizeof(AlarmDataType) + 4;
    buffer[(i * sizeof(AlarmDataType)) + 2] = (u8)crc;
    buffer[(i * sizeof(AlarmDataType)) + 3] = (u8)(crc >> 8);
    BuffLen = 0;
    /*包头Start*/
    sprintf(pSaveBuffer, "Start:");
    BuffLen = BuffLen + START_LEN;
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = BuffLen + PACK_NO_LEN + PACK_ALL_LEN;
    /*xx:xx:xx时间*/
    sprintf(&pSaveBuffer[BuffLen], "%02d:%02d:%02d", calendar.hour, calendar.min, calendar.sec);
    BuffLen = BuffLen + HOUR_LEN + MIN_LEN + SEC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", length);
    BuffLen = BuffLen + DATA_LEN;
    /*数据*/
    memcpy(&pSaveBuffer[BuffLen], buffer, length);
    crc = (u16)MyCrc16((unsigned char *)&pSaveBuffer[BuffLen], length);
    BuffLen  = BuffLen + length;
    /*数据校验*/
    sprintf(&pSaveBuffer[BuffLen], "%04x", crc);
    BuffLen = BuffLen + CRC_LEN;
    /*包尾End*/
    sprintf(&pSaveBuffer[BuffLen], "End");
    BuffLen  = BuffLen + END_LEN;
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", Dtu3Detail.Property.StorageLocat, (u16)(WriteDate / 10000), (u8)((WriteDate % 10000) / 100), (WriteDate % 100));
    File_Write_Que(Dtu3Detail.Property.StorageLocat, path, pSaveBuffer, LastEndAddr, BuffLen);
    Alarm_List_Write(Dtu3Detail.Property.StorageLocat, WriteDate, PackNum, PackAll, LastEndAddr, LastEndAddr + BuffLen, BuffLen, Mark, AbsoluteTime);
    myfree(path);
    myfree(buffer);
    myfree(pSaveBuffer);
    return 0;
}
/***********************************************
** Function name:       Alarm_Data_Read
** Descriptions:        报警数据读取
** input parameters:    无
** output parameters:   *Data数据
** Returned value:      成功长度
*************************************************/
vu16 Alarm_Data_Read(u8 PackNum, u8 *PackAll, AlarmDataType *AlarmDataBuf, u32 *Alarm_time)
{
    char *path = NULL;
    char *buffer = NULL;
    char *pReadBuffer = NULL;
    vu8 Alarm_max = 20;
    vu8 alarm_times = 0;
    vu16 Alarm_num = 0;
    vu32 read_len = 0;
    vu32 backlen = 0;
    vu32 length = (Alarm_max * sizeof(AlarmDataType)) + 4 + 512;
    vu32 i = 0, j = 0;
    vu16 crc;
    vu16 crc_read;
    vu8 UseStorage = DefaultDisk;
    vu8 find = 0;
    vu8 hour = 0;
    vu8 min = 0;
    vu8 sec = 0;
    vu32 GetStartAddr = 0;
    vu32 GetEndAddr = 0;
    vu32 GetLen = 0;
    vu8 GetMark = 0;
    vu8 LastMark = 0;
    vu8 Mark = 0;
    vu8 PackNumRead = 0;
    vu8 PackAllRead = 0;
    vu32 res = 0;
    vu32 AbsoluteTime = 0;
    vu32 AbsoluteTimeGet = 0;
    vu32 alarm_date = 0;
    vu32 alarm_date_last = 0;
    static vu32 alarm_read_date_last = 0;
    static vu32 alarm_read_offset_last = 0;
    static vu8 alarm_read_times = 0;
    volatile calendar_obj calendar;
    volatile calendar_obj calendar_get;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);
    ListDate_Check();
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    Memory_BKP_Read();

    if(PackNum == 0)
    {
        alarm_read_date_snap = Alarm_Real_ReadDate;
        alarm_read_offset_snap = Alarm_Real_ReadOffset;
    }

    if((alarm_read_date_snap != alarm_date) && (alarm_read_date_snap != alarm_date_last))
    {
        alarm_read_date_snap = alarm_date_last;
        alarm_read_offset_snap = 0;
    }

#ifdef DTU3PRO

    for(i = 0; i < 4; i++)
    {
        if((i % 2) == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if((i % 2) == 1)
        {
            UseStorage = ExpansionDisk;
        }

        res = Alarm_List_Read(UseStorage, alarm_read_date_snap, 0, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(res != 0)
        {
            break;
        }

        if((i >= 1) && (alarm_read_date_snap == alarm_date_last))
        {
            alarm_read_date_snap = alarm_date;
        }
    }

#else

    for(i = 0; i < 2; i++)
    {
        res = Alarm_List_Read(UseStorage, alarm_read_date_snap, 0, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(res != 0)
        {
            break;
        }

        if(alarm_read_date_snap == alarm_date_last)
        {
            alarm_read_date_snap = alarm_date;
        }
    }

#endif
    alarm_times = 0;

    for(;;)
    {
        res = Alarm_List_Read(UseStorage, alarm_read_date_snap, alarm_read_offset_snap, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(PackNumRead == PackNum)
        {
            break;
        }
        else
        {
            if(res == 0)
            {
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                alarm_read_offset_snap = alarm_read_offset_snap + ALARM_LIST_NEXT;

                if(alarm_times > PackAllRead)
                {
                    myfree(path);
                    myfree(buffer);
                    myfree(pReadBuffer);
                    return 0;
                }

                alarm_times++;
            }
        }
    }

    if(PackNum == 0)
    {
        if((alarm_read_date_snap == alarm_read_date_last) && (alarm_read_offset_snap == alarm_read_offset_last))
        {
            alarm_read_times++;

            if(alarm_read_times >= STORE_REREAD_TIMES)
            {
                alarm_read_offset_snap = alarm_read_offset_snap + ALARM_LIST_NEXT;
                Alarm_Real_ReadOffset = alarm_read_offset_snap;
                Memory_BKP_Save();
                alarm_read_times = 0;
            }
        }
        else
        {
            alarm_read_times = 0;
            alarm_read_date_last = alarm_read_date_snap;
            alarm_read_offset_last = alarm_read_offset_snap;
        }
    }

    memset(pReadBuffer, 0, (length));
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", UseStorage, (u16)(alarm_read_date_snap / 10000), (u8)((alarm_read_date_snap % 10000) / 100), (alarm_read_date_snap % 100));
    read_len = File_Read(UseStorage, path, pReadBuffer, GetStartAddr, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(buffer);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
        {
            i += START_LEN;
            /*获取第几包*/
            PackNumRead = (u8)astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = (u8)astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*获取时*/
            hour = (u8)astr2int(&pReadBuffer[i], HOUR_LEN);
            i += HOUR_LEN;
            /*获取分*/
            min = (u8)astr2int(&pReadBuffer[i], MIN_LEN);
            i += MIN_LEN;
            /*获取秒*/
            sec = (u8)astr2int(&pReadBuffer[i], SEC_LEN);
            i += SEC_LEN;
            /*获取长度*/
            length = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;
            myfree(pReadBuffer);
            pReadBuffer = mymalloc(length + CRC_LEN + END_LEN);
            memset(pReadBuffer, 0, (length + CRC_LEN + END_LEN));
            j = i;
            File_Read(UseStorage, path, pReadBuffer, GetStartAddr + i, length + CRC_LEN + END_LEN);
            crc = (u16)MyCrc16((unsigned char *)&pReadBuffer[i - j], length);
            crc_read = (u16)hstr2int(&pReadBuffer[i - j + length], CRC_LEN);

            if(crc == crc_read)
            {
                buffer = mymalloc(length);
                memset(buffer, 0, length);
                memcpy(buffer, &pReadBuffer[i - j], length);
                Alarm_num = (u16)buffer[0] + (u16)(buffer[1] << 8);

                if(Alarm_num <= Alarm_max)
                {
                    crc_read = (u16)buffer[Alarm_num * sizeof(AlarmDataType) + 2] + (u16)(buffer[Alarm_num * sizeof(AlarmDataType) + 3] << 8);
                    crc = (u16)MyCrc16((unsigned char *)&buffer[2], Alarm_num * sizeof(AlarmDataType));

                    if(crc == crc_read)
                    {
                        calendar_get.w_year = (u16)(alarm_read_date_snap / 10000);
                        calendar_get.w_month = (u8)((alarm_read_date_snap / 100) % 100);
                        calendar_get.w_date = (u8)(alarm_read_date_snap % 100);
                        calendar_get.hour = hour;
                        calendar_get.min = min;
                        calendar_get.sec = sec;
                        *Alarm_time = DateToSec(calendar_get);

                        for(i = 0; i < Alarm_num; i++)
                        {
                            for(j = 0; j < sizeof(AlarmDataType); j++)
                            {
                                AlarmDataBuf[i].DataMsg[j] = buffer[(i * sizeof(AlarmDataType)) + 2 + j];
                            }
                        }

                        alarm_read_offset_snap = alarm_read_offset_snap + ALARM_LIST_NEXT;
                    }
                }
                else
                {
                    Alarm_num = 0;
                }
            }
            else
            {
                myfree(path);
                myfree(buffer);
                myfree(pReadBuffer);
                return 0;
            }

            i += length;
            i += CRC_LEN;
            i += END_LEN;
            myfree(path);
            myfree(buffer);
            myfree(pReadBuffer);
            return Alarm_num;
        }
    }

    myfree(path);
    myfree(buffer);
    myfree(pReadBuffer);
    return 0;
}
/***********************************************
** Function name:       Alarm_Data_Success
** Descriptions:        报警数据成功标记
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Success(void)
{
    char *data = NULL;
    char *path = NULL;
    char *pReadBuffer = NULL;
    vu8 Mark = 0;
    vu8 UseStorage = DefaultDisk;
    vu8 error_times = 0;
    vu16 i = 0;
    vu16 j = 0;
    vu16 res = 0;
    vu32 StartAddr;
    vu32 EndAddr;
    vu32 DataLen;
    vu32 Read_len = 0;
    vu32 find = 0;
    vu32 read_len = 0;
    vu32 length = 2048;
    vu32 AbsoluteTimeGet = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    Memory_BKP_Read();
#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if((i % 2) == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if((i % 2) == 1)
        {
            UseStorage = ExpansionDisk;
        }

        memset(path, 0, PathLen);
        sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", UseStorage, (u16)(alarm_read_date_snap / 10000), (u8)((alarm_read_date_snap % 10000) / 100), (alarm_read_date_snap % 100));
        res = File_Read(UseStorage, path, pReadBuffer, 0, length);

        if(res != 0)
        {
            break;
        }
    }

#endif
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", UseStorage, (u16)(alarm_read_date_snap / 10000), (u8)((alarm_read_date_snap % 10000) / 100), (alarm_read_date_snap % 100));
    memset(pReadBuffer, 0, length);

    if(alarm_read_offset_snap > ALARM_LIST_NEXT)
    {
        read_len = File_Read(UseStorage, path, pReadBuffer, (alarm_read_offset_snap - ALARM_LIST_NEXT), length);
    }
    else
    {
        read_len = File_Read(UseStorage, path, pReadBuffer, 0, length);
    }

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "ListStart:", LIST_START_LEN) == 0)
        {
            i += LIST_START_LEN;
            /*获取第几包*/
            //*PackNum = (u8)astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            //*PackAll = (u8)astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*起始地址*/
            //*StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            //*EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            //*Len = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                j = i;
                i = i + MARK_LEN + MARK_RES_LEN;
                Mark = 0;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 0;
                i = i + MARK_LEN + MARK_RES_LEN;
                Mark = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 0;
                Mark = 2;
            }
        }
    }

    if(find == 1)
    {
        if(strncmp(&pReadBuffer[j], "Mark:Success", MARK_LEN + MARK_RES_LEN) != 0)
        {
            File_Write(UseStorage, path, "Mark:Success", (alarm_read_offset_snap - ALARM_LIST_NEXT) + j, MARK_LEN + MARK_RES_LEN);
        }
    }

    Alarm_Real_ReadDate = alarm_read_date_snap;
    Alarm_Real_ReadOffset = alarm_read_offset_snap;
    Memory_BKP_Save();
    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}
/***********************************************
** Function name:       Alarm_Data_Failure
** Descriptions:        报警数据失败偏移
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Failure(void)
{
    Memory_BKP_Read();
    Alarm_Real_ReadDate = alarm_read_date_snap;
    Alarm_Real_ReadOffset = alarm_read_offset_snap;
    Memory_BKP_Save();
}

/***********************************************
** Function name:       Alarm_Data_Num
** Descriptions:        读取未发送报警数
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
vu16 Alarm_Data_Num(void)
{
    vu8 i = 0;
    vu8 UseStorage = DefaultDisk;
    vu8 GetMark = 0;
    vu8 PackNumRead = 0;
    vu8 PackAllRead = 0;
    vu16 num = 0;
    vu32 res = 0;
    vu32 size = 0;
    vu32 size_all = 0;
    vu32 GetLen = 0;
    vu32 GetEndAddr = 0;
    vu32 GetStartAddr = 0;
    vu32 alarm_date = 0;
    vu32 alarm_read_max = 0;
    vu32 alarm_date_last = 0;
    vu32 AbsoluteTimeGet = 0;
    char *path = NULL;
    volatile calendar_obj calendar;
    path = mymalloc(PathLen);
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);
    Memory_BKP_Read();

    if((Alarm_Real_ReadDate != alarm_date) && (Alarm_Real_ReadDate != alarm_date_last))
    {
        Alarm_Real_ReadDate = alarm_date_last;
        Alarm_Real_ReadOffset = 0;
    }

#ifdef DTU3PRO

    for(i = 0; i < 4; i++)
    {
        if((i % 2) == 0)
        {
            UseStorage = DefaultDisk;
        }
        else if((i % 2) == 1)
        {
            UseStorage = ExpansionDisk;
        }

        res = Alarm_List_Read(UseStorage, Alarm_Real_ReadDate, 0, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(res != 0)
        {
            break;
        }

        if((i >= 1) && (Alarm_Real_ReadDate == alarm_date_last))
        {
            Alarm_Real_ReadDate = alarm_date;
        }
    }

#else

    for(i = 0; i < 2; i++)
    {
        res = Alarm_List_Read(UseStorage, alarm_read_date_snap, 0, (u8 *)&PackNumRead, (u8 *)&PackAllRead, (u32 *)&GetStartAddr, (u32 *)&GetEndAddr, (u32 *)&GetLen, (u8 *)&GetMark, (u32 *)&AbsoluteTimeGet);

        if(res != 0)
        {
            break;
        }

        if(alarm_read_date_snap == alarm_date_last)
        {
            alarm_read_date_snap = alarm_date;
        }
    }

#endif
    size = Alarm_List_Get_size(UseStorage, alarm_date);
    size_all = size_all + size;

    if(Alarm_Real_ReadDate == alarm_date_last)
    {
        size = Alarm_List_Get_size(UseStorage, alarm_date_last);
        size_all = size_all + size;
    }

    if(Alarm_Real_ReadOffset > size_all)
    {
        Alarm_Real_ReadOffset = Alarm_List_Get_size(UseStorage, alarm_date);
        Memory_BKP_Save();
    }

    num = (size_all - Alarm_Real_ReadOffset) / ALARM_LIST_NEXT;
    //printf("Size:%d Num:%d\n", (size_all - size_success), num);
    myfree(path);
    return num;
}


/***********************************************
** Function name:       Alarm_Data_Delete
** Descriptions:        清空实时报警数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Delete(void)
{
    vu16 i = 0;
    vu16 j = 0;
    vu16 Num = 0;
    vu16 Total = 50;
    vu32 DirName[20] = {0};
    vu32 alarm_data_get = 0;
    char *path = NULL;
    char *FileName[Total];
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);

    for(i = 0; i < Total; i++)
    {
        FileName[i] = mymalloc(50);
        memset(FileName[i], 0, 50);
    }

    //告警数据存储文件夹
    sprintf(path, "%d:/Data/", DefaultDisk);
    lf_ListDirFilesChar(DefaultDisk, path, "Alarm_", 6, FileName, (u16 *)&Num, Total);

    for(i = 0; i < Num; i++)
    {
        j = 0;

        if(strncmp(FileName[i], "Alarm_", 6) == 0)
        {
            j = 6;
            alarm_data_get = astr2int(&FileName[i][j], 8);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", DefaultDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
            lf_unlink(DefaultDisk, path);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", DefaultDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
            lf_unlink(DefaultDisk, path);
            printf("%d\n", alarm_data_get);
        }
    }

#ifdef DTU3PRO

    for(i = 0; i < Total; i++)
    {
        memset(FileName[i], 0, 50);
    }

    memset(path, 0, PathLen);
    //告警数据存储文件夹
    sprintf(path, "%d:/Data/", ExpansionDisk);
    lf_ListDirFilesChar(ExpansionDisk, path, "Alarm_", 6, FileName, (u16 *)&Num, Total);

    for(i = 0; i < Num; i++)
    {
        j = 0;

        if(strncmp(FileName[i], "Alarm_", 6) == 0)
        {
            j = 6;
            alarm_data_get = astr2int(&FileName[i][j], 8);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", ExpansionDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
            lf_unlink(ExpansionDisk, path);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", ExpansionDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
            lf_unlink(ExpansionDisk, path);
            printf("%d\n", alarm_data_get);
        }
    }

#endif
    Alarm_Real_ReadDate = 0;
    Alarm_Real_ReadOffset = 0;
    alarm_read_date_snap = 0;
    alarm_read_offset_snap = 0;

    for(i = 0; i < Total; i++)
    {
        myfree(FileName[i]);
    }

    myfree(path);
}
/***********************************************
** Function name:       Alarm_Data_Expired_Delete
** Descriptions:        删除过期报警数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Data_Expired_Delete(void)
{
    vu16 i = 0;
    vu16 j = 0;
    vu16 Num = 0;
    vu16 Total = 50;
    vu32 DirName[20] = {0};
    vu32 alarm_date = 0;
    vu32 alarm_data_get = 0;
    vu32 alarm_date_last = 0;
    char *path = NULL;
    char *FileName[Total];
    volatile calendar_obj calendar;
    RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
    alarm_date = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
    alarm_date_last = alarm_date;
    Date_Minus((u32 *)&alarm_date_last);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);

    for(i = 0; i < Total; i++)
    {
        FileName[i] = mymalloc(50);
        memset(FileName[i], 0, 50);
    }

    //告警数据存储文件夹
    sprintf(path, "%d:/Data/", DefaultDisk);
    lf_ListDirFilesChar(DefaultDisk, path, "Alarm_", 6, FileName, (u16 *)&Num, Total);

    for(i = 0; i < Num; i++)
    {
        j = 0;

        if(strncmp(FileName[i], "Alarm_", 6) == 0)
        {
            j = 6;
            alarm_data_get = astr2int(&FileName[i][j], 8);

            if((alarm_data_get != alarm_date) && (alarm_data_get != alarm_date_last))
            {
                memset(path, 0, PathLen);
                sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", DefaultDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
                lf_unlink(DefaultDisk, path);
                memset(path, 0, PathLen);
                sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", DefaultDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
                lf_unlink(DefaultDisk, path);
                printf("%d\n", alarm_data_get);
            }
        }
    }

#ifdef DTU3PRO

    for(i = 0; i < Total; i++)
    {
        memset(FileName[i], 0, 50);
    }

    memset(path, 0, PathLen);
    //告警数据存储文件夹
    sprintf(path, "%d:/Data/", ExpansionDisk);
    lf_ListDirFilesChar(ExpansionDisk, path, "Alarm_", 6, FileName, (u16 *)&Num, Total);

    for(i = 0; i < Num; i++)
    {
        j = 0;

        if(strncmp(FileName[i], "Alarm_", 6) == 0)
        {
            j = 6;
            alarm_data_get = astr2int(&FileName[i][j], 8);

            if((alarm_data_get != alarm_date) && (alarm_data_get != alarm_date_last))
            {
                memset(path, 0, PathLen);
                sprintf(path, "%d:/Data/Alarm_%04d%02d%02d.dat", ExpansionDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
                lf_unlink(ExpansionDisk, path);
                memset(path, 0, PathLen);
                sprintf(path, "%d:/Data/Alarm_%04d%02d%02d_list.dat", ExpansionDisk, (u16)(alarm_data_get / 10000), (u8)((alarm_data_get % 10000) / 100), (alarm_data_get % 100));
                lf_unlink(ExpansionDisk, path);
                printf("%d\n", alarm_data_get);
            }
        }
    }

#endif

    for(i = 0; i < Total; i++)
    {
        myfree(FileName[i]);
    }

    myfree(path);
}
#endif
#endif
/***********************************************
** Function name:       AlarmInfo
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void AlarmInfo_DirCheck(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    //告警数据存储文件夹
    sprintf(path, "%d:/AlarmInfo", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
/***********************************************
** Function name:       AlarmInfo_Write
** Descriptions         告警信息临时存储写入
** input parameters:    *AlarmDataType 结构体指针  Alarm_num个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
u8 AlarmInfo_Write(AlarmDataType *AlarmDataBuf, u8 Alarm_num)
{
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu32 fnum = 0;
    vu16 crc = 0;
    vu32 savetime = 0;
    savetime = RTC_GetWorldSecond(Dtu3Detail.Property.timezone);
    buffer = mymalloc((Alarm_num * sizeof(AlarmDataType)) + 4);
    memset(buffer, 0, (Alarm_num * sizeof(AlarmDataType)) + 4);
    buffer[0] = (u8)Alarm_num;
    buffer[1] = (u8)(Alarm_num >> 8);

    for(i = 0; i < Alarm_num; i++)
    {
        for(j = 0; j < sizeof(AlarmDataType); j++)
        {
            buffer[(i * sizeof(AlarmDataType)) + 2 + j] = AlarmDataBuf[i].DataMsg[j];
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], i * sizeof(AlarmDataType));
    buffer[(i * sizeof(AlarmDataType)) + 2] = (u8)crc;
    buffer[(i * sizeof(AlarmDataType)) + 3] = (u8)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/AlarmInfo/%012d.dat", DefaultDisk, savetime);
    AlarmInfo_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, buffer, 0, (Alarm_num * sizeof(AlarmDataType)) + 4);
    myfree(path);
    myfree(buffer);
    return (u16)fnum;
}
/***********************************************
** Function name:       AlarmInfo_Read
** Descriptions:        告警信息临时存储读取
** input parameters:    无
** output parameters:   *AlarmDataBuf 结构体指针
** Returned value:      成功个数
*************************************************/
u8 AlarmInfo_Read(AlarmDataType *AlarmDataBuf)
{
    char *path = NULL;
    char *buffer = NULL;
    vu16 i, j;
    vu16 Alarm_num = 0;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu8 Max = 5;
    vu16 Num = 0;
    //    char hex[8];
    vu32 fnum = 0;
    vu32 FileNameNum[Max];
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    /*将最小文件名设为最大值*/
    vu32 MinNum = 0xFFFFFFFF;
    memset32_t((u32 *)FileNameNum, 0xFFFFFFFF, Max);
    AlarmInfo_DirCheck();
    Num = 0;
    memset(path, 0, PathLen);
    sprintf(path, "%d:/AlarmInfo", DefaultDisk);
    /*通过扫描找到所有文件名*/
    lf_ListDirFiles(DefaultDisk, path, (u32 *)FileNameNum, (u16 *)&Num, Max, 12);

    for(i = 0; i < Num; i++)
    {
        /*比较文件名，找到数值最小的1个文件名（日期）*/
        if(FileNameNum[i] != 0)
        {
            if(MinNum > FileNameNum[i])
            {
                MinNum = FileNameNum[i];
            }
        }
    }

    if(MinNum != 0xFFFFFFFF)
    {
        AlarmReadNum = MinNum;
        memset(path, 0, PathLen);
        sprintf(path, "%d:/AlarmInfo/%012d.dat", DefaultDisk, AlarmReadNum);
        buffer = mymalloc((PORT_LEN * sizeof(AlarmDataType)) + 4);
        memset(buffer, 0, ((PORT_LEN * sizeof(AlarmDataType)) + 4));
        fnum = File_Read(DefaultDisk, path, buffer, 0, ((PORT_LEN * sizeof(AlarmDataType)) + 4));
        Alarm_num = (u16)buffer[0] + (u16)(buffer[1] << 8);

        if(Alarm_num <= PORT_LEN)
        {
            crc_read = (u16)buffer[Alarm_num * sizeof(AlarmDataType) + 2] + (u16)(buffer[Alarm_num * sizeof(AlarmDataType) + 3] << 8);
            crc = (u16)MyCrc16((unsigned char *)&buffer[2], Alarm_num * sizeof(AlarmDataType));

            if(crc == crc_read)
            {
                for(i = 0; i < Alarm_num; i++)
                {
                    for(j = 0; j < sizeof(AlarmDataType); j++)
                    {
                        AlarmDataBuf[i].DataMsg[j] = buffer[(i * sizeof(AlarmDataType)) + 2 + j];
                    }
                }
            }
        }
        else
        {
            Alarm_num = 0;
        }
    }
    else
    {
        Alarm_num = 0;
    }

    myfree(path);
    myfree(buffer);
    return Alarm_num;
}
/***********************************************
** Function name:       AlarmInfo_Num
** Descriptions:        读取未发送临时报警包数
** input parameters:    无
** output parameters:
** Returned value:      包数
*************************************************/
u8 AlarmInfo_Num(void)
{
    char *path = NULL;
    vu16 i;
    vu8 Max = 5;
    vu16 Num = 0;
    vu32 fnum = 0;
    vu32 FileNameNum[Max];
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    memset32_t((u32 *)FileNameNum, 0xFFFFFFFF, Max);
    AlarmInfo_DirCheck();
    Num = 0;
    memset(path, 0, PathLen);
    sprintf(path, "%d:/AlarmInfo", DefaultDisk);
    /*通过扫描找到所有文件名*/
    lf_ListDirFiles(DefaultDisk, path, (u32 *)FileNameNum, (u16 *)&Num, Max, 12);
    myfree(path);
    return Num;
}
/***********************************************
** Function name:       AlarmInfo_Success
** Descriptions:        删除发送成功临时报警
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void AlarmInfo_Success(void)
{
    vs16 res;
    lfs_dir_t dir;
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/AlarmInfo/%012d.dat", DefaultDisk, AlarmReadNum);
    res = lf_unlink(DefaultDisk, path);
    myfree(path);
}
/***********************************************
** Function name:       AlarmInfo_Delete
** Descriptions:        清空临时报警
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void AlarmInfo_Delete(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    AlarmInfo_DirCheck();
    sprintf(path, "%d:/AlarmInfo", DefaultDisk);
    lf_DeleteAllFiles(DefaultDisk, path);
    myfree(path);
}
/***********************************************
** Function name:       Alarm_Serial_Num_Write
** Descriptions:        告警序号存储
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Alarm_Serial_Num_Write(u16 *AlarmSN, calendar_obj calendar)
{
    char *path = NULL;
    vu8 i = 0;
    u8 *save_buf = NULL;
    vu16 crc = 0;
    vu16 buff_len = 0;
    vu32 save_time = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/AlarmSN.dat", DefaultDisk);
    //查看文件大小
    buff_len = Read_File_Length(DefaultDisk, path);

    if(buff_len > 0)
    {
        //文件存在先删除文件
        lf_unlink(DefaultDisk, path);
    }

    //100 * sizeof(u16)) + sizeof(u16) + sizeof(u32)
    save_buf = mymalloc(206);
    memset(save_buf, 0, 206);
    save_time = DateToSec(calendar);
    save_buf[0] = (u8)(save_time);
    save_buf[1] = (u8)(save_time >> 8);
    save_buf[2] = (u8)(save_time >> 16);
    save_buf[3] = (u8)(save_time >> 24);

    for(i = 0; i < 100; i++)
    {
        save_buf[i * 2 + 4] = (u8)(AlarmSN[i]);
        save_buf[i * 2 + 5] = (u8)(AlarmSN[i] >> 8);
    }

    crc = (u16)MyCrc16(save_buf, 204);
    save_buf[204] = (u8)(crc);
    save_buf[205] = (u8)(crc >> 8);
    File_Write(DefaultDisk, path, (char *)save_buf, 0, 206);
    myfree(path);
    myfree(save_buf);
}
/***********************************************
** Function name:       Alarm_Serial_Num_Read
** Descriptions:        告警序号读取
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 Alarm_Serial_Num_Read(u16 *AlarmSN, calendar_obj *calendar)
{
    char *path = NULL;
    vu8 i = 0;
    u8 *readbuf = NULL;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu16 buff_len = 0;
    vu32 read_time = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    readbuf = mymalloc(206);
    memset(readbuf, 0, 206);
    sprintf(path, "%d:/AlarmSN.dat", DefaultDisk);
    File_Read(DefaultDisk, path, (char *)readbuf, 0, 206);
    crc = (u16)MyCrc16(readbuf, 204);
    crc_read = (u16)readbuf[204] + (u16)(readbuf[205] << 8);

    if(crc == crc_read)
    {
        //100 * sizeof(u16)) + sizeof(u16) + sizeof(u32)
        read_time = ((u32)readbuf[0]) + ((u32)readbuf[1] << 8) +
                    ((u32)readbuf[2] << 16) + ((u32)readbuf[3] << 24);
        SecToDate(read_time, calendar);

        for(i = 0; i < 100; i++)
        {
            AlarmSN[i] = ((u16)readbuf[i * 2 + 4]) + (u16)(readbuf[i * 2 + 5] << 8);
        }

        myfree(path);
        myfree(readbuf);
        return 1;
    }
    else
    {
        myfree(path);
        myfree(readbuf);
        return 0;
    }
}

/***********************************************
** Function name:       GPST_Get_size
** Descriptions:        获取自检数据大小
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 GPST_Get_size(void)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/GPST.dat", DefaultDisk);
    BuffLen = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return BuffLen;
}
/***********************************************
** Function name:       GPSTData_Write
** Descriptions:        自检数据写入
** input parameters:    存储结构体GPSTVal *GPSTValBuf, u16 GPST_num
** output parameters:   无
** Returned value:      成功长度
*************************************************/
void GPST_Data_Write(GPSTVal *GPSTValBuf, u8 GPST_num)
{
    char *path = NULL;
    char *buffer = NULL;
    vu16 i = 0;
    vu16 j = 0;
    vu16 crc = 0;
    vu16 length = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    length = GPST_num * GPST_SIZE_MAX;
    buffer = mymalloc(length);
    memset(buffer, 0, length);

    for(i = 0; i < GPST_num; i++)
    {
        for(j = 0; j < sizeof(GPSTVal); j++)
        {
            buffer[i * GPST_SIZE_MAX + j] = GPSTValBuf[i].Data[j];
        }

        crc = (u16)MyCrc16((unsigned char *)&buffer[i * GPST_SIZE_MAX + 0], (GPST_SIZE_MAX - 2));
        buffer[(i + 1) * GPST_SIZE_MAX - 2] = (u8)(crc);
        buffer[(i + 1) * GPST_SIZE_MAX - 1] = (u8)(crc >> 8);
    }

    sprintf(path, "%d:/GPST.dat", DefaultDisk);
    File_Write_Que(DefaultDisk, path, buffer, GPST_Write_Offset, length);
    GPST_Write_Offset = GPST_Write_Offset + length;
    myfree(path);
    myfree(buffer);
}
/***********************************************
** Function name:       GPST_Data_Read
** Descriptions:        自检数据读取
** input parameters:    GPSTVal *GPSTValBuf, 期望读取数量u16 GPST_num
** output parameters:   *Data数据
** Returned value:      成功读取数量
*************************************************/
vu16 GPST_Data_Read(GPSTVal *GPSTValBuf, u8 GPST_num)
{
    char *path = NULL;
    char *buffer = NULL;
    u8 read_num = 0;
    u8 read_num_max = 0;
    vu16 crc = 0;
    vu16 crc_read = 0;
    vu16 length = 0;
    vu16 read_len = 0;
    vu32 i = 0;
    vu32 j = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    length = GPST_num * GPST_SIZE_MAX;
    buffer = mymalloc(length);
    memset(buffer, 0, length);
    sprintf(path, "%d:/GPST.dat", DefaultDisk);
    read_len = File_Read(DefaultDisk, path, buffer, GPST_Read_Offset, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(buffer);
        return 0;
    }

    for(i = 0; i < (read_len / GPST_SIZE_MAX); i++)
    {
        crc = (u16)MyCrc16((unsigned char *)&buffer[i * GPST_SIZE_MAX + 0], (GPST_SIZE_MAX - 2));
        crc_read = ((u16)buffer[(i + 1) * GPST_SIZE_MAX - 1] << 8) + buffer[(i + 1) * GPST_SIZE_MAX - 2];

        if(crc == crc_read)
        {
            for(j = 0; j < sizeof(GPSTVal); j++)
            {
                GPSTValBuf[read_num].Data[j] = buffer[i * GPST_SIZE_MAX + j];
            }

            read_num++;
        }
    }

    GPST_Read_Offset_Temp = GPST_Read_Offset + read_len;
    myfree(path);
    myfree(buffer);
    return read_num;
}
/***********************************************
** Function name:       GPST_Data_Success
** Descriptions:        自检数据成功标记
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPST_Data_Success(void)
{
    GPST_Read_Offset = GPST_Read_Offset_Temp;
}

/***********************************************
** Function name:       GPST_Data_Num
** Descriptions:        自检数据未发送报警数
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
vu16 GPST_Data_Num(void)
{
    vu16 num = 0;
    vu32 gpst_size = 0;
    gpst_size = GPST_Get_size();

    if(gpst_size > GPST_Read_Offset)
    {
        num = (gpst_size - GPST_Read_Offset) / GPST_SIZE_MAX;
    }
    else
    {
        num = 0;
        GPST_Read_Offset = gpst_size;
    }

    return num;
}


/***********************************************
** Function name:       GPST_Data_Delete
** Descriptions:        清空自检数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPST_Data_Delete(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/GPST.dat", DefaultDisk);
    lf_unlink(DefaultDisk, path);
    GPST_Write_Offset = 0;
    GPST_Read_Offset = 0;
    GPST_Read_Offset_Temp = 0;
    myfree(path);
}

#ifdef DTU3PRO
/***********************************************
** Function name:       SunSpec_Info_Get_size
** Descriptions:        获取自检数据大小
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
u32 SunSpec_Info_Get_size(void)
{
    char *path = NULL;
    vu32 BuffLen = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/SunSpec.dat", DefaultDisk);
    BuffLen = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return BuffLen;
}
/***********************************************
** Function name:       SunSpec_Info_Write
** Descriptions:        SunSpec数据写入
** input parameters:    存储结构体SUNSPEC_INFO *SunSpec_Info_Buf, u8 Info_num
** output parameters:   无
** Returned value:      成功长度
*************************************************/
void SunSpec_Info_Write(SUNSPEC_INFO *SunSpec_Info_Buf, u8 Info_num)
{
    char *path = NULL;
    char *buffer = NULL;
    vu16 i = 0;
    vu16 j = 0;
    vu16 crc = 0;
    vu16 length = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    length = Info_num * SUNSPEC_SIZE_MAX + 4;
    buffer = mymalloc(length);
    memset(buffer, 0, length);
    buffer[0] = (u8)Info_num;
    buffer[1] = (u8)(Info_num >> 8);

    for(i = 0; i < Info_num; i++)
    {
        for(j = 0; j < sizeof(SUNSPEC_INFO); j++)
        {
            buffer[i * SUNSPEC_SIZE_MAX + j + 2] = SunSpec_Info_Buf[i].Data[j];
        }
    }

    crc = (u16)MyCrc16((unsigned char *)&buffer[2], Info_num * SUNSPEC_SIZE_MAX);
    buffer[Info_num * SUNSPEC_SIZE_MAX + 2] = (u8)crc;
    buffer[Info_num * SUNSPEC_SIZE_MAX + 3] = (u8)(crc >> 8);
    SunSpec_Info_Delete();
    sprintf(path, "%d:/SunSpec.dat", DefaultDisk);
    File_Write_Que(DefaultDisk, path, buffer, 0, length);
    myfree(path);
    myfree(buffer);
}
/***********************************************
** Function name:       SunSpec_Info_Read
** Descriptions:        SunSpec数据读取
** input parameters:    SUNSPEC_INFO *SunSpec_Info_Buf
** output parameters:   *Data数据
** Returned value:      成功读取数量
*************************************************/
vu16 SunSpec_Info_Read(SUNSPEC_INFO *SunSpec_Info_Buf)
{
    char *path = NULL;
    char *buffer = NULL;
    u8 read_num_max = 0;
    vu16 crc = 0;
    vu16 read_num = 0;
    vu16 crc_read = 0;
    vu16 length = 0;
    vu16 read_len = 0;
    vu32 i = 0;
    vu32 j = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    length = 100 * SUNSPEC_SIZE_MAX + 4;
    buffer = mymalloc(length);
    memset(buffer, 0, length);
    sprintf(path, "%d:/SunSpec.dat", DefaultDisk);
    read_len = File_Read(DefaultDisk, path, buffer, 0, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(buffer);
        return 0;
    }

    read_num = (u16)buffer[0] + (u16)(buffer[1] << 8);
    crc = (u16)MyCrc16((unsigned char *)&buffer[2], read_num * SUNSPEC_SIZE_MAX);
    crc_read = buffer[read_num * SUNSPEC_SIZE_MAX + 2] + ((u16)buffer[read_num * SUNSPEC_SIZE_MAX + 3] << 8);

    if(crc == crc_read)
    {
        for(i = 0; i < read_num; i++)
        {
            for(j = 0; j < sizeof(SUNSPEC_INFO); j++)
            {
                SunSpec_Info_Buf[i].Data[j] = buffer[i * SUNSPEC_SIZE_MAX + j + 2];
            }
        }
    }
    else
    {
        read_num = 0;
    }

    myfree(path);
    myfree(buffer);
    return read_num;
}

/***********************************************
** Function name:       SunSpec_Info_Delete
** Descriptions:        清空SunSpec数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void SunSpec_Info_Delete(void)
{
    char *path = NULL;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/SunSpec.dat", DefaultDisk);
    lf_unlink(DefaultDisk, path);
    myfree(path);
}
#endif
/***********************************************
** Function name:       History_Scanning
** Descriptions:        历史目录扫描
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 History_Scanning(void)
{
    vu8 Mark;
    vu8 i = 0;
    vu8 Style = 0;
    vu32 StartAddr;
    vu32 EndAddr;
    vu32 DataLen;
    vu32 AbsoluteTime = 0;
    Memory_BKP_Read();
    vu32 HistoryTime;

    for(i = 0; i < 2; i++)
    {
#ifndef DEBUG
        IWDG_Feed();
#endif
        DIR_Read((u8 *)&Style, (u32 *)&StartAddr, (u32 *)&EndAddr, (u32 *)&DataLen, (u8 *)&Mark, (u32 *)&AbsoluteTime);

        if(Style == History_Style)
        {
            if(Mark == 0)
            {
                UploadedDate = DIR_ReadDate;
                UploadedOffset = StartAddr;
                InsideOffset = 0;
                Memory_BKP_Save();
                break;
            }
        }
        else if(Style == AlarmDt_Style)
        {
            if(Mark == 0)
            {
                Alarm_History_ReadDate = DIR_ReadDate;
                Alarm_History_ReadOffset = StartAddr;
                Memory_BKP_Save();
                break;
            }
        }

        Style = 0;

        if((WriteDate == DIR_ReadDate) && (DIR_ReadOffset >= DIR_WriteOffset))
        {
            break;
        }
        else
        {
            DIR_ReadOffset = DIR_ReadOffset + DIR_NEXT;
        }
    }

    Memory_BKP_Save();
    return Style;
}
/***********************************************
** Function name:       File_Processing
** Descriptions:        文件系统分时写入数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void File_Processing(void)
{
    char *path = NULL;
    vu16 Num = 0;
#ifdef TimeTest
    vu32 File_time = 0;
#endif
    vu32 write_num = 0;
    vu32 Capacity = 0;
    volatile calendar_obj calendar;
    static vu32 DownSize = 0;
    static vu32 LaveCapacity_0 = 0;
    static vu32 LaveCapacity_1 = 0;
    static vu16 Num_0 = 0;
    static vu16 Num_1 = 0;
    Memory_Timeout_Free();

    //    DownBuf_Timeout_Free();
    switch(fp_state)
    {
        case fp_init:
            if(Fs_Init(0))
            {
                if(File_fp_get.path != NULL)
                {
                    myfree(File_fp_get.path);
                    File_fp_get.path = NULL;
                }

                if(File_fp_get.buffer != NULL)
                {
                    myfree(File_fp_get.buffer);
                    File_fp_get.buffer = NULL;
                }

                fp_state = fp_idle;
            }

            break;

        case fp_idle:
            if(Queue_IsEmpty() == 1)
            {
                IdleTime = LocalTime;

                if(File_fp_get.path == NULL)
                {
                    File_fp_get.path = mymalloc(PathLen);
                }
                else
                {
                    memset(File_fp_get.path, 0, PathLen);
                }

                if(File_fp_get.buffer == NULL)
                {
                    File_fp_get.buffer = mymalloc(BufferLen);
                }
                else
                {
                    memset(File_fp_get.buffer, 0, BufferLen);
                }

                if(Queue_Out(&File_fp_get))
                {
                    File_WriteOffset = 0;
                    fp_state = fp_open;
                }
            }
            else
            {
                if((LocalTime - IdleTime) >= 1000 * 5)
                {
                    IdleTime = LocalTime;

                    if(File_fp_get.path != NULL)
                    {
                        myfree(File_fp_get.path);
                        File_fp_get.path = NULL;
                    }

                    if(File_fp_get.buffer != NULL)
                    {
                        myfree(File_fp_get.buffer);
                        File_fp_get.buffer = NULL;
                    }
                }

                RTC_GetWorldTime((calendar_obj *)&calendar, Dtu3Detail.Property.timezone);
#ifdef MEMORY_DEBUG_TEXT

                if((LocalTime - CheckTime) >= 1000 * 10)
                {
                    {
#else

                if((calendar.hour > SCAN_TIME_HOURS) && (calendar.min > SCAN_TIME_MINUTES) && (calendar.sec > SCAN_TIME_SECONDS))
                {
                    if((LocalTime - CheckTime) >= 1000 * 60 * 5)
                    {
#endif
                        CheckTime = LocalTime;
                        fp_state = fp_check0;
                    }
                }
            }
            break;

        case fp_open:
#if SCATTER
            if(memory_down_mode == 1)
            {
                File_res = lf_open(File_fp_get.disk, &lfnew, File_fp_get.path, LFS_O_RDWR | LFS_O_CREAT);
                lf_seek(File_fp_get.disk, &lfnew, File_fp_get.off);
                lf_write(File_fp_get.disk, &lfnew, File_fp_get.buffer, File_fp_get.size, (u32 *)&write_num);
                lf_close(File_fp_get.disk, &lfnew);
                fp_state = fp_idle;
            }
            else
            {
                if(File_fp_get.path != NULL)
                {
                    //                if(res == LFS_ERR_OK)
                    //                {
                    //                    Write_Luck = 1;
                    //                    File_WriteOffset = 0;
                    //                    fp_state = fp_write;
                    //                }
                    Write_Luck = 1;

                    if((File_fp_get.size - File_WriteOffset) > WriteLimit)
                    {
                        File_res = lf_open(File_fp_get.disk, &lfnew, File_fp_get.path, LFS_O_RDWR | LFS_O_CREAT);
                        WriteMode = 0;
                        //                    lf_seek(fp_get.disk, &lfnew, (fp_get.off + File_WriteOffset));
                        //                    lf_write(fp_get.disk, &lfnew, &fp_get.buffer[File_WriteOffset], WriteLimit, &write_num);
                        //                    lf_close(fp_get.disk, &lfnew);
                        //                    File_WriteOffset = File_WriteOffset + WriteLimit;
                        //                    fp_state = fp_open;
                        fp_state = fp_write;
                    }
                    else
                    {
                        File_res = lf_open(File_fp_get.disk, &lfnew, File_fp_get.path, LFS_O_RDWR | LFS_O_CREAT);
                        WriteMode = 1;
                        fp_state = fp_write;
                        //                    lf_seek(fp_get.disk, &lfnew, (fp_get.off + File_WriteOffset));
                        //                    lf_write(fp_get.disk, &lfnew, &fp_get.buffer[File_WriteOffset], fp_get.size - File_WriteOffset, &write_num);
                        //                    lf_close(fp_get.disk, &lfnew);
                        //                    fp_state = fp_idle;
                    }
                }
                else
                {
                    fp_state = fp_idle;
                }

                //#ifdef TimeTest
                ////            if((LocalTime - File_time) > 30)
                //            {
                //                printf("fp_write: %d\n", (LocalTime - File_time));
                //            }
                //#endif
            }

#else
            File_res = lf_open(File_fp_get.disk, &lfnew, File_fp_get.path, LFS_O_RDWR | LFS_O_CREAT);
            lf_seek(File_fp_get.disk, &lfnew, File_fp_get.off);
            lf_write(File_fp_get.disk, &lfnew, File_fp_get.buffer, File_fp_get.size, &write_num);
            lf_close(File_fp_get.disk, &lfnew);
            fp_state = fp_idle;
#endif
            break;

        case fp_write:
            if(WriteMode == 0)
            {
                lf_seek(File_fp_get.disk, &lfnew, (File_fp_get.off + File_WriteOffset));
                lf_write(File_fp_get.disk, &lfnew, &File_fp_get.buffer[File_WriteOffset], WriteLimit, (u32 *)&write_num);
                //                    lf_close(fp_get.disk, &lfnew);
                File_WriteOffset = File_WriteOffset + WriteLimit;
            }
            else
            {
                lf_seek(File_fp_get.disk, &lfnew, (File_fp_get.off + File_WriteOffset));
                lf_write(File_fp_get.disk, &lfnew, &File_fp_get.buffer[File_WriteOffset], File_fp_get.size - File_WriteOffset, (u32 *)&write_num);
                //                    lf_close(fp_get.disk, &lfnew);
                //                    fp_state = fp_idle;
            }

            fp_state = fp_close;
            //#ifdef TimeTest
            //            File_time = LocalTime;
            //#endif
            //            if((fp_get.size - File_WriteOffset) > WriteLimit)
            //            {
            //                lf_seek(fp_get.disk, &lfnew, (fp_get.off + File_WriteOffset));
            //                lf_write(fp_get.disk, &lfnew, &fp_get.buffer[File_WriteOffset], WriteLimit, &write_num);
            //                File_WriteOffset = File_WriteOffset + WriteLimit;
            //                fp_state = fp_write;
            //            }
            //            else
            //            {
            //                lf_seek(fp_get.disk, &lfnew, (fp_get.off + File_WriteOffset));
            //                lf_write(fp_get.disk, &lfnew, &fp_get.buffer[File_WriteOffset], fp_get.size - File_WriteOffset, &write_num);
            //                fp_state = fp_close;
            //            }
            //#ifdef TimeTest
            //            {
            //                printf("lf_write: %d\n", (LocalTime - File_time));
            //            }
            //#endif
            break;

        case fp_close:
            lf_close(File_fp_get.disk, &lfnew);

            if(WriteMode == 0)
            {
                fp_state = fp_open;
            }
            else
            {
                //                    lf_close(fp_get.disk, &lfnew);
                Write_Luck = 0;
                fp_state = fp_idle;
            }

            //#ifdef TimeTest
            //            File_time = LocalTime;
            //#endif
            //            lf_close(fp_get.disk, &lfnew);
            //            Write_Luck = 0;
            //            fp_state = fp_idle;
            //#ifdef TimeTest
            //            if((LocalTime - File_time) > 30)
            //            {
            //                printf("lf_open: %d\n", (LocalTime - File_time));
            //            }
            //#endif
            break;

        case fp_check0:
            Capacity = 0;
            SavedQuan = 0;
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/", DefaultDisk);
            //实时数据已存数量
            lf_DirectorySize(DefaultDisk, path, (u32 *)&Capacity, (u16 *)&SavedQuan);
            myfree(path);
            fp_state = fp_check1;
            break;

        case fp_check1:
#ifdef DTU3PRO
            Capacity = 0;
            SavedQuanExp = 0;
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:/Data/", ExpansionDisk);
            //实时数据已存数量
            lf_DirectorySize(ExpansionDisk, path, (u32 *)&Capacity, (u16 *)&SavedQuanExp);
            myfree(path);
#endif
            fp_state = fp_check2;
            break;

        case fp_check2:
            Num_0 = 0;
            LaveCapacity_0 = 0;
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:", DefaultDisk);
            lf_LaveCapacity(DefaultDisk, path, (u32 *)&LaveCapacity_0, (u16 *)&Num_0);
            myfree(path);
#ifdef DTU3PRO
            fp_state = fp_check3;
#else
            fp_state = fp_check4;
#endif
            break;

        case fp_check3:
#ifdef DTU3PRO
            Num_1 = 0;
            LaveCapacity_1 = 0;
            path = mymalloc(PathLen);
            memset(path, 0, PathLen);
            sprintf(path, "%d:", ExpansionDisk);
            lf_LaveCapacity(ExpansionDisk, path, (u32 *)&LaveCapacity_1, (u16 *)&Num_1);
            myfree(path);
#endif
            fp_state = fp_check4;
            break;

        case fp_check4:
#ifdef DTU3PRO
            if(((s32)(LaveCapacity_0) <= (s32)(RESERVE_SPACE + (DOWN_SPACE - DownSize))) &&
                    ((s32)(LaveCapacity_1) <= (s32)(RESERVE_SPACE)))
#else
            if((s32)(LaveCapacity_0) <= (s32)(RESERVE_SPACE + (DOWN_SPACE - DownSize)))
#endif
            {
                DeleteDataState = Dele_Default;
                fp_state = fp_delete;
            }
            else
            {
#ifdef DTU3PRO

                if((Dtu3Detail.Property.StorageLocat != ExpansionDisk) &&
                        ((s32)(LaveCapacity_0) <= (s32)(RESERVE_SPACE + (DOWN_SPACE - DownSize))) &&
                        ((s32)(LaveCapacity_1) > (s32)(RESERVE_SPACE)))
                {
                    Dtu3Detail.Property.StorageLocat = ExpansionDisk;
                    //System_DtuDetail_Write(&Dtu3Detail);
                    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                }
                else if((Dtu3Detail.Property.StorageLocat != DefaultDisk) &&
                        ((s32)(LaveCapacity_0) > (s32)(RESERVE_SPACE + (DOWN_SPACE - DownSize))) &&
                        ((s32)(LaveCapacity_1) <= (s32)(RESERVE_SPACE)))
                {
                    Dtu3Detail.Property.StorageLocat = DefaultDisk;
                    //System_DtuDetail_Write(&Dtu3Detail);
                    System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                }

#endif
                fp_state = fp_idle;
            }

            break;

        case fp_delete:
            {
                DeleteOldestData();

                if(DeleteDataState == Dele_End)
                {
#ifdef MEMORY_DEBUG_TEXT
                    printf("DeleteDataState \n");
#endif
                    fp_state = fp_idle;
                }
            }
            break;

        default:
            fp_state = fp_idle;
            break;
    }
}

/***********************************************
** Function name:       Get_FileStatus
** Descriptions:        获取文件系统运行状态
** input parameters:    无
** output parameters:   无
** Returned value:      1 忙 0 空闲
*************************************************/
u8 Get_FileStatus(void)
{
    if(fp_state == fp_idle)
    {
        if(Queue_IsEmpty() == 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}
#ifdef DTU3PRO
/***********************************************
** Function name:       System_Cfg_Read
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
void System_Cfg_Read(SystemCfgNew *cfg)
{
    vu16 cfg_crc = 0;
    volatile SystemCfg SystemCfg_get;
    volatile SystemCfgNew SystemCfgNew_get;
    memset((u8 *)SystemCfg_get.Data, 0, sizeof(SystemCfg));
    memset((u8 *)SystemCfgNew_get.Data, 0, sizeof(SystemCfgNew));
    EEPROM_BufferRead(EEPROM_Factory_New_Site, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
    cfg_crc = (u16)MyCrc16((unsigned char *)&SystemCfgNew_get.Data[10], (sizeof(SystemCfgNew) - 10 - 2));

    if(cfg_crc == SystemCfgNew_get.Property.cfg_crc)
    {
        memset((u8 *)cfg->Data, 0, sizeof(SystemCfgNew));
        memcpy((u8 *)cfg->Data, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
    }
    else
    {
        memset((u8 *)SystemCfg_get.Data, 0, sizeof(SystemCfg));
        memset((u8 *)SystemCfgNew_get.Data, 0, sizeof(SystemCfgNew));
        EEPROM_BufferRead(EEPROM_Factory_Def_Site, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
        cfg_crc = (u16)MyCrc16((unsigned char *)&SystemCfgNew_get.Data[10], (sizeof(SystemCfgNew) - 10 - 2));

        if(cfg_crc == SystemCfgNew_get.Property.cfg_crc)
        {
            memset((u8 *)cfg->Data, 0, sizeof(SystemCfgNew));
            memcpy((u8 *)cfg->Data, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
        }
        else
        {
            EEPROM_BufferRead(EEPROM_Factory_Def_Site, (u8 *)SystemCfg_get.Data, sizeof(SystemCfg));
            cfg_crc = (u16)MyCrc16((unsigned char *)&SystemCfg_get.Data[10], (sizeof(SystemCfg) - 10 - 2));

            if(cfg_crc == SystemCfg_get.Property.cfg_crc)
            {
                memset((u8 *)cfg->Data, 0, sizeof(SystemCfgNew));
                memset((u8 *)SystemCfgNew_get.Data, 0, sizeof(SystemCfgNew));
                //升级标志
                memcpy((u8 *)SystemCfgNew_get.Property.UpgradeSign, (u8 *)SystemCfg_get.Property.UpgradeSign, sizeof(SystemCfgNew_get.Property.UpgradeSign));
                //服务器域名
                memcpy((u8 *)SystemCfgNew_get.Property.ServerDomainName, (u8 *)SystemCfg_get.Property.ServerDomainName, sizeof(SystemCfgNew_get.Property.ServerDomainName));
                //服务器端口号
                SystemCfgNew_get.Property.ServerPort = SystemCfg_get.Property.ServerPort;
                //APN
                memcpy((u8 *)SystemCfgNew_get.Property.APN, (u8 *)SystemCfg_get.Property.APN, sizeof(SystemCfgNew_get.Property.APN));
                //网络模式
                SystemCfgNew_get.Property.netmode_select = SystemCfg_get.Property.netmode_select;
                //硬件小版本号
                SystemCfgNew_get.Property.small_version = SystemCfg_get.Property.small_version;
                //CRC
                SystemCfgNew_get.Property.cfg_crc = (u16)MyCrc16((unsigned char *)&SystemCfgNew_get.Data[10], (sizeof(SystemCfgNew) - 10 - 2));
                System_Cfg_Write((SystemCfgNew *)&SystemCfgNew_get);
                memcpy((u8 *)cfg->Data, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
            }
            else if(0xFFFF == SystemCfg_get.Property.cfg_crc)
            {
                if((SystemCfg_get.Property.ServerPort == 10081) || (SystemCfg_get.Property.ServerPort == 10017))
                {
                    memset((u8 *)cfg->Data, 0, sizeof(SystemCfgNew));
                    memset((u8 *)SystemCfgNew_get.Data, 0, sizeof(SystemCfgNew));
                    //升级标志
                    memcpy((u8 *)SystemCfgNew_get.Property.UpgradeSign, (u8 *)SystemCfg_get.Property.UpgradeSign, sizeof(SystemCfgNew_get.Property.UpgradeSign));
                    //服务器域名
                    memcpy((u8 *)SystemCfgNew_get.Property.ServerDomainName, (u8 *)SystemCfg_get.Property.ServerDomainName, sizeof(SystemCfgNew_get.Property.ServerDomainName));
                    //服务器端口号
                    SystemCfgNew_get.Property.ServerPort = SystemCfg_get.Property.ServerPort;
                    //APN
                    memcpy((u8 *)SystemCfgNew_get.Property.APN, (u8 *)SystemCfg_get.Property.APN, sizeof(SystemCfgNew_get.Property.APN));
                    //网络模式
                    SystemCfgNew_get.Property.netmode_select = SystemCfg_get.Property.netmode_select;
                    //硬件小版本号
                    SystemCfgNew_get.Property.small_version = SystemCfg_get.Property.small_version;
                    //CRC
                    SystemCfgNew_get.Property.cfg_crc = (u16)MyCrc16((unsigned char *)&SystemCfgNew_get.Data[10], (sizeof(SystemCfgNew) - 10 - 2));
                    System_Cfg_Write((SystemCfgNew *)&SystemCfgNew_get);
                    memcpy((u8 *)cfg->Data, (u8 *)SystemCfgNew_get.Data, sizeof(SystemCfgNew));
                }
            }
        }
    }
}
/***********************************************
** Function name:       System_Cfg_Write
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:      flash格式化并写入数据时间为28ms
*************************************************/
void System_Cfg_Write(SystemCfgNew *cfg)
{
    vu16 cfg_crc = 0;
    cfg_crc = (u16)MyCrc16((unsigned char *)&cfg->Data[10], (sizeof(SystemCfgNew) - 10 - 2));
    cfg->Property.cfg_crc = cfg_crc;
    EEPROM_Erase(EEPROM_Factory_Def_Site);
    EEPROM_BufferWrite(EEPROM_Factory_Def_Site, (u8 *)cfg->Data, sizeof(SystemCfgNew));
    EEPROM_Erase(EEPROM_Factory_New_Site);
    EEPROM_BufferWrite(EEPROM_Factory_New_Site, (u8 *)cfg->Data, sizeof(SystemCfgNew));
}
#endif
/***********************************************
** Function name:       System_Dtu_Info_Get_Save_Num
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_Dtu_Info_Get_Save_Num(void)
{
    vu8 i = 0;
    vu16 Dtu_Read_Num[16];
    vu16 Dtu_Read_Max = 0xFFFD;
    vu16 Dtu_Read_Max_0 = 0;
    vu16 Dtu_Read_Max_1 = 0;
    u8 *Dtu_Info_Buf;
    Dtu_Info_Buf = mymalloc(100);
    memset(Dtu_Info_Buf, 0, 100);

    for(i = 0; i < 16; i++)
    {
        Dtu_Read_Num[i] = 0xFFFF;
        memset(Dtu_Info_Buf, 0, 100);
        EEPROM_BufferRead_Site(EEPROM_DTU_Site, Dtu_Info_Buf, i * DTU_INFO_MAX, 2);
        Dtu_Read_Num[i] = (u16)(Dtu_Info_Buf[0]) + (u16)(Dtu_Info_Buf[1] << 8);
    }

    if((((Dtu_Read_Num[0] + 1) == Dtu_Read_Num[1]) || (Dtu_Read_Num[1] == 0xFFFF)) &&
            (((Dtu_Read_Num[1] + 1) == Dtu_Read_Num[2]) || (Dtu_Read_Num[2] == 0xFFFF)) &&
            (((Dtu_Read_Num[2] + 1) == Dtu_Read_Num[3]) || (Dtu_Read_Num[3] == 0xFFFF)) &&
            (((Dtu_Read_Num[3] + 1) == Dtu_Read_Num[4]) || (Dtu_Read_Num[4] == 0xFFFF)) &&
            (((Dtu_Read_Num[4] + 1) == Dtu_Read_Num[5]) || (Dtu_Read_Num[5] == 0xFFFF)) &&
            (((Dtu_Read_Num[5] + 1) == Dtu_Read_Num[6]) || (Dtu_Read_Num[6] == 0xFFFF)) &&
            (((Dtu_Read_Num[6] + 1) == Dtu_Read_Num[7]) || (Dtu_Read_Num[7] == 0xFFFF)))
    {
        if((((Dtu_Read_Num[8] + 1) == Dtu_Read_Num[9]) || (Dtu_Read_Num[9] == 0xFFFF)) &&
                (((Dtu_Read_Num[9] + 1) == Dtu_Read_Num[10]) || (Dtu_Read_Num[10] == 0xFFFF)) &&
                (((Dtu_Read_Num[10] + 1) == Dtu_Read_Num[11]) || (Dtu_Read_Num[11] == 0xFFFF)) &&
                (((Dtu_Read_Num[11] + 1) == Dtu_Read_Num[12]) || (Dtu_Read_Num[12] == 0xFFFF)) &&
                (((Dtu_Read_Num[12] + 1) == Dtu_Read_Num[13]) || (Dtu_Read_Num[13] == 0xFFFF)) &&
                (((Dtu_Read_Num[13] + 1) == Dtu_Read_Num[14]) || (Dtu_Read_Num[14] == 0xFFFF)) &&
                (((Dtu_Read_Num[14] + 1) == Dtu_Read_Num[15]) || (Dtu_Read_Num[15] == 0xFFFF)))
        {
            for(i = 0; i < 8; i++)
            {
                if(Dtu_Read_Num[i] != 0xFFFF)
                {
                    if(Dtu_Read_Num[i] >= Dtu_Read_Max_0)
                    {
                        Dtu_Read_Max_0 = Dtu_Read_Num[i];
                        Dtu_Read_Max = Dtu_Read_Max_0;
                    }
                }
            }

            for(i = 8; i < 16; i++)
            {
                if(Dtu_Read_Num[i] != 0xFFFF)
                {
                    if(Dtu_Read_Num[i] > Dtu_Read_Max_1)
                    {
                        Dtu_Read_Max_1 = Dtu_Read_Num[i];
                        Dtu_Read_Max = Dtu_Read_Max_1;
                    }
                }
            }

            if((Dtu_Read_Max_0 + 1) >= SAVE_NUM_MAX)
            {
                if(Dtu_Read_Num[8] != 0xFFFF)
                {
                    Dtu_Read_Max_1 = (Dtu_Read_Max_1 + SAVE_NUM_MAX) % 0xFFFF;
                }
            }

            if((Dtu_Read_Max_1 + 1) >= SAVE_NUM_MAX)
            {
                if(Dtu_Read_Num[0] != 0xFFFF)
                {
                    Dtu_Read_Max_0 = (Dtu_Read_Max_0 + SAVE_NUM_MAX) % 0xFFFF;
                }
            }

            if(Dtu_Read_Max_0 > Dtu_Read_Max_1)
            {
                Dtu_Read_Max = Dtu_Read_Max_0;
            }
            else if(Dtu_Read_Max_1 > Dtu_Read_Max_0)
            {
                Dtu_Read_Max = Dtu_Read_Max_1;
            }
        }
        else
        {
            Dtu_Read_Max = 0xFFFF;
        }
    }
    else
    {
        Dtu_Read_Max = 0xFFFE;
    }

    myfree(Dtu_Info_Buf);
    return Dtu_Read_Max;
}

/***********************************************
** Function name:       System_Dtu_Info_Read
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_Dtu_Info_Read(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf)
{
    vu16 i = 0;
    vu16 j = 0;
    u8 *Dtu_Info_Buf;
    vu16 Dtu_Read_Num[16];
    vu16 Dtu_Read_Max = 0;
    vu16 DtuDetail_crc = 0;
    vu16 DtuMajor_crc = 0;
    vu16 All_crc = 0;
    DtuMajor *DtuMajorbuf_get;
    Dtu_Info_Buf = mymalloc(DTU_INFO_MAX);
    memset(Dtu_Info_Buf, 0, DTU_INFO_MAX);
    DtuMajorbuf_get = mymalloc(sizeof(DtuMajor));
    memset(DtuMajorbuf_get, 0, sizeof(DtuMajor));
    Dtu_Read_Max = System_Dtu_Info_Get_Save_Num();
    memset((u8 *)DtuDetailbuf->PropertyMsg, 0, sizeof(DtuDetail));
    //路由器SSID
    memset((u8 *)DtuMajorbuf->Property.wifi_ssid, 0, sizeof(DtuMajorbuf_get->Property.wifi_ssid));
    //路由器密码
    memset((u8 *)DtuMajorbuf->Property.wifi_passward, 0, sizeof(DtuMajorbuf_get->Property.wifi_passward));
    //服务器域名
    memset((u8 *)DtuMajorbuf->Property.ServerDomainName, 0, sizeof(DtuMajorbuf_get->Property.ServerDomainName));
    //APN
    memset((u8 *)DtuMajorbuf->Property.APN, 0, sizeof(DtuMajorbuf_get->Property.APN));
    memset((u8 *)DtuMajorbuf->Property.DtuToolAddr, 0, sizeof(DtuMajorbuf_get->Property.DtuToolAddr));
    DtuMajorbuf->Property.wifi_ssid_save_set = 0;
    //服务器上传间隔
    DtuMajorbuf->Property.server_send_time = 0;
    //上网模式选择
    DtuMajorbuf->Property.netmode_select = 0;
    DtuMajorbuf->Property.rule_id = 0;
    //服务器端口号
    DtuMajorbuf->Property.ServerPort = 0;

    for(i = 0; i < 16; i++)
    {
        memset(Dtu_Info_Buf, 0, 2 * DTU_INFO_MAX);

        if(Dtu_Read_Max >= i)
        {
            EEPROM_BufferRead_Site(EEPROM_DTU_Site, Dtu_Info_Buf, ((Dtu_Read_Max - i) % 16) * DTU_INFO_MAX, DTU_INFO_MAX);
            All_crc = (u16)MyCrc16((unsigned char *)Dtu_Info_Buf, (DTU_INFO_MAX - 2));

            if(All_crc == ((Dtu_Info_Buf[DTU_INFO_MAX - 2]) + (Dtu_Info_Buf[DTU_INFO_MAX - 1] << 8)))
            {
                DtuDetail_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[2], DTU_DETAIL_MAX);
                DtuMajor_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[DTU_DETAIL_MAX + 4], DTU_MAJOR_MAX);

                if((DtuDetail_crc == ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + 2]) + ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + 3]) << 8))) &&
                        (DtuMajor_crc == ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 4]) + ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 5]) << 8))))
                {
                    for(j = 0; j < sizeof(DtuDetail); j++)
                    {
                        DtuDetailbuf->PropertyMsg[j] = Dtu_Info_Buf[2 + j];
                    }

                    for(j = 0; j < sizeof(DtuMajor); j++)
                    {
                        DtuMajorbuf_get->PropertyMsg[j] = Dtu_Info_Buf[DTU_DETAIL_MAX + 4 + j];
                    }

                    //路由器SSID
                    memcpy((u8 *)DtuMajorbuf->Property.wifi_ssid, (u8 *)DtuMajorbuf_get->Property.wifi_ssid, sizeof(DtuMajorbuf_get->Property.wifi_ssid));
                    //路由器密码
                    memcpy((u8 *)DtuMajorbuf->Property.wifi_passward, (u8 *)DtuMajorbuf_get->Property.wifi_passward, sizeof(DtuMajorbuf_get->Property.wifi_passward));
                    //服务器域名
                    memcpy((u8 *)DtuMajorbuf->Property.ServerDomainName, (u8 *)DtuMajorbuf_get->Property.ServerDomainName, sizeof(DtuMajorbuf_get->Property.ServerDomainName));
                    //APN
                    memcpy((u8 *)DtuMajorbuf->Property.APN, (u8 *)DtuMajorbuf_get->Property.APN, sizeof(DtuMajorbuf_get->Property.APN));
                    memcpy((u8 *)DtuMajorbuf->Property.APN2, (u8 *)DtuMajorbuf_get->Property.APN2, sizeof(DtuMajorbuf_get->Property.APN2));
                    memcpy((u8 *)DtuMajorbuf->Property.DtuToolAddr, (u8 *)DtuMajorbuf_get->Property.DtuToolAddr, sizeof(DtuMajorbuf_get->Property.DtuToolAddr));
                    DtuMajorbuf->Property.wifi_ssid_save_set = DtuMajorbuf_get->Property.wifi_ssid_save_set;
                    //服务器上传间隔
                    DtuMajorbuf->Property.server_send_time = DtuMajorbuf_get->Property.server_send_time;
                    //上网模式选择
                    DtuMajorbuf->Property.netmode_select = DtuMajorbuf_get->Property.netmode_select;
                    DtuMajorbuf->Property.rule_id = DtuMajorbuf_get->Property.rule_id;
                    //服务器端口号
                    DtuMajorbuf->Property.ServerPort = DtuMajorbuf_get->Property.ServerPort;
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }

    if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id, (char *)Dtu3Major.Property.ServerDomainName, (u16)strlen((char *)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
    {
#ifdef DTU3PRO
        System_Cfg_Read((SystemCfgNew *)&system_cfg);
#else
        ReadSystemConfig((SystemCfgNew *)&system_cfg);
#endif

        if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id, (char *)system_cfg.Property.ServerDomainName, (u16)strlen((char *)system_cfg.Property.ServerDomainName), system_cfg.Property.ServerPort) == 0)
        {
            memset((u8 *)Dtu3Major.Property.ServerDomainName, 0, sizeof(Dtu3Major.Property.ServerDomainName));
            memcpy((u8 *)Dtu3Major.Property.ServerDomainName, (u8 *)system_cfg.Property.ServerDomainName, sizeof(system_cfg.Property.ServerDomainName));
            Dtu3Major.Property.ServerPort = system_cfg.Property.ServerPort;
            memcpy((u8 *)Dtu3Major.Property.APN, (u8 *)system_cfg.Property.APN, sizeof(system_cfg.Property.APN));
#ifdef DTU3PRO
            memcpy((u8 *)Dtu3Major.Property.APN2, (u8 *)system_cfg.Property.APN2, sizeof(system_cfg.Property.APN2));
#endif
        }

        if(Domain_Check((u8 *)Dtu3Major.Property.Pre_Id, (char *)Dtu3Major.Property.ServerDomainName, (u16)strlen((char *)Dtu3Major.Property.ServerDomainName), Dtu3Major.Property.ServerPort))
        {
            Reset_Domain_Config((u8 *)Dtu3Major.Property.Pre_Id);
        }

        //写入参数
        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
    }

    myfree(Dtu_Info_Buf);
    myfree(DtuMajorbuf_get);
    return Dtu_Read_Max;
}
/***********************************************
** Function name:       System_Info_Read_Uncheck
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_Info_Read_Uncheck(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf)
{
    vu16 i = 0;
    vu16 j = 0;
    u8 *Dtu_Info_Buf;
    vu16 Dtu_Read_Num[16];
    vu16 Dtu_Read_Max = 0;
    vu16 DtuDetail_crc = 0;
    vu16 DtuMajor_crc = 0;
    vu16 All_crc = 0;
    DtuMajor *DtuMajorbuf_get;
    Dtu_Info_Buf = mymalloc(DTU_INFO_MAX);
    memset(Dtu_Info_Buf, 0, DTU_INFO_MAX);
    DtuMajorbuf_get = mymalloc(sizeof(DtuMajor));
    memset(DtuMajorbuf_get, 0, sizeof(DtuMajor));
    Dtu_Read_Max = System_Dtu_Info_Get_Save_Num();
    memset((u8 *)DtuDetailbuf->PropertyMsg, 0, sizeof(DtuDetail));
    //路由器SSID
    memset((u8 *)DtuMajorbuf->Property.wifi_ssid, 0, sizeof(DtuMajorbuf_get->Property.wifi_ssid));
    //路由器密码
    memset((u8 *)DtuMajorbuf->Property.wifi_passward, 0, sizeof(DtuMajorbuf_get->Property.wifi_passward));
    //服务器域名
    memset((u8 *)DtuMajorbuf->Property.ServerDomainName, 0, sizeof(DtuMajorbuf_get->Property.ServerDomainName));
    //APN
    memset((u8 *)DtuMajorbuf->Property.APN, 0, sizeof(DtuMajorbuf_get->Property.APN));
    memset((u8 *)DtuMajorbuf->Property.DtuToolAddr, 0, sizeof(DtuMajorbuf_get->Property.DtuToolAddr));
    DtuMajorbuf->Property.wifi_ssid_save_set = 0;
    //服务器上传间隔
    DtuMajorbuf->Property.server_send_time = 0;
    //上网模式选择
    DtuMajorbuf->Property.netmode_select = 0;
    DtuMajorbuf->Property.rule_id = 0;
    //服务器端口号
    DtuMajorbuf->Property.ServerPort = 0;

    for(i = 0; i < 16; i++)
    {
        memset(Dtu_Info_Buf, 0, 2 * DTU_INFO_MAX);

        if(Dtu_Read_Max >= i)
        {
            EEPROM_BufferRead_Site(EEPROM_DTU_Site, Dtu_Info_Buf, ((Dtu_Read_Max - i) % 16) * DTU_INFO_MAX, DTU_INFO_MAX);
            All_crc = (u16)MyCrc16((unsigned char *)Dtu_Info_Buf, (DTU_INFO_MAX - 2));

            if(All_crc == ((Dtu_Info_Buf[DTU_INFO_MAX - 2]) + (Dtu_Info_Buf[DTU_INFO_MAX - 1] << 8)))
            {
                DtuDetail_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[2], DTU_DETAIL_MAX);
                DtuMajor_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[DTU_DETAIL_MAX + 4], DTU_MAJOR_MAX);

                if((DtuDetail_crc == ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + 2]) + ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + 3]) << 8))) &&
                        (DtuMajor_crc == ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 4]) + ((u16)(Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 5]) << 8))))
                {
                    for(j = 0; j < sizeof(DtuDetail); j++)
                    {
                        DtuDetailbuf->PropertyMsg[j] = Dtu_Info_Buf[2 + j];
                    }

                    for(j = 0; j < sizeof(DtuMajor); j++)
                    {
                        DtuMajorbuf_get->PropertyMsg[j] = Dtu_Info_Buf[DTU_DETAIL_MAX + 4 + j];
                    }

                    memcpy((u8 *)DtuMajorbuf->Property.Pre_Id, (u8 *)DtuMajorbuf_get->Property.Pre_Id, sizeof(DtuMajorbuf_get->Property.Pre_Id));
                    memcpy((u8 *)DtuMajorbuf->Property.Id, (u8 *)DtuMajorbuf_get->Property.Id, sizeof(DtuMajorbuf_get->Property.Id));
                    //DTU硬件版本
                    memcpy((u8 *)DtuMajorbuf->Property.DtuHw_Ver, (u8 *)DtuMajorbuf_get->Property.DtuHw_Ver, sizeof(DtuMajorbuf_get->Property.DtuHw_Ver));
                    //DTU软件版本
                    memcpy((u8 *)DtuMajorbuf->Property.DtuSw_Ver, (u8 *)DtuMajorbuf_get->Property.DtuSw_Ver, sizeof(DtuMajorbuf_get->Property.DtuSw_Ver));
                    //DTU RF 硬件版本号
                    memcpy((u8 *)DtuMajorbuf->Property.RfHw_Ver, (u8 *)DtuMajorbuf_get->Property.RfHw_Ver, sizeof(DtuMajorbuf_get->Property.RfHw_Ver));
                    //DTU RF 软件版本
                    memcpy((u8 *)DtuMajorbuf->Property.RfFw_Ver, (u8 *)DtuMajorbuf_get->Property.RfFw_Ver, sizeof(DtuMajorbuf_get->Property.RfFw_Ver));
                    //路由器SSID
                    memcpy((u8 *)DtuMajorbuf->Property.wifi_ssid, (u8 *)DtuMajorbuf_get->Property.wifi_ssid, sizeof(DtuMajorbuf_get->Property.wifi_ssid));
                    //路由器密码
                    memcpy((u8 *)DtuMajorbuf->Property.wifi_passward, (u8 *)DtuMajorbuf_get->Property.wifi_passward, sizeof(DtuMajorbuf_get->Property.wifi_passward));
                    //服务器域名
                    memcpy((u8 *)DtuMajorbuf->Property.ServerDomainName, (u8 *)DtuMajorbuf_get->Property.ServerDomainName, sizeof(DtuMajorbuf_get->Property.ServerDomainName));
                    //APN
                    memcpy((u8 *)DtuMajorbuf->Property.APN, (u8 *)DtuMajorbuf_get->Property.APN, sizeof(DtuMajorbuf_get->Property.APN));
                    memcpy((u8 *)DtuMajorbuf->Property.APN2, (u8 *)DtuMajorbuf_get->Property.APN2, sizeof(DtuMajorbuf_get->Property.APN2));
                    memcpy((u8 *)DtuMajorbuf->Property.DtuToolAddr, (u8 *)DtuMajorbuf_get->Property.DtuToolAddr, sizeof(DtuMajorbuf_get->Property.DtuToolAddr));
                    DtuMajorbuf->Property.wifi_ssid_save_set = DtuMajorbuf_get->Property.wifi_ssid_save_set;
                    //服务器上传间隔
                    DtuMajorbuf->Property.server_send_time = DtuMajorbuf_get->Property.server_send_time;
                    //上网模式选择
                    DtuMajorbuf->Property.netmode_select = DtuMajorbuf_get->Property.netmode_select;
                    DtuMajorbuf->Property.rule_id = DtuMajorbuf_get->Property.rule_id;
                    //服务器端口号
                    DtuMajorbuf->Property.ServerPort = DtuMajorbuf_get->Property.ServerPort;
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }

    myfree(Dtu_Info_Buf);
    myfree(DtuMajorbuf_get);
    return Dtu_Read_Max;
}

/***********************************************
** Function name:       System_Dtu_Info_Write
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_Dtu_Info_Write(DtuDetail *DtuDetailbuf, DtuMajor *DtuMajorbuf)
{
    vu16 i = 0;
    vu16 Dtu_Save_Num = 0;
    vu16 DtuDetail_crc = 0;
    vu16 DtuMajor_crc = 0;
    vu16 All_crc = 0;
    u8 *Dtu_Info_Buf;
    DtuDetail *DtuDetailbuf_get;
    DtuMajor *DtuMajorbuf_get;
    Dtu_Info_Buf = mymalloc(DTU_INFO_MAX);
    memset(Dtu_Info_Buf, 0, DTU_INFO_MAX);

    for(i = 0; i < 2; i++)
    {
        Dtu_Save_Num = System_Dtu_Info_Get_Save_Num();

        if(Dtu_Save_Num == 0xFFFE)
        {
            EEPROM_Erase_Site(EEPROM_DTU_Site, 0);
            Dtu_Save_Num = 0xFFFF;
        }
        else if(Dtu_Save_Num == 0xFFFF)
        {
            EEPROM_Erase_Site(EEPROM_DTU_Site, 0x1000);
            Dtu_Save_Num = 7;
        }
        else if(Dtu_Save_Num == 0xFFFD)
        {
            Dtu_Save_Num = 0xFFFF;
        }
    }

    if((Dtu_Save_Num != 0xFFFF) && ((Dtu_Save_Num + 1) >= SAVE_NUM_MAX))
    {
        Dtu_Save_Num = (Dtu_Save_Num + 1) % SAVE_NUM_MAX;
    }
    else
    {
        Dtu_Save_Num = Dtu_Save_Num + 1;
    }

    if((Dtu_Save_Num % 16) == 0)
    {
        EEPROM_Erase_Site(EEPROM_DTU_Site, 0);
    }
    else if((Dtu_Save_Num % 16) == 8)
    {
        EEPROM_Erase_Site(EEPROM_DTU_Site, 0x1000);
    }

    memset(Dtu_Info_Buf, 0, DTU_INFO_MAX);
    Dtu_Info_Buf[0] = (u8)Dtu_Save_Num;
    Dtu_Info_Buf[1] = (u8)(Dtu_Save_Num >> 8);

    for(i = 0; i < sizeof(DtuDetail); i++)
    {
        Dtu_Info_Buf[2 + i] = DtuDetailbuf->PropertyMsg[i];
    }

    memset(&Dtu_Info_Buf[2 + sizeof(DtuDetail)], 0, (DTU_DETAIL_MAX - sizeof(DtuDetail)));
    DtuDetail_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[2], DTU_DETAIL_MAX);
    Dtu_Info_Buf[DTU_DETAIL_MAX + 2] = (u8)DtuDetail_crc;
    Dtu_Info_Buf[DTU_DETAIL_MAX + 3] = (u8)(DtuDetail_crc >> 8);

    for(i = 0; i < sizeof(DtuMajor); i++)
    {
        Dtu_Info_Buf[DTU_DETAIL_MAX + 4 + i] = DtuMajorbuf->PropertyMsg[i];
    }

    memset(&Dtu_Info_Buf[DTU_DETAIL_MAX + 4 + sizeof(DtuMajor)], 0, (DTU_MAJOR_MAX - sizeof(DtuMajor)));
    DtuMajor_crc = (u16)MyCrc16((unsigned char *)&Dtu_Info_Buf[DTU_DETAIL_MAX + 4], DTU_MAJOR_MAX);
    Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 4] = (u8)DtuMajor_crc;
    Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 5] = (u8)(DtuMajor_crc >> 8);
    memset(&Dtu_Info_Buf[DTU_DETAIL_MAX + DTU_MAJOR_MAX + 6], 0, (DTU_INFO_MAX - (DTU_DETAIL_MAX + DTU_MAJOR_MAX + 8)));
    All_crc = (u16)MyCrc16((unsigned char *)Dtu_Info_Buf, (DTU_INFO_MAX - 2));
    Dtu_Info_Buf[DTU_INFO_MAX - 2] = (u8)All_crc;
    Dtu_Info_Buf[DTU_INFO_MAX - 1] = (u8)(All_crc >> 8);
    EEPROM_BufferWrite_Site(EEPROM_DTU_Site, Dtu_Info_Buf, ((Dtu_Save_Num % 16) * DTU_INFO_MAX), DTU_INFO_MAX);
    myfree(Dtu_Info_Buf);
    return Dtu_Save_Num;
}
/***********************************************
** Function name:       System_MI_SN_Get_Save_Num
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_MI_SN_Get_Save_Num(void)
{
    vu8 i = 0;
    vu16 MI_Read_Num[2];
    vu16 MI_Read_Max = 0;
    u8 *MI_Info_Buf = NULL;
    MI_Info_Buf = mymalloc(100);
    memset(MI_Info_Buf, 0, 100);

    for(i = 0; i < 2; i++)
    {
        MI_Read_Num[i] = 0xFFFF;
        memset(MI_Info_Buf, 0, 100);
        EEPROM_BufferRead_Site(EEPROM_MI_Site, MI_Info_Buf, i * MI_SN_MAX, 2);
        MI_Read_Num[i] = (u16)(MI_Info_Buf[0]) + (u16)(MI_Info_Buf[1] << 8);
    }

    if(MI_Read_Num[0] != 0xFFFF)
    {
        if((MI_Read_Num[0] + 1)  >= SAVE_NUM_MAX)
        {
            MI_Read_Num[1] = (MI_Read_Num[1] + SAVE_NUM_MAX) % 0xFFFF;
        }

        MI_Read_Max = MI_Read_Num[0];
    }

    if(MI_Read_Num[1] != 0xFFFF)
    {
        if((MI_Read_Num[1] + 1)  >= SAVE_NUM_MAX)
        {
            MI_Read_Num[0] = (MI_Read_Num[0] + SAVE_NUM_MAX) % 0xFFFF;
        }

        MI_Read_Max = MI_Read_Num[1];
    }

    if(MI_Read_Num[1] > MI_Read_Num[0])
    {
        MI_Read_Max = MI_Read_Num[1];
    }
    else if(MI_Read_Num[0] > MI_Read_Num[1])
    {
        MI_Read_Max = MI_Read_Num[0];
    }

    myfree(MI_Info_Buf);
    return MI_Read_Max;
}
/***********************************************
** Function name:       System_MI_SN_Read
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_MI_SN_Read(InverterMajor *InverterMajorBuf)
{
    vu16 i = 0;
    vu16 j = 0;
    vu16 k = 0;
    vu16 MI_Save_Num = 0;
    vu16 InverterMajor_crc = 0;
    u8 *MI_Info_Buf = NULL;
    MI_Info_Buf = mymalloc(MI_SN_MAX);
    memset(MI_Info_Buf, 0, MI_SN_MAX);
    MI_Save_Num = System_MI_SN_Get_Save_Num();

    for(i = 0; i < 2; i++)
    {
        memset(MI_Info_Buf, 0, MI_SN_MAX);

        if(MI_Save_Num > i)
        {
            EEPROM_BufferRead_Site(EEPROM_MI_Site, MI_Info_Buf, ((MI_Save_Num - i) % 2) * MI_SN_MAX, MI_SN_MAX);
            InverterMajor_crc = (u16)MyCrc16((unsigned char *)MI_Info_Buf, (MI_SN_MAX - 2));

            if(InverterMajor_crc == ((MI_Info_Buf[MI_SN_MAX - 2]) + (MI_Info_Buf[MI_SN_MAX - 1] << 8)))
            {
                for(j = 0; j < PORT_LEN; j++)
                {
                    for(k = 0; k < sizeof(InverterMajor); k++)
                    {
                        InverterMajorBuf[j].PropertyMsg[k] = MI_Info_Buf[2 + (j * sizeof(InverterMajor)) + k];
                        //InverterMajorBuf[j].PropertyMsg[k] = MI_Info_Buf[2 + (j * MI_SN_MAX_SIZE) + k];
                    }
                }

                break;
            }
        }
        else
        {
            MI_Save_Num = 0;
        }
    }

    myfree(MI_Info_Buf);
    return MI_Save_Num;
}
/***********************************************
** Function name:       System_MI_SN_Write
** Descriptions:
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
u16 System_MI_SN_Write(InverterMajor *InverterMajorBuf)
{
    vu16 i = 0;
    vu16 j = 0;
    vu16 MI_Save_Num = 0;
    vu16 InverterMajor_crc = 0;
    u8 *MI_Info_Buf = NULL;
    MI_Info_Buf = mymalloc(MI_SN_MAX);
    memset(MI_Info_Buf, 0, MI_SN_MAX);
    MI_Save_Num = System_MI_SN_Get_Save_Num();

    if((MI_Save_Num + 1) >= SAVE_NUM_MAX)
    {
        MI_Save_Num = (MI_Save_Num + 1) % SAVE_NUM_MAX;
    }
    else
    {
        MI_Save_Num = MI_Save_Num + 1;
    }

    if((MI_Save_Num % 2) == 0)
    {
        EEPROM_Erase_Site(EEPROM_MI_Site, 0);
    }
    else if((MI_Save_Num % 2) == 1)
    {
        EEPROM_Erase_Site(EEPROM_MI_Site, 0x1000);
    }

    memset(MI_Info_Buf, 0, MI_SN_MAX);
    MI_Info_Buf[0] = (u8)MI_Save_Num;
    MI_Info_Buf[1] = (u8)(MI_Save_Num >> 8);

    for(i = 0; i < PORT_LEN; i++)
    {
        for(j = 0; j < sizeof(InverterMajor); j++)
        {
            MI_Info_Buf[2 + (i * sizeof(InverterMajor)) + j] = InverterMajorBuf[i].PropertyMsg[j];
            //MI_Info_Buf[2 + (i * MI_SN_MAX_SIZE) + j] = InverterMajorBuf[i].PropertyMsg[j];
        }
    }

    memset(&MI_Info_Buf[2 + sizeof(InverterMajor)*PORT_LEN], 0, (MI_SN_MAX - (sizeof(InverterMajor)*PORT_LEN + 4)));
    //memset(&MI_Info_Buf[2 + MI_SN_MAX_SIZE * PORT_LEN], 0, (MI_SN_MAX - (MI_SN_MAX_SIZE * PORT_LEN + 4)));
    InverterMajor_crc = (u16)MyCrc16((unsigned char *)MI_Info_Buf, (MI_SN_MAX - 2));
    MI_Info_Buf[MI_SN_MAX - 2] = (u8)InverterMajor_crc;
    MI_Info_Buf[MI_SN_MAX - 1] = (u8)(InverterMajor_crc >> 8);
    EEPROM_BufferWrite_Site(EEPROM_MI_Site, MI_Info_Buf, ((MI_Save_Num % 2) * 0x1000), MI_SN_MAX);
    myfree(MI_Info_Buf);
    return MI_Save_Num;
}
///***********************************************
//** Function name:       Data_Migration
//** Descriptions:
//** input parameters:    无
//** output parameters:   无
//** Returned value:
//*************************************************/
//void Data_Migration(void)
//{
//    vu8 i = 0;
//    vu32 DtuDetail_len = 0;
//    vu32 DtuMajor_len = 0;
//    vu32 MIMajor_len = 0;
//    char *path = NULL;
//    path = mymalloc(PathLen);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
//    DtuDetail_len = Read_File_Length(DefaultDisk, path);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
//    DtuMajor_len = Read_File_Length(DefaultDisk, path);
//    memset(path, 0, PathLen);
//    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
//    MIMajor_len = Read_File_Length(DefaultDisk, path);
//    if(DtuMajor_len != 0)
//    {
//        memset(&Dtu3Major.PropertyMsg, 0, sizeof(Dtu3Major));
//        memset(&Dtu3Detail.PropertyMsg, 0, sizeof(Dtu3Detail));
//        System_DtuMajor_Read(&Dtu3Major);
//        System_DtuDetail_Read(&Dtu3Detail);
//        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
//        System_DtuDetail_Delete();
//        System_DtuMajor_Delete();
//    }
//    if(MIMajor_len != 0)
//    {
//        for(i = 0; i < PORT_LEN; i++)
//        {
//            memset(MIMajor[i].PropertyMsg, 0, sizeof(InverterMajor));
//        }
//        InverterMajor_Read(MIMajor);
//        System_MI_SN_Write((InverterMajor *)MIMajor);
//        InverterMajor_Delete();
//    }
//    myfree(path);
//}
///***********************************************
//** Function name:       MI_Program_Check
//** Descriptions:        并网保护校验
//** input parameters:    无
//** output parameters:   无
//** Returned value:      1 成功 0 失败
//*************************************************/
//u8 Grid_Program_Check(void)
//{
//    char *buffer = NULL;
//    vu8 temp = 0;
//    vu8 find_head = 0;
//    vu16 length = 0;
//    vu16 BufSize = 0;
//    vu16 i = 0, j, k = 0;
//    length = Grid_Profiles_List_Get_Length();
//    BufSize = length;
//    buffer = mymalloc(BufSize);
//    memset(buffer, 0, BufSize);
//    u16 temp_para;
//    u8 temp_string[150] = {0};
//    length = Grid_Profiles_List_Read_Length(buffer, 0, BufSize);
//    for(i = 0; i < length; i++)
//    {
//        //找文件头
//        if(strncmp(&buffer[i], "countrystd=", 11) == 0)
//        {
//            //找到头
//            find_head = 1;
//            break;
//        }
//    }
//    //没找到头
//    if(i == length)
//    {
//        temp = 0;
//    }
//    //有头
//    if(1 == find_head)
//    {
//        while(i < length)
//        {
//            i++;
//            if(buffer[i] == '=')
//            {
//                i++;
//                temp_para = 0;
//                for(j = 0; j < 5; j++)
//                {
//                    if(buffer[i] == '\r')
//                    {
//                        break;
//                    }
//                    if(buffer[i] == '\n')
//                    {
//                        break;
//                    }
//                    temp_para = temp_para * 10;
//                    temp_para = temp_para + (buffer[i] - 0x30);
//                    i++;
//                }
//                temp_string[2 * k] = temp_para >> 8;
//                temp_string[2 * k + 1] = temp_para;
//                k++;
//                if(k > 80)
//                {
//                    break;
//                }
//            }
//            //文件结束
//            if(strncmp(&buffer[i], "modbus_crc16=", 13) == 0)
//            {
//                i = i + 13;
//                temp_para = 0;
//                for(j = 0; j < 5; j++)
//                {
//                    if(buffer[i] == '\r')
//                    {
//                        break;
//                    }
//                    if(buffer[i] == '\n')
//                    {
//                        break;
//                    }
//                    temp_para = temp_para * 10;
//                    temp_para = temp_para + (buffer[i] - 0x30);
//                    i++;
//                }
//                break;
//            }
//        }
//        i = 2 * k;
//        j = UART_CRC16_Work(&temp_string[0], i);
//        // 校验通过
//        if(temp_para == j)
//        {
//            temp = 1;
//        }
//        else
//        {
//            temp = 0;
//        }
//    }
//    myfree(buffer);
//    return temp;
//}
/***********************************************
** Function name:       MI_Program_Check
** Descriptions:        MI升级程序校验
** input parameters:    无
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
u8 MI_Program_Check(void)
{
    char *buffer = NULL;
    volatile char Cache[MAX_ONE_LINE_LEN];
    vu8 counter;
    vu8 Oneline[MAX_ONE_LINE_LEN];
    vu8 model;
    vu8 linelen = 0;
    vu8 perused;
    vu8 FLASE_CRC = 0, OK_CRC = 1, flag_crc = 1;
    vu16 addr_up_crc = 0x8010;
    vu16 addr_down_crc = 0;
    vu16 addr_lose = 0;
    vu16 addr_len = 0;
    vu16 timeout = 0;
    vu16 crc = 0, CRC_ResultC = 0, CRC_FW = 0, x = 0;
    vu16 BufSize = 2048;
    vu32 i = 0, j = 0;
    vu32 offset = 0;
    vu32 length = 0;
    perused = my_mem_perused();
    length = MI_Program_Get_Length();

    if(length == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (u32)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 10))) / 32) * 32;

    if(length < BufSize)
    {
        BufSize = length;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    buffer = mymalloc(BufSize);

    if(buffer == NULL)
    {
        return 0;
    }

    memset(buffer, 0, BufSize);
    memset((char *)Cache, 0, MAX_ONE_LINE_LEN);
    memset((char *)Oneline, 0, MAX_ONE_LINE_LEN);
    length = 0;

    while(1)
    {
#ifndef DEBUG
        IWDG_Feed();
#endif

        if((length - i) <= 200)
        {
            offset += i;
            memset(buffer, 0, BufSize);
            length = MI_Program_Read_Length(buffer, offset, BufSize);
            i = 0;
        }

        while(i <= BufSize)
        {
            if(strncmp(&buffer[i], "\r\n:", 3) == 0)
            {
                timeout = 0;
                i = i + 3;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) || \
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset((char *)Oneline, 0, sizeof(Oneline));
                linelen = (u8)j;
                Ascii2Hex((u8 *)Cache, (u8 *)Oneline, (u8)j);

                if(flag_crc == 1)
                {
                    if((Oneline[0] == 0x02) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x10))
                    {
                        flag_crc = 2;
                        crc = line_crc((u8 *)Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    break;
                }
                else if(flag_crc == 2)
                {
                    if((Oneline[0] == 0x02) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x04))
                    {
                        flag_crc = 3;
                        crc = line_crc((u8 *)Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    break;
                }
                else if(flag_crc == 3)
                {
                    //三代微逆不判断第4行是否是程序入口地址 2020-01-06
                    //                    if((Oneline[0] == 0x04) && (Oneline[3] == 0x00))
                    //                    {
                    flag_crc = 4;
                    crc = line_crc((u8 *)Oneline, linelen);

                    if(Oneline[linelen / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    //                    }
                    //                    else
                    //                    {
                    //                        myfree(buffer);
                    //                        return FLASE_CRC;
                    //                    }
                    break;
                }
                else if(flag_crc == 4)
                {
                    flag_crc = 0;
                    CRC_FW = (u16)((Oneline[12] & 0xFFFF) << 8) + (u16)Oneline[13];

                    if((Oneline[0] == 0x18) && (Oneline[3] == 0x00))
                    {
                        crc = line_crc((u8 *)Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    if(model == 1)
                    {
                        CRC_ResultC = 0xFFFF;

                        for(counter = 4; counter < 13; counter = counter + 2) //
                        {
                            x = (u16)((Oneline[counter]) & 0x00FF) << 8;
                            x = x + Oneline[counter + 1];
                            CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                        }

                        if((CRC_ResultC >> 8) != Oneline[counter])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }

                        if((CRC_ResultC & 0x00FF) != Oneline[counter + 1])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else if(model == 2)
                    {
                        CRC_ResultC = 0xFFFF;

                        for(counter = 4; counter < 27; counter = counter + 2)
                        {
                            if(counter == 14)
                            {
                                counter = 16;
                            }

                            x = (u16)((Oneline[counter]) & 0x00FF) << 8;
                            x = x + Oneline[counter + 1];
                            CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                        }

                        if(CRC_ResultC >> 8 != Oneline[14])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }

                    if((CRC_ResultC & 0x00FF) != Oneline[15])
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    CRC_ResultC = 0xFFFF;
                    break;
                }

                if((Oneline[0] == 0x00) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x01) && (Oneline[4] == 0xFF))
                {
                    flag_crc = 1;

                    if(CRC_FW == CRC_ResultC)
                    {
                        myfree(buffer);
                        return OK_CRC;
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }
                }
                //去掉中间高位变化  20170920
                else if(strncmp(&buffer[i], "02000004", 8) == 0)
                {
                    i = i + 14;
                    break;
                }
                else
                {
                    addr_down_crc = (((u16)(Oneline[1]) & 0x00FF) << 8) + (u16)(Oneline[2]);
                    crc = line_crc((u8 *)Oneline, linelen);

                    if(Oneline[linelen / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    addr_lose = addr_down_crc - addr_up_crc;

                    for(counter = (u8)addr_lose; counter > addr_len; counter--)
                    {
                        x = 0xFFFF;
                        CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                    }

                    for(counter = 4; counter < ((Oneline[0] + 4)); counter = counter + 2)
                    {
                        if((Oneline[1] == 0xef) && (Oneline[2] == 0x50))
                        {
                            x = 1;
                        }

                        x = (u16)((Oneline[counter]) & 0x00FF) << 8;
                        x = x + Oneline[counter + 1];
                        CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                    }

                    addr_up_crc = ((u16)(Oneline[1] << 8) + (Oneline[2]));
                    addr_len = ((Oneline[0]) / 2);
                    break;
                }

                i = i + j;
            }
            else if(strncmp(&buffer[i], "\r\n\r\n:", 5) == 0)
            {
                i = i + 5;
                timeout = 0;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset((u8 *)Oneline, 0, sizeof(Oneline));
                linelen = (u8)j;
                Ascii2Hex((u8 *)Cache, (u8 *)Oneline, (u8)j);

                if((Oneline[0] == 0x06) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x11))
                {
                    //00010001表示250W/300W微逆程序，00110001表示500W/600W微逆程序  00210001表示1000W/1200W微逆程序
                    //                    if(((Oneline[4] == 0x00) && (Oneline[5] == 0x01) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01)) ||
                    //                            ((Oneline[4] == 0x10) && (Oneline[5] == 0x10) && (Oneline[6] == 0x00) && (Oneline[7] == 0x00)))
                    //                    {
                    //                        model = 1;
                    //                    }
                    //                    else if(((Oneline[4] == 0x00) && (Oneline[5] == 0x11) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01)) ||
                    //                            ((Oneline[4] == 0x10) && (Oneline[5] == 0x11) && (Oneline[6] == 0x00) && (Oneline[7] == 0x00)))
                    //                    {
                    //                        model = 2;
                    //                    }
                    //                    //1000w按照500w来校验
                    //                    else if(((Oneline[4] == 0x00) && (Oneline[5] == 0x21) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01)) ||
                    //                            ((Oneline[4] == 0x10) && (Oneline[5] == 0x12) && (Oneline[6] == 0x00) && (Oneline[7] == 0x00)))
                    //                    {
                    //                        model = 2;
                    //                    }
                    //dong 2020-07-02   除了//00010001表示250W/300W微逆程序 用校验1 其他的都用校验2
                    if((Oneline[4] == 0x00) && (Oneline[5] == 0x01) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01))
                    {
                        model = 1;
                    }
                    else
                    {
                        model = 2;
                    }

                    crc = line_crc((u8 *)Oneline, linelen);

                    if(Oneline[linelen / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }
                }
                else
                {
                    myfree(buffer);
                    return FLASE_CRC;
                }

                i = i + j;
                break;
            }
            else
            {
                i++;
            }
        }

        memset((char *)Cache, 0, sizeof(Cache));
        timeout++;

        if(timeout > 5000)
        {
            myfree(buffer);
            return FLASE_CRC;
        }
    }

    myfree(buffer);
}
#endif
/***********************************************
** Function name:       DTU_Program_Check
** Descriptions:        DTU升级程序校验
** input parameters:    硬件版本号
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
u8 DTU_Program_Check(u16 Hardware)
{
    vu8 hardware[2] = {0};
    char *buffer = NULL;
    volatile char Cache[MAX_ONE_LINE_LEN];
    vu8 CRC16Hi, CRC16Lo;
    vu8 temp_crc;
    vu8 SaveHi, SaveLo;
    vu8 x;
    vu8 *ptr;
    vu8 len = 0;
    vu8 cmd = 0;
    vu8 counter;
    vu8 Oneline[MAX_ONE_LINE_LEN];
    vu8 StartLine[14];
    vu8 linelen = 0;
    vu8 perused;
    vu16 LineNum = 0;
    vu16 timeout = 0;
    vu16 crc = 0, CRC_ResultC = 0, CRC_FW = 0;
    vu32 y;
    vu32 i = 0;
    vu8 j = 0;
    vu32 offset = 0;
    vu32 address = 0;
    vu32 length = 0;
    vu16 BufSize = 4096;
    vu32 time = 0;
    perused = my_mem_perused();
    length = DTU_Program_Get_Length();
    hardware[0] = (u8)(Hardware >> 8);
    hardware[1] = (u8)(Hardware);

    if(length == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (u32)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 10))) / 32) * 32;

    if(length < BufSize)
    {
        BufSize = length;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    CRC16Hi = 0xFF;
    CRC16Lo = 0xFF;
    buffer = mymalloc(BufSize);

    if(buffer == NULL)
    {
        return 0;
    }

    memset(buffer, 0, BufSize);
    length = 0;

    while(1)
    {
#ifndef Bootloader
#ifndef DEBUG
        IWDG_Feed();
#endif
#endif

        if((length - i) <= 100)
        {
            offset += i;
            memset(buffer, 0, BufSize);
            length = DTU_Program_Read_Length(buffer, offset, BufSize);
            i = 0;
        }

        while(i <= length)
        {
            //\r\n:            除第一行外的每行开始
            if(strncmp(&buffer[i], "\r\n:", 3) == 0)
            {
                timeout = 0;
                i = i + 3;
                LineNum ++;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset((char *)Oneline, 0, sizeof(Oneline));
                Ascii2Hex((u8 *)Cache, (u8 *)Oneline, j);
                crc = line_crc((u8 *)Oneline, j);

                if(Oneline[j / 2 - 1] != crc)
                {
                    myfree(buffer);
                    return 0;
                }

                if((Oneline[0] == 0x00) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x01) && (Oneline[4] == 0xFF))  //结尾行
                {
                    if(CRC_FW == CRC_ResultC)
                    {
                        myfree(buffer);
                        return 1;
                    }
                    else
                    {
                        myfree(buffer);
                        return 0;
                    }
                }

                if(LineNum == 2)
                {
                    //开始地址  32
                    StartLine[0] = Oneline[5];
                    StartLine[1] = Oneline[4];
                    StartLine[2] = Oneline[7];
                    StartLine[3] = Oneline[6];
                    //结束地址  32
                    StartLine[4] = Oneline[9];
                    StartLine[5] = Oneline[8];
                    StartLine[6] = Oneline[11];
                    StartLine[7] = Oneline[10];
                    //文件校验  16
                    StartLine[8] = Oneline[13];
                    StartLine[9] = Oneline[12];
                    //系列号    16
                    StartLine[10] = Oneline[15];
                    StartLine[11] = Oneline[14];
                    //自校验    16
                    StartLine[12] = Oneline[17];
                    StartLine[13] = Oneline[16];
                    CRC_ResultC = 0xFFFF;
                    CRC_ResultC = CalcCRC16((u16 *)StartLine, 12);

                    //系列号校验
                    if((hardware[0] == StartLine[10]) && (hardware[1] == StartLine[11]))
                    {
                        //自校验通过  则开始文件校验
                        if(CRC_ResultC == ((StartLine[12] << 8) + StartLine[13]))
                        {
                            CRC_FW = (u16)(StartLine[8] << 8) + (u16)StartLine[9];
                        }
                        else
                        {
                            myfree(buffer);
                            return 0;
                        }
                    }
                    //硬件版本不对
                    else
                    {
                        myfree(buffer);
                        return 0;
                    }
                }
                else if(LineNum >= 4)
                {
                    //长度
                    len = *(Oneline + 0);
                    //地址
                    address = (u32)((*(Oneline + 1)) << 8) + (u32)(*(Oneline + 2));
                    //命令
                    cmd = *(Oneline + 3);

                    //数据
                    if(cmd == 00)
                    {
                        ptr = Oneline + 4;

                        for(x = 0; x < len; x++)
                        {
                            CRC16Lo = CRC16Lo ^ (*(ptr + x));

                            for(y = 0; y < 8; y++)
                            {
                                SaveHi = CRC16Hi;
                                SaveLo = CRC16Lo;
                                //高位右移一位
                                CRC16Hi = CRC16Hi / 2;
                                //低位右移一位
                                CRC16Lo = CRC16Lo / 2;

                                //如果高位字节最后一位为1
                                if((SaveHi & 0x01) == 0x01)
                                {
                                    //则低位字节右移后前面补1
                                    CRC16Lo = CRC16Lo | 0x80;
                                }

                                //否则自动补0
                                if((SaveLo & 0x01) == 0x01)
                                {
                                    //如果LSB为1，则与多项式码进行异或
                                    CRC16Hi = CRC16Hi ^ 0xa0;
                                    CRC16Lo = CRC16Lo ^ 0x01;
                                }
                            }
                        }

                        CRC_ResultC = (u16)(CRC16Hi << 8 | CRC16Lo);
                    }
                }

                i = i + j;
                break;
            }
            //程序总开始
            else if(strncmp(&buffer[i], "\r\n\r\n:", 5) == 0)
            {
                timeout = 0;
                i = i + 5;
                LineNum ++;
                memset((char *)Cache, 0, sizeof(Cache));

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        //下载程序结束  没找到程序头
                        break;
                    }
                }

                memset((char *)Oneline, 0, sizeof(Oneline));
                Ascii2Hex((u8 *)Cache, (u8 *)Oneline, j);
                crc = line_crc((u8 *)Oneline, j);

                if(Oneline[j / 2 - 1] != crc)
                {
                    myfree(buffer);
                    return 0;
                }

                i = i + j;
                break;
            }
            else
            {
                i++;
            }
        }

        memset((char *)Cache, 0, sizeof(Cache));
        timeout++;

        if(timeout > 5000)
        {
            myfree(buffer);
            return 0;
        }
    }

    myfree(buffer);
}
#ifdef Bootloader
//hex文件转BIN文件中转
u8 packageone[(MAX_PRO_PACKAGE / 2)];
//hex头
u8 hex_start[5] = {'\r', '\n', '\r', '\n', ':'};
//hex头
u8 hex_start1[4] = {'\n', '\r', '\n', ':'};
//行头
u8 hex_pack_start[3] = {'\r', '\n', ':'};
//bin文件写入地址
u32 FlashWriteAddress;
//上一包写入地址
u32 FlashWr_Haddress;
//bin文件开始地址
//u32 USER_BIN_ADDRESS = FLASH_ADDR_BIN;
//去除hex文件头和尾 并校验
void IAP_WRdata_to_flash(u8 *dat, u32 startaddr, u32 endaddr, u32 offset)
{
    u8 len;
    u16 i;
    u8 cmd;
    u32 address;
    //长度
    len = *(dat + 0);
    //地址
    address = ((*(dat + 1)) << 8) + (*(dat + 2));
    //命令
    cmd = *(dat + 3);

    //数据
    if(cmd == 00)
    {
        //不加偏移量
        FlashWriteAddress = FlashWr_Haddress + address + offset;

        // 有效范围内
        if((FlashWriteAddress >= startaddr) && (FlashWriteAddress <= endaddr))
        {
            //写入
            FLASH_Write(FlashWriteAddress, (u32 *)(dat + 4), len);
        }
    }
    // 04是高位地址变化
    else if(cmd == 4)
    {
        address = ((*(dat + 4)) << 8) + (*(dat + 5));
        FlashWr_Haddress = (address << 16) & 0xFFFF0000;//高位地址发生变化
    }
    // 01是结束符
    else if(cmd == 1)
    {
        //程序结束
    }
}
//u8 ReadBuffer[25 * 1024 + 50] = {0}; //读出的hex文件 20K+520字节
// hex文件转BIN文件
/***********************************************
** Function name:
** Descriptions:        hex文件转bin文件
** input parameters:    startaddr bin文件写入地址
** output parameters:   无
** Returned value:      无
*************************************************/
u8 IAP_Myprogram(u32 startaddr, u32 endaddr, u32 offset)
{
    //找到头文件标志位
    u8 find = 0;
    u8 *ReadBuffer;
    u8 Oneline[MAX_PRO_PACKAGE] = {0};
    u32 i = 0;
    u32 j = 0;
    u32 read_len = 25 * 1024;
    //读取hex文件偏移量
    u32 read_offset = 0;
    s32 length = 0;
    ReadBuffer = mymalloc(read_len);
    memset(ReadBuffer, 0, read_len);
    //偏移量初始化为0
    read_offset = 0;
    //获取DTU长度
    length = DTU_Program_Get_Length();

    //获取长度失败
    if(length == 0)
    {
        myfree(ReadBuffer);
        return 0;
    }

    //读取头文件
    length = DTU_Program_Read_Length((char *)ReadBuffer, read_offset, MAX_PRO_PACKAGE * 2);
    i = 0;
    find = 0;

    //找头
    while(i < length)
    {
        if((strncmp((char *)&ReadBuffer[i], (char *)hex_start, 5) == 0) || (strncmp((char *)&ReadBuffer[i], (char *)hex_start1, 4) == 0)) //程序总开始
        {
            FLASH_ERASURE(startaddr, endaddr);

            if(strncmp((char *)&ReadBuffer[i], (char *)hex_start, 5) == 0)
            {
                i = i + 5;
            }
            else if(strncmp((char *)&ReadBuffer[i], (char *)hex_start1, 4) == 0)
            {
                i = i + 4;
            }

            memset(Oneline, 0, sizeof(Oneline));

            for(j = 0; j < MAX_PRO_PACKAGE; j++)
            {
                if(((ReadBuffer[i + j] <= '9') && (ReadBuffer[i + j] >= '0')) || ((ReadBuffer[i + j] <= 'F') && (ReadBuffer[i + j] >= 'A')) ||
                        ((ReadBuffer[i + j] <= 'f') && (ReadBuffer[i + j] >= 'a')))
                {
                    Oneline[j] = ReadBuffer[i + j];
                }
                else
                {
                    break;
                }
            }

            memset(packageone, 0, sizeof(packageone));
            ascii_to_hex(Oneline, packageone, j);

            //校验一包是否正确
            if(package_crc(packageone, (j / 2)) == 0)
            {
                //编程一行个数地址类型数据CRC
                IAP_WRdata_to_flash(&packageone[0], startaddr, endaddr, offset);
                //找到头
                find = 1;
                read_offset = read_offset + j + i;
                i = 0;
                length = 0;
                break;
            }
            else
            {
                myfree(ReadBuffer);
                return 0;
            }
        }
        else
        {
            i++;

            //没找到头
            if(i == length - 1)
            {
                myfree(ReadBuffer);
                return 0;
            }
        }
    }

    //找到文件头
    while(find == 1)
    {
        if(length - i < 100)
        {
            read_offset += i;
            memset((char *)ReadBuffer, 0, sizeof(ReadBuffer));
            length = DTU_Program_Read_Length((char *)ReadBuffer, read_offset, read_len);
            i = 0;
        }

        while(i < read_len)
        {
            //\r\n: 除第一行外的每行开始
            if(strncmp((char *)&ReadBuffer[i], (char *)hex_pack_start, 3) == 0)
            {
                //程序总结束
                if(strncmp((char *)&ReadBuffer[i + 2], ":00000001FF", 11) == 0)
                {
                    myfree(ReadBuffer);
                    return 1;
                }
                //中间数据
                else
                {
                    i = i + 3;

                    for(j = 0; j < MAX_PRO_PACKAGE; j++)
                    {
                        if(((ReadBuffer[i + j] <= '9') && (ReadBuffer[i + j] >= '0')) || ((ReadBuffer[i + j] <= 'F') && (ReadBuffer[i + j] >= 'A')) ||
                                ((ReadBuffer[i + j] <= 'f') && (ReadBuffer[i + j] >= 'a')))
                        {
                            Oneline[j] = ReadBuffer[i + j];
                        }
                        else
                        {
                            break;
                        }
                    }

                    memset(packageone, 0, sizeof(packageone));
                    ascii_to_hex(Oneline, packageone, j);

                    //校验一包是否正确
                    if(package_crc(packageone, (j / 2)) == 0)
                    {
                        //编程行
                        IAP_WRdata_to_flash(&packageone[0], startaddr, endaddr, offset);
                        i = i + j;
                        break;
                    }
                    else
                    {
                        myfree(ReadBuffer);
                        return 0;
                    }
                }
            }
            else
            {
                i++;
            }
        }
    }

    myfree(ReadBuffer);
    return 0;
}
#endif