#ifndef __LITTLE_H
#define __LITTLE_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#include "lfs.h"
/*open flags
//以只读方式打开文件
*LFS_O_RDONLY
//以只写方式打开文件
*LFS_O_WRONLY
//以读写方式打开文件
*LFS_O_RDWR
//如果文件不存在，则创建一个文件
*LFS_O_CREAT
// 如果文件已存在则失败
*LFS_O_EXCL
// 将现有文件截断为零大小
*LFS_O_TRUNC
// 每次写入时移动到文件末尾
*LFS_O_APPEND

*/
#define FS_TEST(x)  printf(x);

/* Open or create a file */
int lf_open(uint8_t DiskNum, lfs_file_t *file, const char *path, int flags);
/* Close an open file object */
int lf_close(uint8_t DiskNum, lfs_file_t *file);
/* Read data from a file */
int lf_read(uint8_t DiskNum, lfs_file_t *file, void *buffer, uint32_t size, uint32_t *br);
/* Write data to a file */
int lf_write(uint8_t DiskNum, lfs_file_t *file, const void *buffer, uint32_t size, uint32_t *bw);

int lf_sync(uint8_t DiskNum, lfs_file_t *file);
/* Move file pointer of a file object */
int lf_seek(uint8_t DiskNum, lfs_file_t *file, uint32_t off);

int lf_tell(uint8_t DiskNum, lfs_file_t *file);

int lf_seekend(uint8_t DiskNum, lfs_file_t *file);
uint32_t lf_fsize(uint8_t DiskNum, lfs_file_t *file);
/* Open a directory */
int lf_opendir(uint8_t DiskNum, lfs_dir_t *dir, const char *path);
/* Close an open directory */
int lf_closedir(uint8_t DiskNum, lfs_dir_t *dir);
/* Read a directory item */
int lf_readdir(uint8_t DiskNum, lfs_dir_t *dir, struct lfs_info *info);
/* Create a sub directory */
int lf_mkdir(uint8_t DiskNum, const char *path);
/* Delete an existing file or directory */
int lf_unlink(uint8_t DiskNum, const char *path);
/* Rename/Move a file or directory */
int lf_rename(uint8_t DiskNum, const char *oldpath, const char *newpath);
/* Get file status */
int lf_stat(uint8_t DiskNum, const char *path, struct lfs_info *info);
/* Mount a logical drive */
int lf_mount(uint8_t DiskNum);

int lf_format(uint8_t DiskNum);
/* Unmount a logical drive */
int lf_unmount(uint8_t DiskNum);

//打印所有文件
void lf_PrintfAllFiles(uint8_t DiskNum, char *path);
//删除目录下所有内容
void lf_DeleteAllFiles(uint8_t DiskNum, char *path);
//目录大小
void lf_DirectorySize(uint8_t DiskNum, char *path, uint32_t *Size, uint16_t *Num);
//剩余容量
void lf_LaveCapacity(uint8_t DiskNum, char *path, uint32_t *capacity, uint16_t *Num);

//罗列所有文件
void lf_ListAllFiles(uint8_t DiskNum, char *path, char *FileName[], uint16_t *Num, uint16_t Total);
void lf_ListDirFiles(uint8_t DiskNum, char *path, uint32_t *FileName, uint16_t *Num, uint16_t Total, uint8_t NameLength);
int lfs_test(void);
void lfs_test_demo(void);
uint32_t lf_Total_Capacity(uint8_t DiskNum);
void lf_ListDirs(u8 DiskNum, char *path, u32 *DirName, u16 *Num);
void lf_ListDirFilesChar(u8 DiskNum, char *path, char *Same_Buf, u16 Same_len, char **FileName, u16 *Num, u16 Total);
#endif
