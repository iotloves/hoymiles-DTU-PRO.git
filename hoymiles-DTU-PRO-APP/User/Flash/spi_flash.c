#include "spi_flash.h"
#ifdef DTU3PRO
#include "iwdg.h"
#else
#include "stm32_wdg.h"
#endif
/***********************************************
** Function name:       SPI_GPIO_Init
** Descriptions:        spi gpio 初始化
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
static void SPI_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    /* 使能 FLASH_SPI 及GPIO 时钟 */
    /*!< SPI_FLASH_SPI_CS_GPIO, SPI_FLASH_SPI_MOSI_GPIO,
    SPI_FLASH_SPI_MISO_GPIO,SPI_FLASH_SPI_SCK_GPIO 时钟使能 */
    FLASH_SPI_GPIO_INIT(FLASH_SPI_SCK_GPIO_CLK | FLASH_SPI_MISO_GPIO_CLK | FLASH_SPI_MOSI_GPIO_CLK | FLASH_CS_GPIO_CLK | FLASH_CS1_GPIO_CLK, ENABLE);
#ifdef DTU3PRO
    //设置引脚复用
    GPIO_PinAFConfig(FLASH_SPI_SCK_GPIO_PORT, FLASH_SPI_SCK_PINSOURCE, FLASH_SPI_SCK_AF);
    GPIO_PinAFConfig(FLASH_SPI_MISO_GPIO_PORT, FLASH_SPI_MISO_PINSOURCE, FLASH_SPI_MISO_AF);
    GPIO_PinAFConfig(FLASH_SPI_MOSI_GPIO_PORT, FLASH_SPI_MOSI_PINSOURCE, FLASH_SPI_MOSI_AF);
    /*!< 配置 SPI_FLASH_SPI 引脚: SCK */
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
#else
    /*!< 配置 SPI_FLASH_SPI 引脚: SCK */
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_SCK_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
#endif
    GPIO_Init(FLASH_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);
    /*!< 配置 SPI_FLASH_SPI 引脚: MISO */
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_MISO_PIN;
    GPIO_Init(FLASH_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);
    /*!< 配置 SPI_FLASH_SPI 引脚: MOSI */
    GPIO_InitStructure.GPIO_Pin = FLASH_SPI_MOSI_PIN;
    GPIO_Init(FLASH_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);
    /*!< 配置 SPI_FLASH_SPI 引脚: CS */
    GPIO_InitStructure.GPIO_Pin = FLASH_CS_PIN;
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
#endif
    GPIO_Init(FLASH_CS_GPIO_PORT, &GPIO_InitStructure);
    /*!< 配置 SPI_FLASH_SPI 引脚: CS */
    GPIO_InitStructure.GPIO_Pin = FLASH_CS1_PIN;
    GPIO_Init(FLASH_CS1_GPIO_PORT, &GPIO_InitStructure);
    /* 停止信号 FLASH: CS引脚高电平*/
    SPI_FLASH_CS_HIGH(FLASH_CS0);
    /* 停止信号 FLASH: CS引脚高电平*/
    SPI_FLASH_CS_HIGH(FLASH_CS1);
}
/***********************************************
** Function name:       SPI_FLASH_Init
** Descriptions:        SPI_FLASH初始化
** input parameters:    spi初始化
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_Init(void)
{
    SPI_InitTypeDef  SPI_InitStructure;
    /*!< SPI_FLASH_SPI 时钟使能 */
    FLASH_SPI_CLK_INIT(FLASH_SPI_CLK, ENABLE);
    SPI_GPIO_Init();
    /* FLASH_SPI 模式配置 */
    /*FLASH芯片 支持SPI模式0及模式3，据此设置CPOL CPHA*/
    /*设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工*/
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    /*设置SPI工作模式:设置为主SPI*/
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    /*设置SPI的数据大小:SPI发送接收8位帧结构*/
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    /*选择了串行时钟的稳态:时钟悬空高  1*/
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    /*数据捕获于第2个时钟沿           0*/
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    /*NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制*/
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    /*定义波特率预分频的值:波特率预分频值为16*/
#ifndef DTU3PRO
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
#else
    /*SPI降速，从21m-->10m*/
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;//SPI_BaudRatePrescaler_2;
    /*hzwang_20200424*/
#endif
    /*指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始*/
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    /*CRC值计算的多项式*/
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    /*根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器*/
    SPI_Init(FLASH_SPI, &SPI_InitStructure);
    /* 使能 FLASH_SPI  */
    SPI_Cmd(FLASH_SPI, ENABLE);
}
/***********************************************
** Function name:       SPI_FLASH_SectorErase
** Descriptions:        擦除FLASH扇区
** input parameters:    CS:Flash片选 SectorAddr:要擦除的扇区地址
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_SectorErase(u8 CS, u32 SectorAddr)
{
    IWDG_Feed();
    /* 发送FLASH写使能命令 */
    SPI_FLASH_WriteEnable(CS);
    SPI_FLASH_WaitForWriteEnd(CS);
    /* 擦除扇区 */
    /* 选择FLASH: CS低电平 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送扇区擦除指令*/
    SPI_ReadWriteByte(W25X_SectorErase);
    /*发送擦除扇区地址的高位*/
    SPI_ReadWriteByte((SectorAddr & 0xFF0000) >> 16);
    /* 发送擦除扇区地址的中位 */
    SPI_ReadWriteByte((SectorAddr & 0xFF00) >> 8);
    /* 发送擦除扇区地址的低位 */
    SPI_ReadWriteByte(SectorAddr & 0xFF);
    /* 停止信号 FLASH: CS 高电平 */
    SPI_FLASH_CS_HIGH(CS);
    /* 等待擦除完毕*/
    SPI_FLASH_WaitForWriteEnd(CS);
}

/***********************************************
** Function name:       SPI_FLASH_BulkErase
** Descriptions:        擦除FLASH扇区，整片擦除
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_BulkErase(u8 CS)
{
    IWDG_Feed();
    /* 发送FLASH写使能命令 */
    SPI_FLASH_WriteEnable(CS);
    /* 整块 Erase */
    /* 选择FLASH: CS低电平 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送整块擦除指令*/
    SPI_ReadWriteByte(W25X_ChipErase);
    /* 停止信号 FLASH: CS 高电平 */
    SPI_FLASH_CS_HIGH(CS);
    /* 等待擦除完毕*/
    SPI_FLASH_WaitForWriteEnd(CS);
}


/***********************************************
** Function name:       SPI_FLASH_PageWrite
** Descriptions:        对FLASH按页写入数据，调用本函数写入数据前需要先擦除扇区
** input parameters:    CS:Flash片选 pBuffer:要写入数据的指针 WriteAddr:写入地址 NumByteToWrite:写入数据长度，必须小于等于SPI_FLASH_PerWritePageSize
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_PageWrite(u8 CS, u8 *pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
    vu16 i;
    /* 发送FLASH写使能命令 */
    SPI_FLASH_WriteEnable(CS);
    /* 选择FLASH: CS低电平 */
    SPI_FLASH_CS_LOW(CS);
    /* 写页写指令*/
    SPI_ReadWriteByte(W25X_PageProgram);
    /*发送写地址的高位*/
    SPI_ReadWriteByte((WriteAddr & 0xFF0000) >> 16);
    /*发送写地址的中位*/
    SPI_ReadWriteByte((WriteAddr & 0xFF00) >> 8);
    /*发送写地址的低位*/
    SPI_ReadWriteByte(WriteAddr & 0xFF);

    for(i = 0; i < NumByteToWrite; i++)
    {
        //循环写数
        SPI_ReadWriteByte(pBuffer[i]);
    }

    /* 停止信号 FLASH: CS 高电平 */
    SPI_FLASH_CS_HIGH(CS);
    /* 等待写入完毕*/
    SPI_FLASH_WaitForWriteEnd(CS);
}

/***********************************************
** Function name:       SPI_FLASH_BufferWrite
** Descriptions:        对FLASH写入数据，调用本函数写入数据前需要先擦除扇区
** input parameters:    CS:Flash片选 pBuffer:要写入数据的指针 WriteAddr:写入地址 NumByteToWrite:写入数据长度
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_BufferWrite(u8 CS, u8 *pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
    vu16 pageremain;
    //单页剩余的字节数
    pageremain = 256 - WriteAddr % 256;

    if(NumByteToWrite <= pageremain)
    {
        //不大于256个字节
        pageremain = NumByteToWrite;
    }

    while(1)
    {
        IWDG_Feed();
        SPI_FLASH_PageWrite(CS, pBuffer, WriteAddr, pageremain);

        if(NumByteToWrite == pageremain)
        {
            //写入结束了
            break;
        }
        else //NumByteToWrite>pageremain
        {
            pBuffer += pageremain;
            WriteAddr += pageremain;
            //减去已经写入了的字节数
            NumByteToWrite -= pageremain;

            if(NumByteToWrite > 256)
            {
                //一次可以写入256个字节
                pageremain = 256;
            }
            else
            {
                //不够256个字节了
                pageremain = NumByteToWrite;
            }
        }
    }
}
/***********************************************
** Function name:       SPI_FLASH_BufferRead
** Descriptions:        读取FLASH数据
** input parameters:    CS:Flash片选 *pBuffer:存储读出数据的指针  ReadAddr:读取地址  NumByteToRead:读取数据长度
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_BufferRead(u8 CS, u8 *pBuffer, u32 ReadAddr, u16 NumByteToRead)
{
    IWDG_Feed();
    /* 选择FLASH: CS低电平 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送 读 指令 */
    SPI_ReadWriteByte(W25X_ReadData);
    /* 发送 读 地址高位 */
    SPI_ReadWriteByte((ReadAddr & 0xFF0000) >> 16);
    /* 发送 读 地址中位 */
    SPI_ReadWriteByte((ReadAddr & 0xFF00) >> 8);
    /* 发送 读 地址低位 */
    SPI_ReadWriteByte(ReadAddr & 0xFF);

    /* 读取数据 */
    while(NumByteToRead--)
    {
        /* 读取一个字节*/
        *pBuffer = SPI_ReadWriteByte(Dummy_Byte);
        /* 指向下一个字节缓冲区 */
        pBuffer++;
    }

    /* 停止信号 FLASH: CS 高电平 */
    SPI_FLASH_CS_HIGH(CS);
}

/***********************************************
** Function name:      SPI_FLASH_ReadID
** Descriptions:       读取FLASH ID
** input parameters:   CS:Flash片选
** output parameters:  无
** Returned value:     FLASH ID
*************************************************/
u32 SPI_FLASH_ReadID(u8 CS)
{
    vu32 Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;
    /* 开始通讯:CS低电平 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送JEDEC指令，读取ID */
    SPI_ReadWriteByte(W25X_JedecDeviceID);
    /* 读取一个字节数据 */
    Temp0 = SPI_ReadWriteByte(Dummy_Byte);
    /* 读取一个字节数据 */
    Temp1 = SPI_ReadWriteByte(Dummy_Byte);
    /* 读取一个字节数据 */
    Temp2 = SPI_ReadWriteByte(Dummy_Byte);
    /* 停止通讯:CS高电平 */
    SPI_FLASH_CS_HIGH(CS);
    /*把数据组合起来，作为函数的返回值*/
    Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;
    return Temp;
}
/***********************************************
** Function name:       SPI_FLASH_ReadDeviceID
** Descriptions:        读取FLASH Device ID
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      FLASH Device ID
*************************************************/
u32 SPI_FLASH_ReadDeviceID(u8 CS)
{
    vu32 Temp = 0;
    /* Select the FLASH: Chip Select low */
    SPI_FLASH_CS_LOW(CS);
    /* Send "RDID " instruction */
    SPI_ReadWriteByte(W25X_DeviceID);
    SPI_ReadWriteByte(Dummy_Byte);
    SPI_ReadWriteByte(Dummy_Byte);
    SPI_ReadWriteByte(Dummy_Byte);
    /* Read a byte from the FLASH */
    Temp = SPI_ReadWriteByte(Dummy_Byte);
    /* Deselect the FLASH: Chip Select high */
    SPI_FLASH_CS_HIGH(CS);
    return Temp;
}

/***********************************************
** Function name:       SPI_FLASH_Check
** Descriptions:        Flash校验
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      FLASH Device ID
*************************************************/
u8 SPI_FLASH_Check(u8 CS)
{
    vu32 Device_ID = 0;
    Device_ID = SPI_FLASH_ReadID(CS);

    if((Device_ID == W25X16) || (Device_ID == W25Q16) || (Device_ID == W25Q64) || (Device_ID == W25Q128))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*******************************************************************************
* Function Name  : SPI_FLASH_StartReadSequence
* Description    : Initiates a read data byte (READ) sequence from the Flash.
*                  This is done by driving the /CS line low to select the device,
*                  then the READ instruction is transmitted followed by 3 bytes
*                  address. This function exit and keep the /CS line low, so the
*                  Flash still being selected. With this technique the whole
*                  content of the Flash is read with a single READ instruction.
* Input          : - ReadAddr : FLASH's internal address to read from.
* Output         : None
* Return         : None
*******************************************************************************/
void SPI_FLASH_StartReadSequence(u8 CS, u32 ReadAddr)
{
    /* Select the FLASH: Chip Select low */
    SPI_FLASH_CS_LOW(CS);
    /* Send "Read from Memory " instruction */
    SPI_ReadWriteByte(W25X_ReadData);
    /* Send the 24-bit address of the address to read from -----------------------*/
    /* Send ReadAddr high nibble address byte */
    SPI_ReadWriteByte((ReadAddr & 0xFF0000) >> 16);
    /* Send ReadAddr medium nibble address byte */
    SPI_ReadWriteByte((ReadAddr & 0xFF00) >> 8);
    /* Send ReadAddr low nibble address byte */
    SPI_ReadWriteByte(ReadAddr & 0xFF);
}

/***********************************************
** Function name:       SPI_ReadWriteByte
** Descriptions:        使用SPI发送一个字节的数据
** input parameters:    byte:要发送的数据
** output parameters:   无
** Returned value:      返回接收到的数据
*************************************************/
u8 SPI_ReadWriteByte(u8 byte)
{
    vu16 retry = 0;

    /* 等待发送缓冲区为空，TXE事件 */
    while(SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_TXE) == RESET)
    {
        retry++;

        if(retry > 20000)
        {
            return 0;
        }
    }

    /* 写入数据寄存器，把要写入的数据写入发送缓冲区 */
    SPI_I2S_SendData(FLASH_SPI, byte);
    retry = 0;

    /* 等待接收缓冲区非空，RXNE事件 */
    while(SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_RXNE) == RESET)
    {
        retry++;

        if(retry > 20000)
        {
            return 0;
        }
    }

    /* 读取数据寄存器，获取接收缓冲区数据 */
    return (u8)SPI_I2S_ReceiveData(FLASH_SPI);
}

/*******************************************************************************
* Function Name  : SPI_FLASH_SendHalfWord
* Description    : Sends a Half Word through the SPI interface and return the
*                  Half Word received from the SPI bus.
* Input          : Half Word : Half Word to send.
* Output         : None
* Return         : The value of the received Half Word.
*******************************************************************************/
u16 SPI_FLASH_SendHalfWord(u16 HalfWord)
{
    vu16 retry = 0;

    /* Loop while DR register in not emplty */
    while(SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_TXE) == RESET)
    {
        retry++;

        if(retry > 20000)
        {
            return 0;
        }
    }

    /* Send Half Word through the FLASH_SPI peripheral */
    SPI_I2S_SendData(FLASH_SPI, HalfWord);
    retry = 0;

    /* Wait to receive a Half Word */
    while(SPI_I2S_GetFlagStatus(FLASH_SPI, SPI_I2S_FLAG_RXNE) == RESET)
    {
        retry++;

        if(retry > 20000)
        {
            return 0;
        }
    }

    /* Return the Half Word read from the SPI bus */
    return SPI_I2S_ReceiveData(FLASH_SPI);
}

/***********************************************
** Function name:       SPI_FLASH_WriteEnable
** Descriptions:        向FLASH发送 写使能 命令
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_FLASH_WriteEnable(u8 CS)
{
    /* 通讯开始:CS低 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送写使能命令*/
    SPI_ReadWriteByte(W25X_WriteEnable);
    /*通讯结束:CS高 */
    SPI_FLASH_CS_HIGH(CS);
}
/***********************************************
** Function name:       W25QXX_ReadSR
** Descriptions:        读FLASH状态
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      FLASH状态
*************************************************/
u8 W25QXX_ReadSR(u8 CS)
{
    vu8 byte = 0;
    /*使能器件*/
    SPI_FLASH_CS_LOW(CS);
    /*发送读取状态寄存器命令*/
    SPI_ReadWriteByte(W25X_ReadStatusReg);
    /*读取一个字节*/
    byte = SPI_ReadWriteByte(0Xff);
    /*取消片选*/
    SPI_FLASH_CS_HIGH(CS);
    return byte;
}
/***********************************************
** Function name:       SPI_FLASH_WaitForWriteEnd
** Descriptions:        等待WIP(BUSY)标志被置0，即等待到FLASH内部数据写入完毕
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      无
*************************************************/
u16 SPI_FLASH_WaitForWriteEnd(u8 CS)
{
    vu16 retry = 0;

    /*等待BUSY位清空*/
    while((W25QXX_ReadSR(CS) & 0x01) == 0x01)
    {
        retry++;

        if(retry > 20000)
        {
            return 0;
        }
    }

    return 1;
}

/***********************************************
** Function name:       SPI_Flash_PowerDown
** Descriptions:        进入掉电模式
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_Flash_PowerDown(u8 CS)
{
    /* 选择 FLASH: CS 低 */
    SPI_FLASH_CS_LOW(CS);
    /* 发送 掉电 命令 */
    SPI_ReadWriteByte(W25X_PowerDown);
    /* 停止信号  FLASH: CS 高 */
    SPI_FLASH_CS_HIGH(CS);
}
/***********************************************
** Function name:       SPI_Flash_WAKEUP
** Descriptions:        唤醒
** input parameters:    CS:Flash片选
** output parameters:   无
** Returned value:      无
*************************************************/
void SPI_Flash_WAKEUP(u8 CS)
{
    /*选择 FLASH: CS 低 */
    SPI_FLASH_CS_LOW(CS);
    /* 发上 上电 命令 */
    SPI_ReadWriteByte(W25X_ReleasePowerDown);
    /* 停止信号 FLASH: CS 高 */
    SPI_FLASH_CS_HIGH(CS);                   //等待TRES1
}

/***********************************************
** Function name:       EEPROM_Erase
** Descriptions:        擦除EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_Erase(u8 Site)
{
    switch(Site)
    {
        case EEPROM_Factory_Def_Site:
            SPI_FLASH_SectorErase(EEPROM_Disk, EEPROM_Factory_Def_Addr);
            break;

        case EEPROM_Factory_New_Site:
            SPI_FLASH_SectorErase(EEPROM_Disk, EEPROM_Factory_New_Addr);
            break;
    }
}
/***********************************************
** Function name:       EEPROM_BufferWrite
** Descriptions:        写EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_BufferWrite(u8 Site, u8 *pBuffer, u16 NumByteToWrite)
{
    switch(Site)
    {
        case EEPROM_Factory_Def_Site:
            SPI_FLASH_BufferWrite(EEPROM_Disk, pBuffer, EEPROM_Factory_Def_Addr, NumByteToWrite);
            break;

        case EEPROM_Factory_New_Site:
            SPI_FLASH_BufferWrite(EEPROM_Disk, pBuffer, EEPROM_Factory_New_Addr, NumByteToWrite);
            break;
    }
}
/***********************************************
** Function name:       EEPROM_BufferRead
** Descriptions:        读EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_BufferRead(u8 Site, u8 *pBuffer, u16 NumByteToRead)
{
    switch(Site)
    {
        case EEPROM_Factory_Def_Site:
            SPI_FLASH_BufferRead(EEPROM_Disk, pBuffer, EEPROM_Factory_Def_Addr, NumByteToRead);
            break;

        case EEPROM_Factory_New_Site:
            SPI_FLASH_BufferRead(EEPROM_Disk, pBuffer, EEPROM_Factory_New_Addr, NumByteToRead);
            break;
    }
}
/***********************************************
** Function name:       EEPROM_Erase_Site
** Descriptions:        擦除EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_Erase_Site(u8 Site, u16 Addr)
{
    switch(Site)
    {
        case EEPROM_DTU_Site:
            SPI_FLASH_SectorErase(EEPROM_Disk, EEPROM_DTU_Addr + Addr);
            break;

        case EEPROM_MI_Site:
            SPI_FLASH_SectorErase(EEPROM_Disk, EEPROM_MI_Addr + Addr);
            break;
    }
}

/***********************************************
** Function name:       EEPROM_BufferWrite_Site
** Descriptions:        写EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_BufferWrite_Site(u8 Site, u8 *pBuffer, u16 Addr, u16 NumByteToWrite)
{
    switch(Site)
    {
        case EEPROM_DTU_Site:
            SPI_FLASH_BufferWrite(EEPROM_Disk, pBuffer, EEPROM_DTU_Addr + Addr, NumByteToWrite);
            break;

        case EEPROM_MI_Site:
            SPI_FLASH_BufferWrite(EEPROM_Disk, pBuffer, EEPROM_MI_Addr + Addr, NumByteToWrite);
            break;
    }
}
/***********************************************
** Function name:       EEPROM_BufferRead_Site
** Descriptions:        读EEPROM
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
void EEPROM_BufferRead_Site(u8 Site, u8 *pBuffer, u16 Addr, u16 NumByteToRead)
{
    switch(Site)
    {
        case EEPROM_DTU_Site:
            SPI_FLASH_BufferRead(EEPROM_Disk, pBuffer, EEPROM_DTU_Addr + Addr, NumByteToRead);
            break;

        case EEPROM_MI_Site:
            SPI_FLASH_BufferRead(EEPROM_Disk, pBuffer, EEPROM_MI_Addr + Addr, NumByteToRead);
            break;
    }
}

/*********************************************END OF FILE**********************/
