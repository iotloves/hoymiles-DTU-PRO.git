#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

#ifdef DTU3PRO
#include "stm32f4xx.h"
//#include "stm32f4xx_dma.h"
#ifdef DEBUG
#include "stdio.h"
#endif
/**************SPI接口定义-开头***************/
#define FLASH_SPI                           SPI2
#define FLASH_SPI_CLK                       RCC_APB1Periph_SPI2
#define FLASH_SPI_CLK_INIT                  RCC_APB1PeriphClockCmd

#define FLASH_SPI_GPIO_INIT                 RCC_AHB1PeriphClockCmd
#define FLASH_SPI_SCK_PIN                   GPIO_Pin_10
#define FLASH_SPI_SCK_GPIO_PORT             GPIOB
#define FLASH_SPI_SCK_GPIO_CLK              RCC_AHB1Periph_GPIOB
#define FLASH_SPI_SCK_PINSOURCE             GPIO_PinSource10
#define FLASH_SPI_SCK_AF                    GPIO_AF_SPI2

#define FLASH_SPI_MISO_PIN                  GPIO_Pin_2
#define FLASH_SPI_MISO_GPIO_PORT            GPIOC
#define FLASH_SPI_MISO_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define FLASH_SPI_MISO_PINSOURCE            GPIO_PinSource2
#define FLASH_SPI_MISO_AF                   GPIO_AF_SPI2

#define FLASH_SPI_MOSI_PIN                  GPIO_Pin_3
#define FLASH_SPI_MOSI_GPIO_PORT            GPIOC
#define FLASH_SPI_MOSI_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define FLASH_SPI_MOSI_PINSOURCE            GPIO_PinSource3
#define FLASH_SPI_MOSI_AF                   GPIO_AF_SPI2

#define FLASH_CS_PIN                        GPIO_Pin_6
#define FLASH_CS_GPIO_PORT                  GPIOE
#define FLASH_CS_GPIO_CLK                   RCC_AHB1Periph_GPIOE

#define FLASH_CS1_PIN                       GPIO_Pin_0
#define FLASH_CS1_GPIO_PORT                 GPIOE
#define FLASH_CS1_GPIO_CLK                  RCC_AHB1Periph_GPIOE

#define SPIx_DMA                            DMA1
#define SPIx_DMA_CLK                        RCC_AHB1Periph_DMA1
#define SPIx_TX_DMA_CHANNEL                 DMA_Channel_0
#define SPIx_TX_DMA_STREAM                  DMA1_Stream4
#define SPIx_TX_DMA_FLAG_TCIF               DMA_FLAG_TCIF4
#define SPIx_RX_DMA_CHANNEL                 DMA_Channel_0
#define SPIx_RX_DMA_STREAM                  DMA1_Stream3
#define SPIx_RX_DMA_FLAG_TCIF               DMA_FLAG_TCIF3

#define SPI_FLASH_CS_LOW(x)                 {if(x==0){FLASH_CS_GPIO_PORT->BSRRH=FLASH_CS_PIN;}else{FLASH_CS1_GPIO_PORT->BSRRH=FLASH_CS1_PIN;}}
#define SPI_FLASH_CS_HIGH(x)                {if(x==0){FLASH_CS_GPIO_PORT->BSRRL=FLASH_CS_PIN;}else{FLASH_CS1_GPIO_PORT->BSRRL=FLASH_CS1_PIN;}}

/**************SPI接口定义-结尾***************/
#else
#include "stm32f10x.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#ifdef DEBUG
#include "stdio.h"
#endif
/**************SPI接口定义-开头***************/
#define FLASH_SPI                           SPI1
#define FLASH_SPI_CLK                       RCC_APB2Periph_SPI1
#define FLASH_SPI_CLK_INIT                  RCC_APB2PeriphClockCmd

#define FLASH_SPI_GPIO_INIT                 RCC_APB2PeriphClockCmd
#define FLASH_SPI_SCK_PIN                   GPIO_Pin_5
#define FLASH_SPI_SCK_GPIO_PORT             GPIOA
#define FLASH_SPI_SCK_GPIO_CLK              RCC_APB2Periph_GPIOA
#define FLASH_SPI_SCK_PINSOURCE             GPIO_PinSource5
#define FLASH_SPI_SCK_AF                    GPIO_AF_SPI1

#define FLASH_SPI_MISO_PIN                  GPIO_Pin_6
#define FLASH_SPI_MISO_GPIO_PORT            GPIOA
#define FLASH_SPI_MISO_GPIO_CLK             RCC_APB2Periph_GPIOA
#define FLASH_SPI_MISO_PINSOURCE            GPIO_PinSource6
#define FLASH_SPI_MISO_AF                   GPIO_AF_SPI1

#define FLASH_SPI_MOSI_PIN                  GPIO_Pin_7
#define FLASH_SPI_MOSI_GPIO_PORT            GPIOA
#define FLASH_SPI_MOSI_GPIO_CLK             RCC_APB2Periph_GPIOA
#define FLASH_SPI_MOSI_PINSOURCE            GPIO_PinSource7
#define FLASH_SPI_MOSI_AF                   GPIO_AF_SPI1

#define FLASH_CS_PIN                        GPIO_Pin_4
#define FLASH_CS_GPIO_PORT                  GPIOA
#define FLASH_CS_GPIO_CLK                   RCC_APB2Periph_GPIOA

#define FLASH_CS1_PIN                       GPIO_Pin_4
#define FLASH_CS1_GPIO_PORT                 GPIOA
#define FLASH_CS1_GPIO_CLK                  RCC_APB2Periph_GPIOA

#define SPI_FLASH_CS_LOW(x)                 {if(x==0){GPIO_ResetBits(FLASH_CS_GPIO_PORT,FLASH_CS_PIN);}else{GPIO_ResetBits(FLASH_CS1_GPIO_PORT,FLASH_CS1_PIN);}}
#define SPI_FLASH_CS_HIGH(x)                {if(x==0){GPIO_SetBits(FLASH_CS_GPIO_PORT,FLASH_CS_PIN);}else{GPIO_SetBits(FLASH_CS1_GPIO_PORT,FLASH_CS1_PIN);}}
/**************SPI接口定义-结尾***************/
#endif

/* Private typedef -----------------------------------------------------------*/
#define  W25X16                         0xEF3015     //W25X16
#define  W25Q16                         0xEF4015     //W25Q16
#define  W25Q64                         0XEF4017     //W25Q64
#define  W25Q128                        0XEF4018     //W25Q128
#define  W25Q128                        0XEF4018     //W25Q128

#define EEPROM_Factory_Def_Site         0
#define EEPROM_Factory_Def_Addr         0xFFF000

#define EEPROM_Factory_New_Site         1
#define EEPROM_Factory_New_Addr         0xFFA000

#define EEPROM_DTU_Site                 0
#define EEPROM_DTU_Addr                 0xFFB000
#define EEPROM_DTU_SECTORS_MUM          2

#define EEPROM_MI_Site                  1
#define EEPROM_MI_Addr                  0xFFD000
#define EEPROM_MI_SECTORS_NUM           2

#define EEPROM_Disk                     0
#define SPI_FLASH_PageSize              256
#define SPI_FLASH_PerWritePageSize      256

#define FLASH_CS0  0
#define FLASH_CS1  1


#define BUFFERSIZE                      2048
/* Private define ------------------------------------------------------------*/
/*命令定义-开头*******************************/
#define W25X_WriteEnable                0x06
#define W25X_WriteDisable               0x04
#define W25X_ReadStatusReg              0x05
#define W25X_WriteStatusReg             0x01
#define W25X_ReadData                   0x03
#define W25X_FastReadData               0x0B
#define W25X_FastReadDual               0x3B
#define W25X_PageProgram                0x02
#define W25X_BlockErase                 0xD8
#define W25X_SectorErase                0x20
#define W25X_ChipErase                  0xC7
#define W25X_PowerDown                  0xB9
#define W25X_ReleasePowerDown           0xAB
#define W25X_DeviceID                   0xAB
#define W25X_ManufactDeviceID           0x90
#define W25X_JedecDeviceID              0x9F

#define WIP_Flag                        0x01  /* Write In Progress (WIP) flag */
#define Dummy_Byte                      0xFF

/*等待超时时间*/
#define SPIT_FLAG_TIMEOUT         ((u32)0x1000)
#define SPIT_LONG_TIMEOUT         ((u32)(10 * SPIT_FLAG_TIMEOUT))

/*信息输出*/
#ifdef DEBUG
#define FLASH_DEBUG_ON         1
#else
#define FLASH_DEBUG_ON         0
#endif
#ifdef DEBUG
#define FLASH_INFO(fmt,arg...)           printf("<<-FLASH-INFO->> "fmt"\n",##arg)
#define FLASH_ERROR(fmt,arg...)          printf("<<-FLASH-ERROR->> "fmt"\n",##arg)
#define FLASH_DEBUG(fmt,arg...)          do{\
        if(FLASH_DEBUG_ON)\
            printf("<<-FLASH-DEBUG->> [%d]"fmt"\n",__LINE__, ##arg);\
    }while(0)
#else
#define FLASH_INFO(fmt,arg ...)        do{}while(0)
#define FLASH_ERROR(fmt,arg ...)       do{}while(0)
#define FLASH_DEBUG(fmt,arg ...)       do{}while(0)
#endif


void SPI_FLASH_Init(void);
void SPI_FLASH_SectorErase(u8 CS, u32 SectorAddr);
void SPI_FLASH_BulkErase(u8 CS);
void SPI_FLASH_PageWrite(u8 CS, u8 *pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferWrite(u8 CS, u8 *pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SPI_FLASH_BufferRead(u8 CS, u8 *pBuffer, u32 ReadAddr, u16 NumByteToRead);
u32 SPI_FLASH_ReadID(u8 CS);
u32 SPI_FLASH_ReadDeviceID(u8 CS);
void SPI_FLASH_StartReadSequence(u8 CS, u32 ReadAddr);
void SPI_Flash_PowerDown(u8 CS);
void SPI_Flash_WAKEUP(u8 CS);


u8 SPI_ReadWriteByte(u8 byte);
u16 SPI_FLASH_SendHalfWord(u16 HalfWord);
void SPI_FLASH_WriteEnable(u8 CS);
u16 SPI_FLASH_WaitForWriteEnd(u8 CS);
u8 SPI_FLASH_Check(u8 CS);

void EEPROM_Erase(u8 Site);
void EEPROM_BufferWrite(u8 Site, u8 *pBuffer, u16 NumByteToWrite);
void EEPROM_BufferRead(u8 Site, u8 *pBuffer, u16 NumByteToRead);

void EEPROM_Erase_Site(u8 Site, u16 Addr);
void EEPROM_BufferWrite_Site(u8 Site, u8 *pBuffer, u16 Addr, u16 NumByteToWrite);
void EEPROM_BufferRead_Site(u8 Site, u8 *pBuffer, u16 Addr, u16 NumByteToRead);
#endif /* __SPI_FLASH_H */

