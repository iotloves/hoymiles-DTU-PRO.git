#ifndef __USART_GPRS_H
#define __USART_GPRS_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "led.h"
#define GPRS_SendDataNumMax             20
#define GPRS_SendAtNumMax               8
#define GPRS_ConnectNumMax              10
#define GPRS_ConnectGPRSNumMax          120


#define GPRS_USARTx                     USART6
#define GPRS_USART_CLK                  RCC_APB2Periph_USART6
#define GPRS_USART_APBxClkCmd           RCC_APB2PeriphClockCmd
#define GPRS_USART_BAUDRATE             38400

//USART GPIO 引脚宏定义
#define GPRS_USART_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define GPRS_USART_GPIO_APBxClkCmd      RCC_AHB1PeriphClockCmd

#define GPRS_USART_TX_GPIO_PORT         GPIOC
#define GPRS_USART_TX_GPIO_PIN          GPIO_Pin_6
#define GPRS_USART_TX_GPIO_AF           GPIO_AF_USART6
#define GPRS_USART_TX_SOURCE            GPIO_PinSource6
#define GPRS_USART_RX_GPIO_PORT         GPIOC
#define GPRS_USART_RX_GPIO_PIN          GPIO_Pin_7
#define GPRS_USART_RX_GPIO_AF           GPIO_AF_USART6
#define GPRS_USART_RX_SOURCE            GPIO_PinSource7
#define GPRS_USART_IRQ                  USART6_IRQn
#define GPRS_USART_IRQHandler           USART6_IRQHandler

#define GPRS_RCCPeriphClockCmd          RCC_AHB1PeriphClockCmd
//GPRS Power
#define GPRS_GPIO_PORT                  GPIOE                           /* GPIO端口 */
#define GPRS_GPIO_CLK                   RCC_AHB1Periph_GPIOE            /* GPIO端口时钟 */
#define GPRS_GPIO_PIN                   GPIO_Pin_14                      /* 连接到SCL时钟线的GPIO */

//GPRS PWR_KEY
#define GPRS_PWR_GPIO_PORT              GPIOE                           /* GPIO端口 */
#define GPRS_PWR_GPIO_CLK               RCC_AHB1Periph_GPIOE            /* GPIO端口时钟 */
#define GPRS_PWR_GPIO_PIN               GPIO_Pin_15                     /* 连接到SCL时钟线的GPIO */

#define GPRS_DMA_Irqn                   DMA2_Stream1_IRQn
#define GPRS_DMA_IRQHandler             DMA2_Stream1_IRQHandler
#else
#include "stm32f10x.h"
#include "define.h"
#include "string.h"
#include "stm32f10x_dma.h"
#include "usart_debug.h"



#define  GPRS_RCCPeriphClockCmd         RCC_APB2PeriphClockCmd
#define  GPRS_GPIO_RCC                  RCC_APB2Periph_GPIOC
#define  GPRS_GPIO_AFIO                 RCC_APB2Periph_AFIO
#define  GPRS_USARTx                    UART4
#define  GPRS_USART_CLK                 RCC_APB1Periph_UART4
#define  GPRS_USART_APBxClkCmd          RCC_APB1PeriphClockCmd
#define  GPRS_USART_BAUDRATE            38400

// USART GPIO 引脚宏定义
#define  GPRS_USART_GPIO_CLK            RCC_APB2Periph_GPIOC
#define  GPRS_USART_GPIO_APBxClkCmd     RCC_APB2PeriphClockCmd

#define  GPRS_USART_TX_GPIO_PORT        GPIOC
#define  GPRS_USART_TX_GPIO_PIN         GPIO_Pin_10
#define  GPRS_USART_RX_GPIO_PORT        GPIOC
#define  GPRS_USART_RX_GPIO_PIN         GPIO_Pin_11

#define  GPRS_USART_IRQ                 UART4_IRQn
#define  GPRS_USART_IRQHandler          UART4_IRQHandler


//GPRS Power
#define GPRS_GPIO_PORT                  GPIOC                          /* GPIO端口 */
#define GPRS_GPIO_CLK                   RCC_APB2Periph_GPIOC        /* GPIO端口时钟 */
#define GPRS_GPIO_PIN                   GPIO_Pin_9                    /* 连接到SCL时钟线的GPIO */

//GPRS PWR_KEY
#define GPRS_PWR_GPIO_PORT              GPIOA                          /* GPIO端口 */
#define GPRS_PWR_GPIO_CLK               RCC_APB2Periph_GPIOA        /* GPIO端口时钟 */
#define GPRS_PWR_GPIO_PIN               GPIO_Pin_0                    /* 连接到SCL时钟线的GPIO */

#define GPRS_DMA_Irqn                   DMA2_Channel3_IRQn
#define GPRS_DMA_IRQHandler             DMA2_Channel3_IRQHandler
#endif

//#define GPRS_USART_LEN  520  // 收发数组长度
#ifdef DTU3PRO
#define GPRS_POWER_ON               digitalLo(GPRS_GPIO_PORT,GPRS_GPIO_PIN)    //GPRS上电
#define GPRS_POWER_OFF              digitalHi(GPRS_GPIO_PORT,GPRS_GPIO_PIN)    //GPRS断电

#define GPRS_PWRKEY_ON             digitalLo(GPRS_PWR_GPIO_PORT,GPRS_PWR_GPIO_PIN) //GPRS KEY GPIO拉低  --对应的PWRKEY 为拉高
#define GPRS_PWRKEY_OFF            digitalHi(GPRS_PWR_GPIO_PORT,GPRS_PWR_GPIO_PIN) //GPRS KEY GPIO拉高  --对应的PWRKEY 为拉低(三极管)
#else
#define GPRS_POWER_ON               digitalHi(GPRS_GPIO_PORT,GPRS_GPIO_PIN)  //GPRS上电
#define GPRS_POWER_OFF              digitalLo(GPRS_GPIO_PORT,GPRS_GPIO_PIN)    //GPRS断电

#define GPRS_PWRKEY_ON             digitalLo(GPRS_PWR_GPIO_PORT,GPRS_PWR_GPIO_PIN) //GPRS KEY GPIO拉低  --对应的PWRKEY 为拉高 
#define GPRS_PWRKEY_OFF            digitalHi(GPRS_PWR_GPIO_PORT,GPRS_PWR_GPIO_PIN) //GPRS KEY GPIO拉高  --对应的PWRKEY 为拉低(三极管)
#endif
void GPRS_USART_Config(void);

void GPRS_GPIO_Config(void);

void GPRS_PWR_GPIO_Config(void);

void GPRS_Usart_SendData(u8 *dat, u16 len);

void GPRS_Usart_RxClear(void);  //串口清除缓存数据

void GPRS_DMA(uint8_t DMAFlag);

#endif
