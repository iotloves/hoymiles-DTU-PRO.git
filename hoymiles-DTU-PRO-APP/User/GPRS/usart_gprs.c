#include "usart_gprs.h"
#include "stdio.h"
#include "led.h"
#include "Memory.h"
#include "usart_wifi.h"
#include "malloc.h"
#include "TimeProcessing.h"
#include "gprs.h"
#include "HW_Version.h"
#ifndef DTU3PRO
#include "define.h"
#endif
extern vu8 memory_down_mode;
//DMA
extern volatile char DownBuf[2][DOWNLOAD_SIZE];
extern vu8 DmaUseNum;
extern vu32 DownOffset;
//串口空闲状态开关
//extern u8 IT_IDLE;
extern vu8 DownloadEnd;
// 接受的字节数
vu16 gprs_usart_rxd_len = 0;
// 中断发送计数器(count)
vu16 gprs_usart_tx_cnt = 0;
// 中断发送buff的字节数
vu16 gprs_usart_tx_rp = 0;
// 发送的字节数
vu16 gprs_usart_tx_wp = 0;
//开始标志位
vu8 gprs_usart_start_flg = 0;
//结束标志位
vu8 gprs_usart_finish_flg = 0;
//接受buff
vu8 gprs_usart_receive_buff[SOCKET_LEN];
//发送buff暂存
vu8 gprs_usart_send_buff[SOCKET_LEN];
//gprs公共buff
vu8 gprs_share_buff[SOCKET_LEN];
extern vu32 gprs_usart_out_timer;
extern vu32 LocalTime;
//下载文件标志位
extern vu8 downfile;
//微逆升级还是DTU升级标志位
extern vu8 downfile_module;
extern vu8 GPRS_state;
extern volatile DtuMajor Dtu3Major;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];


/***********************************************
** Function name:
** Descriptions:        初始化GPRS模块
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_GPIO_Config(void)
{
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef GPIO_InitStructure;
#ifdef DTU3PRO
    //开启GPRS相关的GPIO外设时钟
    GPRS_RCCPeriphClockCmd(GPRS_GPIO_CLK, ENABLE);
    //选择要控制的GPIO引脚
    GPIO_InitStructure.GPIO_Pin = GPRS_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(GPRS_GPIO_PORT, &GPIO_InitStructure);
    //配置为开机不上电
    GPIO_ResetBits(GPRS_GPIO_PORT, GPRS_GPIO_PIN);
#else
    GPRS_RCCPeriphClockCmd(GPRS_GPIO_RCC | GPRS_GPIO_AFIO, ENABLE);
    //要先开时钟，再重映射；关闭jtag，保留swd。
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
    //开启GPRS相关的GPIO外设时钟
    GPRS_RCCPeriphClockCmd(GPRS_GPIO_CLK, ENABLE);
    //选择要控制的GPIO引脚
    GPIO_InitStructure.GPIO_Pin = GPRS_GPIO_PIN;
    //设置引脚模式为通用推挽输出
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(GPRS_GPIO_PORT, &GPIO_InitStructure);
    // 配置为开机不上电
    GPIO_ResetBits(GPRS_GPIO_PORT, GPRS_GPIO_PIN);
#endif
}
/***********************************************
** Function name:
** Descriptions:        初始化GPRS  PWR_KEY
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_PWR_GPIO_Config(void)
{
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef GPIO_InitStructure;
#ifdef DTU3PRO
    RCC_AHB1PeriphClockCmd(GPRS_PWR_GPIO_CLK, ENABLE);
    //选择要控制的GPIO引脚
    GPIO_InitStructure.GPIO_Pin = GPRS_PWR_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(GPRS_PWR_GPIO_PORT, &GPIO_InitStructure);
    // 配置为开机不上电
    GPIO_SetBits(GPRS_PWR_GPIO_PORT, GPRS_PWR_GPIO_PIN);
#else
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
    //要先开时钟，再重映射；关闭jtag，保留swd。
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
    //开启没6相关的GPIO外设时钟
    RCC_APB2PeriphClockCmd(GPRS_PWR_GPIO_CLK, ENABLE);
    //选择要控制的GPIO引脚
    GPIO_InitStructure.GPIO_Pin = GPRS_PWR_GPIO_PIN;
    //设置引脚模式为通用推挽输出
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(GPRS_PWR_GPIO_PORT, &GPIO_InitStructure);
    //配置为开机上电
    GPIO_SetBits(GPRS_PWR_GPIO_PORT, GPRS_PWR_GPIO_PIN);
#endif
}

/**
 * @brief  配置嵌套向量中断控制器NVIC
 * @param  无
 * @retval 无
 */
static void NVIC_Configuration_GPRS(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    /* 嵌套向量中断控制器组选择 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    /* 配置USART为中断源 */
    NVIC_InitStructure.NVIC_IRQChannel = GPRS_USART_IRQ;
    /* 抢断优先级*/
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    /* 子优先级 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    /* 使能中断 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    /* 初始化配置NVIC */
    NVIC_Init(&NVIC_InitStructure);
    //DMA发送中断设置
    NVIC_InitStructure.NVIC_IRQChannel = GPRS_DMA_Irqn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/***********************************************
** Function name:
** Descriptions:        USART DMA 配置,工作参数配置
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_DMA(u8 DMAFlag)
{
    DMA_InitTypeDef DMA_InitStructure;
    Wifi_DMA(0);
    DmaUseNum = 0;
    memset((char *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
    memset((char *)DownBuf[DmaUseNum + 1], 0, DOWNLOAD_SIZE);
#ifdef DTU3PRO
    //启动DMA时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
    DMA_DeInit(DMA2_Stream1);

    //等待DMA可配置
    while(DMA_GetCmdStatus(DMA2_Stream1) != DISABLE);

    /* 配置 DMA Stream */
    //通道选择
    DMA_InitStructure.DMA_Channel = DMA_Channel_5;
    //DMA外设地址
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&GPRS_USARTx->DR;
    //DMA 存储器0地址
    DMA_InitStructure.DMA_Memory0BaseAddr = (u32)DownBuf[DmaUseNum];
    //外设到存储器模式
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    //数据传输量
    DMA_InitStructure.DMA_BufferSize = (DOWNLOAD_SIZE - 32);
    //外设非增量模式
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    //存储器增量模式
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    //外设数据长度:8位
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    //存储器数据长度:8位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    // 使用普通模式
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    //中等优先级
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
    //存储器突发单次传输
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    //外设突发单次传输
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    //初始化DMA Stream
    DMA_Init(DMA2_Stream1, &DMA_InitStructure);
    //配置DMA发送完成后产生中断
    DMA_ITConfig(DMA2_Stream1, DMA_IT_TC, ENABLE);
    DMA_Cmd(DMA2_Stream1, ENABLE);
#else
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
    //DMA2通道3配置
    // 关DMA通道
    DMA_Cmd(DMA2_Channel3, DISABLE);
    // 恢复缺省值
    DMA_DeInit(DMA2_Channel3);
    //DMA外设地址
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&GPRS_USARTx->DR);
    //DMA 存储器0地址
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)DownBuf[DmaUseNum];
    //外设到存储器模式
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    //数据传输量
    DMA_InitStructure.DMA_BufferSize = (DOWNLOAD_SIZE - 32);
    //外设非增量模式
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    //存储器增量模式
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    //外设数据长度:8位
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    //存储器数据长度:8位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    // 使用普通模式
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    //中等优先级
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    //设置DMA的2个memory中的变量互相访问
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    //初始化DMA Stream
    DMA_Init(DMA2_Channel3, &DMA_InitStructure);
    DMA_ITConfig(DMA2_Channel3, DMA_IT_TC, ENABLE); //配置DMA发送完成后产生中断
    //使能通道3
    DMA_Cmd(DMA2_Channel3, ENABLE);
#endif

    //采用DMA方式接收
    if(DMAFlag == 1)
    {
        //开启串口的 DMA 接受功能
        USART_DMACmd(GPRS_USARTx, USART_DMAReq_Rx, ENABLE);
        // 关闭串口接收中断
        USART_ITConfig(GPRS_USARTx, USART_IT_RXNE, DISABLE);
        // 关闭串口发送中断
        //USART_ITConfig(GPRS_USARTx, USART_IT_TC, DISABLE);
        // 使能串口空闲中断
        USART_ITConfig(GPRS_USARTx, USART_IT_IDLE, ENABLE);
        memory_down_mode = 1;
    }
    else
    {
        USART_DMACmd(GPRS_USARTx, USART_DMAReq_Rx, DISABLE);
        // 使能串口接收中断
        USART_ITConfig(GPRS_USARTx, USART_IT_RXNE, ENABLE);
        // 使能串口发送中断
        //USART_ITConfig(GPRS_USARTx, USART_IT_TC, ENABLE);
        // 使能串口空闲中断
        USART_ITConfig(GPRS_USARTx, USART_IT_IDLE, DISABLE);
        memory_down_mode = 0;
    }
}
/**
 * @brief  USART GPIO 配置,工作参数配置
 * @param  无
 * @retval 无
 */
void GPRS_USART_Config(void)
{
    USART_InitTypeDef USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    // 打开串口外设的时钟
    GPRS_USART_APBxClkCmd(GPRS_USART_CLK, ENABLE);
    // 打开串口GPIO的时钟
    GPRS_USART_GPIO_APBxClkCmd(GPRS_USART_GPIO_CLK, ENABLE);
#ifdef DTU3PRO
    //设置引脚复用
    GPIO_PinAFConfig(GPRS_USART_TX_GPIO_PORT, GPRS_USART_TX_SOURCE, GPRS_USART_TX_GPIO_AF);
    GPIO_PinAFConfig(GPRS_USART_RX_GPIO_PORT, GPRS_USART_RX_SOURCE, GPRS_USART_RX_GPIO_AF);
#endif
    // 将USART Tx的GPIO配置为推挽复用模式
    GPIO_InitStructure.GPIO_Pin = GPRS_USART_TX_GPIO_PIN;
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
#endif
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPRS_USART_TX_GPIO_PORT, &GPIO_InitStructure);
    // 将USART Rx的GPIO配置为浮空输入模式
    GPIO_InitStructure.GPIO_Pin = GPRS_USART_RX_GPIO_PIN;
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
#endif
    GPIO_Init(GPRS_USART_RX_GPIO_PORT, &GPIO_InitStructure);
    // 配置串口的工作参数
    // 配置波特率
    USART_InitStructure.USART_BaudRate = GPRS_USART_BAUDRATE;
    // 配置 针数据字长
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    // 配置停止位
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    // 配置校验位
    USART_InitStructure.USART_Parity = USART_Parity_No;
    // 配置硬件流控制
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    // 配置工作模式，收发一起
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    // 完成串口的初始化配置
    USART_Init(GPRS_USARTx, &USART_InitStructure);
    // 串口中断优先级配置
    NVIC_Configuration_GPRS();
    // 使能串口接收中断
    USART_ITConfig(GPRS_USARTx, USART_IT_RXNE, ENABLE);
    // 使能串口发送中断
    USART_ITConfig(GPRS_USARTx, USART_IT_TC, ENABLE);
    // 使能串口
    USART_Cmd(GPRS_USARTx, ENABLE);
    USART_ClearFlag(GPRS_USARTx, USART_FLAG_TC);
}


/***********************************************
** Function name:
** Descriptions:        gprs串口DMA中断
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_DMA_IRQHandler(void)
{
    //char *Buffer;
#ifdef DTU3PRO
    if(DMA_GetITStatus(DMA2_Stream1, DMA_IT_TCIF1) != RESET)
    {
        //清除全部中断标志
        DMA_ClearFlag(DMA2_Stream1, DMA_IT_TCIF1);
        DMA_Cmd(DMA2_Stream1, DISABLE);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((char *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值 传输地址
        DMA2_Stream1->M0AR = (u32)DownBuf[DmaUseNum];
        DMA2_Stream1->NDTR = (DOWNLOAD_SIZE - 32);
        //打开DMA
        DMA_Cmd(DMA2_Stream1, ENABLE);
#else

    if(DMA_GetFlagStatus(DMA2_IT_TC3) != RESET)
    {
        //清除全部中断标志
        DMA_ClearFlag(DMA2_IT_TC3);
        DMA_Cmd(DMA2_Channel3, DISABLE);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((u8 *)DownBuf[DmaUseNum], 0,  DOWNLOAD_SIZE);
        // 设置DMA的传输值
        DMA2_Channel3 -> CNDTR = (DOWNLOAD_SIZE - 32);
        // 设置传输地址
        DMA2_Channel3 -> CMAR  = (u32)DownBuf[DmaUseNum];
        //打开DMA
        DMA_Cmd(DMA2_Channel3, ENABLE);
#endif
        gprs_usart_out_timer = LocalTime;
#ifndef DTU3PRO
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Dtu3Major.Property.netmode_select);
#else
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Netmode_Used);
#endif
        //printf("%s\n",DownBuf[(DmaUseNum + 1) % 2]);
    }
}


void GPRS_USART_IRQHandler(void)
{
    vu8 buffer = 0;
    vu32 temp = 0;

    if(USART_GetITStatus(GPRS_USARTx, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(GPRS_USARTx, USART_IT_RXNE);
        buffer = (u8)USART_ReceiveData(GPRS_USARTx);
        gprs_usart_out_timer = LocalTime;
        //开始标志位
        gprs_usart_start_flg = 1;
        //结束标志位
        gprs_usart_finish_flg = 0;
        gprs_usart_receive_buff[gprs_usart_rxd_len++] = buffer;

        if(gprs_usart_rxd_len > SOCKET_LEN)
        {
            gprs_usart_rxd_len = 0;
            //开始标志位
            gprs_usart_start_flg = 0;
        }
    }

    if(USART_GetFlagStatus(GPRS_USARTx, USART_FLAG_ORE) != RESET)
    {
        USART_ReceiveData(GPRS_USARTx);
    }

    if(USART_GetFlagStatus(GPRS_USARTx, USART_FLAG_TC) != RESET)
    {
        //发送计数器大于1
        if(gprs_usart_tx_cnt > 1)
        {
            USART_SendData(GPRS_USARTx, gprs_usart_send_buff[gprs_usart_tx_rp]);
            gprs_usart_tx_rp = (gprs_usart_tx_rp + 1) % SOCKET_LEN;
        }

        //最后一位是空 不发
        if(gprs_usart_tx_cnt > 0)
        {
            gprs_usart_tx_cnt--;
            //memset(gprs_usart_send_buff,0,sizeof(gprs_usart_send_buff));
        }

        USART_ClearITPendingBit(GPRS_USARTx, USART_IT_TC);
        USART_ClearITPendingBit(GPRS_USARTx, USART_FLAG_TC);
    }
    else if(USART_GetITStatus(GPRS_USARTx, USART_IT_IDLE) != RESET)
    {
        USART_ClearITPendingBit(GPRS_USARTx, USART_IT_IDLE);
#ifdef DTU3PRO
        DMA_Cmd(DMA2_Stream1, DISABLE);
        temp = GPRS_USARTx->SR;
        temp = GPRS_USARTx->DR;
        //清除全部中断标志
        DMA_ClearFlag(DMA2_Stream1, DMA_IT_TCIF1);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((char *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值 传输地址
        DMA2_Stream1->M0AR = (u32)DownBuf[DmaUseNum];
        DMA2_Stream1->NDTR = (DOWNLOAD_SIZE - 32);
        //打开DMA
        DMA_Cmd(DMA2_Stream1, ENABLE);
#else
        DMA_Cmd(DMA2_Channel3, DISABLE);
        temp = GPRS_USARTx->SR;
        temp = GPRS_USARTx->DR;
        //清除全部中断标志
        DMA_ClearFlag(DMA2_IT_TC3);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((u8 *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值
        DMA2_Channel3 -> CNDTR = (DOWNLOAD_SIZE - 32);
        // 设置传输地址
        DMA2_Channel3 -> CMAR  = (u32)DownBuf[DmaUseNum];
        //打开DMA
        DMA_Cmd(DMA2_Channel3, ENABLE);
#endif
        gprs_usart_out_timer = LocalTime;
#ifndef DTU3PRO
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Dtu3Major.Property.netmode_select);
#else
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Netmode_Used);
#endif
    }
}
/***********************************************
** Function name:
** Descriptions:        串口发送一个字符
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
//这样发送数据只需要发送第一个字符后 其他的中断发送 不用硬延时查询是否发送完
u8 GPRS_Usart_PutChar(u8 ch)
{
    // Wait until there is space in the TX buffer:
    vu8 i = 0;

    while(gprs_usart_tx_cnt > SOCKET_LEN)
    {
        if(i > 200)
        {
            gprs_usart_tx_cnt = 0;
        }

        i++;
    }

    if(0 == gprs_usart_tx_cnt)
    {
        gprs_usart_tx_wp = 0;
        gprs_usart_tx_rp = 1;
        gprs_usart_send_buff[gprs_usart_tx_wp] = ch;
    }
    else
    {
        gprs_usart_send_buff[gprs_usart_tx_wp] = ch;
    }

    gprs_usart_tx_wp = (gprs_usart_tx_wp + 1) % SOCKET_LEN;
    gprs_usart_tx_cnt++;
    return (ch);
}

/***********************************************
** Function name:
** Descriptions:        串口发送
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_Usart_SendData(u8 *dat, u16 len)
{
    vu16 i;

    for(i = 0; i < len; i++)
    {
        GPRS_Usart_PutChar(*(dat + i));

        if(i == (len - 1))
        {
            // Write first char directly to the UART SFR
            USART_SendData(GPRS_USARTx, (u8)gprs_usart_send_buff[0]);
        }
    }
}

/***********************************************
** Function name:
** Descriptions:        串口清除缓存数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_Usart_RxClear(void)
{
    vu16 i;

    for(i = 0; i < SOCKET_LEN; i++)
    {
        gprs_usart_receive_buff[i] = 0;
    }

    gprs_usart_rxd_len = 0;
}

