#include "gprs.h"
#include "string.h"
#include "usart_gprs.h"
#include "led.h"
#include "stdio.h"
#include "wifi.h"
#include "usart_nrf.h"
#include "NetProtocol.h"
#include "Memory.h"
#include "TimeProcessing.h"
#include "HW_Version.h"
#include "ServerProtocol.h"
#ifndef DTU3PRO
#ifdef DEBUG1
vu8 errr_flag = 0;
#endif
#endif

extern vu8 NetProVer;
//升级标志位
extern vu8 upgrade;
//校验文件失败
extern vu8 Check_file_failed;
//app 标志位
extern vu8 APP_Flg;
#ifndef DTU3PRO
//LED状态
extern vu8 led_state;
#endif
extern vu8 Registration_frame;
//连接外网标志位
extern vu8 connect_net;
//连上服务器标志位
extern vu8 connect_server;
// 有服务器数据
extern vu8 server_data;
#ifndef DTU3PRO
//DTU型号 默认值
extern vu8 HW_Ver;
#endif
//微逆升级还是DTU升级标志位
extern vu8 downfile_module;
//下载文件次数
extern vu8 user_http_file_times;
// socket状态
extern vu32 socket_state;

extern volatile DtuMajor Dtu3Major;
//打包暂存buff
extern vu8 pack_buff[PACKAGE_ALL][SOCKET_LEN];
//每包的长度
extern vu16 pack_len[PACKAGE_ALL];
//C-S的当前包数
extern vu8 package_to_server;
//C-S的总包数
extern vu8 total_package_to_server;
//发送数据长度
extern vu16 socket_data_len;
//要发送服务器的数据
extern vu8 server_socket_send_buff[SOCKET_LEN];

extern vu8 downfile;
// 接受的字节数
extern vu16 gprs_usart_rxd_len;
// 中断发送计数器(count)
extern vu16 gprs_usart_tx_cnt;
// 中断发送buff的字节数
extern vu16 gprs_usart_tx_rp;
// 发送的字节数
extern vu16 gprs_usart_tx_wp;
//开始标志位
extern vu8 gprs_usart_start_flg;
//结束标志位
extern vu8 gprs_usart_finish_flg;
// 下载ip
extern vu8 up_pro_destip[4];
//端口号
extern vu16 up_pro_destport;
//DNS
extern vu8 up_pro_domain[ROW_MAX_LEN];

extern vu8 up_pro_filename[ROW_MAX_LEN];
extern vu8 up_pro_url[ROW_MAX_LEN];

extern vu8 gprs_usart_receive_buff[SOCKET_LEN];
extern vu8 gprs_usart_send_buff[SOCKET_LEN];


extern vu8 gprs_share_buff[SOCKET_LEN];
extern  vu32 LocalTime;
//服务器发送数据计数器
extern vu8 SendData_to_Server_Num;
//发送数据长度标志位
volatile bool send_len_flg = 0;
//下载文件的地址是域名还是ip 0是ip 1是域名
extern vu8 HttpMode;
//下载完成标志位
extern vu8 DownloadEnd;
//GPRS启动计时器
vu32 gprs_start_timer = 0;
//gprs 串口计时器
vu32 gprs_usart_out_timer = 0;
//重发计时器
vu32 gprs_data_link_time = 0;
//gprs 状态机
vu8 GPRS_state = 0;
//发送次数
vu8 gprs_send_nub = 0;
//发送查询CSQ的次数
vu8 gprs_send_csq = 0;
//重启次数
vu32 gprs_restart_nub = 0;
//下载文件超时 20分钟
vu8 down_timeout = 0;


/***********************************************
** Function name:
** Descriptions:        操作 GPRS模块主循环
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void GPRS_Server(void)
{
    vu8 APN[40] = {0};
    vu16 i, j = 0;

    //有数据但是超时
    if((1 == gprs_usart_start_flg) && (LocalTime > (400 + gprs_usart_out_timer)))
    {
        gprs_usart_out_timer = LocalTime;

        //收到一包
        if((0 == gprs_usart_finish_flg) && (1 == gprs_usart_start_flg))
        {
            gprs_usart_finish_flg = 1;
        }

        gprs_usart_start_flg  = 0;

        //命令应答
        if((GPRS_state < GS_IDLE) || ((GS_SEND_HEART < GPRS_state) && (GPRS_state < GS_WAIT_IDLE)))
        {
            i = 0;

            while(i < gprs_usart_rxd_len)
            {
                if(strncmp((char *)&gprs_usart_receive_buff[i], "OK\r\n", 4) == 0)
                {
                    gprs_data_link_time = LocalTime;

                    switch(GPRS_state)
                    {
                        case GS_ATE0:
                            GPRS_state = GS_GMR;
                            break;

                        case GS_CPIN:
                            GPRS_state = GS_CREG;
                            break;

                        case GS_CREG:
                            GPRS_state = GS_CCID;
                            break;

                        case GS_QIDEACT:
                            GPRS_state = GS_QIPROMPT;
                            break;

                        case GS_QIPROMPT:
                            GPRS_state = GS_CGATT;
                            break;

                        case GS_CGATT:
                            GPRS_state = GS_QIFGCNT;
                            break;

                        case GS_QIFGCNT:
                            GPRS_state = GS_QICSGP;
                            break;

                        case GS_QICSGP:
                            GPRS_state = GS_QIREGAPP;
                            break;

                        case GS_QIREGAPP:
                            GPRS_state = GS_QIACT;
                            break;

                        case GS_QIACT:
                            GPRS_state = GS_QIDNSIP;
                            break;

                        case GS_QIDNSIP:
                            GPRS_state = GS_QIOPEN;
                            break;

                        case GS_QIOPEN:
                            //70s
                            gprs_data_link_time = LocalTime + 70000;
                            break;

                        //HTTP
                        case GS_CLOSE_HTTP:
                            GPRS_state = GS_CLOSE_SOCKET;
                            break;

                        case GS_CLOSE_SOCKET:
                            GPRS_state = GS_GRAPP;
                            break;

                        case GS_GRAPP:
                            GPRS_state = GS_HTTP;
                            break;

                        case GS_HTTP:
                            GPRS_state = GS_ID;
                            break;

                        case GS_CONNECT_HTTP:
                            GPRS_state = GS_OPEN_HTTP;
                            break;

                        case GS_OPEN_HTTP:
                            GPRS_state = GS_DOWN_FILE;
                            break;
                    }

                    // 溢出
                    if(GPRS_state > GS_WAIT_IDLE)
                    {
                        GPRS_state = GS_WAIT_IDLE;
                    }

                    gprs_send_nub = 0;
                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    break;
                }
                //软件版本
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "Revision: ", 10) == 0)
                {
                    i = i + 10;
                    memcpy((u8 *)Dtu3Major.Property.Gprs_Ver, (u8 *)&gprs_usart_receive_buff[i], 19);
                    GPRS_state  = GS_CSQ;
                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    gprs_send_nub = 0;
                    break;
                }
                //+CSQ: 26,99
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "+CSQ: ", 6) == 0)
                {
                    gprs_send_nub = 0;
                    Dtu3Major.Property.GPRS_CSQ = gprs_usart_receive_buff[i + 6] - 0x30;

                    if((gprs_usart_receive_buff[i + 7] >= '0') && (gprs_usart_receive_buff[i + 7] <= '9'))
                    {
                        Dtu3Major.Property.GPRS_CSQ = Dtu3Major.Property.GPRS_CSQ * 10;
                        Dtu3Major.Property.GPRS_CSQ = Dtu3Major.Property.GPRS_CSQ + (gprs_usart_receive_buff[i + 7] - 0x30);
                    }

                    if(GS_CSQ == GPRS_state)
                    {
                        if((Dtu3Major.Property.GPRS_CSQ > 10) && (Dtu3Major.Property.GPRS_CSQ != 99))
                        {
                            GPRS_state  = GS_CPIN;
                            gprs_send_csq = 0;
                        }
                        else
                        {
                            gprs_send_csq ++;

                            // 100次以后 继续向下执行
                            if(gprs_send_csq > 100)
                            {
                                GPRS_state = GS_CPIN;
                                gprs_send_csq = 0;
                            }
                        }
                    }

                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    break;
                }
                //卡号
                else if((strncmp((char *)&gprs_usart_receive_buff[i], "\r\n", 2) == 0) && (GPRS_state == GS_CCID))
                {
                    gprs_send_nub = 0;
                    i = i + 2;
                    memset((u8 *)Dtu3Major.Property.Gprs_SIMNum, 0, sizeof(Dtu3Major.Property.Gprs_SIMNum));

                    for(j = i; j < sizeof(Dtu3Major.Property.Gprs_SIMNum); j++)
                    {
                        if((gprs_usart_receive_buff[j] == '\r') || ((gprs_usart_receive_buff[j] == '\n')))
                        {
                            break;
                        }
                    }

                    memcpy((u8 *)Dtu3Major.Property.Gprs_SIMNum, (u8 *)&gprs_usart_receive_buff[i], j - i);
                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    GPRS_state = GS_QIDEACT;
                    break;
                }
                else if(GS_QIOPEN == GPRS_state)
                {
                    if((strncmp((char *)&gprs_usart_receive_buff[i], "ALREADY CONNECT", 15) == 0) || (strncmp((char *)&gprs_usart_receive_buff[i], "CONNECT OK", 10) == 0))  //TCP已经连接
                    {
                        gprs_send_nub = 0;
                        GPRS_state = GS_IDLE;
                        gprs_data_link_time = LocalTime;
#ifndef DTU3PRO

                        if((APP_Flg != 1) && (Dtu3Major.Property.netmode_select == GPRS_MODE))
#else
                        if((APP_Flg != 1) && (Netmode_Used == GPRS_MODE))
#endif
                        {
                            socket_state = 0;
                            //每次切换AT命令的时候都先发注册帧
                            socket_state |= REGISTRATION_PACK;
                            Registration_frame = 0;
                        }

#ifndef DTU3PRO

                        if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                        if(Netmode_Used == GPRS_MODE)
#endif
                        {
                            //连接上服务器
                            connect_server = 1;
                            connect_net = 1;
                            //连接上服务器
                            Dtu3Major.Property.LinkMode = 1;
                        }

                        break;
                    }
                    //建立连接失败
                    else if(strncmp((char *)&gprs_usart_receive_buff[i], "CONNECT FAIL", 12) == 0)
                    {
                        gprs_data_link_time = LocalTime;
                        GPRS_state = GS_QIDEACT;
                        break;
                    }
                }
                else if((GS_ID == GPRS_state) && (strncmp((char *)&gprs_usart_receive_buff[i], "CONNECT", 7) == 0))
                {
                    gprs_send_nub = 0;
                    GPRS_state = GS_CONNECT_HTTP;
                    break;
                }
                else if((GS_DOWN_FILE == GPRS_state) && (strncmp((char *)&gprs_usart_receive_buff[i], "CONNECT", 7) == 0))
                {
                    gprs_send_nub = 0;
                    GPRS_state = GS_WAIT_IDLE;
                    break;
                }
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "\r\nERROR\r\n", 9) == 0)
                {
                    //关闭是ERROR 状态也加
                    if((GPRS_state == GS_CLOSE_HTTP) || (GPRS_state == GS_CLOSE_SOCKET))
                    {
                        if(GPRS_state == GS_CLOSE_HTTP)
                        {
                            GPRS_state = GS_CLOSE_SOCKET;
                        }
                        else if(GPRS_state == GS_CLOSE_SOCKET)
                        {
                            GPRS_state = GS_GRAPP;
                        }
                    }

                    if(((GPRS_state == GS_GRAPP) || GPRS_state == GS_HTTP))
                    {
                        GPRS_state = GS_CLOSE_HTTP;
                    }
                    else if(GPRS_state < GS_IDLE)
                    {
#ifdef DEBUG1
                        printf("GS_CONTROL_POFF:1\r\n");
#endif
                        GPRS_state = GS_CONTROL_POFF;
                        gprs_data_link_time = LocalTime + 2000;
                    }

                    gprs_data_link_time = LocalTime;
                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    break;
                }
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "ERROR:", 6) == 0)
                {
#ifdef DEBUG1
                    printf("GS_CONTROL_POFF:2\r\n");
#endif
                    GPRS_state = GS_CONTROL_POFF;
                    gprs_data_link_time = LocalTime + 2000;
                    gprs_send_nub = 0;
                    gprs_data_link_time = LocalTime;
                    break;
                }

                i++;
            }

            memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
            gprs_usart_rxd_len = 0;
        }
        //接受到服务器数据
        else
        {
            i = 0;

            while(i < gprs_usart_rxd_len)
            {
                if(strncmp((char *)&gprs_usart_receive_buff[i], ">", 1) == 0)
                {
                    send_len_flg = 1;
                    break;
                }

                if(strncmp((char *)&gprs_usart_receive_buff[i], "SEND OK", 7) == 0)
                {
                    send_len_flg = 0;
                    break;
                }

                if(strncmp((char *)&gprs_usart_receive_buff[i], "OK\r\n", 4) == 0)
                {
                    gprs_send_nub = 0;
                    memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
                    break;
                }
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "+CSQ: ", 6) == 0)
                {
                    gprs_send_nub = 0;
                    Dtu3Major.Property.GPRS_CSQ = gprs_usart_receive_buff[i + 6] - 0x30;

                    if((gprs_usart_receive_buff[i + 7] >= '0') && (gprs_usart_receive_buff[i + 7] <= '9'))
                    {
                        Dtu3Major.Property.GPRS_CSQ = Dtu3Major.Property.GPRS_CSQ * 10;
                        Dtu3Major.Property.GPRS_CSQ = Dtu3Major.Property.GPRS_CSQ + (gprs_usart_receive_buff[i + 7] - 0x30);
                    }

                    break;
                }
                else if(strncmp((char *)&gprs_usart_receive_buff[i], "ERROR", 5) == 0)
                {
                    //重新发送长度
                    send_len_flg = 0;
                    break;
                }
                else if((gprs_usart_receive_buff[0] == 0x48) && (gprs_usart_receive_buff[1] == 0x4d))
                {
                    //处理接受的服务器数据
#ifndef DTU3PRO
                    if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                    if(Netmode_Used == GPRS_MODE)
#endif
                    {
                        //连接的外网
                        connect_net = 1;
                        //连上服务器标志位
                        connect_server = 1;
                        //服务器数据
                        server_data = 1;
                        pb_receive((char *)&gprs_usart_receive_buff);
                    }
                    else
                    {
                        GPRS_state = GS_IDLE;
                    }

                    break;
                }
                else if(gprs_usart_receive_buff[0] == 0xA5)
                {
#ifndef DTU3PRO

                    if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                    if(Netmode_Used == GPRS_MODE)
#endif
                        Process_Server_Data((char *)&gprs_usart_receive_buff);

                    if(Dtu3Major.Property.netmode_select  == GPRS_MODE)
                    {
                        //连接的外网
                        connect_net = 1;
                        //连上服务器标志位
                        connect_server = 1;
                        //服务器数据
                        server_data = 1;
                    }

                    break;
                }

                i++;
            }
        }

        memset((u8 *)gprs_usart_receive_buff, 0, sizeof(gprs_usart_receive_buff));
        gprs_usart_rxd_len = 0;
    }
    //下载程序卡顿1.5分钟  开始校验
    //    else if(Check_file_failed == 1 || ((GPRS_state == GS_WAIT_IDLE) && (LocalTime > (90000 + gprs_usart_out_timer))))
    else if((GPRS_state == GS_WAIT_IDLE) && (LocalTime > (90000 + gprs_usart_out_timer)))
    {
#ifdef DEBUG1
        printf("down_file_stop:%d\r\n", GPRS_state);
#endif
        //        Check_file_failed = 0;
        //        gprs_send_nub = 0;
        //        GPRS_state = GS_CLOSE_HTTP;
        //        //关闭DMA
        //        GPRS_DMA(0);
        //        gprs_data_link_time = LocalTime;
        gprs_usart_out_timer = LocalTime;
        DownloadEnd = 1;
    }

    //校验失败 重新下载
#ifndef DTU3PRO

    if((Check_file_failed == 1) && (Dtu3Major.Property.netmode_select == GPRS_MODE))
#else
    if((Check_file_failed == 1) && (Netmode_Used == GPRS_MODE))
#endif
    {
        Check_file_failed = 0;
        gprs_send_nub = 0;
        GPRS_state = GS_CLOSE_HTTP;
        //关闭DMA
        GPRS_DMA(0);
        gprs_data_link_time = LocalTime;
        gprs_usart_out_timer = LocalTime;
    }
    //发送命令
    else if((GPRS_state != GS_IDLE))
    {
        if(LocalTime  > (1000 + gprs_data_link_time))
        {
            if((gprs_send_nub > 20) || (SendData_to_Server_Num > 20))
            {
                gprs_send_nub = 0;
                SendData_to_Server_Num = 0;
#ifdef DEBUG1
                printf("GS_CONTROL_POFF:3\r\n");
#endif
                GPRS_state = GS_CONTROL_POFF;
                gprs_data_link_time = LocalTime + 2000;
            }

            gprs_usart_finish_flg = 0;
            gprs_data_link_time = LocalTime;
            gprs_usart_tx_cnt = 0;
            memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
            gprs_usart_start_flg = 0;

            if(((GS_CONTROL_KEYON < GPRS_state) && (GPRS_state < GS_IDLE)) || ((GS_SEND_HEART < GPRS_state) && (GPRS_state < GS_DOWN_FILE)))
            {
                //发送一次加一次
                gprs_send_nub++;
            }

            switch(GPRS_state)
            {
                //0GPRS电源关
                case GS_CONTROL_POFF:
                    GPRS_POWER_OFF;
                    GPRS_PWRKEY_OFF;
                    gprs_restart_nub++;
                    GPRS_state ++;
#ifndef DTU3PRO

                    if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                    if(Netmode_Used == GPRS_MODE)
#endif
                    {
#ifndef DTU3PRO
                        led_state = 2;
#endif
                        // 网络标志位
                        connect_net = 0;
                        // 连接服务器标志位
                        connect_server = 0;
                        // 有服务器数据
                        server_data = 0;
                        //连接上服务器
                        Dtu3Major.Property.LinkMode = 0;
                    }

                    Dtu3Major.Property.GPRS_CSQ = 0;
                    //重启每超过200次 时间增加一分钟
                    gprs_data_link_time = LocalTime + 3000 + 60 * 1000 * (gprs_restart_nub / 200);
                    break;

                //1    GPRS电源开
                case GS_CONTROL_PON:
                    GPRS_POWER_ON;
                    GPRS_PWRKEY_ON;
                    GPRS_state ++;
                    gprs_data_link_time = LocalTime + 1000;
                    break;

                //2  PWR_KEY 拉低
                case GS_CONTROL_KEYOFF:
                    GPRS_PWRKEY_OFF;
                    GPRS_state ++;
                    gprs_data_link_time = LocalTime + 1000;
                    break;

                //3 PWR_KEY 拉高
                case GS_CONTROL_KEYON:
                    GPRS_PWRKEY_ON;
                    GPRS_state ++;
                    gprs_data_link_time = LocalTime + 1000;
                    break;

                //4 关闭回显
                case GS_ATE0:
                    GPRS_Usart_SendData((u8 *)GPRS_ATE0, sizeof(GPRS_ATE0) - 1);
                    break;

                //5 查询软件版本
                case GS_GMR:
                    GPRS_Usart_SendData((u8 *)GPRS_GMR, sizeof(GPRS_GMR) - 1);
                    break;

                //6 查看2G信号强度
                case GS_CSQ:
                    GPRS_Usart_SendData((u8 *)GPRS_CHECK_NET_SIGNAL, sizeof(GPRS_CHECK_NET_SIGNAL) - 1);
                    break;

                // 7 查询PIN码 卡激活问题
                case GS_CPIN:
#ifndef DTU3PRO
                    if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                    if(Netmode_Used == GPRS_MODE)
#endif
                    {
                        connect_net = 0;
                        connect_server = 0;
                        //服务器数据
                        server_data = 0;
                    }

                    GPRS_Usart_SendData((u8 *)GPRS_CPIN, sizeof(GPRS_CPIN) - 1);
                    gprs_data_link_time = LocalTime + 4000;
                    break;

                // 8 查看网络注册等待注册
                case GS_CREG:
                    GPRS_Usart_SendData((u8 *)GPRS_VIEW_NET_REG, sizeof(GPRS_VIEW_NET_REG) - 1);
                    break;

                // 9 SIM 卡号
                case GS_CCID:
                    GPRS_Usart_SendData((u8 *)GPRS_CCID, sizeof(GPRS_CCID) - 1);
                    break;

                //A 关闭 GPRS/CSD PDP 场景
                case GS_QIDEACT:
#ifndef DTU3PRO
                    if(Dtu3Major.Property.netmode_select == GPRS_MODE)
#else
                    if(Netmode_Used == GPRS_MODE)
#endif
                    {
                        connect_net = 1;
                        connect_server = 0;
                        //服务器数据
                        server_data = 0;
                    }

                    GPRS_Usart_SendData((u8 *)GPRS_QIDEACT, sizeof(GPRS_QIDEACT) - 1);
                    //最长40s
                    gprs_data_link_time = LocalTime + 39000;
                    break;

                //B 关闭发送数据回复>和OK
                case GS_QIPROMPT:
                    GPRS_Usart_SendData((u8 *)GPRS_QIPROMPT, sizeof(GPRS_QIPROMPT) - 1);
                    break;

                //C  查询GPRS附着是否成功
                case GS_CGATT:
                    GPRS_Usart_SendData((u8 *) GPRS_CGATT, sizeof(GPRS_CGATT) - 1);
                    gprs_data_link_time = LocalTime + 70000;
                    break;

                //D context0
                case GS_QIFGCNT:
                    GPRS_Usart_SendData((u8 *)GPRS_QIFGCNT, sizeof(GPRS_QIFGCNT) - 1);
                    break;

                //E 设置GPRS的APN  移动联通和不设置
                case GS_QICSGP:
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                    memset((u8 *)APN, 0, sizeof(APN));
                    memcpy((u8 *)APN, (char *)Dtu3Major.Property.APN, sizeof(Dtu3Major.Property.APN));
                    memcpy((u8 *)&APN[sizeof(Dtu3Major.Property.APN)], (char *)Dtu3Major.Property.APN2, sizeof(Dtu3Major.Property.APN2));
                    sprintf((char *)gprs_share_buff, (char *)GPRS_QICSGP, APN);
                    GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    break;

                //F 启动任务并设置接入点APN、用户名和密码
                case GS_QIREGAPP:
                    GPRS_Usart_SendData((u8 *)GPRS_QIREGAPP, sizeof(GPRS_QIREGAPP) - 1);
                    break;

                //10 激活PDP
                case GS_QIACT:
                    GPRS_Usart_SendData((u8 *)GPRS_QIACT, sizeof(GPRS_QIACT) - 1);
                    gprs_data_link_time = LocalTime + 100000;
                    break;

                //设置服务器地址使用域名方式
                case GS_QIDNSIP:
                    GPRS_Usart_SendData((u8 *)GPRS_QIDNSIP, sizeof(GPRS_QIDNSIP) - 1);
                    break;

                // TCP连接
                case GS_QIOPEN:
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                    sprintf((char *)gprs_share_buff, (char *)GPRS_QIOPEN, (char *)Dtu3Major.Property.ServerDomainName, Dtu3Major.Property.ServerPort);
                    GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    //OK 或者ERROR 300ms  状态最长75s
                    gprs_data_link_time = LocalTime + 4000;
                    break;

                // 空闲
                case GS_IDLE:
                    break;

                // 获取配置文件
                case GS_GET_CONFIG:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
#ifdef DEBUG1
                        printf("SEND_GET_CONFIG_LEN:%d\r\n", socket_data_len);
#endif
                    }

                    if(send_len_flg == 1)
                    {
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        //发送完成切换到空闲模式
                        GPRS_state = GS_IDLE;
#ifdef DEBUG1
                        printf("SEND_GET_CONFIG\r\n");
#endif
                    }

                    SendData_to_Server_Num++;
                    break;

                // 设置配置文件
                case GS_SET_CONFIG:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
#ifdef DEBUG1
                        printf("SEND_SET_CONFIG_LEN:%d\r\n", socket_data_len);
#endif
                    }

                    if(send_len_flg == 1)
                    {
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        GPRS_state = GS_IDLE;
#ifdef DEBUG1
                        printf("SEND_SET_CONFIG\r\n");
#endif
                    }

                    SendData_to_Server_Num++;
                    break;

                //应答网络命令
                case GS_RE_SERVER:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
#ifdef DEBUG1
                        printf("SEND_RESPONSE_NET_LEN:%d\r\n", socket_data_len);
#endif
                    }

                    if(send_len_flg == 1)
                    {
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        GPRS_state = GS_IDLE;
#ifdef DEBUG1
                        printf("SEND_RESPONSE_NET\r\n");
#endif
                    }

                    SendData_to_Server_Num++;
                    break;

                // 网络命令执行状态
                case GS_STATUS:
                    if(send_len_flg == 0)
                    {
                        if(NetProVer == 0)
                        {
                            package_net_command_status();
                        }

                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
#ifdef DEBUG1
                        printf("SEND_NET_STATUS_LEN:%d\r\n", socket_data_len);
#endif
                    }

                    if(send_len_flg == 1)
                    {
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
#ifdef DEBUG1
                        printf("SEND_NET_STATUS\r\n");
#endif
                    }

                    SendData_to_Server_Num++;
                    break;

                // info
                case GS_SEND_INFO:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
#ifdef DEBUG1
                        printf("SEND_INFO_LEN:%d\r\n", socket_data_len);
#endif
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
#ifdef DEBUG1
                        printf("SEND_INFO:\r\n");
#endif
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                    }

                    SendData_to_Server_Num++;
                    break;

                case GS_DTU_ALERTING:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                    }

                    SendData_to_Server_Num++;
                    break;

                case GS_MI_SUSPEND_ALERTING:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                    }

                    SendData_to_Server_Num++;
                    break;

                //data
                case GS_SEND_DATA:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

                        if(NetProVer == 1)
                        {
                            socket_data_len = pack_len[package_to_server];
                            memcpy((char *)server_socket_send_buff, (char *)pack_buff[package_to_server], socket_data_len);
                        }
                        else
                        {
                            socket_data_len = pb_RealDataReq((char *)server_socket_send_buff, (char *)pack_buff[package_to_server], pack_len[package_to_server]);
                        }

                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
#ifdef DEBUG1
                        printf("SEND_Data_LEN:%d\r\n", socket_data_len);
#endif
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
#ifdef DEBUG1
                        printf("SEND_Data:\r\n");
#endif
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                    }

                    SendData_to_Server_Num++;
                    break;

                case GS_MI_ALERTING:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                    }

                    SendData_to_Server_Num++;
                    break;

                // history data
                case GS_HISTORY_DATA:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
#ifdef DEBUG1
                        printf("SEND_HIS_Data_LEN:%d\r\n", socket_data_len);
#endif
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
#ifdef DEBUG1
                        printf("SEND_HIS_Data:\r\n");
#endif
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                    }

                    SendData_to_Server_Num++;
                    break;

                case GS_MI_HISTORY_ALERTING:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                    }

                    SendData_to_Server_Num++;
                    break;

                //  heart
                case GS_SEND_HEART:
                    if(send_len_flg == 0)
                    {
                        memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                        sprintf((char *)gprs_share_buff, (char *)GPRS_QISEND, socket_data_len);
#ifdef DEBUG1
                        printf("SEND_HEART_LEN:%d\r\n", socket_data_len);
#endif
                        GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    }

                    if(send_len_flg == 1)
                    {
#ifdef DEBUG1
                        printf("SEND_HEART:\r\n");
#endif
                        GPRS_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        gprs_data_link_time = LocalTime + server_usart_out_timter;
                    }

                    SendData_to_Server_Num++;
                    break;

                /* 下载升级程序文件 */

                // 关闭 GPRS/CSD PDP 场景
                case GS_CLOSE_HTTP:

                    //清除20分钟超时标志位
                    if(user_http_file_times > 2)
                    {
                        /*下载失败清除下载标志位，允许历史数据存储*/
                        downfile = 0;
                        /*hzwang_20200422*/
                        upgrade = 0;
                        GPRS_state = GS_ATE0 ;
                        user_http_file_times = 0;
                        //置失败标志位
                        clear_mi_update_status();
                        GPRS_DMA(0);
                        break;
                    }

                    down_timeout = 0;
                    GPRS_Usart_SendData((u8 *)GPRS_QIDEACT, sizeof(GPRS_QIDEACT) - 1);
                    gprs_data_link_time = LocalTime + 39000;
                    break;

                // 设置GPRS的APN
                case GS_CLOSE_SOCKET:
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                    memset((u8 *)APN, 0, sizeof(APN));
                    memcpy((u8 *)APN, (char *)Dtu3Major.Property.APN, sizeof(Dtu3Major.Property.APN));
                    memcpy((u8 *)&APN[sizeof(Dtu3Major.Property.APN)], (char *)Dtu3Major.Property.APN2, sizeof(Dtu3Major.Property.APN2));
                    sprintf((char *)gprs_share_buff, (char *)GPRS_QICSGP, (char *)APN);
                    GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    break;

                // 启动任务并设置接入点APN、用户名和密码
                case GS_GRAPP:
                    GPRS_Usart_SendData((u8 *)GPRS_QIREGAPP, sizeof(GPRS_QIREGAPP) - 1);
                    break;

                // 激活PDP
                case GS_HTTP:
                    GPRS_Usart_SendData((u8 *)GPRS_QIACT, sizeof(GPRS_QIACT) - 1);
                    gprs_data_link_time = LocalTime + 100000;
                    break;

                // 发送地址长度
                case GS_ID:
                    gprs_data_link_time = LocalTime + 3000;
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));

                    //ip
                    if(0 == HttpMode)
                    {
                        sprintf((char *)gprs_share_buff, (char *)GPRS_FILEADR_IP, up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3], up_pro_destport, up_pro_url, up_pro_filename);
                    }
                    else
                    {
                        sprintf((char *)gprs_share_buff, (char *)GPRS_FILEADR_DNS, up_pro_domain, up_pro_destport, up_pro_url, up_pro_filename);
                    }

                    i = (u16)(strlen((char *)gprs_share_buff) - 2);
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                    sprintf((char *)gprs_share_buff, (char *)GPRS_QHTTPURL, i);
                    GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    break;

                // 连接http服务器
                case GS_CONNECT_HTTP:
                    memset((u8 *)gprs_share_buff, 0, sizeof(gprs_share_buff));
                    gprs_data_link_time = LocalTime + 3000;

                    //ip
                    if(0 == HttpMode)
                    {
                        sprintf((char *)gprs_share_buff, (char *)GPRS_FILEADR_IP, up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3], up_pro_destport, up_pro_url, up_pro_filename);
                    }
                    else
                    {
                        sprintf((char *)gprs_share_buff, (char *)GPRS_FILEADR_DNS, up_pro_domain, up_pro_destport, up_pro_url, up_pro_filename);
                    }

                    //服务器地址:端口
                    GPRS_Usart_SendData((u8 *)gprs_share_buff, (u16)strlen((char *)gprs_share_buff));
                    break;

                //  循环获取
                case GS_OPEN_HTTP:
                    gprs_data_link_time = LocalTime + 6000;
                    //获取1000个字节
                    GPRS_Usart_SendData((u8 *)GPRS_QHTTPGET, sizeof(GPRS_QHTTPGET) - 1);
                    break;

                // 与上一条循环  直到结束
                case GS_DOWN_FILE:
                    gprs_data_link_time = LocalTime + 3000 ;

                    if(DTU_UPDATE == downfile_module)
                    {
                        Download_DTUProgram_Delete();
                    }
                    else
                    {
                        Download_MIProgram_Delete();
                    }

                    GPRS_DMA(1);
                    GPRS_Usart_SendData((u8 *)GPRS_QHTTPREAD, sizeof(GPRS_QHTTPREAD) - 1); //读取数据
                    GPRS_state ++;
                    break;

                // 等待下载文件
                case GS_WAIT_IDLE:
                    //20分钟内下载完毕
                    gprs_data_link_time = LocalTime + 20 * 60000;

                    //下载超时
                    if(1 == down_timeout)
                    {
                        //重新下载
                        GPRS_state = GS_CLOSE_HTTP;
                        GPRS_DMA(0);
                        gprs_data_link_time = LocalTime;
                    }
                    else
                    {
                        down_timeout = 1;
                    }

                    //一次点击下载程序  如果失败 最多 下载3次
                    user_http_file_times++;
                    break;

                default:
                    GPRS_state = GS_IDLE;
                    break;
            }
        }
    }
    //5S采集一次CSQ
    else if(GPRS_state == GS_IDLE)
    {
        if(LocalTime > (5000 + gprs_data_link_time))
        {
            gprs_data_link_time = LocalTime;
            GPRS_Usart_SendData((u8 *)GPRS_CHECK_NET_SIGNAL, sizeof(GPRS_CHECK_NET_SIGNAL) - 1);
#ifdef DEBUG1
            printf("req_csq -- GPRS_state:%d\r\n", GPRS_state);
#endif
        }
    }
}
