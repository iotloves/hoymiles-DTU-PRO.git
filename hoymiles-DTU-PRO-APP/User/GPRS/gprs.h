#ifndef __GPRS_H
#define __GPRS_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "led.h"
#define TESTD 1//0
#define ON  0
#define OFF 1
#define SOCKET_LEN  1024     //要发送最大数据长度

#else
#include "stm32f10x.h"
#include "string.h"
#include "define.h"
#endif
#define ROW_MAX_LEN 70
//下载ip
extern  vu8 up_pro_destip[4];

//端口号
extern vu16 up_pro_destport;

extern vu8 up_pro_filename[ROW_MAX_LEN];
extern vu8 up_pro_url[ROW_MAX_LEN];


void GPRS_Server(void); //GPRS 主循环
void GPRS_PWR_GPIO_Config(void);
//void GPRS_GPRS_init(void);





enum GPRS_STEP
{

    GS_CONTROL_POFF,         //电源关                   0
    GS_CONTROL_PON,          //电源开
    GS_CONTROL_KEYOFF,       //开机键拉低
    GS_CONTROL_KEYON,        //开机键拉高
    GS_ATE0,                 //关闭回显
    GS_GMR,                  //软件版本号               5
    GS_CSQ,                  //网络信号值
    GS_CPIN,                 //查询PIN码
    GS_CREG,                 //网络注册
    GS_CCID,                 //查询卡号
    GS_QIDEACT,              //关闭 GPRS/CSD PDP 场景  A
    GS_QIPROMPT,             //关闭发送数据回复>和OK
    GS_CGATT,                //查询GPRS附着是否成功
    GS_QIFGCNT,              //将CONTXT0设为前台CONTEXT。此后相关操作都是针对CONTEXT0进行操作
    GS_QICSGP,               //设置GPRS的APN  移动联通和不设置
    GS_QIREGAPP,             //启动任务并设置接入点APN、用户名和密码
    GS_QIACT,                //激活PDP
    GS_QIDNSIP,              //设置服务器地址使用域名
    GS_QIOPEN,               //连接服务器
    GS_IDLE,                 //空闲                   13
    GS_GET_CONFIG,           //获取配置文件
    GS_SET_CONFIG,           //设置配置文件
    GS_RE_SERVER,            //应答网络命令
    GS_STATUS,               //网络命令执行状态
    GS_SEND_INFO,            //信息                   18
    GS_DTU_ALERTING,         //采集器告警信息
    GS_MI_SUSPEND_ALERTING,  //微逆挂起告警
    GS_SEND_DATA,            //数据
    GS_MI_ALERTING,          //微逆告警信息
    GS_HISTORY_DATA,         //历史数据
    GS_MI_HISTORY_ALERTING,  //微逆历史告警
    GS_SEND_HEART,           //心跳                   1F
    //下载
    GS_CLOSE_HTTP,           // 关闭0服务。
    GS_CLOSE_SOCKET,         //关闭socket连接
    GS_GRAPP,                //
    GS_HTTP,                 //
    GS_ID,                   //发送地址长度
    GS_CONNECT_HTTP,         //连接下载服务器          25
    GS_OPEN_HTTP,            //
    GS_DOWN_FILE,            //
    GS_WAIT_IDLE             //等待下载                28
};




static const unsigned char GPRS_ATE0[]              = "ATE0\r\n";                           //取消回显       ATE1回显
static const unsigned char GPRS_GMR[]               = "AT+GMR\r\n";                         // 软件版本号
static const unsigned char GPRS_CHECK_NET_SIGNAL[]  = "AT+CSQ\r\n";                         //查看网络信号强度
static const unsigned char GPRS_CPIN[]              = "AT+CPIN?\r\n";                       //查询网络状态
static const unsigned char GPRS_VIEW_NET_REG[]      = "AT+CREG?\r\n";                       //查看网络注册状态 确保网络注册成功
static const unsigned char GPRS_CCID[]              =  "AT+QCCID\r\n";                      //SIM卡号



//static const unsigned char GPRS_GSN[]             =  "AT+GSN\r\n";                        //TA 序列号，TA 上报ME 设备的IMEI 号（国际移动台设备识别码


static const unsigned char GPRS_QIPROMPT[]          =  "AT+QIPROMPT=1\r\n";                 //打开发送数据回复>和OK

static const unsigned char GPRS_QIDEACT[]           =  "AT+QIDEACT\r\n";                    //关闭 GPRS/CSD PDP 场景
static const unsigned char GPRS_CGATT[]             =  "AT+CGATT?\r\n";                     // 查询GPRS附着是否成功


static const unsigned char GPRS_QIFGCNT[]           =  "AT+QIFGCNT=0\r\n";                  //将CONTXT0设为前台CONTEXT。此后相关操作都是针对CONTEXT0进行操作

//static const unsigned char GPRS_QICSGP[]          =  "AT+QICSGP=1,\"CMNET\"\r\n";         //设置GPRS的APN 移动联通可不设置

static const unsigned char GPRS_QICSGP[]            =  "AT+QICSGP=1,\"%s\"\r\n";            //设置GPRS的APN 移动联通可不设置   //南非得时候需要打开
static const unsigned char GPRS_QIREGAPP[]          =  "AT+QIREGAPP\r\n";                   // 启动任务并设置接入点APN、用户名和密码
static const unsigned char GPRS_QIACT[]             =  "AT+QIACT\r\n";                      // 激活PDP



//static const unsigned char GPRS_QIDNSIP_0[]       =  "AT+QIDNSIP=0\r\n";                  //0-ip
static const unsigned char GPRS_QIDNSIP[]           =  "AT+QIDNSIP=1\r\n";                  //1-域名
static const unsigned char GPRS_QIOPEN[]            =  "AT+QIOPEN=\"TCP\",\"%s\",\"%d\"\r\n";
//static const unsigned char GPRS_QIOPEN[]          =  "AT+QIOPEN=\"TCP\",\"datacn.hoymiles.com\",\"10081\"\r\n";
//TCP 服务器地址


static const unsigned char GPRS_QISEND[]            = "AT+QISEND=%d\r\n";                   //发送数据长度
//static const unsigned char GPRS_QICLOSE[]         =  "AT+QICLOSE\r\n";                    //关闭连接
//static const unsigned char GPRS_QIDEACT[]         =  "AT+QIDEACT\r\n";                    //关闭 GPRS/CSD PDP 场景
//send data
//static const unsigned char GPRS_QISACK[]          =  "AT+QISACK\r\n";                     //检查发送情况
static const unsigned char GPRS_QHTTPURL[]          =  "AT+QHTTPURL=%d,30\r\n";
static const unsigned char GPRS_FILEADR_IP[]        =  "http://%d.%d.%d.%d:%d%s%s\r\n";
static const unsigned char GPRS_FILEADR_DNS[]       =  "http://%s:%d%s%s\r\n";
//static const unsigned char GPRS_QHTTPGET[]        =  "AT+QHTTPGET=60,60,5000\r\n";
static const unsigned char GPRS_QHTTPGET[]          =  "AT+QHTTPGET=60\r\n";
static const unsigned char GPRS_QHTTPREAD[]         =  "AT+QHTTPREAD=60\r\n";




/*
 TCP IP
static const unsigned char GPRS_ATE0[]              = "ATE0\r\n";                           //取消回显       ATE1回显

static const unsigned char GPRS_GMR[]               =  "AT+GMR\r\n";                        // 软件版本号

static const unsigned char GPRS_QIDEACT[]           =  "AT+QIDEACT\r\n";                    //关闭 GPRS/CSD PDP 场景
static const unsigned char GPRS_QIFGCNT[]           =  "AT+QIFGCNT=0\r\n";                  //将CONTXT0设为前台CONTEXT。此后相关操作都是针对CONTEXT0进行操作
static const unsigned char GPRS_QICSGP[]            =  "AT+QICSGP=1,\"CMNET\"\r\n";         //设置GPRS的APN 移动联通可不设置

static const unsigned char GPRS_VIEW_NET_CPIN[]     = "AT+CPIN?\r\n";                       //查询SIM卡

static const unsigned char GPRS_VIEW_NET_REG[]      = "AT+CREG?\r\n";                       //查看网络注册状态 确保网络注册成功

static const unsigned char GPRS_CGREG[]             = "AT+CGREG?\r\n";                      //GPRS 网络注册状态

static const unsigned char GPRS_QIREGAPP[]          =  "AT+QIREGAPP\r\n";                   // 启动任务并设置接入点APN、用户名和密码
static const unsigned char GPRS_QIACT[]             =  "AT+QIACT\r\n";                      // 激活PDP
static const unsigned char GPRS_QILOCIP[]           =  "AT+QILOCIP\r\n";                    // 获取IP地址

static const unsigned char GPRS_QIDEACT[]           =  "AT+QIDEACT\r\n";                    //关闭 GPRS/CSD PDP 场景

static const unsigned char GPRS_QIOPEN_DNS[]        =  "AT+QIOPEN=\"TCP\",\"datacn.hoymiles.com\",\"10081\"\r\n";

static const unsigned char GPRS_QISEND[]            =  "AT+QISEND=%d\r\n";                  //发送数据长度

static const unsigned char GPRS_QICLOSE[]           =  "AT+QICLOSE\r\n";                    //关闭连接
*/
/*
http 下载流程

AT+QIFGCNT=0
OK
AT+QICSGP=1,"CMNET" //Set APN
OK
AT+QIREGAPP //Optional
OK
AT+QIACT //Optional
OK
AT+QHTTPURL=61,30 //Set the URL
CONNECT
//for example, input 61 bytes:
http://119.3.31.20:80/cfs/hex/1901/28/856982257737535488.hex
OK
AT+QHTTPGET=60 //Send HTTP GET Request
OK
AT+QHTTPREAD=30 //Read the response of HTTP server.
CONNECT
…. //Output the response data of HTTP server to UART.
//for example, UART outputs:
<?xml version="1.0" encoding="utf-8"?>
<string xmlns="https://api.efxnow.com/webservices2.3">Message='helloquectel' ASCII:104 101
108 108 111 113 117 101 99 116 101 108 </string>
OK
AT+QIDEACT //Deactivate GPRS PDP connect.
DEACT OK
Quecte

*/


// AT+QIREGAPP    //启动任务并设置接入点APN、用户名和密码
// AT+QIACT     //激活移动场景
// AT+QILOCIP   //获取本地IP地址，以上三个顺序捆绑使用, QIACT连续失败3次 则重启模块
////////////////////
// AT+QISTAT   //查询当前连接状态
// AT+QISTATE  //查询当前接入的连接状态
// AT+QISSTAT  //查询当前SERVER状态
// AT+QIDNSCFG 配置域名服务器DNS
// AT+QIDNSGIP 域名解析
// AT+GMR     //读取软件版本
// AT+QCCID   //查询当前SIM 卡的CCID 号
////////////////////
// AT+QIFGCNT=0 //将CONTXT0设为前台CONTEXT。此后相关操作都是针对CONTEXT0进行操作
// AT+QICSGP=1,"CMNET" //设置GPRS的APN
//OPEN连续5次失败 则重启模块
// AT+QIOPEN="TCP","192.168.1.11","8081"    //连接服务器地址
// AT+QICLOSE   //关闭当前失败的TCP连接。 发送成功后用这个关闭
// AT+QIDEACT   //断开当前失败的GPRS场景  过了比较长时间用这个关闭
// AT+QISEND    //不指定长度发送
// AT+QISEND=LEN //指定长度发送
// AT+QISACK     //检查发送情况
// AT+QIACT      //
// AT+QHTTPURL=78,30 //发送地址长度
// http://monitor.hoymiles.com:8081/upload/firmwire/MICLHM-0.2.40-0823-1431-H.hex  //发送地址
// AT+QHTTPGET=60   //获取http数据
// AT+QHTTPREAD=50  //读取数据


#endif
