#ifndef __RNG_H__
#define __RNG_H__
#include "stm32f4xx.h"

uint32_t rng(void);

#endif