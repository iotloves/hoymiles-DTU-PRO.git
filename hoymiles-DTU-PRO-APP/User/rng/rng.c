#include "rng.h"

u32 rng(void)
{
    RNG_Cmd(ENABLE);

    /* 等待随机数产生完毕 */
    while(RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET)
    {
    }

    return RNG_GetRandomNumber();
}
