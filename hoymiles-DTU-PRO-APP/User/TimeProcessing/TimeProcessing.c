#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "netconf.h"
#else
#include "stm32f10x.h"
#endif
#include "ServerProtocol.h"
#include "TimeProcessing.h"
#include "string.h"
#include "usart_nrf.h"
#include "wifi.h"
#include "gprs.h"
#include "usart_nrf.h"
#include "RealData.pb.h"
#include "InfomationData.pb.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "pb.h"
#include "NetProtocol.h"
#include "NetProtocolTools.h"
#include "rtc.h"
#include "HW_Version.h"
#include "usart_wifi.h"
#include "Memory.h"
#include "ST_Flash.h"
#include "malloc.h"
#ifdef DTU3PRO
#include "tcp_echoserver.h"
#include "tcp_echoclient.h"
#include "AntiReflux.h"
extern vu32 ETH_Time;

#else
#include "usart_debug.h"
//升级标志位
extern vu8 upgrade;
//本地串口升级标志位
extern vu8 usart1_update;
#endif

#ifdef DEBUG0703
extern volatile char DownBuf[2][DOWNLOAD_SIZE];
extern vu8 DmaUseNum;
#endif
extern vu32 Wifi_http_link_time;
//实时告警包
extern volatile AlarmDataType pRealAlarm[20];

extern vu8 SelfCheckStateFlg;
//告警流水号
extern vu16 WarnSerNub[PORT_LEN];
#ifdef MEMORY_DEBUG_TEXT
vu8 memory_test_save_cnt = 0;
#endif
extern vu8 NetProVer;
//是否存储历史数据标志位
vu32 storge_historical_data_flg;
extern vu8 computer_id[4];
//发送数据长度标志位
extern volatile bool send_len_flg;
//app 标志位
extern vu8 APP_Flg;
//DTU网络命令
extern vu8 Command;
//回复状态
extern vu16 ReceivingSign;
//server 回复帧数
extern vu8 ReqPackageNow;
//当前请求帧数
extern vu8 PackageNow;
//APP当前请求帧数
extern vu8 AppPackageNow;
extern vu32 LocalTime;
extern vu8 GPRS_state;
extern vu8 Wifi_state;
extern vu8 RealAlarmDataNO;
//tcp和http标志位
extern vu8 Tcp_Http_Flg;
#ifdef DTU3PRO
extern vu8 ETH_State;
extern vu16 GetReceivingSign;
extern vu8 GetPackageNow;
extern vu8 GetPackageNum;
extern volatile MeterMajor Meter[];
extern volatile MeterInfomation MeterInfo[];
//电表数量
extern vu8 MeterLinkNum;
#endif
//微型逆变器详细信息
extern volatile InverterDetail MIDetail;
//逆变器主要信息
extern volatile InverterMajor MIMajor[PORT_LEN];
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
//DTU主要信息
extern volatile DtuMajor Dtu3Major;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;

extern vu16 wifi_tcp_socketdata_nub;
//重发计时器
extern vu32 gprs_data_link_time;
extern vu32 Wifi_data_link_time ;
//DMA开始下载标志
extern  vu8 Dmastartdown;
//NRF告警轮训状态
extern volatile AlarmStateType CurAlarmState;
//单包组件数
vu8 ONEC_SOCKET_SEND_DEVICE = 10;
//校验文件失败
vu8 Check_file_failed = 0;
//DTU网络命令
vu8 dtu_command = 0;
//重启dtu标志位
vu8 restart_dtu = 0;
//下载文件的次数
vu8 user_http_file_times = 0;
//发送数据到服务器计数器
vu8 SendData_to_Server_Num = 0;
//下载完成标志位
vu8 DownloadEnd  = 0;
//微逆在线状态计时器
vu32 mi_linkstate_timer = 0;

//微逆并网自检状态超时计时器
vu32 mi_self_check_status = 0;

//下载文件超时计时器
vu32 downfile_timer = 0;
//打包计时器
vu32 package_timer = 0;
//退出app模式定时器
vu32 disconnect_app_timer = 0;
//查询状态定时器
vu32 query_state_timer = 0;
//查询下载文件完成定时器
vu32 query_downfile_timer = 0;
//发送状态定时器
vu32 socket_state_timer = 0;
//心跳包发送定时器
vu32 send_heart_timer = 0;
//数据包发送定时器
vu32 send_data_timer = 0;
//发送数据时间间隔15分钟
vu32 data_interval = 1000 * 6 * 10 * 15;
//5分钟
vu32 disconnect_app = 1000 * 60 * 5;
//3s
vu32 downfile_interval = 3000;
//socket状态
vu32 socket_state = 0;
//微逆全零数据计数器 0 代表全零数据 1代表有数据
vu8 mi_zero_data_timter = 0;
//S-C的当前包数
vu8 package_to_APP = 0;
//S-C的总包数
vu8 total_package_to_APP = 0;
//C-S的当前包数
vu8 package_to_server = 0;
//C-S的总包数
vu8 total_package_to_server = 0;

//告警当前包数
vu8 AlarmPackNub = 0;
//告警总包数
vu8 TotalAlarmPack = 0;

//取出历史数据的总包数
vu8 total_his_data_package = 0;
//下载文件标志位
vu8 downfile = 0;
//微逆升级标志位 DTU升级标志位
vu8 downfile_module = 0;
//32位绝对时间
vu32 rtc_time = 0;
//打包暂存buff
vu8 pack_buff[PACKAGE_ALL][SOCKET_LEN];
//每包的长度
vu16 pack_len[PACKAGE_ALL];
//要发送服务器的数据
vu8 server_socket_send_buff[SOCKET_LEN];
//发送数据长度
vu16 socket_data_len = 0;
//网络命令执行状态 1执行中 2 执行完成
vu8  net_command_status = 0;
//注册帧标志位 为0 代表未发注册帧 为1代表已发注册帧
vu8  Registration_frame = 0;
//写文件状态
vu8  write_status = 3;
//信息帧 数据帧打包标志位
vu8 data_pack_status = 0;
// 历史数据查询标志位
vu8 HistoryPackMark = 0;
// 微逆告警查询标志位
vu8 MiAlertingPackMark = 0;
// 是否存在微逆告警
vu8 MiAlertingNub = 0;
vu32 Reset_Time = 0;
extern volatile App_DataPollState AppState;

extern volatile SystemCfgNew system_cfg;
extern vu16 WarnReqData[26][6];
/***********************************************
** Function name:
** Descriptions:        打包要发送的数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void send_data_to_server(u8 mode)
{
    if(LocalTime  > (package_timer))
    {
        package_timer = LocalTime;

        // 获取配置文件
        if((socket_state & GET_CONFIG_PACK) != 0)
        {
            package_get_config();

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_GET_CONFIG;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_GET_CONFIG;
                    send_len_flg = 0;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_GET_CONFIG;
                    break;
#endif
            }

            socket_state &= GET_CONFIG_OK;
        }
        // 设置配置文件
        else if((socket_state & SET_CONFIG_PACK) != 0)
        {
            package_set_config();

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_SET_CONFIG;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_SET_CONFIG;
                    send_len_flg = 0;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_SET_CONFIG;
                    break;
#endif
            }

            socket_state &= SET_CONFIG_OK;
        }
        // 应答网络命令帧
        else if((socket_state & NET_COMMAND_PACK) != 0)
        {
            package_respond_command();

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_RE_SERVER;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_RE_SERVER;
                    send_len_flg = 0;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_RE_Server;
                    break;
#endif
            }

            socket_state &= NET_COMMAND_OK;
        }
        // 注册帧
        else if((socket_state & REGISTRATION_PACK) != 0)
        {
            package_heartbeat();

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_SEND_HEART;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_SEND_HEART;
                    send_len_flg = 0;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_Send_Heart;
                    break;
#endif
            }

            //重新连接服务器 -- 重新采集微逆挂起告警
            CurAlarmState = AlarmInforUpdate_Server;
            socket_state &= REGISTRATION_OK;
        }
        // 信息帧
        else if(((socket_state & INFO_DATA_PACK) != 0) && (1 == scan_send_status()))
        {
            //还未打包
            if(1 == data_pack_status)
            {
                //从第一帧开始传
                package_to_server = 0;
                //打包完成
                data_pack_status = 0;
            }

            package_my_information(package_to_server);
#ifdef DEBUG
            printf("package_my_info:%d\n\r", package_to_server);
#endif

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_SEND_INFO;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_SEND_INFO;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_Send_Info;
                    break;
#endif
            }
        }
        //采集器告警信息
        else if(((socket_state & DTU_ALERTING_PACK) != 0) && (1 == scan_send_status()))
        {
            package_dtu_alerting();

            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_DTU_ALERTING;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_DTU_ALERTING;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_DTU_ALERTING;
                    break;
#endif
            }

            socket_state &= DTU_ALERTING_OK;
        }
        //微逆挂起告警
        else if(((socket_state & MI_SUSPEND_ALERTING_PACK) != 0) && (1 == scan_send_status()) && (CurAlarmState == InitState) && (Get_FileStatus() == 0))
        {
            if(package_mi_suspend_alerting() == 1)
            {
                switch(mode)
                {
                    case WIFI_MODE:
                        Wifi_state = WIFI_MI_SUSPEND_ALERTING;
                        break;

                    case GPRS_MODE:
                        GPRS_state = GS_MI_SUSPEND_ALERTING;
                        break;
#ifdef DTU3PRO

                    case CABLE_MODE:
                        ETH_State = ETH_MI_SPSPEND_ALERTING;
                        break;
#endif
                }
            }
            else
            {
                socket_state &= MI_SUSPEND_ALERTING_OK;
            }
        }
        // 实时数据帧
        else if(((socket_state & REAL_DATA_PACK) != 0) && (1 == scan_send_status()))
        {
            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_state = WIFI_SEND_DATA;
                    break;

                case GPRS_MODE:
                    GPRS_state = GS_SEND_DATA;
                    send_len_flg = 0;
                    break;
#ifdef DTU3PRO

                case CABLE_MODE:
                    ETH_State = ETH_Send_Data;
                    break;
#endif
            }
        }
        //微逆实时告警信息
        else if(((socket_state & MI_ALERTING_PACK) != 0) && (1 == scan_send_status()))
        {
            if(package_mi_alerting(AlarmPackNub) == 1)
            {
                switch(mode)
                {
                    case WIFI_MODE:
                        Wifi_state = WIFI_MI_ALERTING;
                        break;

                    case GPRS_MODE:
                        GPRS_state = GS_MI_ALERTING;
                        break;
#ifdef DTU3PRO

                    case CABLE_MODE:
                        ETH_State = ETH_MI_ALERTING;
                        break;
#endif
                }
            }
            else
            {
                socket_state &= MI_ALERTING_OK;
            }
        }
        // 网络命令状态回复帧 空闲状态回复
        else if(((socket_state & NET_STATUS_PACK) != 0) && (1 == scan_send_status()))
        {
            //二代网络协议没有网络状态恢复命令
            if(NetProVer == 1)
            {
                //下载文件时先不回复网络命令
                if(DOWNFILE_START == downfile)
                {
                    if(DTU_WAIT_UPDATE == downfile_module)
                    {
                        downfile_module = DTU_UPDATE;
                        //开始下载文件
                        update_handle();
                    }
                }
                //下载结束 继续回复执行状态
                else if(DOWNFILE_COMPLETE == downfile)
                {
                    //清标志位
                    downfile = 0;
                }

                //状态回复
                if(SelfCheckStateFlg == 3)
                {
                    SelfCheckStateFlg = 0;

                    //有未上传的自检状态
                    if(GPST_Data_Num() > 0)
                    {
                        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
                        socket_data_len = Pack_Self_Inspection((u8 *)&server_socket_send_buff);
#ifdef solar
                        printf("480 zijian:%d\n\r", socket_data_len);
#endif

                        if(socket_data_len == 0)
                        {
                            socket_state &= NET_STATUS_OK;
                            return;
                        }
                    }
                    else
                    {
                        socket_state &= NET_STATUS_OK;
                        return;
                    }

                    switch(mode)
                    {
                        case WIFI_MODE:
                            Wifi_state = WIFI_STATUS;
                            break;

                        case GPRS_MODE:
                            GPRS_state = GS_STATUS;
                            send_len_flg = 0;
                            break;
#ifdef DTU3PRO

                        case CABLE_MODE:
                            ETH_State = ETH_Status;
                            break;
#endif
                    }
                }
            }
            else
            {
                package_net_command_status();

                switch(mode)
                {
                    case WIFI_MODE:
                        Wifi_state = WIFI_STATUS;
                        break;

                    case GPRS_MODE:
                        GPRS_state = GS_STATUS;
                        send_len_flg = 0;
                        break;
#ifdef DTU3PRO

                    case CABLE_MODE:
                        ETH_State = ETH_Status;
                        break;
#endif
                }
            }

            socket_state &= NET_STATUS_OK;
        }
        // 历史数据帧
        else if(((socket_state & HISTORY_DATA_PACK) != 0) && (1 == scan_send_status()))
        {
            //从第一帧开始传
            package_to_server = 0;
            socket_data_len = 0;
            total_package_to_server = 0;
            memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

            //没有历史数据
            if(NetProVer == 1)
            {
                socket_data_len = package_history_data(package_to_server, (u8 *)&total_his_data_package, (u8 *)server_socket_send_buff);
            }
            else
            {
                socket_data_len = package_my_history_data(package_to_server, (u8 *)&total_his_data_package, (u8 *)server_socket_send_buff);
            }

            if(0 == socket_data_len)
            {
                socket_state &= HISTORY_DATA_OK;
                clear_server_send_command();
                //发送成功
                Data_List_History_Mark(1);
            }
            else
            {
                switch(mode)
                {
                    case WIFI_MODE:
                        Wifi_state = WIFI_HISTORY_DATA;
                        break;

                    case GPRS_MODE:
                        GPRS_state = GS_HISTORY_DATA;
                        send_len_flg = 0;
                        break;
#ifdef DTU3PRO

                    case CABLE_MODE:
                        ETH_State = ETH_Send_History;
                        break;
#endif
                }

                //不发送心跳帧
                send_heart_timer = LocalTime;
            }

            //socket_state &= HISTORY_DATA_OK;
        }
        //        // 历史微逆告警帧
        //        else if(((socket_state & MI_HISTORY_ALERTING_PACK) != 0) && (1 == scan_send_status()))
        //        {
        //            package_my_history_alerting();
        //            //            //没有历史数据
        //            //            if(0 == package_my_history_data(package_to_server))
        //            //            {
        //            //                socket_state &= HISTORY_DATA_OK;
        //            //                clear_server_send_command();
        //            //                //发送成功
        //            //                Data_List_History_Mark(1);
        //            //            }
        //            //            else
        //            {
        //                switch(mode)
        //                {
        //                    case WIFI_MODE:
        //                        Wifi_state = WIFI_HISTORY_ALERTING;
        //                        break;
        //                    case GPRS_MODE:
        //                        GPRS_state = GS_MI_HISTORY_ALERTING;
        //                        send_len_flg = 0;
        //                        break;
        //#ifdef DTU3PRO
        //                    case CABLE_MODE:
        //                        ETH_State = ETH_Send_History_ALERTING;
        //                        break;
        //#endif
        //                }
        //            }
        //        }
        // 心跳帧
        else if(((socket_state & HEART_BEAT_PACK) != 0) && (1 == scan_send_status()))
        {
            //如果是下载文件成功后
            if(downfile == DOWNFILE_COMPLETE)
            {
                socket_state |= NET_STATUS_PACK;
                downfile = 0;
            }
            else
            {
                package_heartbeat();

                switch(mode)
                {
                    case WIFI_MODE:
                        Wifi_state = WIFI_SEND_HEART;
                        break;

                    case GPRS_MODE:
                        GPRS_state = GS_SEND_HEART;
                        send_len_flg = 0;
                        break;
#ifdef DTU3PRO

                    case CABLE_MODE:
                        ETH_State = ETH_Send_Heart;
                        break;
#endif
                }
            }

            socket_state &= HEART_BEAT_OK;
        }
    }
}


/***********************************************
** Function name:
** Descriptions:        定时器 定时查询改变状态机的状态
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Timed_Query(void)
{
    vu16 i = 0;

    //下载超时 1s
    if((Dmastartdown == 1) && (LocalTime > downfile_timer + downfile_interval))
    {
        downfile_timer = LocalTime;
        Handle_Downfile();
    }

    //并网保护自检状态或轮训完成超时15分钟上传
    if((NetProVer == 1) && ((SelfCheckStateFlg == 2) || (SelfCheckStateFlg == 1 && (LocalTime > mi_self_check_status + data_interval))) && (Get_FileStatus() == 0))
    {
        SelfCheckStateFlg = 3;
        mi_self_check_status = LocalTime;
        //发送自检执行状态
        socket_state |= NET_STATUS_PACK;

        //清楚NRF并网自检状态命令
        for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
        {
            if(MIReal[i].Data.NetCmd == NET_SELF_STAUS)
            {
                MIReal[i].Data.NetCmd = NET_INIT;
                MIReal[i].Data.NetStatus = NET_NOCMD;
            }
        }

        i = 0;
    }

    //查询下载文件完成
    if((1 == DownloadEnd) && (LocalTime > query_downfile_timer))
    {
        query_downfile_timer = LocalTime;
        write_status = Get_FileStatus();

        if((1 == DownloadEnd) && (0 == write_status))
        {
            DownloadEnd = 0;
#ifndef DTU3PRO

            if(1 == checkout(Dtu3Major.Property.netmode_select))
#else
            if(1 == checkout(Netmode_Used))
#endif
            {
#ifndef DTU3PRO
                upgrade = 0;
#endif
            }
            //校验失败重新下载
            else
            {
                Check_file_failed = 1;
            }
        }
    }

    //查询MI在线状态
    if(LocalTime > mi_linkstate_timer + 5000)
    {
        mi_linkstate_timer =  LocalTime;
        Led_MI_LinkState(true);
    }

    //退出app模式
    if((LocalTime > disconnect_app_timer + disconnect_app) && (1 == APP_Flg))
    {
        disconnect_app_timer = LocalTime;
        APP_Flg = 0;

        if((WIFI_TRAN_SET < Wifi_state) && (Wifi_state < WIFI_ENTM))
        {
            Wifi_state = WIFI_E_REQ;
        }
        else
        {
#ifdef DEBUG11
            printf("WIFI_STA:1\r\n");
#endif
            Wifi_state = WIFI_STA;
        }
    }

    //发送状态改变
    if((LocalTime > socket_state_timer) && (0 == APP_Flg))
    {
        socket_state_timer = LocalTime;
#ifndef DTU3PRO
        send_data_to_server(Dtu3Major.Property.netmode_select);
#else
        send_data_to_server(Netmode_Used);
#endif
    }

#ifdef MEMORY_DEBUG_TEXT

    // 15分钟发送数据帧计时器到
    if((LocalTime > (send_data_timer + (1000 * 10 * Dtu3Major.Property.server_send_time))) && (APP_Flg == 0))
    {
#else

    // 15分钟发送数据帧计时器到
    if(LocalTime > (send_data_timer + (1000 * 60 * Dtu3Major.Property.server_send_time)))
    {
#endif
        calendar_obj calendar;
        RTC_GetTimes(&calendar);
        //存储告警序号
        Alarm_Serial_Num_Write((u16 *)WarnSerNub, calendar);

        //如果有实时告警 不满20条也存储一次
        if(RealAlarmDataNO  > 0)
        {
#ifdef DEBUG0619
            printf("1813-Alarm_Data_Write:%d", RealAlarmDataNO);
#endif
            Alarm_Data_Write(0, 1, (AlarmDataType *)pRealAlarm, RealAlarmDataNO);
            RealAlarmDataNO = 0;
            memset((AlarmDataType *)pRealAlarm, 0, sizeof(AlarmDataType) * 20);
        }

        send_data_timer = LocalTime;
        //微逆连接状态
        Led_MI_LinkState(false);

        if(APP_Flg == 0)
        {
            //平时状态 轮询版本号完成 --发送信息帧
            if(2 == Dtu3Detail.Property.PolledVerState)
            {
                //发送信息帧
                socket_state |= INFO_DATA_PACK;
                //打包标志位
                data_pack_status = 1;
                Registration_frame = 1;
                Dtu3Detail.Property.PolledVerState = 0;
            }

            if(1 == scan_nrf_zero_data())
            {
                mi_zero_data_timter = 0x02;
            }
            else
            {
                if(mi_zero_data_timter > 0)
                {
                    mi_zero_data_timter--;
                }
            }

            //打包实时数据准备发送
            if((0xABCD == storge_historical_data_flg) &&
#ifdef DTU3PRO
                    ((mi_zero_data_timter != 0) || (MeterLinkNum > 0)) &&
#else
                    (mi_zero_data_timter != 0) &&
#endif
                    (Dtu3Detail.Property.PortNum != 0) &&
                    /*hzwang_20200422*//*下载时禁止写入历史数据*/
                    (downfile != DOWNFILE_START) &&
                    //MI升级的时候禁止写入
                    (MI_UpdateBusy() != 1))
            {
                //在发送历史数据和实时数据
                //                if(((socket_state & INFO_DATA_PACK) == 0) && (((socket_state & HISTORY_DATA_PACK) != 0) || ((socket_state & REAL_DATA_PACK) != 0)))
                //                {
                //                    clear_server_send_command();
                //                }
                //不是发送信息帧的时候
                if((socket_state & INFO_DATA_PACK) == 0)
                {
                    //从第一帧开始传
                    package_to_server = 0;
                }

                socket_state |= REAL_DATA_PACK;
                //打包数据
                package_my_real_data();
                //发送数据包就不发送心跳包
                send_heart_timer = LocalTime;
                //存储数据
                Store_historical_data();
                //存储剩余告警
                //UsartNrf3_Process_AlarmFlashWriteEvery15Min();
            }
        }
    }

    //发送心跳帧计时器到
    if((LocalTime > (send_heart_timer + 60000)) && (APP_Flg == 0))
    {
        HistoryPackMark = 1;
        MiAlertingPackMark = 1;
        send_heart_timer = LocalTime;
        socket_state |= HEART_BEAT_PACK;
        return;
    }

    //有告警信息 -- 二代没有告警
    if((MiAlertingPackMark == 1) && (NetProVer == 0))
    {
        MiAlertingPackMark = 0;

        if(((socket_state & MI_ALERTING_PACK) == 0) && (Alarm_Data_Num() > 0))
        {
            socket_state |= MI_ALERTING_PACK;
            return;
        }
    }

    //历史数据
    if(HistoryPackMark == 1)
    {
        HistoryPackMark = 0;

        //有历史数据 --空闲状态扫描
        if(scan_send_status() && (socket_state & HISTORY_DATA_PACK) == 0)
        {
            if(history_data_flag() == 1)
            {
                socket_state |= HISTORY_DATA_PACK;
            }
        }
    }

    //查询状态改变
    if(LocalTime > query_state_timer)
    {
        query_state_timer = LocalTime;

        //组网后 轮询版本号完成 --发送信息帧
        if(1 == Dtu3Detail.Property.PolledVerState)
        {
            //发送信息帧
            socket_state |= INFO_DATA_PACK;
            //打包标志位
            data_pack_status = 1;
            Registration_frame = 1;
            Dtu3Detail.Property.PolledVerState = 0;
        }

        switch(Command)
        {
            //重启DTU
            case Cmd_DTURestart:
                //重启标志位置1
                restart_dtu = 1;
                //DTU 网络命令
                dtu_command = 1;
                Reset_Time = LocalTime;
                break;

            //升级DTU
            case Cmd_DTUUpgrade:
                //DTU 网络命令
                dtu_command = 1;
                downfile = DOWNFILE_START;
                //等待更新 回复完成后跟新
                downfile_module = DTU_WAIT_UPDATE;
#ifndef DTU3PRO
                upgrade = 1;
#endif
                break;

            //升级微逆程序
            case Cmd_MIUpgrade:
                downfile = DOWNFILE_START;
                downfile_module = MI_WAIT_UPDATE;
#ifndef DTU3PRO
                upgrade = 1;
#endif
                break;

            //ID组网
            case Cmd_IDNetwork:

            //防逆流组网
            case Cmd_AntiBackflowNetwork:
                //灯灭
                Dtu3Detail.Property.LedState = 1;
                //DTU 网络命令
                dtu_command = 1;
                break;

            //配置并网文件
            case Cmd_ConfigGridFile:

            //MI重启
            case Cmd_MIRestart:

            //采集版本号
            case Cmd_AcqVNum:

            //防盗设置
            case Cmd_AntiTheftSet:

            //MI开机控制
            case Cmd_MIOpen:

            //MI关机控制
            case Cmd_MIShutdown:

            //限功率控制
            case Cmd_LimitedPower:

            //防逆流控制
            case Cmd_AntiBackflow:

            //清除接地故障
            case Cmd_ClearGroundFault:

            //CT设置
            case Cmd_CTSetting:

            //加锁
            case Cmd_Lock:

            //解锁
            case Cmd_Unlock:

            //中止控制命令执行
            case Cmd_AbortControl:
                break;

            default:
                break;
        }

        //清空命令
        Command = 0;

        switch(ReceivingSign)
        {
            //信息帧c->s
            case TAG_InfoDataReq:
#ifdef DTU3PRO
#ifndef CLIENT
                {
                    char *ReturnBuf;
                    u16 ReturnLen = 0;
                    ReturnBuf = mymalloc(2048);
                    memset(ReturnBuf, 0, 2048);
#ifdef DEBUG
                    printf("GetPackageNum %d\n", GetPackageNum);
                    printf("GetPackageNow %d\n", GetPackageNow);
                    ReturnLen = pb_InfoDataRes(ReturnBuf, GetPackageNow);
#else
                    ReturnLen = pb_InfoDataRes(ReturnBuf, 0);
#endif
                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                    myfree(ReturnBuf);
                }

#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                //printf("%X\n", ReceivingSign);
#endif
                break;

            //信息帧应答
            case TAG_InfoDataRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;

                if(PackageNow == package_to_server)
                {
                    //所有帧发送完成
                    if(PackageNow == total_package_to_server - 1)
                    {
                        //停止发送信息帧
                        socket_state &= INFO_DATA_OK;
                        package_to_server = 0;
                    }
                    else
                    {
                        package_to_server++;
                    }
                }
                //发送包和应答包不一致 重新发送
                else
                {
                    package_to_server = 0;
                }

                clear_server_send_command();
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //挂起告警应答
            case TAG_WarnInfoRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;
                clear_server_send_command();

                //DTU 告警
                if((socket_state & DTU_ALERTING_PACK) != 0)
                {
                    socket_state &= DTU_ALERTING_OK;
                }
                //微逆挂起告警告警置成功标志位
                else if((socket_state & MI_SUSPEND_ALERTING_PACK) != 0)
                {
                    AlarmInfo_Success();

                    //如果挂起告警发送完 清除打包标志位
                    if(AlarmInfo_Num() == 0)
                    {
                        socket_state &= MI_SUSPEND_ALERTING_OK;
                    }
                }

                break;

            //告警信息应答
            case TAG_WarnRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;
                clear_server_send_command();
                //标记成功
                Alarm_Data_Success();

                if(AlarmPackNub == PackageNow)
                {
                    //如果两包接着传第二包
                    if((TotalAlarmPack == 2) && (AlarmPackNub == 0))
                    {
                        AlarmPackNub = 1;
                    }
                    //其他情况都初始化当前告警包和总包
                    else
                    {
                        AlarmPackNub = 0;
                        TotalAlarmPack = 0;
                    }
                }

                //如果未上传告警发送完 清除打包标志位
                if(Alarm_Data_Num() == 0)
                {
                    socket_state &= MI_ALERTING_OK;
                }

                break;

            case TAG_WaveRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                break;

            //            case TAG_HisWarnRes:
            //                break;

            //心跳帧c->s
            case TAG_HBReq:
#ifdef DTU3PRO
#ifndef CLIENT
                {
                    char *ReturnBuf;
                    u16 ReturnLen = 0;
                    ReturnBuf = mymalloc(2048);
                    memset(ReturnBuf, 0, 2048);
                    ReturnLen = pb_HBRes(ReturnBuf);
                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                    myfree(ReturnBuf);
                }

#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //心跳帧应答s->c
            case TAG_HBRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;

                //心跳帧
                if(1 == Registration_frame)
                {
                    clear_server_send_command();
#ifdef DTU3PRO
                    GetReceivingSign = ReceivingSign;
#endif
                }
                //注册帧
                else
                {
                    clear_server_send_command();
                    //发送信息帧
                    socket_state |= INFO_DATA_PACK;

                    //DTU告警
                    //                                      socket_state |= DTU_ALERTING_PACK;
                    //二代没有告警
                    if(NetProVer == 0)
                    {
                        //微逆挂起告警
                        socket_state |= MI_SUSPEND_ALERTING_PACK;
#ifdef DEBUG0619
                        printf("MI_SUSPEND_ALERTING_PACK\n\r");
#endif
                    }

                    //打包标志位
                    data_pack_status = 1;
                    Registration_frame = 1;
                }

#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //实时数据帧c->s
            case TAG_RealDataReq:
#ifdef DTU3PRO
#ifndef CLIENT
#ifdef DEBUG
                printf("GetPackageNum %d\n", GetPackageNum);
                printf("GetPackageNow %d\n", GetPackageNow);
#endif
                {
                    char *ReturnBuf;
                    u16 ReturnLen = 0;
                    ReturnBuf = mymalloc(2048);
                    memset(ReturnBuf, 0, 2048);
                    //#ifdef DEBUG
                    ReturnLen = pb_RealDataRes(ReturnBuf, GetPackageNow);
                    //#else
                    //                    ReturnLen = pb_RealDataRespb_InfoDataRes(ReturnBuf, 0);
                    //#endif
                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                    myfree(ReturnBuf);
                }
#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //实时数据帧应答
            case TAG_RealDataRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;
#ifdef DEBUG
                printf("PackageNow %d\n", PackageNow);
                printf("package_to_server %d\n", package_to_server);
#endif

                if(PackageNow == package_to_server)
                {
                    //所有帧发送完成
                    if(PackageNow == total_package_to_server - 1)
                    {
                        package_to_server = 0;
                        //停止发送数据帧
                        socket_state &= REAL_DATA_OK;
                        clear_server_send_command();
                        //置发送成功标志位
                        Data_List_RealTime_Mark(1);
                    }
                    else if(PackageNow < total_package_to_server)
                    {
                        package_to_server++;
                    }
                    else
                    {
                        package_to_server = 0;
                    }
                }
                //发送包和应答包不一致 重新发送
                else
                {
                    package_to_server = 0;
                    Calculation_package(server);
                }

#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //历史数据帧c->s
            case TAG_HistoryDataReq:
#ifdef DTU3PRO
#ifndef CLIENT
                {
                    char *ReturnBuf;
                    u16 ReturnLen = 0;
                    ReturnBuf = mymalloc(2048);
                    memset(ReturnBuf, 0, 2048);
                    ReturnLen = pb_HistoryDataRes(ReturnBuf, 0);
                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                    myfree(ReturnBuf);
                }

#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //历史数据帧应答
            case TAG_HistoryDataRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //清除发送超时时间
                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;

                if(PackageNow == package_to_server)
                {
                    //所有帧发送完成
                    if(PackageNow == total_his_data_package - 1)
                    {
                        package_to_server = 0;
                        //停止发送数据帧
                        socket_state &= HISTORY_DATA_OK;
                        clear_server_send_command();
                        //发送成功
                        Data_List_History_Mark(1);
                    }
                    else if(PackageNow < total_his_data_package)
                    {
                        package_to_server++;
                        socket_data_len = 0;
                        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

                        //二代协议
                        if(NetProVer == 1)
                        {
                            socket_data_len =   package_history_data(package_to_server, (u8 *)&total_his_data_package, (u8 *)server_socket_send_buff);
                        }
                        else
                        {
                            socket_data_len = package_my_history_data(package_to_server, (u8 *)&total_his_data_package, (u8 *)server_socket_send_buff);
                        }

                        //没有历史数据
                        if(0 == socket_data_len)
                        {
                            package_to_server = 0;
                            socket_state &= HISTORY_DATA_OK;
                            clear_server_send_command();
                            //发送成功
                            Data_List_History_Mark(1);
                        }
                    }
                    //异常标记成功
                    else
                    {
                        package_to_server = 0;
                        socket_state &= HISTORY_DATA_OK;
                        clear_server_send_command();
                        //发送成功
                        Data_List_History_Mark(1);
                    }
                }
                //发送包和应答包不一致 重新发送
                else
                {
                    package_to_server = 0;
                    socket_data_len = 0;
                    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

                    if(NetProVer == 1)
                    {
                        socket_data_len = package_history_data(package_to_server, (u8 *)&total_package_to_server, (u8 *)server_socket_send_buff);
                    }
                    else
                    {
                        socket_data_len = package_my_history_data(package_to_server, (u8 *)&total_package_to_server, (u8 *)server_socket_send_buff);
                    }

                    //没有历史数据
                    if(0 == socket_data_len)
                    {
                        package_to_server = 0;
                        socket_state &= HISTORY_DATA_OK;
                        clear_server_send_command();
                        //发送成功
                        Data_List_History_Mark(1);
                    }
                }

#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //获取配置信息   s->c
            case TAG_DevConfigFetchRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                clear_server_send_timer();
                //回复命令
                socket_state |= GET_CONFIG_PACK;
                break;

            //设置配置信息   s->c
            case TAG_DevConfigPutRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                clear_server_send_timer();
                //回复命令
                socket_state |= SET_CONFIG_PACK;
                break;

            //设备命令   s->c
            case TAG_CommandRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //                              clear_server_send_timer();
                //回复命令
                socket_state |= NET_COMMAND_PACK;
                //                if(NetProVer != 1)
                //                {
                //网络命令执行状态(收到网络命令后先回复一包)
                socket_state |= NET_STATUS_PACK;
                //                }
#ifdef DTU3PRO
                //                //#ifdef CLIENT
                //                {
                //                    char *ReturnBuf;
                //                    u16 ReturnLen = 0;
                //                    ReturnBuf = mymalloc(2048);
                //                    memset(ReturnBuf, 0, 2048);
                //                    ReturnLen = pb_CommandReq(ReturnBuf, 0);
                //                    ETH_SendData(ReturnBuf, ReturnLen);
                //                    myfree(ReturnBuf);
                //                }
                //                //#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("%X\n", ReceivingSign);
#endif
                break;

            //设备命令回复 c->s
            case TAG_CommandReq:
#ifdef DTU3PRO
#ifndef CLIENT
                //                {
                //                    char *ReturnBuf;
                //                    u16 ReturnLen = 0;
                //                    ReturnBuf = mymalloc(2048);
                //                    memset(ReturnBuf, 0, 2048);
                //                    ReturnLen = pb_CommandStatusRes(ReturnBuf, 0);
                //                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                //                    myfree(ReturnBuf);
                //                }
#endif
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //网络命令执行状态c->s
            case TAG_CommandStatusReq:
#ifdef DTU3PRO
#ifndef CLIENT
                {
                    char *ReturnBuf;
                    u16 ReturnLen = 0;
                    ReturnBuf = mymalloc(2048);
                    memset(ReturnBuf, 0, 2048);
                    ReturnLen = pb_CommandStatusRes(ReturnBuf, 0);
                    ETH_ServerSendData(ReturnBuf, ReturnLen);
                    myfree(ReturnBuf);
                }

#endif
#endif
                break;

            //网络命令执行状态回复 s->c
            case TAG_CommandStatusRes:
#ifdef DTU3PRO
                ETH_Get_Reply();
#endif
                //                //清除发送超时时间
                //                clear_server_send_timer();
                //清除发送计数器
                SendData_to_Server_Num = 0;

                //二代协议 -- 并网自检状态回复
                if(NetProVer == 1)
                {
                    //标记并网状态发送成功
                    GPST_Data_Success();

                    //未完全发送 继续发送
                    if(GPST_Data_Num() > 0)
                    {
                        SelfCheckStateFlg = 2;
                        //继续发送
                        socket_state |= NET_STATUS_PACK;
                    }
                    //停止发送状态回复帧
                    else
                    {
                        SelfCheckStateFlg = 0;
                        socket_state &= NET_STATUS_OK;
                    }

                    clear_server_send_command();
                    break;
                }

                //下载文件时先不回复网络命令
                if(DOWNFILE_START == downfile)
                {
                    if(DTU_WAIT_UPDATE == downfile_module)
                    {
                        downfile_module = DTU_UPDATE;
                    }
                    else if(MI_WAIT_UPDATE == downfile_module)
                    {
                        downfile_module = MI_UPDATE;
                    }

                    //开始下载文件
                    update_handle();
                }
                //下载结束 继续回复执行状态
                else if(DOWNFILE_COMPLETE == downfile)
                {
                    //网络命令执行中
                    if(1 == net_command_status)
                    {
                        clear_server_send_command();
                        //继续发送
                        socket_state |= NET_STATUS_PACK;
                        //10s发送间隔
                        package_timer = LocalTime + 10 * 1000;
                    }
                    //网络命令执行完成成功或者失败
                    else
                    {
                        //停止发送状态回复帧
                        socket_state &= NET_STATUS_OK;
                        clear_server_send_command();
                        //清标志位
                        downfile = 0;
                    }

                    if(restart_dtu == 1)
                    {
                        //复位
                        NVIC_SystemReset();
                    }
                }
                else
                {
                    //DTU 网络命令
                    if(1 == dtu_command)
                    {
                        //停止发送状态回复帧
                        socket_state &= NET_STATUS_OK;
                        clear_server_send_command();
                        dtu_command = 0;
                    }
                    //网络命令执行完成
                    else if(2 == net_command_status)
                    {
                        //停止发送状态回复帧
                        socket_state &= NET_STATUS_OK;
                        clear_server_send_command();
                        //清除NRF网络命令
                        clean_nrf_netcommand_status();
                    }
                    //网络命令执行中
                    else if(1 == net_command_status)
                    {
                        clear_server_send_command();
                        //继续发送
                        socket_state |= NET_STATUS_PACK;
                    }

                    if(restart_dtu == 1)
                    {
                        //复位
                        NVIC_SystemReset();
                    }
                }

#ifdef DEBUG
                printf("net_comand_status:%d\n\r", net_command_status);
#endif
#ifdef DTU3PRO
                GetReceivingSign = ReceivingSign;
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            /* APP */

            //App获取配置信息请求
            case TAG_AppGetConfigRes :
                disconnect_app_timer = LocalTime;
                package_app_get_information();
                Wifi_state = App_GetConfigRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_GetConfigReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App获取配置信息请求回复 s->c
            case TAG_AppGetConfigReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App设置配置信息请求 c->s
            case TAG_AppSetConfigRes:
                disconnect_app_timer = LocalTime;
                package_app_set_information();
                Wifi_state = App_SetConfigRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_SetConfigReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App获取配置信息请求回复 s->c
            case TAG_AppSetConfigReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App设备信息请求 c->s
            case TAG_AppInfoRes:
                disconnect_app_timer = LocalTime;
                package_app_information(AppPackageNow);
                Wifi_state = App_InfoRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_InfoReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App设备信息请求回复 s->c
            case TAG_AppInfoReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App实时数据请求 c->s
            case TAG_AppRealDataRes:
                disconnect_app_timer = LocalTime;

                //第一包请求
                if(0 == AppPackageNow)
                {
                    package_app_real_data();
                }

                Wifi_state = App_RealDataRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_RealDataReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App实时数据请求回复 s->c
            case TAG_AppRealDataReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App设备事件请求 c->s
            case TAG_AppWarnRes:
                disconnect_app_timer = LocalTime;
                package_app_get_warn();
                Wifi_state = App_WarnRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_WarnReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App告警请求回复 s->c
            case TAG_AppWarnReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App配置ID请求回复 s->c
            case TAG_AppCommandReq:
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App控制命令请求 c->s
            case TAG_AppCommandRes:
#ifdef DEBUG11
                printf("package_app_config_command\r\n");
#endif
                disconnect_app_timer = LocalTime;
                package_app_config_command();
                Wifi_state = App_ConfigCommandRes;
                Wifi_data_link_time = LocalTime - 1000;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_ConfigCommandReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App控制命令状态请求 c->s
            case TAG_AppCommandStatusRes:
                disconnect_app_timer = LocalTime;
                package_app_status_data();
                Wifi_state = App_StatusDataRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_StatusDataReq;
#endif
#endif

                //下载文件时先不回复网络命令
                if(DOWNFILE_START == downfile)
                {
                    if(DTU_WAIT_UPDATE == downfile_module)
                    {
                        downfile_module = DTU_UPDATE;
                    }
                    else if(MI_WAIT_UPDATE == downfile_module)
                    {
                        downfile_module = MI_UPDATE;
                    }

                    //开始下载文件
                    update_handle();
                }
                //下载结束 继续回复执行状态
                else if(DOWNFILE_COMPLETE == downfile)
                {
                    //网络命令执行中
                    if(1 == net_command_status)
                    {
                        clear_server_send_command();
                        //继续发送
                        socket_state |= NET_STATUS_PACK;
                        //10s发送间隔
                        package_timer = LocalTime + 10 * 1000;
                    }
                    //网络命令执行完成成功或者失败
                    else
                    {
                        //停止发送状态回复帧
                        socket_state &= NET_STATUS_OK;
                        clear_server_send_command();
                        //清标志位
                        downfile = 0;
                    }

                    if(restart_dtu == 1)
                    {
                        //复位
                        NVIC_SystemReset();
                    }
                }

                //                else
                //                {
                //                    //DTU 网络命令
                //                    if(1 == dtu_command)
                //                    {
                //                        socket_state &= NET_STATUS_OK;
                //                        //停止发送状态回复帧
                //                        clear_server_send_command();
                //                        dtu_command = 0;
                //                    }
                //                    //网络命令执行完成
                //                    else if(2 == net_command_status)
                //                    {
                //                        //停止发送状态回复帧
                //                        socket_state &= NET_STATUS_OK;
                //                        clear_server_send_command();
                //                        //清除NRF网络命令
                //                        clean_nrf_netcommand_status();
                //                    }
                //                    //网络命令执行中
                //                    else if(1 == net_command_status)
                //                    {
                //                        clear_server_send_command();
                //                        //继续发送
                //                        socket_state |= NET_STATUS_PACK;
                //                    }
                //                }
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_StatusDataReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App心跳帧请求 c->s
            case TAG_AppHBRes:
                disconnect_app_timer = LocalTime;
                package_app_heartbeat();
                Wifi_state = App_HeartBeatRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_HeartBeatReq;
#endif
#endif
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //App心跳帧回复 s->c
            case TAG_AppHBReq:
                //清除APP发送计数器
#ifdef DEBUG
                printf("ReceivingSign:%X\n", ReceivingSign);
#endif
                break;

            //获取配置信息   s->c
            case TAG_AppDevConfigFetchRes:
                package_app_DevConfigFetch_data();
                //回复命令
                Wifi_state = App_DevConfigFetchRes;
                Wifi_data_link_time = LocalTime - 1000;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_DevConfigFetchReq;
#endif
#endif
                break;

            //设置配置信息   s->c
            case TAG_AppDevConfigPutRes:
                package_app_DevConfigPut_data();
                //回复命令
                Wifi_state = App_DevConfigPutRes;
#ifdef DTU3PRO
#ifdef DEBUG
                ETH_State = ETH_App_DevConfigPutReq;
#endif
#endif
                break;

            default:
                break;
        }

        ReceivingSign = 0;
    }
}

/***********************************************
** Function name:
** Descriptions:        清除发送命令
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void clear_server_send_command(void)
{
#ifndef DTU3PRO

    switch(Dtu3Major.Property.netmode_select)
#else
    switch(Netmode_Used)
#endif
    {
        //切换状态机到空闲状态
        case WIFI_MODE:
            Wifi_state =  WIFI_WAIT;
            break;

        case GPRS_MODE:
            GPRS_state = GS_IDLE;
            break;
#ifdef DTU3PRO

        case CABLE_MODE:
            ETH_State =  ETH_WAIT;
            break;
#endif

        default:
#ifndef DTU3PRO
            //重新判断上网方式
            Connect_Server_Mode();
#endif
            break;
    }
}

/***********************************************
** Function name:
** Descriptions:        清除各个模块发送超时时间 -- 30S 超时
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void clear_server_send_timer(void)
{
    switch(Dtu3Major.Property.netmode_select)
    {
        //切换状态机到空闲状态
        case WIFI_MODE:
            Wifi_data_link_time = LocalTime;
            break;

        case GPRS_MODE:
            gprs_data_link_time = LocalTime;
            break;
#ifdef DTU3PRO

        case CABLE_MODE:
            ETH_Time =  LocalTime - ETH_Wite_Time;
            break;
#endif
    }
}


/***********************************************
** Function name:
** Descriptions:        发送空闲状态扫描
** input parameters:    无
** output parameters:   无
** Returned value:    0代表繁忙   1代表空闲
*************************************************/
u8 scan_send_status(void)
{
    vu8 status = 0;
#ifndef DTU3PRO

    switch(Dtu3Major.Property.netmode_select)
#else
    switch(Netmode_Used)
#endif
    {
        case WIFI_MODE:  //wifi
            if(WIFI_WAIT == Wifi_state)
            {
                status = 1;
            }

            break;

        case GPRS_MODE:
            if(GS_IDLE == GPRS_state)
            {
                status = 1;
            }

            break;
#ifdef DTU3PRO

        case CABLE_MODE:
            if(ETH_State == ETH_WAIT)
            {
                status = 1;
            }

            break;
#endif

        default:
#ifndef DTU3PRO
            //重新判断上网方式
            Connect_Server_Mode();
#endif
            break;
    }

    return status;
}

/***********************************************
** Function name:
** Descriptions:        打包回复获取配置文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_get_config(void)
{
    //总包数
    static vu8 package_all = 0;
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    //增加多包 需要改
    socket_data_len = pb_DevConfigFetchReq((char *)server_socket_send_buff, (u8 *)&package_all, PackageNow);

    //当前包数大于 错误包
    if(package_all <= PackageNow)
    {
    }
}
/***********************************************
** Function name:
** Descriptions:        打包回复设置配置文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_set_config(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_DevConfigPutReq((char *)server_socket_send_buff);
}

/***********************************************
** Function name:
** Descriptions:        打包回复网络命令
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_respond_command(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

    if(NetProVer == 1)
    {
        socket_data_len = Reply_Server_Command((u8 *)server_socket_send_buff, PackageNow);
    }
    else
    {
        socket_data_len = pb_CommandReq((char *)server_socket_send_buff, PackageNow);
    }
}

/***********************************************
** Function name:
** Descriptions:        打包网络命令执行状态
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_net_command_status(void)
{
    // 扫描命令执行状态
    net_command_status =  scan_nrf_netcommand_status();
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_CommandStatusReq((char *)server_socket_send_buff, 1, 0);
}
/***********************************************
** Function name:
** Descriptions:        打包信息帧
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_my_information(u8 PackNum)
{
    //微型逆变器详细信息
    volatile InverterDetail MIDetail[ONEC_SOCKET_SEND_DEVICE];
    //发送包的组件数
    vu8 pvcount = 0;
#ifdef DTU3PRO
    vu8 MeterCount = 0;
    vu8 MeterOff = 0;
#endif
    //组件偏移量
    vu8 pvoff = 0;
    //清空结构体
    memset((InverterDetail *)MIDetail, 0, sizeof(MIDetail));
    //清空发送buff
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    //计算包数
    Calculation_package(server);
    //获取打包时间
    rtc_time = RTC_Getsecond();
#ifndef DTU3PRO

    //最后一包
    if(PackNum + 1 == total_package_to_server)
    {
        //最后一包的组件数
        pvcount = Dtu3Detail.Property.PortNum - PackNum * ONEC_SOCKET_SEND_DEVICE;
    }
    else
    {
        pvcount = ONEC_SOCKET_SEND_DEVICE;
    }

    //读微逆信息
    InverterDetail_Read((InverterDetail *)MIDetail, PackNum * ONEC_SOCKET_SEND_DEVICE, pvcount);

    //不循环打包 边发送边打包
    //打包到结构体
    if(NetProVer == 1)
    {
        socket_data_len = Pack_Information(total_package_to_server, PackNum, rtc_time, 0, 0, pvcount, PackNum * ONEC_SOCKET_SEND_DEVICE, (u8 *)server_socket_send_buff, (InverterDetail *)MIDetail);
    }
    else
    {
        socket_data_len = pb_InfoDataReq(total_package_to_server, PackNum, rtc_time, 0, 0, pvcount, PackNum * ONEC_SOCKET_SEND_DEVICE, (char *)server_socket_send_buff, (InverterDetail *)MIDetail);
    }

#else

    if(NetProVer == 1)
    {
        //最后一包
        if(PackNum + 1 == total_package_to_server)
        {
            //最后一包的组件数
            pvcount = Dtu3Detail.Property.PortNum - PackNum * ONEC_SOCKET_SEND_DEVICE;
        }
        else
        {
            pvcount = ONEC_SOCKET_SEND_DEVICE;
        }

        //读微逆信息
        InverterDetail_Read((InverterDetail *)MIDetail, PackNum * ONEC_SOCKET_SEND_DEVICE, pvcount);
        socket_data_len = Pack_Information(total_package_to_server, PackNum, rtc_time, 0, 0, pvcount, PackNum * ONEC_SOCKET_SEND_DEVICE, (u8 *)server_socket_send_buff, (InverterDetail *)MIDetail);
    }
    else
    {
        //第一包
        if(PackNum == 0)
        {
            if(total_package_to_server == 1)
            {
                MeterCount = MeterLinkNum;
                MeterOff = 0;
                pvcount = (u8)Dtu3Detail.Property.PortNum;
            }
            else
            {
                MeterCount = MeterLinkNum;
                MeterOff = 0;
                //第一包的组件数
                pvcount = ONEC_SOCKET_SEND_DEVICE - MeterCount;
            }
        }
        else
        {
            if(MeterLinkNum > PackNum * ONEC_SOCKET_SEND_DEVICE)
            {
                if(MeterLinkNum > ONEC_SOCKET_SEND_DEVICE)
                {
                    if((MeterLinkNum - (PackNum + 1) * ONEC_SOCKET_SEND_DEVICE) >= 0)
                    {
                        MeterCount = ONEC_SOCKET_SEND_DEVICE;
                        MeterOff = PackNum * ONEC_SOCKET_SEND_DEVICE;
                    }
                    else
                    {
                        MeterCount = MeterLinkNum - PackNum * ONEC_SOCKET_SEND_DEVICE;
                        MeterOff = PackNum * ONEC_SOCKET_SEND_DEVICE;
                    }
                }
                else
                {
                    MeterCount = MeterLinkNum;
                }

                pvcount = ONEC_SOCKET_SEND_DEVICE - MeterCount;
            }
            else
            {
                MeterCount = 0;
                MeterOff = 0;

                if(((PackNum + 1)*ONEC_SOCKET_SEND_DEVICE + MeterLinkNum) > Dtu3Detail.Property.PortNum)
                {
                    //最后一包的组件数
                    pvcount = (u8)(Dtu3Detail.Property.PortNum + MeterLinkNum - PackNum * ONEC_SOCKET_SEND_DEVICE);
                }
                else
                {
                    pvcount = ONEC_SOCKET_SEND_DEVICE;
                }
            }
        }

        if((PackNum * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum) > 0)
        {
            pvoff = PackNum * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum;
        }
        else
        {
            pvoff = 0;
        }

        //读微逆信息
        InverterDetail_Read((InverterDetail *)MIDetail, PackNum * ONEC_SOCKET_SEND_DEVICE, pvcount);
        socket_data_len = pb_InfoDataReq(total_package_to_server, PackNum, rtc_time, MeterCount, MeterOff, 0, 0, pvcount, pvoff, (char *)server_socket_send_buff, (InverterDetail *)MIDetail); //打包到结构体
    }

#endif
}

/***********************************************
** Function name:
** Descriptions:        打包采集告警信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_dtu_alerting(void)
{
    vu16 Alarm_nub = 0;
    vu32 Alarm_time = 0;
    AlarmDataType *AlarmDataBuf = NULL;
    AlarmDataBuf = mymalloc(20 * sizeof(AlarmDataType));
    //    if(AlarmDataBuf == NULL)
    //    {
    //        return;
    //    }
    memset(AlarmDataBuf, 0, 20 * sizeof(AlarmDataType));
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    //    socket_data_len = pb_WarnReq((char *)server_socket_send_buff, Alarm_time, AlarmDataBuf, Alarm_nub, 0);
    myfree(AlarmDataBuf);
}

/***********************************************
** Function name:
** Descriptions:        打包微逆挂起告警信息
** input parameters:    无
** output parameters:   无
** Returned value:      0 无告警 1有告警
*************************************************/
u8 package_mi_suspend_alerting(void)
{
    vu16 Alarm_nub = 0;
    vu32 Alarm_time = 0;
    AlarmDataType *AlarmDataBuf = NULL;
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

    //有告警
    if(AlarmInfo_Num() > 0)
    {
#if DEBUG0619
        printf("AlarmInfo_Num:%d\n\r", AlarmInfo_Num());
#endif
        AlarmDataBuf = mymalloc(20 * sizeof(AlarmDataType));
        //        if(AlarmDataBuf == NULL)
        //        {
        //            return 0;
        //        }
        memset(AlarmDataBuf, 0, 20 * sizeof(AlarmDataType));
        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
        Alarm_nub = AlarmInfo_Read(AlarmDataBuf);
        socket_data_len = pb_WarnInfoReq((char *)server_socket_send_buff, 1, 0, Alarm_time, AlarmDataBuf, Alarm_nub, 1);
        myfree(AlarmDataBuf);

        if(socket_data_len > 0)
        {
            return 1;
        }
        else
        {
            AlarmInfo_Success();
            return 0;
        }
    }
    else
    {
        return 0;
    }
}
/***********************************************
** Function name:
** Descriptions:        打包实时数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_my_real_data(void)
{
    vu8 i = 0;
#ifdef DTU3PRO
    vu8 MeterCount = 0;
    vu8 MeterOff = 0;
#endif
    //发送包的组件数
    vu8 pvcount = 0;
    //组件偏移量
    vu8 pvoff = 0;
    //清空打包buff
    memset((u8 *)pack_buff, 0, sizeof(pack_buff));
    //清空长度
    memset((u8 *)pack_len, 0, sizeof(pack_len));
#ifdef DTU3PRO

    //计算包数
    if(mi_zero_data_timter != 0)
    {
        Calculation_package(server);
    }
    else if(MeterLinkNum > 0)
    {
        total_package_to_server = 1;
    }

#else
    Calculation_package(server);
#endif
    //获取打包时间
    rtc_time = RTC_Getsecond();
    //判断微逆是否在线
    Judge_mi_link_status();
#ifndef DTU3PRO

    for(i = 0; i < total_package_to_server; i++)
    {
        if((i + 1)*ONEC_SOCKET_SEND_DEVICE > Dtu3Detail.Property.PortNum)
        {
            //最后一包的组件数
            pvcount = Dtu3Detail.Property.PortNum - i * ONEC_SOCKET_SEND_DEVICE;
        }
        else
        {
            pvcount = ONEC_SOCKET_SEND_DEVICE;
        }

        pvoff = i * ONEC_SOCKET_SEND_DEVICE;

        //打包到结构体
        if(NetProVer == 1)
        {
            pack_len[i] = Pack_Realdata(total_package_to_server, i, rtc_time, 0, 0, pvcount, pvoff, (u8 *)pack_buff[i]);
        }
        else
        {
            pack_len[i] = RealDataReq_Encode(total_package_to_server, i, rtc_time, 0, 0, pvcount, pvoff, (char *)pack_buff[i]);
        }

#else

    if(total_package_to_server > 11)
    {
        total_package_to_server = 11;
    }

    for(i = 0; i < total_package_to_server; i++)
    {
        //打包到结构体
        if(NetProVer == 1)
        {
            if((i + 1)*ONEC_SOCKET_SEND_DEVICE > Dtu3Detail.Property.PortNum)
            {
                //最后一包的组件数
                pvcount = Dtu3Detail.Property.PortNum - i * ONEC_SOCKET_SEND_DEVICE;
            }
            else
            {
                pvcount = ONEC_SOCKET_SEND_DEVICE;
            }

            pvoff = i * ONEC_SOCKET_SEND_DEVICE;
            pack_len[i] = Pack_Realdata(total_package_to_server, i, rtc_time, 0, 0, pvcount, pvoff, (u8 *)pack_buff[i]);
        }
        else
        {
            //第一包
            if(i == 0)
            {
                if(total_package_to_server == 1)
                {
                    MeterCount = MeterLinkNum;
                    MeterOff = 0;
                    pvcount = (u8)Dtu3Detail.Property.PortNum;
                }
                else
                {
                    MeterCount = MeterLinkNum;
                    MeterOff = 0;
                    //第一包的组件数
                    pvcount = ONEC_SOCKET_SEND_DEVICE - MeterCount;
                }
            }
            else
            {
                if(MeterLinkNum > i * ONEC_SOCKET_SEND_DEVICE)
                {
                    if(MeterLinkNum > ONEC_SOCKET_SEND_DEVICE)
                    {
                        if((MeterLinkNum - (i + 1) * ONEC_SOCKET_SEND_DEVICE) >= 0)
                        {
                            MeterCount = ONEC_SOCKET_SEND_DEVICE;
                            MeterOff = i * ONEC_SOCKET_SEND_DEVICE;
                        }
                        else
                        {
                            MeterCount = MeterLinkNum - i * ONEC_SOCKET_SEND_DEVICE;
                            MeterOff = i * ONEC_SOCKET_SEND_DEVICE;
                        }
                    }
                    else
                    {
                        MeterCount = MeterLinkNum;
                    }

                    pvcount = ONEC_SOCKET_SEND_DEVICE - MeterCount;
                }
                else
                {
                    MeterCount = 0;
                    MeterOff = 0;

                    if(((i + 1)*ONEC_SOCKET_SEND_DEVICE + MeterLinkNum) > Dtu3Detail.Property.PortNum)
                    {
                        //最后一包的组件数
                        pvcount = (u8)(Dtu3Detail.Property.PortNum + MeterLinkNum - i * ONEC_SOCKET_SEND_DEVICE);
                    }
                    else
                    {
                        pvcount = ONEC_SOCKET_SEND_DEVICE;
                    }
                }
            }

            if((i * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum) > 0)
            {
                pvoff = i * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum;
            }
            else
            {
                pvoff = 0;
            }

            //打包到结构体
            pack_len[i] = (u16)RealDataReq_Encode(total_package_to_server, i, rtc_time, MeterCount, MeterOff, 0, 0, pvcount, pvoff, (char *)pack_buff[i]);
        }

#endif
    }

    //打包完清MI数据
    ClearMIRealData();
}

/***********************************************
** Function name:
** Descriptions:        打包数据的时候根据微逆的连接状态判断是否需要清除数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void ClearMIRealData(void)
{
    vu16 i;

    for(i = 0; i < PORT_LEN; i++)
    {
        //微逆离线
        if((MIReal[i].Data.LinkState & MI_CONNECT) == 0)
        {
            MIReal[i].Data.LinkState &= MI_SERVER_DISCONNECT;
            memset((u8 *)MIReal[i].Data.PVVol, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.PVCur, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.GridVol, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Freque, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Power, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Temper, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Run_Status, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Fault_Code, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.Fault_Num, 0, 2 * sizeof(u8));
            memset((u8 *)MIReal[i].Data.PVPower, 0, 2 * sizeof(u8));
            MIReal[i].Data.Real_Time  = 0;
        }

        MIReal[i].Data.LinkState &= MI_DISCONNECT;
    }

    PortNO = 0;
}

void Judge_mi_link_status(void)
{
    vu16 i;

    for(i = 0; i < PORT_LEN; i++)
    {
        //微逆在线
        if((MIReal[i].Data.LinkState & MI_CONNECT) != 0)
        {
            MIReal[i].Data.LinkState |= MI_SERVER_CONNECT;
        }
    }
}
/***********************************************
** Function name:
** Descriptions:        打包采集告警信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 package_mi_alerting(u8 PackageNub)
{
    vu16 Alarm_nub = 0;
    vu32 Alarm_time = 0;
    AlarmDataType *AlarmDataBuf = NULL;
    AlarmDataBuf = mymalloc(20 * sizeof(AlarmDataType));
    //    if(AlarmDataBuf == NULL)
    //    {
    //        return 0;
    //    }
    memset(AlarmDataBuf, 0, 20 * sizeof(AlarmDataType));
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    Alarm_nub = Alarm_Data_Read(PackageNub, (u8 *)&TotalAlarmPack, AlarmDataBuf, (u32 *)&Alarm_time);
    socket_data_len = pb_WarnReq((char *)server_socket_send_buff, TotalAlarmPack, PackageNub, Alarm_time, AlarmDataBuf, Alarm_nub, 1);
    myfree(AlarmDataBuf);

    if(socket_data_len > 0)
    {
        return 1;
    }
    else
    {
        Alarm_Data_Failure();
        return 0;
    }
}
/***********************************************
** Function name:
** Descriptions:        是否有历史数据
** input parameters:    无
** output parameters:   无
** Returned value:      有历史数据返回1 没有历史数据返回0
*************************************************/
u8 history_data_flag(void)
{
    vu16 len = 0;
    //取出历史数据
    //len = Historical_Presence();
    len = History_Scanning();

    //没有历史数据
    if(0 == len)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
/***********************************************
** Function name:
** Descriptions:        存储历史数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Store_historical_data(void)
{
    vu8 i = 0;

    for(i = 0; i < total_package_to_server; i++)
    {
        //打包完存储
        RealTime_Data_Pack(i, total_package_to_server, (char *)pack_buff[i], pack_len[i]);
    }

#ifdef MEMORY_DEBUG_TEXT
    memory_test_save_cnt++;
#endif
}
/***********************************************
** Function name:
** Descriptions:        打包历史数据
** input parameters:    当前包数，总包数，要发送的buff
** output parameters:   无
** Returned value:      有历史数据返回1 没有历史数据返回0
*************************************************/
u16 package_my_history_data(u8 package_now, u8 *package_nub, u8 *dat)
{
    volatile char *his_buffer;
    vu32 his_data_len = 0;
    his_buffer = mymalloc(SOCKET_LEN);
    //    if(his_buffer == NULL)
    //    {
    //        return 0;
    //    }
    memset((char *)his_buffer, 0, SOCKET_LEN);
    //total_his_data_package = 0;
    //取出历史数据
    his_data_len = History_Data_Unpack(package_now, package_nub, (char *)his_buffer);

    //没有历史数据
    if(his_data_len == 0)
    {
        myfree((char *)his_buffer);
        return 0;
    }
    else
    {
        his_data_len = pb_HistoryDataReq((char *)dat, (char *)his_buffer, (u16)his_data_len);
    }

    myfree((char *)his_buffer);
    return (u16)his_data_len;
}

/***********************************************
** Function name:
** Descriptions:        打包微逆历史告警信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
//void package_my_history_alerting(void)
//{
//    vu16 Alarm_nub = 0;
//    vu32 Alarm_time = 0;
//    AlarmDataType *AlarmDataBuf = NULL;
//    AlarmDataBuf = mymalloc(20 * sizeof(AlarmDataType));
//    if(AlarmDataBuf == NULL)
//    {
//        return;
//    }
//    memset(AlarmDataBuf, 0, 20 * sizeof(AlarmDataType));
//    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
//    Alarm_time = RTC_GetWorldSecond(Dtu3Detail.Property.timezone);
//    Alarm_nub = Alarm_History_Data_Read(AlarmDataBuf, (u32 *)&Alarm_time);
//    socket_data_len = pb_HisWarnReq((char *)server_socket_send_buff,  Alarm_time, AlarmDataBuf, Alarm_nub, 1);
//    myfree(AlarmDataBuf);
//}
/***********************************************
** Function name:
** Descriptions:        打包心跳帧
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_heartbeat(void)
{
#ifdef DEBUG
    printf("package_heartbeat\n");
#endif
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

    if(NetProVer == 1)
    {
        socket_data_len = Pack_Heartbeat((u8 *)server_socket_send_buff);
    }
    else
    {
        socket_data_len = pb_HBReq((char *)server_socket_send_buff);
    }
}

/***********************************************
** Function name:
** Descriptions:        升级处理
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void update_handle(void)
{
#ifndef DTU3PRO

    switch(Dtu3Major.Property.netmode_select)
#else
    switch(Netmode_Used)
#endif
    {
        case WIFI_MODE:
            Tcp_Http_Flg = 1;
            break;

        case GPRS_MODE:
            GPRS_state = GS_CLOSE_HTTP;
            break;
#ifdef DTU3PRO

        case CABLE_MODE:
            ETH_State =  ETH_HTTP_Download;
            break;
#endif

        default:
#ifndef DTU3PRO
            Connect_Server_Mode();
#endif
            break;
    }
}


/**********************************************
*                      App                    *
***********************************************/
/***********************************************
** Function name:
** Descriptions:        打包App获取配置信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_get_information(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppGetConfigReq((char *)server_socket_send_buff);
}

/***********************************************
** Function name:
** Descriptions:        打包App设置配置信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_set_information(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppSetConfigReq((char *)server_socket_send_buff);
}

/***********************************************
** Function name:
** Descriptions:        打包App设备信息
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_information(u8 PackNum)
{
    //微型逆变器详细信息
    volatile InverterDetail MIDetail[ONEC_SOCKET_SEND_DEVICE];
    //发送包的组件数
    vu8 pvcount = 0;
    //清空结构体
    memset((InverterDetail *)MIDetail, 0, sizeof(MIDetail));
    //清空发送buff
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    //计算包数
    Calculation_package(app);
    //获取打包时间
    rtc_time = RTC_Getsecond();

    //最后一包
    if(PackNum + 1 == total_package_to_APP)
    {
        //最后一包的组件数
        pvcount = (u8)(Dtu3Detail.Property.PortNum - PackNum * ONEC_SOCKET_SEND_DEVICE);
    }
    else
    {
        pvcount = ONEC_SOCKET_SEND_DEVICE;
    }

    //读微逆信息
    InverterDetail_Read((InverterDetail *)MIDetail, PackNum * ONEC_SOCKET_SEND_DEVICE, pvcount);
    //不循环打包 边发送边打包
    //打包到结构体
#ifndef DTU3PRO
    socket_data_len = pb_AppInfoReq(total_package_to_APP, PackNum, rtc_time, 0, 0, pvcount, PackNum * ONEC_SOCKET_SEND_DEVICE, (char *)server_socket_send_buff, (InverterDetail *)MIDetail);
#else
    socket_data_len = pb_AppInfoReq(total_package_to_APP, PackNum, rtc_time, 0, 0, 0, 0, pvcount, PackNum * ONEC_SOCKET_SEND_DEVICE, (char *)server_socket_send_buff, (InverterDetail *)MIDetail); //打包到结构体
#endif
}
/***********************************************
** Function name:
** Descriptions:        APP 打包实时数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_real_data(void)
{
    vu8 i = 0;
#ifdef DTU3PRO
    vu8 MeterCount = 0;
    vu8 MeterOff = 0;
#endif
    //发送包的组件数
    vu8 pvcount = 0;
    //组件偏移量
    vu8 pvoff = 0;
    //清空打包buff
    memset((u8 *)pack_buff, 0, sizeof(pack_buff));
    //清空长度
    memset((u8 *)pack_len, 0, sizeof(pack_len));
    //计算包数
    Calculation_package(app);
    //获取打包时间
    rtc_time = RTC_Getsecond();

    if((APP_Flg == 1) && ((AppState == PollSigleMi) || (AppState == ManuSigleLossRate)))
    {
        total_package_to_APP = 1;
#ifndef DTU3PRO
        pack_len[0] = pb_AppRealDataReq(total_package_to_APP, 0, rtc_time, 0, 0, pvcount, pvoff, (char *)pack_buff[0]);
#else
        pack_len[0] = pb_AppRealDataReq(total_package_to_APP, 0, rtc_time, 0, 0, 0, 0, pvcount, pvoff, (char *)pack_buff[0]); //打包到结构体
#endif
    }
    else
    {
#ifndef DTU3PRO

        for(i = 0; i < total_package_to_APP; i++)
        {
            if((i + 1)*ONEC_SOCKET_SEND_DEVICE > Dtu3Detail.Property.PortNum)
            {
                //最后一包的组件数
                pvcount = Dtu3Detail.Property.PortNum - i * ONEC_SOCKET_SEND_DEVICE;
            }
            else
            {
                pvcount = ONEC_SOCKET_SEND_DEVICE;
            }

            pvoff = i * ONEC_SOCKET_SEND_DEVICE;
            //打包到结构体
            pack_len[i] = pb_AppRealDataReq(total_package_to_APP, i, rtc_time, 0, 0, pvcount, pvoff, (char *)pack_buff[i]);
#else

        for(i = 0; i < total_package_to_APP; i++)
        {
            if(i + 1 == total_package_to_APP)
            {
                if(total_package_to_APP == 1)
                {
                    MeterCount = MeterLinkNum;
                    MeterOff = 0;
                    pvcount = (u8)Dtu3Detail.Property.PortNum;
                }
                else
                {
                    pvcount = (u8)(Dtu3Detail.Property.PortNum + MeterLinkNum - i * ONEC_SOCKET_SEND_DEVICE);
                }
            }
            else
            {
                if(MeterLinkNum > i * ONEC_SOCKET_SEND_DEVICE)
                {
                    if(MeterLinkNum > ONEC_SOCKET_SEND_DEVICE)
                    {
                        if((MeterLinkNum - (i + 1) * ONEC_SOCKET_SEND_DEVICE) >= 0)
                        {
                            MeterCount = ONEC_SOCKET_SEND_DEVICE;
                            MeterOff = i * ONEC_SOCKET_SEND_DEVICE;
                        }
                        else
                        {
                            MeterCount = MeterLinkNum - i * ONEC_SOCKET_SEND_DEVICE;
                            MeterOff = i * ONEC_SOCKET_SEND_DEVICE;
                        }
                    }
                    else
                    {
                        MeterCount = MeterLinkNum;
                    }

                    pvcount = ONEC_SOCKET_SEND_DEVICE - MeterCount;
                }
                else
                {
                    MeterCount = 0;
                    MeterOff = 0;
                    pvcount = ONEC_SOCKET_SEND_DEVICE;
                }
            }

            if((i * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum) > 0)
            {
                pvoff = i * ONEC_SOCKET_SEND_DEVICE - MeterLinkNum;
            }
            else
            {
                pvoff = 0;
            }

            pack_len[i] = pb_AppRealDataReq(total_package_to_APP, i, rtc_time, MeterCount, MeterOff, 0, 0, pvcount, pvoff, (char *)pack_buff[i]); //打包到结构体
#endif
        }
    }
}

/***********************************************
** Function name:
** Descriptions:        打包App控制命令请求
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_config_command(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppCommandReq((char *)server_socket_send_buff, 0);
}

/***********************************************
** Function name:
** Descriptions:        打包App心跳请求
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_heartbeat(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppHBReq((char *)server_socket_send_buff);
}
/***********************************************
** Function name:
** Descriptions:        打包App控制命令状态
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_status_data(void)
{
    //组件偏移量
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppCommandStatusReq((char *)server_socket_send_buff, 1, 0);
}

/***********************************************
** Function name:
** Descriptions:        打包App控制命令状态
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_DevConfigFetch_data(void)
{
    vu8 package_nub = 0;
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppDevConfigFetchReq((char *)server_socket_send_buff, (u8 *)&package_nub, AppPackageNow);
}
/***********************************************
** Function name:
** Descriptions:        打包App控制命令状态
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_DevConfigPut_data(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppDevConfigPutReq((char *)server_socket_send_buff, AppPackageNow);
}

/***********************************************
** Function name:
** Descriptions:        打包App设备告警请求
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void package_app_get_warn(void)
{
    memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));
    socket_data_len = pb_AppWarnReq((char *)server_socket_send_buff, 1);
}
/***********************************************
** Function name:
** Descriptions:        计算组件需要发送的包数
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/

void Calculation_package(u8 object)
{
#ifndef DTU3PRO

    if(object == server)
    {
        if(Dtu3Detail.Property.PortNum < ONEC_SOCKET_SEND_DEVICE)
        {
            total_package_to_server = 1;
        }
        else
        {
            //共多少包
            total_package_to_server = 1 + ((Dtu3Detail.Property.PortNum - 1) / ONEC_SOCKET_SEND_DEVICE);
        }
    }
    else if(object == app)
    {
        if(Dtu3Detail.Property.PortNum < ONEC_SOCKET_SEND_DEVICE)
        {
            total_package_to_APP = 1;
        }
        else
        {
            total_package_to_APP = 1 + ((Dtu3Detail.Property.PortNum - 1) / ONEC_SOCKET_SEND_DEVICE);
        }
    }

#else

    if(object == server)
    {
        if((Dtu3Detail.Property.PortNum + MeterLinkNum) < ONEC_SOCKET_SEND_DEVICE)
        {
            total_package_to_server = 1;
        }
        else
        {
            total_package_to_server = 1 + (u8)((Dtu3Detail.Property.PortNum + MeterLinkNum - 1) / ONEC_SOCKET_SEND_DEVICE);
        }
    }
    else if(object == app)
    {
        if((Dtu3Detail.Property.PortNum) < ONEC_SOCKET_SEND_DEVICE)
        {
            total_package_to_APP = 1;
        }
        else
        {
            total_package_to_APP = 1 + (u8)((Dtu3Detail.Property.PortNum - 1) / ONEC_SOCKET_SEND_DEVICE);
        }
    }

#endif
}

/***********************************************
** Function name:
** Descriptions:        扫描NRF网络命令执行状态
** input parameters:    无
** output parameters:   无
** Returned value:      1代表执行中 2代表执行完成
*************************************************/
u8 scan_nrf_netcommand_status(void)
{
    vu8 i = 0;
    vu8 temp = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(PORT_NUMBER_CONFIRMATION(i))
        {
            if(((MIReal[i].Data.NetStatus < NET_EXECUTION_COMPLETED) && (MIReal[i].Data.NetStatus > NET_NOCMD)) || (NET_NOT_EXECUTED == MIReal[i].Data.NetStatus))
            {
                temp = 1;
                break;
            }
        }
        //没有执行中的网络命令
        else if(i == (Dtu3Detail.Property.PortNum - 1))
        {
            temp = 2;
            break;
        }
    }

    if(temp != 1)
    {
        temp = 2;
    }

    return temp;
}

/***********************************************
** Function name:
** Descriptions:        清除NRF网络命令执行状态
** input parameters:    无
** output parameters:   无
** Returned value:
*************************************************/
void clean_nrf_netcommand_status(void)
{
    vu8 i = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if(PORT_NUMBER_CONFIRMATION(i))
        {
            if((MIReal[i].Data.NetStatus == NET_EXECUTION_COMPLETED) || (MIReal[i].Data.NetStatus == NET_EXECUTION_FAILURE))
            {
                MIReal[i].Data.NetStatus = NET_NOCMD;
                MIReal[i].Data.NetCmd = NET_INIT;
            }
        }
    }
}

/***********************************************
** Function name:
** Descriptions:      升级失败清除微逆升级状态
** input parameters:
** output parameters:
** Returned value:
*************************************************/
void clear_mi_update_status(void)
{
    vu8 j = 0;

    if(MI_UPDATE == downfile_module)
    {
        //置微逆升级状态失败
        for(j = 0; j < Dtu3Detail.Property.PortNum; j++)
        {
            if(NET_WAIT_DOWNLOAD_PRO == MIReal[j].Data.NetCmd)
            {
                MIReal[j].Data.NetCmd = NET_INIT;
            }

            if(NET_NOT_EXECUTED == MIReal[j].Data.NetStatus)
            {
                MIReal[j].Data.NetStatus = NET_EXECUTION_FAILURE;
            }
        }

        downfile_module = 0;
    }
}
/***********************************************
** Function name:
** Descriptions:        扫描微逆数据是否全零
** input parameters:    无
** output parameters:   无
** Returned value:      1代表有数据 0代表全无数据
*************************************************/
u16 scan_nrf_zero_data(void)
{
    vu16 i = 0;
    vu16 temp = 0;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if((MIReal[i].Data.PVVol[0] != 0) || (MIReal[i].Data.PVVol[1] != 0))
        {
            temp = 1;
            break;
        }

        if(i == (Dtu3Detail.Property.PortNum - 1))
        {
            temp = 0;
            break;
        }
    }

    return temp;
}
/***********************************************
** Function name:
** Descriptions:        校验函数
** input parameters:
** output parameters:   文件校验是否成功
** Returned value:  成功返回1 失败返回0
*************************************************/
u8 checkout(u8 mode)
{
    vu32 i = 0;
    vu8 temp = 0;
#ifndef DTU3PRO
    vu8 cheak_ok[] = "ok";
#endif
#ifndef DTU3PRO

    //本地升级
    if(1 == usart1_update)
    {
#ifdef DEBUG
        printf("Start calibration\r\n");
#endif

        //校验成功
        if(1 == DTU_Program_Check(SERIAL_NUM))
        {
            usart1_update = 0;
            FLASH_ERASURE(FLASH_ADDR_APP_HEX_SET, 1);
            FLASH_WriteMoreData(FLASH_ADDR_APP_HEX_SET, (u16 *)cheak_ok, 1);
#ifdef DEBUG
            printf("check_completed_start_upgrade\r\n");
#endif
            send_update_result((u8 *)computer_id, (u8 *)computer_id, 1);

            for(int i = 0; i < 10000; i++);

            //重启升级
            NVIC_SystemReset();
        }
        else
        {
#ifdef DEBUG
            printf("check_failure -- -- please_upgrade_it_again\r\n");
#endif
            send_update_result((u8 *)computer_id, (u8 *)computer_id, 0);
            return 0;
        }
    }

#endif

    if(DTU_UPDATE == downfile_module)
    {
        //校验成功
        if(1 == DTU_Program_Check(SERIAL_NUM))
        {
#ifdef DTU3PRO
            memset((u8 *)system_cfg.Data, 0, sizeof(SystemCfgNew));
            System_Cfg_Read((SystemCfgNew *)&system_cfg);
            memset((u8 *)system_cfg.Property.UpgradeSign, 0, sizeof(system_cfg.Property.UpgradeSign));
            sprintf((char *)system_cfg.Property.UpgradeSign, "Upgrade");
            System_Cfg_Write((SystemCfgNew *)&system_cfg);

            for(int i = 0; i < 10000; i++);

#endif
#ifdef DEBUG0703
            printf("DtuCheckOk\r\n");
#endif

            if(WIFI_MODE == mode)
            {
                Wifi_state = WIFI_POWER_OFF;
                Tcp_Http_Flg = 0;
#ifndef DTU3PRO
                FLASH_ERASURE(FLASH_ADDR_APP_HEX_SET, 1);
                FLASH_WriteMoreData(FLASH_ADDR_APP_HEX_SET, (u16 *)cheak_ok, 1);

                for(int i = 0; i < 10000; i++);

#endif
                //重启升级
                NVIC_SystemReset();
            }
            else if(GPRS_MODE == mode)
            {
#ifndef DTU3PRO
                FLASH_ERASURE(FLASH_ADDR_APP_HEX_SET, 1);
                FLASH_WriteMoreData(FLASH_ADDR_APP_HEX_SET, (u16 *)cheak_ok, 1);

                for(int i = 0; i < 10000; i++);

#endif
                //重启升级
                NVIC_SystemReset();
            }

#ifdef DTU3PRO
            else if(CABLE_MODE == mode)
            {
                //重启升级
                NVIC_SystemReset();
            }

#endif
            //下载文件完成
            downfile = DOWNFILE_COMPLETE;
            temp = 1;
        }
        //校验失败
        else
        {
#ifdef DEBUG070
            printf("DtuCheckFail\r\n");
#endif
            Download_DTUProgram_Delete();
            temp = 0;
        }
    }
    else if(MI_UPDATE == downfile_module)
    {
        //校验成功
        if(1 == MI_Program_Check())
        {
            //清空下载次数
            user_http_file_times = 0;

            //置微逆升级标志位
            for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
            {
                if(NET_WAIT_DOWNLOAD_PRO == MIReal[i].Data.NetCmd)
                {
                    MIReal[i].Data.NetCmd = NET_DOWNLOAD_PRO;
                    MIReal[i].Data.NetStatus = NET_NOT_EXECUTED;
                }
            }

            //下载文件完成
            downfile = DOWNFILE_COMPLETE;
            downfile_module = 0;
#ifdef DEBUG070
            printf("MiCheckOk\r\n");
#endif

            if(WIFI_MODE == mode)
            {
                Wifi_state = WIFI_POWER_OFF;
                Tcp_Http_Flg = 0;
                Wifi_http_link_time = LocalTime;
            }
            else if(GPRS_MODE == mode)
            {
                GPRS_state = GS_CONTROL_POFF;
                gprs_data_link_time = LocalTime + 2000;
            }

#ifdef DTU3PRO
            else if(CABLE_MODE == mode)
            {
            }

#endif
            temp = 1;
        }
        //校验失败
        else
        {
#ifdef DEBUG070
            printf("MiCheckFail\r\n");
#endif
            Download_MIProgram_Delete();
            temp = 0;
        }
    }

    return temp;
}



/***********************************************
** Function name:
** Descriptions:       微逆轮训状态指示
** input parameters:        ture 表示 实时轮训   false 表示打包时候轮训
** output parameters:
** Returned value:
*************************************************/
void Led_MI_LinkState(bool RealTimeState)
{
    vu8 i;

    //无ID
    if(Dtu3Detail.Property.PortNum == 0)
    {
        Dtu3Detail.Property.LedState = 0;
        return;
    }

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if((MIReal[i].Data.LinkState & MI_CONNECT) == 0)
        {
            if(RealTimeState == false)
            {
                Dtu3Detail.Property.LedState = 1;//搜不全
            }

            return;
        }
    }

    if(i >= Dtu3Detail.Property.PortNum)
    {
        Dtu3Detail.Property.LedState = 2;    //搜全
    }
}


/***********************************************
** Function name:
** Descriptions:       微逆在升级程序和升级并网保护文件
** input parameters:
** output parameters: 0为无繁忙 1为有繁忙--不打包数据
** Returned value:
*************************************************/
u8 MI_UpdateBusy(void)
{
    vu8 i;

    for(i = 0; i < Dtu3Detail.Property.PortNum; i++)
    {
        if((MIReal[i].Data.NetCmd == NET_DOWNLOAD_DAT) || (MIReal[i].Data.NetCmd == NET_DOWNLOAD_PRO))
        {
            if(((MIReal[i].Data.NetStatus > NET_NOCMD) && (MIReal[i].Data.NetStatus < NET_EXECUTION_COMPLETED)) || (MIReal[i].Data.NetStatus == NET_NOT_EXECUTED))
            {
                return 1;
            }
        }
    }

    return 0;
}
