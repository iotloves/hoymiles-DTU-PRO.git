#ifndef __TIMEPROCESSING_H
#define __TIMEPROCESSING_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#define server 1
#define app    2
#else
#include "stm32f10x.h"

#endif
#include <stdbool.h>
#define SOCKET_LEN  1024
//一帧需要的包数 100/10
#define PACKAGE_ALL 11
//服务器串口超时时间
#define server_usart_out_timter    30000
enum
{
    GET_CONFIG_PACK             = 0x0001,                   //获取配置文件
    SET_CONFIG_PACK             = 0x0002,                   //设置配置文件
    NET_COMMAND_PACK            = 0x0004,                   //网络命令
    NET_STATUS_PACK             = 0x0008,                   //网络命令执行状态
    REGISTRATION_PACK           = 0x0010,                   //注册帧
    INFO_DATA_PACK              = 0x0020,                   //信息帧
    DTU_ALERTING_PACK           = 0x0040,                   //采集器告警帧
    MI_SUSPEND_ALERTING_PACK    = 0x0080,                   //微逆挂起告警帧
    REAL_DATA_PACK              = 0x0100,                   //数据帧
    MI_ALERTING_PACK            = 0x0200,                   //微逆告警帧
    HISTORY_DATA_PACK           = 0x0400,                   //历史数据
    MI_HISTORY_ALERTING_PACK    = 0x0800,                   //微逆历史告警
    HEART_BEAT_PACK             = 0x1000,                   //心跳帧

    GET_CONFIG_OK               = 0xFFFE,                   //获取配置文件
    SET_CONFIG_OK               = 0xFFFD,                   //设置配置文件
    NET_COMMAND_OK              = 0xFFFB,                   //网络命令
    NET_STATUS_OK               = 0xFFF7,                   //网络命令执行状态
    REGISTRATION_OK             = 0xFFEF,                   //注册帧
    INFO_DATA_OK                = 0xFFDF,                   //信息帧
    DTU_ALERTING_OK             = 0xFFBF,                   //采集器告警帧
    MI_SUSPEND_ALERTING_OK      = 0xFF7F,                   //微逆挂起告警帧
    REAL_DATA_OK                = 0xFEFF,                   //数据帧
    MI_ALERTING_OK              = 0xFDFF,                   //微逆告警帧
    HISTORY_DATA_OK             = 0xFBFF,                   //历史数据
    MI_HISTORY_ALERTING_OK      = 0xF7FF,                   //微逆历史告警
    HEART_BEAT_OK               = 0xEFFF                    //心跳帧
};
typedef enum
{
    DOWNFILE_START = 1,                             //开始下载文件
    DOWNFILE_COMPLETE,                              //下载文件完成
    DTU_WAIT_UPDATE,                                //DTU升级等待
    DTU_UPDATE,                                     //DTU升级
    MI_WAIT_UPDATE,                                 //MI升级等待
    MI_UPDATE,                                      //MI升级
} update_file;

void ClearMIRealData(void);
void Judge_mi_link_status(void);
void package_get_config(void);
void package_set_config(void);
void package_my_data(void);
void package_respond_command(void);
void package_net_command_status(void);
void package_my_information(u8 page);
void package_my_real_data(void);
u8 history_data_flag(void);
void Store_historical_data(void);
u16 package_my_history_data(u8 package_now, u8 *package_nub, u8 *dat);
void package_heartbeat(void);
void package_dtu_alerting(void);
u8 package_mi_alerting(u8 PackageNub);
void Calculation_package(u8 object);
//APP
void package_app_get_information(void);
void package_app_set_information(void);
void package_app_information(u8 page);
void package_app_real_data(void);
void package_app_get_event(void);
void package_app_set_id(void);
void package_app_config_command(void);
void package_app_status_data(void);
void package_app_heartbeat(void);
void Timed_Query(void);
void clear_server_send_command(void);
u8 scan_send_status(void);
void update_handle(void);
u8 scan_nrf_netcommand_status(void);
void clean_nrf_netcommand_status(void);
void clear_real_data(u16 i);
void storage_real_date(u8 PackNum, u8 PackAll, char *Data, u32 Len);
u8 checkout(u8 mode);
void clear_mi_update_status(void);
u16 scan_nrf_zero_data(void);
//void Read_ServerDomainName(void);
void package_app_DevConfigPut_data(void);
void package_app_DevConfigFetch_data(void);
u8 package_mi_suspend_alerting(void);
//void package_my_history_alerting(void);
void clear_server_send_timer(void);
void package_app_get_warn(void);
void Led_MI_LinkState(bool RealTimeState);
u8 MI_UpdateBusy(void);

#endif
