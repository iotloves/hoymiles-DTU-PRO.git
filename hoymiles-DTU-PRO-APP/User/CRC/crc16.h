#ifndef __CRC16_H
#define __CRC16_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
u16 MyCrc16(u8 *updata, u16 len);
u16 CalcCRC16(u16 *TargetAddr, u32 nub);
u8 line_crc(u8 *dat, u8 len);
u16 CalcCRC16t(u16 Data, u16 CRCNow);
u32 uint8_to_uint32(u8 a, u8 b, u8 c, u8 d);
u8 package_crc(u8 *data, u16 len);
u8 CheckProgramB(uint32_t offset);
u16 UART_CRC16_Work(u8 *CRC_Buf, u16 CRC_Leni);
u16 get_meter_crc(u8 *dat, u8 nub);
u16 ModRTU_CRC(u8 *buf, int len);
u8 package_crc(u8 *data, u16 len);
u16 getCRC16(u8 array[], u16 length);
#endif
