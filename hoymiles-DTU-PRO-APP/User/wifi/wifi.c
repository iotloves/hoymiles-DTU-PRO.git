#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "wifi.h"
#include "SysTick.h"
#include "usart_wifi.h"
#include "gprs.h"
#include "usart_nrf.h"
#include "Memory.h"
#include "Memory_Tools.h"
#include "NetProtocol.h"
#include "TimeProcessing.h"
#include "HW_Version.h"
#include "ServerProtocol.h"
vu8 Wifi_state = 0;
//wifi参数设置标志位 默认0 1表示修改了参数 设置完成以后需要重启WIFI模块
vu8 WifiParaSetFlg = 0;

extern vu8 NetProVer;
//tcp和http标志位
vu8 Tcp_Http_Flg = 0;
//升级标志位
extern vu8 upgrade;
//校验文件失败
extern vu8 Check_file_failed;
extern vu8 Registration_frame;
//app 标志位
extern vu8 APP_Flg;
//下载文件标志位
extern vu8 downfile;
//微逆升级还是DTU升级标志位
extern vu8 downfile_module;

extern vu32 socket_state;
#ifndef DTU3PRO
//LED状态
extern vu8 led_state;
#else
#endif
//连接的外网
extern vu8 connect_net;
//连上服务器标志位
extern vu8 connect_server;
//有服务器数据
extern vu8 server_data;
extern vu8 user_http_file_times;

//WIFI WAP
vu8 WAP[9];
#ifndef DTU3PRO
//ssid 前缀
char pre_ssid[] = "DTUL-";
#else
//ssid 前缀
char pre_ssid[] = "DTUP-";
#endif
//发送数据长度
extern vu16 socket_data_len;
//APP当前请求帧数
extern vu8 AppPackageNow;
//S-C的当前包数
extern vu8 package_to_APP;
//S-C的总包数
extern vu8 total_package_to_APP;
//C-S的当前包数
extern vu8 package_to_server;
//C-S的总包数
extern vu8 total_package_to_server;
//串口超时计数器
vu32 wifi_usart_out_timer = 0;
//打包暂存buff
extern vu8 pack_buff[PACKAGE_ALL][SOCKET_LEN];
//每包的长度
extern vu16 pack_len[PACKAGE_ALL];
//要发送服务器的数据
extern vu8 server_socket_send_buff[SOCKET_LEN];
//接受的字节数
extern vu16 wifi_usart_rxd_len;
//中断发送计数器(count)
extern vu16 wifi_usart_tx_cnt;
//中断发送buff的字节数
extern vu16 wifi_usart_tx_rp;
//发送的字节数
extern vu16 wifi_usart_tx_wp;
//开始标志位
extern vu8 wifi_usart_start_flg;
//结束标志位
extern vu8 wifi_usart_finish_flg;
//DTU主要信息
extern volatile DtuMajor Dtu3Major;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
//发送buff暂存
extern vu8 wifi_usart_send_buff[SOCKET_LEN];
//接受buff
extern vu8 wifi_usart_receive_buff[SOCKET_LEN];
//wifi共享buff
extern vu8 wifi_share_buff[SOCKET_LEN];
//下载文件的地址是域名还是ip 0是ip 1是域名
extern vu8 HttpMode;
//升级标志位
vu8 upgrade;
//下载域名
vu8 up_pro_domain[ROW_MAX_LEN] = {0};
// 下载ip
vu8 up_pro_destip[4] = {0};
//端口号
vu16 up_pro_destport = 0;
//下载文件名
vu8 up_pro_filename[ROW_MAX_LEN] = {0};
//下载url
vu8 up_pro_url[ROW_MAX_LEN] = {0};
extern vu8 Wifi_state;
extern vu32 LocalTime;
//服务器发送数据计数器
extern vu8 SendData_to_Server_Num;
//下载完成标志位
extern vu8 DownloadEnd;
//AT命令计数器
vu8 Wifi_SendAtNum = 0;
////连接服务器计数器
//u8 wifi_ConnectNum = 0;
// 连接wifi计数器
vu8 wifi_ConnectWifiNum = 0;
vu32 Wifi_data_link_time = 0;
vu32 Wifi_http_link_time = 0;
//wifi串口超时时间 APP定义为100ms 服务器定义为200ms
vu16 wifi_out_time = 200;
/***********************************************
** Function name:
** Descriptions:        wifi模块tcp端主循环
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_Tcp(void)
{
    vu16 i = 0;
    vu8 j = 0;
    vu16 server_port  = 0;
    volatile char temp_string[3][40] = {0};
    vu8 k = 0;
    char *p = NULL;

    if(0 == Tcp_Http_Flg)
    {
        //串口有数据（接收）（server--200ms超时接受 app--100ms超时接受 没收到数据默认接受结束）
        if((1 == wifi_usart_start_flg) && (LocalTime > wifi_usart_out_timer + wifi_out_time))
        {
            wifi_usart_out_timer = LocalTime;

            //收到一包数据
            if((1 == wifi_usart_start_flg) && (0 == wifi_usart_finish_flg))
            {
                wifi_usart_finish_flg = 1;
            }

            wifi_usart_start_flg = 0;

            //接收的AT指令回复
            if((WIFI_POWER_ON < Wifi_state) && (Wifi_state < WIFI_WAIT))
            {
                while(i < wifi_usart_rxd_len)
                {
                    if(0 == strncmp((char *)&wifi_usart_receive_buff[i], "a", 1))
                    {
                        Wifi_SendAtNum = 0;
                        Wifi_state = WIFI_STA_A;
                        break;
                    }
                    else if(0 == strncmp((char *)&wifi_usart_receive_buff[i], "OK", 2))
                    {
                        if((WIFI_STA < Wifi_state) && (Wifi_state < WIFI_CONNECT_REQ))
                        {
                            Wifi_SendAtNum = 0;
                        }

                        switch(Wifi_state)
                        {
                            case WIFI_STA_A:
                                Wifi_state = WIFI_E_REQ;
                                break;

                            case WIFI_E_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i], "OK=OFF", 6) == 0))
                                {
                                    Wifi_state = WIFI_VER;
                                }
                                else
                                {
                                    Wifi_state = WIFI_E_SET;
                                }

                                break;

                            case WIFI_E_SET:
                                Wifi_state = WIFI_VER;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_VER:
                                i = i + 3;
                                memset((u8 *)Dtu3Major.Property.Wifi_Vsn, 0, sizeof(Dtu3Major.Property.Wifi_Vsn));
                                memcpy((u8 *)Dtu3Major.Property.Wifi_Vsn, (u8 *)&wifi_usart_receive_buff[i], 10);
                                Wifi_state = WIFI_WMODE_REQ;
                                break;

                            case WIFI_WMODE_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i], "OK=APSTA", 8) == 0) && (Dtu3Major.Property.netmode_select  == WIFI_MODE))
                                {
                                    Wifi_state = WIFI_TRAN_REQ;
                                }
                                else if((strncmp((char *)&wifi_usart_receive_buff[i], "OK=AP\r\n", 7) == 0) && (Dtu3Major.Property.netmode_select != WIFI_MODE))
                                {
                                    Wifi_state = WIFI_TRAN_REQ;
                                }
                                else
                                {
                                    Wifi_state = WIFI_WMODE_SET;
                                }

                                break;

                            case WIFI_WMODE_SET:
                                Wifi_state = WIFI_TRAN_REQ;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_TRAN_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i], "OK=TRANS", 8) == 0))
                                {
                                    Wifi_state = WIFI_WAP_REQ;
                                }
                                else
                                {
                                    Wifi_state = WIFI_TRAN_SET;
                                }

                                break;

                            case WIFI_TRAN_SET:
                                Wifi_state = WIFI_WAP_REQ;
                                Wifi_SendAtNum = 0;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_WAP_REQ:
                                memset((u8 *)temp_string[0], 0, sizeof(temp_string[0]));
                                strncpy((char *)temp_string[0], pre_ssid, 5);
                                hex_ascii((u8 *)Dtu3Major.Property.Id, (u8 *)&temp_string[0][5], 4);

                                //比对id
                                if((strncmp((char *)&wifi_usart_receive_buff[i + 3], (char *)temp_string[0], 13) == 0))
                                {
                                    Wifi_state = WIFI_WSTA_REQ;
                                }
                                else
                                {
                                    Wifi_state = WIFI_WAP_SET;
                                }

                                break;

                            case WIFI_WAP_SET:
                                Wifi_state = WIFI_WSTA_REQ;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_WSTA_REQ:
                                j = 0;
                                i += 3;
                                memset((char *)temp_string, 0, sizeof(temp_string));

                                for(; i < wifi_usart_rxd_len; i++)
                                {
                                    if(',' == wifi_usart_receive_buff[i])
                                    {
                                        j = 0;
                                        i += 1;

                                        for(; i < wifi_usart_rxd_len; i++)
                                        {
                                            if((0x0A == wifi_usart_receive_buff[i]) || (0x0D == wifi_usart_receive_buff[i]))
                                            {
                                                i = wifi_usart_rxd_len;
                                                break;
                                            }

                                            temp_string[1][j++] = wifi_usart_receive_buff[i];
                                        }
                                    }
                                    else
                                    {
                                        temp_string[0][j++] = wifi_usart_receive_buff[i] ;
                                    }
                                }

                                if((0 == strncmp((char *)Dtu3Major.Property.wifi_ssid, (char *)temp_string[0], 40)) && (0 == strncmp((char *)Dtu3Major.Property.wifi_passward, (char *)temp_string[1], 30)))
                                {
                                    // SSID和password 没改变 跳过设置
                                    Wifi_state = WIFI_APP_DNS_REQ;
                                }
                                else
                                {
                                    //APP设置
                                    if(1 == Dtu3Major.Property.wifi_ssid_save_set)
                                    {
                                        //SSID和password 发生改变 重新设置设置
                                        Wifi_state = WIFI_WSTA_SET;
                                        Dtu3Major.Property.wifi_ssid_save_set = 0;
                                        //System_DtuMajor_Write(&Dtu3Major);
                                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                                    }
                                    else
                                    {
                                        memset((u8 *)Dtu3Major.Property.wifi_ssid, 0, sizeof(Dtu3Major.Property.wifi_ssid));
                                        memset((u8 *)Dtu3Major.Property.wifi_passward, 0, sizeof(Dtu3Major.Property.wifi_passward));
                                        memcpy((u8 *)Dtu3Major.Property.wifi_ssid, (char *)temp_string[0], strlen((char *)temp_string[0]));
                                        memcpy((u8 *)Dtu3Major.Property.wifi_passward, (char *)temp_string[1], strlen((char *)temp_string[1]));
                                        //System_DtuMajor_Write(&Dtu3Major);
                                        System_Dtu_Info_Write((DtuDetail *)&Dtu3Detail, (DtuMajor *)&Dtu3Major);
                                    }
                                }

                                break;

                            case WIFI_WSTA_SET:
                                Wifi_state = WIFI_APP_DNS_REQ;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_APP_DNS_REQ:

                                //比对id
                                if((strncmp((char *)&wifi_usart_receive_buff[i + 3], "TCPS,10.10.100.254,10081", 24) == 0))
                                {
                                    Wifi_state = WIFI_SERVER_DNS_REQ;
                                }
                                else
                                {
                                    Wifi_state = WIFI_APP_DNS_SET;
                                }

                                break;

                            case WIFI_APP_DNS_SET:
                                Wifi_state = WIFI_SERVER_DNS_REQ;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_SERVER_DNS_REQ:
#ifndef DTU3PRO
                                if((1 == APP_Flg) || (Dtu3Major.Property.netmode_select  != WIFI_MODE))
#else
                                if((1 == APP_Flg) || (Netmode_Used != WIFI_MODE))
#endif
                                {
                                    if(strncmp((char *)&wifi_usart_receive_buff[i], "OK=NONE", 7) == 0)
                                    {
                                        // app和不是wifi模式断开连接
                                        Wifi_state = WIFI_CONNECT_REQ;
                                    }
                                    else
                                    {
                                        // 设置断开连接
                                        Wifi_state = WIFI_SERVER_DNS_OFF;
                                    }
                                }
                                else
                                {
                                    //                                    Wifi_SendAtNum = 0;
                                    j = 0;
                                    i += 3;
                                    p = strtok((char *)&wifi_usart_receive_buff[i], ",");
                                    strcpy((char *)temp_string[0], p);

                                    while(p != NULL)
                                    {
                                        k += 1;
                                        p =  strtok(NULL, ",");

                                        if(k < 3)
                                        {
                                            strcpy((char *)temp_string[k], p);
                                        }
                                    }

                                    server_port = (u16)atoi((char *)temp_string[2]);
                                    k = 0 ;
                                    k = (u8)strncmp((char *)temp_string[0], "TCP", 3);
                                    k += strncmp((char *)temp_string[1], (char *)Dtu3Major.Property.ServerDomainName, strlen((char *)Dtu3Major.Property.ServerDomainName));

                                    if((0 == k) && (server_port == Dtu3Major.Property.ServerPort))
                                    {
                                        // DNS未发生改变 跳过设置
                                        Wifi_state = WIFI_CONNECT_REQ;
                                    }
                                    else
                                    {
                                        //DNS发生改变 重新设置设置
                                        Wifi_state = WIFI_SERVER_DNS_SET;
                                    }
                                }

                                break;

                            case WIFI_SERVER_DNS_SET:
                                Wifi_state = WIFI_CONNECT_REQ;
                                WifiParaSetFlg = 1;
                                break;

                            case WIFI_SERVER_DNS_OFF:
                                Wifi_state = WIFI_CONNECT_REQ;
                                break;

                            case WIFI_CONNECT_REQ:
                                if(strncmp((char *)&wifi_usart_receive_buff[i], "OK=DISCONNECTED", 15) == 0)
                                {
#ifndef DTU3PRO

                                    if((1 == APP_Flg) || (Dtu3Major.Property.netmode_select  != WIFI_MODE))
#else
                                    if((1 == APP_Flg) || (Netmode_Used != WIFI_MODE))
#endif
                                    {
                                        // app和不是wifi模式跳出循环
                                        Wifi_state = WIFI_ENTM;
                                    }
                                }
                                else
                                {
                                    wifi_ConnectWifiNum = 0;
                                    Wifi_state = WIFI_WIFI_NET_REQ;
                                    Dtu3Major.Property.wifi_RSSI = 0;

                                    for(; i < wifi_usart_rxd_len; i++)
                                    {
                                        if(',' == wifi_usart_receive_buff[i])
                                        {
                                            for(; i < wifi_usart_rxd_len; i++)
                                            {
                                                //回车
                                                if(13 == wifi_usart_receive_buff[i + 1])
                                                {
                                                    break;
                                                }
                                                else
                                                {
                                                    //RSSI值
                                                    Dtu3Major.Property.wifi_RSSI = Dtu3Major.Property.wifi_RSSI * 10 + wifi_usart_receive_buff[i + 1] - '0';
                                                }
                                            }

                                            break;
                                        }
                                    }

#ifndef DTU3PRO

                                    if((1 == APP_Flg) || (Dtu3Major.Property.netmode_select  != WIFI_MODE))
#else
                                    if((1 == APP_Flg) || (Netmode_Used != WIFI_MODE))
#endif
                                    {
                                        //app和不是wifi模式跳出循环
                                        Wifi_state = WIFI_ENTM;
                                    }
                                }

                                break;

                            case WIFI_WIFI_NET_REQ:

                                //路由器是否能上网
                                if(strncmp((char *)&wifi_usart_receive_buff[i], "OK=Success", 10) == 0)
                                {
                                    wifi_ConnectWifiNum = 0;
                                    Wifi_state = WIFI_TCP_CONNECT_REQ;
                                    Wifi_data_link_time = LocalTime;
                                }

                                break;

                            case WIFI_TCP_CONNECT_REQ:
                                if(strncmp((char *)&wifi_usart_receive_buff[i], "OK=CONNECTED", 12) == 0)
                                {
                                    if(WIFI_MODE == Dtu3Major.Property.netmode_select)
                                    {
                                        //连接上服务器
                                        connect_server = 1;
                                    }

                                    Wifi_data_link_time = LocalTime;
                                    Wifi_SendAtNum = 0;
                                    Wifi_state = WIFI_ENTM;
                                }
                                //未连接服务器重新连接
                                else if(strncmp((char *)&wifi_usart_receive_buff[i], "OK=DISCONNECTED", 15) == 0)
                                {
                                    if(Wifi_SendAtNum > 3)
                                    {
                                        Wifi_data_link_time = LocalTime;
                                        Wifi_SendAtNum = 0;
                                        Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
                                        printf("WIFI_POWER_OFF:1\r\n");
#endif
                                        break;
                                    }
                                }

                                break;

                            case WIFI_ENTM:

                                //如果参数重新设置了 重启模块
                                if(WifiParaSetFlg == 1)
                                {
                                    WifiParaSetFlg = 0;
                                    Wifi_state = WIFI_POWER_OFF;
                                    break;
                                }

#ifndef DTU3PRO

                                if((1 == APP_Flg) || (Dtu3Major.Property.netmode_select  != WIFI_MODE))
#else
                                if((1 == APP_Flg) || (Netmode_Used != WIFI_MODE))
#endif
                                {
                                }
                                else
                                {
                                    socket_state = 0;
                                    //清空命令
                                    //clear_server_send_command();
                                    //每次切换AT命令的时候都先发注册帧
                                    socket_state |= REGISTRATION_PACK ;
                                    Registration_frame = 0;
                                }

                                Wifi_state = WIFI_WAIT;
                                Wifi_SendAtNum = 0;
                                break;
                        }

                        break;
                    }

                    i++;
                }
            }
            //数据是HM开头  服务器数据
            else if((wifi_usart_receive_buff[0] == 0x48) && (wifi_usart_receive_buff[1] == 0x4d))
            {
#ifndef DTU3PRO

                if((1 != APP_Flg) && (Dtu3Major.Property.netmode_select == WIFI_MODE))
#else
                if((1 != APP_Flg) && (Netmode_Used == WIFI_MODE))
#endif
                {
                    // 网络标志位
                    connect_net = 1;
                    // 连接服务器标志位
                    connect_server = 1;
                    //有服务器数据
                    server_data = 1;
                }

#ifdef DTU3PRO
                else if((1 != APP_Flg) && (Netmode_Used != WIFI_MODE))
                {
                    Wifi_state =  WIFI_WAIT;
                    return;
                }

#endif
                //处理接受的服务器数据
                pb_receive((char *)&wifi_usart_receive_buff);
            }
            //数据是A5开头  服务器数据
            else if(wifi_usart_receive_buff[0] == 0xA5)
            {
                //#ifdef DEBUG
                //                              printf("receive_2\r\n");
                //#endif
#ifndef DTU3PRO
                if(Dtu3Major.Property.netmode_select == WIFI_MODE)
#else
                if(Netmode_Used == WIFI_MODE)
#endif
                {
                    Process_Server_Data((char *)&wifi_usart_receive_buff);
                }
                else
                {
                    Wifi_state =  WIFI_WAIT;
                    return;
                }
            }

#ifdef DEBUG11
            printf("wifi-receive:%c-%c-%X-%X\n\r", wifi_usart_receive_buff[0], wifi_usart_receive_buff[1], wifi_usart_receive_buff[2], wifi_usart_receive_buff[3]);
#endif
            memset((u8 *)wifi_usart_receive_buff, 0, sizeof(wifi_usart_receive_buff));
            wifi_usart_rxd_len = 0;
        }
        //发送命令
        else if(Wifi_state != WIFI_WAIT)
        {
            //(数据重发机制是30s)
            if(LocalTime > (Wifi_data_link_time + 1000))
            {
                if(SendData_to_Server_Num > 20  || Wifi_SendAtNum > 10  || wifi_ConnectWifiNum > 120)
                {
                    SendData_to_Server_Num = 0;
                    Wifi_SendAtNum = 0;
                    wifi_ConnectWifiNum = 0;
                    Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
                    printf("WIFI_POWER_OFF:2\r\n");
#endif
                }

                wifi_usart_tx_cnt = 0;
                wifi_usart_finish_flg = 0;
                Wifi_data_link_time = LocalTime;

                switch(Wifi_state)
                {
                    // 0 断电
                    case WIFI_POWER_OFF:
#ifdef DEBUG
                        printf("WIFI_POWER_OFF\r\n");
#endif
                        Wifi_data_link_time = LocalTime + 1000 ;
                        WIFI_Disable();
                        Wifi_state = WIFI_POWER_ON;
#ifndef DTU3PRO

                        if(Dtu3Major.Property.netmode_select  == WIFI_MODE)
#else
                        if(Netmode_Used == WIFI_MODE)
#endif
                        {
#ifndef DTU3PRO
                            led_state = 2;
#endif
                            // 网络标志位
                            connect_net = 0;
                            // 连接服务器标志位
                            connect_server = 0;
                            // 有服务器数据
                            server_data = 0;
                        }

                        break;

                    //1  上电
                    case WIFI_POWER_ON:
                        Wifi_data_link_time = LocalTime + 4000 ;
                        WIFI_Enable();
                        Wifi_state = WIFI_STA;
                        break;

                    //2 发送+++
                    case WIFI_STA:
                        Dtu3Major.Property.wifi_RSSI = 0;
                        Wifi_Usart_SendData((u8 *)"+++", 3);
                        Wifi_SendAtNum++;
                        break;

                    //3 发送a
                    case WIFI_STA_A:
                        Wifi_Usart_SendData((u8 *)"a", 1);
                        Wifi_SendAtNum++;
                        break;

                    //4 关闭回显
                    case WIFI_E_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_E_REQ, (u16)strlen((char *)Wifi_E_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //5 关闭回显
                    case WIFI_E_SET:
                        Wifi_Usart_SendData((u8 *)Wifi_E_SET, (u16)strlen((char *)Wifi_E_SET));
                        Wifi_SendAtNum++;
                        break;

                    //6 查询版本号
                    case WIFI_VER:
                        Wifi_Usart_SendData((u8 *)Wifi_VER, (u16)strlen((char *)Wifi_VER));
                        Wifi_SendAtNum++;
                        break;

                    //7 查询WIFI工作模式
                    case WIFI_WMODE_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_WMODE_REQ, (u16)strlen((char *)Wifi_WMODE_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //8 设置WIFI工作模式
                    case WIFI_WMODE_SET:
                        Wifi_data_link_time = LocalTime + 2000;
#ifndef DTU3PRO

                        if(Dtu3Major.Property.netmode_select  == WIFI_MODE)
#else
                        if(Netmode_Used == WIFI_MODE)
#endif
                        {
                            Wifi_Usart_SendData((u8 *)Wifi_WMODE_APSTA_SET, (u16)strlen((char *)Wifi_WMODE_APSTA_SET));
                        }
                        else
                        {
                            Wifi_Usart_SendData((u8 *)Wifi_WMODE_AP_SET, (u16)strlen((char *)Wifi_WMODE_AP_SET));
                        }

                        Wifi_SendAtNum++;
                        break;

                    //9 查询工作是否为透传模式
                    case WIFI_TRAN_REQ :
                        Wifi_Usart_SendData((u8 *)Wifi_TRANS_REQ, (u16)strlen((char *)Wifi_TRANS_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //A 设置工作为透传模式
                    case WIFI_TRAN_SET:
                        Wifi_data_link_time = LocalTime + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_TRANS_SET, (u16)strlen((char *)Wifi_TRANS_SET));
                        Wifi_SendAtNum++;
                        break;

                    //B 查询wifi SSID
                    case WIFI_WAP_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_WAP_REQ, (u16)strlen((char *)Wifi_WAP_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //C 设置 wifi SSID
                    case WIFI_WAP_SET:
                        Wifi_data_link_time = LocalTime + 1000;
                        memset((u8 *)WAP, 0, sizeof(WAP));
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));
                        hex_ascii((u8 *)Dtu3Major.Property.Id, (u8 *)WAP, 4);
                        sprintf((char *)wifi_share_buff, (char *)Wifi_WAP_SET, pre_ssid, (char *)WAP);
                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    //D 查询sta的SSID和密码
                    case WIFI_WSTA_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_WSTA_REQ, (u16)strlen((char *)Wifi_WSTA_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //E 设置sta的SSID和密码
                    case WIFI_WSTA_SET:
                        Wifi_data_link_time = LocalTime + 1000;
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));
                        sprintf((char *)wifi_share_buff, (char *)Wifi_WSTA_SET, (char *)Dtu3Major.Property.wifi_ssid, (char *)Dtu3Major.Property.wifi_passward);
                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    //F 查询 APP DNS
                    case WIFI_APP_DNS_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_APP_DNS_REQ, (u16)strlen((char *)Wifi_APP_DNS_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //10 设置APP DNS
                    case WIFI_APP_DNS_SET:
                        Wifi_data_link_time = LocalTime + 1000;
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));
                        sprintf((char *)wifi_share_buff, (char *)Wifi_APP_DNS_SET);
                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    //11 查询服务器的DNS和端口号
                    case WIFI_SERVER_DNS_REQ:
                        Wifi_Usart_SendData((u8 *)Wifi_SERVER_DNS_REQ, (u16)strlen((char *)Wifi_SERVER_DNS_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //12 设置服务器的DNS和端口号
                    case WIFI_SERVER_DNS_SET:
                        Wifi_data_link_time = LocalTime + 1000;
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));
                        sprintf((char *)wifi_share_buff, (char *)Wifi_SERVER_DNS_SET, (char *)Dtu3Major.Property.ServerDomainName, Dtu3Major.Property.ServerPort);
                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    //13 断开服务器连接
                    case WIFI_SERVER_DNS_OFF:
                        Wifi_data_link_time = LocalTime + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_SERVER_DNS_OFF, (u16)strlen((char *)Wifi_SERVER_DNS_OFF));
                        Wifi_SendAtNum++;
                        break;

                    //14 查询是否连接上WIFI
                    case WIFI_CONNECT_REQ:
                        if(Dtu3Major.Property.netmode_select  == WIFI_MODE)
                        {
                            connect_net = 0;
                        }

                        Wifi_data_link_time = LocalTime + 2000;
                        Wifi_Usart_SendData((u8 *)Wifi_WIFI_REQ, (u16)strlen((char *)Wifi_WIFI_REQ));
                        wifi_ConnectWifiNum++;
                        break;

                    //15 查询WIFI是否能连接外网
                    case WIFI_WIFI_NET_REQ:
                        Wifi_data_link_time = LocalTime + 19000;
                        Wifi_Usart_SendData((u8 *)Wifi_WIFI_NET, (u16)strlen((char *)Wifi_WIFI_NET));
                        wifi_ConnectWifiNum++;
                        break;

                    //16 查询是否连接上服务器
                    case WIFI_TCP_CONNECT_REQ:
#ifndef DTU3PRO
                        if(Dtu3Major.Property.netmode_select  == WIFI_MODE)
#else
                        if(Netmode_Used == WIFI_MODE)
#endif
                        {
                            //连接网络
                            connect_net = 1;
                            //未连接服务器
                            connect_server = 0;
                        }

                        Wifi_data_link_time = LocalTime + 19000;
                        Wifi_Usart_SendData((u8 *)Wifi_SOCKLKB_REQ, (u16)strlen((char *)Wifi_SOCKLKB_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //17 切换到透传模式
                    case WIFI_ENTM:
#ifndef DTU3PRO
                        if(Dtu3Major.Property.netmode_select  == WIFI_MODE)
#else
                        if(Netmode_Used == WIFI_MODE)
#endif
                        {
                            //连接网络
                            connect_net = 1;
                            //未连接服务器
                            connect_server = 1;
                        }

                        Wifi_data_link_time = Wifi_data_link_time + 2000;
                        Wifi_Usart_SendData((u8 *)Wifi_ENTM, (u16)strlen((char *)Wifi_ENTM));
                        Wifi_SendAtNum++;
                        break;

                    //server

                    // 获取配置文件
                    case WIFI_GET_CONFIG:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_state = WIFI_WAIT;
                        SendData_to_Server_Num++ ;
                        break;

                    // 设置配置文件
                    case WIFI_SET_CONFIG:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_state = WIFI_WAIT;
                        SendData_to_Server_Num++ ;
                        break;

                    // 应答网络命令
                    case WIFI_RE_SERVER:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
#ifdef DEBUG0617
                        printf("WIFI_RE_SERVER\n\r");

                        for(i = 0; i < socket_data_len; i++)
                        {
                            printf("%x", server_socket_send_buff[i]);
                        }

                        printf("\n\r");
#endif
                        Wifi_state = WIFI_WAIT;
                        SendData_to_Server_Num++ ;
                        break;

                    // 回复网络命令执行状态
                    case WIFI_STATUS:
                        if(NetProVer == 0)
                        {
                            package_net_command_status();
                        }

                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
#ifdef solar
                        printf("WIFI_STATUS\n\r");

                        for(i = 0; i < socket_data_len; i++)
                        {
                            printf("%x", server_socket_send_buff[i]);
                        }

                        printf("\n\r");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 信息
                    case WIFI_SEND_INFO:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 采集器告警
                    case WIFI_DTU_ALERTING:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 微逆挂起告警
                    case WIFI_MI_SUSPEND_ALERTING:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 数据
                    case WIFI_SEND_DATA:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff));

                        if(NetProVer == 1)
                        {
                            socket_data_len = pack_len[package_to_server];
                            memcpy((u8 *)server_socket_send_buff, (char *)pack_buff[package_to_server], socket_data_len);
                        }
                        else
                        {
                            socket_data_len = pb_RealDataReq((char *)server_socket_send_buff, (char *)pack_buff[package_to_server], pack_len[package_to_server]);
                        }

                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 微逆告警
                    case WIFI_MI_ALERTING:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 历史数据
                    case WIFI_HISTORY_DATA:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    //历史告警
                    case WIFI_HISTORY_ALERTING:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    // 心跳
                    case WIFI_SEND_HEART:
                        Wifi_data_link_time = LocalTime + server_usart_out_timter;
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        SendData_to_Server_Num++ ;
                        break;

                    //  App获取配置信息请求回复
                    case App_GetConfigRes:
#ifdef DEBUG11
                        printf("App_GetConfigRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App设置配置信息请求回复
                    case App_SetConfigRes:
#ifdef DEBUG11
                        printf("App_SetConfigRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App设备信息请求回复
                    case App_InfoRes:
#ifdef DEBUG11
                        printf("App_InfoRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App实时数据请求回复
                    case App_RealDataRes:
#ifdef DEBUG11
                        printf("App_RealDataRes\r\n");
#endif
                        memset((u8 *)server_socket_send_buff, 0, sizeof(server_socket_send_buff)) ;
                        socket_data_len = 0;
                        memcpy((u8 *)server_socket_send_buff, (u8 *)pack_buff[AppPackageNow], sizeof(pack_buff[AppPackageNow]));
                        socket_data_len = pack_len[AppPackageNow];
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App告警请求回复
                    case App_WarnRes:
#ifdef DEBUG11
                        printf("App_WInfoRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App配置ID请求回复
                    case App_SetIDRes:
#ifdef DEBUG11
                        printf("App_SetIDRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    //32 App控制命令请求回复
                    case App_ConfigCommandRes:
#ifdef DEBUG11
                        printf("App_ConfigCommandRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App控制命令状态回复
                    case App_StatusDataRes:
#ifdef DEBUG11
                        printf("App_StatusDataRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App心跳状态回复
                    case App_HeartBeatRes:
#ifdef DEBUG11
                        printf("App_HeartBeatRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    //App配置获取请求
                    case App_DevConfigFetchRes:
#ifdef DEBUG11
                        printf("App_DevConfigFetchRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    // App配置设置请求
                    case App_DevConfigPutRes:
#ifdef DEBUG11
                        printf("App_DevConfigPutRes\r\n");
#endif
                        Wifi_Usart_SendData((u8 *)server_socket_send_buff, socket_data_len);
                        Wifi_data_link_time = LocalTime - 1000;
                        Wifi_state = WIFI_WAIT;
                        break;

                    //18  空闲模式
                    case WIFI_WAIT:
                        Wifi_data_link_time = LocalTime - 1000;
                        break;

                    default:
                        Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
                        printf("WIFI_POWER_OFF:3 -- %d\r\n", Wifi_state);
#endif
                        Wifi_data_link_time = LocalTime - 1000;
                        break;
                }
            }
        }
    }
}

/***********************************************
** Function name:
** Descriptions:        http下载模式
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_Http(void)
{
    vu16 i = 0;

    //HTTP模式标志位
    if(Tcp_Http_Flg == 1)
    {
        //串口有数据（接收）
        if((1 == wifi_usart_start_flg) && (LocalTime > wifi_usart_out_timer + wifi_out_time))
        {
            wifi_usart_out_timer = LocalTime;

            //收到一包数据
            if((1 == wifi_usart_start_flg) && (0 == wifi_usart_finish_flg))
            {
                wifi_usart_finish_flg = 1;
            }

            wifi_usart_start_flg = 0;

            //接收的AT指令回复
            if((WIFI_POWER_ON < Wifi_state) && (Wifi_state < WIFI_SEND_INFO))
            {
                while(i < wifi_usart_rxd_len)
                {
                    if(strncmp((char *)&wifi_usart_receive_buff[i], "a", 1) == 0)
                    {
                        Wifi_state = WIFI_STA_A;
                        Wifi_SendAtNum = 0;
                        break;
                    }
                    else if(strncmp((char *)&wifi_usart_receive_buff[i], "OK", 2) == 0)
                    {
                        Wifi_SendAtNum = 0;

                        switch(Wifi_state)
                        {
                            case WIFI_STA_A:
                                Wifi_state = WIFI_HTTP;
                                break;

                            case WIFI_HTTP:
                                Wifi_state = WIFI_HTPTP_REQ;
                                break;

                            case WIFI_HTPTP_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i + 3], "GET", 3) == 0))
                                {
                                    Wifi_state = WIFI_HTPHEAD_REQ;
                                }
                                else
                                {
                                    Wifi_state = WIFI_HTPTP_SET;
                                }

                                break;

                            case WIFI_HTPTP_SET:
                                Wifi_state = WIFI_HTPHEAD_REQ;
                                break;

                            case WIFI_HTPHEAD_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i + 3], (char *)HTTP_HEAD, 30) == 0))
                                {
                                    Wifi_state = WIFI_HTPSV_SET;
                                }
                                else
                                {
                                    Wifi_state = WIFI_HTPHEAD_SET;
                                }

                                break;

                            case WIFI_HTPHEAD_SET:
                                Wifi_state = WIFI_HTPSV_SET;
                                break;

                            case WIFI_HTPSV_SET:
                                Wifi_state = WIFI_HTPURL_SET;
                                break;

                            case WIFI_HTPURL_SET:
                                Wifi_state = WIFI_HTPCHD_REQ;
                                break;

                            case WIFI_HTPCHD_REQ:
                                if((strncmp((char *)&wifi_usart_receive_buff[i + 3], "ON", 2) == 0))
                                {
                                    Wifi_state = WIFI_Z;
                                }
                                else
                                {
                                    Wifi_state = WIFI_HTPCHD_SET;
                                }

                                break;

                            case WIFI_HTPCHD_SET:
                                Wifi_state = WIFI_Z;
                                break;

                            case WIFI_Z:
                                //下载
                                Wifi_state = WIFI_DOWN_FILE;
                                break;
                        }
                    }

                    i++;
                }

                memset((u8 *)wifi_usart_receive_buff, 0, sizeof(wifi_usart_receive_buff));
                wifi_usart_rxd_len = 0;
            }
        }
        //下载程序卡顿1.5分钟 开始校验
        //        else if(Check_file_failed  || ((Wifi_state == WIFI_DOWN_FILE) && (LocalTime > (90000 + wifi_usart_out_timer))))
        else if((Wifi_state == WIFI_DOWN_FILE) && (LocalTime > (90000 + wifi_usart_out_timer)))
        {
            wifi_usart_out_timer = LocalTime;
            DownloadEnd = 1;
        }

        //校验失败重新下载
#ifndef DTU3PRO

        if((Check_file_failed == 1) && (Dtu3Major.Property.netmode_select == WIFI_MODE))
#else
        if((Check_file_failed == 1) && (Netmode_Used == WIFI_MODE))
#endif
        {
            Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
            printf("WIFI_POWER_OFF:4\r\n");
#endif
            Check_file_failed = 0;
            //关闭DMA
            Wifi_DMA(0);
            Wifi_SendAtNum = 0;
            Wifi_http_link_time = LocalTime;
            wifi_usart_out_timer = LocalTime;
        }
        //发送命令
        else if((Wifi_state != WIFI_WAIT))
        {
            if(Wifi_SendAtNum > 8)
            {
                Wifi_SendAtNum = 0;
                Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
                printf("WIFI_POWER_OFF:5\r\n");
#endif
            }

            if(LocalTime > (Wifi_http_link_time + 1000))
            {
                Wifi_http_link_time = LocalTime;

                switch(Wifi_state)
                {
                    case WIFI_POWER_OFF:

                        //一次点击下载程序  如果失败 最多 下载3次
                        if(user_http_file_times > 2)
                        {
                            /*hzwang_20200422*/
                            downfile = 0;
                            /*下载失败清除下载标志位，允许历史数据存储*/
                            //升级标志位
                            upgrade = 0;
                            //切换TCP模式
                            Tcp_Http_Flg = 0;
                            Wifi_state = WIFI_POWER_OFF ;
#ifdef DEBUG
                            printf("WIFI_POWER_OFF:6\r\n");
#endif
                            user_http_file_times = 0;
                            //清除升级状态
                            clear_mi_update_status();
                            Wifi_DMA(0);
                            break;
                        }

                        Wifi_http_link_time = Wifi_http_link_time + 2000;
                        WIFI_Disable();
                        Wifi_state = WIFI_POWER_ON;
                        break;

                    case WIFI_POWER_ON:
                        Wifi_http_link_time = Wifi_http_link_time + 4000;
                        WIFI_Enable();
                        Wifi_state = WIFI_STA;
                        break;

                    case WIFI_STA:
                        Dtu3Major.Property.wifi_RSSI = 0;
                        Wifi_Usart_SendData((u8 *)"+++", 3);
                        Wifi_SendAtNum++;
                        break;

                    case WIFI_STA_A:
                        Wifi_Usart_SendData((u8 *)"a", 1);
                        Wifi_SendAtNum++;
                        break;

                    //设置工作为HTTP模式
                    case WIFI_HTTP:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPC, (u16)strlen((char *)Wifi_HTPC));
                        Wifi_SendAtNum++;
                        break;

                    //查询HTTP下载模式
                    case WIFI_HTPTP_REQ:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPTP_REQ, (u16)strlen((char *)Wifi_HTPTP_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //设置HTTP下载模式
                    case WIFI_HTPTP_SET:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPTP_SET, (u16)strlen((char *)Wifi_HTPTP_SET));
                        Wifi_SendAtNum++;
                        break;

                    //查询HTTP下载头
                    case WIFI_HTPHEAD_REQ:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPHEAD_REQ, (u16)strlen((char *)Wifi_HTPHEAD_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //设置HTTP下载头
                    case WIFI_HTPHEAD_SET:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPHEAD_SET, (u16)strlen((char *)Wifi_HTPHEAD_SET));
                        Wifi_SendAtNum++;
                        break;

                    // 设置IP+端口
                    case WIFI_HTPSV_SET:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));

                        //ip
                        if(0 == HttpMode)
                        {
                            sprintf((char *)wifi_share_buff, (char *)Wifi_HTPSV_IP_SET, up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3], up_pro_destport);
                        }
                        //DNS
                        else
                        {
                            sprintf((char *)wifi_share_buff, (char *)Wifi_HTPSV_DNS_SET, up_pro_domain, up_pro_destport);
                        }

                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    // 设置URL
                    case WIFI_HTPURL_SET:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        memset((u8 *)wifi_share_buff, 0, sizeof(wifi_share_buff));
                        sprintf((char *)wifi_share_buff, (char *)Wifi_HTPURL_SET, up_pro_url);
                        Wifi_Usart_SendData((u8 *)wifi_share_buff, (u16)strlen((char *)wifi_share_buff));
                        Wifi_SendAtNum++;
                        break;

                    //查询过滤HTTP头
                    case WIFI_HTPCHD_REQ:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPCHD_REQ, (u16)strlen((char *)Wifi_HTPCHD_REQ));
                        Wifi_SendAtNum++;
                        break;

                    //设置过滤HTTP头
                    case WIFI_HTPCHD_SET:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_HTPCHD_SET, (u16)strlen((char *)Wifi_HTPCHD_SET));
                        Wifi_SendAtNum++;
                        break;

                    //切换到透传模式
                    case WIFI_ENTM:
                        Wifi_http_link_time = Wifi_http_link_time + 1000;
                        Wifi_Usart_SendData((u8 *)Wifi_ENTM, (u16)strlen((char *)Wifi_ENTM));
                        Wifi_SendAtNum++;
                        break;

                    //重启模块
                    case WIFI_Z:
                        Wifi_http_link_time = Wifi_http_link_time + 10000;
                        Wifi_Usart_SendData((u8 *)Wifi_OFF, (u16)strlen((char *)Wifi_OFF));
                        Wifi_SendAtNum++;
                        break;

                    case WIFI_DOWN_FILE:

                        //升级DTU和微逆
                        if((DTU_UPDATE == downfile_module) || (MI_UPDATE == downfile_module))
                        {
                            if(DTU_UPDATE == downfile_module)
                            {
                                Download_DTUProgram_Delete();
                            }
                            else
                            {
                                Download_MIProgram_Delete();
                            }

                            Wifi_http_link_time = Wifi_http_link_time + 600000;//240000;   //4min
                        }

                        user_http_file_times++;
                        Wifi_Usart_SendData((u8 *)up_pro_filename, (u16)strlen((char *)up_pro_filename));
                        Wifi_DMA(1);
                        break;

                    case WIFI_WAIT:
                        break;

                    default:
                        Wifi_state = WIFI_POWER_OFF;
#ifdef DEBUG
                        printf("WIFI_POWER_OFF:7\r\n");
#endif
                        break;
                }
            }
        }
    }
}
