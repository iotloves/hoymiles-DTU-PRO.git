#ifndef __USART3_H
#define __USART3_H


#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "led.h"
#define Wifi_SendDataNumMax             20
#define Wifi_SendAtNumMax               8
#define wifi_ConnectNumMax              10
#define wifi_ConnectWifiNumMax          120

#define Wifi_RCCPeriphClockCmd          RCC_AHB1PeriphClockCmd
#define Wifi_USART_RX_GPIO_CLK          RCC_AHB1Periph_GPIOD
#define Wifi_USART_TX_GPIO_CLK          RCC_AHB1Periph_GPIOD
#define WIFI_USARTx                     USART2
#define WIFI_USART_CLK                  RCC_APB1Periph_USART2
#define WIFI_USART_APBxClkCmd           RCC_APB1PeriphClockCmd
#define WIFI_USART_BAUDRATE             38400

// USART GPIO 引脚宏定义
#define WIFI_USART_GPIO_CLK             RCC_AHB1Periph_GPIOD
#define WIFI_USART_GPIO_APBxClkCmd      RCC_AHB1PeriphClockCmd

#define WIFI_USART_TX_GPIO_PORT         GPIOD
#define WIFI_USART_TX_GPIO_PIN          GPIO_Pin_5
#define WIFI_USART_TX_GPIO_AF           GPIO_AF_USART2
#define WIFI_USART_TX_SOURCE            GPIO_PinSource5
#define WIFI_USART_RX_GPIO_PORT         GPIOD
#define WIFI_USART_RX_GPIO_PIN          GPIO_Pin_6
#define WIFI_USART_RX_GPIO_AF           GPIO_AF_USART2
#define WIFI_USART_RX_SOURCE            GPIO_PinSource6
#define WIFI_USART_IRQ                  USART2_IRQn
#define WIFI_USART_IRQHandler           USART2_IRQHandler

//wifi power
/* GPIO端口 */
#define Wifi_POWER_GPIO_PORT            GPIOE
/* GPIO端口时钟 */
#define Wifi_POWER_GPIO_CLK             RCC_AHB1Periph_GPIOE
/* 连接到SCL时钟线的GPIO */
#define Wifi_POWER_GPIO_PIN             GPIO_Pin_5

//wifi reset
#define Wifi_RESET_GPIO_PORT            GPIOE
#define Wifi_RESET_GPIO_CLK             RCC_AHB1Periph_GPIOE
#define Wifi_RESET_GPIO_PIN             GPIO_Pin_4

//wifi reloader
#define Wifi_RELOADERE_GPIO_PORT        GPIOE
#define Wifi_RELOADERE_GPIO_CLK         RCC_AHB1Periph_GPIOE
#define Wifi_RELOADERE_GPIO_PIN         GPIO_Pin_3


#define Wifi_DMA_Irqn                   DMA1_Stream5_IRQn
#define Wifi_DMA_IRQHandler             DMA1_Stream5_IRQHandler
#else
#include "stm32f10x.h"
#include "define.h"
#include "stm32f10x_dma.h"
#define  Wifi_RCCPeriphClockCmd         RCC_APB2PeriphClockCmd
#define  Wifi_GPIO_RCC                  RCC_APB2Periph_GPIOC
#define  Wifi_GPIO_AFIO                 RCC_APB2Periph_AFIO
#define  WIFI_USARTx                    USART3
#define  WIFI_USART_CLK                 RCC_APB1Periph_USART3
#define  WIFI_USART_APBxClkCmd          RCC_APB1PeriphClockCmd
#define  WIFI_USART_BAUDRATE            38400

// USART GPIO 引脚宏定义
#define  WIFI_USART_GPIO_CLK            RCC_APB2Periph_GPIOA
#define  WIFI_USART_GPIO_APBxClkCmd     RCC_APB2PeriphClockCmd

#define  WIFI_USART_TX_GPIO_PORT        GPIOB
#define  WIFI_USART_TX_GPIO_PIN         GPIO_Pin_10
#define  WIFI_USART_RX_GPIO_PORT        GPIOB
#define  WIFI_USART_RX_GPIO_PIN         GPIO_Pin_11

#define  WIFI_USART_IRQ                 USART3_IRQn
#define  WIFI_USART_IRQHandler          USART3_IRQHandler

//wifi power
/* GPIO端口 */
#define Wifi_POWER_GPIO_PORT            GPIOC
/* GPIO端口时钟 */
#define Wifi_POWER_GPIO_CLK             RCC_APB2Periph_GPIOC
/* 连接到SCL时钟线的GPIO */
#define Wifi_POWER_GPIO_PIN             GPIO_Pin_3

//wifi reset
#define Wifi_RESET_GPIO_PORT            GPIOA
#define Wifi_RESET_GPIO_CLK             RCC_APB2Periph_GPIOA
#define Wifi_RESET_GPIO_PIN             GPIO_Pin_1

//wifi reloader
#define Wifi_RELOADERE_GPIO_PORT        GPIOA
#define Wifi_RELOADERE_GPIO_CLK         RCC_APB2Periph_GPIOA
#define Wifi_RELOADERE_GPIO_PIN         GPIO_Pin_8

#define Wifi_DMA_Irqn                   DMA1_Channel3_IRQn
#define Wifi_DMA_IRQHandler             DMA1_Channel3_IRQHandler

#endif

#define DOWNLOAD_SIZE                       2048 + 32//2048

void WIFI_Enable(void);
void WIFI_Disable(void);
//USART GPIO 配置
void Wifi_usart_config(void);
//串口发送
void Wifi_Usart_SendData(u8 *dat, u16 len);
//串口清除缓存数据
void Wifi_Usart_RxClear(void);

u8 BuffCheck(char *buffer);
u8 Grid_Check(char *buffer);

void Wifi_DMA(u8 DMAFlag);
void DownloadWriteFlash(char *DownloadBuf, u32 *WriteOffset, u8 mode);
void Handle_Downfile(void);
#endif
