#ifndef _WIFI_H
#define _WIFI_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#include "define.h"
#include "transchar.h"
#endif
//Wifi


//void Wifi_GPIO_Config(void);
void Wifi_Tcp(void);
void Wifi_Http(void);

static const unsigned char HTTP_HEAD[]             = "Connection: Keep-Alive[0D][0A]"; //下载过滤头

//TCP
//static const unsigned char Wifi_E[]               = "AT+CLEAR\r\n";                  //清除固件flash参数
static const unsigned char Wifi_E_REQ[]             = "AT+E\r\n";                      //查询关闭回显
static const unsigned char Wifi_E_SET[]             = "AT+E=off\r\n";                  //设置关闭回显
static const unsigned char Wifi_VER[]               = "AT+VER\r\n";                    //固件版本号

static const unsigned char Wifi_WMODE_REQ[]         = "AT+WMODE\r\n";                  //查询wifi的工作模式
static const unsigned char Wifi_WMODE_APSTA_SET[]   = "AT+WMODE=APSTA\r\n";            //设置为APSTA模式
static const unsigned char Wifi_WMODE_AP_SET[]      = "AT+WMODE=AP\r\n";               //设置为AP模式
static const unsigned char Wifi_TRANS_REQ[]         = "AT+WKMOD\r\n";                  //查询WIFI工作模式
static const unsigned char Wifi_TRANS_SET[]         = "AT+WKMOD=TRANS\r\n";            //设置WIFI工作模式为透传模式
static const unsigned char Wifi_WAP_REQ[]           = "AT+WAP\r\n";                    //查询wifi的SSID
static const unsigned char Wifi_WAP_SET[]           = "AT+WAP=%s%s,NONE\r\n";          //设置wifi的SSID为DTU的id
static const unsigned char Wifi_WSTA_REQ[]          = "AT+WSTA\r\n";                   //查询关联AP的SSID,密码
static const unsigned char Wifi_WSTA_SET[]          = "AT+WSTA=%s,%s\r\n";             //设置关联AP的SSID,密码
static const unsigned char Wifi_APP_DNS_REQ[]       = "AT+SOCKA\r\n";                  //查询APP的DNS
static const unsigned char Wifi_APP_DNS_SET[]       = "AT+SOCKA=TCPS,10.10.100.254,10081\r\n"; //设置APP的DNS
static const unsigned char Wifi_SERVER_DNS_REQ[]    = "AT+SOCKB\r\n";                  //查询服务器的DNS
static const unsigned char Wifi_SERVER_DNS_SET[]    = "AT+SOCKB=TCP,%s,%d\r\n";         //设置服务器的DNS
static const unsigned char Wifi_SERVER_DNS_OFF[]    = "AT+SOCKB=NONE\r\n";              //关闭服务器连接
static const unsigned char Wifi_WIFI_REQ[]          = "AT+WSLK\r\n";                    //查询是否建立WIFI连接和WIFI的信号强度
static const unsigned char Wifi_WIFI_NET[]          = "AT+PING=datacn.hoymiles.com\r\n";//查询WIFI是否有网络
static const unsigned char Wifi_SOCKLKB_REQ[]       = "AT+SOCKLKB\r\n";                 //查询是否建立TCP连接
//static const unsigned char Wifi_SOCKDISB_REQ[]      = "AT+SOCKDISB\r\n";               //查询是否断开TCP连接
//static const unsigned char Wifi_SOCKDISB_SET[]      = "AT+SOCKDISB=OFF\r\n";           //设置断开TCP连接


//HTTP
static const unsigned char Wifi_HTPC[]              = "AT+WKMOD=HTPC\r\n";             //HTPC模式
static const unsigned char Wifi_HTPTP_REQ[]         = "AT+HTPTP\r\n";                  //查询HTPC下载方式
static const unsigned char Wifi_HTPTP_SET[]         = "AT+HTPTP=GET\r\n";              //设置HTPC下载方式


static const unsigned char Wifi_HTPHEAD_REQ[]       = "AT+HTPHEAD\r\n";                //查询下载头设置
static const unsigned char Wifi_HTPHEAD_SET[]       = "AT+HTPHEAD=Connection: Keep-Alive[0D][0A]\r\n";//设置下载头设置

static const unsigned char Wifi_HTPSV_IP_SET[]      = "AT+HTPSV=%d.%d.%d.%d,%d\r\n";   //设置IP和端口
static const unsigned char Wifi_HTPSV_DNS_SET[]     = "AT+HTPSV=%s,%d\r\n";            //设置域名和端口
static const unsigned char Wifi_HTPURL_SET[]        = "AT+HTPURL=%s\r\n";              //设置 Httpd url
static const unsigned char Wifi_HTPCHD_REQ[]        = "AT+HTPCHD\r\n";                 //查询过滤http头
static const unsigned char Wifi_HTPCHD_SET[]        = "AT+HTPCHD=ON\r\n";              //设置过滤http头
static const unsigned char Wifi_ENTM[]              = "AT+ENTM\r\n";                   //切换到透传
static const unsigned char Wifi_OFF[]               = "AT+Z\r\n";                      //重启模块

enum NRF_STEP
{
    LOOP_REQ_RFVERSISON,          //轮训DTU NRF信息和ID
    LOOP_SENDPRA_STAR,            //并网保护参数
    LOOP_REQ_DAT                  //轮询微逆NRF数据

};

enum Wifi_STEP
{
    WIFI_POWER_OFF = 0,           //模块关机                       //0
    WIFI_POWER_ON,                //模块开机
    WIFI_STA,                     // +++
    WIFI_STA_A,                   //a
    WIFI_E_REQ,                   //查询关闭回显
    WIFI_E_SET,                   //设置关闭回显                   //5
    WIFI_VER,                     //版本号
    WIFI_WMODE_REQ,               //查询 WIFI工作模式(APSTA)
    WIFI_WMODE_SET,               //设置 WIFI工作模式(APSTA)
    WIFI_TRAN_REQ,                //查询透传模式
    WIFI_TRAN_SET,                //设置透传模式                   //A
    WIFI_WAP_REQ,                 //查询WAP（WIFI的SSID）
    WIFI_WAP_SET,                 //设置WAP（WIFI的SSID）
    WIFI_WSTA_REQ,                //查询AP的SSID和密码
    WIFI_WSTA_SET,                //设置AP的SSID和密码             //
    WIFI_APP_DNS_REQ,             //查询APP DNS                    //
    WIFI_APP_DNS_SET,             //设置APP DNS                    //10
    WIFI_SERVER_DNS_REQ,          //查询服务器的DNS和端口号        //
    WIFI_SERVER_DNS_SET,          //设置服务器的DNS和端口号        //
    WIFI_SERVER_DNS_OFF,          //关闭服务器连接
    WIFI_CONNECT_REQ,             //查询wifi连接                   //
    WIFI_WIFI_NET_REQ,            //wifi是否有网络
    WIFI_TCP_CONNECT_REQ,         //查询连接TCP服务器              //
    //    WIFI_TCP_DISCONNECT_REQ,      //TCP B路断开连接查询
    //    WIFI_TCP_DISCONNECT_SET,      //TCP B路断开连接设置
    WIFI_ENTM,                    //从临时AT命令模式切换到透传模式  //0x17
    WIFI_WAIT,                    //空闲模式                        //
    //HTTP模式
    WIFI_HTTP,                    //HTTP模式                        //
    WIFI_HTPTP_REQ,               //查询下载方式
    WIFI_HTPTP_SET,               //HTTP下载方式                    //1B
    WIFI_HTPHEAD_REQ,             //查询下载头
    WIFI_HTPHEAD_SET,             //设置下载头                      //
    WIFI_HTPSV_SET,               //设置IP和端口                    //
    WIFI_HTPURL_SET,              //设置 Httpd url                  //
    WIFI_HTPCHD_REQ,              //查询过滤http头                  //20
    WIFI_HTPCHD_SET,              //过滤http头                      //
    WIFI_DOWN_FILE,               //下载文件                        //
    WIFI_Z,                       //重启模块                        //
    WIFI_GET_CONFIG,              //获取配置信息                    //
    WIFI_SET_CONFIG,              //设置配置信息                    //25
    WIFI_RE_SERVER,               //回复服务器                      //
    WIFI_STATUS,                  //网络命令执行状态                //
    WIFI_SEND_INFO,               //信息                            //
    WIFI_DTU_ALERTING,            //采集器告警信息
    WIFI_MI_SUSPEND_ALERTING,     //微逆挂起告警
    WIFI_SEND_DATA,               //数据                            //2B
    WIFI_MI_ALERTING,             //微逆告警信息
    WIFI_HISTORY_DATA,            //历史数据
    WIFI_HISTORY_ALERTING,        //历史告警
    WIFI_SEND_HEART,              //心跳                            //
    // APP
    App_GetConfigRes,             //App获取配置信息请求回复         //
    App_SetConfigRes,             //App设置配置信息请求回复         //31
    App_InfoRes,                  //App设备信息请求回复             //
    App_RealDataRes,              //App实时数据请求回复
    App_WarnRes,                  //App告警请求回复
    App_SetIDRes,                 //App配置ID请求回复
    App_ConfigCommandRes,         //App控制命令请求回复
    App_StatusDataRes,            //App控制命令状态回复
    App_HeartBeatRes,             //APP心跳                        //38
    App_DevConfigFetchRes,        //App配置获取请求
    App_DevConfigPutRes,          //App配置设置请求

};
#endif
