#include "usart_wifi.h"
#include "string.h"
#include "SysTick.h"
#include "led.h"
#include "Memory.h"
#include "malloc.h"
#include "TimeProcessing.h"
#include "wifi.h"
#include "usart_gprs.h"
#include "HW_Version.h"
#ifndef DTU3PRO
#include "define.h"
#include "stm32_wdg.h"
#else
#include "http_client.h"
#endif
#include "usart_nrf.h"
//DMA

extern volatile DtuMajor Dtu3Major;
//接收状态标记
extern vu32 Wifi_data_link_time;
volatile char DownBuf[2][DOWNLOAD_SIZE];
vu8 DmaUseNum = 0;
vu32 DownOffset = 0;
//DMA开始下载标志
vu8 Dmastartdown = 0;
//下载完成标志位
extern vu8 DownloadEnd;
//app 标志位
vu8 APP_Flg = 0;

// 接受的字节数
vu16 wifi_usart_rxd_len = 0;
// 中断发送计数器(count)
vu16 wifi_usart_tx_cnt = 0;
// 中断发送buff的字节数
vu16 wifi_usart_tx_rp = 0;
// 发送的字节数
vu16 wifi_usart_tx_wp = 0;
//开始标志位
vu8 wifi_usart_start_flg = 0;
//结束标志位
vu8 wifi_usart_finish_flg = 0;

//接收中转buff
vu8 wifi_usart_receive_buff[SOCKET_LEN];
//发送buff暂存
vu8 wifi_usart_send_buff[SOCKET_LEN];

//wifi共享buff
vu8 wifi_share_buff[SOCKET_LEN];

extern  vu32 disconnect_app_timer;
extern  vu32 wifi_usart_out_timer;
extern  vu32 LocalTime;
//下载文件超时计时器
extern  vu32 downfile_timer;

//下载文件标志位
extern vu8 downfile;
//微逆升级还是DTU升级标志位
extern vu8 downfile_module;

extern vu8 Wifi_state;
//tcp和http标志位
extern vu8 Tcp_Http_Flg;
//DTU详细信息
extern volatile DtuDetail Dtu3Detail;
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
//wifi串口超时时间
extern vu16 wifi_out_time;
extern vu8 memory_down_mode;
/***********************************************
** Function name:
** Descriptions:        初始化wifi模块
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
//void Wifi_GPIO_Config(void)
//{
//    /*定义一个GPIO_InitTypeDef类型的结构体*/
//    GPIO_InitTypeDef GPIO_InitStructure;
//#ifdef DTU3PRO
//    //开启Wifi相关的GPIO外设时钟
//    Wifi_RCCPeriphClockCmd(Wifi_GPIO_CLK, ENABLE);
//    //选择要控制的GPIO引脚
//    GPIO_InitStructure.GPIO_Pin = Wifi_GPIO_PIN;
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//    //设置引脚速率为50MHz
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//    //调用库函数，初始化GPIO
//    GPIO_Init(Wifi_GPIO_PORT, &GPIO_InitStructure);
//    //配置为开机不上电
//    GPIO_ResetBits(Wifi_GPIO_PORT, Wifi_GPIO_PIN);
//#else
//    //开启Wifi相关的GPIO外设时钟
//    Wifi_RCCPeriphClockCmd(Wifi_GPIO_CLK, ENABLE);
//    //选择要控制的GPIO引脚
//    GPIO_InitStructure.GPIO_Pin = Wifi_GPIO_PIN;
//    //设置引脚模式为通用推挽输出
//    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
//    //设置引脚速率为50MHz
//    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//    //调用库函数，初始化GPIO
//    GPIO_Init(Wifi_GPIO_PORT, &GPIO_InitStructure);
//    // 配置为开机不上电
//    GPIO_ResetBits(Wifi_GPIO_PORT, Wifi_GPIO_PIN);
//#endif
//}

/***********************************************
** Function name:
** Descriptions:        配置嵌套向量中断控制器NVIC
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
static void NVIC_Configuration_Wifi(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    /* 嵌套向量中断控制器组选择 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);//2
    /* 配置USART为中断源 */
    NVIC_InitStructure.NVIC_IRQChannel = WIFI_USART_IRQ;
    /* 抢断优先级*/
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    /* 子优先级 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    /* 使能中断 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    /* 初始化配置NVIC */
    NVIC_Init(&NVIC_InitStructure);
    //DMA发送中断设置
    NVIC_InitStructure.NVIC_IRQChannel = Wifi_DMA_Irqn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
/***********************************************
** Function name:
** Descriptions:        WIFI所有管家拉低
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void WIFI_Disable(void)
{
    /*定义一个GPIO_InitTypeDef类型的结构体*/
    GPIO_InitTypeDef GPIO_InitStructure;
    //使能PA,PC端口时钟
    RCC_APB2PeriphClockCmd(Wifi_POWER_GPIO_CLK | Wifi_RESET_GPIO_CLK | WIFI_USART_GPIO_CLK, ENABLE);
    //选择要控制的GPIO引脚
    GPIO_InitStructure.GPIO_Pin = Wifi_POWER_GPIO_PIN;
    //设置引脚模式为通用推挽输出
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
#endif
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(Wifi_POWER_GPIO_PORT, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = WIFI_USART_TX_GPIO_PIN | WIFI_USART_RX_GPIO_PIN;
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
#endif
    GPIO_Init(WIFI_USART_TX_GPIO_PORT, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = Wifi_RESET_GPIO_PIN | Wifi_RELOADERE_GPIO_PIN;
    //设置引脚模式为通用推挽输出
#ifdef DTU3PRO
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
#else
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
#endif
    //设置引脚速率为50MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //调用库函数，初始化GPIO
    GPIO_Init(Wifi_RESET_GPIO_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(Wifi_RESET_GPIO_PORT, Wifi_RESET_GPIO_PIN | Wifi_RELOADERE_GPIO_PIN);
    GPIO_ResetBits(Wifi_POWER_GPIO_PORT, Wifi_POWER_GPIO_PIN);
}
/***********************************************
** Function name:
** Descriptions:        WIFI所有管脚拉低
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void WIFI_Enable(void)
{
    GPIO_SetBits(Wifi_POWER_GPIO_PORT, Wifi_POWER_GPIO_PIN);
    GPIO_SetBits(Wifi_RESET_GPIO_PORT, Wifi_RESET_GPIO_PIN | Wifi_RELOADERE_GPIO_PIN);
    Wifi_usart_config();
}
/***********************************************
** Function name:
** Descriptions:        USART DMA 配置,工作参数配置
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_DMA(u8 DMAFlag)
{
    DMA_InitTypeDef DMA_InitStructure;
    memset((char *)DownBuf[0], 0, DOWNLOAD_SIZE);
    memset((char *)DownBuf[1], 0, DOWNLOAD_SIZE);
#ifdef DTU3PRO
    //启动DMA时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    DMA_DeInit(DMA1_Stream5);

    //等待DMA可配置
    while(DMA_GetCmdStatus(DMA1_Stream5) != DISABLE);

    /* 配置 DMA Stream */
    //通道选择
    DMA_InitStructure.DMA_Channel = DMA_Channel_4;
    //DMA外设地址
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&WIFI_USARTx->DR;
    //DMA 存储器0地址
    DMA_InitStructure.DMA_Memory0BaseAddr = (u32)DownBuf[DmaUseNum];
    //外设到存储器模式
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    //数据传输量
    DMA_InitStructure.DMA_BufferSize = (DOWNLOAD_SIZE - 32);
    //外设非增量模式
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    //存储器增量模式
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    //外设数据长度:8位
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    //存储器数据长度:8位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    // 使用普通模式
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    //中等优先级
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
    //存储器突发单次传输
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    //外设突发单次传输
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    //    //双缓冲区模式
    //    DMA_DoubleBufferModeConfig(DMA1_Stream5,(u32)DownBuf[DmaUseNum+1],DMA_Memory_0);
    //    DMA_DoubleBufferModeCmd(DMA1_Stream5,ENABLE);
    //初始化DMA Stream
    DMA_Init(DMA1_Stream5, &DMA_InitStructure);
    //配置DMA发送完成后产生中断
    DMA_ITConfig(DMA1_Stream5, DMA_IT_TC, ENABLE);
    DMA_Cmd(DMA1_Stream5, ENABLE);
#else
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    //DMA1通道3配置
    // 关DMA通道
    DMA_Cmd(DMA1_Channel3, DISABLE);
    // 恢复缺省值
    DMA_DeInit(DMA1_Channel3);
    //DMA外设地址
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)(&USART3->DR);
    //DMA 存储器0地址
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)DownBuf[DmaUseNum];
    //外设到存储器模式
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    //数据传输量
    DMA_InitStructure.DMA_BufferSize = (DOWNLOAD_SIZE - 32);
    //外设非增量模式
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    //存储器增量模式
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    //外设数据长度:8位
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    //存储器数据长度:8位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    // 使用普通模式
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    //中等优先级
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    //设置DMA的2个memory中的变量互相访问
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    //初始化DMA Stream
    DMA_Init(DMA1_Channel3, &DMA_InitStructure);
    //配置DMA发送完成后产生中断
    DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
    //使能通道3
    DMA_Cmd(DMA1_Channel3, ENABLE);
#endif

    //采用DMA方式接收
    if(DMAFlag == 1)
    {
        //开启串口的 DMA 接受功能
        USART_DMACmd(WIFI_USARTx, USART_DMAReq_Rx, ENABLE);
        // 关闭串口接收中断
        USART_ITConfig(WIFI_USARTx, USART_IT_RXNE, DISABLE);
        // 关闭串口发送中断
        // USART_ITConfig(WIFI_USARTx, USART_IT_TC, DISABLE);
        // 使能串口空闲中断
        //USART_ITConfig(WIFI_USARTx, USART_IT_IDLE, ENABLE);
        memory_down_mode = 1;
    }
    else
    {
        USART_DMACmd(WIFI_USARTx, USART_DMAReq_Rx, DISABLE);
        USART_ITConfig(WIFI_USARTx, USART_IT_RXNE, ENABLE);
        //USART_ITConfig(WIFI_USARTx, USART_IT_TC, ENABLE);
        //USART_ITConfig(WIFI_USARTx, USART_IT_IDLE, DISABLE);
        memory_down_mode = 0;
    }

    Dmastartdown = 0;
}

/***********************************************
** Function name:
** Descriptions:        USART GPIO 配置,工作参数配置
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_usart_config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
#ifdef DTU3PRO
    RCC_AHB1PeriphClockCmd(Wifi_USART_RX_GPIO_CLK | Wifi_USART_TX_GPIO_CLK, ENABLE);
    /* 使能 USART 时钟 */
    RCC_APB1PeriphClockCmd(WIFI_USART_CLK, ENABLE);
    //USART_DeInit(WIFI_USARTx);
    //USART_StructInit(&USART_InitStructure);
    //USART_ClockStructInit(&USART_ClockInitStruct);
    /* GPIO初始化 */
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    /* 配置Tx引脚为复用功能  */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = WIFI_USART_TX_GPIO_PIN;
    GPIO_Init(WIFI_USART_TX_GPIO_PORT, &GPIO_InitStructure);
    /* 配置Rx引脚为复用功能 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = WIFI_USART_RX_GPIO_PIN;
    GPIO_Init(WIFI_USART_RX_GPIO_PORT, &GPIO_InitStructure);
    //设置引脚复用
    GPIO_PinAFConfig(WIFI_USART_TX_GPIO_PORT, WIFI_USART_TX_SOURCE, WIFI_USART_TX_GPIO_AF);
    GPIO_PinAFConfig(WIFI_USART_RX_GPIO_PORT, WIFI_USART_RX_SOURCE, WIFI_USART_RX_GPIO_AF);
#else
    // 打开串口GPIO的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
    // 打开串口外设的时钟
    WIFI_USART_APBxClkCmd(WIFI_USART_CLK, ENABLE);
    // 将USART Tx的GPIO配置为推挽复用模式
    GPIO_InitStructure.GPIO_Pin = WIFI_USART_TX_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(WIFI_USART_TX_GPIO_PORT, &GPIO_InitStructure);
    // 将USART Rx的GPIO配置为浮空输入模式
    GPIO_InitStructure.GPIO_Pin = WIFI_USART_RX_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(WIFI_USART_RX_GPIO_PORT, &GPIO_InitStructure);
#endif
    // 配置串口的工作参数
    // 配置波特率
    USART_InitStructure.USART_BaudRate = WIFI_USART_BAUDRATE;
    // 配置 针数据字长
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    // 配置停止位
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    // 配置校验位
    USART_InitStructure.USART_Parity = USART_Parity_No;
    // 配置硬件流控制
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    // 配置工作模式，收发一起
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    // 完成串口的初始化配置
    USART_Init(WIFI_USARTx, &USART_InitStructure);
    // 串口中断优先级配置
    NVIC_Configuration_Wifi();
    // 使能串口接收中断
    USART_ITConfig(WIFI_USARTx, USART_IT_RXNE, ENABLE);
    // 使能串口发送中断
    USART_ITConfig(WIFI_USARTx, USART_IT_TC, ENABLE);
    // 使能串口
    USART_Cmd(WIFI_USARTx, ENABLE);
    USART_ClearFlag(WIFI_USARTx, USART_FLAG_TC);
}


void Handle_Downfile(void)
{
    if(strlen((char *)DownBuf[(DmaUseNum) % 2]))
    {
#ifdef DTU3PRO
        //清除全部中断标志
        DMA_ClearFlag(DMA1_Stream5, DMA_IT_TCIF5);
        DMA_Cmd(DMA1_Stream5, DISABLE);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((char *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值 传输地址
        DMA1_Stream5->M0AR = (u32)DownBuf[DmaUseNum];
        DMA1_Stream5->NDTR = (DOWNLOAD_SIZE - 32);
        //打开DMA
        DMA_Cmd(DMA1_Stream5, ENABLE);
#else
        DMA_Cmd(DMA1_Channel3, DISABLE);
        //清除全部中断标志
        DMA_ClearFlag(DMA1_IT_TC3);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((u8 *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值
        DMA1_Channel3 -> CNDTR = (DOWNLOAD_SIZE - 32);
        // 设置传输地址
        DMA1_Channel3 -> CMAR  = (u32)DownBuf[DmaUseNum];
        //打开DMA
        DMA_Cmd(DMA1_Channel3, ENABLE);
#endif
        wifi_usart_out_timer = LocalTime;
#ifdef DEBUG0703
        printf("SSS:%d", strlen((char *)DownBuf[(DmaUseNum + 1) % 2]));
#endif
#ifndef DTU3PRO
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Dtu3Major.Property.netmode_select);
#else
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Netmode_Used);
#endif
    }
}

/***********************************************
** Function name:
** Descriptions:        wifi串口DMA中断
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_DMA_IRQHandler(void)
{
    //char *Buffer;
#ifdef DTU3PRO
    if(DMA_GetITStatus(DMA1_Stream5, DMA_IT_TCIF5) != RESET)
    {
        //清除全部中断标志
        DMA_ClearFlag(DMA1_Stream5, DMA_IT_TCIF5);
        DMA_Cmd(DMA1_Stream5, DISABLE);
        DmaUseNum = (DmaUseNum + 1) % 2;
        memset((char *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值 传输地址
        DMA1_Stream5->M0AR = (u32)DownBuf[DmaUseNum];
        DMA1_Stream5->NDTR = (DOWNLOAD_SIZE - 32);
        //打开DMA
        DMA_Cmd(DMA1_Stream5, ENABLE);
#else

    if(DMA_GetFlagStatus(DMA1_IT_TC3) != RESET)
    {
        //清除全部中断标志
        DMA_ClearFlag(DMA1_IT_TC3);
        DmaUseNum = (DmaUseNum + 1) % 2;
        DMA_Cmd(DMA1_Channel3, DISABLE);
        memset((u8 *)DownBuf[DmaUseNum], 0, DOWNLOAD_SIZE);
        // 设置DMA的传输值
        DMA1_Channel3 -> CNDTR = (DOWNLOAD_SIZE - 32);
        // 设置传输地址
        DMA1_Channel3 -> CMAR  = (u32)DownBuf[DmaUseNum];
        //打开DMA
        DMA_Cmd(DMA1_Channel3, ENABLE);
#endif
        downfile_timer = LocalTime;
        wifi_usart_out_timer = LocalTime;
#ifdef DEBUG0703
        printf("AAA:%d", strlen((char *)DownBuf[(DmaUseNum + 1) % 2]));
#endif
#ifndef DTU3PRO
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Dtu3Major.Property.netmode_select);
#else
        DownloadWriteFlash((char *)DownBuf[(DmaUseNum + 1) % 2], (u32 *)&DownOffset, Netmode_Used);
#endif
        Dmastartdown = 1;
    }
}

/***********************************************
** Function name:
** Descriptions:        wifi串口中断
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void WIFI_USART_IRQHandler(void)
{
    vu8 buffer = 0;

    if(USART_GetITStatus(WIFI_USARTx, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(WIFI_USARTx, USART_IT_RXNE);
        wifi_usart_out_timer = LocalTime;
        buffer = (u8)USART_ReceiveData(WIFI_USARTx);
        //开始标志位
        wifi_usart_start_flg = 1;
        //结束标志位
        wifi_usart_finish_flg = 0;
        wifi_usart_receive_buff[wifi_usart_rxd_len++] = buffer;
#ifdef DEBUG11

        if((wifi_usart_rxd_len >= 19) && (0 == strncmp((char *)&wifi_usart_receive_buff[wifi_usart_rxd_len - 19], "Client Disconnected", 19)))
        {
            //            wifi_out_time = 200;
            //            //清开始标志位
            //            wifi_usart_start_flg = 0;
            //            wifi_usart_rxd_len = 0;
            //            memset(wifi_usart_receive_buff, 0, sizeof(wifi_usart_receive_buff));
            //            if((WIFI_TRAN_SET < Wifi_state) && (Wifi_state < WIFI_ENTM))
            //            {
            //                Wifi_state = WIFI_E_REQ;
            //            }
            //            else
            //            {
            //                Wifi_state = WIFI_STA;
            //            }
            printf("Client Disconnected\r\n");
            APP_Flg = 0;
        }

#endif

        if((wifi_usart_rxd_len >= 16) && (0 == strncmp((char *)&wifi_usart_receive_buff[wifi_usart_rxd_len - 16], "Client Connected", 16)))
        {
#ifdef DEBUG11
            printf("Client Connected\r\n");
#endif
            //清开始标志位
            wifi_usart_start_flg = 0;
            wifi_usart_rxd_len = 0;
            memset((u8 *)wifi_usart_receive_buff, 0, sizeof(wifi_usart_receive_buff));

            if((WIFI_TRAN_SET < Wifi_state) && (Wifi_state < WIFI_ENTM) && (APP_Flg != 1))
            {
                Wifi_state = WIFI_ENTM;
            }
            else if((WIFI_WAIT <  Wifi_state) && (APP_Flg != 1))
            {
                Wifi_state = WIFI_WAIT;
            }

            Wifi_data_link_time = LocalTime - 1000;
            disconnect_app_timer = LocalTime;
            APP_Flg = 1;
            //wifi串口超时时间
            wifi_out_time = 100;
        }

        if(wifi_usart_rxd_len > SOCKET_LEN)
        {
            wifi_usart_rxd_len = 0;
            //开始标志位
            wifi_usart_start_flg = 0;
        }
    }
    else if(USART_GetFlagStatus(WIFI_USARTx, USART_FLAG_ORE) != RESET)
    {
        USART_ReceiveData(WIFI_USARTx);
    }
    else if(USART_GetFlagStatus(WIFI_USARTx, USART_FLAG_TC) != RESET)
    {
        //发送计数器大于1
        if(wifi_usart_tx_cnt > 1)
        {
            USART_SendData(WIFI_USARTx, wifi_usart_send_buff[wifi_usart_tx_rp]);
            wifi_usart_tx_rp = (wifi_usart_tx_rp + 1) % SOCKET_LEN;
        }

        //最后一位是空 不发
        if(wifi_usart_tx_cnt > 0)
        {
            wifi_usart_tx_cnt--;
        }

        USART_ClearITPendingBit(WIFI_USARTx, USART_IT_TC);
        USART_ClearITPendingBit(WIFI_USARTx, USART_FLAG_TC);
    }
}


/***********************************************
** Function name:
** Descriptions:        串口发送一个字符
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
//这样发送数据只需要发送第一个字符后 其他的中断发送 不用硬延时查询是否发送完
u8 Wifi_Usart_PutChar(u8 ch)
{
    // Wait until there is space in the TX buffer:
    vu8 i = 0;

    while(wifi_usart_tx_cnt > SOCKET_LEN)
    {
        if(i > 200)
        {
            wifi_usart_tx_cnt = 0;
        }

        i++;
    }

    if(0 == wifi_usart_tx_cnt)
    {
        wifi_usart_tx_wp = 0;
        wifi_usart_tx_rp = 1;
        wifi_usart_send_buff[wifi_usart_tx_wp] = ch;
    }
    else
    {
        wifi_usart_send_buff[wifi_usart_tx_wp] = ch;
    }

    wifi_usart_tx_wp = (wifi_usart_tx_wp + 1) % SOCKET_LEN;
    wifi_usart_tx_cnt++;
    return (ch);
}

/***********************************************
** Function name:
** Descriptions:        串口发送
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_Usart_SendData(u8 *dat, u16 len)
{
    vu16 i;

    for(i = 0; i < len; i++)
    {
        Wifi_Usart_PutChar(*(dat + i));

        if(i == (len - 1))
        {
            // Write first char directly to the UART SFR
            USART_SendData(WIFI_USARTx, (u8)wifi_usart_send_buff[0]);
        }
    }
}
/***********************************************
** Function name:
** Descriptions:        串口清除缓存数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Wifi_Usart_RxClear(void)
{
    vu16 i;

    for(i = 0; i < SOCKET_LEN; i++)
    {
        wifi_usart_receive_buff[i] = 0;
    }

    wifi_usart_rxd_len = 0;
}
/***********************************************
** Function name:
** Descriptions:        并网保护文件去头
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 Grid_Check(char *buffer)
{
    vu32 x = 0, y = 0;
    vu32 length;
    char *checkbuf = NULL;
    x = 0;
    y = 0;
    length = strlen(buffer);
    checkbuf = mymalloc(DOWNLOAD_SIZE);
    //    if(checkbuf == NULL)
    //    {
    //        return 0;
    //    }
    memset(checkbuf, 0, DOWNLOAD_SIZE);

    for(x = 0; x < length; x++)
    {
        if(strncmp(&buffer[x], "countrystd=", 11) == 0)
        {
            memcpy(checkbuf, &buffer[x], (length - x));
            memset(buffer, 0, DOWNLOAD_SIZE);
            memcpy(buffer, checkbuf, strlen(checkbuf));
            break;
        }
    }

    myfree(checkbuf);
    return 1;
}
/***********************************************
** Function name:
** Descriptions:        hex文件检测头
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
u8 BuffCheck(char *buffer)
{
    vu32 x = 0, y = 0;
    vu32 length = 0;
    char *checkbuf = NULL;
    x = 0;
    y = 0;
    length = strlen(buffer);

    if(length >= DOWNLOAD_SIZE)
    {
        length = DOWNLOAD_SIZE;
    }

    checkbuf = mymalloc(length + 32);
    //    if(checkbuf == NULL)
    //    {
    //        return 0;
    //    }
    memset(checkbuf, 0, length + 32);

    for(x = 0; x < length;)
    {
        if((x == 0) && (strncmp(&buffer[x], "HTTP", 4) != 0) && (DownOffset == 0))
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;

            if(strncmp(&buffer[x], "\r", 1) == 0)
            {
                x = x + 1;
                continue;
            }
            else if(strncmp(&buffer[x], "\n", 1) == 0)
            {
                x = x + 1;
                continue;
            }
            else if(strncmp(&buffer[x], ":", 1) == 0)
            {
                checkbuf[y] = '\r';
                checkbuf[y + 1] = '\n';
                y = y + 2;
                checkbuf[y] = buffer[x];
                x++;
                y++;
            }
            else
            {
                x++;
            }
        }
        else if(strncmp(&buffer[x], "\r", 1) == 0)
        {
            x = x + 1;
            continue;
        }
        else if(strncmp(&buffer[x], "\n", 1) == 0)
        {
            x = x + 1;
            continue;
        }
        else if(strncmp(&buffer[x], ":00000001FF", 11) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 11);
            x = x + 11;
            y = y + 11;
            memset(buffer, 0, DOWNLOAD_SIZE);
            memcpy(buffer, checkbuf, y);
            myfree(checkbuf);
            return 1;
        }
        else if(strncmp(&buffer[x], ":", 1) == 0)
        {
            if(strncmp(&buffer[x - 6], "Server:", 7) == 0)
            {}
            else if(strncmp(&buffer[x - 4], "Date:", 5) == 0)
            {}
            else if(strncmp(&buffer[x - 4], "Type:", 5) == 0)
            {}
            else if(strncmp(&buffer[x - 6], "Length:", 7) == 0)
            {}
            else if(strncmp(&buffer[x - 8], "Modified:", 9) == 0)
            {}
            else if(strncmp(&buffer[x - 10], "Connection:", 11) == 0)
            {}
            else if(strncmp(&buffer[x - 4], "ETag:", 5) == 0)
            {}
            else if(strncmp(&buffer[x - 6], "Ranges:", 7) == 0)
            {}
            else
            {
                checkbuf[y] = '\r';
                checkbuf[y + 1] = '\n';
                y = y + 2;
            }

            checkbuf[y] = buffer[x];
            x++;
            y++;
        }
        else if(strncmp(&buffer[x], "Server", 6) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 18);
            x = x + 18;
            y = y + 18;
        }
        else if(strncmp(&buffer[x], "Date", 4) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 33);
            x = x + 33;
            y = y + 33;
        }
        else if(strncmp(&buffer[x], "Content", 7) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 18);
            x = x + 18;
            y = y + 18;
        }
        else if(strncmp(&buffer[x], "Last", 4) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 40);
            x = x + 40;
            y = y + 40;
        }
        else if(strncmp(&buffer[x], "Connection", 10) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 20);
            x = x + 20;
            y = y + 20;
        }
        else if(strncmp(&buffer[x], "ETag", 4) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 21);
            x = x + 21;
            y = y + 21;
        }
        else if(strncmp(&buffer[x], "Accept", 6) == 0)
        {
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
            memcpy(&checkbuf[y], &buffer[x], 14);
            x = x + 14;
            y = y + 14;
        }
        else if(strncmp(&buffer[x], "bytes", 5) == 0)
        {
            memcpy(&checkbuf[y], &buffer[x], 5);
            x = x + 5;
            y = y + 5;
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
        }
        else if(strncmp(&buffer[x], "none", 4) == 0)
        {
            memcpy(&checkbuf[y], &buffer[x], 4);
            x = x + 4;
            y = y + 4;
            checkbuf[y] = '\r';
            checkbuf[y + 1] = '\n';
            y = y + 2;
        }
        else if(strncmp(&buffer[x], "CONNECT", 7) == 0)
        {
            x = x + 7;
        }
        else
        {
            checkbuf[y] = buffer[x];
            x++;
            y++;
        }
    }

    memset(buffer, 0, DOWNLOAD_SIZE);
    memcpy(buffer, checkbuf, y);
    myfree(checkbuf);
    return 0;
}
void DownloadWriteFlash(char *DownloadBuf, u32 *WriteOffset, u8 mode)
{
    vu32 length = 0;
    length = strlen(DownloadBuf);

    if(length >= DOWNLOAD_SIZE)
    {
        length = DOWNLOAD_SIZE;
    }

    if(strlen(DownloadBuf) != 0)
    {
#ifdef DEBUG0703
        printf("D_LEN:%d--perused:%d\n\r", strlen(DownloadBuf), my_mem_perused());
#endif

        //DTU程序下载
        if(DTU_UPDATE == downfile_module)
        {
            //接收到结尾接收完成
            if(1 == BuffCheck(DownloadBuf))
            {
#ifdef DEBUG
                printf("Receiving completed\r\n");
#endif
                DownloadEnd = 1;
            }

            if(strlen(DownloadBuf) > 0)
            {
                DTU_Program_Write_Length(DownloadBuf, *WriteOffset, (uint16_t)strlen(DownloadBuf));
            }
        }
        //微逆程序下载
        else if(MI_UPDATE == downfile_module)
        {
            //接收到结尾接收完成
            if(1 == BuffCheck(DownloadBuf))
            {
                DownloadEnd = 1;
            }

            if(strlen(DownloadBuf) > 0)
            {
                MI_Program_Write_Length(DownloadBuf, *WriteOffset, (uint16_t)strlen(DownloadBuf));
            }
        }

        if(strlen(DownloadBuf) > 0)
        {
            *WriteOffset = *WriteOffset + strlen(DownloadBuf);
        }

        if(DownloadEnd == 1)
        {
            switch(mode)
            {
                case WIFI_MODE:
                    Wifi_DMA(0);
                    break;

                case GPRS_MODE:
                    GPRS_DMA(0);
                    break;
#ifndef DTU3PRO

                case local_usart:
                    USART1_DMA(0);
                    break;
#else

                case CABLE_MODE:
                    http_echoclient_disconnect();
                    break;
#endif
            }
        }
    }
}

