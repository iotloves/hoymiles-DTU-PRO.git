//============================================================================
// Copyright (c) 2015, Hoymiles Converter Technology Co., Ltd.
//
// static const short DATA_TEST1[8]   __attribute__((at(0X08020800)))  =
// //start adr      end adr       crc adr     // y      m d
// Version         :   1.00
// {0x0802,0x1000,0x0809,0xB77C,0x9797,0x3AFD,0x07DE,0x04C6 };
// Create          :   2019/9/3 15:38:28
//
//============================================================================

const short DATA_TEST1[8]  __attribute__((section(".ARM.__at_0X08020000"))) =
    //start adr      end adr       crc adr     // y      m d
{
    0x0802,//start Hadr
    0x0200,//start Ladr
    0x0806,//end Hadr
    0x8630,//end Ladr
    0x0E5C,//crc pro
    0x9301,//Ver
    0x6178,//crc self
    0x0387
};//mm dd
