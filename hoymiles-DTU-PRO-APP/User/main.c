#include "main.h"
#include "DRM.h"
#include "SunSpec.h"
//测试服务器
//    memcpy(Dtu3Major.Property.ServerDomainName, "25p48032q7.wicp.vip", sizeof("25p48032q7.wicp.vip"));
//    Dtu3Major.Property.ServerPort = 10204;
//正式服务器
//    memcpy(Dtu3Major.Property.ServerDomainName, "datacn.hoymiles.com", sizeof("datacn.hoymiles.com"));
//    Dtu3Major.Property.ServerPort = 10081;
//DTU主要信息
extern volatile DtuMajor Dtu3Major;
extern vu8 Command;
//微型逆变器详细信息
extern volatile InverterDetail MIDetail;
//逆变器实时数据
extern volatile InverterReal MIReal[PORT_LEN];
extern volatile MeterMajor Meter[];
extern volatile MeterInfomation MeterInfo[];
//电表数量
extern vu8 MeterLinkNum;
extern vu8 SysState;
extern vu8 NetState;
extern vu8 MiState;
extern vu8 ModeState;
extern vu8 FaultState;
extern volatile DtuDetail Dtu3Detail;
extern volatile InverterMajor MIMajor[PORT_LEN];
//发送数据时间间隔15分钟
extern vu32 data_interval;
extern vu8 APP_Flg;
extern vu8 memory_save;
USBH_HOST  USB_Host;
USB_OTG_CORE_HANDLE  USB_OTG_Core;
volatile AlarmDataType AlarmDataBuf[10];
extern vu8 total_package_to_server;
extern vu32 server_link_time;
extern vu8 server_link_mode;
extern vu8 server_signal_strength;
extern vu8 downfile;
#ifdef MEMORY_DEBUG_TEXT
extern vu8 memory_test_save_cnt;
vu16 memory_test_time_cnt = 0;
#endif
int main(void)
{
#ifdef DTU_IAP
    //跳转后打开总中断
    __enable_irq();
    //中断偏移
    SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET;
#endif
#ifdef TimeTest
    vu32 time;
#endif
    vu16 count = 0;
    vu32 time1;
    vu32 time2;
    vu8 times;
    vu32 capacity = 0;
    vu16 Num = 0;
    calendar_obj calendar;
    SysState = Sys_open;
    SystemInit();
    //设置系统中断优先级分组2
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    //初始化LED
    SysTick_Init();
    LED_Init();
    my_mem_init();
    BackupSRAM_Init();
    My_RTC_Init();
    //    Delay_ms(500);
    hardware_delay_ms(1000);
    File_Processing();
    //    Data_Migration();
    UsartNrf_NrfInit();
    UsartNrf_MemInit();
    Version_Init();
    Usart_485_Config();
    //配置网络接口
    ETH_BSP_Config();
    //wifi串口初始化
    //Wifi_usart_config();
    //wifi power
    //Wifi_GPIO_Config();
    //GPRS串口初始化
    GPRS_USART_Config();
    //GPRS power
    GPRS_GPIO_Config();
    //GPRS key power
    GPRS_PWR_GPIO_Config();
    //IO初始化
    KEY_Init();
    //掉电保存区数据校验
    Memory_BKP_Check();
    //MI连接状态
    Led_MI_LinkState(false);
    //读取电表信息
    MeterLinkNum = MeterMajor_Read((MeterMajor *)Meter);
    //初始化USB主机
    USBH_Init(&USB_OTG_Core, USB_OTG_FS_CORE_ID, &USB_Host, &USBH_MSC_cb, &USR_Callbacks);
    DRM_Init();
#ifdef DTU_IAP
#ifndef DEBUG
    IWDG_Init(IWDGTomes);
#endif
#endif

    if(Dtu3Major.Property.server_send_time == 0)
    {
        Dtu3Major.Property.server_send_time = data_interval / (1000 * 60);
    }

    if((Dtu3Detail.Property.DHCP_Switch != 1) && ((
                (Dtu3Detail.Property.IP_ADDR[0] == 0) &&
                (Dtu3Detail.Property.IP_ADDR[1] == 0) &&
                (Dtu3Detail.Property.IP_ADDR[2] == 0) &&
                (Dtu3Detail.Property.IP_ADDR[3] == 0)) || (
                (Dtu3Detail.Property.subnet_mask[0] == 0) &&
                (Dtu3Detail.Property.subnet_mask[1] == 0) &&
                (Dtu3Detail.Property.subnet_mask[2] == 0) &&
                (Dtu3Detail.Property.subnet_mask[3] == 0)) || (
                (Dtu3Detail.Property.default_gateway[0] == 0) &&
                (Dtu3Detail.Property.default_gateway[1] == 0) &&
                (Dtu3Detail.Property.default_gateway[2] == 0) &&
                (Dtu3Detail.Property.default_gateway[3] == 0))))
    {
        Dtu3Detail.Property.DHCP_Switch = 1;
    }

    while(1)
    {
        USBH_Process(&USB_OTG_Core, &USB_Host);
        UsartNrf_SendProcessLoop();
        Wifi_Tcp();
        Wifi_Http();
        Internet_Adaptive();
        ETHProtocol();

        if(memory_save == 1)
        {
            count = 0;

            while(Get_FileStatus() != 0)
            {
#ifndef DEBUG
                IWDG_Feed();
#endif
                File_Processing();
                count++;

                if(count >= 500)
                {
                    break;
                }
            }

            memory_save = 0;
        }
        else if((downfile != 1) || (Netmode_Used  != CABLE_MODE))
        {
            File_Processing();
        }

        if(Netmode_Used  == GPRS_MODE)
        {
            GPRS_Server();
        }

        ResetPolling();
        DRM_Process();
        LWHV_Process();
        ANTI_REFLUX3_process();
        Modbus_RTU_Process();
        SunSpec_RTU_Process();
        Timed_Query();
        IWDG_Feed();
    }
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
