/**
  ******************************************************************************
  * @file    netconf.h
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013
  * @brief   This file contains all the functions prototypes for the netconf.c
  *          file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NETCONF_H
#define __NETCONF_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx.h"

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
#define DHCP_START                      1
#define DHCP_WAIT_ADDRESS               2
#define DHCP_ADDRESS_ASSIGNED           3
#define DHCP_TIMEOUT                    4
#define DHCP_LINK_DOWN                  5
#define ETH_Wite_Time                   30000
enum ETH_Status
{
    ETH_WAIT                    = 0,
    ETH_UPData_DTU              = 1,        //升级DTU
    ETH_Config_File             = 2,        //并网保护文件
    ETH_RE_Server               = 3,
    ETH_GET_CONFIG              = 4,        //获取配置信息
    ETH_SET_CONFIG              = 5,        //设置配置信息
    ETH_Send_Info               = 6,        //信息
    ETH_Send_Data               = 7,        //数据
    ETH_Send_History            = 8,        //历史
    ETH_Send_Heart              = 9,        //心跳
    ETH_Status                  = 10,
    // APP
    ETH_App_GetConfigReq        = 11,       //App获取配置信息请求回复
    ETH_App_SetConfigReq        = 12,       //App设置配置信息请求回复
    ETH_App_InfoReq             = 13,       //App设备信息请求回复
    ETH_App_RealDataReq         = 14,       //App实时数据请求回复
    ETH_App_ALERTINGReq         = 15,       //App告警上报
    ETH_App_SetIDReq            = 16,       //App配置ID请求回复
    ETH_App_ConfigCommandReq    = 17,       //App控制命令请求回复
    ETH_App_StatusDataReq       = 18,       //App控制命令状态回复
    ETH_App_HeartBeatReq        = 19,       //APP心跳
    //HTTP
    ETH_HTTP_Download           = 20,       //HTTP下载文件

    ETH_App_DevConfigFetchReq   = 21,       //App配置获取请求
    ETH_App_DevConfigPutReq     = 22,       //App配置设置请求
    ETH_DTU_ALERTING            = 23,       //DTU告警上报
    ETH_MI_ALERTING             = 24,       //MI告警上报
    ETH_MI_SPSPEND_ALERTING     = 25,       //MI挂起告警上报
    ETH_App_WInfoReq            = 26,       //事件上报
    ETH_Send_History_ALERTING   = 27,       //历史告警
    ETH_HTTP_Wait_Download      = 28,       //HTTP下载文件
};
enum TCP_Status
{
    TCP_Server_Start            = 0,
    TCP_Domain_Check            = 1,
    TCP_DNS_Analy               = 2,
    TCP_DNS_Wait                = 3,
    TCP_Ping_Server             = 4,
    TCP_Link_Server             = 5,
    TCP_Link_Wait               = 6,
    TCP_Offline                 = 7,
    TCP_Down_Wait               = 8,

};
extern vu8 USE_DHCP;


#define LOCAL_SERVER_PORT          1033

#define UDP_SERVER_PORT            5000   /* define the UDP local connection port */
#define UDP_CLIENT_PORT            6000   /* define the UDP remote connection port */

#define CLIENT
#define CLIENT1
//#define CLIENT2

extern vu8 MAC_ADDR[];
extern vu8 IP_ADDR[];
extern vu8 NETMASK_ADDR[];
extern vu8 GW_ADDR[];
/**
 * @brief  Number of milliseconds when to check for link status from PHY
 */
#ifndef LINK_TIMER_INTERVAL
#define LINK_TIMER_INTERVAL        1000
#endif

/* MII and RMII mode selection, for STM324xG-EVAL Board(MB786) RevB ***********/
#define RMII_MODE

/* Uncomment the define below to clock the PHY from external 25MHz crystal (only for MII mode) */
#ifdef  MII_MODE
#define PHY_CLOCK_MCO
#endif

/* STM324xG-EVAL jumpers setting
    +==========================================================================================+
    +  Jumper |       MII mode configuration            |      RMII mode configuration         +
    +==========================================================================================+
    +  JP5    | 2-3 provide 25MHz clock by MCO(PA8)     |  Not fitted                          +
    +         | 1-2 provide 25MHz clock by ext. Crystal |                                      +
    + -----------------------------------------------------------------------------------------+
    +  JP6    |          2-3                            |  1-2                                 +
    + -----------------------------------------------------------------------------------------+
    +  JP8    |          Open                           |  Close                               +
    +==========================================================================================+
  */

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void LwIP_Init(void);
void LwIP_Pkt_Handle(void);
u32 LwIP_Periodic_Handle(vu32 localtime);
void ETHProtocol(void);
void Generate_Mac(void);
void Set_Mac(u8 *MAC);
void ETH_Get_Reply(void);

#ifdef __cplusplus
}
#endif

#endif /* __NETCONF_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
