/**
******************************************************************************
* @file    http_echoclient.c
* @author  MCD Application Team
* @version V1.1.0
* @date    31-July-2013
* @brief   http echoclient application using LwIP RAW API
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
*
* Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at:
*
*        http://www.st.com/software_license_agreement_liberty_v2
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/tcp.h"
#include "lwip/dns.h"
#include "memp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "usart.h"
#include "malloc.h"
#include "string.h"
#include "Memory.h"
#include "netconf.h"
#include "Ethernet.h"
#include "http_client.h"
#include "usart_wifi.h"
#include "HW_Version.h"
#include "usart_nrf.h"
#include "gprs.h"
#include "lwip/priv/tcp_priv.h"
#include "TimeProcessing.h"
#include "iwdg.h"
#include "Memory_Tools.h"
#define HTTP_DOWN_PACK_SIZE         1024*15

#if LWIP_TCP
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern volatile DtuMajor Dtu3Major;
extern vu32 DownOffset;
//#ifndef DTU3PRO
extern volatile char DownBuf[2][DOWNLOAD_SIZE];
//#else
//extern char *DownBuf[2];
//#endif
vu32 DNSTimer = 0;
vu32 http_time = 0;
vu32 http_dns_time = 0;
vu32 http_dns_times = 0;
vu32 http_message_count = 0;
lwip_addr File_addr;
char *File_Link = NULL;
vu8 memory_save = 0;
vu8 down_end = 0;
vu8 HttpClose = 0;
vu8 get_dns_ok = 0;
vu8 Http_Status = 0;
vu8 Http_Start = 0;
extern vu8 HttpMode;
//下载完成标志位
extern vu8 DownloadEnd;
extern vu8 up_pro_domain[];
extern vu8 downfile_module;
extern vu8 user_http_file_times;
extern vu8 downfile;
extern vu8 memory_down_mode;
vu8 mipadr[4];
vu8 get_data = 0;
/*文件分包下载*/
//下载文件总大小
vu32 down_file_size = 0;
//请求次数
vu16 request_cnt    = 0;
//单包有效长度
vu32 pack_size    = 0;
//下一包请求标记
vu8 request_next = 0;
vu32 TotalReceived = 0;
vu8 http_free = 0;
vu8 http_check = 0;
volatile lwip_addr http_addr;
volatile lwip_addr DNS_ip_addr;
volatile char http_link[ROW_MAX_LEN + ROW_MAX_LEN];
vu8 http_state = HTTP_WAIT;
/* ECHO protocol states */
enum http_echoclient_states
{
    ES_NOT_CONNECTED = 0,
    ES_CONNECTED,
    ES_RECEIVED,
    ES_CLOSING,
};

/* structure to be passed as argument to the http callbacks */
struct http_echoclient
{
    enum http_echoclient_states state; /* connection status */
    struct tcp_pcb *pcb;          /* pointer on the current tcp_pcb */
    struct pbuf *p_tx;            /* pointer on pbuf to be transmitted */
};

struct tcp_pcb *http_echoclient_pcb;
struct http_echoclient *http_echoclient_es;
struct tcp_pcb *http_pcb;
/* Private function prototypes -----------------------------------------------*/
static err_t http_echoclient_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void http_echoclient_connection_close(struct tcp_pcb *tpcb, struct http_echoclient *es);
static err_t http_echoclient_poll(void *arg, struct tcp_pcb *tpcb);
static err_t http_echoclient_sent(void *arg, struct tcp_pcb *tpcb, u16 len);
static void http_echoclient_send(struct tcp_pcb *tpcb, struct http_echoclient *es);
static err_t http_echoclient_connected(void *arg, struct tcp_pcb *tpcb, err_t err);

/***********************************************
** Function name:
** Descriptions:        DNS后的地址
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
#ifdef LWIP1_4_1
void my_found(const char *name, lwip_addr *ipaddr, void *arg)
#else
void my_found(const char *name, const lwip_addr *ipaddr, void *arg)
#endif
{
    //08 8a 00 20 = 8.138.0.32
    mipadr[3] = ((u16)(ntohl((ipaddr)->addr)) & 0xff);
    mipadr[2] = ((u16)(ntohl((ipaddr)->addr) >> 8) & 0xff);
    mipadr[1] = ((u16)(ntohl((ipaddr)->addr) >> 16) & 0xff);
    mipadr[0] = ((u16)(ntohl((ipaddr)->addr) >> 24) & 0xff);

    if((mipadr[0] == 8) && (mipadr[1] == 138) && (mipadr[2] == 0) && (mipadr[3] == 32))
    {
        //不插网线也是这个获取
    }
    else if((mipadr[0] == 0) && (mipadr[1] == 0) && (mipadr[2] == 0) && (mipadr[3] == 0))
    {
        //不插网线也是这个获取
    }
    else if((mipadr[0] == 32) && (mipadr[1] == 109) && (mipadr[2] == 1) && (mipadr[3] == 32))
    {
        //不插网线也是这个获取
    }
    else
    {
#ifdef DEBUG
        printf("%d.%d.%d.%d\n", mipadr[0], mipadr[1], mipadr[2], mipadr[3]);
#endif
        get_dns_ok = 1;
    }
}

void URLResolution(char *WebAddr, lwip_addr *ip_addr_get, int *port, char *ipfile) //网址解析
{
    vu8 ip[4] = {0};
    vu8 i = 0;
    vu8 j = 0;
    vu8 k = 0;
    vu8 l = 0;
    char *get = NULL;
    char *URL = NULL;
    vu8 IPorURL = 0;
    get = malloc(20 * sizeof(char));
    memset(get, 0, 20 * sizeof(char));
    URL = malloc(20 * sizeof(char));
    memset(URL, 0, 20 * sizeof(char));

    if(strncmp(&WebAddr[i], "http://", 7) == 0)
    {
        i += 7;
        l = i;
    }

    if((strchr(WebAddr + i, ':') != NULL) || (strchr(WebAddr + i, '/') != NULL))
    {
        for(j = i; i < strlen(WebAddr); i++)
        {
            if((WebAddr[i] == '.') || (WebAddr[i] == ':') || (WebAddr[i] == '/'))
            {
                if(IPorURL == 0)
                {
                    sscanf(get, "%d", (int *)&ip[k]);
                    memset(get, 0, 20 * sizeof(char));
                    k++;
                    j = i + 1;

                    if((WebAddr[i] == ':') || (WebAddr[i] == '/'))
                    {
                        IP4_ADDR(ip_addr_get, ip[0], ip[1], ip[2], ip[3]);
                        i++;
                        break;
                    }
                }
                else if((WebAddr[i] == ':') || (WebAddr[i] == '/'))
                {
                    memset(get, 0, 20 * sizeof(char));
                    memcpy(URL, WebAddr + l, i - l);
                    dns_gethostbyname(URL, ip_addr_get, my_found, NULL);
                    i++;
                    break;
                }
            }
            else if(WebAddr[i] >= '0' && WebAddr[i] <= '9')
            {
                IPorURL = 0;
                get[i - j] = WebAddr[i];
            }
            else
            {
                IPorURL = 1;
            }
        }
    }

    for(j = i; i < strlen(WebAddr); i++)
    {
        if(WebAddr[i] == '/')
        {
            sscanf(get, "%d", port);
            memset(get, 0, 20 * sizeof(char));
            break;
        }

        get[i - j] = WebAddr[i];
    }

    memcpy(ipfile, WebAddr + i, strlen(WebAddr) - i);
    free(get);
    free(URL);
}

/* Private functions ---------------------------------------------------------*/
/**
* @brief  Connects to the http echo server
* @param  None
* @retval None
*/
u8 http_echoclient_connect(lwip_addr tcp_addr, u16 tcp_port, char *tcp_link)
{
    lwip_addr DestIPaddr;
    vu16 port = 4545;
    File_addr = tcp_addr;
    File_Link = tcp_link;
    memset((char *)DownBuf[0], 0, DOWNLOAD_SIZE);
    memset((char *)DownBuf[1], 0, DOWNLOAD_SIZE);

    if(strlen(File_Link) == 0)
    {
        return 0;
    }

    if(http_free == 1)
    {
        http_echoclient_connection_close(http_pcb, http_echoclient_es);
    }

    /* create new tcp pcb */
    http_echoclient_pcb = tcp_new();

    if(http_echoclient_pcb == NULL)
    {
        memp_free(MEMP_TCP_PCB, http_echoclient_pcb);
    }

    if(http_echoclient_pcb != NULL)
    {
        while(tcp_bind(http_echoclient_pcb, IP_ADDR_ANY, port) != ERR_OK)
        {
            port++;
        }

        IP4_ADDR(&DestIPaddr, (u8)(File_addr.addr), (u8)(File_addr.addr >> 8), \
                 (u8)(File_addr.addr >> 16), (u8)(File_addr.addr >> 24));
        /* connect to destination address/port */
        tcp_connect(http_echoclient_pcb, &DestIPaddr, tcp_port, http_echoclient_connected);
#ifdef DEBUG
        printf("\n\r link ok \r\n");
#endif
        return 1;
    }
    else
    {
        /* deallocate the pcb */
        memp_free(MEMP_TCP_PCB, http_echoclient_pcb);
#ifdef DEBUG
        printf("\n\r can not create tcp pcb");
#endif
        return 0;
    }
}

void http_clear_flag(void)
{
    http_time = 0;
    Http_Start = 0;
    Http_Status = 0;
    user_http_file_times = 0;
    http_state = HTTP_WAIT;
}
void http_execution(void)
{
    if((LocalTime - DNSTimer) >= TCP_TMR_INTERVAL)
    {
        DNSTimer =  LocalTime;
        dns_tmr();
    }

    if(downfile == DOWNFILE_START)
    {
        /*增加下载超时机制，超时时间3分钟，最多下载次数3次*/
        if(((LocalTime - http_time) > 1000 * 60 * 3) && (Http_Start == 1))
        {
            if(user_http_file_times > 2)
            {
                downfile = 0;
                Http_Start = 0;
                Http_Status = 0;
                get_dns_ok = 0;
                memory_down_mode = 0;
                user_http_file_times = 0;
                http_state = HTTP_WAIT;

                if(http_free == 1)
                {
                    http_echoclient_connection_close(http_pcb, http_echoclient_es);
                }
            }
            else
            {
                /*下载失败清除下载标志位，允许历史数据存储*/
                Http_Start = 0;
                Http_Status = 1;

                if(http_free == 1)
                {
                    http_echoclient_connection_close(http_pcb, http_echoclient_es);
                }

                http_dns_times = 0;
                http_state = HTTP_WAIT;
            }
        }

        /*hzwang_20200422*/
        switch(http_state)
        {
            case HTTP_WAIT:
                if(Http_Status == 1)
                {
                    if(HttpMode == 1)
                    {
                        get_dns_ok = 0;
                        dns_gethostbyname((char *)up_pro_domain, (lwip_addr *)&DNS_ip_addr, my_found, NULL);
                        http_state = HTTP_DNS_ANALY;
                        http_time = LocalTime;
                        http_dns_time = LocalTime;
                    }
                    else
                    {
                        http_state = HTTP_IP_LINK;
                    }
                }

                break;

            case HTTP_DNS_ANALY:
                if(get_dns_ok == 1)
                {
                    up_pro_destip[0] = mipadr[0];
                    up_pro_destip[1] = mipadr[1];
                    up_pro_destip[2] = mipadr[2];
                    up_pro_destip[3] = mipadr[3];
                    http_state = HTTP_IP_LINK;
                    http_time = LocalTime;
                }
                else if((LocalTime - http_dns_time) >= 60 * 1000)
                {
                    if(http_dns_times > 3)
                    {
                        user_http_file_times++;
                    }
                    else
                    {
                        http_dns_times++;
                        Http_Status = 1;
                        HttpMode = 1;
                        http_state = HTTP_WAIT;
                    }
                }

                break;

            case HTTP_IP_LINK:
                if(DTU_UPDATE == downfile_module)
                {
                    Download_DTUProgram_Delete();
                }
                else
                {
                    Download_MIProgram_Delete();
                }

                DownOffset = 0;
                user_http_file_times++;
                Http_Status = 0;
                memory_down_mode = 1;
                memset((char *)http_link, 0, ROW_MAX_LEN + ROW_MAX_LEN);
                sprintf((char *)http_link, "%s%s", up_pro_url, up_pro_filename);
                IP4_ADDR(&http_addr, up_pro_destip[0], up_pro_destip[1], up_pro_destip[2], up_pro_destip[3]);
                //清除多包数据
                down_file_size = 0;
                //请求次数
                request_cnt    = 0;
                //单包有效长度
                pack_size    = 0;
                //下一包请求标记
                request_next = 1;
                http_time = LocalTime;
                http_state = HTTP_PACKAGE_REQUEST;
                break;

            //        case HTTP_WAIT_MEMORY_SAVE:
            //            if((Get_FileStatus() == 0) && (Queue_IsEmpty() == 0))
            //            {
            //                memory_save = 0;
            //                http_state = HTTP_PACKAGE_REQUEST;
            //            }
            //            else
            //            {
            //                http_time = LocalTime;
            //                memory_save = 1;
            //            }

            //            break;

            case HTTP_PACKAGE_REQUEST:
                if(request_next == 1)
                {
                    request_next = 0;
                    Http_Start = 1;
                    pack_size = 0;
                    memory_save = 0;
                    http_time = LocalTime;
                    get_data = 0;
                    down_end = 0;

                    if(http_addr.addr != 0)
                    {
                        http_echoclient_connect(http_addr, up_pro_destport, (char *)http_link);
                    }
                }

                http_check = 1;
                http_state = HTTP_WAIT_DOWN;
                break;

            case HTTP_WAIT_DOWN:
                if(down_end == 1)
                {
                    if(http_free == 1)
                    {
                        http_echoclient_connection_close(http_pcb, http_echoclient_es);
                    }

                    http_state = HTTP_PACKAGE_REQUEST;
                }
                else if(((LocalTime - http_time) > 1000 * 20) && (get_data == 0))
                {
                    if(http_free == 1)
                    {
                        http_echoclient_connection_close(http_pcb, http_echoclient_es);
                    }

                    request_next = 1;
                    http_state = HTTP_PACKAGE_REQUEST;
                }
                else if(((LocalTime - http_time) > 1000 * 90) && (get_data == 1) && (http_check == 1))
                {
                    http_check = 0;
                    DownloadEnd = 1;

                    if(http_free == 1)
                    {
                        http_echoclient_connection_close(http_pcb, http_echoclient_es);
                    }
                }

                break;
        }
    }
}


/**
* @brief  Disconnects to the TCP echo server
* @param  None
* @retval None
*/
void http_echoclient_disconnect(void)
{
    HttpClose = 1;
    memory_down_mode = 0;
}

/**
  * @brief Function called when http connection established
  * @param tpcb: pointer on the connection contol block
  * @param err: when connection correctly established err should be ERR_OK
  * @retval err_t: returned error
  */
static err_t http_echoclient_connected(void *arg, struct tcp_pcb *tpcb, err_t err)
{
    struct http_echoclient *es = NULL;
    vu16 data_len = 1024;
    u8 *http_data = NULL;
    http_data = mymalloc(data_len);
    //    if(http_data == NULL)
    //    {
    //        return 0;
    //    }
    memset(http_data, 0, data_len);
    http_time = LocalTime;

    if(err == ERR_OK)
    {
        /* allocate structure es to maintain http connection informations */
        es = (struct http_echoclient *)mymalloc(sizeof(struct http_echoclient));
        //        if(es == NULL)
        //        {
        //            return 0;
        //        }
        http_echoclient_es = es;

        if(es != NULL)
        {
            http_message_count = 0;
            es->state = ES_CONNECTED;
            es->pcb = tpcb;
            memset(http_data, 0, sizeof(*http_data));

            //            if(HttpMode == 1)
            //            {
            //                sprintf((char *)http_data,
            //                        "GET %s%s HTTP/1.1\r\n"
            //                        "Host: %s\r\n"
            //                        "Connection: keep-alive\r\n"
            //                        "Upgrade-Insecure-Requests: 1\r\n"
            //                        "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\r\n"
            //                        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3\r\n"
            //                        //                    "Accept-Encoding: gzip, deflate\r\n"
            //                        "Accept-Language: zh-CN,zh;q=0.9,en;q=0.8\r\n"
            //                        //                    "If-None-Match: \"5d22c7d4-1e8e7c5\"\r\n"
            //                        //                    "If-Modified-Since: Mon, 08 Jul 2019 04:34:28 GMT\r\n"
            //                        "\r\n", up_pro_url, up_pro_filename, up_pro_domain
            //                       );
            //            }
            if(HttpMode == 1)
            {
                if((((request_cnt + 1) * HTTP_DOWN_PACK_SIZE) > down_file_size) && (down_file_size != 0))
                {
                    sprintf((char *)http_data,
                            "GET %s%s HTTP/1.0\r\n"
                            "Host: %s\r\n"
                            "Range: bytes=%d-%d\r\n"
                            "User-Agent: HM_DTU_PRO\r\n"
                            //                            "Connection: keep-alive\r\n"
                            "Accept: */* \r\n"
                            "\r\n", up_pro_url, up_pro_filename, up_pro_domain,
                            (request_cnt * HTTP_DOWN_PACK_SIZE),
                            down_file_size);
                }
                else
                {
                    sprintf((char *)http_data,
                            "GET %s%s HTTP/1.0\r\n"
                            "Host: %s\r\n"
                            "Range: bytes=%d-%d\r\n"
                            "User-Agent: HM_DTU_PRO\r\n"
                            //                            "Connection: keep-alive\r\n"
                            "Accept: */* \r\n"
                            "\r\n", up_pro_url, up_pro_filename, up_pro_domain,
                            (request_cnt * HTTP_DOWN_PACK_SIZE),
                            ((request_cnt + 1)*HTTP_DOWN_PACK_SIZE - 1));
                }

                //增加发送：RANGE: 开始-结束 结束>=总长度时候结束
                //解析应答: Content-Range: bytes 1254024-1254025/1254026        //获取2字节
            }
            else
            {
                sprintf((char *)http_data, "GET %s HTTP/1.1\r\n"
                        "Host:%d.%d.%d.%d\r\n"
                        "Accept: */*\r\n"
                        "Accept-Language: zh-cn\r\n"
                        "Connection: Keep-Alive\r\n\r\n", \
                        File_Link, (u8)(File_addr.addr), (u8)(File_addr.addr >> 8),
                        (u8)(File_addr.addr >> 16), (u8)(File_addr.addr >> 24));
            }

            //printf("%s\n", http_data);
            //            printf("%s\n", http_data);
            //#ifdef DEBUG
            //            printf("%s\n", http_data);
            //#endif
            //                                  "/cfs/hex/1901/04/848392097789775872.hex",119,3,31,20);
            //            printf ("GET %s HTTP/1.1\r\n"
            //                                  "Host:%d.%d.%d.%d\r\n"
            //                                  "Connection: Keep-Alive\r\n\r\n",File_Link,\
            //                                  (u8)(File_addr.addr),(u8)(File_addr.addr >> 8), \
            //                                  (u8)(File_addr.addr >> 16),(u8)(File_addr.addr >> 24));
            /* allocate pbuf */
            es->p_tx = pbuf_alloc(PBUF_TRANSPORT, (u16)strlen((char *)http_data), PBUF_POOL);

            if(es->p_tx)
            {
                /* copy data to pbuf */
                pbuf_take(es->p_tx, (char *)http_data, (u16)strlen((char *)http_data));
                /* pass newly allocated es structure as argument to tpcb */
                tcp_arg(tpcb, es);
                /* initialize LwIP tcp_recv callback function */
                tcp_recv(tpcb, http_echoclient_recv);
                /* initialize LwIP tcp_sent callback function */
                tcp_sent(tpcb, http_echoclient_sent);
                /* initialize LwIP http_poll callback function */
                tcp_poll(tpcb, http_echoclient_poll, 1);
                /* send data */
                http_echoclient_send(tpcb, es);
                myfree(http_data);
                return ERR_OK;
            }
        }
        else
        {
            /* close connection */
            if(http_free == 1)
            {
                http_echoclient_connection_close(tpcb, es);
            }

            /* return memory allocation error */
            myfree(http_data);
            return ERR_MEM;
        }
    }
    else
    {
        /* close connection */
        if(http_free == 1)
        {
            http_echoclient_connection_close(tpcb, es);
        }
    }

    http_free = 1;
    http_pcb = tpcb;
    myfree(http_data);
    return err;
}


u32 Parse_Content_Length(char *data, u16 *len)
{
    vu32 size = 0, S = 1, httpoffset = 0;
    vs16 i = 0;
    vs16 j = 0;
    volatile char sizestring[8];
    char *ptr = NULL;
    char *buf = NULL;
    memset((char *)sizestring, 0, sizeof(sizestring));

    /* find Content-Length data in packet buffer */
    for(i = 0; i < *len; i++)
    {
        if(strncmp((char *)(data + i), "Content-Length: ", 16) == 0)
        {
            httpoffset = (u32)(i + 16);
            break;
        }
    }

    /* read Content-Length value */
    i = 0;
    ptr = (char *)(data + httpoffset);

    while(*(ptr + i) != 0x0d)
    {
        sizestring[i] = *(ptr + i);
        i++;
        httpoffset++;
    }

    if(i > 0)
    {
        /* transform string data into numeric format */
        for(j = i - 1; j >= 0; j--)
        {
            size += (sizestring[j] - 0x30) * S;
            S = S * 10;
        }
    }

    memset((char *)sizestring, 0, sizeof(sizestring));

    for(i = 0; i < *len; i++)
    {
        if(strncmp((char *)(data + i), "Accept-Ranges: bytes", 20) == 0)
        {
            buf = mymalloc((u32)(*len - i));
            //            if(buf == NULL)
            //            {
            //                return 0;
            //            }
            memset(buf, 0, (u32)(*len - i));
            memcpy(buf, &data[i], (u32)(*len - i));
            memset(data, 0, *len);
            memcpy(data, buf, (u32)(*len - i));
            myfree(buf);
            break;
        }
    }

    return size;
}

u32 HTTP_Packet_Header_Analy(char *payload, u16 len, char *save_buf)
{
    vu32 size = 0;
    vu16 i = 0;
    vu16 j = 0;
    vu16 k = 0;
    vu16 l = 0;
    vu8 find = 0;
    volatile char sizestring[8];
    memset((char *)sizestring, 0, sizeof(sizestring));

    if(down_file_size == 0)
    {
        for(i = 0; i < len; i++)
        {
            if(strncmp(&payload[i], "Accept-Ranges:", 14) == 0)
            {
                l = i;
                i = i + 14;
                break;
            }
        }

        for(; i < len; i++)
        {
            if(strncmp(&payload[i], "Content-Range:", 14) == 0)
            {
                k = i;
                i = i + 14;
                break;
            }
        }

        for(; i < len; i++)
        {
            if(strncmp(&payload[i], "/", 1) == 0)
            {
                i = i + 1;
                break;
            }
        }

        for(j = i; i < len; i++)
        {
            if(strncmp(&payload[i], "\r\n", 2) == 0)
            {
                down_file_size = astr2int(&payload[j], (u8)(i - j));
                i = i + 2;
                break;
            }
        }

        for(; i < len; i++)
        {
            if(strncmp(&payload[i], "\r\n\r\n", 4) == 0)
            {
                i = i + 4;
                break;
            }
        }

        memset(save_buf, 0, DOWNLOAD_SIZE);
        memcpy(&save_buf[0], &payload[l], (k - l));
        memcpy(&save_buf[k - l], &payload[i], (len - i));
        size = len - i;
    }
    else
    {
        for(i = 0; i < len; i++)
        {
            if(strncmp(&payload[i], "\r\n\r\n", 4) == 0)
            {
                i = i + 4;
                find = 1;
                break;
            }
        }

        memset(save_buf, 0, DOWNLOAD_SIZE);

        if(find == 1)
        {
            memcpy(save_buf, &payload[i], (len - i));
            size = len - i;
        }
        else
        {
            memcpy(save_buf, payload, len);
            size = len;
        }
    }

    return size;
}

/**
* @brief http_receiv callback
* @param arg: argument to be passed to receive callback
* @param tpcb: tpcb connection control block
* @param err: receive error code
* @retval err_t: retuned error
**/
static err_t http_echoclient_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
    struct http_echoclient *es;
    err_t ret_err;
    LWIP_ASSERT("arg != NULL", arg != NULL);
    es = (struct http_echoclient *)arg;
    http_echoclient_es = es;
    HttpClose = 0;
    http_time = LocalTime;

    /* if we receive an empty tcp frame from server => close connection */
    if(p == NULL)
    {
        /* remote host closed connection */
        es->state = ES_CLOSING;

        if(es->p_tx == NULL)
        {
            /* we're done sending, close connection */
            http_echoclient_connection_close(tpcb, es);
        }
        else
        {
            /* send remaining data*/
            http_echoclient_send(tpcb, es);
        }

        ret_err = ERR_OK;
    }
    /* else : a non empty frame was received from echo server but for some reason err != ERR_OK */
    else if(err != ERR_OK)
    {
        /* free received pbuf*/
        pbuf_free(p);
        ret_err = err;
    }
    else if(es->state == ES_CONNECTED)
    {
        /* increment message count */
        http_message_count++;
        /* Acknowledge data reception */
        tcp_recved(tpcb, p->tot_len);
        get_data = 1;
        pack_size = pack_size +  HTTP_Packet_Header_Analy(p->payload, p->len, (char *)DownBuf[0]);
        DownloadWriteFlash((char *)DownBuf[0], (u32 *)&DownOffset, Netmode_Used);

        if((pack_size) >= (HTTP_DOWN_PACK_SIZE))
        {
            request_cnt++;
            request_next = 1;
            memory_save = 1;
            down_end = 1;
        }

        if((HttpClose == 1) || ((request_cnt * HTTP_DOWN_PACK_SIZE) > down_file_size))
        {
            down_end = 0;
            memory_save = 1;

            if(http_free == 1)
            {
                http_echoclient_connection_close(tpcb, es);
            }
        }

        /* free received pbuf*/
        pbuf_free(p);
        ret_err = ERR_OK;
    }
    /* data received when connection already closed */
    else
    {
        /* Acknowledge data reception */
        tcp_recved(tpcb, p->tot_len);
        /* free pbuf and do nothing */
        pbuf_free(p);
        ret_err = ERR_OK;
    }

    http_pcb = tpcb;
    return ret_err;
}
//void test_get()
//{
//    if(strlen(recdata) != 0)
//    {
//        if(FileSize != 0)
//        {
//            //          printf("%d\n",strlen(recdata));
//            //          printf("hello\n");
//            if((TotalReceived + strlen(recdata)) >= FileSize)
//            {
//                MI_Program_Write_Length(recdata, TotalReceived, FileSize - TotalReceived);
//            }
//            else
//            {
//                MI_Program_Write_Length(recdata, TotalReceived, strlen(recdata));
//            }
//            //printf("%s", recdata);
//            TotalReceived += strlen(recdata);
//            if(TotalReceived >= FileSize)
//            {
//                printf("ok\n");
//                //close();
//            }
//        }
//        memset(recdata, 0, strlen(recdata));
//        myfree(recdata);
//        recdata = mymalloc(sizeof(char));
//    }
//}
/**
* @brief function used to send data
* @param  tpcb: tcp control block
* @param  es: pointer on structure of type echoclient containing info on data
*             to be sent
* @retval None
*/
static void http_echoclient_send(struct tcp_pcb *tpcb, struct http_echoclient *es)
{
    struct pbuf *ptr;
    err_t wr_err = ERR_OK;

    while((wr_err == ERR_OK) &&
            (es->p_tx != NULL) &&
            (es->p_tx->len <= tcp_sndbuf(tpcb)))
    {
        /* get pointer on pbuf from es structure */
        ptr = es->p_tx;
        /* enqueue data for transmission */
        wr_err = tcp_write(tpcb, ptr->payload, ptr->len, 1);

        if(wr_err == ERR_OK)
        {
            /* continue with next pbuf in chain (if any) */
            es->p_tx = ptr->next;

            if(es->p_tx != NULL)
            {
                /* increment reference count for es->p */
                pbuf_ref(es->p_tx);
            }

            /* free pbuf: will free pbufs up to es->p (because es->p has a reference count > 0) */
            pbuf_free(ptr);
        }
        else if(wr_err == ERR_MEM)
        {
            /* we are low on memory, try later, defer to poll */
            es->p_tx = ptr;
        }
        else
        {
            /* other problem ?? */
        }
    }
}
/**
* @brief  This function implements the tcp_poll callback function
* @param  arg: pointer on argument passed to callback
* @param  tpcb: tcp connection control block
* @retval err_t: error code
*/
static err_t http_echoclient_poll(void *arg, struct tcp_pcb *tpcb)
{
    err_t ret_err;
    struct http_echoclient *es;
    es = (struct http_echoclient *)arg;

    if(es != NULL)
    {
        if(es->p_tx != NULL)
        {
            /* there is a remaining pbuf (chain) , try to send data */
            http_echoclient_send(tpcb, es);
        }
        else
        {
            /* no remaining pbuf (chain)  */
            if(es->state == ES_CLOSING)
            {
                /* close http connection */
                http_echoclient_connection_close(tpcb, es);
            }
        }

        ret_err = ERR_OK;
    }
    else
    {
        /* nothing to be done */
        tcp_abort(tpcb);
        ret_err = ERR_ABRT;
    }

    return ret_err;
}
/**
* @brief  This function implements the http_sent LwIP callback (called when ACK
*         is received from remote host for sent data)
* @param  arg: pointer on argument passed to callback
* @param  tcp_pcb: tcp connection control block
* @param  len: length of data sent
* @retval err_t: returned error code
*/
static err_t http_echoclient_sent(void *arg, struct tcp_pcb *tpcb, u16 len)
{
    struct http_echoclient *es;
    LWIP_UNUSED_ARG(len);
    es = (struct http_echoclient *)arg;

    if(es->p_tx != NULL)
    {
        /* still got pbufs to send */
        http_echoclient_send(tpcb, es);
    }

    return ERR_OK;
}
/**
* @brief This function is used to close the http connection with server
* @param tpcb: tcp connection control block
* @param es: pointer on echoclient structure
* @retval None
*/
static void http_echoclient_connection_close(struct tcp_pcb *tpcb, struct http_echoclient *es)
{
    /* close tcp connection */
    es->state = ES_CLOSING;
    /* remove callbacks */
    tcp_recv(tpcb, NULL);
    tcp_sent(tpcb, NULL);
    tcp_poll(tpcb, NULL, 0);

    if(es != NULL)
    {
        myfree(es);
    }

    /* close tcp connection */
    tcp_close(tpcb);
    http_free = 0;
}
#endif /* LWIP_http */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
