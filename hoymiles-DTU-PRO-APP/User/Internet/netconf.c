/**
  ******************************************************************************
  * @file    netconf.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013
  * @brief   Network connection configuration
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "lwip/mem.h"
#include "lwip/memp.h"
#include "lwip/tcp.h"
#ifdef LWIP1_4_1
#include "lwip/tcp_impl.h"
#else
#include "lwip/priv/tcp_priv.h"
#endif
#include "lwip/udp.h"
#include "netif/etharp.h"
#include "lwip/dhcp.h"
#include "raw.h"
#include "ethernetif.h"
#include "main.h"
#include "netconf.h"
#include "Ethernet.h"
#include "stdio.h"
#include "SysTick.h"
#include "tcp_echoclient.h"
#include "lwip/dns.h"
#include "gprs.h"
#include "string.h"
#include "NetProtocol.h"
#include "tcp_echoserver.h"
#include "http_client.h"
#include "Memory.h"
#include "TimeProcessing.h"
#include "HW_Version.h"
#include "crc16.h"
#include "ST_Flash.h"
#include "Memory_Tools.h"
/* Private typedef -----------------------------------------------------------*/
#define MAX_DHCP_TRIES              6
#define HTTP_WAIT_TIME              1000//60*1000
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

vu8 MAC_ADDR[6] = {0x50, 0x7B, 0x9D, 0xEE, 0xDD, 0xFA};
vu8 ServerAddr[4] = {122, 112, 156, 19};
vu16 ServerPort = 10081;
vu8  DHCP_Get = 0;
vu32 TCPTimer = 0;
vu32 ARPTimer = 0;
vu32 LinkTimer = 0;
vu32 IPaddress = 0;
vu32 DHCPfineTimer = 0;
vu32 DHCPcoarseTimer = 0;
vu32 DNScoarseTimer = 0;
vu8 DHCP_state;
vu8 ETH_State;
vu16 GetReceivingSign;
vu8 ServerAddr_Get[4];
volatile lwip_addr tcp_addr;
vu32 DHCP_Time = 0;
vu32 ETH_Time = 0;
vu32 ETH_Times = 0;
vu8 ETH_IPaddr[4] = {0};
vu32 dns_time = 0;
vu8 TcpLink = 0;
vu8 TcpState = TCP_Server_Start;
vu8 ping_link = 0;
vu8 TcpServer = 0;
vu8 HTTP_Wait_Down = 0;
vu32 ping_time = 0;
vu32 wait_tcp_time = 0;
vu32 ETH_HTTP_Wait_Time = 0;
struct raw_pcb *ping_pcb;

static long int ping_counter = 0;
extern vu8 NetProVer;
extern vu32  EthStatus;
extern vu8 mipadr[4];
//网络标志位  1未连接网络  2 连接网络
extern volatile vu8 connect_net;
//连接服务器标志位 1未连接服务器 2 连接服务器
extern volatile vu8 connect_server;
//有服务器数据
extern volatile vu8 server_data;
//下载ip
extern vu8 up_pro_destip[4];
//端口号
extern vu16 up_pro_destport;
extern vu8 up_pro_filename[ROW_MAX_LEN];
extern vu8 up_pro_url[ROW_MAX_LEN];
extern vu8 lwip_ready;
extern vu8 server_socket_send_buff[SOCKET_LEN];
extern vu16 socket_data_len;
//打包暂存buff
extern vu8 pack_buff[5][SOCKET_LEN];
//每包的长度
extern vu16 pack_len[PACKAGE_ALL];
//S-C的当前包数
extern vu8 package_to_APP;
//S-C的总包数
extern vu8 total_package_to_APP;
//C-S的当前包数
extern vu8 package_to_server;
//C-S的总包数
extern vu8 total_package_to_server;
//当前请求帧数
extern vu8 AppPackageNow;
//socket状态
extern u32 socket_state;
extern vu8 get_dns_ok;
struct netif gnetif;
extern vu8 PackageNow;
extern volatile DtuMajor Dtu3Major;
extern vu8 Http_Status;
extern vu8  Registration_frame;
extern vu8 APP_Flg;
extern vu8 user_http_file_times;
extern vu8 downfile;
extern vu8 http_state;
extern vu32 http_time;
/* Private functions ---------------------------------------------------------*/
void LwIP_DHCP_Process_Handle(void);


/**
* @brief  Initializes the lwIP stack
* @param  None
* @retval None
*/
void LwIP_Init(void)
{
    volatile lwip_addr ipaddr;
    volatile lwip_addr netmask;
    volatile lwip_addr gw;
    /* Initializes the dynamic memory heap defined by MEM_SIZE.*/
    mem_init();
    /* Initializes the memory pools defined by MEMP_NUM_x.*/
    memp_init();

    if(Dtu3Detail.Property.DHCP_Switch == 1)
    {
        ipaddr.addr = 0;
        netmask.addr = 0;
        gw.addr = 0;
    }
    else
    {
        IP4_ADDR(&ipaddr, Dtu3Detail.Property.IP_ADDR[0], Dtu3Detail.Property.IP_ADDR[1], Dtu3Detail.Property.IP_ADDR[2], Dtu3Detail.Property.IP_ADDR[3]);
        IP4_ADDR(&netmask, Dtu3Detail.Property.subnet_mask[0], Dtu3Detail.Property.subnet_mask[1], Dtu3Detail.Property.subnet_mask[2], Dtu3Detail.Property.subnet_mask[3]);
        IP4_ADDR(&gw, Dtu3Detail.Property.default_gateway[0], Dtu3Detail.Property.default_gateway[1], Dtu3Detail.Property.default_gateway[2], Dtu3Detail.Property.default_gateway[3]);
        ETH_IPaddr[0] = Dtu3Detail.Property.IP_ADDR[0];
        ETH_IPaddr[1] = Dtu3Detail.Property.IP_ADDR[1];
        ETH_IPaddr[2] = Dtu3Detail.Property.IP_ADDR[2];
        ETH_IPaddr[3] = Dtu3Detail.Property.IP_ADDR[3];
        DHCP_Get = 1;
    }

    /* - netif_add(struct netif *netif, lwip_addr *ipaddr,
    lwip_addr *netmask, lwip_addr *gw,
    void *state, err_t (* init)(struct netif *netif),
    err_t (* input)(struct pbuf *p, struct netif *netif))

    Adds your network interface to the netif_list. Allocate a struct
    netif and pass a pointer to this structure as the first argument.
    Give pointers to cleared ip_addr structures when using DHCP,
    or fill them with sane numbers otherwise. The state pointer may be NULL.

    The init function pointer must point to a initialization function for
    your ethernet netif interface. The following code illustrates it's use.*/
    netif_add(&gnetif, (lwip_addr *)&ipaddr, (lwip_addr *)&netmask, (lwip_addr *)&gw, NULL, &ethernetif_init, &ethernet_input);
    /*  Registers the default network interface.*/
    netif_set_default(&gnetif);

    if(EthStatus == (ETH_INIT_FLAG | ETH_LINK_FLAG))
    {
        /* Set Ethernet link flag */
        gnetif.flags |= NETIF_FLAG_LINK_UP;
        /* When the netif is fully configured this function must be called.*/
        netif_set_up(&gnetif);

        if(Dtu3Detail.Property.DHCP_Switch == 1)
        {
            DHCP_state = DHCP_START;
            DHCP_Time = LocalTime;
        }
        else
        {
#ifdef DEBUG
            printf("\n  Static IP address   \n");
            printf("IP: %d.%d.%d.%d\n", Dtu3Detail.Property.IP_ADDR[0], Dtu3Detail.Property.IP_ADDR[1], Dtu3Detail.Property.IP_ADDR[2], Dtu3Detail.Property.IP_ADDR[3]);
            printf("NETMASK: %d.%d.%d.%d\n", Dtu3Detail.Property.subnet_mask[0], Dtu3Detail.Property.subnet_mask[1], Dtu3Detail.Property.subnet_mask[2], Dtu3Detail.Property.subnet_mask[3]);
            printf("Gateway: %d.%d.%d.%d\n", Dtu3Detail.Property.default_gateway[0], Dtu3Detail.Property.default_gateway[1], Dtu3Detail.Property.default_gateway[2], Dtu3Detail.Property.default_gateway[3]);
#endif /* DEBUG */
        }
    }
    else
    {
        /*  When the netif link is down this function must be called.*/
        netif_set_down(&gnetif);

        if(Dtu3Detail.Property.DHCP_Switch == 1)
        {
            DHCP_state = DHCP_LINK_DOWN;
        }

#ifdef DEBUG
        printf("\n  Network Cable is  \n");
        printf("    not connected   \n");
#endif /* DEBUG */
    }

    /* Set the link callback function, this function is called on change of link status*/
    netif_set_link_callback(&gnetif, ETH_link_callback);
}

/**
* @brief  Called when a frame is received
* @param  None
* @retval None
*/
void LwIP_Pkt_Handle(void)
{
    /* Read a received packet from the Ethernet buffers and send it to the lwIP for handling */
    ethernetif_input(&gnetif);
}
extern const ip_addr_t ip_addr_any;
/**
* @brief  LwIP periodic tasks
* @param  localtime the current LocalTime value
* @retval None
*/
u32 LwIP_Periodic_Handle(vu32 localtime)
{
#if LWIP_TCP

    /* TCP periodic process every 250 ms */
    if(localtime - TCPTimer >= TCP_TMR_INTERVAL)
    {
        TCPTimer =  localtime;
        tcp_tmr();
    }

#endif

    /* ARP periodic process every 5s */
    if((localtime - ARPTimer) >= ARP_TMR_INTERVAL)
    {
        ARPTimer =  localtime;
        etharp_tmr();
    }

    /* Check link status periodically */
    if((localtime - LinkTimer) >= LINK_TIMER_INTERVAL)
    {
        ETH_CheckLinkStatus();
    }

#if LWIP_DNS

    if(localtime - DNScoarseTimer >= DNS_TMR_INTERVAL)
    {
        DNScoarseTimer = localtime;
        dns_tmr();
    }

#endif

    if(Dtu3Detail.Property.DHCP_Switch == 1)
    {
        /* Fine DHCP periodic process every 500ms */
        if(localtime - DHCPfineTimer >= DHCP_FINE_TIMER_MSECS)
        {
            DHCPfineTimer =  localtime;
            dhcp_fine_tmr();

            if((DHCP_state != DHCP_ADDRESS_ASSIGNED) &&
                    (DHCP_state != DHCP_TIMEOUT) &&
                    (DHCP_state != DHCP_LINK_DOWN))
            {
                /* process DHCP state machine */
                LwIP_DHCP_Process_Handle();
            }
        }

        /* DHCP Coarse periodic process every 60s */
        if(localtime - DHCPcoarseTimer >= DHCP_COARSE_TIMER_MSECS)
        {
            DHCPcoarseTimer =  localtime;
            dhcp_coarse_tmr();
        }
    }

    return IPaddress;
}

/**
* @brief  LwIP_DHCP_Process_Handle
* @param  None
* @retval None
*/
void LwIP_DHCP_Process_Handle()
{
    volatile lwip_addr ipaddr;
    volatile lwip_addr netmask;
    volatile lwip_addr gw;
#ifndef LWIP1_4_1
    struct dhcp *dhcp;
#endif

    switch(DHCP_state)
    {
        case DHCP_START:
            {
                DHCP_state = DHCP_WAIT_ADDRESS;
                dhcp_start(&gnetif);
                dhcp_network_changed(&gnetif);
                /* IP address should be set to 0
                   every time we want to assign a new DHCP address */
                IPaddress = 0;
                ETH_IPaddr[0] = (u8)(IPaddress);
                ETH_IPaddr[1] = (u8)(IPaddress >> 8);
                ETH_IPaddr[2] = (u8)(IPaddress >> 16);
                ETH_IPaddr[3] = (u8)(IPaddress >> 24);
#ifdef DEBUG
                printf("\n     Looking for    \n");
                printf("     DHCP server    \n");
                printf("     please wait... \n");
#endif /* DEBUG */
                DHCP_Get = 0;
            }
            break;

        case DHCP_WAIT_ADDRESS:
            {
                /* Read the new IP address */
                IPaddress = gnetif.ip_addr.addr;

                if(IPaddress != 0)
                {
                    DHCP_state = DHCP_ADDRESS_ASSIGNED;
                    /* Stop DHCP */
                    ETH_IPaddr[0] = (u8)(IPaddress);
                    ETH_IPaddr[1] = (u8)(IPaddress >> 8);
                    ETH_IPaddr[2] = (u8)(IPaddress >> 16);
                    ETH_IPaddr[3] = (u8)(IPaddress >> 24);
#ifdef DEBUG
                    printf("\n  IP address assigned \n");
                    printf("    by a DHCP server   \n");
                    printf("IP: %d.%d.%d.%d\n", (u8)(IPaddress), (u8)(IPaddress >> 8), \
                           (u8)(IPaddress >> 16), (u8)(IPaddress >> 24));
                    printf("NETMASK: %d.%d.%d.%d\n", (u8)(gnetif.netmask.addr), (u8)(gnetif.netmask.addr >> 8), \
                           (u8)(gnetif.netmask.addr >> 16), (u8)(gnetif.netmask.addr >> 24));
                    printf("Gateway: %d.%d.%d.%d\n", (u8)(gnetif.gw.addr), (u8)(gnetif.gw.addr >> 8), \
                           (u8)(gnetif.gw.addr >> 16), (u8)(gnetif.gw.addr >> 24));
#endif /* DEBUG */
                    dhcp_stop(&gnetif);
                    DHCP_Get = 1;
                }
                else
                {
                    /* DHCP timeout */
#ifdef LWIP1_4_1
                    if(gnetif.dhcp->tries > MAX_DHCP_TRIES)
#else
                    dhcp = netif_dhcp_data(&gnetif);

                    if(dhcp->tries > MAX_DHCP_TRIES)
#endif
                    {
                        DHCP_state = DHCP_TIMEOUT;
                        /* Stop DHCP */
                        dhcp_stop(&gnetif);

                        if((
                                    (Dtu3Detail.Property.IP_ADDR[0] == 0) &&
                                    (Dtu3Detail.Property.IP_ADDR[1] == 0) &&
                                    (Dtu3Detail.Property.IP_ADDR[2] == 0) &&
                                    (Dtu3Detail.Property.IP_ADDR[3] == 0)) || (
                                    (Dtu3Detail.Property.subnet_mask[0] == 0) &&
                                    (Dtu3Detail.Property.subnet_mask[1] == 0) &&
                                    (Dtu3Detail.Property.subnet_mask[2] == 0) &&
                                    (Dtu3Detail.Property.subnet_mask[3] == 0)) || (
                                    (Dtu3Detail.Property.default_gateway[0] == 0) &&
                                    (Dtu3Detail.Property.default_gateway[1] == 0) &&
                                    (Dtu3Detail.Property.default_gateway[2] == 0) &&
                                    (Dtu3Detail.Property.default_gateway[3] == 0)))
                        {
                            ETH_IPaddr[0] = 192;
                            ETH_IPaddr[1] = 168;
                            ETH_IPaddr[2] = 1;
                            ETH_IPaddr[3] = Dtu3Major.Property.Id[3];
                            /* Static address used */
                            IP4_ADDR(&ipaddr, ETH_IPaddr[0], ETH_IPaddr[1], ETH_IPaddr[2], ETH_IPaddr[3]);
                            IP4_ADDR(&netmask, 255, 255, 255, 255);
                            IP4_ADDR(&gw, 192, 168, 1, 1);
                            netif_set_addr(&gnetif, (lwip_addr *)&ipaddr, (lwip_addr *)&netmask, (lwip_addr *)&gw);
                        }
                        else
                        {
                            ETH_IPaddr[0] = Dtu3Detail.Property.IP_ADDR[0];
                            ETH_IPaddr[1] = Dtu3Detail.Property.IP_ADDR[1];
                            ETH_IPaddr[2] = Dtu3Detail.Property.IP_ADDR[2];
                            ETH_IPaddr[3] = Dtu3Detail.Property.IP_ADDR[3];
                            /* Static address used */
                            IP4_ADDR(&ipaddr, Dtu3Detail.Property.IP_ADDR[0], Dtu3Detail.Property.IP_ADDR[1], Dtu3Detail.Property.IP_ADDR[2], Dtu3Detail.Property.IP_ADDR[3]);
                            IP4_ADDR(&netmask, Dtu3Detail.Property.subnet_mask[0], Dtu3Detail.Property.subnet_mask[1], Dtu3Detail.Property.subnet_mask[2], Dtu3Detail.Property.subnet_mask[3]);
                            IP4_ADDR(&gw, Dtu3Detail.Property.default_gateway[0], Dtu3Detail.Property.default_gateway[1], Dtu3Detail.Property.default_gateway[2], Dtu3Detail.Property.default_gateway[3]);
                            netif_set_addr(&gnetif, (lwip_addr *)&ipaddr, (lwip_addr *)&netmask, (lwip_addr *)&gw);
#ifdef DEBUG
                            printf("\n    DHCP timeout    \n");
                            printf("  Static IP address   \n");
                            printf("IP: %d.%d.%d.%d\n", Dtu3Detail.Property.IP_ADDR[0], Dtu3Detail.Property.IP_ADDR[1], Dtu3Detail.Property.IP_ADDR[2], Dtu3Detail.Property.IP_ADDR[3]);
                            printf("NETMASK: %d.%d.%d.%d\n", Dtu3Detail.Property.subnet_mask[0], Dtu3Detail.Property.subnet_mask[1], Dtu3Detail.Property.subnet_mask[2], Dtu3Detail.Property.subnet_mask[3]);
                            printf("Gateway: %d.%d.%d.%d\n", Dtu3Detail.Property.default_gateway[0], Dtu3Detail.Property.default_gateway[1], Dtu3Detail.Property.default_gateway[2], Dtu3Detail.Property.default_gateway[3]);
#endif /* DEBUG */
                        }

                        DHCP_Get = 1;
                    }
                }
            }
            break;

        default:
            break;
    }
}



void ping_send(struct raw_pcb *pcb, ip_addr_t *ipaddr);
#ifdef LWIP1_4_1
u8_t raw_callback(void *arg, struct raw_pcb *pcb, struct pbuf *p, ip_addr_t *addr)
#else
u8_t raw_callback(void *arg, struct raw_pcb *pcb, struct pbuf *p, const ip_addr_t *addr)
#endif
{
    struct ip_hdr  *iphdr;

    if(p->tot_len >= (PBUF_IP_HLEN + 8))
    {
        iphdr = (struct ip_hdr *)((u8_t *)p->payload);
        ping_counter++;

        if((ServerAddr_Get[0] == *(char *)addr) && (ServerAddr_Get[1] == *((char *)addr + 1)) && (ServerAddr_Get[2] == *((char *)addr + 2)) && (ServerAddr_Get[3] == *((char *)addr + 3)))
            //if((ServerAddr_Get[0] == *(char *)addr->addr) && (ServerAddr_Get[1] == *((char *)addr->addr + 1)) && (ServerAddr_Get[2] == *((char *)addr->addr + 2)) && (ServerAddr_Get[3] == *((char *)addr->addr + 3)))
        {
            ping_link = 1;
        }
    }

    return 0;
}

void ping_prepare_echo(struct icmp_echo_hdr *iecho, u16 ping_size)
{
    volatile size_t i;
    size_t data_len = ping_size - sizeof(struct icmp_echo_hdr);
    ICMPH_TYPE_SET(iecho, ICMP_ECHO);
    ICMPH_CODE_SET(iecho, 0);
    iecho->chksum = 0;
    iecho->id = 0x01;
    iecho->seqno = 0x8418;

    for(i = 0; i < data_len; i++)
    {
        ((char *)iecho)[sizeof(struct icmp_echo_hdr) + i] = 1;
    }
}

void ping_send(struct raw_pcb *pcb, ip_addr_t *ipaddr)
{
    struct pbuf *p;
    struct icmp_echo_hdr *iecho;
    //申请pbuf结构
    p = pbuf_alloc(PBUF_IP, 32, PBUF_RAM);

    if(!p)
    {
        return;
    }

    if(p->len == p->tot_len && p->next == NULL)
    {
        iecho = (struct icmp_echo_hdr *)p->payload;
        //填写ICMP首部各字段
        ping_prepare_echo(iecho, 32);
        //底层发送
        raw_sendto(pcb, p, ipaddr);
    }

    pbuf_free(p);
}


u8 DomainCheck(u8 *Domain, u32 size)
{
    vu8 i = 0;
    vu8 j = 0;
    vu8 k = 0;
    vu8 getip = 0;
    vu8 ip[4] = {0};
    char *get = NULL;
    vu8 IPorURL = 0;
    get = mymalloc(20 * sizeof(char));

    if(get == NULL)
    {
        return 0;
    }

    memset(get, 0, 20 * sizeof(char));
    memset((u8 *)ip, 0, sizeof(ip));

    for(i = 0, j = i; i < size; i++)
    {
        if(Domain[i] == '.')
        {
            if(IPorURL == 0)
            {
                sscanf(get, "%d", (int *)&ip[k]);
                memset(get, 0, 20 * sizeof(char));
                k++;
                j = i + 1;
                getip = 0;
            }
        }
        else if(Domain[i] >= '0' && Domain[i] <= '9')
        {
            IPorURL = 0;
            get[i - j] = Domain[i];
            getip = 1;
        }
        else
        {
            IPorURL = 1;
        }
    }

    if(getip == 1)
    {
        sscanf(get, "%d", (int *)&ip[k]);
        memset(get, 0, 20 * sizeof(char));
        k++;
        j = i + 1;
        ServerAddr_Get[0] = ip[0];
        ServerAddr_Get[1] = ip[1];
        ServerAddr_Get[2] = ip[2];
        ServerAddr_Get[3] = ip[3];
#ifdef DEBUG
        printf("%d.%d.%d.%d\n", ServerAddr_Get[0], ServerAddr_Get[1], ServerAddr_Get[2], ServerAddr_Get[3]);
#endif
        IP4_ADDR(&tcp_addr, ServerAddr_Get[0], ServerAddr_Get[1], ServerAddr_Get[2], ServerAddr_Get[3]);
        IPorURL = 0;
    }

    myfree(get);
    return IPorURL;
}

void ETHReconnect(void)
{
    TcpLink = 0;
    TcpState = TCP_Domain_Check;

    if(Netmode_Used  == CABLE_MODE)
    {
        //网络标志位  1未连接网络  2 连接网络
        connect_net = 0;
        //连接服务器标志位 1未连接服务器 2 连接服务器
        connect_server = 0;
        //有服务器数据
        server_data = 0;
    }
}

void ETHProtocol(void)
{
    volatile lwip_addr DNS_ip_addr_get;
    vu32 ip_addr_any_get;
    LwIP_Cofig();

    if(lwip_ready == 1)
    {
        /* check if any packet received */
        if(ETH_CheckFrameReceived())
        {
            /* process received ethernet packet */
            LwIP_Pkt_Handle();
        }

        /* handle periodic timers for LwIP */
        ip_addr_any_get = LwIP_Periodic_Handle(LocalTime);

        if(GET_PHY_LINK_STATUS() == 1)
        {
            switch(TcpState)
            {
                case TCP_Server_Start:
                    TcpServer = tcp_echoserver_init(12345);
                    //新建raw_pcb控制块
                    ping_pcb = raw_new(IP_PROTO_ICMP);
                    //注册回调函数
                    raw_recv(ping_pcb, raw_callback, NULL);
                    //绑定本地IP地址  这里ANYIP
                    raw_bind(ping_pcb, IP_ADDR_ANY);
                    TcpState = TCP_Domain_Check;

                    if(Netmode_Used  == CABLE_MODE)
                    {
                        //网络标志位  1未连接网络  2 连接网络
                        connect_net = 0;
                        //连接服务器标志位 1未连接服务器 2 连接服务器
                        connect_server = 0;
                        //有服务器数据
                        server_data = 0;
                    }

                    break;

                case TCP_Domain_Check:
                    if(DomainCheck((u8 *)Dtu3Major.Property.ServerDomainName, sizeof(Dtu3Major.Property.ServerDomainName)) == 1)
                    {
                        DHCP_Get = 0;
                        get_dns_ok = 0;
                        DHCP_state = DHCP_START;
                        TcpState = TCP_DNS_Analy;
                    }
                    else
                    {
                        TcpState = TCP_Ping_Server;
                    }

                    break;

                case TCP_DNS_Analy:
                    if(Dtu3Detail.Property.DHCP_Switch == 1)
                    {
                        if(DHCP_Get == 1)
                        {
                            dns_gethostbyname((char *)Dtu3Major.Property.ServerDomainName, (lwip_addr *)&DNS_ip_addr_get, my_found, NULL);
                            dns_time = LocalTime;
                            TcpState = TCP_DNS_Wait;
                        }
                        else if((LocalTime - DHCP_Time) >= 3 * 60 * 1000)
                        {
                            DHCP_state = DHCP_START;
                            DHCP_Time = LocalTime;
                        }
                    }
                    else
                    {
                        dns_gethostbyname((char *)Dtu3Major.Property.ServerDomainName, (lwip_addr *)&DNS_ip_addr_get, my_found, NULL);
                        dns_time = LocalTime;
                        TcpState = TCP_DNS_Wait;
                    }

                    break;

                case TCP_DNS_Wait:
                    if(get_dns_ok == 1)
                    {
                        ServerAddr_Get[0] = mipadr[0];
                        ServerAddr_Get[1] = mipadr[1];
                        ServerAddr_Get[2] = mipadr[2];
                        ServerAddr_Get[3] = mipadr[3];
#ifdef DEBUG
                        printf("%d.%d.%d.%d\n", ServerAddr_Get[0], ServerAddr_Get[1], ServerAddr_Get[2], ServerAddr_Get[3]);
#endif
                        IP4_ADDR(&tcp_addr, ServerAddr_Get[0], ServerAddr_Get[1], ServerAddr_Get[2], ServerAddr_Get[3]);
                        TcpState = TCP_Ping_Server;
                    }
                    else if((LocalTime - dns_time) >= 60 * 1000)
                    {
                        get_dns_ok = 0;
                        TcpState = TCP_Domain_Check;
                    }

                    break;

                case TCP_Ping_Server:
                    //发送Ping请求
                    ping_send(ping_pcb, (lwip_addr *)&tcp_addr);
                    ping_time = LocalTime;
                    TcpLink = 0;
                    ping_link = 0;
                    TcpState = TCP_Link_Server;
                    break;

                case TCP_Link_Server:
                    if(ping_link == 1)
                    {
                        if((Netmode_Used  == CABLE_MODE))
                        {
                            tcp_echoclient_connect(tcp_addr, Dtu3Major.Property.ServerPort);
                            wait_tcp_time = LocalTime;
                            TcpState = TCP_Link_Wait;

                            if(downfile == 1)
                            {
                                downfile = 0;
                            }

                            if(Netmode_Used  == CABLE_MODE)
                            {
                                //网络标志位  1未连接网络  2 连接网络
                                connect_net = 1;
                                //连接服务器标志位 1未连接服务器 2 连接服务器
                                connect_server = 0;
                                //有服务器数据
                                server_data = 0;
                            }
                        }
                    }
                    else
                    {
                        if(LocalTime - ping_time >= 10000)
                        {
                            TcpState = TCP_Domain_Check;
                        }
                    }

                    break;

                case TCP_Link_Wait:
                    if(LocalTime - wait_tcp_time >= 5000)
                    {
                        socket_state = 0;
                        //每次切换AT命令的时候都先发注册帧
                        socket_state |= REGISTRATION_PACK;
                        Registration_frame = 0;
                        ETH_State = ETH_WAIT;
                        TcpLink = 1;
                        http_clear_flag();
                        TcpState = TCP_Offline;
                    }

                    break;

                case TCP_Offline:
                    if((TcpLink == 0) || (DHCP_Get == 0))
                    {
                        TcpState = TCP_Domain_Check;

                        if(Netmode_Used  == CABLE_MODE)
                        {
                            //网络标志位  1未连接网络  2 连接网络
                            connect_net = 0;
                            //连接服务器标志位 1未连接服务器 2 连接服务器
                            connect_server = 0;
                            server_data = 0;
                        }
                    }
                    else if((http_state >= HTTP_IP_LINK) && (downfile == 1))
                    {
                        TcpLink = 0;
                        TcpState = TCP_Down_Wait;
                    }

                    break;

                case TCP_Down_Wait:
                    if(downfile != 1)
                    {
                        TcpState = TCP_Domain_Check;
                    }

                    break;
            }

            if((APP_Flg != 1) && (Netmode_Used  == CABLE_MODE))
            {
                http_execution();

                if(TcpLink == 1)
                {
                    tcp_server_detection();

                    if(((LocalTime - ETH_Time) >= ETH_Wite_Time) || (ETH_State == ETH_WAIT))
                    {
                        if(ETH_Times >= 5)
                        {
                            TcpLink = 0;
                            ETH_Times = 0;

                            if(Netmode_Used  == CABLE_MODE)
                            {
                                connect_net = 1;
                                connect_server = 1;
                                server_data = 0;
                            }

                            ETHReconnect();
                            ETH_State = ETH_WAIT;
                        }
                        else
                        {
                            switch(ETH_State)
                            {
                                //空闲模式
                                case ETH_WAIT:
                                    ETH_Times = 0;

                                    if((HTTP_Wait_Down == 1) && ((LocalTime - ETH_HTTP_Wait_Time) > HTTP_WAIT_TIME))
                                    {
                                        ETH_State = ETH_HTTP_Wait_Download;
                                    }

                                    break;

                                //信息
                                case ETH_Send_Info:

                                //心跳
                                case ETH_Send_Heart:

                                //历史数据
                                case ETH_Send_History:

                                //DTU告警上报
                                case ETH_DTU_ALERTING :

                                //MI告警上报
                                case ETH_MI_ALERTING :
                                case ETH_MI_SPSPEND_ALERTING:
                                case ETH_Send_History_ALERTING:
                                    ETH_SendData((char *)server_socket_send_buff, socket_data_len);
                                    GetReceivingSign = 0;
                                    ETH_Time = LocalTime;
                                    ETH_Times++;
                                    break;

                                case ETH_Status:
                                    if(NetProVer == 0)
                                    {
                                        package_net_command_status();
                                    }

                                case ETH_RE_Server:

                                //获取配置信息
                                case ETH_GET_CONFIG:

                                //设置配置信息://回复服务器_
                                case ETH_SET_CONFIG :
                                    ETH_SendData((volatile char *)server_socket_send_buff, socket_data_len);
                                    memset((u8 *)server_socket_send_buff, 0, socket_data_len);
                                    ETH_State = ETH_WAIT;
                                    ETH_Time = LocalTime;
                                    break;

                                // 数据
                                case ETH_Send_Data:
                                    memset((char *)server_socket_send_buff, 0, SOCKET_LEN);

                                    if(NetProVer == 1)
                                    {
                                        socket_data_len = pack_len[package_to_server];
                                        memcpy((char *)server_socket_send_buff, (char *)pack_buff[package_to_server], socket_data_len);
                                    }
                                    else
                                    {
                                        socket_data_len = pb_RealDataReq((char *)server_socket_send_buff, (char *)pack_buff[package_to_server], pack_len[package_to_server]);
                                    }

                                    ETH_SendData((char *)server_socket_send_buff, socket_data_len);
                                    GetReceivingSign = 0;
                                    ETH_Time = LocalTime;
                                    ETH_Times++;
                                    break;

                                case ETH_HTTP_Download:
                                    ETH_HTTP_Wait_Time = LocalTime;
                                    HTTP_Wait_Down = 1;
                                    ETH_State = ETH_WAIT;
                                    break;

                                //App获取配置信息请求回复
                                case ETH_App_GetConfigReq:

                                //App设置配置信息请求回复
                                case ETH_App_SetConfigReq:

                                //App设备信息请求回复
                                case ETH_App_InfoReq:

                                //App告警上报
                                case ETH_App_ALERTINGReq:

                                //App配置ID请求回复
                                case ETH_App_SetIDReq:

                                //App控制命令请求回复
                                case ETH_App_ConfigCommandReq:

                                //App控制命令状态回复
                                case ETH_App_StatusDataReq:

                                //App心跳状态回复
                                case ETH_App_HeartBeatReq:

                                //App配置获取请求
                                case ETH_App_DevConfigFetchReq:

                                //App配置设置请求
                                case ETH_App_DevConfigPutReq:
                                case ETH_App_WInfoReq:
                                    ETH_ServerSendData((char *)server_socket_send_buff, socket_data_len);
                                    memset((u8 *)server_socket_send_buff, 0, socket_data_len);
                                    ETH_State = ETH_WAIT;
                                    ETH_Time = LocalTime;
                                    break;

                                //App实时数据请求回复
                                case ETH_App_RealDataReq:
                                    memset((char *)server_socket_send_buff, 0, SOCKET_LEN);
                                    socket_data_len = 0;
                                    memcpy((char *)server_socket_send_buff, (u8 *)pack_buff[AppPackageNow], sizeof(pack_buff[AppPackageNow]));
                                    socket_data_len = pack_len[AppPackageNow];
                                    ETH_ServerSendData((char *)server_socket_send_buff, socket_data_len);
                                    ETH_State = ETH_WAIT;
                                    ETH_Time = LocalTime;
                                    break;

                                case ETH_HTTP_Wait_Download:
                                    HTTP_Wait_Down = 0;
                                    Http_Status = 1;
                                    user_http_file_times = 0;
                                    ETH_State = ETH_WAIT;
                                    ETH_HTTP_Wait_Time = 0;
                                    ETH_Time = LocalTime;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        else if(Netmode_Used == CABLE_MODE)
        {
            connect_net = 0;
            connect_server = 0;
            server_data = 0;
        }
    }
}

void ETH_Get_Reply(void)
{
    if(Netmode_Used == CABLE_MODE)
    {
        connect_net = 1;
        connect_server = 1;
        server_data = 1;
        ETH_Times = 0;
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
