/**
  ******************************************************************************
  * @file    tcp_echoclient.h
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    31-July-2013
  * @brief   Header file for tcp_echoclient.c
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HTTP_ECHOCLIENT_H__
#define __HTTP_ECHOCLIENT_H__
#include "lwip/tcp.h"
#include "stm32f4xx.h"
#include "lwip.h"
/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
enum Http_Status
{
    HTTP_WAIT               = 0,
    HTTP_DNS_ANALY          = 1,
    HTTP_IP_LINK            = 2,
    HTTP_WAIT_MEMORY_SAVE   = 3,
    HTTP_PACKAGE_REQUEST    = 4,
    HTTP_WAIT_DOWN          = 5,
};

//void tcp_echoclient_connect(void);
uint8_t http_echoclient_connect(lwip_addr tcp_addr, u16_t tcp_port, char *tcp_link);
void http_echoclient_disconnect(void);
void http_printf(char *data);
void URLResolution(char *WebAddr, lwip_addr *ip_addr_get, int *port, char *ipfile); //��ַ����
void http_execution(void);
void http_clear_flag(void);
#ifdef LWIP1_4_1
void my_found(const char *name, lwip_addr *ipaddr, void *arg);
#else
void my_found(const char *name, const lwip_addr *ipaddr, void *arg);
#endif

#endif /* __TCP_ECHOCLIENT_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
