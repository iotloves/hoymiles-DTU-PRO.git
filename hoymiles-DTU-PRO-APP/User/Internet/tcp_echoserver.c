/**
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of and a contribution to the lwIP TCP/IP stack.
 *
 * Credits go to Adam Dunkels (and the current maintainers) of this software.
 *
 * Christiaan Simons rewrote this file to get a more stable echo example.
 *
 **/

/* This file was modified by ST */


#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/tcp.h"
#include "tcp_echoserver.h"
#include "netconf.h"
#include "stdlib.h"
#include "string.h"
#include "NetProtocol.h"
#include "malloc.h"
#include "SysTick.h"
#if LWIP_TCP
vu8 tcp_server_flag = 0;
vu32 tcp_client_time = 0;
volatile lwip_addr clientaddr[5] = {0};
static struct tcp_pcb *echoserver_pcb;
static struct tcp_pcb *ClintTpcb;
/* ECHO protocol states */
enum echoserver_states
{
    ES_NONE = 0,
    ES_ACCEPTED,
    ES_RECEIVED,
    ES_CLOSING
};

/* structure for maintaing connection infos to be passed as argument
   to LwIP callbacks*/
struct echoserver
{
    u8_t state;             /* current connection state */
    struct tcp_pcb *pcb;    /* pointer on the current tcp_pcb */
    struct pbuf *p_tx;         /* pointer on the received/to be transmitted pbuf */
};

struct echoserver *echoserver_es;

static err_t tcp_echoserver_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
static err_t tcp_echoserver_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void tcp_echoserver_error(void *arg, err_t err);
static err_t tcp_echoserver_poll(void *arg, struct tcp_pcb *tpcb);
static err_t tcp_echoserver_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static void tcp_echoserver_send(struct tcp_pcb *tpcb, struct echoserver *es);
static void tcp_echoserver_connection_close(struct tcp_pcb *tpcb, struct echoserver *es);

/***********************************************
** Function name:
** Descriptions:        TCP CLIENT 发送数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void ETH_ServerSendData(char *data, u16 len)
{
    struct echoserver *es = NULL;
    /* allocate structure es to maintain tcp connection informations */
    es = (struct echoserver *)mymalloc(sizeof(struct echoserver));
    //    if(es == NULL)
    //    {
    //        return;
    //    }
    {
        tcp_client_time = LocalTime;
#ifdef DEBUG
        u16 a;
        printf("Server Send %d :\r\n", len);

        for(a = 0; a < len; a++)
        {
            printf("%02x ", data[a]);
        }

        printf("\r\n");
#endif
    }

    if(es != NULL)
    {
        es->state = ES_ACCEPTED;
        es->pcb = ClintTpcb;
        /* allocate pbuf */
        es->p_tx = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_POOL);

        if(es->p_tx)
        {
            pbuf_take(es->p_tx, data, len);
            tcp_echoserver_send(ClintTpcb, es);
        }
    }

    pbuf_free(es->p_tx);
    myfree(es);
}
/**
  * @brief  Initializes the tcp echo server
  * @param  None
  * @retval None
  */
u8 tcp_echoserver_init(u16 tcp_port)
{
    /* create new tcp pcb */
    echoserver_pcb = tcp_new();

    if(echoserver_pcb != NULL)
    {
        err_t err;
        /* bind echo_pcb to port 7 (ECHO protocol) */
        err = tcp_bind(echoserver_pcb, IP_ADDR_ANY, tcp_port);

        if(err == ERR_OK)
        {
            /* start tcp listening for echo_pcb */
            echoserver_pcb = tcp_listen(echoserver_pcb);
            /* initialize LwIP tcp_accept callback function */
            tcp_accept(echoserver_pcb, tcp_echoserver_accept);
#ifdef DEBUG
            printf("create tcp server\n");
#endif
            return 1;
        }
        else
        {
            /* deallocate the pcb */
            memp_free(MEMP_TCP_PCB, echoserver_pcb);
#ifdef DEBUG
            printf("Can not bind pcb\n");
#endif
            return 0;
        }
    }
    else
    {
#ifdef DEBUG
        printf("Can not create new pcb\n");
#endif
        return 0;
    }
}

/**
  * @brief  This functions closes the tcp connection
  * @param  None
  * @retval None
  */
void tcp_echoserver_close(void)
{
    vu8 i = 0;
    vu8 j = 0;
    volatile lwip_addr addr;
    IP4_ADDR(&addr, (u32)ClintTpcb->remote_ip.addr & 0xff, \
             (u32)(ClintTpcb->remote_ip.addr >> 8) & 0xff, \
             (u32)(ClintTpcb->remote_ip.addr >> 16) & 0xff, \
             (u32)(ClintTpcb->remote_ip.addr >> 24) & 0xff);
    tcp_echoserver_connection_close(ClintTpcb, echoserver_es);

    for(i = 0; i < tcp_server_flag; i++)
    {
        if(clientaddr[i].addr == addr.addr)
        {
            clientaddr[i].addr = 0;
            break;
        }
    }

    for(j = 0; j < tcp_server_flag; j++)
    {
        for(i = tcp_server_flag; i > 0; i--)
        {
            if(clientaddr[i].addr != 0)
            {
                if(clientaddr[i - 1].addr == 0)
                {
                    clientaddr[i - 1].addr = clientaddr[i].addr;
                    clientaddr[i].addr = 0;
                    break;
                }
            }
        }
    }

    tcp_server_flag--;
#ifdef DEBUG
    printf("closes the tcp server\n");
#endif
}

/**
  * @brief  This function is the implementation of tcp_accept LwIP callback
  * @param  arg: not used
  * @param  newpcb: pointer on tcp_pcb struct for the newly created tcp connection
  * @param  err: not used
  * @retval err_t: error status
  */
static err_t tcp_echoserver_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
    vu8 i = 0;
    vu8 same_addr = 0;
    err_t ret_err;
    volatile lwip_addr addr;
    struct echoserver *es;
    LWIP_UNUSED_ARG(arg);
    LWIP_UNUSED_ARG(err);
    /* set priority for the newly accepted tcp connection newpcb */
    tcp_setprio(newpcb, TCP_PRIO_MIN);
    /* allocate structure es to maintain tcp connection informations */
    es = (struct echoserver *)mymalloc(sizeof(struct echoserver));
    //    if(es == NULL)
    //    {
    //        return 0;
    //    }
    echoserver_es = es;

    if(es != NULL)
    {
        es->state = ES_ACCEPTED;
        es->pcb = newpcb;
        es->p_tx = NULL;
        /* pass newly allocated es structure as argument to newpcb */
        tcp_arg(newpcb, es);
        /* initialize lwip tcp_recv callback function for newpcb  */
        tcp_recv(newpcb, tcp_echoserver_recv);
        /* initialize lwip tcp_err callback function for newpcb  */
        tcp_err(newpcb, tcp_echoserver_error);
        /* initialize lwip tcp_poll callback function for newpcb */
        tcp_poll(newpcb, tcp_echoserver_poll, 1);
        IP4_ADDR(&addr, (u32)newpcb->remote_ip.addr & 0xff, \
                 (u32)(newpcb->remote_ip.addr >> 8) & 0xff, \
                 (u32)(newpcb->remote_ip.addr >> 16) & 0xff, \
                 (u32)(newpcb->remote_ip.addr >> 24) & 0xff);

        if(tcp_server_flag == 0)
        {
            clientaddr[tcp_server_flag].addr = addr.addr;
            //标记有客户端连上了
            tcp_server_flag++;
        }
        else
        {
            for(i = 0; i < tcp_server_flag; i++)
            {
                if(clientaddr[i].addr == addr.addr)
                {
                    same_addr = 1;
                    break;
                }
            }

            if(same_addr == 0)
            {
                clientaddr[tcp_server_flag].addr = addr.addr;
                //标记有客户端连上了
                tcp_server_flag++;
            }
        }

        //clientaddr
        tcp_client_time = LocalTime;
        ret_err = ERR_OK;
    }
    else
    {
        /*  close tcp connection */
        tcp_echoserver_connection_close(newpcb, es);
        /* return memory error */
        ret_err = ERR_MEM;
    }

    return ret_err;
}


/**
  * @brief  This function is the implementation for tcp_recv LwIP callback
  * @param  arg: pointer on a argument for the tcp_pcb connection
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  pbuf: pointer on the received pbuf
  * @param  err: error information regarding the reveived pbuf
  * @retval err_t: error code
  */
static err_t tcp_echoserver_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
    char *recdata = NULL;
    struct echoserver *es;
    err_t ret_err;
    LWIP_ASSERT("arg != NULL", arg != NULL);
    es = (struct echoserver *)arg;

    /* if we receive an empty tcp frame from client => close connection */
    if(p == NULL)
    {
        /* remote host closed connection */
        es->state = ES_CLOSING;
#ifdef DEBUG
        printf("ES_CLOSING\n");
#endif

        if(es->p_tx == NULL)
        {
            /* we're done sending, close connection */
            tcp_echoserver_connection_close(tpcb, es);
        }
        else
        {
            /* we're not done yet */
            /* acknowledge received packet */
            tcp_sent(tpcb, tcp_echoserver_sent);
            es->p_tx = NULL;
            p->len = 0;
            pbuf_free(p);
            /* send remaining data*/
            tcp_echoserver_send(tpcb, es);
        }

        ret_err = ERR_OK;
    }
    /* else : a non empty frame was received from client but for some reason err != ERR_OK */
    else if(err != ERR_OK)
    {
        /* free received pbuf*/
        es->p_tx = NULL;
        pbuf_free(p);
        ret_err = err;
    }
    else if(es->state == ES_ACCEPTED)
    {
        /* first data chunk in p->payload */
        es->state = ES_ACCEPTED;
        /* store reference to incoming pbuf (chain) */
        es->p_tx = p;
        /* initialize LwIP tcp_sent callback function */
        tcp_sent(tpcb, tcp_echoserver_sent);
        recdata = mymalloc(p->len);
        //        if(recdata == NULL)
        //        {
        //            return 0;
        //        }
        memset(recdata, 0, p->len);

        if(recdata != NULL)
        {
            memcpy(recdata, p->payload, p->len);
#ifdef DEBUG
            printf("upd_rec:%d", p->len);
#endif
            tcp_client_time = LocalTime;
            pb_receive(recdata);
#ifdef DEBUG
            {
                vu16 a;
                printf("Server Rec:\r\n");

                for(a = 0; a < p->len; a++)
                {
                    printf("%02x ", recdata[a]);
                }

                printf("\r\n");
            }
#endif
        }

        tcp_recved(tpcb, p->len);
        myfree(recdata);
        ClintTpcb = tpcb;
        es->p_tx = NULL;
        p->len = 0;
        pbuf_free(p);
        /* send back the received data (echo) */
        tcp_echoserver_send(tpcb, es);
        ret_err = ERR_OK;
    }
    else if(es->state == ES_RECEIVED)
    {
#ifdef DEBUG
        printf("ES_RECEIVED\n");
#endif

        /* more data received from client and previous data has been already sent*/
        if(es->p_tx == NULL)
        {
            es->p_tx = p;
            recdata = mymalloc(p->len * sizeof(char));
            //            if(recdata == NULL)
            //            {
            //                return 0;
            //            }
            memset(recdata, 0, p->len * sizeof(char));

            if(recdata != NULL)
            {
                memcpy(recdata, p->payload, p->len);
                //printf("upd_rec:%s", recdata);
                pb_receive(recdata);
#ifdef DEBUG
                {
                    vu16 a;
                    printf("ES_ACCEPTED Server Rec:\r\n");

                    for(a = 0; a < p->len; a++)
                    {
                        printf("%02x ", recdata[a]);
                    }

                    printf("\r\n");
                }
#endif
            }

            myfree(recdata);
            ClintTpcb = tpcb;
            es->p_tx = NULL;
            p->len = 0;
            pbuf_free(p);
            /* send back received data */
            tcp_echoserver_send(tpcb, es);
        }
        else
        {
            struct pbuf *ptr;
            /* chain pbufs to the end of what we recv'ed previously  */
            ptr = es->p_tx;
            pbuf_chain(ptr, p);
        }

        ret_err = ERR_OK;
    }
    /* data received when connection already closed */
    else
    {
        /* Acknowledge data reception */
        tcp_recved(tpcb, p->tot_len);
        /* free pbuf and do nothing */
        es->p_tx = NULL;
        pbuf_free(p);
        ret_err = ERR_OK;
    }

    return ret_err;
}

/**
  * @brief  This function implements the tcp_err callback function (called
  *         when a fatal tcp_connection error occurs.
  * @param  arg: pointer on argument parameter
  * @param  err: not used
  * @retval None
  */
static void tcp_echoserver_error(void *arg, err_t err)
{
    struct echoserver *es;
    LWIP_UNUSED_ARG(err);
    es = (struct echoserver *)arg;

    if(es != NULL)
    {
        /*  free es structure */
        myfree(es);
    }
}
/**
  * @brief  This function implements the tcp_poll LwIP callback function
  * @param  arg: pointer on argument passed to callback
  * @param  tpcb: pointer on the tcp_pcb for the current tcp connection
  * @retval err_t: error code
  */
static err_t tcp_echoserver_poll(void *arg, struct tcp_pcb *tpcb)
{
    err_t ret_err;
    struct echoserver *es;
    es = (struct echoserver *)arg;

    if(es != NULL)
    {
        if(es->p_tx != NULL)
        {
            /* there is a remaining pbuf (chain) , try to send data */
            tcp_echoserver_send(tpcb, es);
        }
        else
        {
            /* no remaining pbuf (chain)  */
            if(es->state == ES_CLOSING)
            {
                /*  close tcp connection */
                tcp_echoserver_connection_close(tpcb, es);
            }
        }

        ret_err = ERR_OK;
    }
    else
    {
        /* nothing to be done */
        tcp_abort(tpcb);
        ret_err = ERR_ABRT;
    }

    return ret_err;
}
/**
  * @brief  This function implements the tcp_sent LwIP callback (called when ACK
  *         is received from remote host for sent data)
  * @param  None
  * @retval None
  */
static err_t tcp_echoserver_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
    struct echoserver *es;
    LWIP_UNUSED_ARG(len);
    es = (struct echoserver *)arg;

    if(es->p_tx != NULL)
    {
        /* still got pbufs to send */
        es->p_tx = NULL;
        tcp_echoserver_send(tpcb, es);
    }
    else
    {
        /* if no more data to send and client closed connection*/
        if(es->state == ES_CLOSING)
        {
            tcp_echoserver_connection_close(tpcb, es);
        }
    }

    return ERR_OK;
}
/**
  * @brief  This function is used to send data for tcp connection
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  es: pointer on echo_state structure
  * @retval None
  */
static void tcp_echoserver_send(struct tcp_pcb *tpcb, struct echoserver *es)
{
    struct pbuf *ptr;
    err_t wr_err = ERR_OK;

    while((wr_err == ERR_OK) &&
            (es->p_tx != NULL) &&
            (es->p_tx->len <= tcp_sndbuf(tpcb)))
    {
        /* get pointer on pbuf from es structure */
        ptr = es->p_tx;
        /* enqueue data for transmission */
        wr_err = tcp_write(tpcb, ptr->payload, ptr->len, 1);

        if(wr_err == ERR_OK)
        {
            u16_t plen;
            plen = ptr->len;
            /* continue with next pbuf in chain (if any) */
            es->p_tx = ptr->next;

            if(es->p_tx != NULL)
            {
                /* increment reference count for es->p_tx */
                pbuf_ref(es->p_tx);
            }

            /* free pbuf: will free pbufs up to es->p_tx (because es->p_tx has a reference count > 0) */
            pbuf_free(ptr);
            /* Update tcp window size to be advertized : should be called when received
            data (with the amount plen) has been processed by the application layer */
            tcp_recved(tpcb, plen);
        }
        else if(wr_err == ERR_MEM)
        {
            /* we are low on memory, try later / harder, defer to poll */
            es->p_tx = ptr;
        }
        else
        {
            /* other problem ?? */
        }
    }
}
/**
  * @brief  This functions closes the tcp connection
  * @param  tcp_pcb: pointer on the tcp connection
  * @param  es: pointer on echo_state structure
  * @retval None
  */
static void tcp_echoserver_connection_close(struct tcp_pcb *tpcb, struct echoserver *es)
{
    /* remove all callbacks */
    tcp_arg(tpcb, NULL);
    tcp_recv(tpcb, NULL);
    tcp_sent(tpcb, NULL);
    tcp_err(tpcb, NULL);
    tcp_poll(tpcb, NULL, 0);

    if(es != NULL)
    {
        myfree(es);
    }

    /* close tcp connection */
    tcp_close(tpcb);
}


void tcp_server_detection(void)
{
    if(tcp_server_flag > 0)
    {
        if((LocalTime - tcp_client_time) >= 2 * 60 * 1000)
        {
#ifdef DEBUG
            printf("tcp_server_flag %d\n", tcp_server_flag);
            printf("tcp_server_detection\n");
#endif
            tcp_server_flag = 0;
            tcp_echoserver_connection_close(ClintTpcb, echoserver_es);
            tcp_client_time = LocalTime;
        }
    }
}
#endif /* LWIP_TCP */
