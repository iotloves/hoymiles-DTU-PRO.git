#include "pwr.h"
/***********************************************
** Function name:
** Descriptions:
** input parameters:
** output parameters:
** Returned value:
*************************************************/
void PWR_PVD_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    //使能PWR时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    //使能PVD所在的外部中断通道
    NVIC_InitStructure.NVIC_IRQChannel = PVD_IRQn;
    //抢占优先级1
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    //子优先级0
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    //使能外部中断通道
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    EXTI_StructInit(&EXTI_InitStructure);
    //PVD连接到中断线16上
    EXTI_InitStructure.EXTI_Line = EXTI_Line16;
    //使用中断模式
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    //电压低于阀值时产生中断
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    //使能中断线
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    //初始
    EXTI_Init(&EXTI_InitStructure);
    //设定监控阀值  PWR_PVDLevel_2V8
    PWR_PVDLevelConfig(PWR_PVDLEVEL);
    //使能PVD
    PWR_PVDCmd(ENABLE);
}

/***********************************************
** Function name:
** Descriptions:
** input parameters:
** output parameters:
** Returned value:
*************************************************/
void PVD_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line16) != RESET)
    {
        //        printf("没电啦!!!!!\n");
        //        MOTOR_absolute[0]=0;
        //        MOTOR_absolute[1]=1;
        //        MOTOR_absolute[2]=2;
        //        MOTOR_absolute[3]=3;
        //      printf("\r\n写入的数据为：\r\n%s", Tx_Buffer);
        //掉电了紧急情况处理
        //写入坐标值到FLASH
        //        STMFLASH_Write(FLASH_SAVE_ADDR,(u32*)MOTOR_absolute,4);
        //        printf("%d %d %d %d\n",Xabsolute,Yabsolute,Zabsolute,Oabsolute);
        /* Clear the Key Button EXTI line pending bit */
        EXTI_ClearITPendingBit(EXTI_Line16);
    }
}
