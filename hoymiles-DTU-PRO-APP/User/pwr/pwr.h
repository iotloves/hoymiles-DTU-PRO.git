#ifndef __PWR_H
#define __PWR_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "usart.h"
#include "spi_flash.h"

#define PWR_PVDLEVEL PWR_PVDLevel_7
#else
#include "stm32f10x.h"
#include "stm32f10x_pwr.h"
#include "usart_debug.h"
#include "spi_flash.h"
#include "stdio.h"
#define PWR_PVDLEVEL PWR_PVDLevel_2V9
#endif
#define  FLASH_WriteAddress     0
#define  FLASH_ReadAddress      FLASH_WriteAddress
#define  FLASH_SectorToErase    FLASH_WriteAddress

void PWR_PVD_Init(void);
#endif
