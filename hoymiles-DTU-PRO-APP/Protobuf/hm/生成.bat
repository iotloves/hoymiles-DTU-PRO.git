DEL DTU_APP\*.c
DEL  DTU_Server\*.c
DEL  Protobuf\*.c
DEL DTU_APP\*.h
DEL  DTU_Server\*.h
DEL  Protobuf\*.h
..\generator\protoc -o DTU_APP\APPInfomationData.pb DTU_APP\APPInfomationData.proto
..\generator\protoc -o DTU_APP\GetConfig.pb DTU_APP\GetConfig.proto
..\generator\protoc -o DTU_APP\SetConfig.pb DTU_APP\SetConfig.proto

..\generator\protoc -o DTU_Server\DevConfig.pb DTU_Server\DevConfig.proto
..\generator\protoc -o DTU_Server\CommandPB.pb DTU_Server\CommandPB.proto
..\generator\protoc -o DTU_Server\HeartbeatPB.pb DTU_Server\HeartbeatPB.proto
..\generator\protoc -o DTU_Server\InfomationData.pb DTU_Server\InfomationData.proto
..\generator\protoc -o DTU_Server\RealData.pb DTU_Server\RealData.proto
..\generator\protoc -o DTU_Server\AlarmData.pb DTU_Server\AlarmData.proto

..\generator\nanopb_generator.exe -f DTU_APP\APPInfomationData.options DTU_APP\APPInfomationData.pb
..\generator\nanopb_generator.exe -f DTU_APP\GetConfig.options DTU_APP\GetConfig.pb
..\generator\nanopb_generator.exe -f DTU_APP\SetConfig.options DTU_APP\SetConfig.pb

..\generator\nanopb_generator.exe -f DTU_Server\DevConfig.options DTU_Server\DevConfig.pb
..\generator\nanopb_generator.exe -f DTU_Server\CommandPB.options DTU_Server\CommandPB.pb
..\generator\nanopb_generator.exe -f DTU_Server\HeartbeatPB.options DTU_Server\HeartbeatPB.pb
..\generator\nanopb_generator.exe -f DTU_Server\InfomationData.options DTU_Server\InfomationData.pb
..\generator\nanopb_generator.exe -f DTU_Server\RealData.options DTU_Server\RealData.pb
..\generator\nanopb_generator.exe -f DTU_Server\AlarmData.options DTU_Server\AlarmData.pb

DEL DTU_APP\*.pb
DEL DTU_Server\*.pb
..\generator\AStyle.exe  DTU_APP\*.c DTU_APP\*.h DTU_APP\*.cpp  -A1 -C -S -K -Y -f -p -U -o -c -Z -z1 -w  -n --suffix=none --unpad-paren --delete-empty-lines --align-pointer=name --add-braces --indent-col1-comments

..\generator\AStyle.exe  DTU_Server\*.c DTU_Server\*.h DTU_Server\*.cpp  -A1 -C -S -K -Y -f -p -U -o -c -Z -z1 -w  -n --suffix=none --unpad-paren --delete-empty-lines --align-pointer=name --add-braces --indent-col1-comments

move .\DTU_APP\APPInfomationData.pb.c .\Protobuf
move .\DTU_APP\GetConfig.pb.c .\Protobuf
move .\DTU_APP\SetConfig.pb.c .\Protobuf

move .\DTU_APP\APPInfomationData.pb.h .\Protobuf
move .\DTU_APP\GetConfig.pb.h .\Protobuf
move .\DTU_APP\SetConfig.pb.h .\Protobuf

move .\DTU_Server\CommandPB.pb.c .\Protobuf
move .\DTU_Server\DevConfig.pb.c .\Protobuf
move .\DTU_Server\HeartbeatPB.pb.c .\Protobuf
move .\DTU_Server\InfomationData.pb.c .\Protobuf
move .\DTU_Server\RealData.pb.c .\Protobuf
move .\DTU_Server\AlarmData.pb.c .\Protobuf

move .\DTU_Server\CommandPB.pb.h .\Protobuf
move .\DTU_Server\DevConfig.pb.h .\Protobuf
move .\DTU_Server\HeartbeatPB.pb.h .\Protobuf
move .\DTU_Server\InfomationData.pb.h .\Protobuf
move .\DTU_Server\RealData.pb.h .\Protobuf
move .\DTU_Server\AlarmData.pb.h .\Protobuf