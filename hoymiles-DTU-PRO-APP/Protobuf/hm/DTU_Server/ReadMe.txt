2019.12.17
修改InfomationData.proto中增加防逆流电网类型、防逆流开关、a项余电上网、b项余电上网、c项余电上网，防逆流控制方式
2019.12.16
修改CommandPB.proto中mi_sn_item_a的备注 
2019.11.13
新增AlarmData.proto 用于上传告警和滤波
2019.11.06
InfomationData DtuInfoMO中增加dtu_error_code DTU错误代码
2019.10.15
RealData.proto 增加MI PV 当日发电量 
2019.08.22
DevConfig.proto 增加 rule_type  规则类型 0：微逆基本规则 1：微逆并网保护文件
2019.08.12
HeartbeatPB.proto增加了设备的SN字段
2019.08.06
InfomationData.proto 中电表信息MeterInfoMO更改为多包
2019.07.16
协议做了更新，更改CommandPB中的多微逆控制数量最大为100，DevConfig增加多微逆控制，RealDatta中增加电表数据