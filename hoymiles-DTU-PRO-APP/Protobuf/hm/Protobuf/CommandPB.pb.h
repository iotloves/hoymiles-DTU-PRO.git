/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.2 at Wed Jun 24 15:40:28 2020. */

#ifndef PB_COMMANDPB_PB_H_INCLUDED
#define PB_COMMANDPB_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _CommandReqDTO
{
    pb_callback_t dtu_sn;
    int32_t time;
    int32_t action;
    int32_t package_now;
    int32_t err_code;
    int64_t tid;
    /* @@protoc_insertion_point(struct:CommandReqDTO) */
} CommandReqDTO;

typedef struct _CommandResDTO
{
    int32_t time;
    int32_t action;
    int32_t dev_kind;
    int32_t package_nub;
    int32_t package_now;
    int64_t tid;
    pb_callback_t data;
    pb_callback_t es_to_sn;
    pb_size_t mi_to_sn_count;
    int64_t mi_to_sn[100];
    int32_t system_total_a;
    int32_t system_total_b;
    int32_t system_total_c;
    pb_size_t mi_sn_item_a_count;
    int64_t mi_sn_item_a[100];
    pb_size_t mi_sn_item_b_count;
    int64_t mi_sn_item_b[100];
    pb_size_t mi_sn_item_c_count;
    int64_t mi_sn_item_c[100];
    /* @@protoc_insertion_point(struct:CommandResDTO) */
} CommandResDTO;

typedef struct _CommandStatusResDTO
{
    int32_t time;
    int32_t action;
    int32_t package_now;
    int64_t tid;
    int32_t err_code;
    /* @@protoc_insertion_point(struct:CommandStatusResDTO) */
} CommandStatusResDTO;

typedef struct _ESOperatingStatusMO
{
    pb_callback_t es_sn;
    int32_t progress_rate;
    /* @@protoc_insertion_point(struct:ESOperatingStatusMO) */
} ESOperatingStatusMO;

typedef struct _MIOperatingStatusMO
{
    int64_t mi_sn;
    int32_t progress_rate;
    /* @@protoc_insertion_point(struct:MIOperatingStatusMO) */
} MIOperatingStatusMO;

typedef struct _CommandStatusReqDTO
{
    pb_callback_t dtu_sn;
    int32_t time;
    int32_t action;
    int32_t package_nub;
    int32_t package_now;
    int64_t tid;
    pb_callback_t es_sns_sucs;
    pb_size_t mi_sns_sucs_count;
    int64_t mi_sns_sucs[100];
    pb_callback_t es_sns_failds;
    pb_size_t mi_sns_failds_count;
    int64_t mi_sns_failds[100];
    pb_size_t es_mOperatingStatus_count;
    ESOperatingStatusMO es_mOperatingStatus[20];
    pb_size_t mi_mOperatingStatus_count;
    MIOperatingStatusMO mi_mOperatingStatus[50];
    /* @@protoc_insertion_point(struct:CommandStatusReqDTO) */
} CommandStatusReqDTO;

/* Default values for struct fields */

/* Initializer values for message structs */
#define CommandResDTO_init_default               {0, 0, 0, 0, 0, 0, {{NULL}, NULL}, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, 0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define CommandReqDTO_init_default               {{{NULL}, NULL}, 0, 0, 0, 0, 0}
#define ESOperatingStatusMO_init_default         {{{NULL}, NULL}, 0}
#define MIOperatingStatusMO_init_default         {0, 0}
#define CommandStatusReqDTO_init_default         {{{NULL}, NULL}, 0, 0, 0, 0, 0, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default, ESOperatingStatusMO_init_default}, 0, {MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default, MIOperatingStatusMO_init_default}}
#define CommandStatusResDTO_init_default         {0, 0, 0, 0, 0}
#define CommandResDTO_init_zero                  {0, 0, 0, 0, 0, 0, {{NULL}, NULL}, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, 0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define CommandReqDTO_init_zero                  {{{NULL}, NULL}, 0, 0, 0, 0, 0}
#define ESOperatingStatusMO_init_zero            {{{NULL}, NULL}, 0}
#define MIOperatingStatusMO_init_zero            {0, 0}
#define CommandStatusReqDTO_init_zero            {{{NULL}, NULL}, 0, 0, 0, 0, 0, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {{NULL}, NULL}, 0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 0, {ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero, ESOperatingStatusMO_init_zero}, 0, {MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero, MIOperatingStatusMO_init_zero}}
#define CommandStatusResDTO_init_zero            {0, 0, 0, 0, 0}

/* Field tags (for use in manual encoding/decoding) */
#define CommandReqDTO_dtu_sn_tag                 1
#define CommandReqDTO_time_tag                   2
#define CommandReqDTO_action_tag                 3
#define CommandReqDTO_package_now_tag            4
#define CommandReqDTO_err_code_tag               5
#define CommandReqDTO_tid_tag                    6
#define CommandResDTO_time_tag                   1
#define CommandResDTO_action_tag                 2
#define CommandResDTO_dev_kind_tag               3
#define CommandResDTO_package_nub_tag            4
#define CommandResDTO_package_now_tag            5
#define CommandResDTO_tid_tag                    6
#define CommandResDTO_data_tag                   7
#define CommandResDTO_es_to_sn_tag               8
#define CommandResDTO_mi_to_sn_tag               9
#define CommandResDTO_system_total_a_tag         10
#define CommandResDTO_system_total_b_tag         11
#define CommandResDTO_system_total_c_tag         12
#define CommandResDTO_mi_sn_item_a_tag           13
#define CommandResDTO_mi_sn_item_b_tag           14
#define CommandResDTO_mi_sn_item_c_tag           15
#define CommandStatusResDTO_time_tag             1
#define CommandStatusResDTO_action_tag           2
#define CommandStatusResDTO_package_now_tag      3
#define CommandStatusResDTO_tid_tag              4
#define CommandStatusResDTO_err_code_tag         5
#define ESOperatingStatusMO_es_sn_tag            1
#define ESOperatingStatusMO_progress_rate_tag    2
#define MIOperatingStatusMO_mi_sn_tag            1
#define MIOperatingStatusMO_progress_rate_tag    2
#define CommandStatusReqDTO_dtu_sn_tag           1
#define CommandStatusReqDTO_time_tag             2
#define CommandStatusReqDTO_action_tag           3
#define CommandStatusReqDTO_package_nub_tag      4
#define CommandStatusReqDTO_package_now_tag      5
#define CommandStatusReqDTO_tid_tag              6
#define CommandStatusReqDTO_es_sns_sucs_tag      7
#define CommandStatusReqDTO_mi_sns_sucs_tag      8
#define CommandStatusReqDTO_es_sns_failds_tag    9
#define CommandStatusReqDTO_mi_sns_failds_tag    10
#define CommandStatusReqDTO_es_mOperatingStatus_tag 11
#define CommandStatusReqDTO_mi_mOperatingStatus_tag 12

/* Struct field encoding specification for nanopb */
extern const pb_field_t CommandResDTO_fields[16];
extern const pb_field_t CommandReqDTO_fields[7];
extern const pb_field_t ESOperatingStatusMO_fields[3];
extern const pb_field_t MIOperatingStatusMO_fields[3];
extern const pb_field_t CommandStatusReqDTO_fields[13];
extern const pb_field_t CommandStatusResDTO_fields[6];

/* Maximum encoded size of messages (where known) */
/* CommandResDTO_size depends on runtime parameters */
/* CommandReqDTO_size depends on runtime parameters */
/* ESOperatingStatusMO_size depends on runtime parameters */
#define MIOperatingStatusMO_size                 22
/* CommandStatusReqDTO_size depends on runtime parameters */
#define CommandStatusResDTO_size                 55

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define COMMANDPB_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
