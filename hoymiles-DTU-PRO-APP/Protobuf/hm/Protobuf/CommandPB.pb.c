/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.2 at Wed Jun 24 15:40:28 2020. */

#include "CommandPB.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t CommandResDTO_fields[16] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, CommandResDTO, time, time, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, action, time, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, dev_kind, action, 0),
    PB_FIELD(4, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, package_nub, dev_kind, 0),
    PB_FIELD(5, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, package_now, package_nub, 0),
    PB_FIELD(6, INT64, SINGULAR, STATIC, OTHER, CommandResDTO, tid, package_now, 0),
    PB_FIELD(7, BYTES, SINGULAR, CALLBACK, OTHER, CommandResDTO, data, tid, 0),
    PB_FIELD(8, BYTES, REPEATED, CALLBACK, OTHER, CommandResDTO, es_to_sn, data, 0),
    PB_FIELD(9, INT64, REPEATED, STATIC, OTHER, CommandResDTO, mi_to_sn, es_to_sn, 0),
    PB_FIELD(10, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, system_total_a, mi_to_sn, 0),
    PB_FIELD(11, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, system_total_b, system_total_a, 0),
    PB_FIELD(12, INT32, SINGULAR, STATIC, OTHER, CommandResDTO, system_total_c, system_total_b, 0),
    PB_FIELD(13, INT64, REPEATED, STATIC, OTHER, CommandResDTO, mi_sn_item_a, system_total_c, 0),
    PB_FIELD(14, INT64, REPEATED, STATIC, OTHER, CommandResDTO, mi_sn_item_b, mi_sn_item_a, 0),
    PB_FIELD(15, INT64, REPEATED, STATIC, OTHER, CommandResDTO, mi_sn_item_c, mi_sn_item_b, 0),
    PB_LAST_FIELD
};

const pb_field_t CommandReqDTO_fields[7] =
{
    PB_FIELD(1, BYTES, SINGULAR, CALLBACK, FIRST, CommandReqDTO, dtu_sn, dtu_sn, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, CommandReqDTO, time, dtu_sn, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, CommandReqDTO, action, time, 0),
    PB_FIELD(4, INT32, SINGULAR, STATIC, OTHER, CommandReqDTO, package_now, action, 0),
    PB_FIELD(5, INT32, SINGULAR, STATIC, OTHER, CommandReqDTO, err_code, package_now, 0),
    PB_FIELD(6, INT64, SINGULAR, STATIC, OTHER, CommandReqDTO, tid, err_code, 0),
    PB_LAST_FIELD
};

const pb_field_t ESOperatingStatusMO_fields[3] =
{
    PB_FIELD(1, BYTES, SINGULAR, CALLBACK, FIRST, ESOperatingStatusMO, es_sn, es_sn, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, ESOperatingStatusMO, progress_rate, es_sn, 0),
    PB_LAST_FIELD
};

const pb_field_t MIOperatingStatusMO_fields[3] =
{
    PB_FIELD(1, INT64, SINGULAR, STATIC, FIRST, MIOperatingStatusMO, mi_sn, mi_sn, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, MIOperatingStatusMO, progress_rate, mi_sn, 0),
    PB_LAST_FIELD
};

const pb_field_t CommandStatusReqDTO_fields[13] =
{
    PB_FIELD(1, BYTES, SINGULAR, CALLBACK, FIRST, CommandStatusReqDTO, dtu_sn, dtu_sn, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, CommandStatusReqDTO, time, dtu_sn, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, CommandStatusReqDTO, action, time, 0),
    PB_FIELD(4, INT32, SINGULAR, STATIC, OTHER, CommandStatusReqDTO, package_nub, action, 0),
    PB_FIELD(5, INT32, SINGULAR, STATIC, OTHER, CommandStatusReqDTO, package_now, package_nub, 0),
    PB_FIELD(6, INT64, SINGULAR, STATIC, OTHER, CommandStatusReqDTO, tid, package_now, 0),
    PB_FIELD(7, BYTES, REPEATED, CALLBACK, OTHER, CommandStatusReqDTO, es_sns_sucs, tid, 0),
    PB_FIELD(8, INT64, REPEATED, STATIC, OTHER, CommandStatusReqDTO, mi_sns_sucs, es_sns_sucs, 0),
    PB_FIELD(9, BYTES, REPEATED, CALLBACK, OTHER, CommandStatusReqDTO, es_sns_failds, mi_sns_sucs, 0),
    PB_FIELD(10, INT64, REPEATED, STATIC, OTHER, CommandStatusReqDTO, mi_sns_failds, es_sns_failds, 0),
    PB_FIELD(11, MESSAGE, REPEATED, STATIC, OTHER, CommandStatusReqDTO, es_mOperatingStatus, mi_sns_failds, &ESOperatingStatusMO_fields),
    PB_FIELD(12, MESSAGE, REPEATED, STATIC, OTHER, CommandStatusReqDTO, mi_mOperatingStatus, es_mOperatingStatus, &MIOperatingStatusMO_fields),
    PB_LAST_FIELD
};

const pb_field_t CommandStatusResDTO_fields[6] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, CommandStatusResDTO, time, time, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, CommandStatusResDTO, action, time, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, CommandStatusResDTO, package_now, action, 0),
    PB_FIELD(4, INT64, SINGULAR, STATIC, OTHER, CommandStatusResDTO, tid, package_now, 0),
    PB_FIELD(5, INT32, SINGULAR, STATIC, OTHER, CommandStatusResDTO, err_code, tid, 0),
    PB_LAST_FIELD
};


/* Check that field information fits in pb_field_t */
#if !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_32BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in 8 or 16 bit
 * field descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(CommandStatusReqDTO, es_mOperatingStatus[0]) < 65536 && pb_membersize(CommandStatusReqDTO, mi_mOperatingStatus[0]) < 65536), YOU_MUST_DEFINE_PB_FIELD_32BIT_FOR_MESSAGES_CommandResDTO_CommandReqDTO_ESOperatingStatusMO_MIOperatingStatusMO_CommandStatusReqDTO_CommandStatusResDTO)
#endif

#if !defined(PB_FIELD_16BIT) && !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_16BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in the default
 * 8 bit descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(CommandStatusReqDTO, es_mOperatingStatus[0]) < 256 && pb_membersize(CommandStatusReqDTO, mi_mOperatingStatus[0]) < 256), YOU_MUST_DEFINE_PB_FIELD_16BIT_FOR_MESSAGES_CommandResDTO_CommandReqDTO_ESOperatingStatusMO_MIOperatingStatusMO_CommandStatusReqDTO_CommandStatusResDTO)
#endif


/* @@protoc_insertion_point(eof) */
