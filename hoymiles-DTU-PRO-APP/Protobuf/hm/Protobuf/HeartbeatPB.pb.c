/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.2 at Wed Jun 24 15:40:28 2020. */

#include "HeartbeatPB.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t HBReqDTO_fields[6] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, HBReqDTO, offset, offset, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, HBReqDTO, time, offset, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, HBReqDTO, csq, time, 0),
    PB_FIELD(4, BYTES, SINGULAR, CALLBACK, OTHER, HBReqDTO, dtu_sn, csq, 0),
    PB_FIELD(5, BYTES, SINGULAR, CALLBACK, OTHER, HBReqDTO, dev_sn, dtu_sn, 0),
    PB_LAST_FIELD
};

const pb_field_t HBResDTO_fields[4] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, HBResDTO, offset, offset, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, HBResDTO, time, offset, 0),
    PB_FIELD(3, BYTES, SINGULAR, STATIC, OTHER, HBResDTO, ymd_hms, time, 0),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
