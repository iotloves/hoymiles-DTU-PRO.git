/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.2 at Wed Jun 24 15:40:27 2020. */

#include "SetConfig.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t SetConfigRes_fields[38] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, SetConfigRes, offset, offset, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, time, offset, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, lock_password, time, 0),
    PB_FIELD(4, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, lock_time, lock_password, 0),
    PB_FIELD(5, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, limit_power_mypower, lock_time, 0),
    PB_FIELD(6, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, zero_export_433_addr, limit_power_mypower, 0),
    PB_FIELD(7, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, zero_export_enable, zero_export_433_addr, 0),
    PB_FIELD(8, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, netmode_select, zero_export_enable, 0),
    PB_FIELD(9, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, channel_select, netmode_select, 0),
    PB_FIELD(10, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, server_send_time, channel_select, 0),
    PB_FIELD(11, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, serverport, server_send_time, 0),
    PB_FIELD(12, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, apn_set, serverport, 0),
    PB_FIELD(13, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, meter_kind, apn_set, 0),
    PB_FIELD(14, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, meter_interface, meter_kind, 0),
    PB_FIELD(15, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, wifi_ssid, meter_interface, 0),
    PB_FIELD(16, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, wifi_passward, wifi_ssid, 0),
    PB_FIELD(17, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, server_domain_name, wifi_passward, 0),
    PB_FIELD(18, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, inv_type, server_domain_name, 0),
    PB_FIELD(19, BYTES, SINGULAR, CALLBACK, OTHER, SetConfigRes, dtu_sn, inv_type, 0),
    PB_FIELD(20, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, access_model, dtu_sn, 0),
    PB_FIELD(21, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, mac_0, access_model, 0),
    PB_FIELD(22, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, mac_1, mac_0, 0),
    PB_FIELD(23, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, mac_2, mac_1, 0),
    PB_FIELD(24, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, mac_3, mac_2, 0),
    PB_FIELD(25, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, dhcp_switch, mac_3, 0),
    PB_FIELD(26, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, ip_addr_0, dhcp_switch, 0),
    PB_FIELD(27, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, ip_addr_1, ip_addr_0, 0),
    PB_FIELD(28, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, ip_addr_2, ip_addr_1, 0),
    PB_FIELD(29, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, ip_addr_3, ip_addr_2, 0),
    PB_FIELD(30, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, subnet_mask_0, ip_addr_3, 0),
    PB_FIELD(31, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, subnet_mask_1, subnet_mask_0, 0),
    PB_FIELD(32, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, subnet_mask_2, subnet_mask_1, 0),
    PB_FIELD(33, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, subnet_mask_3, subnet_mask_2, 0),
    PB_FIELD(34, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, default_gateway_0, subnet_mask_3, 0),
    PB_FIELD(35, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, default_gateway_1, default_gateway_0, 0),
    PB_FIELD(36, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, default_gateway_2, default_gateway_1, 0),
    PB_FIELD(37, INT32, SINGULAR, STATIC, OTHER, SetConfigRes, default_gateway_3, default_gateway_2, 0),
    PB_LAST_FIELD
};

const pb_field_t SetConfigReq_fields[4] =
{
    PB_FIELD(1, INT32, SINGULAR, STATIC, FIRST, SetConfigReq, offset, offset, 0),
    PB_FIELD(2, INT32, SINGULAR, STATIC, OTHER, SetConfigReq, time, offset, 0),
    PB_FIELD(3, INT32, SINGULAR, STATIC, OTHER, SetConfigReq, err_code, time, 0),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
