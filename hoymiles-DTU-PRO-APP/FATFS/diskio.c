/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/
#include "ff.h"
#include "diskio.h" /* FatFs lower layer API */
#include "sdio_sd.h"
#include "rtc.h"
#include "string.h"
#include "malloc.h"
/* 为每个设备定义一个物理编号 */
#define SDCard              2     // 预留SD卡使用
#define SD_BLOCKSIZE        512

extern  SD_CardInfo SDCardInfo;
/*-----------------------------------------------------------------------*/
/* 获取设备状态                                                          */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(
    BYTE pdrv       /* Physical drive nmuber to identify the drive */
)
{
    DSTATUS status = STA_NOINIT;

    switch(pdrv)
    {
        case SDCard:    /* SD CARD */
            status &= ~STA_NOINIT;
            break;

        default:
            status = STA_NOINIT;
    }

    return status;
}



/*-----------------------------------------------------------------------*/
/* 设备初始化                                                            */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(
    BYTE pdrv               /* Physical drive nmuber to identify the drive */
)
{
    vu16 i;
    DSTATUS status = STA_NOINIT;

    switch(pdrv)
    {
        case SDCard:             /* SD CARD */
            if(SD_Init() == SD_OK)
            {
                status &= ~STA_NOINIT;
            }
            else
            {
                status = STA_NOINIT;
            }

            break;

        default:
            status = STA_NOINIT;
    }

    return status;
}

/*-----------------------------------------------------------------------*/
/* 读扇区：读取扇区内容到指定存储区                                              */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(
    BYTE pdrv,      /* Physical drive nmuber to identify the drive */
    BYTE *buff,     /* Data buffer to store read data */
    DWORD sector,   /* Start sector in LBA */
    UINT count      /* Number of sectors to read */
)
{
    DRESULT status = RES_PARERR;
    SD_Error SD_state = SD_OK;

    switch(pdrv)
    {
        case SDCard:    /* SD CARD */
            if((DWORD)buff & 3)
            {
                DRESULT res = RES_OK;
                DWORD scratch[SD_BLOCKSIZE / 4];

                while(count--)
                {
                    res = disk_read(SDCard, (void *)scratch, sector++, 1);

                    if(res != RES_OK)
                    {
                        break;
                    }

                    memcpy(buff, scratch, SD_BLOCKSIZE);
                    buff += SD_BLOCKSIZE;
                }

                return res;
            }

            SD_state = SD_ReadMultiBlocks(buff, sector * SD_BLOCKSIZE, SD_BLOCKSIZE, count);

            if(SD_state == SD_OK)
            {
                /* Check if the Transfer is finished */
                SD_state = SD_WaitReadOperation();

                while(SD_GetStatus() != SD_TRANSFER_OK);
            }

            if(SD_state != SD_OK)
            {
                status = RES_PARERR;
            }
            else
            {
                status = RES_OK;
            }

            break;

        default:
            status = RES_PARERR;
    }

    return status;
}



/*-----------------------------------------------------------------------*/
/* 写扇区：见数据写入指定扇区空间上                                         */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write(
    BYTE pdrv,          /* Physical drive nmuber to identify the drive */
    const BYTE *buff,   /* Data to be written */
    DWORD sector,       /* Start sector in LBA */
    UINT count          /* Number of sectors to write */
)
{
    vu32 write_addr;
    DRESULT status = RES_PARERR;
    SD_Error SD_state = SD_OK;

    if(!count)
    {
        return RES_PARERR;      /* Check parameter */
    }

    switch(pdrv)
    {
        case SDCard:    /* SD CARD */
            if((DWORD)buff & 3)
            {
                DRESULT res = RES_OK;
                DWORD scratch[SD_BLOCKSIZE / 4];

                while(count--)
                {
                    memcpy(scratch, buff, SD_BLOCKSIZE);
                    res = disk_write(SDCard, (void *)scratch, sector++, 1);

                    if(res != RES_OK)
                    {
                        break;
                    }

                    buff += SD_BLOCKSIZE;
                }

                return res;
            }

            SD_state = SD_WriteMultiBlocks((uint8_t *)buff, sector * SD_BLOCKSIZE, SD_BLOCKSIZE, count);

            if(SD_state == SD_OK)
            {
                /* Check if the Transfer is finished */
                SD_state = SD_WaitWriteOperation();

                /* Wait until end of DMA transfer */
                while(SD_GetStatus() != SD_TRANSFER_OK);
            }

            if(SD_state != SD_OK)
            {
                status = RES_PARERR;
            }
            else
            {
                status = RES_OK;
            }

            break;

        default:
            status = RES_PARERR;
    }

    return status;
}

#endif


/*-----------------------------------------------------------------------*/
/* 其他控制                                                              */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl(
    BYTE pdrv,      /* Physical drive nmuber (0..) */
    BYTE cmd,       /* Control code */
    void *buff      /* Buffer to send/receive control Data */
)
{
    DRESULT status = RES_PARERR;

    switch(pdrv)
    {
        case SDCard:    /* SD CARD */
            switch(cmd)
            {
                // Get R/W sector size (WORD)
                case GET_SECTOR_SIZE:
                    *(WORD *)buff = SD_BLOCKSIZE;
                    break;

                // Get erase block size in unit of sector (DWORD)
                case GET_BLOCK_SIZE:
                    //SDCardInfo.CardBlockSize;
                    *(DWORD *)buff = 1;
                    break;

                case GET_SECTOR_COUNT:
                    *(DWORD *)buff = SDCardInfo.CardCapacity / SDCardInfo.CardBlockSize;
                    break;

                case CTRL_SYNC:
                    break;
            }

            status = RES_OK;
            break;

        default:
            status = RES_PARERR;
    }

    return status;
}

DWORD get_fattime(void)
{
    calendar_obj calendar;
    RTC_GetTimes(&calendar);
    /* 返回当前时间戳 */
    return ((DWORD)(calendar.w_year - 1980) << 25)              /* Year 2015 */
           | ((DWORD)(calendar.w_month) << 21)              /* Month 1 */
           | ((DWORD)(calendar.w_date) << 16)               /* Mday 1 */
           | ((DWORD)(calendar.hour) << 11)                 /* Hour 0 */
           | ((DWORD)(calendar.min) << 5)                   /* Min 0 */
           | ((DWORD)(calendar.sec) >> 1);                  /* Sec 0 */
}

//动态分配内存
void *ff_memalloc(UINT size)
{
    return (void *)mymalloc(size);
}
//释放内存
void ff_memfree(void *mf)
{
    myfree(mf);
}
