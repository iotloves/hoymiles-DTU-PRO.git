#include "usart.h"
#include "led.h"
#include "string.h"
#include "crc16.h"
#include "SysTick.h"

uint32_t Usart_485_Rec_Time;
uint8_t Usart_485_Tx_Buf[Usart_485_Len];     //接收缓冲,最大USART_REC_LEN个字节.
uint16_t Usart_485_Tx_Cnt = 0;
uint16_t Usart_485_Tx_Rp = 0;
uint8_t Usart_485_Rx_Buf[Usart_485_Len];     //接收缓冲,最大USART_REC_LEN个字节.
uint16_t Usart_485_Rx_Cnt = 0;
uint8_t  Usart_485_Rec_Data;

//接收状态
//bit15，    接收完成标志
//bit14，    接收到0x0d
//bit13~0，  接收到的有效字节数目
#ifdef DEBUG
u8 USART_RX_BUF[USART_REC_LEN];
uint16_t USART_RX_STA = 0;       //接收状态标记
#endif
/**
* @brief  配置嵌套向量中断控制器NVIC
* @param  无
* @retval 无
*/
static void NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    /* 嵌套向量中断控制器组选择 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    /* 配置USART为中断源 */
    NVIC_InitStructure.NVIC_IRQChannel = Usart_485_IRQ;
    /* 抢断优先级为1 */
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    /* 子优先级为1 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    /* 使能中断 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    /* 初始化配置NVIC */
    NVIC_Init(&NVIC_InitStructure);
}


/**
* @brief  Usart_485 GPIO 配置,工作模式配置。115200 8-N-1 ，中断接收模式
* @param  无
* @retval 无
*/
void Usart_485_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    RCC_AHB1PeriphClockCmd(Usart_485_RX_GPIO_CLK | Usart_485_TX_GPIO_CLK, ENABLE);
    /* 使能 USART 时钟 */
    RCC_APB2PeriphClockCmd(Usart_485_CLK, ENABLE);
    //USART_DeInit(Usart_485);
    //USART_StructInit(&USART_InitStructure);
    //USART_ClockStructInit(&USART_ClockInitStruct);
    /* GPIO初始化 */
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    /* 配置Tx引脚为复用功能  */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = Usart_485_TX_PIN;
    GPIO_Init(Usart_485_TX_GPIO_PORT, &GPIO_InitStructure);
    /* 配置Rx引脚为复用功能 */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = Usart_485_RX_PIN;
    GPIO_Init(Usart_485_RX_GPIO_PORT, &GPIO_InitStructure);
    /* 连接 PXx 到 USARTx_Tx*/
    GPIO_PinAFConfig(Usart_485_RX_GPIO_PORT, Usart_485_RX_SOURCE, Usart_485_RX_AF);
    /*  连接 PXx 到 USARTx__Rx*/
    GPIO_PinAFConfig(Usart_485_TX_GPIO_PORT, Usart_485_TX_SOURCE, Usart_485_TX_AF);
    /* 配置串Usart_485 模式 */
    /* 波特率设置：Usart_485_BAUDRATE */
    USART_InitStructure.USART_BaudRate = Usart_485_BAUDRATE;
    /* 字长(数据位+校验位)：8 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    /* 停止位：1个停止位 */
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    /* 校验位选择：不使用校验 */
    USART_InitStructure.USART_Parity = USART_Parity_No;
    /* 硬件流控制：不使用硬件流 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    /* USART模式控制：同时使能接收和发送 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    /* 完成USART初始化配置 */
    USART_Init(Usart_485, &USART_InitStructure);
    /* 嵌套向量中断控制器NVIC配置 */
    NVIC_Configuration();
    /* 使能串口接收中断 */
    /* 使能串口 */
    USART_Cmd(Usart_485, ENABLE);
    USART_ITConfig(Usart_485, USART_IT_TC, ENABLE);
    USART_ITConfig(Usart_485, USART_IT_RXNE, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    Usart_485_Rece;
}

/*****************  发送一个字符 **********************/
void Usart_SendByte(USART_TypeDef *pUSARTx, uint8_t ch)
{
    Usart_485_Send;
    /* 发送一个字节数据到USART */
    USART_SendData(pUSARTx, ch);

    /* 等待发送数据寄存器为空 */
    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);

    Usart_485_Rece;
}

void Usart_SendBytes(USART_TypeDef *pUSARTx, uint8_t *ch, uint16_t len)
{
    uint16_t i = 0;
    Usart_485_Send;

    for(i = 0; i < len; i++)
    {
        /* 发送一个字节数据到USART */
        USART_SendData(pUSARTx, ch[i]);

        /* 等待发送数据寄存器为空 */
        while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);
    }

    Usart_485_Rece;
}

/*****************  发送字符串 **********************/
void Usart_SendString(USART_TypeDef *pUSARTx, char *str)
{
    unsigned int k = 0;

    do
    {
        Usart_SendByte(pUSARTx, *(str + k));
        k++;
    }
    while(*(str + k) != '\0');

    /* 等待发送完成 */
    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TC) == RESET)
    {
    }
}

/*****************  发送一个16位数 **********************/
void Usart_SendHalfWord(USART_TypeDef *pUSARTx, uint16_t ch)
{
    uint8_t temp_h, temp_l;
    Usart_485_Send;
    /* 取出高八位 */
    temp_h = (ch & 0XFF00) >> 8;
    /* 取出低八位 */
    temp_l = ch & 0XFF;
    /* 发送高八位 */
    USART_SendData(pUSARTx, temp_h);

    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);

    /* 发送低八位 */
    USART_SendData(pUSARTx, temp_l);

    while(USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET);

    Usart_485_Rece;
}

//重定向c库函数printf到串口，重定向后可使用printf函数
int fputc(int ch, FILE *f)
{
    Usart_485_Send;
    /* 发送一个字节数据到串口 */
    USART_SendData(Usart_485, (uint8_t)ch);

    /* 等待发送完毕 */
    while(USART_GetFlagStatus(Usart_485, USART_FLAG_TXE) == RESET);

    Usart_485_Rece;
    return (ch);
}

//重定向c库函数scanf到串口，重写向后可使用scanf、getchar等函数
int fgetc(FILE *f)
{
    /* 等待串口输入数据 */
    while(USART_GetFlagStatus(Usart_485, USART_FLAG_RXNE) == RESET);

    return (int)USART_ReceiveData(Usart_485);
}



//串口接收中断接收
void USART1_IRQHandler(void)
{
    u8 Res;
    u8 buffer = 0  ;
    u32 temp = 0;
    u8 i = 0;

    if(USART_GetITStatus(Usart_485, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(Usart_485, USART_IT_RXNE);
        buffer = USART_ReceiveData(Usart_485);

        if((Usart_485_Rx_Cnt + 1) >= Usart_485_Len)
        {
            Usart_485_Rx_Cnt = 0;
        }

        Usart_485_Rx_Buf[Usart_485_Rx_Cnt++] = buffer;
        Usart_485_Rec_Data = 1;
        Usart_485_Rec_Time = LocalTime;
#ifdef DEBUG
        Res = buffer; //(USART1->DR);  //读取接收到的数据

        if((USART_RX_STA & 0x8000) == 0) //接收未完成
        {
            if(USART_RX_STA & 0x4000) //接收到了0x0d
            {
                if(Res != 0x0a)
                {
                    USART_RX_STA = 0;    //接收错误,重新开始
                }
                else
                {
                    USART_RX_STA |= 0x8000;    //接收完成了
                }
            }
            else //还没收到0X0D
            {
                if(Res == 0x0d)
                {
                    USART_RX_STA |= 0x4000;
                }
                else
                {
                    USART_RX_BUF[USART_RX_STA & 0X3FFF] = Res ;
                    USART_RX_STA++;

                    if(USART_RX_STA > (USART_REC_LEN - 1))
                    {
                        USART_RX_STA = 0;    //接收数据错误,重新开始接收
                    }
                }
            }
        }

#endif
    }
    else if(USART_GetFlagStatus(Usart_485, USART_FLAG_ORE) != RESET)
    {
        USART_ReceiveData(Usart_485);
    }
    else if(USART_GetFlagStatus(Usart_485, USART_FLAG_TC) != RESET)
    {
        if(Usart_485_Tx_Rp < Usart_485_Tx_Cnt)
        {
            USART_SendData(Usart_485, Usart_485_Tx_Buf[Usart_485_Tx_Rp]);
        }
        else
        {
            Usart_485_Tx_Rp = 0;
            Usart_485_Tx_Cnt = 0;
            Usart_485_Rece;
        }

        Usart_485_Tx_Rp++;
        USART_ClearITPendingBit(Usart_485, USART_IT_TC);
        USART_ClearITPendingBit(Usart_485, USART_FLAG_TC);
    }
}

/******************************************END OF FILE**********************/
