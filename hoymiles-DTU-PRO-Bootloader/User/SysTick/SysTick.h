#ifndef __SYSTICK_H
#define __SYSTICK_H
#ifdef DTU3PRO

#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
extern __IO u32 LocalTime;

void SysTick_Init(void);
void Delay_us(uint32_t us);
void Delay_ms(uint32_t ms);
void Delay_s(uint32_t s);
void hardware_delay_ms(uint32_t time);
#endif /* __SYSTICK_H */
