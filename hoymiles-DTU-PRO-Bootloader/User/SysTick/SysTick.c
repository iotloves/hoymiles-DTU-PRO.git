#include "SysTick.h"

volatile u32 LocalTime = 0;

volatile int32_t SysTickDelayTime;

/***********************************************
** Function name:       SysTick_Init
** Descriptions:        启动系统滴答定时器 SysTick
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void SysTick_Init(void)
{
    /* SystemFrequency / 1000       1ms中断一次
     * SystemFrequency / 100000      10us中断一次
     * SystemFrequency / 1000000     1us中断一次
     */
    if(SysTick_Config(SystemCoreClock / 1000000))
    {
        /* Capture error */
    }
}
/***********************************************
** Function name:       TimingDelay_Decrement
** Descriptions:        获取节拍程序
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void TimingDelay_Decrement(void)
{
    static uint32_t times = 0;

    if(times > 999)
    {
        times = 0;

        if(LocalTime >= 0xFFFFFFFF)
        {
            LocalTime = 0;
        }
        else
        {
            LocalTime++;
        }
    }

    times++;

    if(SysTickDelayTime >= 0)
    {
        SysTickDelayTime--;
    }
}
/***********************************************
** Function name:       SysTick_Handler
** Descriptions:        系统滴答定时器中断处理函数
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void SysTick_Handler(void)
{
    TimingDelay_Decrement();
}

void Delay_us(uint32_t us)
{
    SysTickDelayTime = us;

    while(SysTickDelayTime > 0);
}

void Delay_ms(uint32_t ms)
{
    int32_t delay_ms = 0;
    delay_ms = ms;

    while(delay_ms > 0)
    {
        Delay_us(1000);
        delay_ms --;
    }
}


void Delay_s(uint32_t s)
{
    int32_t delay_s = 0;
    delay_s = s;

    while(delay_s > 0)
    {
        Delay_ms(1000);
        delay_s --;
    }
}

void hardware_delay_ms(uint32_t time)
{
    int32_t delay_ms = 0;
    int32_t i = 0;
    delay_ms = time;

    while(delay_ms > 0)
    {
        i = 10;
        delay_ms--;

        while(i > 0)
        {
            i--;
        }
    }
}
/*********************************************END OF FILE**********************/
