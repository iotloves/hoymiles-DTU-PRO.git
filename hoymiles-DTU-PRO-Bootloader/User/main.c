#include "stm32f4xx.h"
#include "led.h"
#include "ST_Flash.h"
#include "string.h"
#include "SysTick.h"
#include "malloc.h"
#include "Memory.h"
#include "crc16.h"
#include "usart.h"
#include "usbh_usr.h"
#include "ff.h"
#include "SysTick.h"
#include "DiskOperation.h"
#include "usb_bsp.h"
typedef  void (*pFunction)(void);

pFunction Jump_To_Application;

u32 JumpAddress;

extern uint8_t SysState;

//#define ApplicationAddress    0x08020200  //应用程序地址
#define ApplicationAddress      0x08010200  //应用程序地址
#ifdef DEBUG
#define USBWaitTime             100
#else
#define USBWaitTime             5000
#endif
/*
 2、if (((*(volatile u32*)ApplicationAddress) & 0x2FFE0000 ) == 0x20000000)分析：
ApplicationAddress存放的是用户程序Flash的首地址，(*(volatile u32*)ApplicationAddress)
的意思是取用户程序首地址里面的数据，这个数据就是用户代码的堆栈地址，堆栈地址指向RAM，
而RAM的起始地址是0x20000000，因此上面的判断语句执行：判断用户代码的堆栈地址是否落在:0x20000000~0x2001ffff区间中，
这个区间的大小为128K，笔者查阅STM32各型号的RAM大小，目前RAM最大的容量可以做到192K+4K，时钟频率为168MHZ。
一般情况下，我们使用的芯片较多的落在<128K RAM的区间，因此上面的判断语句是没有太大问题的。

3、经过2的分析，test保存的就是堆栈地址（并且是应用程序堆栈的栈顶地址），查看STM32的向量表，可以知道：
栈顶地址 + 4 存放的是复位地址，因此JumpAddress存放的是复位地址。

4、调用__set_MSP函数后，将把用户代码的栈顶地址设为栈顶指针

5、Jump_To_Application();的意思就是设置PC指针为复位地址。
*/
USBH_HOST  USB_Host;
USB_OTG_CORE_HANDLE  USB_OTG_Core;

uint8_t USB_Update_Link = 0;
uint32_t BootTime = 0;
extern SystemCfg system_cfg;
//用户测试主程序
//返回值:0,正常
//       1,有问题
u8 USH_User_App(void)
{
    u8 res = 0;
    uint32_t len = 0;

    if(HCD_IsDeviceConnected(&USB_OTG_Core))//设备连接成功
    {
        len = USB_Get_Program_Size();

        if(len > 0)
        {
            USB_Update_Link = 1;
        }
    }

    return res;
}

enum Boot_Cmd
{
    Boot_Start                  = 0x00,
    Boot_USB_Wait               = 0x01,
    Boot_USB_Comp               = 0x02,
    Boot_USB_Check              = 0x03,
    Boot_USB_Copy               = 0x04,
    Boot_HexCheck               = 0x11,
    Boot_Upgrade                = 0x12,
    Boot_AppCheck               = 0x20,
    Boot_JumpApp                = 0x21,

};

void Jump_Address(void)
{
    if(((*(volatile u32 *)ApplicationAddress) & 0x2FFE0000) == 0x20000000)
    {
        JumpAddress = (*(volatile u32 *)ApplicationAddress);
        JumpAddress = *(volatile u32 *)(ApplicationAddress + 4);
        Jump_To_Application = (pFunction) JumpAddress;
        __set_MSP(*(volatile u32 *) ApplicationAddress);
        RCC_DeInit();
        SysTick->CTRL = 0;
        SysTick->LOAD = 0;
        SysTick->VAL = 0;
        Jump_To_Application();
    }
}

//Upgrade升级
//Regular正常
int main(void)
{
    uint8_t AppError = 0;
    uint8_t ErrorTimes = 0;
    uint8_t CheckTimes = 0;
    uint16_t Boot_state = Boot_Start;
    uint32_t temp_err, i;
    uint32_t DelayTime;
    SysState = Sys_open;
    SystemInit();
    //设置系统中断优先级分组2
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    SysTick_Init();
        //初始化内部内存池
    my_mem_init();
    LED_Init();
    //挂载U盘
    USB_Mount(); 
    //初始化USB主机
    USBH_Init(&USB_OTG_Core, USB_OTG_FS_CORE_ID, &USB_Host, &USBH_MSC_cb, &USR_Callbacks);
    //spi_flash初始化
    SPI_FLASH_Init();
    Delay_ms(500);
    while(1)
    {
        USBH_Process(&USB_OTG_Core, &USB_Host);
        //判断是否直接启动bootloader
        if((LocalTime - DelayTime) >= 10)
        {
            switch(Boot_state)
            {
                case Boot_Start:
                    memset(system_cfg.Data, 0, sizeof(system_cfg));
                    EEPROM_BufferRead(system_cfg.Data, sizeof(system_cfg));

                    if(strncmp((char *)system_cfg.Property.UpgradeSign, "Upgrade", Upgrade_Len) == 0)
                    {
                        CheckTimes = 0;
                        Fs_Init(0);
                        SysState = Sys_updata;
                        Boot_state = Boot_HexCheck;
                    }
                    else if(strncmp((char *)system_cfg.Property.UpgradeSign, "Regular", Upgrade_Len) == 0)
                    {
                        BootTime = LocalTime;
                        ErrorTimes = 0;
                        Boot_state = Boot_USB_Wait;
                    }
                    else
                    {
                        memset(system_cfg.Data, 0, sizeof(system_cfg));
                        EEPROM_BufferRead(system_cfg.Data, sizeof(system_cfg));
                        EEPROM_Erase();
                        memset(system_cfg.Property.UpgradeSign, 0, sizeof(system_cfg.Property.UpgradeSign));
                        sprintf((char *)system_cfg.Property.UpgradeSign, "Regular");
                        EEPROM_BufferWrite(system_cfg.Data, sizeof(system_cfg));
                        BootTime = LocalTime;
                        ErrorTimes = 0;
                        USBH_USR_DeInit();
                        USB_Update_Link = 0;
                        Boot_state = Boot_USB_Wait;
                    }

                    break;

                case Boot_USB_Wait:
                    if(((LocalTime - BootTime) >= USBWaitTime) || (USB_Update_Link == 1))
                    {
                        if(USB_Update_Link == 1)
                        {
                            Fs_Init(0);
                            Boot_state = Boot_USB_Comp;
                        }
                        else
                        {
                            if(AppError == 1)
                            {
                                ErrorTimes ++;
                                BootTime = LocalTime;

                                if(ErrorTimes >= 20)
                                {
                                    Boot_state = Boot_HexCheck;
                                }
                            }
                            else
                            {
                                Boot_state = Boot_AppCheck;
                            }
                        }
                    }

                    break;

                case Boot_USB_Comp:
                    if(USB_Update_Link == 1)
                    {
                        if(USB_Comparison() == 0)
                        {
                            Boot_state = Boot_USB_Check;
                        }
                        else
                        {
                            if(AppError == 1)
                            {
                                Boot_state = Boot_USB_Check;
                            }
                            else
                            {
                                Boot_state = Boot_AppCheck;
                            }
                        }

                        BootTime = LocalTime;
                        USBH_USR_DeInit();
                        USB_Update_Link = 0;
                    }
                    else if((LocalTime - BootTime) >= USBWaitTime)
                    {
                        Boot_state = Boot_AppCheck;
                    }

                    break;

                case Boot_USB_Check:
                    if(USB_Update_Link == 1)
                    {
                        if(USB_DTU_Program_Check(SerialNum) == 1)
                        {
                            SysState = Sys_updata;
                            Boot_state = Boot_USB_Copy;
                        }
                        else
                        {
                            if(AppError == 1)
                            {
                                BootTime = LocalTime;
                                ErrorTimes = 0;
                                Boot_state = Boot_HexCheck;
                            }
                            else
                            {
                                Boot_state = Boot_AppCheck;
                            }
                        }

                        BootTime = LocalTime;
                        USBH_USR_DeInit();
                        USB_Update_Link = 0;
                    }
                    else if((LocalTime - BootTime) >= USBWaitTime)
                    {
                        Boot_state = Boot_AppCheck;
                    }

                    break;

                case Boot_USB_Copy:
                    if(USB_Update_Link == 1)
                    {
                        if(USB_DTU_Program_Copy() == 1)
                        {
                            SysState = Sys_updata;
                            CheckTimes = 0;
                            Boot_state = Boot_HexCheck;
                        }
                        else
                        {
                            Boot_state = Boot_USB_Comp;
                        }

                        BootTime = LocalTime;
                        USBH_USR_DeInit();
                        USB_Update_Link = 0;
                    }
                    else if((LocalTime - BootTime) >= USBWaitTime)
                    {
                        Boot_state = Boot_AppCheck;
                    }

                    break;

                case Boot_HexCheck:
                    if(CheckTimes < 5)
                    {
                        SysState = Sys_updata;

                        if(DTU_Program_Check(SerialNum) == 1)
                        {
                            Boot_state = Boot_Upgrade;
                        }

                        CheckTimes ++;
                    }
                    else
                    {
                        memset(system_cfg.Data, 0, sizeof(system_cfg));
                        EEPROM_BufferRead(system_cfg.Data, sizeof(system_cfg));
                        EEPROM_Erase();
                        memset(system_cfg.Property.UpgradeSign, 0, sizeof(system_cfg.Property.UpgradeSign));
                        sprintf((char *)system_cfg.Property.UpgradeSign, "Regular");
                        EEPROM_BufferWrite(system_cfg.Data, sizeof(system_cfg));
                        CheckTimes = 0;
                        Boot_state = Boot_AppCheck;
                    }

                    break;

                case Boot_Upgrade:
                    if(IAP_Myprogram(FLASH_ADDR_APP_CRC, FLASH_ADDR_APP_END, 0) == 1)
                    {
                        memset(system_cfg.Data, 0, sizeof(system_cfg));
                        EEPROM_BufferRead(system_cfg.Data, sizeof(system_cfg));
                        EEPROM_Erase();
                        memset(system_cfg.Property.UpgradeSign, 0, sizeof(system_cfg.Property.UpgradeSign));
                        sprintf((char *)system_cfg.Property.UpgradeSign, "Regular");
                        EEPROM_BufferWrite(system_cfg.Data, sizeof(system_cfg));
                        CheckTimes = 0;
                        Boot_state = Boot_AppCheck;
                    }
                    else
                    {
                        Boot_state = Boot_HexCheck;
                    }

                    break;

                case Boot_AppCheck:
                    if(CheckTimes < 5)
                    {
                        if(CheckProgramB(0) == 1) //校验应用程序
                        {
                            Boot_state = Boot_JumpApp;
                        }

                        CheckTimes ++;
                    }
                    else
                    {
                        CheckTimes = 0;
                        SysState = Sys_Error;
                        AppError = 1;
                        ErrorTimes = 0;
                        BootTime = LocalTime;
                        USBH_USR_DeInit();
                        USB_Update_Link = 0;
                        Boot_state = Boot_USB_Wait;
                    }

                    break;

                case Boot_JumpApp:
                    USB_Host.gState = HOST_DEV_DISCONNECTED;
                    USBH_Process(&USB_OTG_Core, &USB_Host);
                    USB_OTG_BSP_DisableInterrupt();
                    __disable_irq() ;       //可以使用这个函数 关闭总中断
                    Jump_Address();         //如果正常 直接跳转到用户程序
                    break;
            }

            DelayTime = LocalTime;
        }
    }
}
