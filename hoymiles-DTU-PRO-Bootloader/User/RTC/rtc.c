#include "rtc.h"
#include "led.h"
#include "usart.h"
#include "systick.h"
// 时钟源宏定义

#define RTC_CLOCK_SOURCE_LSE

//#define RTC_CLOCK_SOURCE_LSI



// 异步分频因子

#define ASYNCHPREDIV 0X7F

// 同步分频因子

#define SYNCHPREDIV 0XFF
// 时间宏定义

#define RTC_H12_AMorPM RTC_H12_AM

#define HOURS 1 // 0~23

#define MINUTES 1 // 0~59

#define SECONDS 1 // 0~59



// 日期宏定义

#define WEEKDAY 1 // 1~7

#define DATE 1 // 1~31

#define MONTH 1 // 1~12

#define YEAR 1 // 0~99



// 时间格式宏定义

#define RTC_Format_BINorBCD RTC_Format_BIN


// 备份域寄存器宏定义

#define RTC_BKP_DRX RTC_BKP_DR0

// 写入到备份寄存器的数据宏定义

#define RTC_BKP_DATA 0X32F2


u8 RTC_Get_Week(uint16_t year, u8 month, u8 day);
//RTC时间设置
//hour,min,sec:小时,分钟,秒钟
//ampm:@RTC_AM_PM_Definitions  :RTC_H12_AM/RTC_H12_PM
//返回值:SUCEE(1),成功
//       ERROR(0),进入初始化模式失败
ErrorStatus RTC_Set_Time(u8 hour, u8 min, u8 sec, u8 ampm)
{
    RTC_TimeTypeDef RTC_TimeTypeInitStructure;
    RTC_TimeTypeInitStructure.RTC_Hours = hour;
    RTC_TimeTypeInitStructure.RTC_Minutes = min;
    RTC_TimeTypeInitStructure.RTC_Seconds = sec;
    RTC_TimeTypeInitStructure.RTC_H12 = ampm;
    return RTC_SetTime(RTC_Format_BIN, &RTC_TimeTypeInitStructure);
}
//RTC日期设置
//year,month,date:年(0~99),月(1~12),日(0~31)
//week:星期(1~7,0,非法!)
//返回值:SUCEE(1),成功
//       ERROR(0),进入初始化模式失败
ErrorStatus RTC_Set_Date(u8 year, u8 month, u8 date, u8 week)
{
    RTC_DateTypeDef RTC_DateTypeInitStructure;
    RTC_DateTypeInitStructure.RTC_Date = date;
    RTC_DateTypeInitStructure.RTC_Month = month;
    RTC_DateTypeInitStructure.RTC_WeekDay = week;
    RTC_DateTypeInitStructure.RTC_Year = year;
    return RTC_SetDate(RTC_Format_BIN, &RTC_DateTypeInitStructure);
}

//RTC初始化
//返回值:0,初始化成功;
//       1,LSE开启失败;
//       2,进入初始化模式失败;
void RTC_SetTimes(calendar_obj calendar)
{
    uint8_t RTC_H12;

    if(calendar.hour > 12)
    {
        RTC_H12 = RTC_H12_PM;
    }
    else
    {
        RTC_H12 = RTC_H12_AM;
    }

    RTC_Set_Date((calendar.w_year - 1980), calendar.w_month, calendar.w_date, RTC_Get_Week(calendar.w_year, calendar.w_month, calendar.w_date));   //设置日期
    RTC_Set_Time(calendar.hour, calendar.min, calendar.sec, RTC_H12); //设置时间
}

u8 RTC_Initialize(calendar_obj calendar)
{
    uint8_t RTC_H12;
    RTC_InitTypeDef RTC_InitStructure;
    uint16_t retry = 0X1FFF;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);//使能PWR时钟
    PWR_BackupAccessCmd(ENABLE);    //使能后备寄存器访问

    if(RTC_ReadBackupRegister(RTC_BKP_DR0) != 0x5050)      //是否第一次配置?
    {
        if(calendar.hour > 12)
        {
            RTC_H12 = RTC_H12_PM;
        }
        else
        {
            RTC_H12 = RTC_H12_AM;
        }

        RCC_LSEConfig(RCC_LSE_ON);//LSE 开启

        while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)     //检查指定的RCC标志位设置与否,等待低速晶振就绪
        {
            retry++;
        }

        if(retry == 0)
        {
            return 1;    //LSE 开启失败.
        }

        RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);        //设置RTC时钟(RTCCLK),选择LSE作为RTC时钟
        RCC_RTCCLKCmd(ENABLE);    //使能RTC时钟
        RTC_InitStructure.RTC_AsynchPrediv = 0x7F;//RTC异步分频系数(1~0X7F)
        RTC_InitStructure.RTC_SynchPrediv  = 0xFF;//RTC同步分频系数(0~7FFF)
        RTC_InitStructure.RTC_HourFormat   = RTC_HourFormat_24;//RTC设置为,24小时格式
        RTC_Init(&RTC_InitStructure);
        RTC_Set_Time(calendar.hour, calendar.min, calendar.sec, RTC_H12); //设置时间
        RTC_Set_Date((calendar.w_year - 1980), calendar.w_month, calendar.w_date, RTC_Get_Week(calendar.w_year, calendar.w_month, calendar.w_date));   //设置日期
        RTC_WriteBackupRegister(RTC_BKP_DR0, 0x5050);   //标记已经初始化过了
    }

    return 0;
}


void RTC_GetTimes(calendar_obj *calendar)
{
    RTC_DateTypeDef RTC_DateStruct;
    RTC_TimeTypeDef RTC_TimeStruct;
    RTC_GetTime(RTC_Format_BIN, &RTC_TimeStruct);
    RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
    calendar->w_year = RTC_DateStruct.RTC_Year + 1980;
    calendar->w_month = RTC_DateStruct.RTC_Month;
    calendar->w_date = RTC_DateStruct.RTC_Date;
    calendar->week = RTC_DateStruct.RTC_WeekDay;
    calendar->hour = RTC_TimeStruct.RTC_Hours;
    calendar->min = RTC_TimeStruct.RTC_Minutes;
    calendar->sec = RTC_TimeStruct.RTC_Seconds;
    //    if(RTC_TimeStruct.RTC_H12 == RTC_H12_PM)
    //    {
    //        calendar->hour += 12;
    //    }
}

u8 const table_week[12] = { 0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 }; //月修正数据表
//平年的月份日期表
const u8 mon_table[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//判断是否是闰年函数
//月份   1  2  3  4  5  6  7  8  9  10 11 12
//闰年   31 29 31 30 31 30 31 31 30 31 30 31
//非闰年 31 28 31 30 31 30 31 31 30 31 30 31
//输入:年份
//输出:该年份是不是闰年.1,是.0,不是
u8 Is_Leap_Year(uint16_t year)
{
    if(year % 4 == 0)  //必须能被4整除
    {
        if(year % 100 == 0)
        {
            if(year % 400 == 0)
            {
                return 1;    //如果以00结尾,还要能被400整除
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 0;
    }
}

//获得现在是星期几
//功能描述:输入公历日期得到星期(只允许1901-2099年)
//输入参数：公历年月日
//返回值：星期号
u8 RTC_Get_Week(uint16_t year, u8 month, u8 day)
{
    uint16_t temp2;
    u8 yearH, yearL;
    yearH = year / 100;
    yearL = year % 100;

    // 如果为21世纪,年份数加100
    if(yearH > 19)
    {
        yearL += 100;
    }

    // 所过闰年数只算1900年之后的
    temp2 = yearL + yearL / 4;
    temp2 = temp2 % 7;
    temp2 = temp2 + day + table_week[month - 1];

    if(yearL % 4 == 0 && month < 3)
    {
        temp2--;
    }

    return(temp2 % 7);
}

//日期转绝对时间秒
//功能描述:输入公历年月日时分秒得到绝对时间秒
//输入参数：公历年月日时分秒
//返回值：秒
uint32_t DateToSec(calendar_obj calendar)
{
    uint16_t t;
    uint32_t seccount = 0;

    if(calendar.w_year < 1970 || calendar.w_year > 2099)
    {
        return 1;
    }

    for(t = 1970; t < calendar.w_year; t++)          //把所有年份的秒钟相加
    {
        if(Is_Leap_Year(t))
        {
            seccount += 31622400;                   //闰年的秒钟数
        }
        else
        {
            seccount += 31536000;                   //平年的秒钟数
        }
    }

    calendar.w_month -= 1;

    for(t = 0; t < calendar.w_month; t++)            //把前面月份的秒钟数相加
    {
        seccount += (uint32_t)mon_table[t] * 86400; //月份秒钟数相加

        if(Is_Leap_Year(calendar.w_year) && t == 1)
        {
            seccount += 86400;                      //闰年2月份增加一天的秒钟数
        }
    }

    seccount += (uint32_t)(calendar.w_date - 1) * 86400; //把前面日期的秒钟数相加
    seccount += (uint32_t)calendar.hour * 3600;     //小时秒钟数
    seccount += (uint32_t)calendar.min * 60;        //分钟秒钟数
    seccount += calendar.sec;                       //最后的秒钟加上去
    return seccount;
}
//绝对时间秒转日期
//功能描述:绝对时间秒
//输入参数：
//返回值：公历年月日时分秒
void SecToDate(uint32_t Sec, calendar_obj *calendar)
{
    static uint16_t daycnt = 0;
    uint32_t temp = 0;
    uint16_t temp1 = 0;
    temp = Sec / 86400;                             //得到天数(秒钟数对应的)
    daycnt = temp;
    temp1 = 1970;                                   //从1970年开始

    while(temp >= 365)
    {
        if(Is_Leap_Year(temp1))                      //是闰年
        {
            if(temp >= 366)
            {
                temp -= 366;                        //闰年的秒钟数
            }
            else
            {
                temp1++;
                break;
            }
        }
        else
        {
            temp -= 365;                            //平年
        }

        temp1++;
    }

    calendar->w_year = temp1;                       //得到年份
    temp1 = 0;

    while(temp >= 28)                                //超过了一个月
    {
        if(Is_Leap_Year(calendar->w_year) && temp1 == 1)  //当年是不是闰年/2月份
        {
            if(temp >= 29)
            {
                temp -= 29;                         //闰年的秒钟数
            }
            else
            {
                break;
            }
        }
        else
        {
            if(temp >= mon_table[temp1])
            {
                temp -= mon_table[temp1];           //平年
            }
            else
            {
                break;
            }
        }

        temp1++;
    }

    calendar->w_month = temp1 + 1;                  //得到月份
    calendar->w_date = temp + 1;                    //得到日期
    temp = Sec % 86400;                             //得到秒钟数
    calendar->hour = temp / 3600;                   //小时
    calendar->min = (temp % 3600) / 60;             //分钟
    calendar->sec = (temp % 3600) % 60;             //秒钟
    calendar->week = RTC_Get_Week(calendar->w_year, calendar->w_month, calendar->w_date); //获取星期
}
uint32_t RTC_Getsecond(void)
{
    calendar_obj calendar;
    RTC_GetTimes(&calendar);
    return DateToSec(calendar);
}

void RTC_Setsecond(uint32_t Second)
{
    calendar_obj calendar;
    uint8_t RTC_H12;
    SecToDate(Second, &calendar);

    if(calendar.hour > 12)
    {
        RTC_H12 = RTC_H12_PM;
    }
    else
    {
        RTC_H12 = RTC_H12_AM;
    }

    RTC_Set_Time(calendar.hour, calendar.min, calendar.sec, RTC_H12); //设置时间
    RTC_Set_Date((calendar.w_year - 1980), calendar.w_month, calendar.w_date, calendar.week);   //设置日期
}
uint32_t RTC_GetWorldSecond(int32_t timezone)
{
    uint32_t time = 0;
    time = RTC_Getsecond();
    return (time + timezone);
}
void RTC_GetWorldTime(calendar_obj *calendar, int32_t timezone)
{
    SecToDate(RTC_GetWorldSecond(timezone), calendar);
}