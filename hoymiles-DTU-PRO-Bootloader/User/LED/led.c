#include "led.h"

#define Overload            2000-1//200ms
#define FrequencyDivision   ((84000000/10000)-1)


u8 connect_net = 0;     //网络标志位  0:未连接网络  1:连接网络
u8 connect_server = 0;  //连接服务器标志位 0:未连接服务器 1:连接服务器
u8 server_data = 0;     //有服务器数据 0:无数据 1:有数据

uint8_t DTUError = 0;
uint8_t MIError = 0;
uint8_t MeterError = 0;

void led_time_init(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);                    //使能TIM3时钟
    TIM_TimeBaseInitStructure.TIM_Period = Overload;                        //自动重装载值
    TIM_TimeBaseInitStructure.TIM_Prescaler = FrequencyDivision;            //定时器分频
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;         //向上计数模式
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);                     //初始化TIM3
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);                              //允许定时器3更新中断
    TIM_Cmd(TIM3, ENABLE);                                                  //使能定时器3
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;                         //定时器3中断
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;            //抢占优先级1
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;                   //子优先级3
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void LED_Init(void)
{
    /**
    * LED_RUN           PA0
    * LED_2.4G          PA4
    * LED_GPRS          PA5
    * LED_Internet      PA6
    * LED_Internet      PA8
    **/
    GPIO_InitTypeDef  GPIO_InitStructure;
    RCC_AHB1PeriphClockCmd(LED0_GPIO_CLK | LED1_GPIO_CLK | LED2_GPIO_CLK | LED3_GPIO_CLK | LED4_GPIO_CLK, ENABLE);//使能GPIOF时钟
    //GPIOF9,F10初始化设置
    GPIO_InitStructure.GPIO_Pin = LED0_GPIO_PIN | LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN | LED4_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;                           //普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;                          //推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;                      //100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;                            //上拉
    GPIO_Init(LED0_GPIO_PORT, &GPIO_InitStructure);                         //初始化
    GPIO_SetBits(LED4_GPIO_PORT, LED0_GPIO_PIN | LED1_GPIO_PIN | LED2_GPIO_PIN | LED3_GPIO_PIN | LED4_GPIO_PIN); //设置高，灯灭
    led_time_init();
}

uint32_t cnt = 0;
uint8_t SysState = 0;//Sys_open;
uint8_t NetState = 0;
uint8_t MiState = 0;
uint8_t ModeState = 0;
uint8_t FaultState = 0;
uint8_t Sys_open_cnt = 0;
uint8_t Sys_updata_cnt = 0;
uint8_t Net_cnt = 0;
uint8_t Mi_cnt = 0;
uint8_t Mode_cnt = 0;
uint8_t Fault_cnt = 0;
void led_Status_polling(void)
{
    switch(SysState)
    {
        case Sys_open:
            if(cnt % 3 == 0)
            {
                switch(Sys_open_cnt)
                {
                    case 0:
                        LED0_ON;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 1:
                        LED0_OFF;
                        LED1_ON;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 2:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_ON;
                        LED3_OFF;
                        break;

                    case 3:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_ON;
                        break;
                }

                Sys_open_cnt = (Sys_open_cnt + 1) % 4;
            }

            break;

        case Sys_updata:
            if(cnt % 1 == 0)
            {
                switch(Sys_updata_cnt)
                {
                    case 0:
                        LED0_ON;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 1:
                        LED0_OFF;
                        LED1_ON;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 2:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_ON;
                        LED3_OFF;
                        break;

                    case 3:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_ON;
                        break;
                }

                Sys_updata_cnt = (Sys_updata_cnt + 1) % 4;
            }

            break;

        case Sys_Error:
            if(cnt % 1 == 0)
            {
                switch(Sys_updata_cnt)
                {
                    case 0:
                        LED0_OFF;
                        LED1_OFF;
                        LED2_OFF;
                        LED3_OFF;
                        break;

                    case 1:
                        LED0_ON;
                        LED1_ON;
                        LED2_ON;
                        LED3_ON;
                        break;
                }

                Sys_updata_cnt = (Sys_updata_cnt + 1) % 2;
            }

            break;
    }
}
//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
    //溢出中断
    if(TIM_GetITStatus(TIM3, TIM_IT_Update) == SET)
    {
        //清除中断标志位
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
        led_Status_polling();
        cnt++;

        if(cnt >= 0xFFFFFFFF)
        {
            cnt = 0;
        }
    }
}
