#ifndef __LITTLE_H
#define __LITTLE_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#include "lfs.h"
/*open flags
*LFS_O_RDONLY            //以只读方式打开文件
*LFS_O_WRONLY            //以只写方式打开文件
*LFS_O_RDWR              //以读写方式打开文件
*LFS_O_CREAT             //如果文件不存在，则创建一个文件
*LFS_O_EXCL              // 如果文件已存在则失败
*LFS_O_TRUNC             // 将现有文件截断为零大小
*LFS_O_APPEND            // 每次写入时移动到文件末尾
*/
#define FS_TEST(x)  printf(x);

int lf_open(uint8_t DiskNum, lfs_file_t *file, const char *path, int flags);                         /* Open or create a file */
int lf_close(uint8_t DiskNum, lfs_file_t *file);                                                     /* Close an open file object */
int lf_read(uint8_t DiskNum, lfs_file_t *file, void *buffer, uint32_t size, uint32_t *br);           /* Read data from a file */
int lf_write(uint8_t DiskNum, lfs_file_t *file, const void *buffer, uint32_t size, uint32_t *bw);    /* Write data to a file */
int lf_sync(uint8_t DiskNum, lfs_file_t *file);
int lf_seek(uint8_t DiskNum, lfs_file_t *file, uint32_t off);                                        /* Move file pointer of a file object */
int lf_tell(uint8_t DiskNum, lfs_file_t *file);
int lf_seekend(uint8_t DiskNum, lfs_file_t *file);
uint32_t lf_fsize(uint8_t DiskNum, lfs_file_t *file);
int lf_opendir(uint8_t DiskNum, lfs_dir_t *dir, const char *path);                                   /* Open a directory */
int lf_closedir(uint8_t DiskNum, lfs_dir_t *dir);                                                    /* Close an open directory */
int lf_readdir(uint8_t DiskNum, lfs_dir_t *dir, struct lfs_info *info);                              /* Read a directory item */
int lf_mkdir(uint8_t DiskNum, const char *path);                                                     /* Create a sub directory */
int lf_unlink(uint8_t DiskNum, const char *path);                                                    /* Delete an existing file or directory */
int lf_rename(uint8_t DiskNum, const char *oldpath, const char *newpath);                            /* Rename/Move a file or directory */
int lf_stat(uint8_t DiskNum, const char *path, struct lfs_info *info);                               /* Get file status */
int lf_mount(uint8_t DiskNum);                                                       /* Mount a logical drive */
int lf_format(uint8_t DiskNum);
int lf_unmount(uint8_t DiskNum);                                                     /* Unmount a logical drive */
//打印所有文件
void lf_PrintfAllFiles(uint8_t DiskNum, char *path);
//删除目录下所有内容
void lf_DeleteAllFiles(uint8_t DiskNum, char *path);
//目录大小
void lf_DirectorySize(uint8_t DiskNum, char *path, uint32_t *Size, uint16_t *Num);
//剩余容量
void lf_RemainingCapacity(uint8_t DiskNum, char *path, uint32_t *capacity, uint16_t *Num);

//罗列所有文件
void lf_ListAllFiles(uint8_t DiskNum, char *path, char *FileName[], uint16_t *Num, uint16_t Total);
int lfs_test(void);
void lfs_test_demo(void);
uint32_t lf_Total_Capacity(uint8_t DiskNum);
#endif