#include "DiskOperation.h"
#include "memory_tools.h"
#include "Memory.h"
#include "malloc.h"
#include "rtc.h"
#include "string.h"
#include "stdbool.h"
#include "usart.h"
#include "systick.h"
#include "ff.h"
#include "crc16.h"
#include "stdio.h"
//设备状态标准 0：无设备
#define USB_Drive_Letter                0
#define HeadLen                         4
//FatFs文件系统对象
FATFS   fs;
// 文件对象
FIL     fnew;
FILINFO fno;
uint8_t FatfsDisk = 0;

char headbuf[4] = {"\r\n\r\n"};

uint8_t USB_Mount(void)
{
    uint8_t i;
    char *path;
    FRESULT res;                //文件操作结果
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", USB_Drive_Letter);
    /*挂载设备*/
    res = f_mount(&fs, path, 1); //挂载U盘
    myfree(path);
    return res;
}


uint32_t USB_Get_Free(uint32_t *total, uint32_t *free)
{
    char *path;
    FATFS *pfs;
    DWORD fre_clust = 0;
    FRESULT res;//文件操作结果
    uint32_t fre_sect = 0;
    uint32_t tot_sect = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", USB_Drive_Letter);
    /* 获取设备信息和空簇大小 */
    res = f_getfree((char *)path, &fre_clust, &pfs);

    if(res == 0)
    {
        //得到总扇区数
        tot_sect = (pfs->n_fatent - 2) * pfs->csize;
        //得到空闲扇区数
        fre_sect = fre_clust * pfs->csize;
        //扇区大小不是512字节,则转换为512字节
#if FF_MAX_SS!=512
        tot_sect *= pfs->ssize / 512;
        fre_sect *= pfs->ssize / 512;
#endif
        //单位为KB
        *total = tot_sect >> 1;
        //单位为KB
        *free = fre_sect >> 1;
    }

    myfree(path);
    return res;
}

uint32_t USB_Get_Program_Size(void)
{
    char *path;
    FRESULT res;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Update/Update.hex", USB_Drive_Letter);
    /* 获取文件信息 */
    res = f_stat(path, &fno);

    if(res == FR_OK)
    {
        myfree(path);
        return (long)fno.fsize;
    }
    else
    {
        myfree(path);
        return 0;
    }
}

uint32_t USB_Read_Program(char *buffer, uint32_t Offset, uint32_t len)
{
    //文件操作结果
    FRESULT res;
    char *path;
    uint8_t i = ERROR;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Update/Update.hex", USB_Drive_Letter);
    res = f_open(&fnew, path, FA_OPEN_EXISTING | FA_READ);

    if(res == FR_OK)
    {
        res = f_lseek(&fnew, Offset);
        res = f_read(&fnew, buffer, len, &fnum);
    }

    f_close(&fnew);
    myfree(path);
    return fnum;
}


/***********************************************
** Function name:       USB_DTU_Program_Check
** Descriptions:        DTU升级程序校验
** input parameters:    硬件版本号
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
uint8_t USB_DTU_Program_Check(u16 Serial)
{
    uint8_t serialnum[2] = {0};
    serialnum[0] = Serial >> 8;
    serialnum[1] = Serial;
    char *buffer;
    char Cache[MAX_ONE_LINE_LEN];
    uint8_t CRC16Hi, CRC16Lo;
    uint8_t temp_crc;
    uint8_t SaveHi, SaveLo;
    uint8_t x;
    uint8_t *ptr;
    uint8_t len = 0;
    uint8_t cmd = 0;
    uint8_t counter;
    uint8_t Oneline[MAX_ONE_LINE_LEN];
    uint8_t StartLine[14];
    uint8_t linelen = 0;
    uint8_t perused;
    uint16_t LineNum = 0;
    uint16_t timeout = 0;
    uint16_t crc = 0, CRC_ResultC = 0, CRC_FW = 0;
    uint32_t y;
    uint32_t i = 0, j = 0;
    uint32_t offset = 0;
    uint32_t address = 0;
    uint32_t length = 0;
    uint32_t BufSize = 4096;
    uint8_t Start_flag = 1;
    perused = my_mem_perused();
    length = USB_Get_Program_Size();

    if(length == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (uint32_t)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 20))) / 32) * 32;

    if(length < BufSize)
    {
        BufSize = length;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    uint32_t time = 0;
    CRC16Hi = 0xFF;
    CRC16Lo = 0xFF;
    buffer = mymalloc(BufSize);
    memset(buffer, 0, BufSize);
    length = 0;

    while(1)
    {
#ifndef DTU3PRO
        stm32_wdg_feed(); //喂狗
#endif

        if((length - i) <= 100)
        {
            offset += i;
            memset(buffer, 0, BufSize);
            length = USB_Read_Program(buffer, offset, BufSize);
            i = 0;
        }

        while(i <= BufSize)
        {
            if(strncmp(&buffer[i], "\r\n:", 3) == 0)   //\r\n:            除第一行外的每行开始
            {
                timeout = 0;
                i = i + 3;
                LineNum ++;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset(Oneline, 0, sizeof(Oneline));
                Ascii2Hex((uint8_t *)Cache, Oneline, j);
                crc = line_crc(Oneline, j);

                if(Oneline[j / 2 - 1] != crc)
                {
                    myfree(buffer);
                    return 0;
                }

                if((Oneline[0] == 0x00) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x01) && (Oneline[4] == 0xff))  //结尾行
                {
                    if(CRC_FW == CRC_ResultC)
                    {
                        myfree(buffer);
                        return 1;
                    }
                    else
                    {
                        myfree(buffer);
                        return 0;
                    }
                }

                if(LineNum == 2)
                {
                    StartLine[0] = Oneline[5];          //开始地址  32
                    StartLine[1] = Oneline[4];
                    StartLine[2] = Oneline[7];
                    StartLine[3] = Oneline[6];
                    StartLine[4] = Oneline[9];          //结束地址  32
                    StartLine[5] = Oneline[8];
                    StartLine[6] = Oneline[11];
                    StartLine[7] = Oneline[10];
                    StartLine[8] = Oneline[13];         //文件校验  16
                    StartLine[9] = Oneline[12];
                    StartLine[10] = Oneline[15];        //硬件版本号    16
                    StartLine[11] = Oneline[14];
                    StartLine[12] = Oneline[17];        //自校验    16
                    StartLine[13] = Oneline[16];
                    CRC_ResultC = 0xFFFF;
                    CRC_ResultC = CalcCRC16((uint16_t *)StartLine, 12);

                    if((serialnum[0] == StartLine[10]) && (serialnum[1] == StartLine[11]))     //硬件版本校验
                    {
                        if(CRC_ResultC == ((StartLine[12] << 8) + StartLine[13])) //自校验通过  则开始文件校验
                        {
                            CRC_FW = (StartLine[8] << 8) + StartLine[9];
                        }
                        else
                        {
                            myfree(buffer);
                            return 0;
                        }
                    }
                    else                                //硬件版本不对
                    {
                        myfree(buffer);
                        return 0;
                    }
                }
                else if(LineNum >= 4)
                {
                    len = *(Oneline + 0);               //长度
                    address = ((*(Oneline + 1)) << 8) + (*(Oneline + 2)); //地址
                    cmd = *(Oneline + 3);               //命令

                    if(cmd == 00)                       //数据
                    {
                        ptr = Oneline + 4;

                        for(x = 0; x < len; x++)
                        {
                            CRC16Lo = CRC16Lo ^ (*(ptr + x));

                            for(y = 0; y < 8; y++)
                            {
                                SaveHi = CRC16Hi;
                                SaveLo = CRC16Lo;
                                CRC16Hi = CRC16Hi / 2;          //高位右移一位
                                CRC16Lo = CRC16Lo / 2;          //低位右移一位

                                if((SaveHi & 0x01) == 0x01)     //如果高位字节最后一位为1
                                {
                                    CRC16Lo = CRC16Lo | 0x80;   //则低位字节右移后前面补1
                                }                               //否则自动补0

                                if((SaveLo & 0x01) == 0x01)
                                {
                                    CRC16Hi = CRC16Hi ^ 0xa0;   //如果LSB为1，则与多项式码进行异或
                                    CRC16Lo = CRC16Lo ^ 0x01;
                                }
                            }
                        }

                        CRC_ResultC = (uint16_t)(CRC16Hi << 8 | CRC16Lo);
                    }
                }

                i = i + j;
                break;
            }
            else if(Start_flag == 1)
            {
                if((strncmp(&buffer[i], "\r\n\r\n:", 5) == 0) || (strncmp(&buffer[i], ":", 1) == 0)) //程序总开始
                {
                    if(strncmp(&buffer[i], "\r\n\r\n:", 5) == 0)
                    {
                        i = i + 5;
                    }
                    else if(strncmp(&buffer[i], ":", 1) == 0)
                    {
                        i = i + 1;
                    }

                    Start_flag = 0;
                    timeout = 0;
                    LineNum ++;
                    memset(Cache, 0, sizeof(Cache));

                    for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                    {
                        if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                                ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                        {
                            Cache[j] = buffer[i + j];
                        }
                        else
                        {
                            break;                                  //下载程序结束  没找到程序头
                        }
                    }

                    memset(Oneline, 0, sizeof(Oneline));
                    Ascii2Hex((uint8_t *)Cache, Oneline, j);
                    crc = line_crc(Oneline, j);

                    if(Oneline[j / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return 0;
                    }

                    i = i + j;
                    break;
                }
                else
                {
                    i++;
                }
            }
        }

        memset(Cache, 0, sizeof(Cache));
        timeout++;

        if(timeout > 5000)
        {
            myfree(buffer);
            return 0 ;
        }
    }

    myfree(buffer);
}

uint8_t USB_DTU_Program_Copy(void)
{
    char *buffer;
    uint8_t perused;
    uint32_t i = 0;
    uint32_t offset = 0;
    uint32_t ProgramLen = 0;
    uint32_t length = 0;
    uint32_t BufSize = 4096;
    uint32_t WriteLen = 0;
    perused = my_mem_perused();
    ProgramLen = USB_Get_Program_Size();

    if(ProgramLen == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (uint32_t)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 20))) / 32) * 32;

    if(ProgramLen < BufSize)
    {
        BufSize = ProgramLen;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    buffer = mymalloc(BufSize);
    memset(buffer, 0, BufSize);
    Download_DTUProgram_Delete();
    WriteLen = DTU_Program_Write(headbuf, offset, HeadLen);

    for(i = 0; i < ProgramLen;)
    {
        memset(buffer, 0, BufSize);
        length = USB_Read_Program(buffer, offset, BufSize);

        if(length != 0)
        {
            WriteLen = DTU_Program_Write(buffer, offset + HeadLen, length);

            if(WriteLen != length)
            {
                Download_DTUProgram_Delete();
                myfree(buffer);
                return 0;
            }

            offset = offset + WriteLen;
            i = i + WriteLen;
        }
        else
        {
            Download_DTUProgram_Delete();
            myfree(buffer);
            return 0;
        }
    }

    myfree(buffer);
    return 1;
}



uint8_t USB_Comparison(void)
{
    char *USBBuffer;
    char *MemoryBuffer;
    uint32_t offset = 0;
    uint32_t BufSize = 512;
    USBBuffer = mymalloc(BufSize);
    memset(USBBuffer, 0, BufSize);
    MemoryBuffer = mymalloc(BufSize);
    memset(MemoryBuffer, 0, BufSize);
    USB_Read_Program(USBBuffer, offset, BufSize);
    DTU_Program_Read_Length(MemoryBuffer, offset + HeadLen, BufSize);

    if(strncmp(USBBuffer, MemoryBuffer, BufSize) == 0)
    {
        myfree(USBBuffer);
        myfree(MemoryBuffer);
        return 1;
    }

    myfree(USBBuffer);
    myfree(MemoryBuffer);
    return 0;
}





///**
//* @brief  清除下载文件夹中的内容
//* @param  无
//* @retval 返回接收到的数据
//*/
//uint8_t ClearDownloadFile(void)
//{
//    DIR dir;
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    MIProgram_Offset = 0;
//    DTUProgram_Offset = 0;
//    GridProfilesList_Offset = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[2] == DISKOK)
//    {
//        i = 2;
//    }
//    else if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Download", i);
//        res = f_opendir(&dir, data);

//        if(res != FR_OK)
//        {
//            /* 打开目录失败，就创建目录 */
//            res = f_mkdir(data);
//        }
//        else
//        {
//            /* 如果目录已经存在，关闭它 */
//            res = f_closedir(&dir);
//            sprintf(data, "%d:/Download/GridProfilesList.txt", i);
//            res = f_stat(data, 0);

//            if(res == FR_OK)
//            {
//                /* 删除文件 */
//                f_unlink(data);
//            }

//            sprintf(data, "%d:/Download/MIProgram.hex", i);
//            res = f_stat(data, 0);

//            if(res == FR_OK)
//            {
//                /* 删除文件 */
//                f_unlink(data);
//            }

//            sprintf(data, "%d:/Download/DTUProgram.hex", i);
//            res = f_stat(data, 0);

//            if(res == FR_OK)
//            {
//                /* 删除文件 */
//                f_unlink(data);
//            }
//        }
//    }

//    myfree(data);
//    return 0;
//}

////uint16_t Download_Write_Length(char *FilePath,char *Buffer, uint32_t Offset, uint16_t len)
////{
////
////  FRESULT res;//文件操作结果
////    char *data;
////    uint8_t i=ERROR;
////    uint32_t fnum = 0;
////
////    uint32_t time;
////    time = LocalTime;

////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));

////#ifdef TEST
////  if (FatfsDisk[0] == DISKOK)
////  {
////        i = 0;
////    }
////    else
////    {
////        i = ERROR;
////    }
////
////#else
////  if (FatfsDisk[2] == DISKOK)
////  {
////        i = 2;
////    }
////    else if (FatfsDisk[1] == DISKOK)
////  {
////        i = 1;
////    }
////    else
////    {
////        i = ERROR;
////    }

////#endif

////    if(i<3)
////    {
////        sprintf(data,"%d:/Download/%s",i,FilePath);
////        res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);
////        if (res == FR_OK)
////        {
////            res = f_lseek(&fnew, Offset);
////            res = f_write(&fnew, Buffer, len, &fnum);
////            f_close(&fnew);
////        }
////    }
////    printf("write time:%d\n",(LocalTime-time));
////    myfree(data);
////    return fnum;
////}


//uint16_t Download_Write_Length(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
//{
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    uint32_t time;
//    time = LocalTime;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[2] == DISKOK)
//    {
//        i = 2;
//    }
//    else if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Download/%s", i, FilePath);
//        time = LocalTime;
//        res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);
//        printf("open time:%d\n", (LocalTime - time));
//        time = LocalTime;

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_write(&fnew, Buffer, len, &fnum);
//            printf("write time:%d\n", (LocalTime - time));
//            time = LocalTime;
//            f_close(&fnew);
//            printf("close time:%d\n", (LocalTime - time));
//        }
//    }

//    myfree(data);
//    return fnum;
//}

//uint16_t Download_Read_Length(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
//{
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[2] == DISKOK)
//    {
//        i = 2;
//    }
//    else if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Download/%s", i, FilePath);
//        res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_read(&fnew, Buffer, len, &fnum);
//            f_close(&fnew);
//        }
//    }

//    myfree(data);
//    return fnum;
//}

//uint32_t Download_Get_Length(char *FilePath)
//{
//    uint8_t i = ERROR;
//    uint32_t length;
//    char *data;
//    data = mymalloc(20 * sizeof(char));
//    memset(data, 0, (20 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[2] == DISKOK)
//    {
//        i = 2;
//    }
//    else if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Download/%s", i, FilePath);
//        length = Read_File_Length(data);
//    }
//    else
//    {
//        length = 0;
//    }

//    myfree(data);
//    return length;
//}

//uint32_t Download_Get_Dir_Size(void)
//{
//    uint8_t i = ERROR;
//    uint32_t length = 0;
//    char *data;
//    data = mymalloc(20 * sizeof(char));
//    memset(data, 0, (20 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[2] == DISKOK)
//    {
//        i = 2;
//    }
//    else if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Download/GridProfilesList.txt", i);
//        length += Download_Get_Length(data);
//        sprintf(data, "%d:/Download/MIProgram.hex", i);
//        length += Download_Get_Length(data);
//        sprintf(data, "%d:/Download/DTUProgram.hex", i);
//        length += Download_Get_Length(data);
//    }
//    else
//    {
//        length = 0;
//    }

//    myfree(data);
//    return length;
//}

//uint32_t Grid_Profiles_List_Get_Length(void)
//{
//    uint32_t ReturnNum;
//    GridProfilesList_Offset = 0;
//    ReturnNum = Download_Get_Length("GridProfilesList.txt");
//    return ReturnNum;
//}

//uint16_t Grid_Profiles_List_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/GridProfilesList.txt
//    return Download_Write_Length("GridProfilesList.txt", Buffer, Offset, len);
//}
//uint16_t Grid_Profiles_List_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/GridProfilesList.txt
//    return Download_Read_Length("GridProfilesList.txt", Buffer, Offset, len);
//}

//uint32_t MI_Program_Get_Length(void)
//{
//    uint32_t ReturnNum;
//    MIProgram_Offset = 0;
//    ReturnNum = Download_Get_Length("MIProgram.hex");
//    return ReturnNum;
//}

//uint16_t MI_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/MIProgram.hex
//    return Download_Write_Length("MIProgram.hex", Buffer, Offset, len);
//}
//uint16_t MI_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/MIProgram.hex
//    return Download_Read_Length("MIProgram.hex", Buffer, Offset, len);
//}

//uint32_t DTU_Program_Get_Length(void)
//{
//    uint32_t ReturnNum;
//    DTUProgram_Offset = 0;
//    ReturnNum = Download_Get_Length("DTUProgram.hex");
//    return ReturnNum;
//}

//uint16_t DTU_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/DTUProgram.hex
//    return Download_Write_Length("DTUProgram.hex", Buffer, Offset, len);
//}
//uint16_t DTU_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
//{
//    //Download/MIProgram.hex
//    return Download_Read_Length("DTUProgram.hex", Buffer, Offset, len);
//}



//uint16_t Grid_Profiles_List_Write(char *Buffer, uint16_t len)
//{
//    //Download/GridProfilesList.txt
//    uint16_t ReturnNum;
//    ReturnNum = Grid_Profiles_List_Write_Length(Buffer, GridProfilesList_Offset, len);
//    GridProfilesList_Offset += len;
//    return ReturnNum;
//}
//uint16_t Grid_Profiles_List_Read(char *Buffer, uint16_t len)
//{
//    //Download/GridProfilesList.txt
//    uint16_t ReturnNum;
//    ReturnNum = Grid_Profiles_List_Read_Length(Buffer, GridProfilesList_Offset, len);
//    GridProfilesList_Offset += len;
//    return ReturnNum;
//}

//uint16_t MI_Program_Write(char *Buffer, uint16_t len)
//{
//    //Download/MIProgram.hex
//    uint16_t ReturnNum;
//    ReturnNum = MI_Program_Write_Length(Buffer, MIProgram_Offset, len);
//    MIProgram_Offset += len;
//    return ReturnNum;
//}
//uint16_t MI_Program_Read(char *Buffer, uint16_t len)
//{
//    //Download/MIProgram.hex
//    uint16_t ReturnNum;
//    ReturnNum = MI_Program_Read_Length(Buffer, MIProgram_Offset, len);
//    MIProgram_Offset += len;
//    return ReturnNum;
//}

//uint16_t DTU_Program_Write(char *Buffer, uint16_t len)
//{
//    //Download/DTUProgram.hex
//    uint16_t ReturnNum;
//    ReturnNum = DTU_Program_Write_Length(Buffer, DTUProgram_Offset, len);
//    DTUProgram_Offset += len;
//    return ReturnNum;
//}
//uint16_t DTU_Program_Read(char *Buffer, uint16_t len)
//{
//    //Download/MIProgram.hex
//    uint16_t ReturnNum;
//    ReturnNum = DTU_Program_Read_Length(Buffer, DTUProgram_Offset, len);
//    DTUProgram_Offset += len;
//    return ReturnNum;
//}





//uint32_t System_Get_Dir_Size(void)
//{
//    uint8_t i = ERROR;
//    uint32_t length = 0;
//    char *data;
//    data = mymalloc(20 * sizeof(char));
//    memset(data, 0, (20 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/System/SystemDate.dat", i);
//        length = Read_File_Length(data);
//    }
//    else
//    {
//        length = 0;
//    }

//    myfree(data);
//    return length;
//}

//uint16_t System_Data_Delete(void)
//{
//    //System/SystemDate.dat
//    FRESULT res;//文件操作结果
//    DIR dir;
//    uint8_t i = ERROR;
//    char *data;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/System", i);
//        res = f_opendir(&dir, data);

//        if(res != FR_OK)
//        {
//            /* 打开目录失败，就创建目录 */
//            res = f_mkdir(data);
//        }
//        else
//        {
//            /* 如果目录已经存在，关闭它 */
//            res = f_closedir(&dir);
//            sprintf(data, "%d:/System/SystemDate.dat", i);
//            res = f_stat(data, 0);

//            if(res == FR_OK)
//            {
//                /* 删除文件 */
//                f_unlink(data);
//            }
//        }
//    }

//    myfree(data);
//    return 0;
//}

//uint16_t System_Data_Write(char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //System/SystemDate.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/System/SystemDate.dat", i);
//        res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_write(&fnew, Buffer, len, &fnum);
//            /* 不再读写，关闭文件 */
//            f_close(&fnew);
//        }
//    }

//    myfree(data);
//    return fnum;
//}

//uint16_t System_Data_Read(char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //System/SystemDate.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/System/SystemDate.dat", i);
//        res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_read(&fnew, Buffer, len, &fnum);
//            /* 不再读写，关闭文件 */
//            f_close(&fnew);
//        }
//    }

//    myfree(data);
//    return fnum;
//}
//uint32_t Hardware_Get_Dir_Size(void)
//{
//    uint8_t i = ERROR;
//    uint32_t length = 0;
//    char *data;
//    data = mymalloc(20 * sizeof(char));
//    memset(data, 0, (20 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Hardware/HardwareDate.dat", i);
//        length += Read_File_Length(data);
//    }
//    else
//    {
//        length = 0;
//    }

//    myfree(data);
//    return length;
//}

//uint16_t Hardware_Data_Delete(void)
//{
//    //Hardware/HardwareDate.dat
//    FRESULT res;//文件操作结果
//    DIR dir;
//    uint8_t i = ERROR;
//    char *data;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Hardware", i);
//        res = f_opendir(&dir, data);

//        if(res != FR_OK)
//        {
//            /* 打开目录失败，就创建目录 */
//            res = f_mkdir(data);
//        }
//        else
//        {
//            /* 如果目录已经存在，关闭它 */
//            res = f_closedir(&dir);
//            sprintf(data, "%d:/Hardware/HardwareDate.dat", i);
//            res = f_stat(data, 0);

//            if(res == FR_OK)
//            {
//                /* 删除文件 */
//                f_unlink(data);
//            }
//        }
//    }

//    myfree(data);
//    return 0;
//}
//uint16_t Hardware_Data_Write(char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //Hardware/HardwareDate.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Hardware/HardwareDate.dat", i);
//        res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_write(&fnew, Buffer, len, &fnum);
//            /* 不再读写，关闭文件 */
//            f_close(&fnew);
//        }
//    }

//    myfree(data);
//    return fnum;
//}
//uint16_t Hardware_Data_Read(char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //Hardware/HardwareDate.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//#ifdef TEST

//    if(FatfsDisk[0] == DISKOK)
//    {
//        i = 0;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#else

//    if(FatfsDisk[1] == DISKOK)
//    {
//        i = 1;
//    }
//    else
//    {
//        i = ERROR;
//    }

//#endif

//    if(i < 3)
//    {
//        sprintf(data, "%d:/Hardware/HardwareDate.dat", i);
//        res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);

//        if(res == FR_OK)
//        {
//            res = f_lseek(&fnew, Offset);
//            res = f_read(&fnew, Buffer, len, &fnum);
//            /* 不再读写，关闭文件 */
//            f_close(&fnew);
//        }
//    }

//    myfree(data);
//    return fnum;
//}






////uint16_t Local_Historical_Data_Delete(uint8_t Disk,uint32_t Date)
////{
////    //Data/20190124/HistoricalData.dat
////  FRESULT res;//文件操作结果
////  DIR dir;
////    char *data;
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));

////    sprintf(data,"%d:/Data/%04d%02d%02d",Disk,(uint16_t)(Date/10000),(uint8_t)((Date%10000)/100),(Date%100));
////    res = f_opendir(&dir, data);
////    if (res != FR_OK)
////    {
////        /* 打开目录失败，就创建目录 */
////        res = f_mkdir(data);
////    }
////    else
////    {
////        /* 如果目录已经存在，关闭它 */
////        res = f_closedir(&dir);
////        sprintf(data,"%d:/Data/%04d%02d%02d/HistoricalData.dat",Disk,(uint16_t)(Date/10000),(uint8_t)((Date%10000)/100),(Date%100));
////        res = f_stat(data, 0);
////        if (res == FR_OK)
////        {
////            /* 删除文件 */
////            f_unlink(data);
////        }
////    }
////    myfree(data);
////    return 1;
////}

////uint16_t Local_RealTime_To_Historical(uint8_t Disk,char *Buffer, uint16_t Offset, uint16_t len)
////{
////    //Data/20190124/HistoricalData.dat
////  FRESULT res;//文件操作结果
////  DIR dir;

////    char *data;
////    char *data1;

////    calendar_obj calendar;
////    RTC_GetTimes(&calendar);
////
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));
////    data1=mymalloc(100*sizeof(char));
////    memset(data1,0,(100*sizeof(char)));

////    sprintf(data,"%d:/Data/%04d%02d%02d",Disk,(uint16_t)(calendar.w_year),(uint8_t)(calendar.w_month),(calendar.w_date));
////    res = f_opendir(&dir, data);
////    if (res != FR_OK)
////    {
////        /* 打开目录失败，就创建目录 */
////        res = f_mkdir(data);
////        /*将实时数据存为历史数据*/
////        sprintf(data,"%d:/Data/%04d%02d%02d.dat",Disk,(uint16_t)(calendar.w_year),(uint8_t)(calendar.w_month),(calendar.w_date));
////        sprintf(data1,"%d:/Data/RealTimeData.dat",Disk);
////        res = f_rename(data1, data);
////    }
////    else
////    {
////        /* 如果目录已经存在，关闭它 */
////        res = f_closedir(&dir);
////        sprintf(data,"%d:/Data/%04d%02d%02d.dat",Disk,(uint16_t)(calendar.w_year),(uint8_t)(calendar.w_month),(calendar.w_date));
////        res = f_stat(data, 0);
////        if (res == FR_OK)
////        {
////            /* 删除文件 */
////            f_unlink(data);
////        }
////        /*将实时数据存为历史数据*/
////        sprintf(data,"%d:/Data/%04d%02d%02d.dat",Disk,(uint16_t)(calendar.w_year),(uint8_t)(calendar.w_month),(calendar.w_date));
////        sprintf(data1,"%d:/Data/RealTimeData.dat",Disk);
////        res = f_rename(data1, data);
////    }

////    myfree(data);
////    myfree(data1);
////    return 0;
////}

//uint16_t Local_Historical_Data_Read(uint8_t Disk, uint32_t Date, char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //Data/20190124/HistoricalData.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint32_t fnum = 0;
//    calendar_obj calendar;
//    RTC_GetTimes(&calendar);
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//    sprintf(data, "%d:/Data/%04d%02d%02d.dat", Disk, (uint16_t)(Date / 10000), (uint8_t)((Date % 10000) / 100), (Date % 100));
//    res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);

//    if(res == FR_OK)
//    {
//        res = f_lseek(&fnew, Offset);
//        res = f_read(&fnew, Buffer, len, &fnum);
//        /* 不再读写，关闭文件 */
//        f_close(&fnew);
//    }

//    myfree(data);
//    return fnum;
//}

//uint16_t Local_RealTime_Data_Write(uint8_t Disk, char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //Data/RealTimeData.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint32_t fnum = 0;
//    calendar_obj calendar;
//    RTC_GetTimes(&calendar);
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//    sprintf(data, "%d:/Data/RealTimeData.dat", Disk);
//    res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);

//    if(res == FR_OK)
//    {
//        res = f_lseek(&fnew, Offset);
//        res = f_write(&fnew, Buffer, len, &fnum);
//        /* 不再读写，关闭文件 */
//        f_close(&fnew);
//    }

//    myfree(data);
//    return fnum;
//}

//uint16_t Local_RealTime_Data_Read(uint8_t Disk, char *Buffer, uint16_t Offset, uint16_t len)
//{
//    //Data/RealTimeData.dat
//    FRESULT res;//文件操作结果
//    char *data;
//    uint32_t fnum = 0;
//    calendar_obj calendar;
//    RTC_GetTimes(&calendar);
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, (100 * sizeof(char)));
//    sprintf(data, "%d:/Data/RealTimeData.dat", Disk);
//    res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);

//    if(res == FR_OK)
//    {
//        res = f_lseek(&fnew, Offset);
//        res = f_read(&fnew, Buffer, len, &fnum);
//        /* 不再读写，关闭文件 */
//        f_close(&fnew);
//    }

//    myfree(data);
//    return fnum;
//}

//uint8_t Local_Delete_Directory(char *Path)
//{
//    char *fn;        // 文件名
//    DIR dir;
//    FRESULT res;
//    uint16_t len;
//    res = f_opendir(&dir, Path);

//    if(res == FR_OK)
//    {
//        len = strlen(Path);

//        for(;;)
//        {
//            //读取目录下的内容，再读会自动读下一个文件
//            res = f_readdir(&dir, &fno);

//            //为空时表示所有项目读取完毕，跳出
//            if(res != FR_OK || fno.fname[0] == 0)
//            {
//                break;
//            }

//#if _USE_LFN
//            fn = *fno.lfname ? fno.lfname : fno.fname;
//#else
//            fn = fno.fname;
//#endif

//            if((fno.fattrib & AM_DIR) != AM_DIR)
//            {
//                //合成完整文件名
//                sprintf(&Path[len], "/%s", fn);
//                //删除文件
//                f_unlink(Path);
//                Path[len] = 0;
//            }
//        }

//        //删除文件夹
//        f_unlink(Path);
//        res = f_closedir(&dir);
//        return 1;
//    }
//    else
//    {
//        return 0;
//    }
//}

////uint32_t Local_Find_Earliest_Dir(void)
////{
////    //"%d:/Data/%04d%02d%02d/HistoricalData.dat"
////  FRESULT res;        //部分在递归过程被修改的变量，不用全局变量
////  DIR dir;
////  char *fn;        // 文件名
////    char *data;
////    uint32_t DirNum;
////    uint32_t DirNumMin = 0xFFFFFFFF;
////#if _USE_LFN
//// /* 长文件名支持 */
//// /* 简体中文需要2个字节保存一个“字”*/
////  static char lfn[_MAX_LFN * 2 + 1];
////  fno.lfname = lfn;
////  fno.lfsize = sizeof(lfn);
////#endif
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));
////    sprintf(data,"%d:/Data",0);
////    //打开目录
////  res = f_opendir(&dir, data);
////  if (res == FR_OK)
////  {
////      for (;;)
////      {
////          //读取目录下的内容，再读会自动读下一个文件
////          res = f_readdir(&dir, &fno);
////          //为空时表示所有项目读取完毕，跳出
////          if (res != FR_OK || fno.fname[0] == 0) break;
////#if _USE_LFN
////          fn = *fno.lfname ? fno.lfname : fno.fname;
////#else
////          fn = fno.fname;
////#endif
////          //点表示当前目录，跳过
////          if (*fn == '.') continue;
////          //目录，递归读取
////          if (fno.fattrib & AM_ARC)
////          {
////                DirNum = strtol (fn,NULL,10);
////                if(DirNumMin>DirNum)
////                {
////                    DirNumMin = DirNum;
////                }
////          }
////      }
////  }
//////    printf("DirNumMin:%d\n", DirNum);
////    myfree(data);
////  return DirNumMin;
////}

////RealTimeDisk = 1;
////RealTimeDataOffset = 0;
////uint16_t RealTime_Data_Write(char *Buffer, uint16_t len)
////{
////    //Data/RealTimeData.dat
////    uint8_t i;
////
////  FRESULT res;//文件操作结果
////    char *data;
////  uint32_t fnum = 0;
////    uint32_t Diskmyfree1;
////    uint32_t Diskmyfree2;
////    uint32_t Length1;
////    uint32_t Length2;
//////#ifdef TEST
//////    if (FatfsDisk[0] == DISKOK)
//////    {
//////        i = 0;
//////    }
//////    else
//////    {
//////        i = ERROR;
//////    }
//////
//////#else
//////    if (FatfsDisk[1] == DISKOK)
//////    {
//////        i = 1;
//////    }
//////    else
//////    {
//////        i = ERROR;
//////    }

//////#endif

////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));
////    if((FatfsDisk[1] == DISKOK)&&(FatfsDisk[2] == DISKOK))
////    {
////        Diskmyfree1 = Get_Disk_myfree(1);
////        Diskmyfree2 = Get_Disk_myfree(2);
////        Length1 += SYSTEMSIZE - System_Get_Dir_Size();
////        Length1 += HARDWARESIZE - Hardware_Get_Dir_Size();
////        Length2 += DOWNSIZE - Download_Get_Dir_Size();

////        if((Diskmyfree1 <= Length1)&&(Diskmyfree2 <= Length2))
////        {
////
////        }
////    }
////    else
////    {
////        Diskmyfree1 = Get_Disk_myfree(1);
////        Length1 += SYSTEMSIZE - System_Get_Dir_Size();
////        Length1 += HARDWARESIZE - Hardware_Get_Dir_Size();
////        Length1 += DOWNSIZE - Download_Get_Dir_Size();
////
////        if(Diskmyfree1 <= Length1)
////        {
////
////        }
////    }
////    return fnum;
////}

////uint16_t RealTime_Data_Read(uint8_t Disk,uint32_t Date,char *Buffer, uint16_t Offset, uint16_t len)
////{
////    //Data/RealTimeData.dat
////
////  FRESULT res;//文件操作结果
////    char *data;
////  uint32_t fnum = 0;
////
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));

////    sprintf(data,"%d:/Data/%04d%02d%02d.dat",Disk,(uint16_t)(Date/10000),(uint8_t)((Date%10000)/100),(Date%100));
////    res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);
////    if (res == FR_OK)
////    {

////        res = f_lseek(&fnew, Offset);
////        res = f_read(&fnew, Buffer, len, &fnum);
////        /* 不再读写，关闭文件 */
////        f_close(&fnew);
////    }
////    myfree(data);
////    return fnum;
////}

////uint8_t RealTime_Data_Pack(uint16_t PackNum,uint8_t Data,uint32_t Len)
////{
////    uint32_t crc;
////    char *pSaveBuffer;
////    uint32_t BuffLen;
////    calendar_obj calendar;
////    RTC_GetTimes(&calendar);
////    pSaveBuffer = mymalloc(2048*sizeof(char));
////    memset(pSaveBuffer,0,2048*sizeof(char));
////

////    sprintf(pSaveBuffer,"Start:");


////    BuffLen = strlen(pSaveBuffer);
////    sprintf(&pSaveBuffer[BuffLen],"%02d/%02d",(uint8_t)(PackNum%100),(uint8_t)(PackNum/100));
////    BuffLen = strlen(pSaveBuffer);
////    sprintf(&pSaveBuffer[BuffLen],"%02d:%02d:%02d",(uint8_t)(PackNum/100),(uint8_t)(PackNum%100));
////    BuffLen = strlen(pSaveBuffer);
////    sprintf(&pSaveBuffer[BuffLen],"%02d:%02d:%02d",(uint8_t)(PackNum/100),(uint8_t)(PackNum%100));
////
////
////
////    BuffLen = strlen(pSaveBuffer);
////    sprintf(&pSaveBuffer[BuffLen],"%04d",crc);
////    BuffLen = strlen(pSaveBuffer);
////    sprintf(&pSaveBuffer[BuffLen],"End\n");
////    myfree(pSaveBuffer);
////}

////uint8_t Last_Pack_Failure(void)
////{
////    char *pSaveBuffer;
////    pSaveBuffer = mymalloc(200*sizeof(char));
////    memset(pSaveBuffer,0,200*sizeof(char));
////
////    myfree(pSaveBuffer);
////}

//const char BufferTest[][1500] = {\
//    {
//        "A5E5011041000035356031F110730006010A13011D010124E001140135356031F1\
//1000020A05000000000322126031E010020002000000000005100000000000000208\
//00604160100000000000000000000000000000000002080060416010000000000000\
//00000000000000000000020800604160100000000000000000000000000000000002\
//08006041601000000000000000000000000000000000022800604160100000000000\
//00000000000000000000000228006041601000000000000000000000000000000000\
//02280060416010000000000000000000000000000000000228006041601000000000\
//00000000000000000000000002130060416010000000000000000000000000000000\
//00021300604160100000000000000000000000000000000002130060416010000000\
//00000000000000000000000000021300604160100000000000000000000000000000\
//00000230006041601000000000000000000000000000000000023000604160100000\
//00000000000000000000000000000230006041601000000000000000000000000000\
//00000002300060416010000000000000000000000000000000000240006041601000\
//00000000000000000000000000000002400060416010000000000000000000000000\
//000000000240006041601000000000000000000000000000000000D715"
//    }, \
//    {
//        "A5E5011041000235356031F110730006020A13011D01012EE001140135356031F1\
//1000020A050000000002400060416010000000000000000000000000000000000206\
//00003260100000000000000000000000000000000002060000326010000000000000\
//00000000000000000000020600003260100000000000000000000000000000000002\
//06000032601000000000000000000000000000000000022900003260100000000000\
//00000000000000000000000229000032601000000000000000000000000000000000\
//02290000326010000000000000000000000000000000000229000032601000000000\
//00000000000000000000000002140000326010000000000000000000000000000000\
//00021400003260100000000000000000000000000000000002140000326010000000\
//00000000000000000000000000021400003260100000000000000000000000000000\
//00000227000032601000000000000000000000000000000000022700003260100000\
//00000000000000000000000000000227000032601000000000000000000000000000\
//00000002270000326010000000000000000000000000000000000311166031E01002\
//00020000000000051000000000000002240060416010000000000000000000000000\
//0000000002240060416010000000000000000000000000000000008B15"
//    }, \
//    {
//        "A5E5011041000335356031F110730006030A13011D010206E001140135356031F1\
//1000020A050000000002240060416010000000000000000000000000000000000224\
//00604160100000000000000000000000000000000002090000326010000000000000\
//00000000000000000000020900003260100000000000000000000000000000000002\
//09000032601000000000000000000000000000000000020900003260100000000000\
//00000000000000000000000204000032601000000000000000000000000000000000\
//02040000326010000000000000000000000000000000000204000032601000000000\
//00000000000000000000000002040000326010000000000000000000000000000000\
//000311116031E0100000000000000000000000000000000002320090194110000000\
//00000000000000000000000000023200901941100000000000000000000000000000\
//00000231009019411000000000000000000000000000000000023100901941100000\
//00000000000000000000000000000229009019411000000000000000000000000000\
//00000002290090194110000000000000000000000000000000000228009019411000\
//00000000000000000000000000000002280090194110000000000000000000000000\
//000000000227009019411000000000000000000000000000000000CC15"
//    }, \
//    {
//        "A5E5011041000435356031F110730006040A13011D010219E001140135356031F1\
//1000020A050000000002270090194110000000000000000000000000000000000226\
//00901941100000000000000000000000000000000002260090194110000000000000\
//00000000000000000000022500901941100000000000000000000000000000000002\
//25009019411000000000000000000000000000000000022400901941100000000000\
//00000000000000000000000224009019411000000000000000000000000000000000\
//02230090194110000000000000000000000000000000000223009019411000000000\
//00000000000000000000000002220090194110000000000000000000000000000000\
//00022200901941100000000000000000000000000000000002210090194110000000\
//00000000000000000000000000022100901941100000000000000000000000000000\
//00000220009019411000000000000000000000000000000000022000901941100000\
//00000000000000000000000000000219009019411000000000000000000000000000\
//00000002190090194110000000000000000000000000000000000218009019411000\
//00000000000000000000000000000002180090194110000000000000000000000000\
//0000000002170090194110000000000000000000000000000000004815"
//    }, \
//    {
//        "A5E5011041000535356031F110730006050A13011D01022EE001140135356031F1\
//1000020A050000000002170090194110000000000000000000000000000000000216\
//00901941100000000000000000000000000000000002160090194110000000000000\
//00000000000000000000021500901941100000000000000000000000000000000002\
//15009019411000000000000000000000000000000000021400901941100000000000\
//00000000000000000000000214009019411000000000000000000000000000000000\
//02130090194110000000000000000000000000000000000213009019411000000000\
//00000000000000000000000002120090194110000000000000000000000000000000\
//00021200901941100000000000000000000000000000000002110090194110000000\
//00000000000000000000000000021100901941100000000000000000000000000000\
//00000210009019411000000000000000000000000000000000021000901941100000\
//00000000000000000000000000000209009019411000000000000000000000000000\
//00000002090090194110000000000000000000000000000000000208009019411000\
//00000000000000000000000000000002080090194110000000000000000000000000\
//0000000002070090194110000000000000000000000000000000001F15"
//    }, \
//    {
//        "A572011041000635356031F110730006060A13011D010306E0010F0135356031F1\
//1000020A050000000002070090194110000000000000000000000000000000000205\
//00901941100000000000000000000000000000000002050090194110000000000000\
//00000000000000000000020400901941100000000000000000000000000000000002\
//04009019411000000000000000000000000000000000020300901941100000000000\
//00000000000000000000000203009019411000000000000000000000000000000000\
//02300090194110000000000000000000000000000000000230009019411000000000\
//00000000000000000000000002060090194110000000000000000000000000000000\
//00020600901941100000000000000000000000000000000002020090194110000000\
//00000000000000000000000000020200901941100000000000000000000000000000\
//00000201009019411000000000000000000000000000000000020100901941100000\
//0000000000000000000000000000DD15"
//    }
//};

///*将字符串s转换成相应的整数*/
//int atoi(char s[])
//{
//    int i = 0;
//    int n = 0;

//    for(i = 0; s[i] >= '0' && s[i] <= '9'; ++i)
//    {
//        n = 10 * n + (s[i] - '0');
//    }

//    return n;
//}
///*将大写字母转换成小写字母*/
//int tolower(int c)
//{
//    if(c >= 'A' && c <= 'Z')
//    {
//        return c + 'a' - 'A';
//    }
//    else
//    {
//        return c;
//    }
//}
///*将十六进制的字符串转换成整数  */
//int htoi(char s[])
//{
//    int i = 0;
//    int n = 0;

//    if(s[0] == '0' && (s[1] == 'x' || s[1] == 'X'))
//    {
//        i = 2;
//    }
//    else
//    {
//        i = 0;
//    }

//    for(; (s[i] >= '0' && s[i] <= '9') || (s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z'); ++i)
//    {
//        if(tolower(s[i]) > '9')
//        {
//            n = 16 * n + (10 + tolower(s[i]) - 'a');
//        }
//        else
//        {
//            n = 16 * n + (tolower(s[i]) - '0');
//        }
//    }

//    return n;
//}
//uint32_t astr2int(char *buf, uint8_t len)
//{
//    uint32_t Num;
//    char *data;
//    data = mymalloc(2 * len * sizeof(char));
//    memset(data, 0, 2 * len * sizeof(char));
//    memcpy(data, buf, len);
//    Num = atoi(data);
//    myfree(data);
//    return Num;
//}
//uint32_t hstr2int(char *buf, uint8_t len)
//{
//    uint32_t Num;
//    char *data;
//    data = mymalloc(2 * len * sizeof(char));
//    memset(data, 0, 2 * len * sizeof(char));
//    memcpy(data, buf, len);
//    Num = htoi(data);
//    myfree(data);
//    return Num;
//}



//uint16_t File_Write(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
//{
//    FRESULT res;//文件操作结果
//    uint8_t i = ERROR;
//    uint32_t fnum = 0;
//    res = f_open(&fnew, FilePath, FA_OPEN_ALWAYS | FA_WRITE);

//    if(res == FR_OK)
//    {
//        res = f_lseek(&fnew, Offset);
//        res = f_write(&fnew, Buffer, len, &fnum);
//        /* 不再读写，关闭文件 */
//        f_close(&fnew);
//    }

//    return fnum;
//}
//uint16_t File_Read(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
//{
//    FRESULT res;//文件操作结果
//    uint32_t fnum = 0;
//    res = f_open(&fnew, FilePath, FA_OPEN_EXISTING | FA_READ);

//    if(res == FR_OK)
//    {
//        res = f_lseek(&fnew, Offset);
//        res = f_read(&fnew, Buffer, len, &fnum);
//        //printf("%s\n",Buffer);
//        f_close(&fnew);
//    }

//    return fnum;
//}

////uint16_t RealTime_Data_Write(char *Buffer,uint32_t Offset, uint16_t len)
////{
////
////  FRESULT res;//文件操作结果
////    char *data;
////    uint8_t i=ERROR;
////    uint32_t fnum = 0;
////
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));


////    sprintf(data,"%d:/test.dat",0);
////    printf("%s\n",data);
////    res = f_open(&fnew, data, FA_OPEN_ALWAYS | FA_WRITE);
////    if (res == FR_OK)
////    {
////        res = f_lseek(&fnew, Offset);
////        res = f_write(&fnew, Buffer, len, &fnum);
////    }
////    f_close(&fnew);
////    myfree(data);
////    return fnum;
////}


////uint16_t RealTime_Data_Read(uint8_t Disk,uint32_t Date,char *Buffer, uint16_t Offset, uint16_t len)
////{
////    //Data/RealTimeData.dat

////  FRESULT res;//文件操作结果
////    char *data;
////  uint32_t fnum = 0;
////
////    data=mymalloc(100*sizeof(char));
////    memset(data,0,(100*sizeof(char)));

////    //sprintf(data,"%d:/%04d%02d%02d.dat",Disk,(uint16_t)(Date/10000),(uint8_t)((Date%10000)/100),(Date%100));
////    sprintf(data,"%d:/test.dat",0);
////    res = f_open(&fnew, data, FA_OPEN_EXISTING | FA_READ);
////    if (res == FR_OK)
////    {

////        res = f_lseek(&fnew, Offset);
////        res = f_read(&fnew, Buffer, len, &fnum);
////        /* 不再读写，关闭文件 */
////        f_close(&fnew);
////    }
////    myfree(data);
////    return fnum;
////}


////uint32_t UploadedDate;
////uint32_t UploadedOffset;
////uint32_t WriteDate;
////uint32_t WriteOffset;

//uint8_t RealTime_Data_Pack(uint16_t PackNum, char *Data, uint32_t Len)
//{
//    uint32_t crc = 0;
//    char *path;
//    char *pSaveBuffer;
//    uint32_t BuffLen;
//    calendar_obj calendar;
//    RTC_GetTimes(&calendar);
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, 100 * sizeof(char));
//    pSaveBuffer = mymalloc(2048 * sizeof(char));
//    memset(pSaveBuffer, 0, 2048 * sizeof(char));
//    /*包头Start*/
//    sprintf(pSaveBuffer, "Start:");
//    BuffLen = strlen(pSaveBuffer);
//    /*第几包/包数*/
//    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", (uint8_t)(PackNum % 100), (uint8_t)(PackNum / 100));
//    BuffLen = strlen(pSaveBuffer);
//    /*xx:xx:xx时间*/
//    sprintf(&pSaveBuffer[BuffLen], "%02d:%02d:%02d", calendar.hour, calendar.min, calendar.sec);
//    BuffLen = strlen(pSaveBuffer);
//    /*4字节长度*/
//    sprintf(&pSaveBuffer[BuffLen], "%04d", (uint32_t)Len);
//    BuffLen = strlen(pSaveBuffer);
//    /*数据*/
//    memcpy(&pSaveBuffer[BuffLen], Data, Len);
//    BuffLen = strlen(pSaveBuffer);

//    if(crc > 0xFFFF)
//    {
//        crc %= 0x10000;
//    }

//    /*数据校验*/
//    sprintf(&pSaveBuffer[BuffLen], "%04x", crc);
//    BuffLen = strlen(pSaveBuffer);
//    /*包尾End*/
//    sprintf(&pSaveBuffer[BuffLen], "End");
//    //printf("\n%s\n",pSaveBuffer);
//    sprintf(path, "0:/%04d%02d%02d.dat", *WriteDate / 10000, (*WriteDate / 100) % 100, *WriteDate % 100);
//    //printf("%s\n",path);
//    File_Write(path, pSaveBuffer, *WriteOffset, strlen(pSaveBuffer));
//    *WriteOffset += strlen(pSaveBuffer);
//    myfree(path);
//    myfree(pSaveBuffer);
//}


//uint8_t RealTime_Data_Unpack(uint16_t PackNum, char *Data)
//{
//    FRESULT res;//文件操作结果
//    char *path;
//    char *pReadBuffer;
//    uint32_t length = 200;
//    uint32_t i = 0, j = 0;
//    uint32_t crc;
//    uint8_t find = 0;
//    uint8_t PackNo = 0;
//    uint8_t PackAll = 0;
//    uint8_t hour = 0;
//    uint8_t min = 0;
//    uint8_t sec = 0;
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, (100 * sizeof(char)));
//    pReadBuffer = mymalloc(length * sizeof(char));
//    memset(pReadBuffer, 0, (length * sizeof(char)));
//    sprintf(path, "0:/%04d%02d%02d.dat", *UploadedDate / 10000, (*UploadedDate / 100) % 100, *UploadedDate % 100);
//    File_Read(path, pReadBuffer, *UploadedOffset, length);

//    for(i = 0; i < strlen(pReadBuffer); i++)
//    {
//        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
//        {
//            i += START_LEN;
//            find = 1;
//            //printf("找到头\n");
//            break;
//        }
//    }

//    if(find == 1)
//    {
//        //        printf("%s\n",pReadBuffer);
//        /*获取第几包*/
//        PackNo = astr2int(&pReadBuffer[i], PACK_NO_LEN);
//        i += PACK_NO_LEN;
//        /*获取总包数*/
//        PackAll = astr2int(&pReadBuffer[i], PACK_ALL_LEN);
//        i += PACK_ALL_LEN;
//        /*获取时*/
//        hour = astr2int(&pReadBuffer[i], HOUR_LEN);
//        i += HOUR_LEN;
//        /*获取分*/
//        min = astr2int(&pReadBuffer[i], MIN_LEN);
//        i += MIN_LEN;
//        /*获取秒*/
//        sec = astr2int(&pReadBuffer[i], SEC_LEN);
//        i += SEC_LEN;
//        /*获取长度*/
//        length = astr2int(&pReadBuffer[i], DATA_LEN);
//        i += DATA_LEN;
//        //printf("lenth:%04d\n",len);
//        j = i;
//        pReadBuffer = mymalloc((length + CRC_LEN + END_LEN) * sizeof(char));
//        memset(pReadBuffer, 0, ((length + CRC_LEN + END_LEN)*sizeof(char)));
//        File_Read(path, pReadBuffer, *UploadedOffset + i, length + CRC_LEN + END_LEN);
//        //printf("time:%02d:%02d:%02d\n",hour,min,sec);
//        //printf("lenth:%04d\n",length);
//        memcpy(Data, &pReadBuffer[i - j], length);
//        i += length;
//        crc = hstr2int(&pReadBuffer[i - j], CRC_LEN);
//        //printf("crc:%x\n",crc);
//        i += CRC_LEN;
//        //printf("%s\n",&pReadBuffer[i]);
//        i += END_LEN;
//    }

//    //    else
//    //    {
//    //    }
//    PackNum = PackNo * 100 + PackAll;
//    myfree(path);
//    myfree(pReadBuffer);
//    return length;
//}
//uint8_t History_Data_Unpack(uint32_t Date, uint32_t Time, uint16_t PackNum, char *Data)
//{
//    FRESULT res;//文件操作结果
//    char *path;
//    char *pReadBuffer;
//    uint32_t offset = 0;
//    uint32_t length = 200;
//    uint32_t i = 0, j = 0;
//    uint32_t crc;
//    uint8_t find = 0;
//    uint8_t PackNo = 0;
//    uint8_t PackAll = 0;
//    uint8_t hour = 0;
//    uint8_t min = 0;
//    uint8_t sec = 0;
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, (100 * sizeof(char)));
//    pReadBuffer = mymalloc(length * sizeof(char));
//    memset(pReadBuffer, 0, (length * sizeof(char)));
//    sprintf(path, "0:/%04d%02d%02d.dat", Date / 10000, (Date / 100) % 100, Date % 100);

//    for(;;)
//    {
//        File_Read(path, pReadBuffer, offset, length);

//        for(i = 0, j = 0; i < strlen(pReadBuffer); i++)
//        {
//            if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
//            {
//                i += START_LEN;
//                find = 1;
//                //printf("找到头\n");
//                break;
//            }
//        }

//        if(find == 1)
//        {
//            //printf("%s\n",pReadBuffer);
//            /*获取第几包*/
//            PackNo = astr2int(&pReadBuffer[i], PACK_NO_LEN);
//            i += PACK_NO_LEN;
//            /*获取总包数*/
//            PackAll = astr2int(&pReadBuffer[i], PACK_ALL_LEN);
//            i += PACK_ALL_LEN;
//            /*获取时*/
//            hour = astr2int(&pReadBuffer[i], HOUR_LEN);
//            i += HOUR_LEN;
//            /*获取分*/
//            min = astr2int(&pReadBuffer[i], MIN_LEN);
//            i += MIN_LEN;
//            /*获取秒*/
//            sec = astr2int(&pReadBuffer[i], SEC_LEN);
//            i += SEC_LEN;

//            if(((PackNum / 100) == hour) && ((PackNum % 100) == min))
//            {
//                if((((Time / 10000) % 100) == PackNo) && (((Time / 100) % 100) == PackAll))
//                {
//                    break;
//                }
//            }
//            else
//            {
//                /*获取长度*/
//                length = astr2int(&pReadBuffer[i], DATA_LEN);
//                i += DATA_LEN;
//                //printf("lenth:%04d\n",len);
//                offset = i + length + CRC_LEN + END_LEN;
//            }
//        }
//    }

//    /*获取长度*/
//    length = astr2int(&pReadBuffer[i], DATA_LEN);
//    i += DATA_LEN;
//    //printf("lenth:%04d\n",len);
//    j = i;
//    pReadBuffer = mymalloc((length + CRC_LEN + END_LEN) * sizeof(char));
//    memset(pReadBuffer, 0, ((length + CRC_LEN + END_LEN)*sizeof(char)));
//    File_Read(path, pReadBuffer, *UploadedOffset + i, length + CRC_LEN + END_LEN);
//    //printf("time:%02d:%02d:%02d\n",hour,min,sec);
//    //printf("lenth:%04d\n",length);
//    memcpy(Data, &pReadBuffer[i - j], length);
//    i += length;
//    crc = hstr2int(&pReadBuffer[i - j], CRC_LEN);
//    //printf("crc:%x\n",crc);
//    i += CRC_LEN;
//    //printf("%s\n",&pReadBuffer[i]);
//    i += END_LEN;
//    myfree(path);
//    myfree(pReadBuffer);
//    return length;
//}
////uint8_t RealTime_Data_Subcontract(uint16_t PackNum,char *Data,uint32_t Len)
////{
////    FRESULT res;//文件操作结果
////    char *path;
////    char *pReadBuffer;
////    uint32_t length = 200;
////    uint32_t i=0,j=0;
////    uint32_t crc;
////    uint8_t find=0;
////    uint8_t PackNo=0;
////    uint8_t PackAll=0;
////    uint8_t hour=0;
////    uint8_t min=0;
////    uint8_t sec=0;

////    path = mymalloc(100*sizeof(char));
////    memset(path,0,(100*sizeof(char)));
////
////    pReadBuffer = mymalloc(length*sizeof(char));
////    memset(pReadBuffer,0,(length*sizeof(char)));
////    sprintf(path,"0:/%04d%02d%02d.dat",*UploadedDate/10000,(*UploadedDate/100)%100,*UploadedDate%100);
////
////    File_Read(path,pReadBuffer, *UploadedOffset,length);

////    for(i=0;i<strlen(pReadBuffer);i++)
////    {
////        if (strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
////        {
////            i += START_LEN;
////            find = 1;
////            //printf("找到头\n");
////            break;
////        }
////    }
////    if(find==1)
////    {
//////        printf("%s\n",pReadBuffer);
////        /*获取第几包*/
////        PackNo = astr2int(&pReadBuffer[i],PACK_NO_LEN);
////        i+=PACK_NO_LEN;
////        /*获取总包数*/
////        PackAll = astr2int(&pReadBuffer[i],PACK_ALL_LEN);
////        i+=PACK_ALL_LEN;
////        /*获取时*/
////        hour = astr2int(&pReadBuffer[i],HOUR_LEN);
////        i+=HOUR_LEN;
////        /*获取分*/
////        min = astr2int(&pReadBuffer[i],MIN_LEN);
////        i+=MIN_LEN;
////        /*获取秒*/
////        sec = astr2int(&pReadBuffer[i],SEC_LEN);
////        i+=SEC_LEN;
////        /*获取长度*/
////        length = astr2int(&pReadBuffer[i],DATA_LEN);
////        i+=DATA_LEN;
////        //printf("lenth:%04d\n",len);
////        j=i;
////        pReadBuffer = mymalloc((length+CRC_LEN+END_LEN)*sizeof(char));
////        memset(pReadBuffer,0,((length+CRC_LEN+END_LEN)*sizeof(char)));
////        File_Read(path,pReadBuffer, *UploadedOffset+i,length+CRC_LEN+END_LEN);
////        //printf("time:%02d:%02d:%02d\n",hour,min,sec);
////        //printf("lenth:%04d\n",length);
////        memcpy(Data,&pReadBuffer[i-j],length);
////        i+=length;
////        crc = hstr2int(&pReadBuffer[i-j],CRC_LEN);
////        //printf("crc:%x\n",crc);
////        i+=CRC_LEN;
////        //printf("%s\n",&pReadBuffer[i]);
////        i+=END_LEN;
////    }
//////    else
//////    {

//////    }
////    myfree(path);
////    myfree(pReadBuffer);
////}
//uint8_t Last_Pack_Failure(void)
//{
//    char *pSaveBuffer;
//    pSaveBuffer = mymalloc(200 * sizeof(char));
//    memset(pSaveBuffer, 0, 200 * sizeof(char));
//    myfree(pSaveBuffer);
//}
////Mark://发送结果
////Success//成功
////Failure//失败
//void TransmissionMark(bool mark)
//{
//    char *data;
//    char *path;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, 100 * sizeof(char));
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, 100 * sizeof(char));
//    sprintf(path, "0:/%04d%02d%02d.dat", *WriteDate / 10000, (*WriteDate / 100) % 100, *WriteDate % 100);

//    if(mark)
//    {
//        sprintf(data, "Mark:Success");
//    }
//    else
//    {
//        sprintf(data, "Mark:Failure");
//    }

//    File_Write(path, data, *WriteOffset, strlen(data));
//    *WriteOffset += strlen(data);
//    myfree(data);
//    myfree(path);
//}
//void HistoryTransmissionMark(bool mark)
//{
//    char *data;
//    char *path;
//    char *pReadBuffer;
//    uint32_t i = 0;
//    uint32_t j = 0;
//    uint32_t length = 2048;
//    uint32_t find = 0;
//    data = mymalloc(100 * sizeof(char));
//    memset(data, 0, 100 * sizeof(char));
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, 100 * sizeof(char));
//    pReadBuffer = mymalloc(length * sizeof(char));
//    memset(pReadBuffer, 0, (length * sizeof(char)));
//    sprintf(path, "0:/%04d%02d%02d.dat", *UploadedDate / 10000, (*UploadedDate / 100) % 100, *UploadedDate % 100);

//    if(mark == true)
//    {
//        sprintf(data, "Success");
//    }
//    else
//    {
//        sprintf(data, "Failure");
//    }

//    File_Read(path, pReadBuffer, *UploadedOffset, length);

//    for(i = 0; i < strlen(pReadBuffer); i++)
//    {
//        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
//        {
//            i += START_LEN;
//            find = 1;
//            printf("找到头\n");
//            break;
//        }
//    }

//    if(find == 1)
//    {
//        i += PACK_NO_LEN + PACK_ALL_LEN + HOUR_LEN + MIN_LEN + SEC_LEN;
//        //获取长度
//        length = astr2int(&pReadBuffer[i], DATA_LEN);
//        i = i + DATA_LEN;
//        j = i;
//        pReadBuffer = mymalloc((length + CRC_LEN + END_LEN + MARK_LEN + MARK_RES_LEN) * sizeof(char));
//        memset(pReadBuffer, 0, ((length + CRC_LEN + END_LEN + MARK_LEN + MARK_RES_LEN)*sizeof(char)));
//        File_Read(path, pReadBuffer, *UploadedOffset + i, length + CRC_LEN + END_LEN + MARK_LEN + MARK_RES_LEN);
//        //printf("%s\n",pReadBuffer);
//        i += length + CRC_LEN + END_LEN;
//    }

//    find = 0;

//    for(; i < (strlen(pReadBuffer) + j); i++)
//    {
//        printf("%d\n", *UploadedOffset);

//        if(strncmp(&pReadBuffer[i - j], "Mark:", MARK_LEN) == 0)
//        {
//            i += MARK_LEN;
//            find = 1;
//            //printf("找到\n");
//            break;
//        }
//    }

//    if(find == 1)
//    {
//        File_Write(path, data, *UploadedOffset + i, strlen(data));
//    }

//    *UploadedOffset += i;
//    myfree(data);
//    myfree(path);
//    myfree(pReadBuffer);
//}

//void test(void)
//{
//    char *buffer;
//    char *path;
//    uint32_t i;
//    calendar_obj calendar;
//    RTC_GetTimes(&calendar);
//    *UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
//    *WriteDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
//    path = mymalloc(100 * sizeof(char));
//    memset(path, 0, 100 * sizeof(char));
//    sprintf(path, "0:/%04d%02d%02d.dat", *UploadedDate / 10000, (*UploadedDate / 100) % 100, *UploadedDate % 100);
//    /* 删除文件 */
//    buffer = mymalloc(2048 * sizeof(char));
//    memset(buffer, 0, 2048 * sizeof(char));

//    for(i = 0; i < 10; i++)
//    {
//        printf("%d\n", i);
//        RealTime_Data_Pack(101, (char *)BufferTest[0], strlen(BufferTest[0]));
//        TransmissionMark(false);
//        RealTime_Data_Unpack(101, buffer);
//        //        //printf("%s\n",buffer);
//        HistoryTransmissionMark(true);
//        //        delay_ms(50);
//    }

//    for(i = 0; i < 20; i++)
//    {
//        //        printf("%d\n",i);
//        //        RealTime_Data_Pack(101,(char *)BufferTest[0],strlen(BufferTest[0]));
//        //        TransmissionMark(false);
//        //        RealTime_Data_Unpack(101,buffer,2048);
//        //printf("%s\n",buffer);
//        //        HistoryTransmissionMark(true);
//        //        delay_ms(50);
//    }

//    //    for(i=0;i<14;i++)
//    //    {
//    //        printf("%d\n",i);
//    //        RealTime_Data_Pack(101,(char *)BufferTest[0],strlen(BufferTest[0]));
//    //        TransmissionMark(false);
//    //        RealTime_Data_Unpack(101,buffer,2048);
//    //        //printf("%s\n",buffer);
//    //        HistoryTransmissionMark(true);
//    //        delay_ms(50);
//    //    }
//    myfree(path);
//    myfree(buffer);
//}
