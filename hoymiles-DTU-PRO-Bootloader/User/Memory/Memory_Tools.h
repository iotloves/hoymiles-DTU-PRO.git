#ifndef _MEMORY_TOOL_H
#define _MEMORY_TOOL_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "usart.h"
#else
#include "stm32f10x.h"
#include "usart_debug.h"
#endif

typedef struct
{
    uint8_t disk;
    uint32_t size;
    uint32_t off;
    void *path;
    void *buffer;

} fs_write;

void Queue_init(void);
void Queue_enter(fs_write data);
uint8_t Queue_Out(fs_write *data);
uint8_t Queue_IsEmpty(void);
uint32_t astr2int(char *buf, uint8_t len);
uint32_t hstr2int(char *buf, uint8_t len);
uint16_t CalcCRC8(uint8_t *TargetAddr, uint32_t nub);
uint8_t Queue_IsSameFile(fs_write data);
u8 Ascii2Hex(u8 *ascii, u8 *hex, u8 asciiLen);
u8 Hex2Ascii(u8 *ascii, u8 *hex, u8 hexLen);
#ifdef DTU3PRO
void hex_ascii(uint8_t *b_hex, uint8_t *b_asc2, uint8_t len);
uint8_t McuAsciiToHex(uint8_t ascii); //ascii 转化16进制数
void ascii_to_hex(uint8_t *sour, uint8_t *dest, uint8_t len);
#endif
#endif