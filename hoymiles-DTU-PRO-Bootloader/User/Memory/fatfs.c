#include "fatfs.h"
#include "malloc.h"
#include "string.h"
#include "stdio.h"
#include "Memory.h"
#include "sdio_sd.h"
#ifdef DTU3PRO
FATFS fs;                                                   /* FatFs文件系统对象 */
FIL fnew;
FRESULT res_flash;
FILINFO fno;
#endif

void Fatfs_Init(void)
{
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", Peripheral);
    res_flash = f_mount(&fs, path, 1);

    if(res_flash == FR_NO_FILESYSTEM)
    {
        printf("》FLASH还没有文件系统，即将进行格式化...\r\n");
        res_flash = f_mkfs(path, 0, 0);

        if(res_flash == FR_OK)
        {
            printf("》FLASH已成功格式化文件系统。\r\n");
            /* 格式化后，先取消挂载 */
            res_flash = f_mount(NULL, path, 1);
            /* 重新挂载   */
            res_flash = f_mount(&fs, path, 1);
        }
        else
        {
            printf("《《格式化失败。》》\r\n");
        }
    }
    else if(res_flash != FR_OK)
    {
        printf("！！外部Flash挂载文件系统失败。(%d)\r\n", res_flash);
        printf("！！可能原因：SPI Flash初始化不成功。\r\n");
    }
    else
    {
        printf("》文件系统挂载成功，可以进行测试\r\n");
    }

    myfree(path);
}


uint32_t Get_Disk_Free(uint8_t Disk)
{
    char *path;
    FATFS *pfs;
    FRESULT res;//文件操作结果
    DWORD fre_clust;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:", Disk);

    if(SD_GetState() != SD_CARD_ERROR)
    {
        /* 获取设备信息和空簇大小 */
        res = f_getfree((char *)path, &fre_clust, &pfs);
        myfree(path);
        printf("res %d\n", res);

        if(res == FR_OK)
        {
            return (uint32_t)((fre_clust * pfs->csize * (pfs->ssize / 512) >> 1));
        }
    }

    return 0;
}

