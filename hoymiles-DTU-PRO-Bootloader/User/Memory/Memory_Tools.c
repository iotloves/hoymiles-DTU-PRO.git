#include "Memory_tools.h"
#include "malloc.h"
#include "string.h"
#include "stdio.h"
#include "SysTick.h"
#include "Memory.h"
#include "stdlib.h"

#define CHAR_TO_UPPER(ch)   ((ch >= 'a' && ch <= 'z')?(ch-0x20):ch)
#define MAX_SIZE 20
//定义队列结构体
struct Queue
{
    int front;//队头
    int front_rounds;
    int endline;//队尾
    int end_rounds;

    fs_write data[MAX_SIZE];//数据
};

struct Queue myQueue = { 0, 0, 0, 0, { 0 } };
static uint32_t times = 0;
/***********************************************
** Function name:       Queue_init
** Descriptions:        队列初始化
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Queue_init(void)
{
    uint8_t i;
    times = LocalTime;

    for(i = 0; i < MAX_SIZE; i++)
    {
        if(myQueue.data[i].path != NULL)
        {
            myfree(myQueue.data[i].path);
            myQueue.data[i].path = NULL;
        }

        if(myQueue.data[i].buffer != NULL)
        {
            myfree(myQueue.data[i].buffer);
            myQueue.data[i].buffer = NULL;
        }
    }

    myQueue.front = 0; //队头
    myQueue.front_rounds = 0;
    myQueue.endline = 0; //队尾
    myQueue.end_rounds = 0;
}
/***********************************************
** Function name:       Queue_enter
** Descriptions:        入队
** input parameters:    data 加入队列的数据
** output parameters:   无
** Returned value:      无
*************************************************/
void Queue_enter(fs_write data)
{
    uint16_t i;
    uint16_t len;
    times = LocalTime;

    //判断队列是否溢出
    if((myQueue.endline + 1) % MAX_SIZE != myQueue.front)
    {
        myQueue.data[myQueue.endline].path = mymalloc(PathLen);
        memset(myQueue.data[myQueue.endline].path, 0, PathLen);
        myQueue.data[myQueue.endline].buffer = mymalloc(data.size);
        memset(myQueue.data[myQueue.endline].buffer, 0, data.size);
        memcpy(myQueue.data[myQueue.endline].path, data.path, strlen(data.path));
        myQueue.data[myQueue.endline].off =  data.off;
        myQueue.data[myQueue.endline].size = data.size;
        myQueue.data[myQueue.endline].disk = data.disk;
        memcpy(myQueue.data[myQueue.endline].buffer, data.buffer, data.size);

        if((myQueue.endline + 1) / MAX_SIZE >= 1)
        {
            myQueue.end_rounds++;
        }

        myQueue.endline = (myQueue.endline + 1) % MAX_SIZE;
    }
    else
    {
        return;
    }
}
/***********************************************
** Function name:       Queue_Out
** Descriptions:        出队
** input parameters:    无
** output parameters:   data 从队列读出数据
** Returned value:      1 成功 0失败(无数据)
*************************************************/
uint8_t Queue_Out(fs_write *data)
{
    uint16_t i;
    times = LocalTime;

    if((myQueue.front_rounds * MAX_SIZE + myQueue.front) >= (myQueue.end_rounds * MAX_SIZE + myQueue.endline))
    {
        return 0;
    }
    else
    {
        memcpy(data->path, myQueue.data[myQueue.front].path, strlen(myQueue.data[myQueue.front].path));
        data->size = myQueue.data[myQueue.front].size;
        data->off = myQueue.data[myQueue.front].off;
        data->disk = myQueue.data[myQueue.front].disk;
        memcpy(data->buffer, myQueue.data[myQueue.front].buffer, myQueue.data[myQueue.front].size);
        myfree(myQueue.data[myQueue.front].path);
        myfree(myQueue.data[myQueue.front].buffer);
        myQueue.data[myQueue.front].size = 0;
        myQueue.data[myQueue.front].off = 0;

        if((myQueue.front + 1) / MAX_SIZE >= 1)
        {
            myQueue.front_rounds++;
        }

        myQueue.front = (myQueue.front + 1) % MAX_SIZE;
        return 1;
    }
}
/***********************************************
** Function name:       Queue_IsEmpty
** Descriptions:        查看队列是否为空
** input parameters:    无
** output parameters:   无
** Returned value:      1 有数据 0无数据
*************************************************/
uint8_t Queue_IsEmpty(void)
{
    if((myQueue.front_rounds * MAX_SIZE + myQueue.front) >= (myQueue.end_rounds * MAX_SIZE + myQueue.endline))
    {
        //初始化队列
        if((LocalTime - times) >= 10000)
        {
            times = LocalTime;

            if((myQueue.front_rounds != 0) || (myQueue.end_rounds != 0) || (myQueue.front != 0) || (myQueue.endline != 0))
                //初始化队列
            {
                Queue_init();
            }
        }

        return 0;
    }
    else
    {
        times = LocalTime;
        return 1;
    }
}
/***********************************************
** Function name:       Queue_IsSameFile
** Descriptions:        检查下一数据是否是相同路径
** input parameters:    data 本次数据
** output parameters:   无
** Returned value:      1 相同路径 0 不同路径
*************************************************/
uint8_t Queue_IsSameFile(fs_write data)
{
    if(strncmp(data.path, myQueue.data[(myQueue.front + 1) % MAX_SIZE].path, strlen(data.path)) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/***********************************************
** Function name:       tolower
** Descriptions:        将大写字母转换成小写字母
** input parameters:    c 大写字母
** output parameters:   无
** Returned value:      小写字母
*************************************************/
int tolower(int c)
{
    if(c >= 'A' && c <= 'Z')
    {
        return c + 'a' - 'A';
    }
    else
    {
        return c;
    }
}
/***********************************************
** Function name:       htoi
** Descriptions:        将十六进制的字符串转换成整数
** input parameters:    s[] 十六进制的字符串
** output parameters:   无
** Returned value:      整数
*************************************************/
int htoi(char s[])
{
    int i = 0;
    int n = 0;

    if(s[0] == '0' && (s[1] == 'x' || s[1] == 'X'))
    {
        i = 2;
    }
    else
    {
        i = 0;
    }

    for(; (s[i] >= '0' && s[i] <= '9') || (s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z'); ++i)
    {
        if(tolower(s[i]) > '9')
        {
            n = 16 * n + (10 + tolower(s[i]) - 'a');
        }
        else
        {
            n = 16 * n + (tolower(s[i]) - '0');
        }
    }

    return n;
}
/***********************************************
** Function name:       astr2int
** Descriptions:        字符串转数字
** input parameters:    *buf 字符串 len 长度
** output parameters:   无
** Returned value:      数字
*************************************************/
uint32_t astr2int(char *buf, uint8_t len)
{
    uint32_t Num;
    char *data;
    data = mymalloc(2 * len * sizeof(char));
    memset(data, 0, 2 * len * sizeof(char));
    memcpy(data, buf, len);
    Num = atoi(data);
    myfree(data);
    return Num;
}
/***********************************************
** Function name:       astr2int
** Descriptions:        16进制字符串转数字
** input parameters:    *buf 字符串 len 长度
** output parameters:   无
** Returned value:      数字
*************************************************/
uint32_t hstr2int(char *buf, uint8_t len)
{
    uint32_t Num;
    char *data;
    data = mymalloc(2 * len * sizeof(char));
    memset(data, 0, 2 * len * sizeof(char));
    memcpy(data, buf, len);
    Num = htoi(data);
    myfree(data);
    return Num;
}

/***********************************************
** Function name:       Ascii2Hex
** Descriptions:        字符串转16进制数
** input parameters:    *ascii 字符串 asciiLen 长度
** output parameters:   *hex 16进制数组
** Returned value:      转换个数
*************************************************/
u8 Ascii2Hex(u8 *ascii, u8 *hex, u8 asciiLen)
{
    u8 i, ch, value;
    value = 0;

    for(i = 0; i < (asciiLen >> 1); i++)
    {
        ch = CHAR_TO_UPPER(ascii[i * 2]);

        if(ch >= '0' && ch <= '9')
        {
            value = ch - '0';
        }
        else if(ch >= 'A' && ch <= 'F')
        {
            value = ch - 'A' + 0x0A;
        }
        else
        {
            return i;
        }

        hex[i] = (value << 4);
        ch = CHAR_TO_UPPER(ascii[i * 2 + 1]);

        if(ch >= '0' && ch <= '9')
        {
            value = ch - '0';
        }
        else if(ch >= 'A' && ch <= 'F')
        {
            value = ch - 'A' + 0x0A;
        }
        else
        {
            return i;
        }

        hex[i] += value;
    }

    return i;
}
/***********************************************
** Function name:       Hex2Ascii
** Descriptions:        16进制数转字符串
** input parameters:    *hex 16进制数组 hexLen 长度
** output parameters:   *ascii 字符串
** Returned value:      转换个数
*************************************************/
u8 Hex2Ascii(u8 *ascii, u8 *hex, u8 hexLen)
{
    u8 i, value;

    for(i = 0; i < hexLen; i++)
    {
        value = (hex[i] >> 4);

        if(value > 9)
        {
            value += 0x07;
        }

        ascii[2 * i] = value + 0x30;
        value = (hex[i] & 0x0F);

        if(value > 9)
        {
            value += 0x07;
        }

        ascii[2 * i + 1] = value + 0x30;
    }

    return hexLen * 2;
}


#ifdef DTU3PRO
/***********************************************
** Function name:
** Descriptions:        16进制数转ASCII码
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void hex_ascii(uint8_t *b_hex, uint8_t *b_asc2, uint8_t len)
{
    uint8_t i, j;

    for(i = 0; i < len; i++)
    {
        if((0x0A <= ((*(b_hex + i) >> 4) & 0x0f)) && ((*(b_hex + i) >> 4) & 0x0f) <= 0x0F)
        {
            (*(b_asc2 + 2 * i)) = ((*(b_hex + i) >> 4) & 0x0f) + 0x37;
        }
        else
        {
            (*(b_asc2 + 2 * i)) = ((*(b_hex + i) >> 4) & 0x0f) + 0x30;
        }

        if((0x0A <= (*(b_hex + i) & 0x0f)) && ((*(b_hex + i) & 0x0f) <= 0x0F))
        {
            (*(b_asc2 + 2 * i + 1)) = ((*(b_hex + i)) & 0x0f) + 0x37;
        }
        else
        {
            (*(b_asc2 + 2 * i + 1)) = ((*(b_hex + i)) & 0x0f) + 0x30;
        }
    }
}

uint8_t McuAsciiToHex(uint8_t ascii) //ascii 转化16进制数
{
    if((ascii >= '0') && (ascii <= '9'))
    {
        return(ascii - '0');
    }
    else if((ascii >= 'A') && (ascii <= 'F'))
    {
        return(ascii - 'A' + 0X0A);
    }
    else if((ascii >= 'a') && (ascii <= 'f'))
    {
        return(ascii - 'a' + 0X0A);
    }
    else
    {
        return(0X00);
    }
}

void ascii_to_hex(uint8_t *sour, uint8_t *dest, uint8_t len)
{
    uint8_t i;
    uint8_t temp[MAX_PRO_PACKAGE] = {0};

    for(i = 0; i < len; i++)
    {
        temp[i] = McuAsciiToHex(*(sour + i));
    }

    for(i = 0; i < (len / 2); i++)
    {
        (*(dest + i)) = ((temp[2 * i] << 4) & 0xf0) + (temp[(2 * i) + 1] & 0x0f);
    }
}

#endif