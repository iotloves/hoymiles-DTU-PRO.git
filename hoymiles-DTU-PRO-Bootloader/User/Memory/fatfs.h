#ifndef __FATFS_H
#define __FATFS_H

#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "ff.h"

#endif


uint32_t Get_Disk_Free(uint8_t Disk);
void Fatfs_Init(void);


#endif