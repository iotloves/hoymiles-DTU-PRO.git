#ifndef __MEMORY_H
#define __MEMORY_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#include "spi_flash.h"
#ifndef Bootloader
#include "usart.h"
#include "AntiBackflow.h"
#endif
#else
#include "stm32f10x.h"
#include "stm32f10x_pwr.h"
#include "usart_debug.h"
#define FS_MAX_DIR 1
#endif
#ifndef Bootloader
#include "stdbool.h"
#include "rtc.h"
#include "usart_nrf.h"

#endif
#define DefaultDisk        0
#ifdef DTU3PRO
#define ExpansionDisk      1
#define Peripheral         2
#endif
#define DownloadSize       2097152
#define SystemSize         400
#ifndef DTU3PRO
#define HardwareSize       9000
#else
#define HardwareSize       3672016
#endif
#define DataAverageSize    40
#define RemainQuant        10
#define DayNum             ((24*60)/15)
#define ReadMaxSize        15*1024
#define PathLen            200
#define BufferLen          2048
#define NODISK             0                    //设备状态标准 0：无设备
#define UNFORMATTED        1                    //1：设备初格式化不可用
#define DISKOK             2                    //2：设备挂载成功可用
#define ERROR              0xF7
#define FATFSOK            0xEE
#define DOWNSIZE           2048
#define SYSTEMSIZE         512
#define HARDWARESIZE       1024

#define START_LEN           6
#define PACK_NO_LEN         3
#define PACK_ALL_LEN        2
#define HOUR_LEN            3
#define MIN_LEN             3
#define SEC_LEN             2
#define DATA_LEN            4
#define CRC_LEN             4
#define END_LEN             3
#define MARK_LEN            5
#define MARK_RES_LEN        7
#define DATA_SAVE_ALL       (START_LEN+PACK_NO_LEN+PACK_ALL_LEN+HOUR_LEN+MIN_LEN+SEC_LEN+DATA_LEN+CRC_LEN+DATA_LEN+END_LEN)


#define LIST_START_LEN      10
#define START_ADDR_LEN      10
#define END_ADDR_LEN        10
#define LIST_END_LEN        7
#define LIST_NEXT           (LIST_START_LEN + PACK_NO_LEN + PACK_ALL_LEN + START_ADDR_LEN + END_ADDR_LEN + DATA_LEN+MARK_LEN + MARK_RES_LEN + LIST_END_LEN)
#define MAX_ONE_LINE_LEN    100
#define MAX_PRO_PACKAGE     42        //74//42 + 32  //DTU的hex文件一行去掉：为42个 微逆的hex文件为74个
#define SerialNum           0x0A00
typedef union
{
    struct
    {
        uint8_t UpgradeSign[10];                //升级标志
        uint8_t ServerDomainName[40];           //服务器域名
        uint16_t ServerPort;                    //服务器端口号
        uint8_t APN[20];                        //APN
        uint8_t netmode_select;                 //网络模式
        uint8_t small_version;                  //硬件小版本号
    } Property;
    uint8_t Data[74];
} SystemCfg;



/*文件初始化及写操作*/
void File_Processing(void);
/*MI硬件信息保存*/
#ifndef Bootloader
void InverterMajor_Delete(void);//删除InverterMajor文件
uint16_t InverterMajor_Write(InverterMajor *InverterMajorBuf, uint16_t Pv_nub);//写InverterMajor文件
uint16_t InverterMajor_Read(InverterMajor *InverterMajorBuf);

void InverterDetail_Delete(void);
uint16_t InverterDetail_Write(InverterDetail *InverterDetailBuf, uint16_t InverterDetailOff, uint16_t InverterNum);
uint16_t InverterDetail_Read(InverterDetail *InverterDetailBuf, uint16_t InverterDetailOff, uint16_t InverterNum);

void InverterReal_Delete(void);
uint16_t InverterReal_Write(InverterReal *InverterRealBuf, uint16_t Pv_nub);
uint16_t InverterReal_Read(InverterReal *InverterRealBuf);

#ifdef DTU3PRO
void MeterMajor_Delete(void);
uint16_t MeterInfor_Write(MeterMajor *MeterMajorBuf, uint16_t Meter_nub);
uint16_t MeterMajor_Read(MeterMajor *MeterMajorBuf);
#endif

void System_DtuDetail_Delete(void);
uint16_t System_DtuDetail_Write(DtuDetail *DtuDetailbuf);
uint16_t System_DtuDetail_Read(DtuDetail *DtuDetailbuf);

void System_DtuMajor_Delete(void);
uint16_t System_DtuMajor_Write(DtuMajor *DtuMajorbuf);
uint16_t System_DtuMajor_Read(DtuMajor *DtuMajorbuf);

void ClearDownloadFile(void);


uint32_t RealTime_Data_Pack(uint8_t PackNum, uint8_t PackAll, char *Data, uint32_t Len);
uint32_t RealTime_Data_Unpack(uint8_t *PackNum, uint8_t *PackAll, char *Data);
uint32_t History_Data_Unpack(uint8_t PackNum, uint8_t *PackAll, char *Data);
//uint8_t History_Data_Unpack(uint32_t Date, uint32_t Time, uint8_t PackNum, uint8_t PackAll, char *Data);
void TransmissionMark(uint8_t mark);
void Date_List_RealTime_Mark(uint8_t mark);
void Date_List_History_Mark(uint8_t mark);
uint8_t Historical_Presence(void);
//文件系统状态读取 空闲返回0
uint8_t Get_FileStatus(void);
void Download_GridProfilesList_Delete(void);
void Download_MIProgram_Delete(void);

uint32_t MI_Program_Get_Length(void);
uint16_t MI_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len);
uint16_t MI_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len);
uint32_t Grid_Profiles_List_Get_Length(void);
uint16_t Grid_Profiles_List_Write_Length(char *Buffer, uint32_t Offset, uint16_t len);
uint16_t Grid_Profiles_List_Read_Length(char *Buffer, uint32_t Offset, uint16_t len);
#endif
void Download_DTUProgram_Delete(void);
uint32_t DTU_Program_Get_Length(void);
uint16_t DTU_Program_Write(char *Buffer, uint32_t Offset, uint16_t len);
uint16_t DTU_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len);
uint16_t DTU_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len);

uint8_t MI_Program_Check(void);
uint8_t CheckProgramA(void);
uint8_t DTU_Program_Check(u16 Hardware);

uint8_t Grid_Program_Check(void); // 临时增加++


void Download_DirCheck(void);
uint32_t Download_Get_Dir_Size(void);

void System_DirCheck(void);
uint32_t System_Get_Dir_Size(void);

uint32_t Hardware_Get_Dir_Size(void);
void Hardware_DirCheck(void);
void RealTime_DirCheck(uint8_t Disk);
uint16_t Local_Historical_Data_Read(uint8_t Disk, uint32_t Date, char *Buffer, uint16_t Offset, uint16_t len);
uint16_t Local_RealTime_Data_Write(uint8_t Disk, char *Buffer, uint16_t Offset, uint16_t len);
uint16_t Local_RealTime_Data_Read(uint8_t Disk, char *Buffer, uint16_t Offset, uint16_t len);

void DTU_Data_Delete(void);
uint32_t DTU_Data_Get_Length(void);
uint32_t DTU_Data_Write(char *DTU_Data_Buf, uint32_t DTU_Data_Len, uint32_t DTU_Data_Off);
uint16_t DTU_Data_Read(char *DTU_Data_Buf, uint32_t DTU_Data_Len, uint32_t DTU_Data_Off);

void DTU_Cfg_Data_Delete(void);
uint32_t DTU_Cfg_Data_Get_Length(void);
uint32_t DTU_Cfg_Data_Write(char *DTU_Cfg_Data_Buf, uint32_t DTU_Cfg_Data_Len, uint32_t DTU_Cfg_Data_Off);
uint16_t DTU_Cfg_Data_Read(char *DTU_Cfg_Data_Buf, uint32_t DTU_Cfg_Data_Len, uint32_t DTU_Cfg_Data_Off);

void MI_Data_Delete(void);
uint32_t MI_Data_Get_Length(void);
uint32_t MI_Data_Write(char *MI_Data_Buf, uint32_t MI_Data_Len, uint32_t MI_Data_Off);
uint16_t MI_Data_Read(char *MI_Data_Buf, uint32_t MI_Data_Len, uint32_t MI_Data_Off);

void MI_Cfg_Data_Delete(void);
uint32_t MI_Cfg_Data_Get_Length(void);
uint32_t MI_Cfg_Data_Write(char *MI_Cfg_Data_Buf, uint32_t MI_Cfg_Data_Len, uint32_t MI_Cfg_Data_Off);
uint16_t MI_Cfg_Data_Read(char *MI_Cfg_Data_Buf, uint32_t MI_Cfg_Data_Len, uint32_t MI_Cfg_Data_Off);

void Grid_Profiles_Data_Delete(void);
uint32_t Grid_Profiles_Data_Get_Length(void);
uint32_t Grid_Profiles_Data_Write(char *Grid_Profiles_Data_Buf, uint32_t Grid_Profiles_Data_Len, uint32_t Grid_Profiles_Data_Off);
uint16_t Grid_Profiles_Data_Read(char *Grid_Profiles_Data_Buf, uint32_t Grid_Profiles_Data_Len, uint32_t Grid_Profiles_Data_Off);

void Grid_Profiles_Cfg_Data_Delete(void);
uint32_t Grid_Profiles_Cfg_Data_Get_Length(void);
uint32_t Grid_Profiles_Cfg_Data_Write(char *Grid_Profiles_Cfg_Data_Buf, uint32_t Grid_Profiles_Cfg_Data_Len, uint32_t Grid_Profiles_Cfg_Data_Off);
uint16_t Grid_Profiles_Cfg_Data_Read(char *Grid_Profiles_Cfg_Data_Buf, uint32_t Grid_Profiles_Cfg_Data_Len, uint32_t Grid_Profiles_Cfg_Data_Off);

void DeleteOldestData(void);
void test(void);
void DownTest(void);
void Downwrite(void);
void DownRead(void);
void StatusMark(uint8_t mark);
uint8_t Fs_Init(uint8_t format);
void DeleteHistoryData(void);
void Memory_BKP_Save(void);
void Memory_BKP_Read(void);
#endif
