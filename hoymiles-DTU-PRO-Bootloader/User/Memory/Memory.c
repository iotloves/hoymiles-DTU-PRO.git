#include "Memory.h"
#include "rtc.h"
#include "string.h"
#include "stdio.h"
#include "systick.h"
#include "spi_flash.h"
#include "malloc.h"
#include "littlefs.h"
#include "memory_tools.h"
#include "crc16.h"
#include "ST_Flash.h"
#ifndef Bootloader
#include "HW_Version.h"
#include "NetProtocol.h"
#include "NetProtocolTools.h"
#include "RealData.pb.h"
#include "TimeProcessing.h"
#ifdef DTU3PRO
#include "fatfs.h"
#include "AntiBackflow.h"
#include "iwdg.h"
#else
#include "stm32_wdg.h"
#endif
extern uint32_t storge_historical_data_flg;
extern uint32_t DownOffset;
extern DtuDetail Dtu3Detail;
//DTU主要信息
extern DtuMajor Dtu3Major;
#endif
//实时数据存储位置
uint8_t StorageLocat = DefaultDisk;
#ifdef DTU3PRO
//电表数量
extern uint8_t MeterLinkNum;
#endif

enum fs_state
{
    fp_init = 0,
    fp_open = 1,
    fp_idle = 2,
    fp_write = 3,
    fp_check = 4,
    fp_delete = 5,
};
//文件对象
lfs_file_t lfnew;
static struct lfs_info lfno = { 0 };
/*FatfsDisk[0]:TF卡 FatfsDisk[1]:FLASH0 FatfsDisk[2]:FLASH1*/
static uint8_t FatfsDisk[3] = { 0 };
static uint32_t MIProgram_Offset = 0;
static uint32_t DTUProgram_Offset = 0;
static uint32_t GridProfilesList_Offset = 0;
static uint8_t fp_state = 0;
//const char BufferTest[][2]={{},{}};
volatile SystemCfg system_cfg;
#ifndef Bootloader
uint32_t UploadedDate = 0;
uint32_t UploadedOffset = 0;
uint32_t InsideOffset = 0;
uint32_t WriteDate = 0;
uint32_t WriteOffset = 0;

#ifdef DTU3PRO
uint32_t *UploadedDateSave = (uint32_t *)BKPSRAM_BASE;
uint32_t *UploadedOffsetSave = (uint32_t *)(BKPSRAM_BASE + 2 * sizeof(uint32_t));
uint32_t *WriteDateSave = (uint32_t *)(BKPSRAM_BASE + 4 * sizeof(uint32_t));
uint32_t *WriteOffsetSave = (uint32_t *)(BKPSRAM_BASE + 6 * sizeof(uint32_t));
extern MeterInfomation MeterInfo[];
#else
uint32_t *UploadedDateSave = (uint32_t *)(BKP_BASE + BKP_DR2);
uint32_t *UploadedOffsetSave = (uint32_t *)(BKP_BASE + BKP_DR4);
uint32_t *WriteDateSave = (uint32_t *)(BKP_BASE + BKP_DR6);
uint32_t *WriteOffsetSave = (uint32_t *)(BKP_BASE + BKP_DR8);
uint32_t *storge_historical_data_addr = (uint32_t *)(BKP_BASE + BKP_DR10);  //是否存储历史数据标志位
#endif


#define TEST

/***********************************************
** Function name:       Memory_BKP_Save
** Descriptions:        掉电保存参数保存
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Memory_BKP_Save(void)
{
    UploadedDateSave[0]     =   UploadedDate    % 0x10000;
    UploadedDateSave[1]     =   UploadedDate    / 0x10000;
    UploadedOffsetSave[0]   =   UploadedOffset  % 0x10000;
    UploadedOffsetSave[1]   =   UploadedOffset  / 0x10000;
    WriteDateSave[0]        =   WriteDate       % 0x10000;
    WriteDateSave[1]        =   WriteDate       / 0x10000;
    WriteOffsetSave[0]      =   WriteOffset     % 0x10000;
    WriteOffsetSave[1]      =   WriteOffset     / 0x10000;
#ifndef DTU3PRO
    *storge_historical_data_addr  =  storge_historical_data_flg;
#endif
}
/***********************************************
** Function name:       Memory_BKP_Read
** Descriptions:        读掉电保存区内存
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Memory_BKP_Read(void)
{
    UploadedDate    = UploadedDateSave[0]   + UploadedDateSave[1]   * 0x10000;
    UploadedOffset  = UploadedOffsetSave[0] + UploadedOffsetSave[1] * 0x10000;
    WriteDate       = WriteDateSave[0]      + WriteDateSave[1]      * 0x10000;
    WriteOffset     = WriteOffsetSave[0]    + WriteOffsetSave[1]    * 0x10000;
#ifndef DTU3PRO
    storge_historical_data_flg = *storge_historical_data_addr;
#endif
}

#endif

/***********************************************
** Function name:       Fs_Init
** Descriptions:        Littlefs文件系统初始化
** input parameters:    无
** output parameters:   无
** Returned value:      1:成功 0:失败
*************************************************/
uint8_t Fs_Init(uint8_t format)
{
    static char fs_init = 0;
    int err;
    lfs_dir_t g_dir;
    char *path;

    if(fs_init == 1)
    {
        return 1;
    }
    else
    {
        fs_init = 1;
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
#ifndef Bootloader
    Queue_init();
#endif


    if(format == 1)
    {
        lf_format(0);
#ifdef DTU3PRO
        lf_format(1);
#endif
    }

    //    SPI_FLASH_BulkErase(0);
    //挂载文件系统
    err = lf_mount(DefaultDisk);
    sprintf(path, "%d:", DefaultDisk);

    //littlefs初始化失败
    if(err != LFS_ERR_OK)
    {
        //格式化
        lf_format(DefaultDisk);
        //重新挂载文件系统
        lf_mount(DefaultDisk);
        err = lf_opendir(DefaultDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            lf_mkdir(DefaultDisk, path);
        }

        lf_closedir(DefaultDisk, &g_dir);
    }
    else
    {
        err = lf_opendir(DefaultDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            //格式化
            lf_format(DefaultDisk);
            //重新挂载文件系统
            lf_mount(DefaultDisk);
            lf_mkdir(DefaultDisk, path);
            //printf("lfs makedir\n");
        }
        else
        {
            lf_closedir(DefaultDisk, &g_dir);
        }

#ifndef Bootloader
        Hardware_DirCheck();
        RealTime_DirCheck(DefaultDisk);
        Download_DirCheck();
        System_DirCheck();
#endif
    }

#ifndef Bootloader
#ifdef DTU3PRO
    //挂载文件系统
    err = lf_mount(ExpansionDisk);
    sprintf(path, "%d:", ExpansionDisk);

    //littlefs初始化失败
    if(err != LFS_ERR_OK)
    {
        //格式化
        lf_format(ExpansionDisk);
        //重新挂载文件系统
        lf_mount(ExpansionDisk);
        err = lf_opendir(ExpansionDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            lf_mkdir(ExpansionDisk, path);
        }

        lf_closedir(ExpansionDisk, &g_dir);
    }
    else
    {
        err = lf_opendir(ExpansionDisk, &g_dir, path);

        if(err == LFS_ERR_NOENT)
        {
            //格式化
            lf_format(ExpansionDisk);
            //重新挂载文件系统
            lf_mount(ExpansionDisk);
            lf_mkdir(ExpansionDisk, path);
            //printf("lfs makedir\n");
        }
        else
        {
            lf_closedir(ExpansionDisk, &g_dir);
        }

        RealTime_DirCheck(ExpansionDisk);
    }

    //    Fatfs_Init();
#endif
#endif
    myfree(path);
    return 1;
}

/***********************************************
** Function name:       File_Write
** Descriptions:        文件系统实时写
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t File_Write(uint8_t Disk, char *FilePath, char *Buffer, uint32_t Offset, uint32_t len)
{
    fs_write write_data;
    int res;
    uint16_t i;
    uint32_t fnum = 0;
    //    for(i=0;i<len;i++)
    //    {
    //    printf("%d\n",Buffer[i]);
    //    }
    lf_open(Disk, &lfnew, FilePath, LFS_O_RDWR | LFS_O_CREAT);
    lf_seek(Disk, &lfnew, Offset);
    lf_write(Disk, &lfnew, Buffer, len, &fnum);
    //printf("Write FilePath:%s\n",FilePath);
    //printf("Write Buffer:%s\n",Buffer);
    //printf("Write Offset:%d\n",Offset);
    //printf("Write len:%d\n",len);
    //printf("fnum:%d\n",fnum);
    lf_close(Disk, &lfnew);
    return fnum;
}
#ifndef Bootloader
/***********************************************
** Function name:       File_Write_Que
** Descriptions:        文件系统队列写
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      无效
*************************************************/
uint16_t File_Write_Que(uint8_t Disk, char *FilePath, char *Buffer, uint32_t Offset, uint32_t len)
{
    fs_write write_data;
    int res;
    uint16_t i;
    uint32_t fnum = 0;
    write_data.path = FilePath;
    write_data.buffer = Buffer;
    write_data.off = Offset;
    write_data.size = len;
    write_data.disk = Disk;
    Queue_enter(write_data);
    //printf("Disk %d\n",Disk);
    //printf("FilePath %s\n",FilePath);
    //printf("Buffer %s\n",Buffer);
    //printf("Offset %d\n",Offset);
    //printf("len %d\n",len);
    return fnum;
}
#endif
/***********************************************
** Function name:       File_Read
** Descriptions:        文件系统实时读
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t File_Read(uint8_t Disk, char *FilePath, char *Buffer, uint32_t Offset, uint32_t len)
{
    int res;
    uint32_t fnum = 0;
    uint16_t i;
    res = lf_open(Disk, &lfnew, FilePath, LFS_O_RDWR);

    if(res == LFS_ERR_OK)
    {
        res = lf_seek(Disk, &lfnew, Offset);
        res = lf_read(Disk, &lfnew, Buffer, len, &fnum);
        //        printf("len %d\n",len);
        //        for(i=0;i<len;i++)
        //        {
        //        printf("%d\n",Buffer[i]);
        //        }
        //printf("Read FilePath:%s\n",FilePath);
        //printf("Read Buffer:%s\n",Buffer);
        //printf("Read Offset:%d\n",Offset);
        //printf("Read len:%d\n",len);
        //printf("fnum:%d\n",fnum);
        lf_close(Disk, &lfnew);
    }

    return fnum;
}

/***********************************************
** Function name:       Dir_Check
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Dir_Check(uint8_t Disk, char *FilePath)
{
    int res;
    char *path;
    lfs_dir_t dir;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    memcpy(path, FilePath, strlen(FilePath));
    res = lf_opendir(Disk, &dir, path);

    /* 获取文件信息 */
    if(res == LFS_ERR_NOENT)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(Disk, path);
    }
    else
    {
        lf_closedir(Disk, &dir);
    }

    myfree(path);
}
/***********************************************
** Function name:       Read_File_Length
** Descriptions:        读取文件大小
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      文件大小
*************************************************/
uint32_t Read_File_Length(uint8_t Disk, char *FilePath)
{
    int res;
    char *path;
    uint32_t size = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    memcpy(path, FilePath, strlen(FilePath));
    /* 获取文件信息 */
    res = lf_stat(Disk, FilePath, &lfno);

    if(res == LFS_ERR_OK)
    {
        size = lfno.size;
    }
    else
    {
        size = 0;
    }

    myfree(path);
    return size;
}
/***********************************************
** Function name:       Download_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_DirCheck(void)
{
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
#ifndef Bootloader
/***********************************************
** Function name:       ClearDownloadFile
** Descriptions:        清空下载目录中的文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void ClearDownloadFile(void)
{
    lfs_dir_t dir;
    int res;
    char *path;
    MIProgram_Offset = 0;
    DTUProgram_Offset = 0;
    GridProfilesList_Offset = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        lf_DeleteAllFiles(DefaultDisk, path);
    }

    myfree(path);
}

/***********************************************
** Function name:       Download_GridProfilesList_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_GridProfilesList_Delete(void)
{
    //System/SystemDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/GridProfilesList.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       Download_MIProgram_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_MIProgram_Delete(void)
{
    //System/SystemDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
    DownOffset = 0;

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/MIProgram.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
#endif

/***********************************************
** Function name:       Download_DTUProgram_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Download_DTUProgram_Delete(void)
{
    //System/SystemDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);
#ifndef Bootloader
    DownOffset = 0;
#endif

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

#ifndef Bootloader
/***********************************************
** Function name:       Download_Write_Length
** Descriptions:        在Download目录下指定位置写指定长度
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t Download_Write_Length(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
{
    int res;
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    Download_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return fnum;
}
#endif
/***********************************************
** Function name:       Download_Read_Length
** Descriptions:        在Download目录下指定位置读指定长度
** input parameters:    *FilePath:文件路径 *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t Download_Read_Length(char *FilePath, char *Buffer, uint32_t Offset, uint16_t len)
{
    int res;
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    fnum = File_Read(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return fnum;
}
/***********************************************
** Function name:       Download_Get_Length
** Descriptions:        获取Download目录下指定文件的大小
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      文件大小
*************************************************/
uint32_t Download_Get_Length(char *FilePath)
{
    uint32_t length;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Download/%s", DefaultDisk, FilePath);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
#ifndef Bootloader
/***********************************************
** Function name:       Download_Get_Dir_Size
** Descriptions:        获取Download目录的大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t Download_Get_Dir_Size(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/GridProfilesList.txt", DefaultDisk);
    length += Download_Get_Length(path);
    sprintf(path, "%d:/Download/MIProgram.hex", DefaultDisk);
    length += Download_Get_Length(path);
    sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
    length += Download_Get_Length(path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       Grid_Profiles_List_Get_Length
** Descriptions:        GridProfilesList.txt文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
uint32_t Grid_Profiles_List_Get_Length(void)
{
    uint32_t ReturnNum;
    GridProfilesList_Offset = 0;
    ReturnNum = Download_Get_Length("GridProfilesList.txt");
    return ReturnNum;
}
/***********************************************
** Function name:       Grid_Profiles_List_Write_Length
** Descriptions:        在GridProfilesList.txt文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t Grid_Profiles_List_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/GridProfilesList.txt
    return Download_Write_Length("GridProfilesList.txt", Buffer, Offset, len);
}
/***********************************************
** Function name:       Grid_Profiles_List_Read_Length
** Descriptions:        在GridProfilesList.txt文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t Grid_Profiles_List_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/GridProfilesList.txt
    return Download_Read_Length("GridProfilesList.txt", Buffer, Offset, len);
}
/***********************************************
** Function name:       MI_Program_Get_Length
** Descriptions:        MIProgram.hex文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
uint32_t MI_Program_Get_Length(void)
{
    uint32_t ReturnNum;
    MIProgram_Offset = 0;
    ReturnNum = Download_Get_Length("MIProgram.hex");
    return ReturnNum;
}
/***********************************************
** Function name:       MI_Program_Write_Length
** Descriptions:        在MIProgram.hex文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t MI_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/MIProgram.hex
    return Download_Write_Length("MIProgram.hex", Buffer, Offset, len);
}
/***********************************************
** Function name:       MI_Program_Read_Length
** Descriptions:        在MIProgram.hex文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t MI_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/MIProgram.hex
    return Download_Read_Length("MIProgram.hex", Buffer, Offset, len);
}
#endif
/***********************************************
** Function name:       DTU_Program_Get_Length
** Descriptions:        DTUProgram.hex文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      文件大小
*************************************************/
uint32_t DTU_Program_Get_Length(void)
{
    uint32_t ReturnNum;
    DTUProgram_Offset = 0;
    ReturnNum = Download_Get_Length("DTUProgram.hex");
    return ReturnNum;
}

uint16_t DTU_Program_Write(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/DTUProgram.hex
    int res;
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, (PathLen));
    sprintf(path, "%d:/Download/DTUProgram.hex", DefaultDisk);
    fnum = File_Write(DefaultDisk, path, Buffer, Offset, len);
    myfree(path);
    return fnum;
}
#ifndef Bootloader
/***********************************************
** Function name:       DTU_Program_Write_Length
** Descriptions:        在DTUProgram.hex文件指定位置写指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t DTU_Program_Write_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/DTUProgram.hex
    return Download_Write_Length("DTUProgram.hex", Buffer, Offset, len);
}
#endif
/***********************************************
** Function name:       DTU_Program_Read_Length
** Descriptions:        在DTUProgram.hex文件指定位置读指定长度
** input parameters:    *Buffer:数据 Offset:文件偏移 len:文件长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint16_t DTU_Program_Read_Length(char *Buffer, uint32_t Offset, uint16_t len)
{
    //Download/MIProgram.hex
    return Download_Read_Length("DTUProgram.hex", Buffer, Offset, len);
}
#ifndef Bootloader
/***********************************************
** Function name:       System_Get_Dir_Size
** Descriptions:        System目录大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t System_Get_Dir_Size(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       System_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void System_DirCheck(void)
{
    //Hardware/HardwareDate.dat
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
/***********************************************
** Function name:       System_DtuDetail_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void System_DtuDetail_Delete(void)
{
    //System/SystemDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       System_DtuDetail_Write
** Descriptions:        将结构体写入flash
** input parameters:    *DtuDetailbuf 结构体指针
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t System_DtuDetail_Write(DtuDetail *DtuDetailbuf)
{
    //System/SystemDate.dat
    int res;
    char *path;
    char *Buffer;
    uint32_t fnum = 0;
    uint16_t crc = 0;
    Buffer = mymalloc(sizeof(DtuDetail) + 2);
    memset(Buffer, 0, sizeof(DtuDetail) + 2);
    memcpy(Buffer, DtuDetailbuf->PropertyMsg, sizeof(DtuDetail));
    crc = MyCrc16((unsigned char *)Buffer, sizeof(DtuDetail));
    Buffer[(sizeof(DtuDetail))] = (uint8_t)crc;
    Buffer[(sizeof(DtuDetail)) + 1] = (uint8_t)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
    System_DtuDetail_Delete();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, sizeof(DtuDetail) + 2);
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       System_DtuDetail_Write
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *DtuDetailbuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t System_DtuDetail_Read(DtuDetail *DtuDetailbuf)
{
    //System/SystemDate.dat
    char *path;
    char *Buffer;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuDetail.dat", DefaultDisk);
    Buffer = mymalloc(sizeof(DtuDetail) + 2);
    memset(Buffer, 0, sizeof(DtuDetail) + 2);
    fnum = File_Read(DefaultDisk, path, Buffer, 0, sizeof(DtuDetail) + 2);
    crc_read = (uint16_t)Buffer[sizeof(DtuDetail)] + (uint16_t)(Buffer[sizeof(DtuDetail) + 1] << 8);
    crc = MyCrc16((unsigned char *)Buffer, sizeof(DtuDetail));

    if(crc == crc_read)
    {
        memcpy(DtuDetailbuf->PropertyMsg, Buffer, sizeof(DtuDetail));
    }

    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       System_DtuMajor_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void System_DtuMajor_Delete(void)
{
    //System/SystemDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       System_DtuMajor_Write
** Descriptions:        将结构体写入flash
** input parameters:    *DtuMajorbuf 结构体指针
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t System_DtuMajor_Write(DtuMajor *DtuMajorbuf)
{
    //System/SystemDate.dat
    int res;
    char *path;
    char *Buffer;
    uint32_t fnum = 0;
    uint16_t crc = 0;
    Buffer = mymalloc(sizeof(DtuMajor) + 2);
    memset(Buffer, 0, sizeof(DtuMajor) + 2);
    memcpy(Buffer, DtuMajorbuf->PropertyMsg, sizeof(DtuMajor));
    crc = MyCrc16((unsigned char *)Buffer, sizeof(DtuMajor));
    Buffer[(sizeof(DtuMajor))] = (uint8_t)crc;
    Buffer[(sizeof(DtuMajor)) + 1] = (uint8_t)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
    System_DtuMajor_Delete();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, sizeof(DtuMajor) + 2);
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       System_DtuMajor_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *DtuMajorbuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t System_DtuMajor_Read(DtuMajor *DtuMajorbuf)
{
    //System/SystemDate.dat
    char *path;
    char *Buffer;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/System/DtuMajor.dat", DefaultDisk);
    Buffer = mymalloc(sizeof(DtuMajor) + 2);
    memset(Buffer, 0, sizeof(DtuMajor) + 2);
    fnum = File_Read(DefaultDisk, path, Buffer, 0, sizeof(DtuMajor) + 2);
    crc_read = (uint16_t)Buffer[sizeof(DtuMajor)] + (uint16_t)(Buffer[sizeof(DtuMajor) + 1] << 8);
    crc = MyCrc16((unsigned char *)Buffer, sizeof(DtuMajor));

    if(crc == crc_read)
    {
        memcpy(DtuMajorbuf->PropertyMsg, Buffer, sizeof(DtuMajor));
    }

    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       Hardware_Get_Dir_Size
** Descriptions:        Hardware目录大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t Hardware_Get_Dir_Size(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    length += Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       Hardware_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    *FilePath:文件路径
** output parameters:   无
** Returned value:      无
*************************************************/
void Hardware_DirCheck(void)
{
    //Hardware/HardwareDate.dat
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    Dir_Check(DefaultDisk, path);
    myfree(path);
}
/***********************************************
** Function name:       InverterMajor_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void InverterMajor_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       InverterMajor_Write
** Descriptions:        将结构体写入flash
** input parameters:    *InverterMajorBuf 结构体指针  Pv_nub个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t InverterMajor_Write(InverterMajor *InverterMajorBuf, uint16_t Pv_nub)
{
    //Hardware/InverterMajor.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint32_t fnum = 0;
    uint16_t crc = 0;
    Buffer = mymalloc((Pv_nub * sizeof(InverterMajor)) + 4);
    memset(Buffer, 0, (Pv_nub * sizeof(InverterMajor)) + 4);
    Buffer[0] = (uint8_t)Pv_nub;
    Buffer[1] = (uint8_t)(Pv_nub >> 8);

    for(i = 0; i < Pv_nub; i++)
    {
        for(j = 0; j < sizeof(InverterMajor); j++)
        {
            Buffer[(i * sizeof(InverterMajor)) + 2 + j] = InverterMajorBuf[i].PropertyMsg[j];
        }
    }

    crc = MyCrc16((unsigned char *)&Buffer[2], i * sizeof(InverterMajor));
    Buffer[(i * sizeof(InverterMajor)) + 2] = (uint8_t)crc;
    Buffer[(i * sizeof(InverterMajor)) + 3] = (uint8_t)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
    Hardware_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, (Pv_nub * sizeof(InverterMajor)) + 4);
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       InverterMajor_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *InverterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t InverterMajor_Read(InverterMajor *InverterMajorBuf)
{
    //Hardware/InverterMajor.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint16_t Pv_nub = 0;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterMajor.dat", DefaultDisk);
    Buffer = mymalloc((PORT_LEN * sizeof(InverterMajor)) + 4);
    memset(Buffer, 0, ((PORT_LEN * sizeof(InverterMajor)) + 4));
    fnum = File_Read(DefaultDisk, path, Buffer, 0, ((PORT_LEN * sizeof(InverterMajor)) + 4));
    Pv_nub = (uint16_t)Buffer[0] + (uint16_t)(Buffer[1] << 8);

    if(Pv_nub <= PORT_LEN)
    {
        crc_read = (uint16_t)Buffer[Pv_nub * sizeof(InverterMajor) + 2] + (uint16_t)(Buffer[Pv_nub * sizeof(InverterMajor) + 3] << 8);
        crc = MyCrc16((unsigned char *)&Buffer[2], Pv_nub * sizeof(InverterMajor));

        if(crc == crc_read)
        {
            for(i = 0; i < Pv_nub; i++)
            {
                for(j = 0; j < sizeof(InverterMajor); j++)
                {
                    InverterMajorBuf[i].PropertyMsg[j] = Buffer[(i * sizeof(InverterMajor)) + 2 + j];
                }
            }
        }
    }
    else
    {
        Pv_nub = 0;
    }

    myfree(path);
    myfree(Buffer);
    return Pv_nub;
}
/***********************************************
** Function name:       InverterDetail_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void InverterDetail_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       InverterDetail_Write
** Descriptions:        将结构体写入flash
** input parameters:    *InverterDetailBuf 结构体指针 InverterDetailOff偏移 InverterNum读的个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t InverterDetail_Write(InverterDetail *InverterDetailBuf, uint16_t InverterDetailOff, uint16_t InverterNum)
{
    //Hardware/HardwareDate.dat
    int res;
    char *path;
    char *Buffer;
    uint32_t off;
    uint16_t i, j = 0;
    uint32_t fnum = 0;
    uint32_t crc = 0;
    Buffer = mymalloc(InverterNum * (sizeof(InverterDetail) + 2));
    memset(Buffer, 0, InverterNum * (sizeof(InverterDetail) + 2));

    for(i = 0; i < InverterNum; i++)
    {
        for(j = 0; j < sizeof(InverterDetail); j++)
        {
            Buffer[i * (sizeof(InverterDetail) + 2) + j] = InverterDetailBuf[i].PropertyMsg[j];
        }

        crc = MyCrc16((unsigned char *)&Buffer[i * (sizeof(InverterDetail) + 2)], sizeof(InverterDetail));
        Buffer[i * (sizeof(InverterDetail) + 2) + sizeof(InverterDetail)] = (uint8_t)crc;
        Buffer[i * (sizeof(InverterDetail) + 2) + sizeof(InverterDetail) + 1] = (uint8_t)(crc >> 8);
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    off = InverterDetailOff * (sizeof(InverterDetail) + 2);
    Hardware_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, off, InverterNum * (sizeof(InverterDetail) + 2));
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       InverterDetail_Read
** Descriptions:        读取结构体
** input parameters:    无
** output parameters:   *InverterDetailBuf 结构体指针 InverterDetailOff偏移 InverterNum读的个数
** Returned value:      成功个数
*************************************************/
uint16_t InverterDetail_Read(InverterDetail *InverterDetailBuf, uint16_t InverterDetailOff, uint16_t InverterNum)
{
    //Hardware/HardwareDate.dat
    char *path;
    char *Buffer;
    uint32_t off;
    uint16_t i, j;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterDetail.dat", DefaultDisk);
    Buffer = mymalloc(sizeof(InverterDetail) + 2);
    memset(Buffer, 0, sizeof(InverterDetail) + 2);
    off = InverterDetailOff * (sizeof(InverterDetail) + 2);

    for(i = 0; i < InverterNum; i++)
    {
        fnum = File_Read(DefaultDisk, path, Buffer, off + i * (sizeof(InverterDetail) + 2), sizeof(InverterDetail) + 2);
        crc_read = (uint16_t)Buffer[sizeof(InverterDetail)] + (uint16_t)(Buffer[sizeof(InverterDetail) + 1] << 8);
        crc = MyCrc16((unsigned char *)Buffer, sizeof(InverterDetail));

        if(crc == crc_read)
        {
            for(j = 0; j < sizeof(InverterDetail); j++)
            {
                InverterDetailBuf[i].PropertyMsg[j] = Buffer[j];
            }
        }

        memset(Buffer, 0, sizeof(InverterDetail) + 2);
    }

    myfree(path);
    myfree(Buffer);
    return fnum;
}

/***********************************************
** Function name:       InverterReal_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void InverterReal_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       InverterReal_Write
** Descriptions:        将结构体写入flash
** input parameters:    *InverterRealBuf 结构体指针  Pv_nub个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t InverterReal_Write(InverterReal *InverterRealBuf, uint16_t Pv_nub)
{
    //Hardware/InverterReal.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint32_t fnum = 0;
    uint16_t crc = 0;
    Buffer = mymalloc(Pv_nub * sizeof(uint32_t) +4);
    memset(Buffer, 0, Pv_nub * sizeof(uint32_t) +4);
    Buffer[0] = (uint8_t)Pv_nub;
    Buffer[1] = (uint8_t)(Pv_nub >> 8);

    for(i = 0; i < Pv_nub; i++)
    {
        Buffer[(i * sizeof(uint32_t)) + 2 + 0] = (uint8_t)InverterRealBuf[i].Data.HistoryEnergyL;
        Buffer[(i * sizeof(uint32_t)) + 2 + 1] = (uint8_t)(InverterRealBuf[i].Data.HistoryEnergyL >> 8);
        Buffer[(i * sizeof(uint32_t)) + 2 + 2] = (uint8_t)InverterRealBuf[i].Data.HistoryEnergyH;
        Buffer[(i * sizeof(uint32_t)) + 2 + 3] = (uint8_t)(InverterRealBuf[i].Data.HistoryEnergyH >> 8);
    }

    crc = MyCrc16((unsigned char *)&Buffer[2], i * sizeof(uint32_t));
    Buffer[(i * sizeof(uint32_t)) + 2] = (uint8_t)crc;
    Buffer[(i * sizeof(uint32_t)) + 3] = (uint8_t)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
    Hardware_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, (Pv_nub * sizeof(uint32_t)) + 4);
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       InverterReal_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *InverterRealBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t InverterReal_Read(InverterReal *InverterRealBuf)
{
    //Hardware/InverterReal.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint16_t Pv_nub = 0;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/InverterReal.dat", DefaultDisk);
    Buffer = mymalloc((PORT_LEN * sizeof(uint32_t)) + 4);
    memset(Buffer, 0, ((PORT_LEN * sizeof(uint32_t)) + 4));
    fnum = File_Read(DefaultDisk, path, Buffer, 0, ((PORT_LEN * sizeof(uint32_t)) + 4));
    Pv_nub = (uint16_t)Buffer[0] + (uint16_t)(Buffer[1] << 8);

    if(Pv_nub <= PORT_LEN)
    {
        crc_read = (uint16_t)Buffer[Pv_nub * sizeof(uint32_t) + 2] + (uint16_t)(Buffer[Pv_nub * sizeof(uint32_t) + 3] << 8);
        crc = MyCrc16((unsigned char *)&Buffer[2], Pv_nub * sizeof(uint32_t));

        if(crc == crc_read)
        {
            for(i = 0; i < Pv_nub; i++)
            {
                InverterRealBuf[i].Data.HistoryEnergyL = (uint16_t)Buffer[(i * sizeof(uint32_t)) + 2 + 0] + ((uint16_t)Buffer[(i * sizeof(uint32_t)) + 2 + 1] << 8);
                InverterRealBuf[i].Data.HistoryEnergyH = (uint16_t)Buffer[(i * sizeof(uint32_t)) + 2 + 2] + ((uint16_t)Buffer[(i * sizeof(uint32_t)) + 2 + 3] << 8);
            }
        }
    }
    else
    {
        Pv_nub = 0;
    }

    myfree(path);
    myfree(Buffer);
    return Pv_nub;
}

#ifdef DTU3PRO
/***********************************************
** Function name:       MeterMajor_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MeterMajor_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       MeterMajor_Write
** Descriptions:        将结构体写入flash
** input parameters:    *MeterMajorBuf 结构体指针  Pv_nub个数
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint16_t MeterInfor_Write(MeterMajor *MeterMajorBuf, uint16_t Meter_nub)
{
    //Hardware/InverterReal.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint32_t fnum = 0;
    uint16_t crc = 0;
    Buffer = mymalloc(Meter_nub * sizeof(uint32_t) +4);
    memset(Buffer, 0, Meter_nub * sizeof(uint32_t) +4);
    Buffer[0] = (uint8_t)Meter_nub;
    Buffer[1] = (uint8_t)(Meter_nub >> 8);

    for(i = 0; i < Meter_nub; i++)
    {
        for(j = 0; j < sizeof(MeterMajor); j++)
        {
            Buffer[(i * sizeof(MeterMajor)) + 2 + j] = (uint8_t)MeterMajorBuf[i].PropertyMsg[j];
        }
    }

    crc = MyCrc16((unsigned char *)&Buffer[2], i * sizeof(MeterMajor));
    Buffer[(i * sizeof(MeterMajor)) + 2] = (uint8_t)crc;
    Buffer[(i * sizeof(MeterMajor)) + 3] = (uint8_t)(crc >> 8);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    Hardware_DirCheck();
    fnum = File_Write_Que(DefaultDisk, path, Buffer, 0, (Meter_nub * sizeof(MeterMajor)) + 4);
    myfree(path);
    myfree(Buffer);
    return fnum;
}
/***********************************************
** Function name:       MeterMajor_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t MeterMajor_Read(MeterMajor *MeterMajorBuf)
{
    //Hardware/MeterMajor.dat
    char *path;
    char *Buffer;
    uint16_t i, j;
    uint16_t Meter_nub = 0;
    uint16_t crc = 0;
    uint16_t crc_read = 0;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Hardware/MeterMajor.dat", DefaultDisk);
    Buffer = mymalloc((PORT_LEN * sizeof(MeterMajor)) + 4);
    memset(Buffer, 0, ((PORT_LEN * sizeof(MeterMajor)) + 4));
    fnum = File_Read(DefaultDisk, path, Buffer, 0, ((PORT_LEN * sizeof(MeterMajor)) + 4));
    Meter_nub = (uint16_t)Buffer[0] + (uint16_t)(Buffer[1] << 8);

    if(Meter_nub <= PORT_LEN)
    {
        crc_read = (uint16_t)Buffer[Meter_nub * sizeof(MeterMajor) + 2] + (uint16_t)(Buffer[Meter_nub * sizeof(MeterMajor) + 3] << 8);
        crc = MyCrc16((unsigned char *)&Buffer[2], Meter_nub * sizeof(MeterMajor));

        if(crc == crc_read)
        {
            for(i = 0; i < Meter_nub; i++)
            {
                for(j = 0; j < sizeof(MeterMajor); j++)
                {
                    MeterMajorBuf[i].PropertyMsg[j] = (uint8_t)Buffer[(i * sizeof(MeterMajor)) + 2 + j];
                }

                MeterInfo[i].sn = (MeterMajorBuf[i].Property.Id[2] % 16) * 100 + (MeterMajorBuf[i].Property.Id[3] / 16) * 10 + (MeterMajorBuf[i].Property.Id[3] % 16);
            }
        }
    }
    else
    {
        Meter_nub = 0;
    }

    myfree(path);
    myfree(Buffer);
    return Meter_nub;
}

#endif

/***********************************************
** Function name:       DTU_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void DTU_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/DTU_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       DTU_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t DTU_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       DTU_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t DTU_Data_Write(char *DTU_Data_Buf, uint32_t DTU_Data_Len, uint32_t DTU_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, DTU_Data_Buf, DTU_Data_Off, DTU_Data_Len);
    myfree(path);
    return fnum;
}
/***********************************************
** Function name:       DTU_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t DTU_Data_Read(char *DTU_Data_Buf, uint32_t DTU_Data_Len, uint32_t DTU_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, DTU_Data_Buf, DTU_Data_Off, DTU_Data_Len);
    myfree(path);
    return fnum;
}


/***********************************************
** Function name:       DTU_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void DTU_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       DTU_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t DTU_Cfg_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       DTU_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t DTU_Cfg_Data_Write(char *DTU_Cfg_Data_Buf, uint32_t DTU_Cfg_Data_Len, uint32_t DTU_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, DTU_Cfg_Data_Buf, DTU_Cfg_Data_Off, DTU_Cfg_Data_Len);
    myfree(path);
    return fnum;
}
/***********************************************
** Function name:       DTU_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t DTU_Cfg_Data_Read(char *DTU_Cfg_Data_Buf, uint32_t DTU_Cfg_Data_Len, uint32_t DTU_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/DTU_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, DTU_Cfg_Data_Buf, DTU_Cfg_Data_Off, DTU_Cfg_Data_Len);
    myfree(path);
    return fnum;
}




/***********************************************
** Function name:       MI_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MI_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}
/***********************************************
** Function name:       MI_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t MI_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}
/***********************************************
** Function name:       MI_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t MI_Data_Write(char *MI_Data_Buf, uint32_t MI_Data_Len, uint32_t MI_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, MI_Data_Buf, MI_Data_Off, MI_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       MI_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t MI_Data_Read(char *MI_Data_Buf, uint32_t MI_Data_Len, uint32_t MI_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, MI_Data_Buf, MI_Data_Off, MI_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       MI_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void MI_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       MI_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t MI_Cfg_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       MI_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t MI_Cfg_Data_Write(char *MI_Cfg_Data_Buf, uint32_t MI_Cfg_Data_Len, uint32_t MI_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, MI_Cfg_Data_Buf, MI_Cfg_Data_Off, MI_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       MI_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t MI_Cfg_Data_Read(char *MI_Cfg_Data_Buf, uint32_t MI_Cfg_Data_Len, uint32_t MI_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/MI_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, MI_Cfg_Data_Buf, MI_Cfg_Data_Off, MI_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Grid_Profiles_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       Grid_Profiles_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t Grid_Profiles_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t Grid_Profiles_Data_Write(char *Grid_Profiles_Data_Buf, uint32_t Grid_Profiles_Data_Len, uint32_t Grid_Profiles_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, Grid_Profiles_Data_Buf, Grid_Profiles_Data_Off, Grid_Profiles_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t Grid_Profiles_Data_Read(char *Grid_Profiles_Data_Buf, uint32_t Grid_Profiles_Data_Len, uint32_t Grid_Profiles_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, Grid_Profiles_Data_Buf, Grid_Profiles_Data_Off, Grid_Profiles_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Delete
** Descriptions:        删除文件
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void Grid_Profiles_Cfg_Data_Delete(void)
{
    //Hardware/HardwareDate.dat
    int res;
    lfs_dir_t dir;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config", DefaultDisk);
    res = lf_opendir(DefaultDisk, &dir, (const char *)path);

    if(res != LFS_ERR_OK)
    {
        /* 打开目录失败，就创建目录 */
        res = lf_mkdir(DefaultDisk, (const char *)path);
    }
    else
    {
        /* 如果目录已经存在，关闭它 */
        res = lf_closedir(DefaultDisk, &dir);
        sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
        res = lf_stat(DefaultDisk, path, &lfno);

        if(res == LFS_ERR_OK)
        {
            /* 删除文件 */
            lf_unlink(DefaultDisk, path);
        }
    }

    myfree(path);
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Get_Length
** Descriptions:        文件大小
** input parameters:    无
** output parameters:   无
** Returned value:      目录大小
*************************************************/
uint32_t Grid_Profiles_Cfg_Data_Get_Length(void)
{
    uint32_t length = 0;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    length = Read_File_Length(DefaultDisk, path);
    myfree(path);
    return length;
}

/***********************************************
** Function name:       Grid_Profiles_CFG_Data_Write
** Descriptions:        写入flash
** input parameters:
** output parameters:   无
** Returned value:      成功个数
*************************************************/
uint32_t Grid_Profiles_Cfg_Data_Write(char *Grid_Profiles_Cfg_Data_Buf, uint32_t Grid_Profiles_Cfg_Data_Len, uint32_t Grid_Profiles_Cfg_Data_Off)
{
    //Hardware/InverterReal.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    fnum = File_Write_Que(DefaultDisk, path, Grid_Profiles_Cfg_Data_Buf, Grid_Profiles_Cfg_Data_Off, Grid_Profiles_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       Grid_Profiles_Cfg_Data_Read
** Descriptions:        读出结构体
** input parameters:    无
** output parameters:   *MeterMajorBuf 结构体指针
** Returned value:      成功个数
*************************************************/
uint16_t Grid_Profiles_Cfg_Data_Read(char *Grid_Profiles_Cfg_Data_Buf, uint32_t Grid_Profiles_Cfg_Data_Len, uint32_t Grid_Profiles_Cfg_Data_Off)
{
    //Hardware/MeterMajor.dat
    char *path;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Config/Grid_Profiles_Cfg_Data.dat", DefaultDisk);
    fnum = File_Read(DefaultDisk, path, Grid_Profiles_Cfg_Data_Buf, Grid_Profiles_Cfg_Data_Off, Grid_Profiles_Cfg_Data_Len);
    myfree(path);
    return fnum;
}

/***********************************************
** Function name:       RealTime_DirCheck
** Descriptions:        目录校验,目录不存在则创建
** input parameters:    Disk 硬盘盘符
** output parameters:   无
** Returned value:      无
*************************************************/
void RealTime_DirCheck(uint8_t Disk)
{
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", Disk);
    Dir_Check(Disk, path);
    myfree(path);
}

/***********************************************
** Function name:       Date_List_Write
** Descriptions:
** input parameters:
** output parameters:   无
** Returned value:      无
*************************************************/
uint32_t Date_List_Write(uint8_t Disk, uint32_t Date, uint32_t *Offset, uint8_t PackNum, uint8_t PackAll, uint32_t StartAddr, uint32_t EndAddr, uint32_t Len, uint8_t Mark)
{
    char *path;
    char *pSaveBuffer;
    int res;
    uint32_t BuffLen = 0;
    uint32_t length = 2048;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Disk, (uint16_t)(Date / 10000), (uint8_t)((Date % 10000) / 100), (Date % 100));
    pSaveBuffer = mymalloc(length);
    memset(pSaveBuffer, 0, length);
    /*包头*/
    sprintf(pSaveBuffer, "ListStart:");
    BuffLen = strlen(pSaveBuffer);
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = strlen(pSaveBuffer);
    /*起始地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", StartAddr);
    BuffLen = strlen(pSaveBuffer);
    /*结束地址*/
    sprintf(&pSaveBuffer[BuffLen], "%010d", EndAddr);
    BuffLen = strlen(pSaveBuffer);
    /*4字节数据长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = strlen(pSaveBuffer);

    /*数据包发送状态*/
    switch(Mark)
    {
        case 0:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Failure");
            break;

        case 1:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Success");
            break;

        case 2:
            sprintf(&pSaveBuffer[BuffLen], "Mark:Between");
            break;
    }

    BuffLen = strlen(pSaveBuffer);
    /*包尾*/
    sprintf(&pSaveBuffer[BuffLen], "ListEnd");
    BuffLen = strlen(pSaveBuffer);
    lf_open(Disk, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
    lf_seek(Disk, &lfnew, *Offset);
    lf_write(Disk, &lfnew, pSaveBuffer, BuffLen, &fnum);
    lf_close(Disk, &lfnew);
    //printf("Write FilePath:%s\n",FilePath);
    //printf("Write Buffer:%s\n",Buffer);
    //printf("Write Offset:%d\n",Offset);
    //printf("Write len:%d\n",len);
    //printf("fnum:%d\n",fnum);
    *Offset = *Offset + BuffLen;
    myfree(path);
    myfree(pSaveBuffer);
    return BuffLen;
}

uint32_t Date_List_Read(uint8_t Disk, uint32_t Date, uint32_t Offset, uint8_t *PackNum, uint8_t *PackAll, uint32_t *StartAddr, uint32_t *EndAddr, uint32_t *Len, uint8_t *Mark)
{
    int res;
    char *path;
    char *pReadBuffer;
    uint32_t read_len = 0;
    uint32_t i = 0;
    uint32_t BuffLen = 0;
    uint32_t length = 2048;
    uint32_t fnum = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", Disk, (uint16_t)(Date / 10000), (uint8_t)((Date % 10000) / 100), (Date % 100));
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, length);
    read_len = File_Read(Disk, path, pReadBuffer, Offset, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "ListStart:", LIST_START_LEN) == 0)
        {
            i += LIST_START_LEN;
            /*获取第几包*/
            *PackNum = astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*起始地址*/
            *StartAddr = astr2int(&pReadBuffer[i], START_ADDR_LEN);
            i += START_ADDR_LEN;
            /*结束地址*/
            *EndAddr = astr2int(&pReadBuffer[i], END_ADDR_LEN);
            i += END_ADDR_LEN;
            *Len = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;

            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                i = i + MARK_LEN + MARK_RES_LEN;
                *Mark = 2;
                break;
            }

            i = i + LIST_END_LEN;
            break;
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return read_len;
}

void Date_List_RealTime_Mark(uint8_t mark)
{
    static uint32_t FailureOffset = 0;
    static uint8_t FailureTimes = 0;
    char *data;
    char *path;
    char *pReadBuffer;
    uint32_t fnum = 0;
    uint32_t Offset = 0;
    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t length = 2048;
    uint32_t Read_len = 0;
    uint32_t find = 0;
    int32_t offset = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
    Memory_BKP_Read();
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", StorageLocat, (uint16_t)(WriteDate / 10000), (uint8_t)((WriteDate % 10000) / 100), (WriteDate % 100));

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    find = 0;

    for(;;)
    {
        Read_len = File_Read(StorageLocat, path, pReadBuffer, (WriteOffset - offset), length);

        if(Read_len == 0)
        {
            if(offset < WriteOffset)
            {
                offset = offset + LIST_NEXT;
            }
            else
            {
                return;
            }
        }

        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                Offset = Offset + LIST_NEXT;
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                return;
            }
        }

        if(find == 1)
        {
            break;
        }
    }

    if(find == 1)
    {
        if(strncmp(&pReadBuffer[i - j], data, MARK_LEN + MARK_RES_LEN) != 0)
        {
            lf_open(StorageLocat, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
            lf_seek(StorageLocat, &lfnew, WriteOffset - offset + i);
            lf_write(StorageLocat, &lfnew, data, strlen(data), &fnum);
            lf_close(StorageLocat, &lfnew);
        }

        i = i + MARK_LEN + MARK_RES_LEN;
    }

    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}
void Date_List_History_Mark(uint8_t mark)
{
    static uint32_t FailureOffset = 0;
    static uint8_t FailureTimes = 0;
    char *data;
    char *path;
    char *pReadBuffer;
    uint8_t UseStorage = DefaultDisk;
    uint32_t fnum = 0;
    uint32_t Offset = 0;
    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t length = 2048;
    uint32_t Read_len = 0;
    uint32_t find = 0;
    data = mymalloc(100);
    memset(data, 0, 100);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pReadBuffer = mymalloc(length);
    memset(pReadBuffer, 0, (length));
    Memory_BKP_Read();
#ifdef DTU3PRO

    for(i = 0; i < 2; i++)
    {
        if(i == 0)
        {
            UseStorage = DefaultDisk;
            sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", UseStorage, (uint16_t)(UploadedDate / 10000), (uint8_t)((UploadedDate % 10000) / 100), (UploadedDate % 100));
            Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset, length);

            if(Read_len != 0)
            {
                break;
            }
        }
        else if(i == 1)
        {
            UseStorage = ExpansionDisk;
            sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", UseStorage, (uint16_t)(UploadedDate / 10000), (uint8_t)((UploadedDate % 10000) / 100), (UploadedDate % 100));
            Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset, length);

            if(Read_len != 0)
            {
                break;
            }
        }
    }

#else
    UseStorage = DefaultDisk;
    sprintf(path, "%d:/Data/%04d%02d%02d_list.dat", UseStorage, (uint16_t)(UploadedDate / 10000), (uint8_t)((UploadedDate % 10000) / 100), (UploadedDate % 100));
    Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset, length);
#endif

    if(mark == 1)
    {
        sprintf(data, "Mark:Success");
    }
    else if(mark == 0)
    {
        sprintf(data, "Mark:Failure");
    }
    else if(mark == 2)
    {
        sprintf(data, "Mark:Between");
    }

    if(Read_len != 0)
    {
        for(i = 0; i < Read_len; i++)
        {
            if(strncmp(&pReadBuffer[i], "Mark:Failure", MARK_LEN + MARK_RES_LEN) == 0)
            {
                find = 1;
                //printf("find head\n");
                //printf("%d\n", i);
                break;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Between", MARK_LEN + MARK_RES_LEN) == 0)
            {
                Offset = Offset + LIST_NEXT;
                memset(pReadBuffer, 0, (length));
                Read_len = File_Read(UseStorage, path, pReadBuffer, UploadedOffset + Offset, length);
                i = 0;
            }
            else if(strncmp(&pReadBuffer[i], "Mark:Success", MARK_LEN + MARK_RES_LEN) == 0)
            {
                myfree(data);
                myfree(path);
                myfree(pReadBuffer);
                return;
            }
        }

        if(find == 1)
        {
            if(strncmp(&pReadBuffer[i - j], data, MARK_LEN + MARK_RES_LEN) != 0)
            {
                lf_open(UseStorage, &lfnew, path, LFS_O_RDWR | LFS_O_CREAT);
                lf_seek(UseStorage, &lfnew, UploadedOffset + Offset + i);
                lf_write(UseStorage, &lfnew, data, strlen(data), &fnum);
                lf_close(UseStorage, &lfnew);
            }

            i = i + MARK_LEN + MARK_RES_LEN;

            if(mark == 1)
            {
                UploadedOffset = UploadedOffset + InsideOffset + i;
                InsideOffset = 0;
                FailureOffset = 0;
                FailureTimes = 0;
            }
            else if(mark == 0)
            {
                if(UploadedOffset == FailureOffset)
                {
                    if(FailureTimes >= 3)
                    {
                        UploadedOffset = UploadedOffset + InsideOffset + i;
                        InsideOffset = 0;
                        FailureOffset = 0;
                        FailureTimes = 0;
                    }
                    else
                    {
                        FailureTimes++;
                        InsideOffset = 0;
                    }
                }
                else
                {
                    FailureOffset = UploadedOffset;
                    FailureTimes++;
                }
            }
        }
    }

    Memory_BKP_Save();
    myfree(data);
    myfree(path);
    myfree(pReadBuffer);
}

uint8_t Historical_Presence(void)
{
    uint32_t UploadedSec = 0;
    uint32_t ScanningTimes = 0;
    char *path;
    char *pReadBuffer;
    uint32_t read_len = 0;
    uint32_t BetweenLen = 0;
    uint32_t backlen = 0;
    uint32_t length = 2048;
    int32_t offset = 0;
    uint32_t i = 0, j = 0;
    uint16_t crc;
    uint16_t crc_read;
    uint8_t UseStorage = DefaultDisk;
    uint8_t find = 0;
    uint8_t hour = 0;
    uint8_t min = 0;
    uint8_t sec = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    uint8_t GetPackNum = 0;
    uint8_t GetPackAll = 0;
    uint32_t GetStartAddr = 0;
    uint32_t GetEndAddr = 0;
    uint32_t GetLen = 0;
    uint32_t DateRange = 0;
    uint8_t GetMark = 0;
    uint8_t Mark = 0;
    uint32_t res = 0;
    calendar_obj calendar;
    RTC_GetTimes(&calendar);
    Memory_BKP_Read();
    pReadBuffer = mymalloc(length);

    if(WriteDate != (calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date))
    {
        WriteDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        WriteOffset = 0;
    }

    DateRange = calendar.w_year * 10000 + (calendar.w_month - 1) * 100 + calendar.w_date;

    if(UploadedDate == 0)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate > WriteDate)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate < DateRange)
    {
        UploadedDate = DateRange;
        UploadedOffset = 0;
        InsideOffset = 0;
    }

    for(;;)
    {
#ifdef DTU3PRO

        for(i = 0;; i++)
        {
            if((i % 2) == 0)
            {
                UseStorage = DefaultDisk;
                res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

                if(res != 0)
                {
                    break;
                }
            }
            else if((i % 2) == 1)
            {
                UseStorage = ExpansionDisk;
                res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

                if(res != 0)
                {
                    break;
                }
            }

            if((i % 2) == 1)
            {
                if(UploadedDate < WriteDate)
                {
                    UploadedDate++;
                    calendar.w_year = UploadedDate / 10000;
                    calendar.w_month = (UploadedDate / 100) % 100;
                    calendar.w_date = UploadedDate % 100;
                    UploadedSec = DateToSec(calendar);
                    SecToDate(UploadedSec, &calendar);
                    UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                    UploadedOffset = 0;
                    Memory_BKP_Save();
                }
                else if(UploadedDate == WriteDate)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    UploadedDate = WriteDate;
                    UploadedOffset = 0;
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }

#else
        UseStorage = DefaultDisk;
        res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);
#endif

        if(res == 0)
        {
            if(UploadedDate < WriteDate)
            {
                UploadedDate++;
                calendar.w_year = UploadedDate / 10000;
                calendar.w_month = (UploadedDate / 100) % 100;
                calendar.w_date = UploadedDate % 100;
                UploadedSec = DateToSec(calendar);
                SecToDate(UploadedSec, &calendar);
                UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                UploadedOffset = 0;
                Memory_BKP_Save();
            }
            else if(UploadedDate == WriteDate)
            {
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                UploadedDate = WriteDate;
                UploadedOffset = 0;
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
        }
        else
        {
            break;
        }
    }

    for(;;)
    {
        if(GetMark == 0)
        {
            if((GetPackNum == 0) && (GetPackAll == 0) && (GetStartAddr == 0) && (GetEndAddr == 0))
            {
                if(UploadedDate < WriteDate)
                {
                    UploadedDate++;
                    calendar.w_year = UploadedDate / 10000;
                    calendar.w_month = (UploadedDate / 100) % 100;
                    calendar.w_date = UploadedDate % 100;
                    UploadedSec = DateToSec(calendar);
                    SecToDate(UploadedSec, &calendar);
                    UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                    UploadedOffset = 0;
                    Memory_BKP_Save();
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            if(GetMark == 1)
            {
                UploadedOffset += offset;
                offset = 0;
                Memory_BKP_Save();
                ScanningTimes++;

                if(ScanningTimes >= 100)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }

            offset += LIST_NEXT;
            res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

            if(res == 0)
            {
                if(UploadedDate < WriteDate)
                {
                    UploadedDate++;
                    calendar.w_year = UploadedDate / 10000;
                    calendar.w_month = (UploadedDate / 100) % 100;
                    calendar.w_date = UploadedDate % 100;
                    UploadedSec = DateToSec(calendar);
                    SecToDate(UploadedSec, &calendar);
                    UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                    UploadedOffset = 0;
                    Memory_BKP_Save();
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }
    }

    myfree(path);
    myfree(pReadBuffer);
    return 1;
}
/***********************************************
** Function name:       Local_Historical_Data_Read
** Descriptions:        读取历史数据
** input parameters:    Disk 盘符 Date日期  Offset读偏移  len长度
** output parameters:   *Buffer读出的数据
** Returned value:      成功长度
*************************************************/
uint16_t Local_Historical_Data_Read(uint8_t Disk, uint32_t Date, char *Buffer, uint16_t Offset, uint16_t len)
{
    //Data/20190124/HistoricalData.dat
    char *path;
    uint32_t fnum = 0;
    calendar_obj calendar;
    RTC_GetTimes(&calendar);
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", Disk, (uint16_t)(Date / 10000), (uint8_t)((Date % 10000) / 100), (Date % 100));
    fnum = File_Read(Disk, path, Buffer, Offset, len);
    myfree(path);
    return fnum;
}
/***********************************************
** Function name:       RealTime_Data_Pack
** Descriptions:        实时数据打包成存储格式
** input parameters:    PackNum当前包数 PackAll总包数 *Data数据 len长度
** output parameters:   无
** Returned value:      成功长度
*************************************************/
uint32_t RealTime_Data_Pack(uint8_t PackNum, uint8_t PackAll, char *Data, uint32_t Len)
{
    uint16_t crc = 0;
    uint32_t fsize = 0;
    char *path;
    char *pSaveBuffer;
    uint32_t BuffLen = 0;
    uint8_t LastPackNum = 0;
    uint8_t LastPackAll = 0;
    uint32_t LastStartAddr = 0;
    uint32_t LastEndAddr = 0;
    uint32_t LastLen = 0;
    uint8_t LastMark = 0;
    uint8_t Mark = 0;
    uint32_t res = 0;
    calendar_obj calendar;
    RTC_GetTimes(&calendar);

    if(Len == 0)
    {
        return 0;
    }

    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    pSaveBuffer = mymalloc((Len + 256));
    memset(pSaveBuffer, 0, (Len + 256));
    Memory_BKP_Read();

    if(WriteDate != (calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date))
    {
        WriteDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        WriteOffset = 0;
    }

    if(WriteOffset >= LIST_NEXT)
    {
        res = Date_List_Read(StorageLocat, WriteDate, WriteOffset - LIST_NEXT, &LastPackNum, &LastPackAll, &LastStartAddr, &LastEndAddr, &LastLen, &LastMark);
    }

    if(res == 0)
    {
        LastPackNum = 0;
        LastPackAll = 0;
        LastStartAddr = 0;
        LastEndAddr = 0;
        LastLen = 0;
        LastMark = 0;
    }

    BuffLen = 0;
    /*包头Start*/
    sprintf(pSaveBuffer, "Start:");
    BuffLen = BuffLen + START_LEN;
    /*第几包/包数*/
    sprintf(&pSaveBuffer[BuffLen], "%02d/%02d", PackNum, PackAll);
    BuffLen = BuffLen + PACK_NO_LEN + PACK_ALL_LEN;
    /*xx:xx:xx时间*/
    sprintf(&pSaveBuffer[BuffLen], "%02d:%02d:%02d", calendar.hour, calendar.min, calendar.sec);
    BuffLen = BuffLen + HOUR_LEN + MIN_LEN + SEC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", Len);
    BuffLen = BuffLen + DATA_LEN;
    /*数据*/
    memcpy(&pSaveBuffer[BuffLen], Data, Len);
    crc = MyCrc16((unsigned char *)&pSaveBuffer[BuffLen], Len);
    BuffLen  = BuffLen + Len;
    /*数据校验*/
    sprintf(&pSaveBuffer[BuffLen], "%04x", crc);
    BuffLen = BuffLen + CRC_LEN;
    /*4字节长度*/
    sprintf(&pSaveBuffer[BuffLen], "%04d", strlen(pSaveBuffer));
    BuffLen  = BuffLen + DATA_LEN;
    /*包尾End*/
    sprintf(&pSaveBuffer[BuffLen], "End");
    BuffLen  = BuffLen + END_LEN;
    //printf("\n%s\n",pSaveBuffer);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", StorageLocat, (uint16_t)(WriteDate / 10000), (uint8_t)((WriteDate % 10000) / 100), (WriteDate % 100));
    //printf("%s\n",path);
    RealTime_DirCheck(0);
    File_Write_Que(StorageLocat, path, pSaveBuffer, LastEndAddr, BuffLen);

    if((PackNum + 1) == PackAll)
    {
        Mark = 0;
    }
    else
    {
        Mark = 2;
    }

    Date_List_Write(StorageLocat, WriteDate, &WriteOffset, PackNum, PackAll, LastEndAddr, LastEndAddr + BuffLen, BuffLen, Mark);
    Memory_BKP_Save();
    myfree(path);
    myfree(pSaveBuffer);
    return 0;
}

/***********************************************
** Function name:       History_Data_Unpack
** Descriptions:        历史数据解包
** input parameters:    无
** output parameters:   PackNum当前包数 PackAll总包数 *Data数据
** Returned value:      成功长度
*************************************************/
uint32_t History_Data_Unpack(uint8_t PackNum, uint8_t *PackAll, char *Data)
{
    char *path;
    char *pReadBuffer;
    uint32_t UploadedSec = 0;
    uint32_t ScanningTimes = 0;
    uint32_t read_len = 0;
    uint32_t BetweenLen = 0;
    uint32_t backlen = 0;
    uint32_t length = 2048;
    int32_t offset = 0;
    uint32_t i = 0, j = 0;
    uint16_t crc;
    uint16_t crc_read;
    uint8_t UseStorage = DefaultDisk;
    uint8_t find = 0;
    uint8_t hour = 0;
    uint8_t min = 0;
    uint8_t sec = 0;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    uint8_t GetPackNum = 0;
    uint8_t GetPackAll = 0;
    uint32_t GetStartAddr = 0;
    uint32_t GetEndAddr = 0;
    uint32_t GetLen = 0;
    uint8_t GetMark = 0;
    uint8_t Mark = 0;
    uint32_t res = 0;
    calendar_obj calendar;
    RTC_GetTimes(&calendar);
    Memory_BKP_Read();
    pReadBuffer = mymalloc(length);

    if(WriteDate != (calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date))
    {
        WriteDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
        WriteOffset = 0;
    }

    if(UploadedDate == 0)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }
    else if(UploadedDate > WriteDate)
    {
        UploadedDate = WriteDate;
        UploadedOffset = 0;
        InsideOffset = 0;
    }

    if(PackNum == 0)
    {
        InsideOffset = 0;
    }

    for(;;)
    {
#ifdef DTU3PRO

        for(i = 0; i < 2; i++)
        {
            if(i == 0)
            {
                UseStorage = DefaultDisk;
                res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

                if(res != 0)
                {
                    break;
                }
            }
            else if(i == 1)
            {
                UseStorage = ExpansionDisk;
                res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

                if(res != 0)
                {
                    break;
                }
            }
        }

#else
        UseStorage = DefaultDisk;
        res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);
#endif

        if(res == 0)
        {
            if(UploadedDate < WriteDate)
            {
                UploadedDate++;
                calendar.w_year = UploadedDate / 10000;
                calendar.w_month = (UploadedDate / 100) % 100;
                calendar.w_date = UploadedDate % 100;
                UploadedSec = DateToSec(calendar);
                SecToDate(UploadedSec, &calendar);
                UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                UploadedOffset = 0;
                Memory_BKP_Save();
            }
            else if(UploadedDate == WriteDate)
            {
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
            else
            {
                UploadedDate = WriteDate;
                UploadedOffset = 0;
                GetPackNum = 0;
                GetPackAll = 0;
                GetStartAddr = 0;
                GetEndAddr = 0;
                GetLen = 0;
                GetMark = 0;
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }
        }
        else
        {
            break;
        }
    }

    for(;;)
    {
        if(GetMark == 0)
        {
            if((GetPackNum == 0) && (GetPackAll == 0) && (GetStartAddr == 0) && (GetEndAddr == 0))
            {
                if(UploadedDate < WriteDate)
                {
                    UploadedDate++;
                    calendar.w_year = UploadedDate / 10000;
                    calendar.w_month = (UploadedDate / 100) % 100;
                    calendar.w_date = UploadedDate % 100;
                    UploadedSec = DateToSec(calendar);
                    SecToDate(UploadedSec, &calendar);
                    UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                    UploadedOffset = 0;
                    Memory_BKP_Save();
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
            else
            {
                break;
            }
        }
        else
        {
            if(GetMark == 1)
            {
                UploadedOffset += offset;
                offset = 0;
                Memory_BKP_Save();
                ScanningTimes++;

                if(ScanningTimes >= 100)
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }

            offset += LIST_NEXT;
            res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

            if(res == 0)
            {
                if(UploadedDate < WriteDate)
                {
                    UploadedDate++;
                    calendar.w_year = UploadedDate / 10000;
                    calendar.w_month = (UploadedDate / 100) % 100;
                    calendar.w_date = UploadedDate % 100;
                    UploadedSec = DateToSec(calendar);
                    SecToDate(UploadedSec, &calendar);
                    UploadedDate = calendar.w_year * 10000 + calendar.w_month * 100 + calendar.w_date;
                    UploadedOffset = 0;
                    Memory_BKP_Save();
                }
                else
                {
                    GetPackNum = 0;
                    GetPackAll = 0;
                    GetStartAddr = 0;
                    GetEndAddr = 0;
                    GetLen = 0;
                    GetMark = 0;
                    myfree(path);
                    myfree(pReadBuffer);
                    return 0;
                }
            }
        }
    }

    for(;;)
    {
        if(GetPackNum == PackNum)
        {
            if(GetPackNum == 0)
            {
                UploadedOffset = UploadedOffset + InsideOffset + offset;
            }

            break;
        }
        else if(GetPackNum > PackNum)
        {
            offset = offset - LIST_NEXT;
        }
        else if(GetPackNum < PackNum)
        {
            offset = offset + LIST_NEXT;
        }

        res = Date_List_Read(UseStorage, UploadedDate, UploadedOffset + InsideOffset + offset, &GetPackNum, &GetPackAll, &GetStartAddr, &GetEndAddr, &GetLen, &GetMark);

        if(res == 0)
        {
            GetPackNum = 0;
            GetPackAll = 0;
            GetStartAddr = 0;
            GetEndAddr = 0;
            GetLen = 0;
            GetMark = 0;
            myfree(path);
            myfree(pReadBuffer);
            return 0;
        }
    }

    length = 2048;
    find = 0;
    memset(pReadBuffer, 0, (length));
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data/%04d%02d%02d.dat", DefaultDisk, (uint16_t)(UploadedDate / 10000), (uint8_t)((UploadedDate % 10000) / 100), (UploadedDate % 100));
    read_len = File_Read(UseStorage, path, pReadBuffer, GetStartAddr, length);

    if(read_len == 0)
    {
        myfree(path);
        myfree(pReadBuffer);
        return 0;
    }

    for(i = 0; i < read_len; i++)
    {
        if(strncmp(&pReadBuffer[i], "Start:", START_LEN) == 0)
        {
            i += START_LEN;
            /*获取第几包*/
            GetPackNum = astr2int(&pReadBuffer[i], PACK_NO_LEN);
            i += PACK_NO_LEN;
            /*获取总包数*/
            *PackAll = astr2int(&pReadBuffer[i], PACK_ALL_LEN);
            i += PACK_ALL_LEN;
            /*获取时*/
            hour = astr2int(&pReadBuffer[i], HOUR_LEN);
            i += HOUR_LEN;
            /*获取分*/
            min = astr2int(&pReadBuffer[i], MIN_LEN);
            i += MIN_LEN;
            /*获取秒*/
            sec = astr2int(&pReadBuffer[i], SEC_LEN);
            i += SEC_LEN;
            /*获取长度*/
            length = astr2int(&pReadBuffer[i], DATA_LEN);
            i += DATA_LEN;
            myfree(pReadBuffer);
            pReadBuffer = mymalloc(length + CRC_LEN + END_LEN);
            memset(pReadBuffer, 0, (length + CRC_LEN + END_LEN));
            j = i;
            File_Read(StorageLocat, path, pReadBuffer, GetStartAddr + i, length + CRC_LEN + END_LEN);
            crc = MyCrc16((unsigned char *)&pReadBuffer[i - j], length);
            crc_read = hstr2int(&pReadBuffer[i - j + length], CRC_LEN);

            if(crc == crc_read)
            {
                memcpy(Data, &pReadBuffer[i - j], length);
            }
            else
            {
                UploadedOffset = UploadedOffset + (LIST_NEXT * (*PackAll));
                Memory_BKP_Save();
                myfree(path);
                myfree(pReadBuffer);
                return 0;
            }

            i += length;
            i += CRC_LEN;
            i += END_LEN;
            InsideOffset = InsideOffset + LIST_NEXT;
            Memory_BKP_Save();
            myfree(path);
            myfree(pReadBuffer);
            return length;
        }
    }

    Memory_BKP_Save();
    myfree(path);
    myfree(pReadBuffer);
    return 0;
}

/***********************************************
** Function name:       DeleteOldestData
** Descriptions:        删除最早数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void DeleteOldestData(void)
{
    uint8_t DeleteFlag = 0;
    uint16_t Num = 0;
#ifdef DTU3PRO
    uint16_t Max = 60;
#else
    uint16_t Max = 30;
#endif
    uint32_t FileNameNum[Max];
    /*将就小文件名设为最大值*/
    uint32_t MinNum[10] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
    uint32_t MinNumExp[10] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
    char *FileName[Max];
    char *MinPath;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    uint16_t i, j;

    for(i = 0; i < Max; i++)
    {
        FileName[i] = mymalloc(PathLen);
        memset(FileName[i], 0, PathLen);
    }

    Num = 0;
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", DefaultDisk);
    /*通过扫描找到所有文件名*/
    lf_ListAllFiles(DefaultDisk, path, FileName, &Num, Max);

    for(i = 0; i < Num; i++)
    {
        FileNameNum[i] = astr2int(FileName[i], 8);

        /*比较文件名，找到数值最小的5个文件名（日期）*/
        if(FileNameNum[i] != 0)
        {
            for(j = 0; j < RemainQuant; j++)
            {
                if(MinNum[j] > FileNameNum[i])
                {
                    MinNum[j] = FileNameNum[i];
                    break;
                }
            }
        }
    }

#ifdef DTU3PRO

    for(i = 0; i < Max; i++)
    {
        memset(FileName[i], 0, PathLen);
    }

    Num = 0;
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", ExpansionDisk);
    /*通过扫描找到所有文件名*/
    lf_ListAllFiles(ExpansionDisk, path, FileName, &Num, Max);

    for(i = 0; i < Num; i++)
    {
        FileNameNum[i] = astr2int(FileName[i], 8);

        //printf("%d\n", FileNameNum[i]);
        if(FileNameNum[i] != 0)
        {
            for(j = 0; j < RemainQuant; j++)
            {
                if(MinNumExp[j] > FileNameNum[i])
                {
                    MinNumExp[j] = FileNameNum[i];
                    break;
                }
            }
        }
    }

#endif
    //printf("MinNum %d\n", MinNum);
    MinPath = mymalloc(PathLen);

    for(i = 0; i < RemainQuant; i++)
    {
        memset(MinPath, 0, PathLen);
#ifndef DTU3PRO
        sprintf(MinPath, "%d:/Data/%d.dat", DefaultDisk, MinNum[i]);
        //printf("MinPath %s\n", MinPath);
        lf_unlink(DefaultDisk, MinPath);
#else

        /*比较两组目录，找到最小的文件名所在的目录并删除*/
        for(j = 0; j < RemainQuant; j++)
        {
            DeleteFlag = 0;

            if(MinNumExp[j] < MinNum[i])
            {
                memset(MinPath, 0, PathLen);
                sprintf(MinPath, "%d:/Data/%d.dat", ExpansionDisk, MinNumExp[j]);
                //printf("MinPath %s\n", MinPath);
                lf_unlink(ExpansionDisk, MinPath);
                memset(MinPath, 0, PathLen);
                sprintf(MinPath, "%d:/Data/%d_list.dat", ExpansionDisk, MinNumExp[j]);
                //printf("MinPath %s\n", MinPath);
                lf_unlink(ExpansionDisk, MinPath);

                for(; j < (RemainQuant - 1); j++)
                {
                    MinNumExp[j] = MinNumExp[j + 1];
                }

                MinNumExp[RemainQuant - 1] = 0xFFFFFFFF;
                DeleteFlag = 1;
            }

            if(DeleteFlag == 0)
            {
                memset(MinPath, 0, PathLen);
                sprintf(MinPath, "%d:/Data/%d.dat", DefaultDisk, MinNum[i]);
                //printf("MinPath %s\n", MinPath);
                lf_unlink(DefaultDisk, MinPath);
                memset(MinPath, 0, PathLen);
                sprintf(MinPath, "%d:/Data/%d_list.dat", DefaultDisk, MinNum[i]);
                //printf("MinPath %s\n", MinPath);
                lf_unlink(DefaultDisk, MinPath);
            }
        }

#endif
    }

    for(i = 0; i < Max; i++)
    {
        if(FileName[i] != NULL)
        {
            myfree(FileName[i]);
        }
    }

    myfree(path);
    myfree(MinPath);
}


void DeleteHistoryData(void)
{
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", DefaultDisk);
    lf_DeleteAllFiles(DefaultDisk, path);
#ifdef DTU3PRO
    memset(path, 0, PathLen);
    sprintf(path, "%d:/Data", ExpansionDisk);
    lf_DeleteAllFiles(ExpansionDisk, path);
#endif
    myfree(path);
}


/***********************************************
** Function name:       File_Processing
** Descriptions:        文件系统分时写入数据
** input parameters:    无
** output parameters:   无
** Returned value:      无
*************************************************/
void File_Processing(void)
{
    static uint32_t time = 0;
    static uint32_t IdleTimes = 0;
    static uint32_t IdleTime = 0;
    static uint32_t writetimes = 0;
    static uint32_t writenum = 0;
    static uint32_t writetime = 0;
    static int res = 0;
    uint32_t module_num = 0;
    uint32_t pv_length = 0;
    uint32_t Meter_length = 0;
    uint32_t Rp_length = 0;
    uint16_t AvailableQuan = 0;             //实时数据最大可存储数量
    uint16_t SavedQuan = 0;                 //实时数据已存数量
#ifdef DTU3PRO
    uint16_t AvailableQuanExp = 0;          //实时数据最大可存储数量
    uint16_t SavedQuanExp = 0;              //实时数据已存数量
#endif
    uint32_t write_num = 0;
    uint32_t Capacity = 0;
    uint16_t Num = 0;
    static fs_write fp_get;
    char *path;
    path = mymalloc(PathLen);
    memset(path, 0, PathLen);

    switch(fp_state)
    {
        case fp_init:
            if(Fs_Init(0))
            {
                if(fp_get.path != NULL)
                {
                    myfree(fp_get.path);
                    fp_get.path = NULL;
                }

                if(fp_get.buffer != NULL)
                {
                    myfree(fp_get.buffer);
                    fp_get.buffer = NULL;
                }

                fp_state = fp_idle;
            }

            break;

        case fp_idle:
            if(Queue_IsEmpty() == 1)
            {
                if(fp_get.path == NULL)
                {
                    fp_get.path = mymalloc(PathLen);
                }
                else
                {
                    memset(fp_get.path, 0, PathLen);
                }

                if(fp_get.buffer == NULL)
                {
                    fp_get.buffer = mymalloc(BufferLen);
                }
                else
                {
                    memset(fp_get.buffer, 0, BufferLen);
                }

                if(Queue_Out(&fp_get))
                {
                    fp_state = fp_write;
                }
            }
            else
            {
                //printf("2perused:%d\n", my_mem_perused());
                if((LocalTime - IdleTime) <= 20)
                {
                    IdleTimes++;

                    if(IdleTimes == 10)
                    {
                        if(fp_get.path != NULL)
                        {
                            myfree(fp_get.path);
                            fp_get.path = NULL;
                        }

                        if(fp_get.buffer != NULL)
                        {
                            myfree(fp_get.buffer);
                            fp_get.buffer = NULL;
                        }
                    }

                    if(IdleTimes >= 200000)
                    {
                        IdleTimes = 0;
                        fp_state = fp_check;
                    }
                }
                else
                {
                    IdleTimes = 0;
                }

                IdleTime = LocalTime;
            }

            break;

        case fp_write:
            if(fp_get.path != NULL)
            {
                //                printf("fp_write\n");
                //printf("fp_get.size %d\n", fp_get.size);
                //#ifdef DEBUG
                //                printf("fp_get.path %s\n", fp_get.path);
                //                printf("fp_get.off %d\n", fp_get.off);
                //#endif
                //                printf("fp_get.buffer %s\n", fp_get.buffer);
                //printf("fp_get.path:%s\n",fp_get.path);
                //printf("fp_get.buffer:%s\n",fp_get.buffer);
                //printf("fp_get.off:%d\n",fp_get.off);
                //printf("fp_get.size:%d\n",fp_get.size);
                res = lf_open(fp_get.disk, &lfnew, fp_get.path, LFS_O_RDWR | LFS_O_CREAT);

                if(res == LFS_ERR_OK)
                {
                    time = LocalTime;
                    lf_seek(fp_get.disk, &lfnew, fp_get.off);
                    lf_write(fp_get.disk, &lfnew, fp_get.buffer, fp_get.size, &write_num);
                    lf_close(fp_get.disk, &lfnew);
                }
            }

            fp_state = fp_idle;
            break;

        case fp_check:
            //printf("fp_check\n");
            //以扫描的方式查看文件系统中实时数据文件的容量，若超过了设定值便进行删除操作
            //注：lite版本为单个存储块，Pro为2个存储块，Pro需要交叉扫描，需要标记当前正在使用的
            //实时数据最大可存储数量
            pv_length = Dtu3Detail.Property.PortNum * (sizeof(PvDataMO) / (1.5));
#ifdef DTU3PRO
            Meter_length = MeterLinkNum * (sizeof(MeterDataMO) / (1.5));
            Rp_length = 0 * (sizeof(RpDataMO));
            module_num = Dtu3Detail.Property.PortNum + MeterLinkNum + 0;
#else
            Meter_length = 0;
            Rp_length = 0;
            module_num = Dtu3Detail.Property.PortNum;
#endif
            //DataAverageSize
            AvailableQuan = (uint16_t)((lf_Total_Capacity(DefaultDisk) - DownloadSize - SystemSize - HardwareSize) / ((pv_length + Meter_length + Rp_length + (DataAverageSize + DATA_SAVE_ALL + LIST_NEXT) * (module_num / ONEC_SOCKET_SEND_DEVICE)) * DayNum));
            sprintf(path, "%d:/Data/", DefaultDisk);
            //实时数据已存数量
            lf_DirectorySize(DefaultDisk, path, &Capacity, &SavedQuan);
#ifdef DTU3PRO
            //实时数据最大可存储数量
            AvailableQuanExp = (uint16_t)(lf_Total_Capacity(ExpansionDisk) / ((pv_length + Meter_length + Rp_length + (DataAverageSize + DATA_SAVE_ALL + LIST_NEXT) * (module_num / ONEC_SOCKET_SEND_DEVICE)) * DayNum));
            sprintf(path, "%d:/Data/", ExpansionDisk);
            //实时数据已存数量
            lf_DirectorySize(ExpansionDisk, path, &Capacity, &SavedQuanExp);
#endif
#ifndef DTU3PRO

            if(SavedQuan >= (AvailableQuan - (RemainQuant) / 2))
#else
            if((SavedQuan >= (AvailableQuan - (RemainQuant) / 2)) && (SavedQuanExp >= (AvailableQuanExp - (RemainQuant) / 2)))
#endif
            {
                fp_state = fp_delete;
            }
            else
            {
#ifdef DTU3PRO

                if((StorageLocat != ExpansionDisk) && (SavedQuan >= (AvailableQuan - 1)) && (SavedQuanExp <= (AvailableQuanExp - RemainQuant)))
                {
                    StorageLocat = ExpansionDisk;
                }
                else if((StorageLocat != DefaultDisk) && (SavedQuan <= (AvailableQuan - RemainQuant)) && (SavedQuanExp >= (AvailableQuanExp - 1)))
                {
                    StorageLocat = DefaultDisk;
                }

#endif
                fp_state = fp_idle;
            }

            break;

        case fp_delete:
            {
                DeleteOldestData();
                fp_state = fp_idle;
            }
            break;

        default:
            fp_state = fp_idle;
            break;
    }

    myfree(path);
}
/***********************************************
** Function name:       Get_FileStatus
** Descriptions:        获取文件系统运行状态
** input parameters:    无
** output parameters:   无
** Returned value:      1 忙 0 空闲
*************************************************/
uint8_t Get_FileStatus(void)
{
    if(fp_state == fp_idle)
    {
        if(Queue_IsEmpty() == 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}

/***********************************************
** Function name:       MI_Program_Check   ++++
** Descriptions:        并网保护校验
** input parameters:    无
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
uint8_t Grid_Program_Check(void)
{
    char *buffer;
    u8 temp = 0;
    u8 find_head = 0;
    uint16_t length = 0;
    uint16_t BufSize = 0;
    uint16_t i = 0, j, k = 0;
    length = Grid_Profiles_List_Get_Length();
    //    length = 1500;
    BufSize = length;
    buffer = mymalloc(BufSize);
    memset(buffer, 0, BufSize);
    u16 temp_para;
    u8 temp_string[150] = {0};
    length = Grid_Profiles_List_Read_Length(buffer, 0, BufSize);

    for(i = 0; i < length; i++)
    {
        if(strncmp(&buffer[i], "countrystd=", 11) == 0)  //找文件头
        {
            find_head = 1;                              //找到头
            break;
        }
    }

    if(i == length)                                     //没找到头
    {
        temp = 0;
    }

    if(1 == find_head)                                  //有头
    {
        while(i < length)
        {
            i++;

            if(buffer[i] == '=')
            {
                i++;
                temp_para = 0;

                for(j = 0; j < 5; j++)
                {
                    if(buffer[i] == '\r')
                    {
                        break;
                    }

                    if(buffer[i] == '\n')
                    {
                        break;
                    }

                    temp_para = temp_para * 10;
                    temp_para = temp_para + (buffer[i] - 0x30);
                    i++;
                }

                temp_string[2 * k] = temp_para >> 8;
                temp_string[2 * k + 1] = temp_para;
                k++;

                if(k > 80)
                {
                    break;
                }
            }

            if(strncmp(&buffer[i], "modbus_crc16=", 13) == 0)   //文件结束
            {
                i = i + 13;
                temp_para = 0;

                for(j = 0; j < 5; j++)
                {
                    if(buffer[i] == '\r')
                    {
                        break;
                    }

                    if(buffer[i] == '\n')
                    {
                        break;
                    }

                    temp_para = temp_para * 10;
                    temp_para = temp_para + (buffer[i] - 0x30);
                    i++;
                }

                break;
            }
        }

        i = 2 * k;
        j = UART_CRC16_Work(&temp_string[0], i);

        if(temp_para == j)                                  // 校验通过
        {
            temp = 1;
        }
        else
        {
            temp = 0;
        }
    }

    return temp;
}
/***********************************************
** Function name:       MI_Program_Check
** Descriptions:        MI升级程序校验
** input parameters:    无
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
uint8_t MI_Program_Check(void)
{
    char *buffer;
    char Cache[MAX_ONE_LINE_LEN];
    uint8_t counter;
    uint8_t Oneline[MAX_ONE_LINE_LEN];
    uint8_t model;
    uint8_t linelen = 0;
    uint8_t perused;
    uint8_t FLASE_CRC = 0, OK_CRC = 1, flag_crc = 1;
    uint16_t addr_up_crc = 0x8010;
    uint16_t addr_down_crc = 0;
    uint16_t addr_lose = 0;
    uint16_t addr_len = 0;
    uint16_t timeout = 0;
    uint16_t crc = 0, CRC_ResultC = 0, CRC_FW = 0, x = 0;
    uint32_t BufSize = 2048;
    uint32_t i = 0, j = 0;
    uint32_t offset = 0;
    uint32_t length = 0;
    perused = my_mem_perused();
    length = MI_Program_Get_Length();

    if(length == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (uint32_t)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 10))) / 32) * 32;

    if(length < BufSize)
    {
        BufSize = length;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    buffer = mymalloc(BufSize);
    memset(buffer, 0, BufSize);
    memset(Cache, 0, MAX_ONE_LINE_LEN);
    memset(Oneline, 0, MAX_ONE_LINE_LEN);
    length = 0;

    while(1)
    {
#ifndef DTU3PRO
        WDG_Feed(); //喂狗
#else
#ifndef DEBUG
        IWDG_Feed();
#endif
#endif

        if((length - i) <= 200)
        {
            offset += i;
            memset(buffer, 0, BufSize);
            length = MI_Program_Read_Length(buffer, offset, BufSize);
            i = 0;
        }

        while(i <= BufSize)
        {
            if(strncmp(&buffer[i], "\r\n:", 3) == 0)
            {
                timeout = 0;
                i = i + 3;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) || \
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset(Oneline, 0, sizeof(Oneline));
                linelen = j;
                Ascii2Hex((uint8_t *)Cache, Oneline, j);

                if(flag_crc == 1)
                {
                    if((Oneline[0] == 0x02) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x10))
                    {
                        flag_crc = 2;
                        crc = line_crc(Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    break;
                }
                else if(flag_crc == 2)
                {
                    if((Oneline[0] == 0x02) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x04))
                    {
                        flag_crc = 3;
                        crc = line_crc(Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    break;
                }
                else if(flag_crc == 3)
                {
                    if((Oneline[0] == 0x04) && (Oneline[3] == 0x00))
                    {
                        flag_crc = 4;
                        crc = line_crc(Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    break;
                }
                else if(flag_crc == 4)
                {
                    flag_crc = 0;
                    CRC_FW = ((Oneline[12] & 0xffff) << 8) + Oneline[13];

                    if((Oneline[0] == 0x18) && (Oneline[3] == 0x00))
                    {
                        crc = line_crc(Oneline, linelen);

                        if(Oneline[linelen / 2 - 1] != crc)
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    if(model == 1)
                    {
                        CRC_ResultC = 0XFFFF;

                        for(counter = 4; counter < 13; counter = counter + 2) //
                        {
                            x = ((Oneline[counter]) & 0x00ff) << 8;
                            x = x + Oneline[counter + 1];
                            CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                        }

                        if((CRC_ResultC >> 8) != Oneline[counter])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }

                        if((CRC_ResultC & 0x00ff) != Oneline[counter + 1])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }
                    else if(model == 2)
                    {
                        CRC_ResultC = 0XFFFF;

                        for(counter = 4; counter < 27; counter = counter + 2)
                        {
                            if(counter == 14)
                            {
                                counter = 16;
                            }

                            x = ((Oneline[counter]) & 0x00ff) << 8;
                            x = x + Oneline[counter + 1];
                            CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                        }

                        if(CRC_ResultC >> 8 != Oneline[14])
                        {
                            myfree(buffer);
                            return FLASE_CRC;
                        }
                    }

                    if((CRC_ResultC & 0x00ff) != Oneline[15])
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    CRC_ResultC = 0XFFFF;
                    break;
                }

                if((Oneline[0] == 0x00) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x01) && (Oneline[4] == 0xff))
                {
                    flag_crc = 1;

                    if(CRC_FW == CRC_ResultC)
                    {
                        myfree(buffer);
                        return OK_CRC;
                    }
                    else
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }
                }
                //去掉中间高位变化  20170920
                else if(strncmp(&buffer[i], "02000004", 8) == 0)
                {
                    i = i + 14;
                    break;
                }
                else
                {
                    addr_down_crc = (((Oneline[1]) & 0x00ff) << 8) + (Oneline[2]);
                    crc = line_crc(Oneline, linelen);

                    if(Oneline[linelen / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return FLASE_CRC;
                    }

                    addr_lose = addr_down_crc - addr_up_crc;

                    for(counter = addr_lose; counter > addr_len; counter--)
                    {
                        x = 0xffff;
                        CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                    }

                    for(counter = 4; counter < ((Oneline[0] + 4)); counter = counter + 2)
                    {
                        if((Oneline[1] == 0xef) && (Oneline[2] == 0x50))
                        {
                            x = 1;
                        }

                        x = ((Oneline[counter]) & 0x00ff) << 8;
                        x = x + Oneline[counter + 1];
                        CRC_ResultC = CalcCRC16t(x, CRC_ResultC);
                    }

                    addr_up_crc = ((Oneline[1] << 8) + (Oneline[2]));
                    addr_len = ((Oneline[0]) / 2);
                    break;
                }

                i = i + j;
            }
            else if(strncmp(&buffer[i], "\r\n\r\n:", 5) == 0)
            {
                i = i + 5;
                timeout = 0;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset(Oneline, 0, sizeof(Oneline));
                linelen = j;
                Ascii2Hex((uint8_t *)Cache, Oneline, j);

                if((Oneline[0] == 0x06) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x11))
                {
                    //00010001表示250W/300W微逆程序，00110001表示500W/600W微逆程序  00210001表示1000W/1200W微逆程序
                    if((Oneline[4] == 0x00) && (Oneline[5] == 0x01) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01))
                    {
                        model = 1;
                    }
                    else if((Oneline[4] == 0x00) && (Oneline[5] == 0x11) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01))
                    {
                        model = 2;
                    }
                    //1000w按照500w来校验
                    else if((Oneline[4] == 0x00) && (Oneline[5] == 0x21) && (Oneline[6] == 0x00) && (Oneline[7] == 0x01))
                    {
                        model = 2;
                    }

                    crc = line_crc(Oneline, linelen);

                    if(Oneline[linelen / 2 - 1] != crc)
                    {
                        myfree(buffer);
                        return    FLASE_CRC;
                    }
                }
                else
                {
                    myfree(buffer);
                    return FLASE_CRC;
                }

                i = i + j;
                break;
            }
            else
            {
                i++;
            }
        }

        memset(Cache, 0, sizeof(Cache));
        timeout++;

        if(timeout > 5000)
        {
            myfree(buffer);
            return FLASE_CRC ;
        }
    }

    myfree(buffer);
}
#endif
/***********************************************
** Function name:       DTU_Program_Check
** Descriptions:        DTU升级程序校验
** input parameters:    硬件版本号
** output parameters:   无
** Returned value:      1 成功 0 失败
*************************************************/
uint8_t DTU_Program_Check(u16 Hardware)
{
    uint8_t hardware[2] = {0};
    hardware[0] = Hardware >> 8;
    hardware[1] = Hardware;
    char *buffer;
    char Cache[MAX_ONE_LINE_LEN];
    uint8_t CRC16Hi, CRC16Lo;
    uint8_t temp_crc;
    uint8_t SaveHi, SaveLo;
    uint8_t x;
    uint8_t *ptr;
    uint8_t len = 0;
    uint8_t cmd = 0;
    uint8_t counter;
    uint8_t Oneline[MAX_ONE_LINE_LEN];
    uint8_t StartLine[14];
    uint8_t linelen = 0;
    uint8_t perused;
    uint16_t LineNum = 0;
    uint16_t timeout = 0;
    uint16_t crc = 0, CRC_ResultC = 0, CRC_FW = 0;
    uint32_t y;
    uint32_t i = 0, j = 0;
    uint32_t offset = 0;
    uint32_t address = 0;
    uint32_t length = 0;
    uint32_t BufSize = 4096;
    perused = my_mem_perused();
    length = DTU_Program_Get_Length();

    if(length == 0)
    {
        myfree(buffer);
        return 0;
    }

    BufSize = (uint32_t)((MEM_MAX_SIZE - ((MEM_MAX_SIZE / 100) * (perused + 10))) / 32) * 32;

    if(length < BufSize)
    {
        BufSize = length;
    }
    else if(BufSize > ReadMaxSize)
    {
        BufSize = ReadMaxSize;
    }

    uint32_t time = 0;
    CRC16Hi = 0xFF;
    CRC16Lo = 0xFF;
    buffer = mymalloc(BufSize);
    memset(buffer, 0, BufSize);
    length = 0;

    while(1)
    {
#ifndef Bootloader
#ifndef DTU3PRO
        WDG_Feed(); //喂狗
#else
#ifndef DEBUG
        IWDG_Feed();
#endif
#endif
#endif

        if((length - i) <= 100)
        {
            offset += i;
            memset(buffer, 0, BufSize);
            length = DTU_Program_Read_Length(buffer, offset, BufSize);
            i = 0;
        }

        while(i <= length)
        {
            if(strncmp(&buffer[i], "\r\n:", 3) == 0)   //\r\n:            除第一行外的每行开始
            {
                timeout = 0;
                i = i + 3;
                LineNum ++;

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;
                    }
                }

                memset(Oneline, 0, sizeof(Oneline));
                Ascii2Hex((uint8_t *)Cache, Oneline, j);
                crc = line_crc(Oneline, j);

                if(Oneline[j / 2 - 1] != crc)
                {
                    myfree(buffer);
                    return 0;
                }

                if((Oneline[0] == 0x00) && (Oneline[1] == 0x00) && (Oneline[2] == 0x00) && (Oneline[3] == 0x01) && (Oneline[4] == 0xff))  //结尾行
                {
                    if(CRC_FW == CRC_ResultC)
                    {
                        myfree(buffer);
                        return 1;
                    }
                    else
                    {
                        myfree(buffer);
                        return 0;
                    }
                }

                if(LineNum == 2)
                {
                    StartLine[0] = Oneline[5];          //开始地址  32
                    StartLine[1] = Oneline[4];
                    StartLine[2] = Oneline[7];
                    StartLine[3] = Oneline[6];
                    StartLine[4] = Oneline[9];          //结束地址  32
                    StartLine[5] = Oneline[8];
                    StartLine[6] = Oneline[11];
                    StartLine[7] = Oneline[10];
                    StartLine[8] = Oneline[13];         //文件校验  16
                    StartLine[9] = Oneline[12];
                    StartLine[10] = Oneline[15];        //硬件版本号    16
                    StartLine[11] = Oneline[14];
                    StartLine[12] = Oneline[17];        //自校验    16
                    StartLine[13] = Oneline[16];
                    CRC_ResultC = 0xFFFF;
                    CRC_ResultC = CalcCRC16((uint16_t *)StartLine, 12);

                    if((hardware[0] == StartLine[10]) || (hardware[1] == StartLine[11]))     //硬件版本校验
                    {
                        if(CRC_ResultC == ((StartLine[12] << 8) + StartLine[13])) //自校验通过  则开始文件校验
                        {
                            CRC_FW = (StartLine[8] << 8) + StartLine[9];
                        }
                        else
                        {
                            myfree(buffer);
                            return 0;
                        }
                    }
                    else                                //硬件版本不对
                    {
                        myfree(buffer);
                        return 0;
                    }
                }
                else if(LineNum >= 4)
                {
                    len = *(Oneline + 0);               //长度
                    address = ((*(Oneline + 1)) << 8) + (*(Oneline + 2)); //地址
                    cmd = *(Oneline + 3);               //命令

                    if(cmd == 00)                       //数据
                    {
                        ptr = Oneline + 4;

                        for(x = 0; x < len; x++)
                        {
                            CRC16Lo = CRC16Lo ^ (*(ptr + x));

                            for(y = 0; y < 8; y++)
                            {
                                SaveHi = CRC16Hi;
                                SaveLo = CRC16Lo;
                                CRC16Hi = CRC16Hi / 2;          //高位右移一位
                                CRC16Lo = CRC16Lo / 2;          //低位右移一位

                                if((SaveHi & 0x01) == 0x01)     //如果高位字节最后一位为1
                                {
                                    CRC16Lo = CRC16Lo | 0x80;   //则低位字节右移后前面补1
                                }                               //否则自动补0

                                if((SaveLo & 0x01) == 0x01)
                                {
                                    CRC16Hi = CRC16Hi ^ 0xa0;   //如果LSB为1，则与多项式码进行异或
                                    CRC16Lo = CRC16Lo ^ 0x01;
                                }
                            }
                        }

                        CRC_ResultC = (uint16_t)(CRC16Hi << 8 | CRC16Lo);
                    }
                }

                i = i + j;
                break;
            }
            else if(strncmp(&buffer[i], "\r\n\r\n:", 5) == 0)   //程序总开始
            {
                timeout = 0;
                i = i + 5;
                LineNum ++;
                memset(Cache, 0, sizeof(Cache));

                for(j = 0; j < MAX_ONE_LINE_LEN; j++)
                {
                    if(((buffer[i + j] <= '9') && (buffer[i + j] >= '0')) || ((buffer[i + j] <= 'F') && (buffer[i + j] >= 'A')) ||
                            ((buffer[i + j] <= 'f') && (buffer[i + j] >= 'a')))
                    {
                        Cache[j] = buffer[i + j];
                    }
                    else
                    {
                        break;                                  //下载程序结束  没找到程序头
                    }
                }

                memset(Oneline, 0, sizeof(Oneline));
                Ascii2Hex((uint8_t *)Cache, Oneline, j);
                crc = line_crc(Oneline, j);

                if(Oneline[j / 2 - 1] != crc)
                {
                    myfree(buffer);
                    return 0;
                }

                i = i + j;
                break;
            }
            else
            {
                i++;
            }
        }

        memset(Cache, 0, sizeof(Cache));
        timeout++;

        if(timeout > 5000)
        {
            myfree(buffer);
            return 0 ;
        }
    }

    myfree(buffer);
}

#ifdef Bootloader
//hex文件转BIN文件中转
uint8_t packageone[(MAX_PRO_PACKAGE / 2)];
//hex头
uint8_t hex_start[5] = {'\r', '\n', '\r', '\n', ':'};
//hex头
uint8_t hex_start1[4] = {'\n', '\r', '\n', ':'};
//行头
uint8_t hex_pack_start[3] = {'\r', '\n', ':'};
uint32_t FlashWriteAddress; //bin文件写入地址
uint32_t FlashWr_Haddress;  //上一包写入地址
//uint32_t USER_BIN_ADDRESS = FLASH_ADDR_BIN;                  //bin文件开始地址





void IAP_WRdata_to_flash(uint8_t *dat, uint32_t startaddr, uint32_t endaddr, uint32_t offset) //去除hex文件头和尾 并校验
{
    uint8_t len;
    uint16_t i;
    uint8_t cmd;
    uint32_t address;
    //长度
    len = *(dat + 0);
    //地址
    address = ((*(dat + 1)) << 8) + (*(dat + 2));
    //命令
    cmd = *(dat + 3);

    //数据
    if(cmd == 00)
    {
        //不加偏移量
        FlashWriteAddress = FlashWr_Haddress + address + offset ;

        // 有效范围内
        if((FlashWriteAddress >= startaddr) && (FlashWriteAddress <= endaddr))
        {
            //写入
            FLASH_Write(FlashWriteAddress, (uint32_t *)(dat + 4), len);
        }
    }
    // 04是高位地址变化
    else if(cmd == 4)
    {
        address = ((*(dat + 4)) << 8) + (*(dat + 5));
        FlashWr_Haddress = (address << 16) & 0xffff0000;//高位地址发生变化
    }
    // 01是结束符
    else if(cmd == 1)
    {
        //程序结束
    }
}

//uint8_t ReadBuffer[25 * 1024 + 50] = {0}; //读出的hex文件 20K+520字节
// hex文件转BIN文件
/***********************************************
** Function name:
** Descriptions:        hex文件转bin文件
** input parameters:    startaddr bin文件写入地址
** output parameters:   无
** Returned value:      无
*************************************************/
uint8_t IAP_Myprogram(uint32_t startaddr, uint32_t endaddr, uint32_t offset)
{
    uint8_t find = 0;                                //找到头文件标志位
    uint8_t *ReadBuffer;
    uint8_t Oneline[MAX_PRO_PACKAGE] = {0};
    uint32_t i = 0;
    uint32_t j = 0;
    uint32_t read_len = 25 * 1024;
    //读取hex文件偏移量
    uint32_t read_offset = 0;
    int32_t length = 0;
    ReadBuffer = mymalloc(read_len);
    memset(ReadBuffer, 0, read_len);
    read_offset = 0 ;                           //偏移量初始化为0
    length = DTU_Program_Get_Length();          //获取DTU长度

    //获取长度失败
    if(length == 0)
    {
        myfree(ReadBuffer);
        return 0;
    }

    length = DTU_Program_Read_Length((char *)ReadBuffer, read_offset, MAX_PRO_PACKAGE * 2); //读取头文件
    i = 0;
    find = 0;

    //找头
    while(i < length)
    {
        if((strncmp((char *)&ReadBuffer[i], (char *)hex_start, 5) == 0) || (strncmp((char *)&ReadBuffer[i], (char *)hex_start1, 4) == 0)) //程序总开始
        {
            FLASH_ERASURE(startaddr, endaddr);

            if(strncmp((char *)&ReadBuffer[i], (char *)hex_start, 5) == 0)
            {
                i = i + 5;
            }
            else if(strncmp((char *)&ReadBuffer[i], (char *)hex_start1, 4) == 0)
            {
                i = i + 4;
            }

            memset(Oneline, 0, sizeof(Oneline));

            for(j = 0; j < MAX_PRO_PACKAGE; j++)
            {
                if(((ReadBuffer[i + j] <= '9') && (ReadBuffer[i + j] >= '0')) || ((ReadBuffer[i + j] <= 'F') && (ReadBuffer[i + j] >= 'A')) ||
                        ((ReadBuffer[i + j] <= 'f') && (ReadBuffer[i + j] >= 'a')))
                {
                    Oneline[j] = ReadBuffer[i + j];
                }
                else
                {
                    break;
                }
            }

            memset(packageone, 0, sizeof(packageone));
            ascii_to_hex(Oneline, packageone, j);

            //校验一包是否正确
            if(package_crc(packageone, (j / 2)) == 0)
            {
                //编程一行个数地址类型数据CRC
                IAP_WRdata_to_flash(&packageone[0], startaddr, endaddr, offset);
                find = 1; //找到头
                read_offset = read_offset + j + i;
                i = 0;
                length = 0;
                break;
            }
            else
            {
                myfree(ReadBuffer);
                return 0;
            }
        }
        else
        {
            i++;

            //没找到头
            if(i == length - 1)
            {
                myfree(ReadBuffer);
                return 0;
            }
        }
    }

    //找到文件头
    while(find == 1)
    {
        if(length - i < 100)
        {
            read_offset += i;
            memset((char *)ReadBuffer, 0, sizeof(ReadBuffer));
            length = DTU_Program_Read_Length((char *)ReadBuffer, read_offset, read_len);
            i = 0;
        }

        while(i < read_len)
        {
            if(strncmp((char *)&ReadBuffer[i], (char *)hex_pack_start, 3) == 0) //\r\n: 除第一行外的每行开始
            {
                if(strncmp((char *)&ReadBuffer[i + 2], ":00000001FF", 11) == 0) //程序总结束
                {
                    myfree(ReadBuffer);
                    return 1;
                }
                else//中间数据
                {
                    i = i + 3;

                    for(j = 0; j < MAX_PRO_PACKAGE; j++)
                    {
                        if(((ReadBuffer[i + j] <= '9') && (ReadBuffer[i + j] >= '0')) || ((ReadBuffer[i + j] <= 'F') && (ReadBuffer[i + j] >= 'A')) ||
                                ((ReadBuffer[i + j] <= 'f') && (ReadBuffer[i + j] >= 'a')))
                        {
                            Oneline[j] = ReadBuffer[i + j];
                        }
                        else
                        {
                            break;
                        }
                    }

                    memset(packageone, 0, sizeof(packageone));
                    ascii_to_hex(Oneline, packageone, j);

                    //校验一包是否正确
                    if(package_crc(packageone, (j / 2)) == 0)
                    {
                        //编程行
                        IAP_WRdata_to_flash(&packageone[0], startaddr, endaddr, offset);
                        i = i + j;
                        break;
                    }
                    else
                    {
                        myfree(ReadBuffer);
                        return 0;
                    }
                }
            }
            else
            {
                i++;
            }
        }
    }

    myfree(ReadBuffer);
    return 0;
}
#endif