#ifndef __CRC16_H
#define __CRC16_H
#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
unsigned int MyCrc16(unsigned char *updata, unsigned int len);
uint16_t CalcCRC16(uint16_t *TargetAddr, uint32_t nub);
uint8_t line_crc(uint8_t *dat, uint8_t len);
uint16_t CalcCRC16t(uint16_t Data, uint16_t CRCNow);
uint32_t uint8_to_uint32(uint8_t a, uint8_t b, uint8_t c, uint8_t d);
u8 package_crc(u8 *data, u16 len);
uint8_t CheckProgramB(uint32_t offset);
unsigned int UART_CRC16_Work(unsigned char *CRC_Buf, unsigned int CRC_Leni);
uint16_t get_meter_crc(uint8_t *dat, uint8_t nub);
#endif
