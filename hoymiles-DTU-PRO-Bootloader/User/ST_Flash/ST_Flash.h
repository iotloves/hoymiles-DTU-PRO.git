#ifndef __ST_FLASH_H
#define __ST_FLASH_H

#ifdef DTU3PRO
#include "stm32f4xx.h"
#else
#include "stm32f10x.h"
#endif
#include <stdio.h>

#define Upgrade_Len             7

//FLASH起始地址
#define STM32_FLASH_BASE 0x08000000     //STM32 FLASH的起始地址



/*定义STM32 MCU的类型*/
typedef enum
{
    STM32F0,
    STM32F1,
    STM32F2,
    STM32F3,
    STM32F4,
    STM32F7,
    STM32L0,
    STM32L1,
    STM32L4,
    STM32H7,
} MCUTypedef;


//FLASH 扇区的起始地址
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000)   //扇区0起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000)   //扇区1起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000)   //扇区2起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000)   //扇区3起始地址, 16 Kbytes  
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000)   //扇区4起始地址, 64 Kbytes  
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000)   //扇区5起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000)   //扇区6起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000)   //扇区7起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000)   //扇区8起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000)   //扇区9起始地址, 128 Kbytes  
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000)   //扇区10起始地址,128 Kbytes  
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000)   //扇区11起始地址,128 Kbytes  

#ifdef DTU3PRO
#define FLASH_ADDR_BOOTLOAD             FLASH_BASE+0                //bootload      48K


//#define FLASH_ADDR_APP_CRC              0x08020000         //应用程序校验 2K
//#define FLASH_ADDR_APP                  0x08020200         //应用程序
#define FLASH_ADDR_APP_CRC              0x08010000         //应用程序校验 2K
#define FLASH_ADDR_APP                  0x08010200         //应用程序
#define FLASH_ADDR_APP_END              FLASH_BASE+512*1024         //应用程序结束地址 2K

//#define FLASH_ADDR_BIN_CRC              FLASH_BASE+268*1024         //bin文件校验 2K 0x08043000
//#define FLASH_ADDR_BIN                  FLASH_BASE+270*1024         //BIN文件  200K 0x08043800
//#define FLASH_ADDR_BIN_END              FLASH_BASE+470*1024         //BIN文件  结束 0x08075800
//#define FLASH_ADDR_END                  FLASH_BASE+512*1024         //结束
#endif

#define PRO_FIRST_ADDR                  FLASH_ADDR_APP_CRC          //存放程序的首地址的地址   32
#define PRO_END_ADDR                    FLASH_ADDR_APP_CRC+4        //存放程序末地址的地址     32
#define PRO_CRC_ADDR                    FLASH_ADDR_APP_CRC+8        //存放程序校验信息的地址   16
#define VERSION                         FLASH_ADDR_APP_CRC+10       //存放硬件版本信息的地址   16
#define SELF_CRC_ADDR                   FLASH_ADDR_APP_CRC+12       //存放自校验信息的地址     16


//void FLASH_WriteMoreData(uint32_t startAddress, uint16_t *writeData, uint16_t countToWrite);

//void FLASH_ReadMoreData(uint32_t startAddress, uint16_t *readData, uint16_t countToRead);

void FLASH_ERASURE(uint32_t startAddress, uint32_t endAddress);

void FLASH_Write(uint32_t WriteAddr, uint32_t *pBuffer, uint32_t NumToWrite);

void FLASH_Read(uint32_t ReadAddr, uint32_t *pBuffer, uint32_t NumToRead);

u8 IAP_Myprogram(uint32_t startaddr, uint32_t endaddr, uint32_t offset);

void GetSTM32MCUID(uint32_t *id, MCUTypedef type);

uint16_t GetSTM32FlashSize(void);
#endif

